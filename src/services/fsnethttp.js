import { Constants } from '../constants/constants';
import API from "./index";
var axios = require('axios');

export class Fsnethttp{

    constructor(){
        this.Constants = new Constants();
    }


    /************User Settings API Calls Start****************/
    //Change Password
    changePassword(obj) {
        return API.post(this.Constants.BASE_URL+'changePassword', obj);
    }

    //Signature Password
    signaturePassword(obj) {
        return API.post(this.Constants.BASE_URL+'signaturePassword', obj);
    }

    //Get Signature exists or not for the logged in user
    checkSignaturePassword() {
        return API.get(this.Constants.BASE_URL+'checkSignaturePassword');
    }
    //verifySignature Password
    verifySignature(obj) {
        return API.post(this.Constants.BASE_URL+'verifySignaturePassword', obj);
    }

    //Get user profile data
    getUserProfile(userId) {
        return API.get(this.Constants.BASE_URL+`profile/${userId}`);
    }

    //Get user profile data
    getProfile() {
        return API.get(this.Constants.BASE_URL+`profile`);
    }

    //Get Individual user profile data
    getPublicProfile(data) {
        return API.get(this.Constants.BASE_URL+`user/public/profile/${data.id}?accountType=${data.accountType}`);
    }

    //Update Impersonated in privacy
    updatePrivacy(obj) {
        return API.post(this.Constants.BASE_URL+'userSettings', obj);
    }
    
    //Update user profile
    updateProfile(obj) {
        return API.post(this.Constants.BASE_URL+'profile', obj);
    }

    //Update user signature
    upadateSignature(obj) {
        return API.post(this.Constants.BASE_URL+'signaturePic', obj);
    }

    //LP Signature and Capital Commitment Signature 
    signatureAppendAPI(type,obj) {
        const url = type === 'SA'?'lp/signature': type === 'SL' ? 'lp/changeSideletter/signature':type === 'AA' ? 'lp/signature/ammendment':type === 'FA' ? 'lp/signature/fundagreement' : type === 'CPFA' ? 'lp/signature/fundagreement' : 'lp/changeCommitment/signature'
        return API.post(this.Constants.BASE_URL+url, obj);
        
    }

    sendDocuments(obj) {
        return API.post(this.Constants.BASE_URL+'lp/sendDocuments', obj);
    }

    viewSignedDoc(subscriptionId) {
        return API.get(this.Constants.BASE_URL+'lp/viewSignedDocs/'+subscriptionId);
    }

    /************User Settings API Calls End****************/

    // Get email id of the user using ID from the url.
    getInvitationData(data){
        return API.get(this.Constants.BASE_URL+'getInvitationData/'+data);
    }

    //check username exists
    checkUserName(userName) {
        return API.post(this.Constants.BASE_URL+'isUsernameAvailable', userName);
    }

    //User login
    login(userObj) {
        return API.post(this.Constants.BASE_URL+'login', userObj);
    }

    logout() {
        return API.get(this.Constants.BASE_URL+'logout');
    }

    //create vc frim
    createVcFirm(obj) {
        return API.post(this.Constants.BASE_URL+'createVCFirm', obj);
    }

    //FSNET registration
    register(userObj) {
        return API.post(this.Constants.BASE_URL+'register', userObj,{
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        });
    }

    //Forgot password

    forgotPassword(obj) {
        return API.post(this.Constants.BASE_URL+'forgotPassword', obj);
    }

    //Set Password
    setPassword(urlValues,postObj) {
        return API.post(this.Constants.BASE_URL+`user/setPassword/${urlValues.code}/${urlValues.id}`,postObj);
    }
   
    //Set Password
    userAcceptCode(postObj) {
        return API.post(this.Constants.BASE_URL+`fund/changegp/accepted`,postObj);
    }


    //Forgot username
    forgotUsername(obj) {
        return API.post(this.Constants.BASE_URL+'forgotUsername', obj);
    }

    //Get forgotPasswordGetUsername
    forgotPasswordGetUsername(obj) {
        return API.post(this.Constants.BASE_URL+'forgotPasswordGetUsername', obj);
    }

    //Verify forgot password code
    verifycode(obj) {
        return API.post(this.Constants.BASE_URL+'forgotPasswordTokenValidate', obj);
    }

    //Reset password
    resetPassword(obj) {
        return API.post(this.Constants.BASE_URL+'resetPassword', obj);
    }

    //Create fund details step1
    fundStore(obj) {
        return API.post(this.Constants.BASE_URL+'fund/store', obj);
    }

    //remove upload files
    deleteFile(obj) {
        return API.post(this.Constants.BASE_URL+'/fund/remove/file', obj);
    }

    //Get list of gp delegates
    getGpDelegates(firmId,fundId) {
        return API.get(this.Constants.BASE_URL+'fund/gpDelegateList/'+firmId+'/'+fundId);
    }

    //Get list of LP's
    getLp(firmId,fundId) {
        return API.get(this.Constants.BASE_URL+'fund/lpList/'+firmId+'/'+fundId);
    }

    //Get LP's Sort
    getLpSort(firmId,fundId, colName,sortVal) {
        return API.get(this.Constants.BASE_URL+'fund/lpList/'+firmId+'/'+fundId+ '/'+colName+'/'+sortVal);
    }

    //Invite Gp Delegates to the fund
    addGpDelegate(obj) {
        return API.post(this.Constants.BASE_URL+'fund/gpDelegate/invite', obj);
    }
    
    //Invite Gp Signatories to the fund
    addGpSignatory(obj) {
        return API.post(this.Constants.BASE_URL+'fund/gpSignatory/invite', obj);
    }

    checkEmail(url, obj) {
        return API.post(this.Constants.BASE_URL+url, obj);
    }

    //Invite LP to the fund
    addLp(obj,url) {
        return API.post(this.Constants.BASE_URL+url, obj);
    }

    //Invite LP to the fund
    updateLp(obj,url) {
        return API.post(this.Constants.BASE_URL+url, obj);
    }

    //Remove LP to the fund
     removeLp(obj) {
        return API.post(this.Constants.BASE_URL+'fund/removeLp', obj);
    }

    //Remove LP from LP list in fund
    removeLpFromFund(obj, URL) {
        return API.post(`${this.Constants.BASE_URL}${URL}`, obj);
    }

    //Remove GP Signatory from GP Signatory list in fund
    removeGPSignatoryFromFund(obj) {
        return API.post(this.Constants.BASE_URL+'fund/removeFundGPSignatory ', obj);
    }

    //unselect closed LP from LP list in fund
    removeClosedLpFromFund(obj) {
        return API.post(this.Constants.BASE_URL+'fund/delete/removeClosedLp', obj);
    }

    //Deactivate fund
    deactivateFund(obj) {
        return API.post(this.Constants.BASE_URL+'fund/deactivate', obj);
    }
   
    //send reminder to LP
    sendReminder(obj) {
        return API.post(this.Constants.BASE_URL+'fund/sendRemainder', obj);
    }

    //Remove LP to the fund
    removeGp(obj) {
        return API.post(this.Constants.BASE_URL+'fund/removeFundGpDelegate', obj);
    }

    //Assign GP delegates to the fund.
    assignDelegatesToFund(obj) {
        return API.post(this.Constants.BASE_URL+'fund/assignGpDelegate', obj);
    }

    //Assign LP to the fund.
    assignLpToFund(obj) {
        return API.post(this.Constants.BASE_URL+'fund/assignLp', obj);
    }

    //Assign each LP to fund
    assingLp(obj) {
        return API.post(this.Constants.BASE_URL+'fund/assign/lp', obj);
    }

    //Invite investos to fund
    inviteInvestors(obj) {
        return API.post(this.Constants.BASE_URL+'fund/sendLpInvitations', obj);
    }

    //Assign GP Signatory to the fund.
    assignGpSignatoryToFund(obj) {
        return API.post(this.Constants.BASE_URL+'fund/assignGpSignatory', obj);
    }

    //Assign LP Signatory to the fund.
    assignLpSignatoryToFund(obj) {
        return API.post(this.Constants.BASE_URL+'fund/assignLpSignatory', obj);
    }

    //Settings for GP Signatories sign
    signatorySettings(obj) {
        return API.post(this.Constants.BASE_URL+'fund/signatory/settings', obj);
    }

    //Get list of funds
    getListOfAllFunds() {
        return API.get(this.Constants.BASE_URL+'funds');
    }

    //Get fund
    getFund(id) {
        return API.get(this.Constants.BASE_URL+'fund/'+id);
    }

    //Get VC Firms for Delegate
    getVcFirms(delegateId) {
        return API.get(this.Constants.BASE_URL+'getVCfirms/'+delegateId);
    }

    checkConsent(id) {
        return API.get(this.Constants.BASE_URL+'delegate/checkConsentForClose/fund/'+id);
    }

    //Get fund LPS
    getFundLps(id) {
        return API.get(this.Constants.BASE_URL+'fund/lps/'+id);
    }

    //Get Addendums
    getAddendums(id) {
        return API.get(this.Constants.BASE_URL+'getFundsAddendums/'+id);
    }

    //Add Addendums
    addAddendums(obj) {
        return API.post(this.Constants.BASE_URL+'addendum',obj);
    }

    //Get LP names for document locker GP
    getLpList(vcFirmId,fundId) {
        return API.get(this.Constants.BASE_URL+`fund/lpList/${vcFirmId}/${fundId}`);
    }

    //Get Document Types for document locker GP
    getDocumentTypes() {
        return API.get(this.Constants.BASE_URL+`getDocumentTypes`);
    }

    //Get Document Locker for GP
    getGpDocumentLocker(fundId) {
        return API.get(this.Constants.BASE_URL+`gpDocumentLocker/${fundId}`);
    }

    //Get gpSideLetter for GP
    gpSideLetter(fundId) {
        return API.get(this.Constants.BASE_URL+`gpSideLetter/${fundId}`);
    }

    //Get gpSideLetter for GP
    gpAmendment(fundId) {
        return API.get(this.Constants.BASE_URL+`getGPAmendments/${fundId}`);
    }
    
    //Get Document Locker for LP
    getLpDocumentLocker(subscriptionId) {
        return API.get(this.Constants.BASE_URL+`lpDocumentLocker/${subscriptionId}`);
    }

    //Get Pending Actions 
    pendingActions(subscriptionId) {
        return API.get(this.Constants.BASE_URL+`lp/pendingActions/${subscriptionId}`);
    }

    //Filter the document locker for GP
    filterDocumentLocker(fundId,lpId, lastName, filter,orderType) {
        let params=''
        if(lpId) {
            params = params+`lpId=${lpId}`
        }
        if(lastName) {
            params = params+`&orderCol=${lastName}&order=${orderType}`
        }
        if(filter) {
            params = params+`&docType=${filter}`
        }
        return API.get(this.Constants.BASE_URL+`gpDocumentLocker/${fundId}?${params}`);
    }

    // download files
    downloadFiles(obj,type) {
        return API.post(this.Constants.BASE_URL+`download/${type ? type : 'files'}`, obj);
    }
    
    downloadTracker(fundId) {
        return API.get(this.Constants.BASE_URL+`fund/downloadTracker/${fundId}`);

    }

    getOfflinePendingDocuments(subscriptionId) {
        return API.get(this.Constants.BASE_URL+`getOfflinePendingDocuments/${subscriptionId}`);
    }

    //Get fund Info
    getFundInfo(id) {
        return API.get(this.Constants.BASE_URL+'fund/view/info/'+id);
    }

    //Get fund
    getFundDetails(params) {
        let url = `${this.Constants.BASE_URL}fund/view/${params.fundId}?status=${params.status}&offset=${params.offset}&orderCol=${params.orderCol}&order=${params.order}`;
        return API.get(url);
    }

    //Get Fund Summary details.
    fundTotalSummaryDetails(fundId,status) {
        return API.get(this.Constants.BASE_URL+`fund/view/${fundId}/total?status=${status}`);
    }


    //Get Left Side Fund Summary details.
    fundSummaryDetails(fundId) {
        return API.get(this.Constants.BASE_URL+`fund/view/${fundId}/leftSideSummary`);
    }

    //Get Fund Summary Right side details.
    fundRightSideSummaryDetails(fundId,status) {
        return API.get(this.Constants.BASE_URL+`fund/view/${fundId}/rightSideSummary?status=${status}`);
    }

    //Get search based list of funds
    getSearchFunds(name) {
        return API.get(this.Constants.BASE_URL+'funds/'+name);
    }

    //Start Fund API
    startFund(fundId) {
        return API.post(this.Constants.BASE_URL+'fund/publish', fundId);
    }

    //Upload patnership document
    uploadDocumentToFund(fundId, obj) {
        return API.post(this.Constants.BASE_URL+'fund/document/add/'+fundId, obj);
    }

    //Upload Manual Signature document
    uploadManualSignatureDocumentToFund(fundId, obj) {
        return API.post(this.Constants.BASE_URL+'fund/additionalSignatoryPages/add/'+fundId, obj);
    }

    //Delete Manual Signaturedocument
    deleteManualSignatureDocumentToFund(obj) {
        return API.post(this.Constants.BASE_URL+'fund/additionalSignatoryPages/remove', obj);
    }

    /************LP Subscription API Calls Start***************/
    //Get Subscription document when user click on Confirm and submit in Review screen.
    getSubscriptionDoc(subscriptionId) {
        return API.get(this.Constants.BASE_URL+`lp/subscription/pdf/${subscriptionId}`);
    }

    getSubscriptionAgrement(subscriptionId,url) {
        return API.get(this.Constants.BASE_URL+`lp/${url}/${subscriptionId}`);
    }

    lpSignatorydocsForSignatories(obj) {
        return API.post(this.Constants.BASE_URL+'lp/submit/docsForSignatories', obj);
    }

    //Get LP funds
    getListOfLpFunds() {
        return API.get(this.Constants.BASE_URL+'lp/fund/list');
    }
    //Get LP Subscription details
    getLpSubscriptionDetails(id) {
        return API.get(this.Constants.BASE_URL+'lp/subscription/'+id);
    }

    getCapitalCommitmentChangeAgreement(id) {
        return API.get(this.Constants.BASE_URL+'lp/subscription/'+id);
    }

    getCapitalCommitmentChangeDoc(id) {
        return API.get(this.Constants.BASE_URL+'document/view/cc/'+id);
    }
    
    getSideLetterAgreement(id) {
        return API.get(this.Constants.BASE_URL+'getSideLetterAgreement/'+id);
    }

    //Get Previous Fund details
    previousFundDetails(id) {
        return API.get(this.Constants.BASE_URL+'lp/prePopulate/subscription/'+id);
    }

    //Submit Prepopulate
    SubmitPrePopulate(obj) {
        return API.post(this.Constants.BASE_URL+'lp/subscription/clone', obj);
    }

    //Search pre populate funds
    searchPrePopulateFunds(id,searchName) {
        return API.get(this.Constants.BASE_URL+`lp/prePopulate/subscription/${id}/${searchName}`);
    }

    //Update LP Subscription details
    updateLpSubscriptionDetails(obj) {
        return API.post(this.Constants.BASE_URL+'lp/subscription', obj);
    }

    //Check Unique Title
    titleUniqueCheck(obj) {
        return API.post(this.Constants.BASE_URL+'titleUniqueCheck', obj);
    }

    //Delete Subscription
    deleteSubscription(obj) {
        return API.post(this.Constants.BASE_URL+'deleteSubscription', obj);
    }

    //Restore Subscription
    restoreInvestment(obj) {
        return API.post(this.Constants.BASE_URL+'restoreDeclinedInvestment', obj);
    }

    //Reject Fund
    rejectGPInvitedFund(obj) {
        return API.post(this.Constants.BASE_URL+'lp/fund/reject', obj);
    }
    
    //Accept Fund
    acceptGPInvitedFund(obj) {
        return API.post(this.Constants.BASE_URL+'lp/fund/accept', obj);
    }

    //Get Investor Sub Types
    getInvestorSubTypes() {
        return API.get(this.Constants.BASE_URL+'getInvestorSubType/LLC');
    }

    //Get Investor Sub Types
    getInvestorTrustSubTypes() {
        return API.get(this.Constants.BASE_URL+'getInvestorSubType/Trust');
    }

    //Get Investor Sub Types
    submitSubscription(id) {
        return API.get(this.Constants.BASE_URL+'pdf/'+id);
    }

    //Rescind Fund
    rescindSubscription(id) {
        return API.get(this.Constants.BASE_URL+'rescindSubscription/'+id);
    }

    //Send for Signature for LP Delegate
    sendForSignatureByDelegate(obj) {
        return API.post(this.Constants.BASE_URL+'sendForSignatureByDelegate', obj);
    }

    //Get Jurisdiction Types
    getJurisdictionTypes(url) {
        return API.get(this.Constants.BASE_URL+url);
    }

    //Invite Gp Delegates to the fund
    addLpDelegate(obj) {
        return API.post(this.Constants.BASE_URL+'fund/lpDelegate/invite', obj);
    }
    
    //Invite Gp Delegates to the fund
    addLpDelegateSignatory(obj,url) {
        return API.post(this.Constants.BASE_URL+url, obj);
    }

    //Get LP Delegates
    getLpDelegates(id, subscriptionId) {
        return API.get(this.Constants.BASE_URL+'fund/lpDelegateList/'+id+'/'+subscriptionId);
    }

    //Remove LP Delegate
    removeLpDelegate(obj) {
        return API.post(this.Constants.BASE_URL+'fund/removeFundLpDelegate', obj);
    }

    //Remove LP Signatory
    removeLpSignatory(obj) {
        return API.post(this.Constants.BASE_URL+'fund/removeFundLpSignatory', obj);
    }

    //get All Countries
    getAllCountries() {
        return API.get(this.Constants.BASE_URL+'getAllCountries');
    }

    //get All State
    getUSStates() {
        return API.get(this.Constants.BASE_URL+'getUSStates');
    }

    //get All states by country
    getStates(countryId) {
        return API.get(this.Constants.BASE_URL+`getStates/${countryId}`);
    }

    //Get closed ready users for close fund
    getClosedReadyLps(fundId) {
        return API.get(this.Constants.BASE_URL+`fund/getClosedReadyLps/${fundId}`);
    }

    //Check closed ready users  or delegates approved for close fund
    checkClosedReadyLps(fundId) {
        return API.get(this.Constants.BASE_URL+`fund/check/ClosedReadyLps/${fundId}`);
    }

    //Close Fund
    closeFund(obj,url) {
        return API.post(this.Constants.BASE_URL+url,obj);
    }

    //Approve Closing
    approveClosingGpSignatory(obj) {
        return API.post(this.Constants.BASE_URL+`gp/signatory/signature`,obj);
    }

    //Approve Closing
    approveClosing(obj) {
        return API.post(this.Constants.BASE_URL+`delegate/lp/approvalForClose`,obj);
    }

    //Send Message to user
    sendMessage(obj) {
        return API.post(this.Constants.BASE_URL+`sendMessage`,obj);
    }

    //Increase fund commitment
    lpCommitmentChange(obj) {
        return API.post(this.Constants.BASE_URL+`lpCommitmentChange`,obj);
    }

    //gp Counter sign for Fund Commitment
    signFundCommitment(obj) {
        return API.post(this.Constants.BASE_URL+`lpCommitmentChange/gp/sign`,obj);
    }

    //gp Counter Side Letter
    signSideLetter(obj) {
        return API.post(this.Constants.BASE_URL+`lpSideletter/gp/sign`,obj);
    }

    //Get All Alerts for the user
    getAllAlerts(id) {
        return API.get(this.Constants.BASE_URL+`user/notifications/${id}`);
    }

    //Dismiss All Alerts for the user
    dismissAllAlerts() {
        return API.post(this.Constants.BASE_URL+`user/notifications/dismissAll`);
    }

    //Dismiss Alert for the user
    dismissAlert(obj) {
        return API.post(this.Constants.BASE_URL+`user/notifications/dismiss`,obj);
    }

    /************LP Subscription API Calls End****************/
    /************Docusign API Calls Start****************/
    //Get Docusign URL
    getAggrement(envelopId,subscriptionId) {
        return API.get(this.Constants.BASE_URL+`getPartnershipAgreement/${envelopId}/${subscriptionId}`);
    }
    //Get Patnership document URL
    getPatnershipAggrement(subscriptionId) {
        return API.get(this.Constants.BASE_URL+`getPartnershipAgreementPDF/${subscriptionId}`);
    }
    //Get Addendum URL
    getAddendumAgreement(envelopId,subscriptionId) {
        return API.get(this.Constants.BASE_URL+`getAddendumAgreement/${envelopId}/${subscriptionId}`);
    }
    envelopStatus(envelopId,type) {
        if(type === null) {
            return API.get(this.Constants.BASE_URL+`recipients/status/${envelopId}`);
        } else {
            return API.get(this.Constants.BASE_URL+`recipients/status/${envelopId}/${type}`);
        }
    }
    getCapitalCommitmentChangeAgreement(envelopId) {
        return API.get(this.Constants.BASE_URL+`getCapitalCommitmentChangeAgreement/${envelopId}`);
    }

    getFundDoc(url) {
        return API.get(`${url}`);
    }

    deleteSubscription (obj) {
        return API.post(this.Constants.BASE_URL+`lp/delete`,obj);
    }


    /************Docusign API Calls End****************/

    /*********************Get Json file */
    getJson(page) {
        return API.get(`${this.Constants.URL}/lang/en/${page}.json`); 
    }

    /****Edit Subscription form API calls */
    getSectionData(fundId,subscriptionId,num){
        return API.get(this.Constants.BASE_URL+`subscription/form/get/${fundId}/${subscriptionId}/${num}`);
    }

    reIssueSubscriptionAgreement(fundId,subscriptionId) {
        return API.get(this.Constants.BASE_URL+`reIssueSubscriptionAgreement/${fundId}/${subscriptionId}`);
    }

    generateSubscriptionDoc(fundId,subscriptionId) {
        return API.get(this.Constants.BASE_URL+`subscription/sample/generate/${fundId}/${subscriptionId}`);
    }

    getSubscriptonEditHtmlStatus(fundId,subscriptionId) {
        return API.get(this.Constants.BASE_URL+`subscription/form/html/status/${fundId}/${subscriptionId}`);
    }

    updateSectionData(fundId,subscriptionId,num,postObj){
        return API.post(this.Constants.BASE_URL+`subscription/form/edit/${fundId}/${subscriptionId}/${num}`,postObj);
    }

    showDefaultPdf(fundId,subscriptionId) {
        return API.get(this.Constants.BASE_URL+`subscription/sample/generate/${fundId}/${subscriptionId}`);
    }

    restoreToDefault(fundId,subscriptionId,section) {
        return API.get(this.Constants.BASE_URL+`subscription/form/restoreToDefault/${fundId}/${subscriptionId}/${section}`);
    }

    saveQuestion(postObj,id) {
        if(id) {
            return API.post(this.Constants.BASE_URL+`question/edit/${id}`,postObj);
        }else {
            return API.post(this.Constants.BASE_URL+`question/add`,postObj);
        }
    }
    
    deleteQuestion(id) {
        return API.post(this.Constants.BASE_URL+`question/delete/${id}`);
    }

    questionList(fundId,subscriptionId) {
        return API.get(this.Constants.BASE_URL+`question/list/${fundId}/${subscriptionId}`);
    }

    checkFundStatus(fundId) {
        return API.get(this.Constants.BASE_URL+`checkFundStatus/${fundId}`);
    }

    existingFAandAmmendments(fundId) {
        return API.get(this.Constants.BASE_URL+`existingFAandAmmendments/${fundId}`);
    }

    displayAmmendmentTracker(fundId) {
        return API.get(this.Constants.BASE_URL+`displayAmmendmentTracker/${fundId}`);
    }
    
    investorsApprovedDetails(fundId,ammendmentId) {
        return API.get(this.Constants.BASE_URL+`investorsDetails/${fundId}/${ammendmentId}`);
    }

    effectuateOrTerminate(url,obj) {
        return API.post(this.Constants.BASE_URL+`${url}`,obj);
    }

    uploadWithoutInvestorConsent(formData,fundId,type,url) {
        return API.post(this.Constants.BASE_URL+`${url}/${fundId}/${type}`,formData);
    }

    uploadDocsToClosedInvestors(obj,fundId,type) {
        return API.post(this.Constants.BASE_URL+`uploadDocsToClosedInvestors/${fundId}/${type}`,obj);
    }

    uploadSectionFile(obj) {
        return API.post(this.Constants.BASE_URL+`subscription/agreement`,obj);
    }

     // download files
     downloadSectionDoc(fundId,subscriptionId,section) {
        return API.get(this.Constants.BASE_URL+`subscription/${section}/download/${fundId}/${subscriptionId}`);
    }


    /* Admin Dashboard API Calls */
    getFirms(obj) {
        const queryParams = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
        return API.get(this.Constants.BASE_URL+`firm/list/?${queryParams}`);
    }
    getFunds(vcFirmId,obj) {
        let queryParams;
        if(obj) {
            queryParams = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
        }
        return API.get(this.Constants.BASE_URL+`firm/fund/list/${vcFirmId}/?${queryParams}`);
    }
    sendInviteToGp(obj) {
        return API.post(this.Constants.BASE_URL+`sendGPAccountDetails`,obj);
    }
    triggerAccountAndFundDetailsEmail(obj) {
        return API.post(this.Constants.BASE_URL+`triggerAccountAndFundDetailsEmail`,obj);
    }
    getGpandDelegateList(obj) {
        let queryParams;
        if(obj) {
            queryParams = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
        }
        return API.get(this.Constants.BASE_URL+`admin/gp/gpDelegates/list?${queryParams}`);
    }
    getLpandDelegateList(obj) {
        let queryParams;
        if(obj) {
            queryParams = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
        }
        return API.get(this.Constants.BASE_URL+`admin/lp/lpDelegates/list?${queryParams}`);
    }
    sendNotification(userId,obj) {
        return API.post(this.Constants.BASE_URL+`admin/setNotificationSetting/user/${userId}`,obj);
    }

    getAllFunds() {
        return API.get(this.Constants.BASE_URL+`listFunds`);
    }

    deleteFund(obj) {
        return API.post(this.Constants.BASE_URL+`fund/delete`,obj);
    }

    deleteUser(obj) {
        return API.post(this.Constants.BASE_URL+`admin/user/delete`,obj);
    }

    listLps() {
        return API.get(this.Constants.BASE_URL+`listLps`);
    }

    getFundLPList(id) {
        return API.get(this.Constants.BASE_URL+`listLps/${id}`);
    }

    addUser(obj,url) {
        return API.post(this.Constants.BASE_URL+url,obj);
    }

    /**** Impersonate User ******/ 
    impersonateUser(obj) {
        return API.post(this.Constants.BASE_URL+`impersonateUser`,obj);
    }

    getDocuments(id) {
        return API.get(this.Constants.BASE_URL+`lp/currentSignatureView/${id}`);
    }

    viewSADoc(id) {
        return API.get(this.Constants.BASE_URL+`document/view/sa/${id}`);
    }

    eventsLog(pageNumber,obj) {
        let queryParams;
        if(obj) {
            queryParams = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
        }
        let url = pageNumber ? `admin/events/list?limit=${pageNumber}&${queryParams}` : `admin/events/list?${queryParams}`
        return API.get(this.Constants.BASE_URL+url);
    }

    eventTypes() {
        return API.get(this.Constants.BASE_URL+`eventTypes`);
    }

    listUsers() {
        return API.get(this.Constants.BASE_URL+`listUsers`);
    }

    reInviteLPToFund(obj) {
        return API.post(this.Constants.BASE_URL+`lp/fund/reinvite`,obj);
    }

    createFormOtherSubscription(obj) {
        return API.post(this.Constants.BASE_URL+`create/subscription`,obj);
    }

    /**********Secondary Signatory To Primary Fund Manager********/
    convertToSignatory(obj) {
        return API.post(this.Constants.BASE_URL+`fund/gp/convert/to/secondary`,obj);
    }

    convertToGp(obj) {
        return API.post(this.Constants.BASE_URL+`fund/change/gp`,obj);
    }

    // Side Letters
    uploadSideLetter(obj,type) {
        const url = type == 'OfflineLP' ? 'fund/offlineSideLetter' : 'sideletter/upload'
        return API.post(this.Constants.BASE_URL+url,obj);
    }
    
    getDocTempFile(obj) {
        return API.post(this.Constants.BASE_URL+`closeFundDocUpload`,obj);
    }

    gpCancelCommitmentChange(subscriptionId) {
        return API.get(this.Constants.BASE_URL+`gpCancelCommitmentChange/${subscriptionId}`);
    }

    offlineChangeCommitment(obj) {
        return API.post(this.Constants.BASE_URL+`fund/offlineCapitalCommitment`,obj);
    }

    //Upload Amendments for Offline Lp
    offlineAA(obj) {
        return API.post(this.Constants.BASE_URL+`fund/offlinefundAmendmentSignatures`,obj);
    }

    //Upload Sub Doc for Offline Lp
    offlineSA(obj) {
        return API.post(this.Constants.BASE_URL+`fund/offlineSA`,obj);
    }

    //Upload Fund Doc Signatures for Offline Lp
    offlineLpSignature(obj) {
        return API.post(this.Constants.BASE_URL+`fund/offlineLpSignature`,obj);
    }

    //Get Fund Lps
    getFundLps(vcFirmId,fundId) {
        return API.get(this.Constants.BASE_URL+`fund/lpsList/${vcFirmId}/${fundId}`);
    }

    //Switch account
    switchAccount(obj) {
        return API.post(this.Constants.BASE_URL+`switchaccount`,obj);
    }

    listUsersForAccount() {
        return API.get(this.Constants.BASE_URL+`listUsersForAccount`);
    }

    getUserInfo() {
        return API.get(this.Constants.BASE_URL+`session/user/info`);
    }

    //Fund Agreement change 
    uploadFANonClosedInvestors(obj,fundId) {
        return API.post(this.Constants.BASE_URL+`fund/uploadFANonClosedInvestors/${fundId}`,obj);
    }

    sendRemainderToClosedInvestors(fundId,amendmentId) {
        return API.get(this.Constants.BASE_URL+`sendRemainderToClosedInvestors/${fundId}/${amendmentId}`);
    }

    checkDocForSigning(url) {
        return API.get(`${url}`);
    }

    getCPForClosedInvestors(subscriptionId,documentId) {
        return API.get(this.Constants.BASE_URL+`closedlp/getRevisedFundAgreement/${subscriptionId}/${documentId}`);
    }

}

import { reactLocalStorage } from 'reactjs-localstorage';

export class FsnetAuth{

    // Check if the user loggedin or not
    // the user logged in it will returns ture
    // Otherwise it will return false
    
    isAuthenticated(){
        let userDetails = reactLocalStorage.get('token');
        if(userDetails === "null" || userDetails === undefined){
            return false;
        }else{
            return true;
        }
    }

    isAdmin(){
        let userDetails = reactLocalStorage && reactLocalStorage.get('userData') ? JSON.parse(reactLocalStorage.get('userData')):undefined;
        if(userDetails !== undefined && userDetails.accountType === 'FSNETAdministrator'){
            return true;
        }else{
            return false;
        }
    }

}
import axios from "axios";
import { Constants } from "../constants/constants";
import { requestInterceptor } from "./RequestInterceptor";
import { responseInterceptor } from "./ResponseInterceptor";
import { requestErrorInterceptor } from "./RequestErrorInterceptor";
import { responseErrorInterceptor } from "./ResponseErrorInterceptor";

const constants = new Constants();

const axiosInstance = axios.create({
  baseURL: constants.BASE_URL
});

axiosInstance.interceptors.request.use(requestInterceptor, requestErrorInterceptor);
axiosInstance.interceptors.response.use(responseInterceptor, responseErrorInterceptor);

export default axiosInstance;

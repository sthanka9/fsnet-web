import { reactLocalStorage } from "reactjs-localstorage";
const responseErrorInterceptor = reqConfig => {
  if(reqConfig.response) {
    if(reqConfig.response.status === 403) {
      window.location.href = '/403'
    } else if(reqConfig.response.status === 404) {
      window.location.href = '/404'
    } else if(reqConfig.response.status === 406) {
      window.location.href = '/dashboard' //This is for Gp Signatories accepted while accessing the application.
    } else if(reqConfig.response.status === 401) {
      reactLocalStorage.clear();
      window.location.href = '/login'
    } else if(reqConfig.response.status === 502) {
      window.location.href = '/502'
    }else if(reqConfig.response.status === 400) {
      return Promise.reject(reqConfig);
    } else if(reqConfig.response.status === 500) {
      return Promise.reject(reqConfig);
    }
  } else {
    return Promise.reject(reqConfig);
  }
};

export { responseErrorInterceptor };

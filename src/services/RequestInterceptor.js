import { reactLocalStorage } from "reactjs-localstorage";
const requestInterceptor = reqConfig => {
  try {
    let token = reactLocalStorage.get("token");
    token = token && JSON.parse(token);
    reqConfig.headers = {
      ...reqConfig.headers,
      "x-auth-token": token
    };
    return reqConfig;
  } catch (error) {
    return Promise.reject(error);
  }
};

export { requestInterceptor };

import { Constants } from '../constants/constants';
import API from "./index";
var axios = require('axios');

export class Fundhttp{

    constructor(){
        this.Constants = new Constants();
    }


    getSubscriptionDetails(fundId,subscriptionId) {
        return API.get(this.Constants.BASE_URL+`fund/getSubscriptionDetails/${fundId}/${subscriptionId}`);
    }

    checkDocForSigning(url) {
        return API.get(`${url}`);
    }

    editUser(obj) {
        return API.post(this.Constants.BASE_URL+'edit/GpDelegateOrSignatory', obj);
    }

    addoreditUser(obj,type) {
        let url;
        if(type === 2 && ['SecondaryGP','GPDelegate'].indexOf(obj.accountType) > -1) {
            url = 'edit/GpDelegateOrSignatory'
        } else if(type === 1 && obj.accountType === 'SecondaryGP') {
            url = 'fund/gpSignatory/invite'
        } else if(type === 1 && obj.accountType === 'GPDelegate') {
            url = 'fund/gpDelegate/invite'
        } else if(type === 1 && obj.accountType === 'LP') {
            url = 'fund/add/lp'
        } else if(type === 2 && obj.accountType === 'LP') {
            url = 'fund/update/lp'
        } else if(type === 2 && obj.accountType === 'LPDelegate') {
            url = 'fund/update/lpdelegate'
        } else if(type === 1 && obj.accountType === 'OfflineLP') {
            url = 'fund/add/offlineLp'
        } else if(type === 2 && obj.accountType === 'OfflineLP') {
            url = 'fund/update/offlineLp'
        }
        return API.post(`${this.Constants.BASE_URL}${url}`, obj);
    }

    //Remove GP Signatory from GP Signatory list in fund
    removeUser(obj,type,isFromUserModal) {
        let url;
        if(type === 'SecondaryGP') {
            url = 'fund/removeFundGPSignatory'
        } else if(type === 'GPDelegate') {
            url = 'fund/removeFundGpDelegate'
        } else if(type === 'LP' && isFromUserModal) {
            url = 'fund/delete/lp'
        } else {
            url = 'lp/delete/soft'
        } 
        return API.post(`${this.Constants.BASE_URL}${url}`, obj);
    }

    /**
     * Copy or Resend eamil action
     */

    fundUserActions(obj, type) {
        let url;
        if(type === 1) {
            url = 'copy/copyLink'
        } else {
            url = 'resend/resendEmail'
        }
        return API.post(`${this.Constants.BASE_URL}${url}`, obj);
    }

    /**
     * Get list of timezones
     */
    getTimeZones() {
        return API.get(`${this.Constants.BASE_URL}gettimezones`);
    }


    /**
     * Get User status history
     */
    actionsHistory(obj) {
        return API.post(`${this.Constants.BASE_URL}actionsHistory`,obj);
    }

    /**
     * Send all invitations to the investor
     */
    sendAllLpInvitations(fundId) {
        return API.get(`${this.Constants.BASE_URL}fund/sendAllLpInvitations/${fundId}`);
    }

    /**
     * Get Firm Investors
     */
    getFirmInvestors(fundId) {
        return API.get(`${this.Constants.BASE_URL}fund/getfirmlps/${fundId}`);
    }

    /**
     * Assign Firm Investors
     */
    addExisitingInvestors(obj) {
        return API.post(`${this.Constants.BASE_URL}fund/assign/investors`,obj);
    }

}

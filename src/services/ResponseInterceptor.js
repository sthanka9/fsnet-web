import { reactLocalStorage } from "reactjs-localstorage";
const responseInterceptor = resConfig => {
  try {
    let token = reactLocalStorage.get("token");
    token = token && JSON.parse(token);
    return resConfig;
  } catch (error) {
    return Promise.reject(error);
  }
};

export { responseInterceptor };

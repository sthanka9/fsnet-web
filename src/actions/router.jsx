import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import LoginComponent from '../components/login/login.componet';
import RegisterComponent from '../components/register/register.component';
import UserAcceptComponent from '../components/userAccept/userAccept.component';
import PageNotFoundComponent from '../components/pagenotfound/pagenotfound.component';
import DashboardComponent from '../components/dashboard/dashboard.component';
import TermsAndConditionsComponent from '../components/termsandconditions/termsandconditions.component';
import ForgotPasswordComponent from '../components/forgotpassword/forgotpassword.component';
import ForgotUserNameComponent from '../components/forgotusername/forgotusername.component';
import CreateFundComponent from '../components/createfund/createfund.component';
import FundSetupComponent from '../components/fundSetup/fundSetup.component';
import editFundComponent from '../components/editfund/editfund.component';
import SetPasswordComponent from '../components/setpassword/setpassword.component';
import LpSubscriptionFormComponent from '../components/lp/lpsubscriptionform.component';
import SettingsComponent from '../components/settings/settings.component';
import PdfRenderer from '../components/pdfrenderer/pdfRenderer.component';
import editSubscriptionAggrement from '../components/editSubscriptionAggrement/editSubscription.component';
import counterSignComponent from '../components/counterSign/counterSign.component';
import adminDashboardComponent from '../components/adminDashboard/adminDashboard.component';
import UserRolesComponent from '../components/user/userRoles.component';

class FsnetRouter extends Component{

    render(){
        return(
            <Router>
                <div>
                    <Switch>
                        <Route exact path='/' component={LoginComponent}/> 
                        <Route exact path='/login' component={LoginComponent}/> 
                        <Route exact path='/register/*' component={RegisterComponent}/> 
                        <Route exact path='/user/accept' component={UserAcceptComponent}/> 
                        <Route path='/404' component={PageNotFoundComponent}/>
                        <Route path='/403' component={PageNotFoundComponent}/>
                        <Route path='/500' component={PageNotFoundComponent}/>
                        <Route path='/502' component={PageNotFoundComponent}/>
                        <Route path='/dashboard' component={DashboardComponent}/>
                        <Route path='/terms-and-conditions' component={TermsAndConditionsComponent}/>
                        <Route path='/forgot-password' component={ForgotPasswordComponent}/>
                        <Route path='/forgot-username' component={ForgotUserNameComponent}/>
                        <Route path='/user/setPassword' component={SetPasswordComponent}/>
                        <Route path='/createfund' component={CreateFundComponent}/>
                        <Route path='/fundSetup' component={FundSetupComponent}/>
                        <Route path='/lp' component={LpSubscriptionFormComponent}/>
                        <Route path='/fund' component={editFundComponent}/>
                        <Route path='/settings' component={SettingsComponent}/>
                        <Route path='/subscriptionDocumentPdf' component={PdfRenderer}/>
                        <Route path='/subscriptionPdf' component={PdfRenderer}/>
                        <Route path='/sideLetterPdf' component={PdfRenderer}/>
                        <Route path='/signDocuments' component={PdfRenderer}/>
                        <Route path='/capitalCommitmentPdf' component={PdfRenderer}/>
                        <Route path='/editSubForm/:id' component={editSubscriptionAggrement}/>
                        <Route path='/counterSign/:id' component={counterSignComponent}/>
                        <Route path='/adminDashboard' component={adminDashboardComponent}/>
                        <Route path='/userRoles' component={UserRolesComponent}/>
                        <Redirect from='*' to='/404'/>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default FsnetRouter;
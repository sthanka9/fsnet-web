import React, { Component } from 'react';
import { Row, Col, Button, FormControl } from 'react-bootstrap';
import HeaderComponent from '../authheader/header.component'
import './forgotpassword.component.css';
import { FsnetAuth } from '../../services/fsnetauth';
import { Fsnethttp } from '../../services/fsnethttp';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/rrui.css';
import 'react-phone-number-input/style.css';
import { Constants } from '../../constants/constants';
import Loader from '../../widgets/loader/loader.component';
import successImage from '../../images/success.png';

class ForgotPasswordComponent extends Component {
    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.state = {
            showForgotScreen1: true,
            showForgotScreen2: false,
            showForgotScreen3: false,
            showForgotScreen4: false,
            email: '',
            accountType: '',
            accountTypeRequired: '',
            forgotScreen1ErrorMsz: '',
            showVerifyCodeIcon: false,
            invalidVerifyCodeMsz: '',
            verificationCode: '',
            createNewPwdErr: false,
            createNewPwdErrMsz: '',
            password: '',
            confirmPassword: '',
            emailErrorBorder: false,
            accountTypeBorder: false,
            verifyCodeBorder: false,
            resetCfrmPasswordBorder: false,
            resetPasswordBorder: false,
            emailErrorMsz: '',
            isscreen1FormValid: false,
            isemailOrcellNumberValid: false,
            isAccountTypeValid: false,
            verifyCodeFormValid: false,
            isscreen3FormValid: false,
            username:'',
            hideScreen1:false,
            userNameList:[],
            usernameMsz:'',
            usernameBorder: false,
            jsonData:{}
        }
        this.showForgotScreenFn = this.showForgotScreenFn.bind(this);
        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.sendCodeAgain = this.sendCodeAgain.bind(this);
        this.verifyCode = this.verifyCode.bind(this);
    }

    componentDidMount() {
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.getJsonData()
        //Check if user already logged in
        //If yes redirect to dashboard page
        // if(this.FsnetAuth.isAuthenticated()){
        //     this.props.history.push('/dashboard');
        // }
    }

    getJsonData() {
        this.Fsnethttp.getJson('login').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    // Update state params values and login button visibility

    updateStateParams(updatedDataObject) {
        this.setState(updatedDataObject, () => {
            this.enableDisableForgotButtion();
        });
    }

    // Enable / Disble functionality of Login Button

    enableDisableForgotButtion() {
        let status = (this.state.isemailOrcellNumberValid) ? true : false;
        this.setState({
            isscreen1FormValid: status
        });
    }

    handleChangeEvent(event, type) {
        let dataObj = {};
        switch (type) {
            case 'email':
                if (event.target.value.trim() === '') {
                    this.setState({
                        email: event.target.value,
                        isemailOrcellNumberValid: false
                    })
                    dataObj = {
                        isemailOrcellNumberValid: false
                    };
                } else {
                    this.setState({
                        email: event.target.value,
                        forgotScreen1ErrorMsz: '',
                        emailErrorMsz: '',
                        emailErrorBorder: false,
                        isemailOrcellNumberValid: true
                    })
                    dataObj = {
                        isemailOrcellNumberValid: true
                    };
                }

                this.updateStateParams(dataObj);
                break;
            case 'username':
                if (event.target.value === '0' || event.target.value === '' || event.target.value === undefined) {
                    this.setState({
                        usernameBorder: true,
                        usernameMsz: this.Constants.USER_NAME_REQUIRED,
                        username:'',
                        isscreen1FormValid: false,
                    })
                } else {
                    this.setState({
                        username: event.target.value,
                        usernameBorder: false,
                        usernameMsz:'',
                        isscreen1FormValid:true
                    })
                   
                }
                break;
            case 'password':
                if (event.target.value.trim() === '' || this.state.confirmPassword === '') {
                    this.setState({
                        isscreen3FormValid: false,
                    })
                } else {
                    this.setState({
                        isscreen3FormValid: true,
                    })
                }
                this.setState({
                    password: event.target.value,
                    createNewPwdErr: false,
                    createNewPwdErrMsz: '',
                    resetPasswordBorder: false,
                })
                break;
            case 'confirmPassword':
                if (event.target.value.trim() === '' || this.state.password === '') {
                    this.setState({
                        isscreen3FormValid: false,
                    })
                } else {
                    this.setState({
                        isscreen3FormValid: true,
                    })
                }
                this.setState({
                    confirmPassword: event.target.value,
                    createNewPwdErr: false,
                    createNewPwdErrMsz: '',
                    resetCfrmPasswordBorder: false,
                })
                break;
            default:
            // do nothing
        }
    }

    verifyCode(event, button) {
        let code = '';
        if (button !== 'verifyResetButton') {
            code = event.target.value;
        } else {
            code = this.state.verificationCode
        }
        if ((event.target !== undefined && event.target.value.length === 6) || button === 'verifyResetButton') {
            this.open();
            let postObj = { email: this.state.email.trim(), code: parseInt(code) }
            let newPostObj = this.clean(postObj)
            this.Fsnethttp.verifycode(newPostObj).then(result => {
                this.close();
                if (result) {
                    this.setState({
                        showVerifyCodeIcon: true,
                        verificationCode: code,
                        verifyCodeBorder: false,
                        invalidVerifyCodeMsz: '',
                        verifyCodeFormValid: true
                    });
                    if (button === 'verifyResetButton') {
                        this.setState({
                            showForgotScreen2: false,
                            showForgotScreen3: true,
                        });
                    }
                }
            })
                .catch(error => {
                    this.close();
                    if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                        this.setState({
                            showVerifyCodeIcon: false,
                            invalidVerifyCodeMsz: error.response.data.errors[0].msg,
                            verificationCode: code,
                            verifyCodeFormValid: false,
                        });
                    }
                });
        } else {
            this.setState({
                showVerifyCodeIcon: false,
                invalidVerifyCodeMsz: '',
                verificationCode: '',
                verifyCodeBorder: false,
                verifyCodeFormValid: false,
            });
        }
    }

    // ProgressLoader : close progress loader
    close() {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open() {
        this.setState({ showModal: true });
    }

    clean(obj) {
        for (var propName in obj) {
            if (obj[propName] === null || obj[propName] === undefined || obj[propName] === '') {
                delete obj[propName];
            }
        }
        return obj;
    }

    handleVerifyCode(e) {
        console.log(e)
    }

    //Send code again
    sendCodeAgain() {
        let forgotObj = { email: this.state.email.trim() }
        let newObj = this.clean(forgotObj)
        this.Fsnethttp.forgotPassword(newObj).then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    showForgotScreen1: false,
                    showForgotScreen2: true,
                    hideScreen1:false
                });
            }
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        forgotScreen1ErrorMsz: error.response.data.errors[0].msg
                    })
                }
            });
    }


     //Get all usernames 
     checkUserNames() {
         if(!this.state.hideScreen1) {
             let postObj = { email: this.state.email }
             this.Fsnethttp.forgotPasswordGetUsername(postObj).then(result => {
                 this.close();
                 if (result.data) {
                     if(result.data && result.data.data.length === 1) {
                         this.setState({
                             username: result.data.data[0],
                         },()=>{
                             this.sendCodeAgain();
                         })
                     } else if(result.data && result.data.data.length > 0) {
                         this.setState({
                             hideScreen1: true,
                             isscreen1FormValid:false,
                             userNameList: result.data.data
                         })
                     } else {
                         this.setState({
                             forgotScreen1ErrorMsz: this.Constants.FORGOT_USER_NOT_EXISTS
                         })
                     }
                 }
             })
             .catch(error => {
                 this.close();
                 if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        forgotScreen1ErrorMsz: error.response.data.errors[0].msg,
                    })
                 }else {
                    this.setState({
                        forgotScreen1ErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                 }
             });
         } else {
            this.sendCodeAgain();
         }
    }

    //Show forgot screens based on the screen type.
    showForgotScreenFn(event, screen) {
        switch (screen) {
            case 'screen1':
                let valid = this.screen1Validations();
                if (!valid) {
                    this.open();
                    this.sendCodeAgain();
                }
                break;
            case 'screen2':
                if (this.state.verificationCode !== '') {
                    this.verifyCode(this.state.verificationCode, 'verifyResetButton');
                } else {
                    this.setState({
                        invalidVerifyCodeMsz: this.Constants.INVALID_VERIFY_CODE,
                        verifyCodeBorder: true
                    })
                }
                break;
            case 'screen3':
                let resetPwdvalid = this.screen3Validations();
                if (!resetPwdvalid) {
                    this.open();
                    this.resetPassword();
                }
                break;
            case 'screen4':
                window.location = '/';;
                break;
            default:
            // do nothing
        }

    }

    resetPassword() {
        let resetPwdObj = { email: this.state.email, username: this.state.username, code: this.state.verificationCode, password: this.state.password, passwordConfirmation: this.state.confirmPassword }
        let newPostObj = this.clean(resetPwdObj)
        this.Fsnethttp.resetPassword(newPostObj).then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    showForgotScreen3: false,
                    showForgotScreen4: true,
                });
            }
        })
        .catch(error => {
            this.close();
            if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                this.setState({
                    createNewPwdErr: false,
                    createNewPwdErrMsz: error.response.data.errors[0].msg,
                });
            }
        });
    }

    screen1Validations() {
        let email = this.state.email.trim();
        // let type = this.state.accountType;
        let error = false;
        let emailRegex = this.Constants.EMAIL_REGEX;
        // if (type === '0' || type === '') {
        //     this.setState({
        //         accountTypeRequired: this.Constants.ACCOUNT_TYPE_REQUIRED,
        //         accountTypeBorder: true
        //     })
        //     error = true;
        // }

        if (email === '') {
            this.setState({
                forgotScreen1ErrorMsz: this.Constants.EMAIL_MOBILE_REQUIRED,
                emailErrorBorder: true,
            })
            error = true;
        }

        if (email !== '' && !emailRegex.test(email)) {
            this.setState({
                emailErrorMsz: this.Constants.VALID_EMAIL,
                emailErrorBorder: true,

            })
            error = true;
        }

        if (error) {
            return true
        } else {
            return false
        }
    }

    screen3Validations() {
        let password = this.state.password.trim();
        let cnfrmPassword = this.state.confirmPassword.trim();
        let error = false;
        if (password === '') {
            this.setState({
                resetPasswordBorder: true
            });
        }
        if (cnfrmPassword === '') {
            this.setState({
                resetCfrmPasswordBorder: true
            });
        }
        if (password === '' || cnfrmPassword === '') {
            this.setState({
                createNewPwdErr: true,
                createNewPwdErrMsz: this.Constants.LOGIN_PASSWORD_REQUIRED,
            });
            error = true;
        } else if (password !== cnfrmPassword) {
            this.setState({
                createNewPwdErr: true,
                createNewPwdErrMsz: this.Constants.REQUIRED_PASSWORD_AGAINPASSWORD_SAME,
            });
            error = true;
        } else {
            let passwordRegex = this.Constants.PASSWORD_REGEX;

            if (!passwordRegex.test(this.state.password) || !passwordRegex.test(this.state.confirmPassword)) {
                this.setState({
                    createNewPwdErr: true,
                    createNewPwdErrMsz: this.Constants.PASSWORD_NOT_MATCH,
                });
                error = true;
            }
        }

        if (error) {
            return true
        } else {
            return false
        }
    }

    render() {
        return (
            <div className="parentContainer" id="forgotContainer">
                <HeaderComponent></HeaderComponent>
                <Row className="forgotContainer">
                    <div className="forgotTopBorder"></div>
                    <div className="forgotParentDiv">
                        <h1 className="forgot-text" hidden={this.state.showForgotScreen4}>{this.state.jsonData.FORGOT_PASSWORD}</h1>
                        {!this.state.hideScreen1 && !this.state.showForgotScreen2 && !this.state.showForgotScreen3 && !this.state.showForgotScreen4
                        ?
                            <div className="instructions-content" hidden={!this.state.showForgotScreen1}>{this.state.jsonData.FORGOT_PASSWORD_SCREEN1_INSTRUCTIONS}</div>
                        : 
                            null
                        }
                        <div className="instructions-content" hidden={!this.state.hideScreen1}>{this.state.jsonData.FORGOT_PASSWORD_MULTIPLE_USERS_TEXT}</div>
                        <div className="instructions-content marginBottom30" hidden={!this.state.showForgotScreen2}>{this.state.jsonData.FORGOT_PASSWORD_SCREEN2_TEXT}</div>
                        <div className="instructions-content" hidden={!this.state.showForgotScreen3}>{this.state.jsonData.FORGOT_PASSWORD_SCREEN3_TEXT}</div>
                        <Row hidden={!this.state.showForgotScreen1}>
                                <div hidden={this.state.hideScreen1} className="marginTop24">
                                    <Col lg={12} md={12} sm={12} xs={12} className="marginBot20">
                                        <label className="forgot-label-text">{this.state.jsonData.EMAIL_ADDRESS}</label>
                                        <input type="text" name="email" onChange={(e) => this.handleChangeEvent(e, 'email')} className={"forgotFormControl " + (this.state.emailErrorBorder ? 'inputError' : '')} placeholder={this.state.jsonData.EMAIL_ADDRESS_PLACEHOLDER} /><br />
                                        <span className="error">{this.state.emailErrorMsz}</span>
                                    </Col>
                                </div>
                                <div className="error width100 marginLeft15">{this.state.forgotScreen1ErrorMsz} </div>
                                <Row>
                                    <Button className={"forgot-submit " + (this.state.isscreen1FormValid ? 'btnEnabled' : '')} disabled={!this.state.isscreen1FormValid} onClick={(e) => this.showForgotScreenFn(e, 'screen1')}>{this.state.jsonData.SUBMIT_TEXT}</Button> <br />
                                </Row>
                                <label className="cancel-text cancelBtn"> <a href="/login">{this.state.jsonData.CANCEL_TEXT}</a></label>
                        </Row>
                        {
                            this.state.showForgotScreen2 &&
                            <Row>
                                <Col lg={12} md={12} sm={12} xs={12}>
                                    <label className="forgot-label-text">{this.state.jsonData.VERIFICATION_CODE}</label>
                                    <input type="text" name="verifyPhoneNumber" placeholder={this.state.jsonData.VERIFICATION_CODE_PLACEHOLDER} maxLength="6" minLength="6" onChange={(e) => this.verifyCode(e)} className={"forgotFormControl marginBotNone " + (this.state.verifyCodeBorder ? 'inputError' : '')} />
                                    <div className="code-success-icon" hidden={!this.state.showVerifyCodeIcon}>
                                        <i className="fa fa-check-circle" aria-hidden="true"></i>
                                    </div>
                                    <span className="sendCodeAgain" onClick={this.sendCodeAgain}>{this.state.jsonData.SEND_CODE_AGAIN}</span><br />
                                    <span className="error">{this.state.invalidVerifyCodeMsz}</span>
                                </Col>
                                <Row>
                                    <Button className={"forgot-submit " + (this.state.verifyCodeFormValid ? 'btnEnabled' : '')} disabled={!this.state.verifyCodeFormValid} onClick={(e) => this.showForgotScreenFn(e, 'screen2')}>{this.state.jsonData.RESET_PASSWORD}</Button> <br />
                                </Row>
                                <label className="cancel-text cancelBtn"> <a href="/forgot-password">{this.state.jsonData.CANCEL_TEXT}</a></label>
                            </Row>
                        }
                        <Row hidden={!this.state.showForgotScreen3}>
                            <Row className="marginLeftNone marginTop20">
                                <Col className="width40" lg={6} md={6} sm={6} xs={12}>
                                    <label className="forgot-label-text">{this.state.jsonData.PASSWORD}</label>
                                    <input type="password" name="password" onChange={(e) => this.handleChangeEvent(e, 'password')} className={"forgotFormControl inputMarginBottom " + (this.state.resetPasswordBorder ? 'inputError' : '')} /><br />
                                </Col>
                                <Col className="width40" lg={6} md={6} sm={6} xs={12}>
                                    <label className="forgot-label-text">{this.state.jsonData.PASSWORD_AGAIN}</label>
                                    <input type="password" onChange={(e) => this.handleChangeEvent(e, 'confirmPassword')} name="confirmPassword" className={"forgotFormControl " + (this.state.resetCfrmPasswordBorder ? 'inputError' : '')} />
                                </Col>
                            </Row>
                            <div className="passwordDNRequirements marginLeft15 marginTop10" hidden={!this.state.createNewPwdErr}>{this.state.createNewPwdErrMsz}</div>
                            <Row>
                                <Button className={"forgot-submit " + (this.state.isscreen3FormValid ? 'btnEnabled' : '')} disabled={!this.state.isscreen3FormValid} onClick={(e) => this.showForgotScreenFn(e, 'screen3')}>{this.state.jsonData.RESET_PASSWORD}</Button> <br />
                            </Row>
                            <label className="cancel-text cancelBtn"> <a href="/forgot-password">{this.state.jsonData.CANCEL_TEXT}</a></label>
                        </Row>
                        <Row hidden={!this.state.showForgotScreen4}>
                            <Col lg={12} md={12} sm={12} xs={12}>
                                <label className="forgot-success-text">{this.state.jsonData.SUCCESS}</label>
                                <label className="forgot-reset-text">{this.state.jsonData.FORGOT_PASSWORD_SCREEN4_TEXT}</label>
                                <div className="success-icon">
                                    <img src={successImage} alt="success" />
                                </div>
                                <div className="text-center">
                                    <Button className="forgot-submit fsnetBtn" onClick={(e) => this.showForgotScreenFn(e, 'screen4')}>{this.state.jsonData.SIGN_IN_TEXT}</Button> <br />
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Row>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default ForgotPasswordComponent;


import React, { Component } from 'react';
import vanillaLogo from '../../images/Vanilla.png';

class PageNotFoundComponent extends Component{

    constructor(props) {
        super(props);
        this.state = {
            status: null
        }

    }


    componentDidMount() {
        const url = window.location.href;
        if(url.indexOf('404') > -1) {
            this.setState({status:404})
        } else if(url.indexOf('500') > -1) {
            this.setState({status:500})
        } else if(url.indexOf('403') > -1) {
            this.setState({status:403})
        } else if(url.indexOf('502') > -1) {
            this.setState({status:502})
        } else {
            window.location.href = '/404'
        }
    }

    render(){
        return(
            <div className="text-center">
                <img src={vanillaLogo} alt="vanilla" className="vanilla-logo margin20" />
                <h2 hidden={this.state.status !== 404}>Not Found.</h2>
                <h2 hidden={this.state.status !== 500}>Something went wrong.</h2>
                <h2 hidden={this.state.status !== 403}>You don't have permission to access this page.</h2>
                <h2 hidden={this.state.status !== 502}>Server is down. Please try after sometime.</h2>
                <a href="/">Click here</a> <span>to go back Home.</span>
            </div>
        );
    }
}

export default PageNotFoundComponent;


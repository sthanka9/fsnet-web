import React, { Component } from 'react';
import '../settings.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Link } from "react-router-dom";
import { Row,Col, Button, FormGroup, Radio} from 'react-bootstrap';
import 'react-phone-number-input/rrui.css'
import 'react-phone-number-input/style.css'
import {Constants} from '../../../constants/constants';
import { reactLocalStorage } from 'reactjs-localstorage';
import { Fsnethttp } from '../../../services/fsnethttp';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../../util/util';

var close = {}
class privacyComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            allowGPdelegatesToSign: true,
            isImpersonatingAllowed:1,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            userList:[]
        }
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast(timed) {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }


    componentDidMount() {
        this.getUserInfo();
    }

    handleInputChangeEvent = (event,type) => {
        //console.log(event)
        if(event === 'on') {
            this.setState({
                isImpersonatingAllowed: 1
            })
        } else if(event === 'off') {
            this.setState({
                isImpersonatingAllowed: 0
            })
        }
    }

    getUserInfo = () => {
        this.open();
        
        this.Fsnethttp.getUserProfile().then(result=>{
            this.close();
            this.FsnetUtil.setLeftNavHeight();
            window.scrollTo(0, 0);
            this.setState({
                isImpersonatingAllowed:result.data.isImpersonatingAllowed === true  ? 1: 0,
                userList:JSON.parse(reactLocalStorage.get('userData'))
            })
            
        })
        .catch(error=>{
            this.close();
        });
    }

    updateUserInfo = () => {
        this.open();
        
        let obj = {isImpersonatingAllowed:this.state.isImpersonatingAllowed};
        this.Fsnethttp.updatePrivacy(obj).then(result=>{
            this.close();
            this.setState({
                showToast: true,
                toastMessage: 'User settings has been updated.',
                toastType: 'success',
            })
        })
        .catch(error=>{
            this.close();
        });
    }

    // ProgressLoader : close progress loader
     close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }
    
    render() {
        return (
            <div className="width100 privacy-container">
                <div className="main-heading"><span className="main-title">Settings</span><Link to="/dashboard" className="cancel-fund">Cancel</Link></div>
                <div className="profileContainer">
                    <h1 className="title">Privacy Options</h1>
                    <div className="subText" hidden={this.state.userList.accountType !== 'GP'}>Allow Vanilla admin access to view and edit firm Funds? This will allow an admin to impersonate a Fund Manager from firm and take actions on their behalf</div>
                    <div className="subText" hidden={this.state.userList.accountType !== 'LP'}>Allow Vanilla Fund Manager access to view and edit firm Funds? This will allow an Fund Manager to impersonate a Investor from firm and take actions on their behalf</div>
                    <Radio name="isImpersonatingAllowed" checked={this.state.isImpersonatingAllowed === 1} className="marginLeft15" inline onChange={(e) => this.handleInputChangeEvent('on','isImpersonatingAllowed')}>
                        On
                        <span className="radio-checkmark"></span>
                    </Radio>
                    <Radio name="isImpersonatingAllowed" checked={this.state.isImpersonatingAllowed === 0} className="marginLeft15" inline onChange={(e) => this.handleInputChangeEvent('off','isImpersonatingAllowed')}>
                        Off
                        <span className="radio-checkmark"></span>
                    </Radio>
                    {/* <div className="subText">Allow delegates to sign documents on behalf of Fund Manager?</div>
                    <FormGroup>
                        <Radio name="allowGPdelegatesToSign" className="marginLeft15" inline>
                            On
                            <span className="radio-checkmark"></span>
                        </Radio>
                        <Radio name="allowGPdelegatesToSign" className="marginLeft15" inline>
                            Off
                            <span className="radio-checkmark"></span>
                        </Radio>
                    </FormGroup> */}
                </div>
                <div className="footer-profile">
                    <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.updateUserInfo}>Save Changes</Button>
                </div>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>                        
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default privacyComponent;


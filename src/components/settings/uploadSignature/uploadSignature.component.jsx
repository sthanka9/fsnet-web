import React, { Component } from 'react';
import '../settings.component.css';
import Loader from '../../../widgets/loader/loader.component';
import emptyImage from '../../../images/emptyImage.png';
import { Link } from "react-router-dom";
import { Row, Col, Button, FormControl} from 'react-bootstrap';
import 'react-phone-number-input/rrui.css'
import 'react-phone-number-input/style.css'
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import { Constants } from '../../../constants/constants';
import html2canvas from 'html2canvas';

var close = {}
class uploadSignatureComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            userImageName: 'Signature_Img.jpg',
            currentUserImage: emptyImage,
            profilePicFile: {},
            showToast: false,
            signImageUser: false,
            toastMessage: '',
            toastType: 'success',
            signatureName: null,
            fontsArray:[],
            signatureFont:"azkia-demo"

        }
        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast(timed) {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    changeSignatureStyles = () => {
        if (this.state.signatureFont === "adine-kirnberg" && this.state.signatureName.indexOf('J') === -1) {
            document.getElementById('img_val').style.height = '40px'
            document.getElementById('img_val').style.lineHeight = 'initial'
        } else if(this.state.signatureFont === "adine-kirnberg" && this.state.signatureName.indexOf('J') > -1) {
            document.getElementById('img_val').style.lineHeight = '45px';
            document.getElementById('img_val').style.height = 'initial';
        } else if (this.state.signatureFont === "azkia-demo") {
            document.getElementById('img_val').style.lineHeight = '50px';
            document.getElementById('img_val').style.height = 'initial';
        } else {
            document.getElementById('img_val').style.lineHeight = 'initial';
            document.getElementById('img_val').style.height = 'initial';
        }
    }

    convertToImage = () => {
        this.changeSignatureStyles();
        if(this.state.signatureName) {
            html2canvas(document.getElementById("img_val")).then(canvas => {
                  console.log(canvas.toDataURL("image/png"))
                  this.setState({
                    currentUserImage:canvas.toDataURL("image/png")
                  },()=>{
                      this.saveProfileFn();
                  })
            });
        }
    }

    showSignPage = () => {
        this.setState({
            signImageUser:false
        })
    }

    componentDidMount() {
        this.setState({
            signatureName :this.FsnetUtil.userName()
        },()=>{
            let userId = this.FsnetUtil._getId();
            this.getUserInfo(userId);
        })
        document.getElementById('leftNavBar').style.position = 'fixed'
        let accountType = this.FsnetUtil.getUserRole();
        if(accountType !== 'GP' && accountType !== 'SecondaryGP') {
            this.props.history.push('/dashboard');
        }
        //this.getJsonData();

    }

    getJsonData = () => {
        this.Fsnethttp.getJson('fonts').then(result=>{
            this.setState({
                fontsArray:result.data
            })
        });
    }

    getUserInfo = (userId) => {
        this.open();
        
        this.Fsnethttp.getUserProfile(userId).then(result=>{
            this.close();
            this.FsnetUtil.setLeftNavHeight();
            window.scrollTo(0, 0);
            this.setState({
                currentUserImage: result.data.signaturePic  ? result.data.signaturePic : emptyImage,
                signImageUser: result.data.signaturePic ? true: false
            })
        })
        .catch(error=>{
            this.close();
        });
    }

    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }

    //USer profile pic upload.
    handleChange = (event) => {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            this.imageFile = event.target.files[0];
            let imageName = event.target.files[0].name
            var sFileExtension = imageName.split('.')[imageName.split('.').length - 1].toLowerCase();
            const imageFormats = ['png', 'jpg', 'jpeg', 'gif', 'tif', 'svg'];
            if (imageFormats.indexOf(sFileExtension) === -1) {
                document.getElementById('uploadBtn').value = "";
                alert('Please upload valid image.')
                return true;
            }
            //Change user profile image size limit here.
            if (this.imageFile.size <= 1600000) {
                this.setState({
                    profilePicFile: event.target.files[0],
                });
                reader.readAsDataURL(this.imageFile);
                this.setState({
                    userImageName: event.target.files[0].name,
                    isFormValid:true,
                    signImageUser:true
                });
                reader.onload = () => {
                    this.setState({
                        currentUserImage: reader.result,
                    });
                }
            } else {
                alert('Please upload image Maximum file size : 160X50')
            }
        }
    }

    //Clear the input file value
    removeImageBtn = () => {
        this.setState({
            userImageName: 'Signature_Img.jpg',
            currentUserImage: emptyImage,
            profilePicFile: {},
            isFormValid:false
        });
        document.getElementById('uploadBtn').value = "";
        this.setState({ signImageUser : false });
    }

    uploadBtnClick = () => {
        document.getElementById('uploadBtn').click();
    }

    saveProfileFn = () => {
        if(this.state.signatureName) {
            this.open();
            
            //call the signup api
            var formData = new FormData();
            formData.append("signaturePic", this.state.currentUserImage);
            formData.append("date", new Date());
            formData.append("timeZone", Intl.DateTimeFormat().resolvedOptions().timeZone);
            this.Fsnethttp.upadateSignature( formData).then(result => {
                this.close();
                this.setState({
                    showToast: true,
                    toastMessage: 'Signature has been uploaded successfully.',
                    toastType: 'success',
                })
                // window.location.reload();
            })
            .catch(error => {
                this.close();
    
            });
        }
    }

    returnToTracker = () => {
        let fundId = this.FsnetUtil._getId();
        this.props.history.push('/fund/view/'+this.FsnetUtil._encrypt(fundId))
    }

    render() {
        return (
            <div className="width100">
                <div className="main-heading"><span className="main-title">Configure Signature</span><Link to="/dashboard" className="cancel-fund">Cancel</Link></div>
                {/* <div className="profileContainer" hidden={!this.state.signImageUser}>
                    <label className="label-text marginBot12">Signature Image</label>
                    <Row className="profile-Row profileMargin marginBot24">
                        <Col lg={12} md={12} sm={12} xs={12}>
                            <img src={ this.state.currentUserImage } alt="profile-pic" className="profile-pic-sign" />
                            <img src={editImage} className="cursor paddingLeft10" onClick={this.showSignPage}></img>
                        </Col>
                    </Row>
                </div> */}
                <div className="profileContainer">
                    <label className="label-text marginBot12 width100">Generate Signature Image</label>
                    <Row className="marginBot24">
                        <Col lg={6} md={6} sm={6} xs={12} className="width40">
                            <label className="input-label">Signature Fonts</label>
                            <FormControl name='signatureFont' className="selectFormControl" componentClass="select" value={this.state.signatureFont} onChange={(e) => this.setState({signatureFont:e.target.value})}>
                                <option value="azkia-demo">Symphony</option>
                                <option value="adine-kirnberg">Affinity</option>
                                <option value="400 30px akadora">Caviar</option>
                                <option value="400 30px 'Great Vibes', Helvetica, sans-serif">Great Vibes</option>
                            </FormControl>
                        </Col>
                        </Row>
                        {
                            this.state.signatureFont === "adine-kirnberg" || this.state.signatureFont === "azkia-demo"?
                            <div id="img_val" style={{ fontFamily: this.state.signatureFont}}>{this.state.signatureName}</div>:
                            <div id="img_val" style={{ font: this.state.signatureFont}}>{this.state.signatureName}</div>
                        }
                    <FormControl type="text" name="signature" className="inputFormControl inputWidth290 marginBot20" maxLength="50" placeholder="John Doe" value={this.state.signatureName} onChange={(e)=>this.setState({signatureName:e.target.value})}/>
                    {/* <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.convertToImage}> Convert</Button> */}
                </div>
                <div className="footer-profile">
                    {/* <Button type="button" className={"fsnetSubmitButton " + (this.state.isFormValid ? 'btnEnabled' : 'disabled')} disabled={!this.state.isFormValid} onClick={this.saveProfileFn}>Save Changes</Button> */}
                    <Button type="button" className="fsnetSubmitButton btnEnabled marginRight20" onClick={this.convertToImage} disabled={this.state.signatureName === null || this.state.signatureName === ''}>Save Signature</Button>
                    <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.returnToTracker}>Return to Fund Tracker</Button>
                        
                </div>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default uploadSignatureComponent;


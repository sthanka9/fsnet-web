import React, { Component } from 'react';
import '../settings.component.css';
import Loader from '../../../widgets/loader/loader.component';
import userDefaultImage from '../../../images/default_user.png';
import { Link } from "react-router-dom";
import { Row,Col, Button, Checkbox as CBox, FormControl} from 'react-bootstrap';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/rrui.css'
import 'react-phone-number-input/style.css'
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import {Constants} from '../../../constants/constants';

var close = {}
class profileComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            userImageName: 'Profile_Pic.jpg',
            currentUserImage: userDefaultImage,
            profilePicFile: {},
            countriesList:[],
            statesList:[],
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            user:{},
            firstName:'',
            firstNameBorder:'',
            firstNameMsz:'',
            firstNameValid:'',
            lastName:'',
            lastNameBorder:'',
            lastNameMsz:'',
            lastNameValid:'',
            email:'',
            emailBorder:'',
            emailMsz:'',
            emailValid:'',
            cellNumber:'',
            cellNumberBorder:'',
            cellNumberMsz:'',
            cellNumberValid:'',
            streetAddress1:'',
            streetAddress2:'',
            city:'',
            state:'',
            country:'',
            zipcode:'',
            isFormValid:false,
            primary_first:'',
            primary_last:'',
            primary_email:'',
            primary_phone:'',
            primary_checked:false,
            profileErrorMsz:'',
            userId:null,
            accountType:null
            
        }
        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast(timed) {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.setState({userId:id,accountType:this.FsnetUtil.getUserRole()},()=>{
            this.getUserInfo(id);
        })
        document.getElementById('leftNavBar').style.position = ''
    }

    getUserInfo = (id) => {
        this.open();
        
        this.Fsnethttp.getUserProfile(id).then(result=>{
            this.close();
            this.FsnetUtil.setLeftNavHeight();
            window.scrollTo(0, 0);
            this.getAllCountries();
            this.setState({
                user: result.data,
                userImageName: result.data.profilePic ? result.data.profilePic.originalname : 'Profile_Pic.jpg',
                currentUserImage: result.data.profilePic ? result.data.profilePic.url : userDefaultImage,
            },()=>{
                this.updateInputFields();
                let userData = JSON.parse(reactLocalStorage.get('userData'));
                userData['firstName'] = result.data.firstName;
                userData['lastName'] = result.data.lastName;
                userData['middleName'] = result.data.middleName;
                userData['cellNumber'] = result.data.cellNumber;
                userData['profilePic'] = result.data.profilePic;
                if(this.state.userId == userData.id) {
                    reactLocalStorage.set('userData', JSON.stringify(userData));
                }
            })
        })
        .catch(error=>{
            this.close();
            this.getAllCountries();
        });
    }

    updateInputFields = () => {
        let fields = ['firstName', 'lastName', 'middleName','email', 'cellNumber', 'profilePic', 'city', 'country', 'zipcode', 'state', 'streetAddress1','streetAddress2'];
        for (let index of fields) {
            this.setState({
                [index]:this.state.user[index]
            })
        }
        // let primaryFields = [ 'primary_first', 'primary_last', 'primary_email','primary_phone']
        // for (let index of primaryFields) {
        //     if(this.state.user.primaryContact) {
        //         this.setState({
        //             [index]:this.state.user.primaryContact[index]
        //         })
        //     }
        //     if(this.state.user.primaryContact && this.state.user.primaryContact[index] !=='' && this.state.user.primaryContact[index] !== undefined && this.state.user.primaryContact[index] !== null) {
        //         this.setState({
        //             primary_checked:true
        //         })
        //     } 
        // }
        let validNames = ['isFormValid', 'firstNameValid', 'lastNameValid', 'cellNumberValid', 'emailValid']
        for (let index of validNames) {
            this.setState({
                [index]:true
            })
        }
    }

    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }

    updateStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, ()=>{
            this.enableDisableSubmitButton();
        });
    }

    // Enable / Disble functionality of Save changes Button

    enableDisableSubmitButton = () => {
        let status = (this.state.firstNameValid && this.state.lastNameValid && this.state.emailValid  ) ? true : false;
        this.setState({
            isFormValid : status
        });
    }

    //USer profile pic upload.
    handleChange = (event) => {
        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
            this.imageFile = event.target.files[0];
            let imageName = event.target.files[0].name
            var sFileExtension = imageName.split('.')[imageName.split('.').length - 1].toLowerCase();
            const imageFormats = ['png','jpg','jpeg','gif','tif','svg'];
            if(imageFormats.indexOf(sFileExtension) === -1) {
                document.getElementById('uploadBtn').value = "";
                alert('Please upload valid image.')
                return true;
            }
            //Change user profile image size limit here.
            if(this.imageFile.size <=1600000) {
                    this.setState({
                        profilePicFile : event.target.files[0],
                    });
                reader.readAsDataURL(this.imageFile);
                this.setState({
                    userImageName: event.target.files[0].name
                });
                reader.onload = () => {
                    this.setState({
                        currentUserImage : reader.result,
                    });
                }
            } else {
               alert('Please upload image Maximum file size : 512X512')
            }
        }
    }

     //Clear the input file value
     removeImageBtn = () => {
        this.setState({
            userImageName: 'Profile_Pic.jpg',
            currentUserImage : userDefaultImage,
            profilePicFile:{}
        });
        document.getElementById('uploadBtn').value = "";
    }

    uploadBtnClick = () => {
        document.getElementById('uploadBtn').click();
    }

    getAllCountries = () => {
        
        this.open();
        this.Fsnethttp.getAllCountries().then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    countriesList: result.data
                })
                this.getStates(this.state.user.country)
            }
        })
        .catch(error => {
            this.close();
        });
    }

    countryChange = (e,name) => {
        const id = e.target.value;
        if(name === 'state') {
            this.setState({
                state:id
            })
        } else {
            this.setState({
                country:id
            })
            this.getStates(id)
        }
    }

    getStates = (id) => {
        
        if(id !== null && id !== 'null' && id !== '' && id !== undefined) {
            this.open();
            this.Fsnethttp.getStates(id).then(result => {
                this.close();
                if (result.data) {
                    this.setState({
                        statesList: result.data,
                    })
                }
            })
            .catch(error => {
                this.close();
                this.setState({
                    statesList: []
                })
            });
        }
    }

    inputHandleChange = (e,type,msz) => {
        let value;
        let dataObj = {};
        if(type === 'primary_checked') {
            this.setState({
                primary_checked: e.target.checked
            })
            return true;
        }
        if(type === 'cellNumber' || type === 'primary_phone') {
            value = e;
        } else {
            if(e.target.value.trim() === '' || e.target.value.trim() === undefined) {
                value = e.target.value.trim();
            } else {
                value = e.target.value;
            }
        }
        if(value === '' || value === undefined) {
            this.setState({
                [type]: '',
                [type+'Border']: true,
                [type+'Msz']: this.Constants[msz],
                [type+'Valid']: false,
            })
            dataObj ={
                [type+'Valid'] :false
            };
            this.updateStateParams(dataObj);
        } else {
            this.setState({
                [type]: value,
                [type+'Border']: false,
                [type+'Msz']: '',
                [type+'Valid']: true,
            })
            dataObj ={
                [type+'Valid'] :true
            };
            this.updateStateParams(dataObj);
        }
    }

    saveProfileFn = () => {
        let valid = this.checkValidations();
        if(!valid) {
            this.open();
            
            //call the signup api
            var formData = new FormData();
            formData.append("firstName", this.state.firstName?this.state.firstName:'');
            formData.append("lastName", this.state.lastName?this.state.lastName:'');
            formData.append("middleName", this.state.middleName?this.state.middleName:'');
            formData.append("email", this.state.email?this.state.email:'');
            formData.append("cellNumber", this.state.cellNumber?this.state.cellNumber:'');
            formData.append("streetAddress1", this.state.streetAddress1?this.state.streetAddress1:'');
            formData.append("streetAddress2", this.state.streetAddress2?this.state.streetAddress2:'');
            formData.append("zipcode", this.state.zipcode?this.state.zipcode:'');
            formData.append("city", this.state.city?this.state.city:'');
            formData.append("country", this.state.country?this.state.country:'');
            formData.append("userId", this.state.userId?this.state.userId:'');
            if(this.state.country) {
                formData.append("state", this.state.state?this.state.state:'');
            } else {
                formData.append("state", '');
            }
            if(this.state.userImageName !== 'Profile_Pic.jpg') {
                formData.append("profilePic", this.state.profilePicFile);
            } else {
                formData.append("removedProfilePic", true);
            }
            // if(this.state.primary_checked) {
            //     let obj = {primary_first:this.state.primary_first?this.state.primary_first:'',primary_last:this.state.primary_last?this.state.primary_last:'',primary_email:this.state.primary_email?this.state.primary_email:'',primary_phone:this.state.primary_phone?this.state.primary_phone:'' }
            //     formData.append("primaryContact", JSON.stringify(obj));
            // } else {
            //     let obj = {primary_first:'',primary_last:'',primary_email:'',primary_phone:'' }
            //     formData.append("primaryContact", JSON.stringify(obj));
            // }
            this.Fsnethttp.updateProfile(formData).then(result=>{
                this.close();
                this.setState({
                    showToast: true,
                    toastMessage: this.state.accountType === 'FSNETAdministrator' ? 'Your edits to the user profile have been saved':'Profile has been updated successfully.',
                    toastType: 'success',
                },()=>{
                    let userData = JSON.parse(reactLocalStorage.get('userData'));
                    if(userData['id'] == this.state.userId) {
                        userData['firstName'] = this.state.firstName;
                        userData['middleName'] = this.state.middleName;
                        userData['lastName'] = this.state.lastName;
                        userData['profilePic'] = result.data.profilePic
                        reactLocalStorage.set('userData', JSON.stringify(userData));
                    }
                })
                setTimeout(() => {
                    window.history.back();
                }, 2000);
            })
            .catch(error=>{
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        profileErrorMsz: error.response.data.errors[0].msg,
                    });
                }
            });
        }
    }

    checkValidations = () => {
        let error = false;
        if((this.state.cellNumber !== '' && this.state.cellNumber !== null && this.state.cellNumber !== undefined) || (this.state.primary_checked && this.state.primary_phone !== '' && this.state.primary_phone !== null && this.state.primary_phone !== undefined)) {
            if((this.state.cellNumber.length < 12 || this.state.cellNumber.length > 13 ) || (this.state.primary_phone !== '' && (this.state.primary_phone.length < 12 || this.state.primary_phone.length > 13 ))) {
                this.setState({
                    profileErrorMsz: this.Constants.CELL_NUMBER_VALID
                })
                error = true;
            }
        } 
        if(this.state.primary_email !== '' && this.state.primary_email !== null && this.state.primary_email !== undefined || this.state.email !== '' && this.state.email !== null && this.state.email !== undefined) {
            let emailRegex = this.Constants.EMAIL_REGEX;
            if ((this.state.primary_email !== '' && !emailRegex.test(this.state.primary_email)) || !emailRegex.test(this.state.email)) {
                this.setState({
                    profileErrorMsz: this.Constants.VALID_EMAIL
                })
                error = true;
            }
        }
        if (error) {
            return true
        } else {
            return false
        }
    }
    
    render() {
        return (
            <div className="width100">
                <div className="main-heading"><span className="main-title">Edit Profile</span><Link to="/dashboard" className="cancel-fund">Cancel</Link></div>
                <div className="profileContainer">
                    <h1 className="title">Personal Details</h1>
                    {
                        this.state.accountType !== 'GP'?
                            <p className="subtext marginBottom25">To change your name or email address, please contact the Fund Manager.</p>:''
                    }
                    <form>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">First Name</label>
                                <FormControl type="text" name="firstName" value={this.state.firstName || ''} maxLength="200" placeholder="John" className={"inputFormControl " + (this.state.firstNameBorder ? 'inputError' : '')} onChange={ (e) => this.inputHandleChange(e, 'firstName')} onBlur={ (e) => this.inputHandleChange(e, 'firstName','FIRST_NAME_REQUIRED')}/>
                                <span className="error">{this.state.firstNameMsz}</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Last Name</label>
                                <FormControl type="text" name="lastName" className={"inputFormControl " + (this.state.lastNameBorder ? 'inputError' : '')} value={this.state.lastName || ''} maxLength="200" placeholder="Doe" onChange={ (e) => this.inputHandleChange(e, 'lastName')} onBlur={ (e) => this.inputHandleChange(e, 'lastName','LAST_NAME_REQUIRED')}/>
                                <span className="error">{this.state.lastNameMsz}</span>
                            </Col>
                        </Row>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Middle Name or Initial</label>
                                <FormControl type="text" name="middleName" value={this.state.middleName || ''} maxLength="200" placeholder="John" className="inputFormControl" onChange={ (e) => this.inputHandleChange(e, 'middleName')}/>
                                {/* <span className="error">{this.state.middleNameMsz}</span> */}
                            </Col>
                        </Row>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Email Address</label>
                                {
                                    this.state.accountType === 'GP' || this.state.accountType === 'FSNETAdministrator'?
                                    <FormControl type="text" name="email" className={"inputFormControl " + (this.state.emailBorder ? 'inputError' : '')} id="email" value={this.state.email || ''} placeholder="JohnDoe@gmail.com" onChange={ (e) => this.inputHandleChange(e, 'email')} onBlur={ (e) => this.inputHandleChange(e, 'email','VALID_EMAIL')}/>:
                                    <FormControl type="text" name="email" className="inputFormControl" id="email" value={this.state.user.email || ''} disabled={this.state.accountType !== 'FSNETAdministrator'} placeholder="JohnDoe@gmail.com"/>
                                }
                                <span className="error">{this.state.emailMsz}</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Phone Number (Cell)</label>
                                <PhoneInput maxLength="14" placeholder="(123) 456-7890" country="US" value={this.state.cellNumber || ''} onChange={ phone => this.inputHandleChange(phone, 'cellNumber')}/>
                                {/* <span className="error">{this.state.cellNumberMsz}</span> */}
                            </Col>
                        </Row>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Street Address</label>
                                <FormControl type="text" name="street" className="inputFormControl" value={this.state.streetAddress1 || ''} placeholder="123 Easy St." onChange={ (e) => this.inputHandleChange(e, 'streetAddress1')}/>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label"></label>
                                <FormControl type="text" name="address" className="inputFormControl" value={this.state.streetAddress2 || ''} placeholder="Apartment, Suite, Unit, Building, etc." onChange={ (e) => this.inputHandleChange(e, 'streetAddress2')}/>
                            </Col>
                        </Row>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">City</label>
                                <FormControl type="text" name="city" className="inputFormControl" value={this.state.city || ''} placeholder="San Francisco" onChange={ (e) => this.inputHandleChange(e, 'city')}/>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Zip Code</label>
                                <FormControl type="text" name="zipCode" className="inputFormControl" value={this.state.zipcode || ''} placeholder="95051" onChange={ (e) => this.inputHandleChange(e, 'zipcode')}/>
                            </Col>
                        </Row>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Country/Region</label>
                                <FormControl name='country' defaultValue={0} className="selectFormControl" value={this.state.country || ''} componentClass="select" onChange={(e) => this.countryChange(e,'country')}>
                                <option value={0}>Select Country</option>
                                {this.state.countriesList.map((record, index) => {
                                    return (
                                        <option value={record['id']} key={index} >{record['name']}</option>
                                    );
                                })}
                                </FormControl>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">State</label>
                                <FormControl name='state' defaultValue={0} onChange={(e) => this.countryChange(e,'state')} className="selectFormControl" value={this.state.state || ''} componentClass="select">
                                <option value={0}>Select State</option>
                                    {this.state.statesList.map((record, index) => {
                                        return (
                                            <option value={record['id']} key={index} >{record['name']}</option>
                                        );
                                    })}
                                </FormControl>
                            </Col>
                            
                        </Row>
                        <label className="label-text marginBot12">Profile Picture: (Image must not exceed 512 x 512)</label>
                        <Row className="profile-Row profileMargin marginBot24 marginLeft15">
                            {/* <Col lg={6} md={6} sm={6} xs={12} className="width40"> */}
                                <img src={this.state.currentUserImage} alt="profile-pic" className="profile-pic"/>
                                {/* <span className="profilePicName">{this.state.userImageName}</span> */}
                            {/* </Col> */}
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <input type="file" id="uploadBtn" className="hide" onChange={ (e) => this.handleChange(e) } />
                                <Button className="uploadFile fsnetBtn" onClick={this.uploadBtnClick}>Upload File</Button> <br/>
                                <label className="removeBtn" onClick={this.removeImageBtn}>Remove</label>
                            </Col>
                        </Row>
                        {/* <h1 className="title">Primary Contact</h1>
                        <Row className="profile-Row profileMargin">
                            <CBox className="marginLeft8 marginLeft20" checked={this.state.primary_checked} onChange={(e) => this.inputHandleChange(e,'primary_checked')}>
                                <span className="checkmark"></span>
                            </CBox><span className="marginTop10 subtext">Please fill below details if details are not same as above.</span>
                        </Row>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">First Name</label>
                                <FormControl type="text" name="primary_first" disabled={!this.state.primary_checked} className="inputFormControl" value={this.state.primary_first || ''} maxLength="200" placeholder="John" onChange={(e) => this.inputHandleChange(e,'primary_first')}/>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Last Name</label>
                                <FormControl type="text" name="primary_last" disabled={!this.state.primary_checked} className="inputFormControl" maxLength="200" value={this.state.primary_last || ''}  placeholder="Doe" onChange={(e) => this.inputHandleChange(e,'primary_last')}/>
                            </Col>
                        </Row>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Email Address</label>
                                <FormControl type="text" name="primary_email" disabled={!this.state.primary_checked}className="inputFormControl" id="email" value={this.state.primary_email || ''}  placeholder="JohnDoe@gmail.com"  onChange={(e) => this.inputHandleChange(e,'primary_email')}/>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Primary Phone Number</label>
                                <PhoneInput maxLength="14" placeholder="(123) 456-7890" disabled={!this.state.primary_checked} country="US" value={this.state.primary_phone || ''}  onChange={ phone => this.inputHandleChange(phone,'primary_phone') }/>
                            </Col>
                        </Row> */}
                    </form>
                    <div className="error marginTop20">{this.state.profileErrorMsz}</div>
                </div>
                <div className="footer-profile">
                    <Button type="button" className={"fsnetSubmitButton "+ (this.state.isFormValid ? 'btnEnabled' : 'disabled') } disabled={!this.state.isFormValid} onClick={this.saveProfileFn}>Save Changes</Button>
                </div>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>                        
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default profileComponent;


import React, { Component } from 'react';
import '../settings.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Link } from "react-router-dom";
import { Row, Col, Button } from 'react-bootstrap';
import {Constants} from '../../../constants/constants';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { Fsnethttp } from '../../../services/fsnethttp';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import editImage from '../../../images/edit.svg';;

var close = {}
class authenticationPasswordComponent extends Component {

    constructor(props) {
        super(props);
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.handleInputChangeEvent = this.handleInputChangeEvent.bind(this);
        this.resetPasswordFn = this.resetPasswordFn.bind(this);
        this.changeSignPassword = this.changeSignPassword.bind(this);
        
        
        this.state = {
            showModal: false,
            isFormValid: false, 
            passwordValid: false,
            password: '', 
            ppasswordBorder: false, 
            passwordMsz: '',
            cnfrmPasswordValid: false,
            cnfrmPassword: '', 
            cnfrmPasswordBorder: false, 
            cnfrmPasswordMsz: '',
            changePasswordErrorMsz: '',
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            hasSignature:false
        }

        var close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    componentDidMount() {
        this.checkPassword();
    }

    checkPassword = () => {
        this.open();
        this.FsnetUtil.setLeftNavHeight();
        window.scrollTo(0, 0);
        
        this.Fsnethttp.checkSignaturePassword().then(result=>{
            this.close();
            this.setState({
                hasSignature: !result.error
            })
        })
        .catch(error=>{
            this.close();
            this.setState({
                hasSignature: false
            })
        });
    }

    changeSignPassword = () => {
        this.setState({
            hasSignature: false
        })
    }

    closeToast(timed) {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }

    //Onchange event for all input text boxes.
    handleInputChangeEvent = (event,type,errorName) => {
        let dataObj = {}; 
        this.setState({
            changePasswordErrorMsz:''
        })
        switch(type) {
            case type:
                if(event.target.value.trim() === '' || event.target.value === undefined) {
                    this.setState({
                        [type+'Msz']: this.Constants[errorName],
                        [type+'Valid']: false,
                        [type+'Border']: true,
                        [type]:''
                    })
                    dataObj ={
                        [type+'Valid'] :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        [type]: event.target.value.trim(),
                        [type+'Msz']: '',
                        [type+'Valid']: true,
                        [type+'Border']: false,
                    })
                    dataObj ={
                        [type+'Valid'] :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            default:
                // do nothing
        }
    }

    // Update state params values and save button visibility

    updateStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, ()=>{
            this.enableDisableSaveButton();
        });
    }

    // Enable / Disble functionality of save Button

    enableDisableSaveButton = () => {
        let status = (this.state.passwordValid && this.state.cnfrmPasswordValid ) ? true : false;
        this.setState({
            isFormValid : status
        });
    }

    resetPasswordFn = () => {
        let passwordRegex = this.Constants.PASSWORD_REGEX;
       
        if(this.state.password !== this.state.cnfrmPassword) {
            this.setState({
                changePasswordErrorMsz:this.Constants.REQUIRED_PASSWORD_AGAINPASSWORD_SAME
            })
            return true;
        } else if((!passwordRegex.test(this.state.password) || !passwordRegex.test(this.state.cnfrmPassword)) && (this.state.password.trim() !== '' && this.state.cnfrmPassword.trim() !== '')) {
            this.setState({
                changePasswordErrorMsz:this.Constants.PASSWORD_RULE_MESSAGE
            })
        } else {
            this.setState({
                changePasswordErrorMsz:''
            })
            this.open();
            let obj = {signaturePassword:this.state.password};
            
            this.Fsnethttp.signaturePassword(obj).then(result=>{
                this.close();
                this.setState({
                    showToast: true,
                    toastMessage: 'Signature Password has changed successfully.',
                    toastType: 'success',
                    password:'',
                    cnfrmPassword:''
                })
                window.location.reload();
            })
            .catch(error=>{
                this.close();
                if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        changePasswordErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        changePasswordErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });
        }
            
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }
    
    render() {
        return (
            <div className="width100">
                <div className="main-heading"><span className="main-title">Signature Authentication Password</span><Link to="/dashboard" className="cancel-fund">Cancel</Link></div>
                <div className="profileContainer" hidden={this.state.hasSignature}>
                    <div className="subtext">Password must contain 8 or more characters with a mix of letters, numbers & symbols.</div>
                    <Row className="marginTop20">
                        <Col className="width40" lg={6} md={6} sm={6} xs={12}>
                            <label className="input-label">Password</label>
                            <input type="password" name="newPassword" placeholder="Enter Password" value={this.state.password} className={"forgotFormControl inputMarginBottom " + (this.state.passwordBorder ? 'inputError' : '')} onChange={(e) => this.handleInputChangeEvent(e,'password', 'LOGIN_PASSWORD_REQUIRED')} onBlur={(e) => this.handleInputChangeEvent(e,'password', 'LOGIN_PASSWORD_REQUIRED')}/>
                            <span className="error">{this.state.passwordMsz}</span>
                        </Col>
                    </Row>
                    <Row className="marginTop20">
                        <Col className="width40" lg={6} md={6} sm={6} xs={12}>
                            <label className="input-label">Confirm Password</label>
                            <input type="password" name="confirmPassword" placeholder="Enter Confirm Password" value={this.state.cnfrmPassword} className={"forgotFormControl " + (this.state.cnfrmPasswordBorder ? 'inputError' : '')} onChange={(e) => this.handleInputChangeEvent(e,'cnfrmPassword', 'CNFRM_PWD_REQUIRED')} onBlur={(e) => this.handleInputChangeEvent(e,'cnfrmPassword', 'CNFRM_PWD_REQUIRED')} />
                            <span className="error">{this.state.cnfrmPasswordMsz}</span>
                        </Col>
                    </Row>
                    <div className="error marginTop20">{this.state.changePasswordErrorMsz}</div>
                    <Row className="marginTop20">
                        <Button className={"reset-password-btn "+ (this.state.isFormValid ? 'btnEnabled' : 'disabled') } onClick={this.resetPasswordFn}>Submit</Button>
                    </Row>
                    <label className="cancel-btn cancelBtn marginTop20"> <a href="/dashboard">Cancel</a></label>
                </div>
                <div className="profileContainer" hidden={!this.state.hasSignature}>
                    <Row className="marginTop20">
                        <Col className="" lg={6} md={6} sm={6} xs={12} className="marginTop40">
                            <label className="input-label authInline marginRight20">Authentication Password</label>
                            <label className="marginRight20">******************</label>
                            <img src={editImage} className="cursor" onClick={this.changeSignPassword}></img>
                        </Col>
                    </Row>
                </div>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>                        
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default authenticationPasswordComponent;


import React, { Component } from "react";
import {
  Button,
  Modal,
  FormControl,
  Row,
  Col,
  Form,
  ControlLabel
} from "react-bootstrap";
import html2canvas from "html2canvas";
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';

class LpSignComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showModal: false,
            lpsubscriptionTotalObj: this.props.subscriptionData ? this.props.subscriptionData : {},
            showConfirmModal: false,
            showSignatureModal: false,
            signatureName: '',
            signatureFont: "azkia-demo",
            password: "",
            investorType: 'LLC',
            subscriptionDocPdf: null,
            fundAggrementPdf: null,
            subscriptionId: null,
            noAuthenticationPassword: false,
            signaturePasswordMsz: "",
            type: null,
            step: 'showSubscription',
            accountType: this.FsnetUtil.getAdminRole(),
            subscriptionAgreementName: '',
            userName: this.FsnetUtil.userName(),
            title: null,
            disableSigButton: false,
            titleMask: '',
            sideLetterUrl: null
        }


    }

    //Unsuscribe the pubsub
    componentWillUnmount() {

    }

    componentDidMount() {
        this.getSubscriptionDoc(this.state.lpsubscriptionTotalObj.id);
    }

    changeState = (e,step) => {
        this.open();
        this.setState({ step: step },()=>{
          setTimeout(() => {
            this.close();
          }, 1000);
        })
      }
    
      close = () =>{
        this.setState({ showModal: false });
      }
    
      // ProgressLoader : show progress loade
      open = () =>{
        this.setState({ showModal: true });
      }
    
      initHeightForModal = () => {
        setTimeout(() => {
          this.changeSignatureStyles()
        }, 100);
      }
    
      openSignatureModal = (isCapital) => {
        this.setState({ signatureName: this.FsnetUtil.userName() });
        this.setState({ showSignatureModal: true }, () => {
            this.initHeightForModal();
          if (
            navigator.userAgent.indexOf("MSIE") != -1 ||
            !!document.documentMode == true
          ) {
            document.getElementById("pdf-view").style.display = "none";
          }
        });
      };
    
      openConfiramtionModal = () => {
        this.setState({ showConfirmModal: true });
      };
    
      closeSignatureModal = () => {
        if (
          navigator.userAgent.indexOf("MSIE") != -1 ||
          !!document.documentMode == true
        ) {
          document.getElementById("pdf-view").style.display = "block";
        }
        this.setState({
          showSignatureModal: false,
          password: "",
          noAuthenticationPassword: false,
          signaturePasswordMsz: "",
        });
      };
    
      closeConfirmModal = () => {
        this.setState({ showConfirmModal: false });
      };
    
      checkError = () => {
        if(this.state.type === 'subScriptionDoc' && this.state.investorType !== 'Individual') {
          if(this.state.title === '' || this.state.title === null) {
            this.setState({
              signaturePasswordMsz: this.state.investorType === 'LLC' ? 'Please Enter the value for Signatory Title' : "Please Enter the value for Signatory Title"
            })
            return true;
          }
        }
        return false;
      }
    
      convertToImage = () => {
        let value = this.checkError();
        if(!value) {
        this.open();
        html2canvas(document.getElementById("img_val")).then(canvas => {
          let postObj = {
            signaturePic: canvas.toDataURL("image/png"),
            signaturePassword: this.state.password,
            subscriptionId: this.state.subscriptionId,
            date: new Date(),
            timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
            subscriptionFileName:this.state.subscriptionAgreementName
          };
            if(this.state.investorType === 'LLC') {
              postObj['entityTitle']= this.state.title
            } else if(this.state.investorType === 'Trust') {
              postObj['trustTitle']=this.state.title
            }else if(this.state.investorType === 'Individual' && this.state.title) {
              postObj['legalTitle']=this.state.title
            }
          if (this.state.type === "capitalCommitment") {
            postObj[
              "documentId"
            ] = this.FsnetUtil.getEnvelopeIdForCapitalCommitment();
          }
          if (this.state.type === "sideLetter") {
            postObj[
              "documentId"
            ] = this.FsnetUtil.getEnvelopeIdForSideLetter();
          }
          this.Fsnethttp
            .signatureAppendAPI(this.state.type, postObj)
            .then(result => {
              this.close();
              this.closeSignatureModal();
            })
            .catch(error => {
              this.close();
              if (
                error.response !== undefined &&
                error.response.data !== undefined &&
                error.response.data.errors !== undefined &&
                error.response.data.errors[0].code === "SET_PASSWORD"
              ) {
                this.setState({
                  noAuthenticationPassword: true
                });
              } else if (
                error.response !== undefined &&
                error.response.data !== undefined &&
                error.response.data.errors !== undefined
              ) {
                this.setState({
                  signaturePasswordMsz: error.response.data.errors[0].msg
                });
              }
            });
        });
        }
      };
    
      changeSignatureStyles = () => {
        if (this.state.userName && this.state.userName.length > 25) {
          document.getElementById('img_val').style.lineHeight = '40px';
        } else {
          document.getElementById('img_val').style.lineHeight = 'initial';
          document.getElementById('img_val').style.height = 'initial';
        }
      }
    
      onChange = field => event => {
        this.setState({ [field]: event.target.value, signaturePasswordMsz:'',disableSigButton: event.target.value.trim() === '' && field == 'title' ? true :false },()=>{
          if(field !== 'title') {
            this.initHeightForModal();
          } 
        });
      };

    getSubscriptionDoc = (id) => {
        if(id) {
            this.open();
            const httpUrl = 'currentSignatureView';//url.indexOf('subscriptionDocumentPdf') > -1 ? 'currentSignatureView': 'signatureView'
            this.Fsnethttp.getSubscriptionAgrement(id, httpUrl).then(result => {
                if (result.data) {
                    this.setState(
                        {
                            subscriptionDocPdf: result.data.subscriptionAgreementUrl + '?token=' + this.FsnetUtil.getToken(),
                            fundAggrementPdf: result.data.fundAgreementUrl + '?token=' + this.FsnetUtil.getToken(),
                            sideLetterUrl: result.data.sideLetterUrl ? result.data.sideLetterUrl + '?token=' + this.FsnetUtil.getToken() : null,
                            subscriptionId: id,
                            subscriptionAgreementName: result.data.subscription ? result.data.subscription.subscriptionAgreementName : '',
                            investorType: result.data.subscription ? result.data.subscription.investorType : 'LLC',
                            signatureName: result.data.subscription ? result.data.subscription.dynamicTitle : this.FsnetUtil.userName(),
                            title: result.data.subscription.investorType === 'LLC' ? result.data.subscription.dynamicTitle : result.data.subscription.investorType === 'Trust' ? result.data.subscription.dynamicTitle ? result.data.subscription.dynamicTitle : 'Trustee' : result.data.subscription.dynamicTitle ? result.data.subscription.dynamicTitle : '',
                            titleMask: result.data.subscription.investorType === 'LLC' ? result.data.subscription.dynamicTitle : result.data.subscription.investorType === 'Trust' ? result.data.subscription.dynamicTitle ? result.data.subscription.dynamicTitle : 'Trustee' : result.data.subscription.dynamicTitle ? result.data.subscription.dynamicTitle : ''
                        }
                    );
                }
                setTimeout(() => {
                    this.close();
                }, 5000);
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    render() {
        return (
            <div className="accreditedInvestor width100 marginTopSticky" id="subscriptionReview">
                <div className="step6ClassAboveFooter">
                <div className="pdf">
              <h1 className="title pdf-title" hidden={this.state.step !== 'default'}>
                Your Subscription Agreement has been completed. Please review the
                document below. Once you are satisfied, please click "Confirm and Sign"
                to append your signature.
            </h1>
              <h1 className="title pdf-title" hidden={this.state.step !== 'showSubscription'}>
                Please review and approve your Subscription Agreement.
            </h1>
            <h1 className="title pdf-title" hidden={this.state.step !== 'showFundAgrement'}>
                Please review and approve the Fund Agreement.
            </h1>
            <h1 className="title pdf-title" hidden={this.state.step !== 'showSideLetter'}>
                Please review and approve the Side Letter Agreement.
            </h1>
              <Button type="button" className="fsnetSubmitButton btnEnabled fontFamilyBold" hidden={this.state.step !== 'default'} onClick={(e) => this.changeState(e, 'showSubscription')}>
                Confirm and Sign
            </Button>
              
            {
              this.state.sideLetterUrl ? 
                <div>
                  <Button type="button" className="fsnetSubmitButton btnEnabled fontFamilyBold subapprovedBtn" hidden={this.state.step !== 'showSubscription'} onClick={(e) => this.changeState(e, 'showFundAgrement')}>
                      Approved. Continue to Review Fund Agreement
                  </Button>
                  <Button type="button" className="fsnetSubmitButton fontFamilyBold subapprovedBtn btnEnabled" hidden={this.state.step !== 'showFundAgrement'} onClick={(e) => this.changeState(e, 'showSideLetter')}>
                    Approved. Continue to Review Side Letter
                  </Button>
                  <Button type="button" className={"fsnetSubmitButton fontFamilyBold fundapprovedBtn " + (this.state.accountType === 'FSNETAdministrator' ? 'disabled':'btnEnabled')} hidden={this.state.step !== 'showSideLetter'} onClick={() => this.openSignatureModal(false)}>
                    Approved. Apply Electronic Signature to All Reviewed Documents
                  </Button>
                </div>
                :
                <div>
                  <Button type="button" className="fsnetSubmitButton btnEnabled fontFamilyBold subapprovedBtn" hidden={this.state.step !== 'showSubscription'} onClick={(e) => this.changeState(e, 'showFundAgrement')}>
                      Approved. Continue to Review Fund Agreement
                  </Button>
                  <Button type="button" className={"fsnetSubmitButton fontFamilyBold fundapprovedBtn " + (this.state.accountType === 'FSNETAdministrator' ? 'disabled':'btnEnabled')} hidden={this.state.step !== 'showFundAgrement'} onClick={() => this.openSignatureModal(false)}>
                    Approved. Apply Electronic Signature to All Reviewed Documents
                  </Button>
                </div>

            }
              {
                this.state.step == 'showSubscription' ?
                  <div className="iframe-div">
                    <iframe className="pdf-frame" src={this.state.subscriptionDocPdf} scrolling="no" frameBorder="0" onLoad={this.checkIframeLoaded}/>
                  </div>
                :
                this.state.step == 'showSideLetter' ?
                  <div className="iframe-div">
                    <iframe className="pdf-frame" src={this.state.sideLetterUrl} scrolling="no" frameBorder="0" id="pdf-view" />
                  </div>
                :
                <div className="iframe-div">
                  <iframe className="pdf-frame" src={this.state.fundAggrementPdf} scrolling="no" frameBorder="0" id="pdf-view" />
                </div>
              }
            </div>
                </div>
                <Modal
                    backdrop="static" className={!this.state.noAuthenticationPassword ? "generateSignature" : "passwordModal"} show={this.state.showSignatureModal} onHide={this.closeSignatureModal}>
                    <Modal.Header closeButton />
                    <Modal.Body hidden={this.state.noAuthenticationPassword}>
                        <label className="label-text marginBot12 width100 fontFamilyBoldSize">
                            Configure and Append Signature
            </label>
                        <Row className="marginEdit">
                            <Col lg={12} md={12} sm={12} xs={12} className="width40 paddingZero">
                                <label className="input-label">Signature Fonts</label>
                                <FormControl name="signatureFont" className="selectFormControl" componentClass="select" value={this.state.signatureFont} onChange={this.onChange("signatureFont")}>
                                    <option value="azkia-demo">Symphony</option>
                                    <option value="adine-kirnberg">Affinity</option>
                                    <option value="400 30px akadora">Caviar</option>
                                    <option value="400 30px 'Great Vibes', Helvetica, sans-serif">
                                        Great Vibes
                  </option>
                                </FormControl>
                            </Col>
                        </Row>
                        {this.state.signatureFont === "adine-kirnberg" ||
                            this.state.signatureFont === "azkia-demo" ? (
                                <div id="img_val" style={{ fontFamily: this.state.signatureFont }}>
                                    {this.FsnetUtil.userName()}
                                </div>
                            ) : (
                                <div id="img_val" style={{ font: this.state.signatureFont }}>
                                    {this.FsnetUtil.userName()}
                                </div>
                            )}
                        <Form autoComplete="off">

                            {
                                this.state.type === 'subScriptionDoc' &&
                                <div>
                                    {
                                        (this.state.investorType == 'LLC' || this.state.investorType == 'Trust') &&
                                        <ControlLabel className="block marginTop10">{this.state.investorType == 'LLC' ? 'Enter your Signatory Title within the Entity' : 'Enter your Signatory Title within the Trust'}</ControlLabel>
                                    }
                                    {
                                        (this.state.investorType === 'Individual' && this.state.titleMask !== '') &&
                                        <ControlLabel className="block marginTop10">Enter your Signatory Title within the Joint Individual</ControlLabel>
                                    }
                                    <FormControl type="text" name="title" className="inputFormControl  marginBot20" placeholder={this.state.investorType == 'LLC' ? 'Director' : 'Trustee'} autoComplete="off" value={this.state.title} hidden={this.state.titleMask === ''} onChange={this.onChange("title")} />
                                </div>
                            }
                            <ControlLabel className="marginTop10 block">Please re-enter your password </ControlLabel>
                            <ControlLabel className="marginTop10 font14">{this.Constants.PASSWORD_MESSAGE}</ControlLabel>
                            <FormControl type="password" name="signaturePassword" className="inputFormControl  marginBot10" autoComplete="new-password" value={this.state.password} placeholder="Enter password" onChange={e => this.setState({ password: e.target.value.trim() })} />
                            <div className="error marginBot10 height20">
                                {this.state.signaturePasswordMsz}
                            </div>
                            <Button type="button" className="fsnetSubmitButton btnEnabled uploadBtn fontFamilyBold" onClick={this.convertToImage} disabled={this.state.password === "" || this.state.disableSigButton}>
                                {" "}
                                Add Signature{" "}
                            </Button>
                        </Form>
                    </Modal.Body>
                    <Modal.Body hidden={!this.state.noAuthenticationPassword}>
                        <div className="title">
                            Please add authentication password using this{" "}
                            <a href="/settings/authentication-password">link</a>.
            </div>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
            </div>
        );
    }
}

export default LpSignComponent;


import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Radio, Row, Col, FormControl, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';

class capitalCommitmentComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.onChangeEvent = this.onChangeEvent.bind(this);
        this.proceedToNext = this.proceedToNext.bind(this);
        this.proceedToBack = this.proceedToBack.bind(this);
        this.state = {
            showModal: false,
            investorType: 'LLC',
            investorSubType: 0,
            lpCapitalCommitment: '',
            showTooltipModal: false,
            showSecurityTooltipModal:false,
            jsonData:{},
            isFormChanged:false
        }

    }


    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }


    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.getJsonData();
        this.getSubscriptionDetails(id);
    }

    getJsonData() {
        this.Fsnethttp.getJson('capitalCommitment').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getSubscriptionDetails(id) {
        
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    let obj = result.data.data;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    if(obj.investorQuestions && obj.investorQuestions.length > 0) {
                        obj['currentPageCount'] = result.data.data.investorType == 'Trust' ? 8 : (result.data.data.investorType == 'LLC' ? 8 : 5);
                    } else {
                        obj['currentPageCount'] = result.data.data.investorType == 'Trust' ? 7 : (result.data.data.investorType == 'LLC' ? 7 : 4);
                    }
                    obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                    PubSub.publish('investorData', obj);
                    this.setState({
                        getInvestorObj: result.data.data,
                        investorType: result.data.data.investorType?result.data.data.investorType:'LLC',
                        investorSubType: result.data.data.investorSubType ? result.data.data.investorSubType : (result.data.data.investorType == 'Trust' ? 9 : 0),
                        lpCapitalCommitment: result.data.data.lpCapitalCommitment,
                        lpCapitalCommitmentStr: result.data.data.lpCapitalCommitment && result.data.data.lpCapitalCommitment !=0 ? this.FsnetUtil.convertToCurrency(result.data.data.lpCapitalCommitment) : ''
                    }, () => {
                        //console.log('jdfjhsd');
                        // this.updateInvestorInputFields(this.state.getInvestorObj)
                    })
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    proceedToNext() {
        let postobj = { investorType: this.state.investorType, investorSubType: this.state.investorSubType, subscriptionId: this.state.getInvestorObj.id, step: this.state.investorType == 'Trust' || this.state.investorType == 'LLC' ? 8 : 5, lpCapitalCommitment: this.state.lpCapitalCommitment.toString(),isFormChanged:this.state.isFormChanged }
        this.open();
        
        this.Fsnethttp.updateLpSubscriptionDetails(postobj).then(result => {
            this.close();
            if (result.data) {
                const url = this.FsnetUtil.getUserRole() === 'LP' ? '/lp/lpSignatory/' : '/lp/review/'
                this.props.history.push(url + this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
            }
        })
        .catch(error => {
            this.close();
            if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                this.setState({
                    accreditedInvestorErrorMsz: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    accreditedInvestorErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                });
            }
        });

    }

    proceedToBack() {
        //console.log('id:::', this.state.getInvestorObj);
        if(this.state.getInvestorObj.investorQuestions.length > 0) {
            this.props.history.push('/lp/additionalQuestions/'+this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
        } else if(this.state.investorType == 'LLC' || this.state.investorType == 'Trust') {
            this.props.history.push('/lp/erisa/'+this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
        } else {
            this.props.history.push('/lp/qualifiedPurchaser/'+this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
        }
    }

    numbersOnly(event) {
        if (event.which !=46 && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    }

    formChange = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name] : value
        })
    }

    formBlur  = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name] : value ? this.FsnetUtil.convertToCurrencyTwodecimals(value):null,
            lpCapitalCommitment:value
        })
    }

    formFocus = (e) => {
        let name = e.target.name;
        this.setState({
            [name] : this.state.lpCapitalCommitment && this.state.lpCapitalCommitment != 0 ? this.state.lpCapitalCommitment : null,
        })
    }
    

    onChangeEvent(e) {
        let name = e.target.name;
        let value = e.target.value;
        const re = this.Constants.NUMBER_REGEX;
        if (re.test(value.trim())) {
            this.setState({
                [name]: value ? value : '',
                lpCapitalCommitment: value && value != 0 ? value : '',
                [name + 'Valid']: true,
                [name + 'Touched']: true,
                isFormChanged:true
            })
            return true;
        } else {
            if (parseInt(value) < 0 || value === '') {
                this.setState({
                    [name]: value ? value : '',
                    lpCapitalCommitment: value && value != 0 ? value : '',
                    [name + 'Valid']: false,
                    [name + 'Touched']: true
                })
                return true;
            }
        }
        // this.setState({
        //     [name]: value
        // })
        // return true;
    }

    valueTouched(e) {
        let name = e.target.name;
        let capitalValue = e.target.value ? Number(e.target.value.replace(/[^0-9.-]+/g,"")) : '';
        let value = capitalValue ? this.FsnetUtil.convertToCurrency(capitalValue) : '';
        let intValue = value ? Number(value.replace(/[^0-9.-]+/g,"")) : '';
        this.setState({
            [name]: value,
            lpCapitalCommitment: intValue,
            [name+'Touched']: true
        })
    }

    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                    trigger={['click', 'hover', 'focus']}
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement="left"
                    delayShow={300}
                    delayHide={150}
                >
                    <span>{children}</span>
                </OverlayTrigger>
            );
        }
        return (
            <div className="accreditedInvestor width100 marginTopSticky">
                <div className="formGridDivMargins min-height-400">
                    {/* individual investor type block starts */}
                    <div className="title">{this.state.jsonData.CAPITAL_COMMITMENT}</div>
                    <Row className="step1Form-row">
                        <Col xs={12} md={12}>
                            <label className="form-label width100 erisa-heading">{this.state.jsonData.CAPITAL_COMMITMENT_OFFERING}</label>
                        </Col>
                        <Col xs={12} md={12}>
                            <FormControl type="text" name='lpCapitalCommitmentStr' autoComplete="off" placeholder={this.state.jsonData.ENTER_CAPITAL_AMOUNT_PLACEHOLDER} value={this.state.lpCapitalCommitmentStr} className={"inputFormControl inputWidth290 " + (this.state.lpCapitalCommitmentTouched && !this.state.lpCapitalCommitment ? 'inputError' : '')} onChange={(e) => {this.onChangeEvent(e)}} onKeyPress={(e) => {this.numbersOnly(e)}} onBlur={(e) => {this.formBlur(e)}} onFocus={(e) => {this.formFocus(e)}}  />
                            {this.state.lpCapitalCommitmentTouched && !this.state.lpCapitalCommitment ? <span className="error">{this.Constants.TRUST_NAME_REQUIRED}</span> : null}
                        </Col>
                    </Row>
                    {/* individual investor type block ends */}
                </div>
                <div className="margin30 error">{this.state.accreditedInvestorErrorMsz}</div>
                <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.getInvestorObj && this.state.getInvestorObj.subscriptionStatus && this.state.getInvestorObj.subscriptionStatus.name === 'Closed'}>
                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className={"fa fa-chevron-right " + (!this.state.lpCapitalCommitment ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default capitalCommitmentComponent;


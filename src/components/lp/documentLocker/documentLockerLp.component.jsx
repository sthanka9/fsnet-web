import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Row, Checkbox as CBox, Button } from 'react-bootstrap';
import { Constants } from '../../../constants/constants';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';

class documentLockerLpComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            fundId: null,
            showModal: false,
            selectedDocs: [],
            lpDocumentLockerList: [],
            showDocumentLocker: false,
        }

    }


    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }


    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.setState({
            fundId: id
        })
        window.scrollTo(0, 0);
        this.getLpDocumentLocker(id);
    }

    //Get list of Addendums
    getLpDocumentLocker(id) {
        if (id) {
            this.open();
            
            this.Fsnethttp.getLpDocumentLocker(id).then(result => {
                this.close();
                PubSub.publish('leftNavPage', 'documentLockerLp');
                this.setState({
                    lpDocumentLockerList: result.data.data
                })
                //this.checkTypeForallLps(result.data.data.lpDocuments)
            })
                .catch(error => {
                    this.close();
                    this.setState({
                        lpDocumentLockerList: [],
                    })

                });
        }
    }

    checkTypeForallLps(obj) {
        if (obj && obj.length > 0) {
            for (let index of obj) {
                if (index.documentsForSignature.length > 0 || index.commitmentHistory) {
                    this.setState({
                        showDocumentLocker: true
                    })
                    return true
                }
            }
        }
        this.setState({
            showDocumentLocker: false
        })
    }

    openDoc = (data) => () => {
        const docUrl = `${data.documentUrl}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    selectDocuments = (isAll, obj) => (e) => {
        let selectedDocs = this.state.selectedDocs;
        let idx = selectedDocs.filter(x => x.id === obj.id );
        let findIndex = selectedDocs.findIndex(x => x.id === obj.id );
        if(isAll) {
            this.setState({ 
                includeAllFile: e.target.checked 
            });
        } else {
            if(e.target.checked && !isAll) {
                if(idx.length === 0) {
                    selectedDocs.push(obj);
                } else {
                    selectedDocs.splice(findIndex, 1);
                }
            } else {
                selectedDocs.splice(findIndex, 1);
            }
        }
        this.setState({
            selectedDocs
        });
    }

    downloadDocuments = () => {
        this.open();
        let postObj = {
            fileIds: this.state.selectedDocs.map((k) => {return {id:k.id,name:k.name}}),
            subscriptionId: this.state.lpDocumentLockerList.length ? this.state.lpDocumentLockerList[0].subscriptionId : 0,
            fundId: null
        }
        this.Fsnethttp.downloadFiles(postObj).then(res => {
            this.close();
            if (res.data && res.data.url) {
                const docUrl = `${res.data.url}`
                this.FsnetUtil._openNewTab(docUrl)
            }
        }).catch(error => {
            this.close();
        })
    }

    getDocumentName = (data) => {
        let name = '';
        if(data) {
            let type = data.documentType;
            name = (['FUND_AGREEMENT','CONSOLIDATED_FUND_AGREEMENT','CONSOLIDATED_AMENDMENT_AGREEMENT','AMENDMENT_AGREEMENT'].indexOf(type) > -1 && data.name) ? data.name : type === 'SUBSCRIPTION_AGREEMENT' ? 'Subscription Agreement': type === 'CHANGE_COMMITMENT_AGREEMENT' ? 'Capital Commitment Increase Letter' : type === 'SIDE_LETTER_AGREEMENT' ? 'Side Letter' : type;
        }
        return name;
    }


    render() {

        return (
            <div className="width100 form-main-div documentLockerContainer marginTopStickyDocLocker">
                <div className="staticContent lockerTitle">
                    <h2 className="title marginBottom2 marginLeft30 marginRight20 marginTop20">Document Locker</h2>
                    <Button className="newAddendumButton download-btn" disabled={!this.state.selectedDocs.length} onClick={this.downloadDocuments}>
                        <i className="fa fa-download paddingRight5"></i>Download Selected Docs
                        </Button>
                </div>
                <Row className="full-width marginTop20 marginLeft61" hidden={this.state.lpDocumentLockerList.length === 0}>
                    <div className="name-heading documentLockerDateCol">
                        Date
                    </div>
                    <div className="name-heading documentLockerTypeColWidth">
                        Type
                    </div>
                    <div className="name-heading">
                        Download
                    </div>
                </Row>
                <div className={"documentLockerLp documentLockerLpTableWidth " + (this.state.lpDocumentLockerList.length === 0 ? 'borderNone' : 'marginTop30')}>
                    {this.state.lpDocumentLockerList.length > 0 ?
                        this.state.lpDocumentLockerList.map((record, index) => {
                            return (
                                <div className="userRow" key={index}>
                                    <div className="documentLockerCol lp-name documentLockerDateCol">{record['createdAt']}
                                        {
                                            (record['createdAt'] && record['timezone']) ?
                                                <span> ({record['timezone']})</span> : ''
                                        }
                                    </div>
                                    <div className="documentLockerCol lp-name documentLockerTypeCol cursor" onClick={this.openDoc(record)}>{this.getDocumentName(record)} </div>
                                    <div style={{ 'display': 'inline-block', 'padding': '0 35px', width: '65px !important' }}>
                                        <CBox checked={record['selected']} onChange={this.selectDocuments(false, record)} style={{ 'marginRight': '0px' }}>
                                            <span className="checkmark"></span>
                                        </CBox>
                                    </div>
                                </div>
                            );
                        })
                        :
                        <div className="title margin20 text-center">{this.Constants.NO_DOCUMENTLOCKER_DOCUMENTS}</div>
                    }
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default documentLockerLpComponent;


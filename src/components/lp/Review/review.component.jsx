import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Row, Col, Button, Modal } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { Route, Link } from "react-router-dom";
import { PubSub } from 'pubsub-js';
import { reactLocalStorage } from 'reactjs-localstorage';
import ToastComponent from '../../toast/toast.component';

var win, subSignwin, patnershipSignwin, signedWin, totalPagesCompleted = {}, subScriptionwin = {}, close = {};
class reviewComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.proceedToBack = this.proceedToBack.bind(this);
        this.sendForSignature = this.sendForSignature.bind(this);
        this.viewSignedDoc = this.viewSignedDoc.bind(this);
        this.handledocModalClose = this.handledocModalClose.bind(this);
        this.handledocModalShow = this.handledocModalShow.bind(this);
        this.handlealertModalClose = this.handlealertModalClose.bind(this);
        this.handlealertModalShow = this.handlealertModalShow.bind(this);
        this.closeConfirmModal = this.closeConfirmModal.bind(this);
        this.closeSignatureModal = this.closeSignatureModal.bind(this);
        this.viewSubscriptionDoc = this.viewSubscriptionDoc.bind(this);
        this.closeLPDelegateInvestorModal = this.closeLPDelegateInvestorModal.bind(this);
        this.sendDocuments = this.sendDocuments.bind(this);
        this.redirectToDashbord = this.redirectToDashbord.bind(this);
        this.state = {
            showModal: false,
            docModal: false,
            investorType: 'LLC',
            lpObj: {},
            investorSubTypeName: '',
            jurisdictionEntityLegallyRegisteredName: '',
            alertModal: false,
            jsonData: {},
            enableConfirmAndSubmit: false,
            loggedInUserObj: {},
            showConfirmModal: false,
            showSignatureModal: false,
            subscriptionSignedDocUrl: null,
            patnershipDocUrl: null,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            showLPDelegateInvestorModal: false,
            countriesList: [],
            usStatesList: [],
            statesByCountryList: [],
            showNumberOfBeneficialOwners: false,
            isPendingAction:false,
            subscriptionStatus:{}
        }

        //Get the data from other steps
        totalPagesCompleted = PubSub.subscribe('totalPagesCompleted', (msg, data) => {
            this.disableConfrimAndSubmitBtn(data.count)
        });

        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    disableConfrimAndSubmitBtn(count) {
        if (this.state.lpObj.investorType === 'Individual' && (count === 4 || count === 5)) {
            this.setState({
                enableConfirmAndSubmit: true
            }, () => {
                this.checkAdditionalQuestions()
            })
        } else if (this.state.lpObj.investorType === 'LLC' && (count === 7 || count === 8)) {
            this.setState({
                enableConfirmAndSubmit: true
            }, () => {
                this.checkAdditionalQuestions()
            })
        } else if (this.state.lpObj.investorType === 'Trust' && (count === 6 || count === 7)) {
            this.setState({
                enableConfirmAndSubmit: true
            }, () => {
                this.checkAdditionalQuestions()
            })
        } else {
            this.setState({
                enableConfirmAndSubmit: false
            })
        }
    }

    checkAdditionalQuestions() {
        let questionsList = this.state.lpObj.investorQuestions;
        if (questionsList && questionsList.length > 0) {
            for (let index of questionsList) {
                if (index['isRequired'] && (index['questionResponse'] === null || index['questionResponse'] === '' || index['questionResponse'] === undefined)) {
                    this.setState({
                        enableConfirmAndSubmit: false
                    })
                    break;
                } else {
                    this.setState({
                        enableConfirmAndSubmit: true
                    })
                }
            }
        }
    }

    closeToast(timed) {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(totalPagesCompleted);
        PubSub.unsubscribe(close);
    }

    openSubscriptionDocModal() {
        this.setState({ showConfirmModal: true })
    }

    closeConfirmModal() {
        this.setState({ showConfirmModal: false });
        // this.getSubscriptionDetails(this.state.lpObj.id,'url');
        window.location.href = '/dashboard'
    };

    closeSignatureModal() {
        this.setState({ showSignatureModal: false })
    }

    openLPDelegateInvestorModal() {
        this.setState({ showLPDelegateInvestorModal: true })
    }

    closeLPDelegateInvestorModal() {
        this.setState({ showLPDelegateInvestorModal: false })
    }

    openSignatureModal() {
        this.setState({ showSignatureModal: true })
    }

    viewSubscriptionDoc() {
        this.open();

        this.Fsnethttp.viewSignedDoc(this.state.lpObj.id).then(result => {
            this.close();
            const docUrl = `${result.data.url}?token=${this.FsnetUtil.getToken()}`
            this.FsnetUtil._openDoc(docUrl)
        })
            .catch(error => {
                this.close();
            });
    }

    sendDocuments() {
        if (this.state.lpObj.id) {
            this.open();

            const postObj = { subscriptionId: this.state.lpObj.id }
            this.Fsnethttp.sendDocuments(postObj).then(result => {
                this.close();
                this.setState({ showConfirmModal: false });
                window.location.reload();
            })
                .catch(error => {
                    this.close();
                    this.setState({ showConfirmModal: false });
                });
        }
    }

    redirectToDashbord() {
        this.props.history.push('/dashboard');
    }

    handledocModalClose() {
        this.setState({
            docModal: false
        })
    }
    handledocModalShow() {
        this.setState({
            docModal: true
        })
    }
    handlealertModalClose() {
        this.setState({
            alertModal: false
        })
    }
    handlealertModalShow() {
        this.setState({
            alertModal: true
        })
    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        let userObj = reactLocalStorage.getObject('userData');
        let urlValues = this.FsnetUtil.decodeUrl();
        this.setState({
            subscriptionId: id,
            loggedInUserObj: userObj,
            isPendingAction: urlValues.isPendingAction ? true : false
        })
        this.getJsonData();
        this.getAllCountries();
        this.getUSStates();
        this.getSubscriptionDetails(id);
    }

    getJsonData() {
        this.Fsnethttp.getJson('reviewConfirm').then(result => {
            if (result && result.data) {
                this.setState({
                    jsonData: result.data
                })
            }
        });

    }


    getSubscriptionDetails(id, type) {

        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    if (type === 'url') {
                        PubSub.publish('investorData', result.data.data); //Call this to show icon in actions required
                    }
                    this.setState({
                        investorType: result.data.data.investorType ? result.data.data.investorType : 'LLC',
                    }, () => {
                        if (result.data.data.mailingAddressCountry) {
                            this.getStateNameByCountry(result.data.data.mailingAddressCountry)
                        }
                    })
                    if (result.data.data.investorType === 'Individual') {
                        this.updateIndividualData(this.FsnetUtil.decodeObj(result.data.data));
                    } else if (result.data.data.investorType === 'LLC') {
                        this.updateLLCData(this.FsnetUtil.decodeObj(result.data.data));
                    } else if (result.data.data.investorType === 'Trust') {
                        this.updateTrustData(this.FsnetUtil.decodeObj(result.data.data));
                    } else {
                        this.setState({
                            lpObj:result.data.data
                        })
                    }
                    this.showNumberOfBeneficialOwners(result.data.data);
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }


    showNumberOfBeneficialOwners(obj) {
        let fileds = ['entityProposingAcquiringInvestment', 'entityHasMadeInvestmentsPriorToThedate', 'partnershipWillNotConstituteMoreThanFortyPercent', 'beneficialInvestmentMadeByTheEntity'];
        for (let index of fileds) {
            if (obj[index] === false && obj.companiesAct == 1) {
                this.setState({
                    showNumberOfBeneficialOwners: true
                })
            }
        }
    }

    updateIndividualData(data) {
        let obj = data;
        obj['currentInvestorInfoPageNumber'] = 1;
        obj['currentPageCount'] = data.investorQuestions && data.investorQuestions.length > 0 ? 5 : 4;
        obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
        PubSub.publish('investorData', obj);
        this.setState({
            lpObj: data,
            subscriptionStatus:data.subscriptionStatus
        })
    }

    updateLLCData(data) {
        let obj = data;
        obj['currentInvestorInfoPageNumber'] = 1;
        obj['currentPageCount'] = data.investorQuestions && data.investorQuestions.length > 0 ? 8 : 7;
        obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
        PubSub.publish('investorData', obj);
        this.setState({
            lpObj: data,
            subscriptionStatus:data.subscriptionStatus
        }, () => {
            if (this.state.lpObj.otherInvestorSubType !== null && this.state.lpObj.otherInvestorSubType !== undefined) {
            } else {
                this.investorSubTypes();
            }
        })
    }

    updateTrustData(data) {
        let obj = data;
        obj['currentInvestorInfoPageNumber'] = 1;
        obj['currentPageCount'] = data.investorQuestions && data.investorQuestions.length > 0 ? 10 : 9;
        obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
        PubSub.publish('investorData', obj);
        this.setState({
            lpObj: data,
            subscriptionStatus:data.subscriptionStatus
        }, () => {
            this.investorTrustSubTypes();
        })
    }

    proceedToBack() {
        if (this.state.lpObj.investorType === 'Individual') {
            this.props.history.push('/lp/qualifiedPurchaser/' + this.state.lpObj.id);
        } else if (this.state.lpObj.investorType === 'LLC') {
            this.props.history.push('/lp/erisa/' + this.state.lpObj.id);
        }
    }

    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    //Call investor sub types
    investorSubTypes() {

        this.open();
        this.Fsnethttp.getInvestorSubTypes().then(result => {
            this.close();
            let id = parseInt(this.state.lpObj.investorSubType);
            if (result.data) {
                for (let index of result.data) {
                    if (index['id'] === id) {
                        this.setState({
                            investorSubTypeName: index['name']
                        })
                    }
                }
            }
        })
            .catch(error => {
                this.close();
            });
    }

    investorTrustSubTypes() {

        this.open();
        this.Fsnethttp.getInvestorTrustSubTypes().then(result => {
            this.close();
            let id = parseInt(this.state.lpObj.investorSubType);
            if (result.data) {
                for (let index of result.data) {
                    if (index['id'] === id) {
                        this.setState({
                            investorSubTypeName: index['name']
                        })
                    }
                }
            }
        })
            .catch(error => {
                this.close();
            });
    }

    //Call investor sub types for trust
    getAllCountries() {

        this.open();
        this.Fsnethttp.getAllCountries().then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    countriesList: result.data
                })
            }
        })
            .catch(error => {
                this.close();
            });
    }

    getCountryName(id) {
        let getName = this.state.countriesList.filter(data => data.id == id);
        return getName[0] ? getName[0].name : null
    }

    getStateName(id) {
        let getName = this.state.usStatesList.filter(data => data.id == id);
        return getName[0] ? getName[0].name : null
    }

    getStateByCountry(id) {
        let getName = this.state.statesByCountryList.filter(data => data.id == id);
        return getName[0] ? getName[0].name : null
    }

    getStateNameByCountry(country) {
        this.open();
        this.Fsnethttp.getStates(country).then(result => {
            this.close();
            this.setState({
                statesByCountryList: result.data
            })
        })
            .catch(error => {
                this.close();
            });
    }

    //Call investor sub types for trust
    getUSStates() {
        this.open();
        this.Fsnethttp.getUSStates().then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    usStatesList: result.data
                })
            }
        })
            .catch(error => {
                this.close();
            });
    }


    checkIsJointIndividual = () => {
        let isJointIndividual = false;
        if(this.state.lpObj.investorType === 'Individual' && this.state.lpObj.areYouSubscribingAsJointIndividual && this.state.lpObj.noOfSignaturesRequired < 1) {
            isJointIndividual = true;
        }
        return isJointIndividual;
    }

    submitSubscription = (proceed) => () => {
        let isJointIndividual = this.checkIsJointIndividual();
        if(isJointIndividual && !proceed) {
            this.openjointIndividualModal();
            return;
        }
        if (this.state.lpObj.isCurrentUserSigned && !proceed) {
            this.handledocModalShow()
        } else {
            this.submitToSign(false)();
        }

    }

    submitToSign = (value) => () => {
        this.setState({
            enableConfirmAndSubmit: false
        })
        this.handledocModalClose();
        this.closejointIndividualModal();
        var $this = this;
        window.scrollTo(0, 0);
        if (value) {
            const url = `/signDocuments?sId=${this.FsnetUtil._encrypt(this.this.state.lpObj.id)}&type=SL&documentId=${this.FsnetUtil._encrypt(this.state.lpObj.sideletterAgreement.id)}${this.FsnetUtil._isIEEdge() ? '&token='+this.FsnetUtil.getToken():''}`
            subScriptionwin = window.open(url, '_blank', 'width = 1000px, height = 800px');
        } else {
            const url = `/signDocuments?sId=${this.FsnetUtil._encrypt(this.state.lpObj.id)}&type=SA${this.FsnetUtil._isIEEdge() ? '&token='+this.FsnetUtil.getToken():''}`
            subScriptionwin = window.open(url, '_blank', 'width = 1000px, height = 800px');
            var timer = setInterval(function () {
                if (subScriptionwin && subScriptionwin.closed) {
                    clearInterval(timer);
                    $this.setState({
                        enableConfirmAndSubmit: true
                    }, () => {
                        const obj = reactLocalStorage.get('signedDoc') ? JSON.parse(reactLocalStorage.get('signedDoc')) : '';
                        if (obj && obj.type === 'success' ) {
                            localStorage.removeItem('signedDoc');
                            $this.openSubscriptionDocModal();
                        } else if($this.FsnetUtil._isIEEdge()) {
                            window.location.href = '/dashboard';
                        }
                    })
                }
            }, 1000);
        }
    }

    //LP Delegate send for signature
    sendForSignature() {

        let obj = { fundId: this.state.lpObj.fundId, subscriptionId: this.state.lpObj.id }
        this.open();
        this.Fsnethttp.sendForSignatureByDelegate(obj).then(result => {
            this.close();
            this.openLPDelegateInvestorModal();
        })
            .catch(error => {
                this.close();
            });
    }

    subscriptionSubmiToDocusign() {

        this.open();
        this.Fsnethttp.submitSubscription(this.state.subscriptionId).then(result => {
            this.close();
            if (result.data) {
                this.handledocModalClose()
                win = window.open(result.data.url+'?token=' + JSON.parse(reactLocalStorage.get('token')), '_blank', 'width = 1000px, height = 600px')
                var timer = setInterval(function () {
                    if (win && win.closed) {
                        clearInterval(timer);
                        window.location.reload()
                    }
                }, 1000);
            }
        })
            .catch(error => {
                this.close();
            });
    }

    viewSignedDoc(e, type) {
        if (type === 'modal' && this.state.subscriptionSignedDocUrl) {
            subSignwin = window.open(this.state.subscriptionSignedDocUrl+'?token=' + JSON.parse(reactLocalStorage.get('token')), '_blank', 'width = 1000px, height = 600px')
            var $this = this;
            var timer = setInterval(function () {
                if (subSignwin && subSignwin.closed) {
                    clearInterval(timer);
                    if ($this.state.sideLetterUrl) {
                        patnershipSignwin = window.open($this.state.patnershipDocUrl+'?token=' + JSON.parse(reactLocalStorage.get('token')), '_blank', 'width = 1000px, height = 600px')
                        var patnershipTimer = setInterval(function () {
                            if (patnershipSignwin && patnershipSignwin.closed) {
                                clearInterval(patnershipTimer);
                                const docUrl = `${$this.state.sideLetterUrl}?token=${$this.FsnetUtil.getToken()}`
                                $this.FsnetUtil._openDoc(docUrl)
                            }
                        }, 1000);
                    } else {
                        const docUrl = `${$this.state.patnershipDocUrl}?token=${$this.FsnetUtil.getToken()}`
                        $this.FsnetUtil._openDoc(docUrl)
                    }
                }
            }, 1000);
        } else if (this.state.sideLetterUrl) {
            const docUrl = `${this.state.sideLetterUrl}?token=${this.FsnetUtil.getToken()}`
            this.FsnetUtil._openDoc(docUrl)
        } else if (type !== 'modal') {
            this.getDocumets()
        }
    }

    getDocumets() {
        const docUrl = `${this.Constants.BASE_URL}document/view/sa/${this.state.subscriptionId}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    closeNotifySigModal = () =>{
        this.setState({
            notifySigModal:false
        },()=>{
            window.location.reload()
        })
    }

    openNotifySigModal = () =>{
        this.setState({
            notifySigModal:true
        })
    }

    closejointIndividualModal = () => {this.setState({jointIndividualModal:false})}

    proceedSignatoryPage = () => {
        this.closejointIndividualModal();
        this.props.history.push(`/lp/lpSignatory/${this.FsnetUtil._encrypt(this.state.subscriptionId)}`);
    }

    openjointIndividualModal = () => {this.setState({jointIndividualModal:true})}

    lpSignatorySubmit = (val) => () => {
        const obj = { subscriptionId: this.state.subscriptionId }
        this.open();
        this.Fsnethttp.lpSignatorydocsForSignatories(obj).then(result => {
            this.close();
            this.closeResendInviteForSignatoryModal()
            if(val) {
                this.openNotifySigModal();
            } else {
                window.location.reload()
            }
        })
            .catch(error => {
                this.close();
            });
    }

    closeResendInviteForSignatoryModal = () => { this.setState({ showResendInviteForSignatory: false }) }
    showResendInviteForSignatoryModal = () => { this.setState({ showResendInviteForSignatory: true }) }


    navigateToTracker = () => {
        window.location.href = `/fund/view/${this.FsnetUtil._encrypt(this.state.lpObj.fundId)}`
    }

    render() {
        return (
            <div className="accreditedInvestor width100 marginTopSticky" id="subscriptionReview">
                <div className="step6ClassAboveFooter">
                    {
                        this.state.subscriptionStatus.name === 'Closed' ?
                            <div className="staticContent">
                                <h2 className="title marginBottom2">Fund Summary</h2>
                            </div> :
                            <div className="staticContent">
                                <h2 className="title marginBottom2">Review & Confirm</h2>
                                {
                                    this.state.loggedInUserObj.accountType === 'LPDelegate' ?
                                        <h4 className="subtext marginBottom30">Verify that everything is correct before sending to Investor for signature.</h4>
                                        :
                                        <h4 className="subtext marginBottom30 marginRight10">{this.state.investorType === 'Individual' && this.state.lpObj.areYouSubscribingAsJointIndividual ? 'Verify that everything is correct before proceeding.  You will be asked to notify your other signatories to sign first then you will be allowed to sign and submit to the Fund Manager.':'Verify that everything is correct before proceeding to signature.'}</h4>
                                }
                            </div>
                    }

                    <div hidden={this.state.investorType !== 'Individual'}>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.INVESTOR_INFORMATION}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.INVESTOR_TYPE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouSubscribingAsJointIndividual ? 'Joint Individual' : 'Individual'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.EMAIL_ADDRESS}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.email}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">Country Residence:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getCountryName(this.state.lpObj.countryResidenceId)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.countryResidenceId != 231}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">State Residence:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getStateName(this.state.lpObj.stateResidenceId)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.SUBSCRIBING_AS_JOINT_INDIVIDUALS_WITH_SPOUSE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouSubscribingAsJointIndividual != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.areYouSubscribingAsJointIndividual != true}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.INDICATE_LEGAL_OWNERSHIP_TYPE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.typeOfLegalOwnership == 'communityProperty' ? 'Community Property' : ''}</span>
                                            <span className="lightContent">{this.state.lpObj.typeOfLegalOwnership == 'tenantsInCommon' ? 'Tenants in Common' : ''}</span>
                                            <span className="lightContent">{this.state.lpObj.typeOfLegalOwnership == 'jointTenants' ? 'Joint Tenants' : ''}</span>
                                            <span className="lightContent">{this.state.lpObj.typeOfLegalOwnership == 'Other' ? 'Other' : ''}</span>
                                            <span className="lightContent">{this.state.lpObj.typeOfLegalOwnership == 'jointTenantsRights' ? 'Joint Tenants with Rights of Survivorship' : ''}</span>
                                            <span className="lightContent">{this.state.lpObj.typeOfLegalOwnership == 'tenantsEntirety' ? 'Tenants by the Entirety' : ''}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.lpObj.areYouSubscribingAsJointIndividual != true}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.LEGAL_TITLE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>
                                            <span className="lightContent">{this.state.lpObj.legalTitle}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.FOIA_SENSITIVE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.releaseInvestmentEntityRequired != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.releaseInvestmentEntityRequired != true}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">FOIA or similar statutes:</span>
                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.indirectBeneficialOwnersSubjectFOIAStatutes}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.DISQUALIFYING_EVENT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.isSubjectToDisqualifyingEvent != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.fundManagerInfo == null}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.DISCLOSE_FUND_MANAGER}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.fundManagerInfo}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.COUNTRY}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getCountryName(this.state.lpObj.mailingAddressCountry)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.STATE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getStateByCountry(this.state.lpObj.mailingAddressState)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.STREET}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressStreet}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.CITY}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressCity}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.ZIP}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressZip}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.PRIMARY_BUSINESS_TELEPHONE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressPhoneNumber}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/investorInfo/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>
                        {
                            this.state.lpObj && this.state.lpObj.investorQuestions && this.state.lpObj.investorQuestions.length > 0 ?
                                <Row className="step6-row" >
                                    <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                        <span className="col1">{this.state.jsonData.ADDITIONAL_QUESTIONS}</span>
                                    </Col>
                                    <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                        {

                                            this.state.lpObj.investorQuestions.map((record, index) => {
                                                return (
                                                    <Row>
                                                        <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                            <div>
                                                                <span className="col2">{record['questionTitle']}:</span>

                                                            </div>
                                                        </Col>
                                                        <Col md={5} sm={5} xs={5}>
                                                            <div>
                                                                {
                                                                    record['typeOfQuestion'] === 2 || record['typeOfQuestion'] === 3 ?
                                                                        record['typeOfQuestion'] === 2 ?
                                                                            <span className="lightContent">
                                                                                {record['questionResponse'] == 1 ? 'True' : record['questionResponse'] != '' ? 'False' : ''}</span>
                                                                            :
                                                                            <span className="lightContent">{record['questionResponse'] == 1 ? 'Yes' : record['questionResponse'] != '' ? 'No' : ''}</span>
                                                                        :
                                                                        <span className="lightContent">{record['questionResponse']}</span>
                                                                }
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                );
                                            })

                                        }

                                    </Col>

                                    <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                        <span className="col4 colm"><Link to={"/lp/additionalQuestions/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                                    </Col>
                                </Row>
                                :
                                ''
                        }
                        <Row className="step6-row" hidden={this.state.loggedInUserObj.accountType !== 'LP'}>
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.SIGNATORY}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={12} sm={12} xs={12} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.SIGNATORY_TEXT}</span>

                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/lpSignatory/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.CAPITAL_COMMITMENT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.OFFER_SUBSCRIBE_CAPITAL_COMMITMENT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.lpCapitalCommitment ? this.FsnetUtil.convertToCurrencyTwodecimals(this.state.lpObj.lpCapitalCommitment) : ''}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/capitalCommitment/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.ACCREDITED_INVESTOR}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.ACCREDITED_INVESTOR}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouAccreditedInvestor != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/AccreditedInvestor/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.QUALIFIED_PURCHASER}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.QUALIFIED_PURCHASER}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouQualifiedPurchaser != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/qualifiedPurchaser/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" hidden={this.state.lpObj.areYouQualifiedClient == null}>
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1 step-col-pad1">
                                <span className="col1">{this.state.jsonData.QUALIFIED_CLIENT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.QUALIFIED_CLIENT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouQualifiedClient != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/qualifiedPurchaser/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>


                    </div>



                    <div hidden={this.state.investorType !== 'LLC'}>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.INVESTOR_INFORMATION}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">

                                    </Col>
                                    <Col md={5} sm={5} xs={5}>

                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.INVESTOR_TYPE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.jsonData.ENTITY}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.investorSubTypeName === ''}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div >
                                            <span className="col2">{this.state.jsonData.INVESTOR_SUB_TYPE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.investorSubTypeName}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.lpObj.otherInvestorSubType === null}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.INVESTOR_SUB_TYPE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>
                                            <span className="lightContent">{this.state.jsonData.OTHER_ENTITY}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.lpObj.otherInvestorSubType === null}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div >
                                            <span className="col2">{this.state.jsonData.ENTER_ENTITY_TYPE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.otherInvestorSubType}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.EMAIL_ADDRESS}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.email}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.ENTITYS_NAME}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.entityName}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.countryDomicileId == 0}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.ENTITYS_JURISDICTION}:</span>
                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getCountryName(this.state.lpObj.countryDomicileId)}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.lpObj.stateDomicileId == 0 || this.state.lpObj.stateDomicileId == null}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.SELECT_STATE}:</span>
                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>
                                            <span className="lightContent">{this.getStateName(this.state.lpObj.stateDomicileId)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.countryResidenceId == 0}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3" >
                                        <div>
                                            <span className="col2">Country Residence:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getCountryName(this.state.lpObj.countryResidenceId)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.stateResidenceId == 0 || this.state.lpObj.stateResidenceId == null}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">State Residence:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getStateName(this.state.lpObj.stateResidenceId)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.TAX_EXEMPT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.isEntityTaxExemptForUSFederalIncomeTax != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.lpObj.isEntityTaxExemptForUSFederalIncomeTax !== true}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.ENTITY_US_501_C3}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.isEntityUS501c3 != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.FOIA_SENSITIVE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.releaseInvestmentEntityRequired != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.releaseInvestmentEntityRequired != true}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">FOIA or similar statutes:</span>
                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.indirectBeneficialOwnersSubjectFOIAStatutes}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.DISQUALIFYING_EVENT}:</span>
                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>
                                            <span className="lightContent">{this.state.lpObj.isSubjectToDisqualifyingEvent != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.lpObj.fundManagerInfo == null}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.DISCLOSE_FUND_MANAGER}:</span>
                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>
                                            <span className="lightContent">{this.state.lpObj.fundManagerInfo}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.lpObj.fundManagerInfo == null}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.OTHER_INVESTOR_ATTRIBUTES}:</span>
                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>
                                            <span className="lightContent">{this.state.lpObj.otherInvestorAttributes ? this.state.lpObj.otherInvestorAttributes.join(', ') : null}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.COUNTRY}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>
                                            <span className="lightContent">{this.getCountryName(this.state.lpObj.mailingAddressCountry)}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.STATE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getStateByCountry(this.state.lpObj.mailingAddressState)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.STREET}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressStreet}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.CITY}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressCity}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.ZIP}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressZip}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.PRIMARY_BUSINESS_TELEPHONE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressPhoneNumber}</span>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/investorInfo/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>
                        {
                            this.state.lpObj && this.state.lpObj.investorQuestions && this.state.lpObj.investorQuestions.length > 0 ?
                                <Row className="step6-row" >
                                    <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                        <span className="col1">{this.state.jsonData.ADDITIONAL_QUESTIONS}</span>
                                    </Col>
                                    <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                        {

                                            this.state.lpObj.investorQuestions.map((record, index) => {
                                                return (
                                                    <Row>
                                                        <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                            <div>
                                                                <span className="col2">{record['questionTitle']}:</span>

                                                            </div>
                                                        </Col>
                                                        <Col md={5} sm={5} xs={5}>
                                                            <div>
                                                                {
                                                                    record['typeOfQuestion'] === 2 || record['typeOfQuestion'] === 3 ?
                                                                        record['typeOfQuestion'] === 2 ?
                                                                            <span className="lightContent">
                                                                                {record['questionResponse'] == 1 ? 'True' : record['questionResponse'] != '' ? 'False' : ''}</span>
                                                                            :
                                                                            <span className="lightContent">{record['questionResponse'] == 1 ? 'Yes' : record['questionResponse'] != '' ? 'No' : ''}</span>
                                                                        :
                                                                        <span className="lightContent">{record['questionResponse']}</span>
                                                                }
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                );
                                            })

                                        }

                                    </Col>

                                    <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                        <span className="col4 colm"><Link to={"/lp/additionalQuestions/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                                    </Col>
                                </Row>
                                :
                                ''
                        }

                        <Row className="step6-row" hidden={this.state.loggedInUserObj.accountType !== 'LP'}>
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.SIGNATORY}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={12} sm={12} xs={12} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.SIGNATORY_TEXT}</span>

                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/lpSignatory/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.CAPITAL_COMMITMENT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.OFFER_SUBSCRIBE_CAPITAL_COMMITMENT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.lpCapitalCommitment ? this.FsnetUtil.convertToCurrencyTwodecimals(this.state.lpObj.lpCapitalCommitment) : ''}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/capitalCommitment/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>
                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.ACCREDITED_INVESTOR}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.ACCREDITED_INVESTOR}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouAccreditedInvestor != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/AccreditedInvestor/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.QUALIFIED_PURCHASER}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.QUALIFIED_PURCHASER}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouQualifiedPurchaser != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/qualifiedPurchaser/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" hidden={this.state.lpObj.areYouQualifiedClient == null}>
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.QUALIFIED_CLIENT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.QUALIFIED_CLIENT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouQualifiedClient != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/qualifiedPurchaser/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.COMPANIES_ACT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.STATUS_COMPANIES_ACT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="answerText">
                                        <div>
                                            <span className="lightContent" hidden={this.state.lpObj.companiesAct != 1}>{this.state.jsonData.ENTITY_NOT_INVESTMENT_COMPANY}</span>
                                            <span className="lightContent" hidden={this.state.lpObj.companiesAct != 2}>{this.state.jsonData.ENTITY_WOULD_BE_INVESTMENT_COMPANY_SECTION_3C1}</span>
                                            <span className="lightContent" hidden={this.state.lpObj.companiesAct != 3}>{this.state.jsonData.ENTITY_WOULD_BE_INVESTMENT_COMPANY_SECTION_3C7}</span>
                                            <span className="lightContent" hidden={this.state.lpObj.companiesAct != 4}>{this.state.jsonData.ENTITY_IS_AN_INVESTMENT_COMPANY}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <div>
                                    {this.state.lpObj.investorSubType == 9
                                        ?
                                        <Row className="step-row-borderBottom step-row-borderTop remainingQuesitionPadding">
                                            <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                <div>
                                                    <span className="col2">{this.state.jsonData.EQUITY_OWNERS_NUMBER}:</span>

                                                </div>
                                            </Col>
                                            <Col md={5} sm={5} xs={5} className="">
                                                <div>

                                                    <span className="lightContent">{this.state.lpObj.numberOfDirectEquityOwners}</span>
                                                </div>
                                            </Col>
                                        </Row>

                                        :
                                        <Row className="step-row-borderBottom step-row-borderTop remainingQuesitionPadding">
                                            <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                <div>
                                                    <span className="col2">{this.state.jsonData.EQUITY_OWNERS_NUMBER}:</span>

                                                </div>
                                            </Col>
                                            <Col md={5} sm={5} xs={5} className="">
                                                <div>

                                                    <span className="lightContent">{this.state.lpObj.numberOfDirectEquityOwners}</span>
                                                </div>
                                            </Col>
                                        </Row>

                                    }
                                    <Row className="remainingQuesitionPadding">
                                        <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                            <div>
                                                <span className="col2">{this.state.jsonData.CONTROL_COMMON_CONTROL_OTHER_INVESTORS}:</span>

                                            </div>
                                        </Col>
                                        <Col md={5} sm={5} xs={5} className="">
                                            <div>

                                                <span className="lightContent">{this.state.lpObj.existingOrProspectiveInvestorsOfTheFund != true ? 'No' : 'Yes'}</span>
                                            </div>
                                        </Col>
                                    </Row>

                                    <Row hidden={this.state.lpObj.existingOrProspectiveInvestorsOfTheFund != true} className="step-row-borderTop remainingQuesitionPadding">
                                        <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                            <div>
                                                <span className="col2">{this.state.jsonData.EXISTING_PROSPECTIVE_INVESTORS}:</span>

                                            </div>
                                        </Col>
                                        <Col md={5} sm={5} xs={5} className="">
                                            <div>

                                                <span className="lightContent">{this.state.lpObj.numberOfexistingOrProspectives}</span>
                                            </div>
                                        </Col>
                                    </Row>

                                </div>
                                 
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/companiesAct/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.LOOK_THROUGH_ISSUES}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row className="step-row-borderBottom firstQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.NOT_ORGANIZED_PURPOSE_ACQUIRING_INTEREST}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.entityProposingAcquiringInvestment != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="step-row-borderBottom remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.BENEFICIAL_SHARE_INVESTMENTS_SAME_PROPORTIONS}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.entityHasMadeInvestmentsPriorToThedate != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="step-row-borderBottom remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.INVESTMENT_FUND_NOT_CONSTITUE_MORE_THAN_ENTITY}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.partnershipWillNotConstituteMoreThanFortyPercent != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.NO_ABILITY_BENEFICIAL_PARTICIPATION}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.beneficialInvestmentMadeByTheEntity != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                {(this.state.showNumberOfBeneficialOwners)
                                    ?
                                    <div>
                                        {this.state.lpObj.investorSubType == 9
                                            ?
                                            <Row className="step-row-borderBottom step-row-borderTop remainingQuesitionPadding">
                                                <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                    <div>
                                                        <span className="col2">{this.state.jsonData.EQUITY_OWNERS_NUMBER}:</span>

                                                    </div>
                                                </Col>
                                                <Col md={5} sm={5} xs={5} className="">
                                                    <div>

                                                        <span className="lightContent">{this.state.lpObj.numberOfDirectEquityOwners}</span>
                                                    </div>
                                                </Col>
                                            </Row>

                                            :
                                            <Row className="step-row-borderBottom step-row-borderTop remainingQuesitionPadding">
                                                <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                    <div>
                                                        <span className="col2">{this.state.jsonData.EQUITY_OWNERS_NUMBER}:</span>

                                                    </div>
                                                </Col>
                                                <Col md={5} sm={5} xs={5} className="">
                                                    <div>

                                                        <span className="lightContent">{this.state.lpObj.numberOfDirectEquityOwners}</span>
                                                    </div>
                                                </Col>
                                            </Row>

                                        }
                                    </div>
                                    :
                                    null
                                }
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/entityProposing/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.ERISA}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row className="step-row-borderBottom firstQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.EMPLOYEE_BENEFIT_PLAN_SECTION_3_3}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.employeeBenefitPlan != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="step-row-borderBottom remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.PLAN_DEFINED_SECTION_4975E1}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.planAsDefinedInSection4975e1 != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.BENEFIT_PLAN_INVESTOR_UNDER_ASSET_REGULATION}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.benefitPlanInvestor != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.lpObj.benefitPlanInvestor !== true} className="step-row-borderTop remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div >
                                            <span className="col2">{this.state.jsonData.TOTAL_VALUE_EQUITY_INTEREST_BENEFIT_PLAN_INVESTORS}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.totalValueOfEquityInterests}%</span>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/erisa/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>
                    </div>




                    <div hidden={this.state.investorType !== 'Trust'}>
                        <Row id="step6-row1" >
                            <Col md={2} sm={2} xs={6} className="step6-col-pad">
                                <span className="col1">{this.state.jsonData.INVESTOR_INFORMATION}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.INVESTOR_TYPE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.investorType}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row hidden={this.state.investorSubTypeName === ''}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.INVESTOR_SUB_TYPE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.investorSubTypeName}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.EMAIL_ADDRESS}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.email}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.TRUSTS_NAME}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.trustName}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">Country Residence:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getCountryName(this.state.lpObj.countryResidenceId)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.countryResidenceId != 231}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">State Residence:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getStateName(this.state.lpObj.stateResidenceId)}</span>
                                        </div>
                                    </Col>
                                </Row>


                                {this.state.lpObj.investorSubType == 9
                                    ?
                                    <Row>
                                        <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                            <div>
                                                <span className="col2">{this.state.jsonData.TAX_EXEMPT}:</span>

                                            </div>
                                        </Col>
                                        <Col md={5} sm={5} xs={5}>
                                            <div>

                                                <span className="lightContent">{this.state.lpObj.isEntityTaxExemptForUSFederalIncomeTax !== true ? 'No' : 'Yes'}</span>
                                            </div>
                                        </Col>
                                    </Row>
                                    :
                                    <Row>
                                        <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                            <div>
                                                <span className="col2">{this.state.jsonData.TAX_EXEMPT}:</span>

                                            </div>
                                        </Col>
                                        <Col md={5} sm={5} xs={5}>
                                            <div>

                                                <span className="lightContent">{this.state.lpObj.isEntityTaxExemptForUSFederalIncomeTax !== true ? 'No' : 'Yes'}</span>
                                            </div>
                                        </Col>
                                    </Row>

                                }
                                {this.state.lpObj.investorSubType == 9
                                    ?
                                    <div hidden={this.state.lpObj.isEntityTaxExemptForUSFederalIncomeTax !== true}>
                                        <Row>
                                            <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                <div>
                                                    <span className="col2">{this.state.jsonData.IS_TRUST_501C3}:</span>

                                                </div>
                                            </Col>
                                            <Col md={5} sm={5} xs={5}>
                                                <div>

                                                    <span className="lightContent">{this.state.lpObj.isTrust501c3 !== true ? 'No' : 'Yes'}</span>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                    :
                                    <div hidden={this.state.lpObj.isEntityTaxExemptForUSFederalIncomeTax !== true}>
                                        <Row>
                                            <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                <div>
                                                    <span className="col2">{this.state.jsonData.IS_TRUST_501C3}:</span>

                                                </div>
                                            </Col>
                                            <Col md={5} sm={5} xs={5}>
                                                <div>

                                                    <span className="lightContent">{this.state.lpObj.isTrust501c3 !== true ? 'No' : 'Yes'}</span>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                }
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.FOIA_SENSITIVE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.releaseInvestmentEntityRequired !== true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>


                                <Row hidden={this.state.lpObj.releaseInvestmentEntityRequired != true}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">FOIA or similar statutes:</span>
                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.indirectBeneficialOwnersSubjectFOIAStatutes}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.DISQUALIFYING_EVENT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.isSubjectToDisqualifyingEvent != true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.fundManagerInfo == null}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.DISCLOSE_FUND_MANAGER}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.fundManagerInfo}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.fundManagerInfo == null}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.OTHER_INVESTOR_ATTRIBUTES}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.otherInvestorAttributes ? this.state.lpObj.otherInvestorAttributes.join(', ') : null}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.COUNTRY}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getCountryName(this.state.lpObj.mailingAddressCountry)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.STATE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.getStateByCountry(this.state.lpObj.mailingAddressState)}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.STREET}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressStreet}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.CITY}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressCity}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.ZIP}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressZip}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.investorSubType != 10}>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.EXACT_LEGAL_TITLE_DESIGNATION}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.legalTitleDesignation}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.PRIMARY_BUSINESS_TELEPHONE}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5}>
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.mailingAddressPhoneNumber}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/investorInfo/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>
                        {
                            this.state.lpObj && this.state.lpObj.investorQuestions && this.state.lpObj.investorQuestions.length > 0 ?
                                <Row className="step6-row" >
                                    <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                        <span className="col1">{this.state.jsonData.ADDITIONAL_QUESTIONS}</span>
                                    </Col>
                                    <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                        {

                                            this.state.lpObj.investorQuestions.map((record, index) => {
                                                return (
                                                    <Row>
                                                        <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                            <div>
                                                                <span className="col2">{record['questionTitle']}:</span>

                                                            </div>
                                                        </Col>
                                                        <Col md={5} sm={5} xs={5}>
                                                            <div>
                                                                {
                                                                    record['typeOfQuestion'] === 2 || record['typeOfQuestion'] === 3 ?
                                                                        record['typeOfQuestion'] === 2 ?
                                                                            <span className="lightContent">
                                                                                {record['questionResponse'] == 1 ? 'True' : record['questionResponse'] != '' ? 'False' : ''}</span>
                                                                            :
                                                                            <span className="lightContent">{record['questionResponse'] == 1 ? 'Yes' : record['questionResponse'] != '' ? 'No' : ''}</span>
                                                                        :
                                                                        <span className="lightContent">{record['questionResponse']}</span>
                                                                }
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                );
                                            })

                                        }

                                    </Col>

                                    <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                        <span className="col4 colm"><Link to={"/lp/additionalQuestions/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                                    </Col>
                                </Row>
                                :
                                ''
                        }

                        <Row className="step6-row" hidden={this.state.loggedInUserObj.accountType !== 'LP'}>
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.SIGNATORY}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={12} sm={12} xs={12} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.SIGNATORY_TEXT}</span>

                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/lpSignatory/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.CAPITAL_COMMITMENT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.OFFER_SUBSCRIBE_CAPITAL_COMMITMENT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.lpCapitalCommitment ? this.FsnetUtil.convertToCurrencyTwodecimals(this.state.lpObj.lpCapitalCommitment) : ''}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/capitalCommitment/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.ACCREDITED_INVESTOR}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.ACCREDITED_INVESTOR}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouAccreditedInvestor !== true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/AccreditedInvestor/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.QUALIFIED_PURCHASER}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.QUALIFIED_PURCHASER}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouQualifiedPurchaser !== true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/qualifiedPurchaser/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" hidden={this.state.lpObj.areYouQualifiedClient == null}>
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.QUALIFIED_CLIENT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row>
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.QUALIFIED_CLIENT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.areYouQualifiedClient !== true ? 'No' : 'Yes'}</span>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/qualifiedPurchaser/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>


                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.COMPANIES_ACT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row className=" firstQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.STATUS_COMPANIES_ACT}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="answerText">
                                        <div>

                                            <span className="lightContent" hidden={this.state.lpObj.companiesAct != 1}>{this.state.jsonData.TRUST_NOT_INVESTMENT_COMPANY}</span>
                                            <span className="lightContent" hidden={this.state.lpObj.companiesAct != 2}>{this.state.jsonData.TRUST_WOULD_BE_INVESTMENT_COMPANY_SECTION_3C1}</span>
                                            <span className="lightContent" hidden={this.state.lpObj.companiesAct != 3}>{this.state.jsonData.TRUST_WOULD_BE_INVESTMENT_COMPANY_SECTION_3C7}</span>
                                            <span className="lightContent" hidden={this.state.lpObj.companiesAct != 4}>{this.state.jsonData.TRUST_IS_AN_INVESTMENT_COMPANY}</span>
                                        </div>
                                    </Col>
                                </Row>

                               
                                <div>
                                    {this.state.lpObj.investorSubType == 9
                                        ?
                                        <Row className="step-row-borderBottom step-row-borderTop remainingQuesitionPadding">
                                            <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                <div>
                                                    <span className="col2">{this.state.jsonData.SPECIFY_NUMBER_GRANTORS}:</span>

                                                </div>
                                            </Col>
                                            <Col md={5} sm={5} xs={5} className="">
                                                <div>

                                                    <span className="lightContent">{this.state.lpObj.numberOfDirectEquityOwners}</span>
                                                </div>
                                            </Col>
                                        </Row>

                                        :
                                        <Row className="step-row-borderBottom step-row-borderTop remainingQuesitionPadding">
                                            <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                <div>
                                                    <span className="col2">{this.state.jsonData.SPECIFY_NUMBER_BENEFICIARIES}:</span>

                                                </div>
                                            </Col>
                                            <Col md={5} sm={5} xs={5} className="">
                                                <div>

                                                    <span className="lightContent">{this.state.lpObj.numberOfDirectEquityOwners}</span>
                                                </div>
                                            </Col>
                                        </Row>

                                    }
                                    <Row className="remainingQuesitionPadding">
                                        <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                            <div>
                                                <span className="col2">{this.state.jsonData.TRUST_EXISTING_PROSPECTIVE_INVESTORS_FUND_CONTROL}:</span>

                                            </div>
                                        </Col>
                                        <Col md={5} sm={5} xs={5} className="">
                                            <div>

                                                <span className="lightContent">{this.state.lpObj.existingOrProspectiveInvestorsOfTheFund != true ? 'No' : 'Yes'}</span>
                                            </div>
                                        </Col>
                                    </Row>

                                    <Row hidden={this.state.lpObj.existingOrProspectiveInvestorsOfTheFund != true} className="step-row-borderTop remainingQuesitionPadding">
                                        <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                            <div>
                                                <span className="col2">{this.state.jsonData.HOW_MANY_EXISTING_PROSPECTIVE_INVESTORS}:</span>

                                            </div>
                                        </Col>
                                        <Col md={5} sm={5} xs={5} className="">
                                            <div>

                                                <span className="lightContent">{this.state.lpObj.numberOfexistingOrProspectives}</span>
                                            </div>
                                        </Col>
                                    </Row>

                                </div>
                                   


                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/companiesAct/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row">
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.LOOK_THROUGH_ISSUES}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row className="step-row-borderBottom firstQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.NOT_ORGANIZED_PURPOSE_ACQUIRING_INTEREST}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.entityProposingAcquiringInvestment != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                               
                                <Row className="step-row-borderBottom remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.BENEFICIAL_SHARE_INVESTMENTS_SAME_PROPORTIONS}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.entityHasMadeInvestmentsPriorToThedate != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="step-row-borderBottom remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.INVESTMENT_FUND_NOT_CONSTITUE_MORE_THAN_ENTITY}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.partnershipWillNotConstituteMoreThanFortyPercent != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.NO_ABILITY_BENEFICIAL_PARTICIPATION}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.beneficialInvestmentMadeByTheEntity != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>
                                {(this.state.showNumberOfBeneficialOwners)
                                    ?
                                    <div>
                                        {this.state.lpObj.investorSubType == 9
                                            ?
                                            <Row className="step-row-borderTop remainingQuesitionPadding">
                                                <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                    <div>
                                                        <span className="col2">{this.state.jsonData.EQUITY_OWNERS_NUMBER}:</span>

                                                    </div>
                                                </Col>
                                                <Col md={5} sm={5} xs={5} className="">
                                                    <div>

                                                        <span className="lightContent">{this.state.lpObj.numberOfDirectEquityOwners}</span>
                                                    </div>
                                                </Col>
                                            </Row>

                                            :
                                            <Row className="step-row-borderTop remainingQuesitionPadding">
                                                <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                                    <div>
                                                        <span className="col2">{this.state.jsonData.EQUITY_OWNERS_NUMBER}:</span>

                                                    </div>
                                                </Col>
                                                <Col md={5} sm={5} xs={5} className="">
                                                    <div>

                                                        <span className="lightContent">{this.state.lpObj.numberOfDirectEquityOwners}</span>
                                                    </div>
                                                </Col>
                                            </Row>

                                        }
                                    </div>
                                    :
                                    null
                                }
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/entityProposing/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.ERISA}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <Row className="step-row-borderBottom firstQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.EMPLOYEE_BENEFIT_PLAN_SECTION_3_3}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.employeeBenefitPlan != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row className="step-row-borderBottom remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.PLAN_DEFINED_SECTION_4975E1}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.planAsDefinedInSection4975e1 != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row className="remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.BENEFIT_PLAN_INVESTOR_UNDER_ASSET_REGULATION}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.benefitPlanInvestor != true ? 'False' : 'True'}</span>
                                        </div>
                                    </Col>
                                </Row>

                                <Row hidden={this.state.lpObj.benefitPlanInvestor !== true} className="step-row-borderTop remainingQuesitionPadding">
                                    <Col md={7} sm={7} xs={7} className="step-col-pad3">
                                        <div>
                                            <span className="col2">{this.state.jsonData.TOTAL_VALUE_EQUITY_INTEREST_BENEFIT_PLAN_INVESTORS}:</span>

                                        </div>
                                    </Col>
                                    <Col md={5} sm={5} xs={5} className="">
                                        <div>

                                            <span className="lightContent">{this.state.lpObj.totalValueOfEquityInterests}%</span>
                                        </div>
                                    </Col>
                                </Row>

                                
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"/lp/erisa/" + this.state.lpObj.id}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>


                    </div>






                    <div hidden={this.state.investorType !== 'revocableTrust'}>
                        <Row id="step6-row1" >
                            <Col md={2} sm={2} xs={6} className="step6-col-pad">
                                <span className="col1">{this.state.jsonData.INVESTOR_INFORMATION} (1/2)</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <div className="col2">{this.state.jsonData.INVESTOR_TYPE}:  {this.state.jsonData.TRUST}</div>
                                <div className="col2">{this.state.jsonData.INVESTOR_SUB_TYPE}: {this.state.jsonData.REVOCABLE_TRUST}</div>
                                <div className="col2">{this.state.jsonData.NUMBER_OF_GRANTORS_TRUST}: </div>
                                <div className="col2">{this.state.jsonData.EMAIL_ADDRESS}:</div>
                                <div className="col2">{this.state.jsonData.ENTITYS_NAME}:</div>
                                <div className="col2">{this.state.jsonData.TRUST_LEGALLY_DOMICILED}:</div>
                                <div className="col2">{this.state.jsonData.IS_ENTITY_TAX_EXEMPT_US_FEDERAL}: </div>
                                <div className="col2">{this.state.jsonData.ENTITY_US_501_C3}: </div>
                                <div className="col2">{this.state.jsonData.IS_ENTITY_FUND_OF_FUNDS}: </div>
                                <div className="col2">{this.state.jsonData.IS_ENTITY_REQUIRED_TO_RELEASE_INVESTMENT_INFORMATION}: </div>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"investorInfo"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.INVESTOR_INFORMATION} (2/2)</span>
                            </Col>
                            <Col md={7} sm={7} sx={6}>
                                <div className="col2">{this.state.jsonData.STREET}:</div>
                                <div className="col2">{this.state.jsonData.CITY}:</div>
                                <div className="col2">{this.state.jsonData.STATE}:</div>
                                <div className="col2">{this.state.jsonData.Zip}:</div>
                                <div className="col2">{this.state.jsonData.PHONE_NUMBER}:</div>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"investorInfo"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.ACCREDITED_INVESTOR}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <span className="col2">{this.state.jsonData.IS_TRUST_ACCREDITED_INVESTOR_MEANING_RULE_501}:</span>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"AccreditedInvestor"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.QUALIFIED_PURCHASER}/{this.state.jsonData.QUALIFIED_CLIENT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <span className="col2">{this.state.jsonData.IS_TRUST_QUALIFIED_PURCHASER_SECTION_2A51}:</span>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"qualifiedPurchaser"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.COMPANIES_ACT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <div className="col2">{this.state.jsonData.TRUST_IS_INVESTMENT_COMPANY_TO_COMAPANIES_ACT}</div>
                                <div className="col2">{this.state.jsonData.NUMBER_OF_DIRECT_EQUITY_OWNERS_ENTITY}</div>
                                <div className="col2">{this.state.jsonData.ANY_EXISTING_PROSPECTIVE_INVESTORS}</div>
                                <div className="col2">{this.state.jsonData.EXISTING_OR_PROSPECTIVE_INVESTORS}</div>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"companiesAct"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.ERISA}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <div className="col2">{this.state.jsonData.TRUST_AN_EMPLOYEE_BENEFIT_PLAN_SECTION_3_3}:</div>
                                <div className="col2">{this.state.jsonData.TRUST_IS_PLAN_SECTION_4975E1}:</div>
                                <div className="col2">{this.state.jsonData.TRUST_IS_ENTITY_DEEMED_BENEFIT_PLAN_INVESTOR}:</div>
                                <div className="col2">{this.state.jsonData.PERSON_EXECUTING_AGREEMENT}:</div>
                                <div className="col2">{this.state.jsonData.PERSON_EXECUTING_AGREEMENT}:</div>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"erisa"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>
                    </div>

                    <div hidden={this.state.investorType !== 'iRevocableTrust'}>
                        <Row id="step6-row1" >
                            <Col md={2} sm={2} xs={6} className="step6-col-pad">
                                <span className="col1">{this.state.jsonData.INVESTOR_INFORMATION} (1/2)</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <div className="col2">{this.state.jsonData.INVESTOR_TYPE}:  {this.state.jsonData.TRUST}</div>
                                <div className="col2">{this.state.jsonData.INVESTOR_SUB_TYPE}: {this.state.jsonData.IREVOCABLE_TRUST}:</div>
                                <div className="col2">{this.state.jsonData.TRUSTS_NAME}:</div>
                                <div className="col2">{this.state.jsonData.EMAIL_ADDRESS}:</div>
                                <div className="col2">{this.state.jsonData.ENTITYS_NAME}:</div>
                                <div className="col2">{this.state.jsonData.TRUST_LEGALLY_DOMICILED}:</div>
                                <div className="col2">{this.state.jsonData.IS_ENTITY_TAX_EXEMPT_US_FEDERAL}: </div>
                                <div className="col2">{this.state.jsonData.ENTITY_US_501_C3}: </div>
                                <div className="col2">{this.state.jsonData.IS_ENTITY_REQUIRED_UNITED_STATES_FOIA}: </div>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"investorInfo"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.INVESTOR_INFORMATION} (2/2)</span>
                            </Col>
                            <Col md={7} sm={7} sx={6}>
                                <div className="col2">{this.state.jsonData.STREET}:</div>
                                <div className="col2">{this.state.jsonData.CITY}:</div>
                                <div className="col2">{this.state.jsonData.STATE}:</div>
                                <div className="col2">{this.state.jsonData.ZIP}:</div>
                                <div className="col2">{this.state.jsonData.EXACT_LEGAL_TITLE_DESIGNATION}:</div>
                                <div className="col2">{this.state.jsonData.PHONE_NUMBER}:</div>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"investorInfo"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.ACCREDITED_INVESTOR}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <span className="col2">{this.state.jsonData.IS_TRUST_ACCREDITED_INVESTOR_MEANING_RULE_501}:</span>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"AccreditedInvestor"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.QUALIFIED_PURCHASER}/{this.state.jsonData.QUALIFIED_CLIENT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <span className="col2">{this.state.jsonData.IS_TRUST_QUALIFIED_PURCHASER_SECTION_2A51}:</span>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"qualifiedPurchaser"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.COMPANIES_ACT}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <div className="col2">{this.state.jsonData.TRUST_IS_INVESTMENT_COMPANY_TO_COMAPANIES_ACT}</div>
                                <div className="col2">{this.state.jsonData.NUMBER_OF_DIRECT_EQUITY_OWNERS_ENTITY}</div>
                                <div className="col2">{this.state.jsonData.ANY_EXISTING_PROSPECTIVE_INVESTORS_CONTROL_WITH_ENTITY}</div>
                                <div className="col2">{this.state.jsonData.EXISTING_OR_PROSPECTIVE_INVESTORS}</div>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"companiesAct"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>

                        <Row className="step6-row" >
                            <Col md={2} sm={2} xs={6} className="step-col-pad step-col-pad1">
                                <span className="col1">{this.state.jsonData.ERISA}</span>
                            </Col>
                            <Col md={9} sm={9} xs={6} className="step-col-pad2">
                                <div className="col2">{this.state.jsonData.TRUST_AN_EMPLOYEE_BENEFIT_PLAN_SECTION_3_3}:</div>
                                <div className="col2">{this.state.jsonData.TRUST_IS_PLAN_SECTION_4975E1}:</div>
                                <div className="col2">{this.state.jsonData.TRUST_IS_ENTITY_DEEMED_BENEFIT_PLAN_INVESTOR}:</div>
                                <div className="col2">{this.state.jsonData.PERSON_EXECUTING_AGREEMENT}:</div>
                                <div className="col2">{this.state.jsonData.PERSON_EXECUTING_AGREEMENT}:</div>
                            </Col>
                            <Col md={1} sm={1} xs={6} className="step-col-pad4" hidden={this.state.subscriptionStatus.name === 'Closed'}>
                                <span className="col4 colm"><Link to={"erisa"}>{this.state.jsonData.CHANGE}</Link></span>
                            </Col>
                        </Row>
                    </div>

                    {
                        ['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) > -1 ?
                            <div className="staticTextBelowTable staticTextBelowTablePadding text-center">
                                <Button className="confirmSubmitReview" onClick={this.navigateToTracker}>Proceed To Tracker</Button>
                            </div>
                            :
                            <div className="staticTextBelowTable staticTextBelowTablePadding text-center">
                                {
                                    (this.state.isPendingAction && (['LP','SecondaryLP'].indexOf(this.FsnetUtil.getUserRole()) > -1))?
                                        <Button className="confirmSubmitReview" onClick={this.submitToSign(false)}>{this.state.jsonData.CONFIRM_AND_SUBMIT}</Button>
                                    :
                                    <div>
                                        {
                                            this.state.subscriptionStatus.name === 'Close-Ready' || this.state.subscriptionStatus.name === 'Closed' || this.state.subscriptionStatus.id == 16 ?
                                                <div>
                                                    <Button className="confirmSubmitReview marginRight20" onClick={this.viewSignedDoc}>View Document</Button>
                                                    {
                                                        (this.state.lpObj.noOfSignaturesRequired > 0 && this.state.subscriptionStatus.name !== 'Closed') &&
                                                        <Button className="confirmSubmitReview marginRight20" onClick={this.showResendInviteForSignatoryModal}>Reissue Document</Button>
                                                    }
                                                    {
                                                        this.state.loggedInUserObj.accountType === 'LP' && this.state.subscriptionStatus.name !== 'Closed' ?
                                                            <Button className="confirmSubmitReview" onClick={this.submitSubscription(false)} disabled={(this.state.lpObj.noOfSignaturesRequired > 0 && !this.state.lpObj.isAllSignatoriesSigned) || (!this.state.enableConfirmAndSubmit)}>{this.state.jsonData.CONFIRM_AND_SUBMIT}</Button>
                                                            :
                                                            <Button className="confirmSubmitReview" onClick={this.submitSubscription(false)} disabled={this.state.lpObj.isCurrentUserSigned || !this.state.enableConfirmAndSubmit} hidden={(this.state.subscriptionStatus.name === 'Closed') || this.state.loggedInUserObj.accountType === 'LPDelegate'}>{this.state.jsonData.CONFIRM_AND_SUBMIT}</Button>
                                                    }
                                                </div>
                                            :
                                            (!this.state.lpObj.isCurrentUserSigned && this.state.subscriptionStatus.name === 'Open-Ready-Draft') &&
                                                this.state.lpObj.subscriptionStatus.name === 'Open-Ready-Draft' && this.state.lpObj.noOfSignaturesRequired > 0 && this.FsnetUtil.getUserRole() === 'LP' ?
                                                    <Button className="confirmSubmitReview" onClick={this.lpSignatorySubmit(true)} disabled={!this.state.enableConfirmAndSubmit || this.state.loggedInUserObj.isLpSignatoriesNotified} hidden={(this.state.subscriptionStatus.name === 'Closed') || this.state.loggedInUserObj.accountType === 'LPDelegate'}>Notify Signatories</Button>
                                                    :
                                                    <Button className="confirmSubmitReview" onClick={this.submitSubscription(false)} disabled={!this.state.enableConfirmAndSubmit} hidden={(this.state.subscriptionStatus.name === 'Closed' && !this.state.lpObj.isCurrentUserSigned) || this.state.loggedInUserObj.accountType === 'LPDelegate'}>{this.state.jsonData.CONFIRM_AND_SUBMIT}</Button>
                                        }

                                        {
                                            (this.state.subscriptionStatus.name === 'Open-Ready-Draft' && this.state.lpObj.sideletterAgreement && !this.state.lpObj.sideletterAgreement.isCurrentUserSigned && this.state.lpObj.isCurrentUserSigned) &&
                                            <Button className="confirmSubmitReview" onClick={this.submitSubscription(true)} disabled={!this.state.enableConfirmAndSubmit} hidden={this.state.loggedInUserObj.accountType === 'LPDelegate' || !this.state.enableConfirmAndSubmit}>{this.state.jsonData.CONFIRM_AND_SUBMIT}</Button>
                                        }

                                        <Button className="confirmSubmitReview" disabled={!this.state.enableConfirmAndSubmit} onClick={this.sendForSignature} hidden={this.state.loggedInUserObj.accountType !== 'LPDelegate'}>{this.state.jsonData.SEND_SIGNATURE}</Button>

                                    </div>
                                }
                                
                            </div>
                    }


                </div>

                <Modal id="subscriptionDelModal" backdrop="static" className="resubmitModal" show={this.state.docModal} onHide={this.handledocModalClose}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.SUBMIT_SUBSCRIPTION_AGREEMENT}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.RESUBMIT_SUBSCRIPTION_AGREEMENT}?</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handledocModalClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.submitToSign(false)}>{this.state.jsonData.SUBMIT}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="subscriptionDelModal" backdrop="static" show={this.state.alertModal} onHide={this.handlealertModalClose}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.ALERT}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.SIGN_PARTNERSHIP_AGREEMENT}</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={12} md={12} sm={12} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.handlealertModalClose}>{this.state.jsonData.OK}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal backdrop="static" className="viewSignDocu"
                    show={this.state.showConfirmModal}
                    onHide={this.closeConfirmModal}
                >
                    <Modal.Header closeButton />
                    <Modal.Body>
                        <label className="label-text marginBot12 width100">
                            Your documents have been signed and sent to the Fund Manager for review.  You may view these documents at any time prior to closing in the “View Pending Documents” section. 
                        </label>
                        {/* <div className="text-center marginTop30">
                            <Button
                                type="button"
                                className="fsnetSubmitButton btnEnabled marginRight10 fontFamilyBold viewSignDocButton marginLeft30"
                                onClick={(e) => this.viewSignedDoc(e, 'modal')}
                            >
                                View Signed Documents
                        </Button>
                        </div> */}
                    </Modal.Body>
                </Modal>
                <Modal backdrop="static" className="" show={this.state.showLPDelegateInvestorModal} onHide={this.closeLPDelegateInvestorModal} dialogClassName="investorForSign">
                    <Modal.Header closeButton />
                    <Modal.Body>
                        <label className="label-text marginBot12 width100">
                            Thank you. The Fund's documents have been sent to the Investor for signature.
                        </label>
                        <div className="text-center marginTop30">
                            <Button type="button" className="fsnetSubmitButton btnEnabled marginRight10 fontFamilyBold viewSignDocButton marginLeft30" onClick={this.redirectToDashbord}>Okay</Button>
                        </div>
                    </Modal.Body>
                </Modal>
                <Modal backdrop="static" className="" show={this.state.notifySigModal} onHide={this.closeNotifySigModal} dialogClassName="investorForSign">
                    <Modal.Header closeButton />
                    <Modal.Body>
                        <label className="title-md marginBot12 width100">
                            Secondary Signatory is required to proceed. Secondary Signatory has been notified to complete the signing.
                        </label>
                        <Row className="marginTop10">
                            <Col lg={12} md={12} sm={12} xs={12} className="text-center">
                                <Button type="button" className="fsnetSubmitButton btnEnabled width100Px" onClick={this.closeNotifySigModal}>Ok</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="subscriptionDelModal" backdrop="static" className="resubmitModal"  show={this.state.showResendInviteForSignatory} onHide={this.closeResendInviteForSignatoryModal} dialogClassName="marginTop10">
                    <Modal.Header closeButton />
                    <Modal.Title>Notify Investors/Signatories</Modal.Title>
                    <Modal.Body>
                        <label className="label-text marginBot20 width100">
                            {this.state.jsonData.NOTIFT_SUBSCRIPTION_AGREEMENT}
                        </label>
                        <Row className="marginTop30">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeResendInviteForSignatoryModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button className="fsnetSubmitButton btnEnabled" onClick={this.lpSignatorySubmit(false)}>Submit</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="subscriptionDelModal" backdrop="static" className="resubmitModal"  show={this.state.jointIndividualModal} onHide={this.closejointIndividualModal} dialogClassName="marginTop10">
                    <Modal.Header closeButton />
                    <Modal.Title>JOINT INDIVIDUAL</Modal.Title>
                    <Modal.Body>
                        <label className="label-text marginBot20 width100">
                            {this.state.jsonData.JOINT_INDIVIDUAL_MODAL}
                        </label>
                        <Row className="marginTop30">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled btn-md" onClick={this.proceedSignatoryPage}>ADD SECONDARY SIGNATORY</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button className="fsnetSubmitButton btnEnabled btn-md" onClick={this.submitSubscription(true)}>PROCEED</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal backdrop="static" className='passwordModal' show={this.state.showSignatureModal} onHide={this.closeSignatureModal}>
                    <Modal.Header closeButton />
                    <Modal.Body>
                        <div className="title">Please add authentication password using this <a href="/settings/authentication-password">link</a>.</div>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
            </div>
        );
    }
}

export default reviewComponent;


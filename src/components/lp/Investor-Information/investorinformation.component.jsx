import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Button, Radio, Row, Col, FormControl, Modal, OverlayTrigger, Tooltip, Checkbox as CBox } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import PhoneInput from 'react-phone-number-input';
import { PubSub } from 'pubsub-js';
import { reactLocalStorage } from 'reactjs-localstorage';


// var getLpSubscription = {};
var tooltipsCount = 0, objList = [], objListArray = [];
class InvestorInformationComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            investmentAvailableForRestoreModal: false,
            investorSubTypeValue: 0,
            isFormChanged: false,
            showModal: false,
            showConfirmationModal: false,
            email: '',
            titleUniqueCheckModal: false,
            investorType: 'LLC',
            userObj: reactLocalStorage.get('userData') ? JSON.parse(reactLocalStorage.get('userData')) : '',
            mailingAddressPhoneNumber: reactLocalStorage.get('userData') ? JSON.parse(reactLocalStorage.get('userData')).cellNumber : '',
            cellNumberBorder: false,
            cellNumberMsz: '',
            mailingAddressPhoneNumberValid: false,
            areYouSubscribingAsJointIndividual: null,
            investorPageValid: false,
            name: '',
            nameBorder: false,
            nameMsz: '',
            nameValid: false,
            otherInvestorAttributes: [],
            mailingAddressStreet: '',
            streetBorder: false,
            streetMsz: '',
            mailingAddressStreetValid: false,
            mailingAddressCity: '',
            cityBorder: false,
            cityMsz: '',
            stateResidenceId: 0,
            countryResidenceId: 0,
            numberOfGrantorsOfTheTrust: '',
            mailingAddressCityValid: false,
            mailingAddressCountry: '',
            mailingAddressState: '',
            stateBorder: false,
            stateMsz: '',
            mailingAddressStateValid: false,
            mailingAddressZip: '',
            zipBorder: false,
            zipMsz: '',
            mailingAddressZipValid: false,
            typeOfLegalOwnership: '',
            showIndividualStep1: true,
            lpsubscriptionTotalObj: {},
            typeOfLegalOwnershipValid: false,
            areYouSubscribingAsJointIndividualValid: false,
            enableLeftIcon: false,
            typeOfLegalOwnershipName: '',
            showLLCInvestorInfoPage1: true,
            showTrustRevocableInvestorPage1: true,
            showTrustIrrevocableInvestorPage1: true,
            showInvestorType: true,
            investorSubTypes: [],
            investorTrustSubTypes: [],
            countriesList: [],
            usStatesList: [],
            statesList: [],
            IstheEntityRequiredFOIAWorldwide: null,
            IstheEntityRequiredFOIAWorldwideValid: true,
            isEntityTaxExemptForUSFederalIncomeTax: null,
            isEntityTaxExemptForUSFederalIncomeTaxValid: true,
            isSubjectToDisqualifyingEvent: null,
            isSubjectToDisqualifyingEventValid: false,
            isEntityUS501c3: null,
            isTrust501c3: null,
            isEntityUS501c3Valid: false,
            releaseInvestmentEntityRequired: null,
            releaseInvestmentEntityRequiredValid: false,
            istheEntityFundOfFundsOrSimilarTypeVehicle: null,
            istheEntityFundOfFundsOrSimilarTypeVehicleValid: false,
            entityName: '',
            entityTitle: '',
            trustName: '',
            legalTitleDesignation: '',
            numberOfGrantorsOfTheTrust: '',
            entityNameBorder: false,
            entityNameMsz: '',
            entityNameValid: false,
            entityTitleBorder: false,
            entityTitleMsz: '',
            entityTitleValid: false,
            investorSubType: 0,
            investorSubTypeBorder: false,
            investorSubTypeMsz: '',
            investorSubTypeValid: false,
            otherInvestorSubType: '',
            otherInvestorSubTypeBorder: false,
            otherInvestorSubTypeMsz: '',
            otherInvestorSubTypeValid: false,
            countryDomicileId: 0,
            stateResidence: '',
            countryResidence: '',
            countryDomicileIdBorder: false,
            countryDomicileIdValid: false,
            countryDomicileIdMsz: '',
            indirectBeneficialOwnersSubjectFOIAStatutes: '',
            indirectBeneficialOwnersSubjectFOIAStatutesBorder: false,
            indirectBeneficialOwnersSubjectFOIAStatutesValid: false,
            indirectBeneficialOwnersSubjectFOIAStatutesMsz: '',
            investorJurisdictionTypes: [],
            isEntityUS501c3Msz: '',
            isEntityTaxExemptForUSFederalIncomeTaxMsz: '',
            investorInfoErrorMsz: '',
            stateResidenceId: 0,
            stateResidenceIdBorder: false,
            stateResidenceIdMsz: '',
            jsonData: {},
            openPreviousFund: false,
            previousFundList: [],
            prepopulateId: null,
            disablePreviousFundBtn: false,
            hidePreviousFundBtn: false,
            addedQuestionList: []
        }
    }

    //Get the id from the url
    //Get the investor sub types
    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.investorSubTypes();
        this.investorTrustSubTypes();
        this.getAllCountries();
        this.getUSStates();
        this.getSubscriptionDetails(id);
        this.getJsonData();
    }

    openPreviousFundModal = (type) => {
        if (this.state.lpsubscriptionTotalObj.id) {
            this.open();
            this.Fsnethttp.previousFundDetails(this.state.lpsubscriptionTotalObj.id).then(result => {
                this.close();
                if (result.data && result.data.funds.length > 0) {
                    if (type === 'url') {
                        this.setState({
                            openPreviousFund: false,
                            disablePreviousFundBtn: false
                        })
                    } else {
                        this.setState({
                            openPreviousFund: true,
                            previousFundList: result.data.funds,
                            searchList: result.data.funds
                        })
                    }
                } else {
                    this.setState({
                        disablePreviousFundBtn: true,
                    })
                }
            })
                .catch(error => {
                    this.close();
                    this.setState({
                        disablePreviousFundBtn: true
                    })
                });
        }
    }

    closePreviousFundModal = () => {
        this.setState({
            openPreviousFund: false,
            prepopulateErrorMsz: null
        })
    }

    submitPrePopulate = () => {
        if (this.state.prepopulateId) {
            const getObj = this.state.previousFundList.filter(doc => doc.id == this.state.prepopulateId);
            const selectedId = getObj[0].fundInvestor[0].id;
            this.open();

            let obj = { selectedSubscriptionId: selectedId, subscriptionId: this.state.lpsubscriptionTotalObj.id, lpId: this.state.lpsubscriptionTotalObj.lpId };
            this.Fsnethttp.SubmitPrePopulate(obj).then(result => {
                this.close();
                this.closePreviousFundModal();
                window.location.reload();
            }).catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined) {
                    this.setState({
                        prepopulateErrorMsz: error.response.data.message,
                    });
                } else {
                    this.setState({
                        prepopulateErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            })
        }
    }

    searchFunds = (e) => {
        const value = e.target.value;
        const filterFunds = this.state.searchList.filter(obj => obj.legalEntity.toLowerCase().indexOf(value.trim().toLowerCase()) > -1);
        this.setState({
            previousFundList: filterFunds
        });
    }

    checkTooltips(text, type) {
        let tooltipWords = ['Disqualifying Event', 'Regulation D of Rule 506(d)', 'Securities Act'];
        let count = 0;
        for (let idx = 0; idx < tooltipWords.length; idx++) {
            if (text && text.indexOf(tooltipWords[idx]) > -1 && objList.indexOf(type) === -1) {
                let arrayList = this.FsnetUtil.allIndexOf(text, tooltipWords[idx])
                let splitText = [];
                let currentIndex = 0;
                for (let jdx = 0; jdx < arrayList.length; jdx++) {
                    let jdxLength = arrayList[jdx] + tooltipWords[idx].length;
                    if (jdx === (arrayList.length - 1)) {
                        splitText.push(text.substr(currentIndex, text.length))
                    } else {
                        splitText.push(text.substr(currentIndex, jdxLength))
                        currentIndex = jdxLength
                    }
                }
                let newText = ''
                if (splitText.length === 1) {
                    count++;
                    let indexPos = tooltipsCount + count
                    text = text.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                } else {
                    for (let pos of splitText) {
                        count++;
                        let indexPos = tooltipsCount + count
                        newText = newText + pos.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                    }
                    text = newText;
                }

            }
            if (text && idx === tooltipWords.length - 1 && objList.indexOf(type) === -1) {
                objListArray.push({ type: type, text: text })
                objList.push(type)
            }
        }
        tooltipsCount = tooltipsCount + count;
        for (let index of objListArray) {
            if (index['type'] && index['text']) {
                if (index['type'] === type) {
                    return index['text'];
                }
            }
        }
        return text
    }

    addClickEvent(obj) {
        if (tooltipsCount > 0) {
            for (let idx = 0; idx < tooltipsCount; idx++) {
                if (document.getElementById("anchor" + (idx + 1))) {
                    document.getElementById("anchor" + (idx + 1)).addEventListener("click", function () {
                        let name = document.getElementById("anchor" + (idx + 1)).getAttribute('tooltip_id');
                        let modalType, type;
                        if (name === 'Disqualifying Event') {
                            modalType = 'disqualifingEvent';
                            type = 'disqualifingEventModal'
                        } else if (name === 'Regulation D of Rule 506(d)') {
                            modalType = 'actModalWindow';
                            type = 'regulation506d'
                        }
                        else if (name === 'Securities Act') {
                            modalType = 'actModalWindow';
                            type = 'security'
                        }
                        PubSub.publish('openModal', { investorType: obj.investorType, investorSubType: obj.investorSubType, modalType: modalType, type: type });
                    });
                }
            }
        }
    }

    getJsonData() {
        this.Fsnethttp.getJson('investorInformation').then(result => {
            this.setState({
                jsonData: result.data
            })
        });

    }
    //Call investor sub types
    investorSubTypes() {
        this.open();
        this.Fsnethttp.getInvestorSubTypes().then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    investorSubTypes: result.data
                }, () => {
                    if (this.state.lpsubscriptionTotalObj.mailingAddressCountry) {
                        this.getStatesByCountry(this.state.lpsubscriptionTotalObj.mailingAddressCountry);
                    }
                    this.checkInvestorSubType(this.state.lpsubscriptionTotalObj)
                    this.updateInvestorInputFields(this.state.lpsubscriptionTotalObj)
                })
            }
        })
            .catch(error => {
                this.close();
            });
    }

    //Call investor sub types for trust
    investorTrustSubTypes() {

        this.open();
        this.Fsnethttp.getInvestorTrustSubTypes().then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    investorTrustSubTypes: result.data
                })
            }
        })
            .catch(error => {
                this.close();
            });
    }

    //Call investor sub types for trust
    getAllCountries() {

        this.open();
        this.Fsnethttp.getAllCountries().then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    countriesList: result.data
                })
            }
        })
            .catch(error => {
                this.close();
            });
    }

    //Call investor sub types for trust
    getUSStates() {
        this.open();
        this.Fsnethttp.getUSStates().then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    usStatesList: result.data
                })
            }
        })
            .catch(error => {
                this.close();
            });
    }

    getStatesByCountry(value) {
        this.open();
        return this.Fsnethttp.getStates(value).then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    statesList: result.data,
                }, () => {
                    //Senegal Country has no states so making it as valid.
                    if (result.data.length == 0 && this.state.mailingAddressCountry == 192) {
                        this.setState({
                            mailingAddressStateValid: true,
                            mailingAddressState: 1,
                            mailingAddressStateTouched: true
                        })
                    }
                })
            }
        })
            .catch(error => {
                this.close();
            });
    }


    //Get countries or states based on sub types selection
    //isus value is taken to check whether investor sub type belongs to US or  nonUS
    jurisdictionTypes(isus, value) {


        let url = 'getAllCountries'
        if (isus == 1) {
            url = 'getUSStates'
        }
        if (value === 'otherEntity') {
            // url = 'getAllCountries/1'
            url = 'getAllCountries'
        }
        this.Fsnethttp.getJurisdictionTypes(url).then(result => {
            if (result.data) {
                this.setState({
                    investorJurisdictionTypes: result.data,
                    investorSubTypeValue: isus
                })
            }
        })
            .catch(error => {
                this.close();
            });
    }

    //Get the fund data
    getSubscriptionDetails(id) {
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    this.setState({
                        lpsubscriptionTotalObj: this.FsnetUtil.decodeObj(result.data.data),
                        email: result.data.data.email,
                        investorType: result.data.data.investorType ? result.data.data.investorType : 'LLC',
                        investorSubType: result.data.data.investorSubType ? result.data.data.investorSubType : 0,
                        fundId: result.data.data.fundId
                    }, () => {
                        // this.addedQuestionsValidations();
                        this.addClickEvent(result.data.data);
                        this.openPreviousFundModal('url')
                    })
                    if (result.data.data.investorType === 'Individual') {
                        this.updateIndividualData(this.FsnetUtil.decodeObj(result.data.data))
                    } else if (result.data.data.investorType === 'LLC') {
                        this.updateLLCData(this.FsnetUtil.decodeObj(result.data.data))
                    } else if (result.data.data.investorType === 'Trust') {
                        this.updateTrustData(this.FsnetUtil.decodeObj(result.data.data))
                    }
                }
            })
                .catch(error => {
                    this.close();
                    //this.props.history.push('/dashboard');
                });
        }
    }

    //Update Trust data when investor type is LLC
    updateTrustData(data) {
        this.investorTrustSubTypes();
        let obj = data;
        obj['currentInvestorInfoPageNumber'] = 1;
        obj['currentPageCount'] = 1;
        obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
        PubSub.publish('investorData', obj);
        this.setState({
            lpsubscriptionTotalObj: data,
        }, () => {
            if (this.state.investorType == 'Trust') {
                if (this.state.lpsubscriptionTotalObj.mailingAddressCountry) {
                    this.getStatesByCountry(this.state.lpsubscriptionTotalObj.mailingAddressCountry);
                }
                if (this.state.lpsubscriptionTotalObj.countryResidenceId) {
                    this.getUSStates(this.state.lpsubscriptionTotalObj.countryResidenceId)
                }
            }
            this.updateInvestorInputFields(this.state.lpsubscriptionTotalObj)
        })
    }

    //Update individual data when investor type is individual
    updateIndividualData(data) {
        let obj = data;
        obj['currentInvestorInfoPageNumber'] = 1;
        obj['currentPageCount'] = 1;
        obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
        //This is for to send data to left nav component
        PubSub.publish('investorData', obj);
        this.setState({
            lpsubscriptionTotalObj: data,
        }, () => {
            if (this.state.lpsubscriptionTotalObj.mailingAddressCountry) {
                this.getStatesByCountry(this.state.lpsubscriptionTotalObj.mailingAddressCountry);
            }
            if (this.state.lpsubscriptionTotalObj.countryResidenceId) {
                this.getUSStates(this.state.lpsubscriptionTotalObj.countryResidenceId)
            }
            this.checkInvestorSubType(this.state.lpsubscriptionTotalObj)
            this.updateInvestorInputFields(this.state.lpsubscriptionTotalObj)
        })
    }




    //Update LLC data when investor type is LLC
    updateLLCData(data) {
        // this.investorSubTypes();
        let obj = data;
        obj['currentInvestorInfoPageNumber'] = 1;
        obj['currentPageCount'] = 1;
        obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
        PubSub.publish('investorData', obj);
        this.setState({
            lpsubscriptionTotalObj: data,
        }, () => {
            this.investorSubTypes();

        })
    }

    //Call Jurisdiction types for the first time when investorSubTypes value is exists
    checkInvestorSubType(data) {
        if (data.investorSubType !== null) {
            if (this.state.investorSubTypes.length > 0) {
                for (let index of this.state.investorSubTypes) {
                    if (index['id'] === this.state.lpsubscriptionTotalObj.investorSubType) {
                        let isus = index['isUS'];
                        this.jurisdictionTypes(isus, this.state.lpsubscriptionTotalObj.investorSubType);
                    }
                }
            }
        } else if (this.state.lpsubscriptionTotalObj.otherInvestorSubType !== null && this.state.lpsubscriptionTotalObj.otherInvestorSubType !== undefined) {
            this.jurisdictionTypes(0, 'otherEntity');
        }
    }

    //Update all fileds to state 
    updateInvestorInputFields(data) {
        if (data) {
            let keys = [];
            for (let index in data) {
                keys.push(index);
            }
            if (keys) {
                for (let idx of keys) {
                    if (idx === 'typeOfLegalOwnership') {
                        this.getOwnerShipName(data[idx])
                    }
                    this.setState({
                        [idx]: data[idx]
                    }, () => {
                        if (this.state.otherInvestorSubType !== null && this.state.otherInvestorSubType !== undefined) {
                            this.setState({
                                investorSubType: 'otherEntity'
                            }, () => {
                                if (this.state.investorSubType == 'otherEntity' && this.state.investorType == 'Trust') {
                                    this.setState({
                                        investorSubType: this.state.investorTrustSubTypes[0].id
                                    }, () => {
                                        this.validateTrustPages();
                                    })
                                }
                            })
                        }
                        if (idx == 'mailingAddressPhoneNumber' && data['mailingAddressPhoneNumber'] == null || data['mailingAddressPhoneNumber'] == undefined || data['mailingAddressPhoneNumber'] == '') {
                            this.setState({
                                mailingAddressPhoneNumber: JSON.parse(reactLocalStorage.get('userData')).cellNumber
                            })
                        }

                        if (this.state.investorType == 'Trust' || this.state.investorType == 'LLC') {
                            if (idx == 'isEntityTaxExemptForUSFederalIncomeTax' || idx == 'isTrust501c3' || idx == 'IstheEntityRequiredFOIAWorldwide' || idx == 'releaseInvestmentEntityRequired' || idx == 'isSubjectToDisqualifyingEvent') {
                                if (data[idx] == null || data[idx] == undefined || data[idx] == '') {
                                    this.setState({
                                        [idx]: false,
                                        [idx + 'Valid']: true
                                    }, () => {
                                        if (this.state.investorType == 'Trust') {
                                            this.validateTrustPages();
                                        }
                                    })
                                }
                            }
                            if (idx == 'otherInvestorAttributes' && data[idx] && data[idx].length > 0) {
                                if (this.state.investorType == 'LLC') {
                                    if (data[idx] && data[idx].length > 0) {
                                        this.setState({
                                            [idx]: data[idx],
                                            [idx + 'Valid']: true
                                        }, () => {
                                            let name = 'otherInvestorAttributes' + 'Valid'
                                            let dataObj = {
                                                [name]: true
                                            };
                                            this.updateStateParams(dataObj);
                                        })
                                    } else {
                                        this.setState({
                                            [idx]: data[idx],
                                            otherInvestorAttributesValid: false
                                        }, () => {
                                            let name = 'otherInvestorAttributes' + 'Valid'
                                            let dataObj = {
                                                [name]: false
                                            };
                                            this.updateStateParams(dataObj);
                                        })
                                    }
                                } else {
                                    this.setState({
                                        [idx]: data[idx],
                                        [idx + 'Valid']: true
                                    }, () => {
                                        if (this.state.investorType == 'Trust') {
                                            this.validateTrustPages();
                                        }
                                    })
                                }
                            }
                        }

                        if (this.state.investorType == 'Trust') {
                            this.validateTrustPages();
                        } else {
                            this.enableNextButtonStep1();
                        }
                    })
                }
            }
        }
    }

    //Enable next icon based on mandatory fileds.
    //Mandatory fields will be changed for investor type is individual and LLC
    enableNextButtonStep1() {
        let mandatoryFileds = [];
        if (this.state.investorType === 'Individual') {
            mandatoryFileds = ['investorType', 'areYouSubscribingAsJointIndividual', 'isSubjectToDisqualifyingEvent'];
        } else if (this.state.investorType === 'LLC') {
            mandatoryFileds = ['investorType', 'investorSubType', 'countryDomicileId', 'entityName', 'isEntityTaxExemptForUSFederalIncomeTax', 'releaseInvestmentEntityRequired', 'isSubjectToDisqualifyingEvent'];
        }
        let validCount = 0;
        for (let field of mandatoryFileds) {
            if (this.state[field] === null || this.state[field] === '' || this.state[field] === undefined) {
                this.setState({
                    investorPageValid: false,
                    [field + 'Valid']: false
                })
                // break;
            } else if (field == 'areYouSubscribingAsJointIndividual' && this.state['areYouSubscribingAsJointIndividual'] == true && !this.state['typeOfLegalOwnership']) {
                this.setState({
                    investorPageValid: false,
                    [field + 'Valid']: false
                })
            } else {
                this.setState({
                    investorPageValid: true,
                    [field + 'Valid']: true
                })
                validCount++;
            }
        }
        // //console.log('valid count::::', validCount, mandatoryFileds.length);
        if (validCount != mandatoryFileds.length) {
            this.setState({
                investorPageValid: false
            }
            )
        }
    }

    //Enable next icon based on mandatory fileds for investor information2 page.
    //Mandatory fields will be changed for investor type is individual and LLC
    enableNextButtonStep2() {
        let mandatoryFileds = ['mailingAddressStreet', 'mailingAddressCity', 'mailingAddressState', 'mailingAddressZip', 'mailingAddressPhoneNumber'];
        for (let field of mandatoryFileds) {
            if (this.state[field] === null && this.state[field] === '' && this.state[field] === undefined) {
                this.setState({
                    investorPageValid: false,
                    [field + 'Valid']: false
                })
                break;
            } else {
                this.setState({
                    investorPageValid: true,
                    [field + 'Valid']: true
                })
            }
        }
    }

    //Update owner name based on the typeOfLegalOwnershipName
    getOwnerShipName(name) {
        if (name === 'tenantsInCommon') {
            this.setState({
                typeOfLegalOwnershipName: 'Tenants in Common'
            })
        } else if (name === 'jointTenants') {
            this.setState({
                typeOfLegalOwnershipName: 'Joint Tenants'
            })
        } else if (name === 'communityProperty') {
            this.setState({
                typeOfLegalOwnershipName: 'Community Property'
            })
        } else if (name === 'Other') {
            this.setState({
                typeOfLegalOwnershipName: 'Other'
            })
        }
    }

    //ProgressLoader : Close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () => {
        this.setState({ showModal: true });
    }

    //Open modal
    openStartConfirmationModal = () => {
        this.setState({ showConfirmationModal: true });
    }

    //Close modal
    closeConfirmationModal = () => {
        this.setState({ showConfirmationModal: false });
    }

    //On change event for all input fileds
    investorHandleChangeEvent = (event, type, radioTypeName, blur) => {
        this.setState({
            isFormChanged: true
        })
        let key = type;
        let value;
        if (type === 'mailingAddressZip') {
            const re = /^[0-9\b]+$/;
            // if value is not blank, then test the regex
            if (event.target.value.trim() !== '') {
                if (!re.test(event.target.value.trim()) || event.target.value.trim().length > 10) {
                    return true
                }
            }
        }

        if (blur !== 'cellNumberBlur') {
            if (key === 'mailingAddressPhoneNumber') {
                value = event
            } else {
                if (event.target.value.trim() === '') {
                    value = event.target.value.trim()
                } else {
                    value = event.target.value
                }
            }
        } else {
            value = event.target.value
        }
        let dataObj = {};
        switch (type) {
            case 'radio':
                if (blur === 'investorType') {
                    let obj = this.state.lpsubscriptionTotalObj;
                    obj['investorType'] = radioTypeName;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    obj['currentPage'] = 'investorInfo';
                    obj['currentPageCount'] = 1;
                    PubSub.publish('investorData', obj);
                    if (radioTypeName == 'Trust') {
                        this.setState({
                            investorSubType: this.state.investorTrustSubTypes.length ? this.state.investorTrustSubTypes[0].id : 0
                        }, () => {
                            this.validateTrustPages();
                        })
                    } else {
                        this.setState({
                            investorSubType: this.state.lpsubscriptionTotalObj.investorSubType
                        })
                    }
                }
                if (blur === 'typeOfLegalOwnership') {
                    this.getOwnerShipName(radioTypeName)
                }
                if (blur === 'isEntityUS501c3') {
                    this.setState({
                        isEntityUS501c3Msz: ''
                    })
                }
                if (blur === 'areYouSubscribingAsJointIndividual') {
                    if (radioTypeName == false) {
                        this.setState({
                            legalTitle: '',
                            typeOfLegalOwnership: '',
                        })
                    }
                }
                if (blur === 'isEntityTaxExemptForUSFederalIncomeTax') {
                    this.setState({
                        isEntityTaxExemptForUSFederalIncomeTaxMsz: ''
                    })
                }
                if (blur === 'istheEntityFundOfFundsOrSimilarTypeVehicle') {
                    this.setState({
                        entityFundOfFundsOrSimilarTypeVehicleMsz: ''
                    })
                }
                this.setState({
                    [blur]: radioTypeName,
                }, () => {
                    if (blur === 'investorType') {
                        this.enableNextButtonStep1();
                        this.enableDisableInvestorDetailsButton();
                        let name = blur + 'Valid'
                        dataObj = {
                            [name]: true
                        };
                        this.updateStateParams(dataObj);
                    }
                })
                if (blur !== 'investorType') {
                    let name = blur + 'Valid'
                    dataObj = {
                        [name]: true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case key:
                if (type === 'investorSubType' || type === 'countryDomicileId' || type == 'stateDomicileId') {
                    let isus = event.target[event.target.selectedIndex].getAttribute('isus');
                    if (type === 'investprSubType') {
                        this.setState({
                            investorSubTypeValue: isus
                        })
                    }
                    if (value === '0') {
                        value = ''
                    } else {
                        if (type === 'investorSubType') {
                            this.jurisdictionTypes(isus, event.target.value);
                            this.setState({
                                stateDomicileId: 0,
                                stateDomicileIdValid: false,
                                countryDomicileId: 0,
                                countryDomicileIdValid: false,
                                // stateResidence:0,
                                // stateResidenceValid: false,
                                // countryResidence:0,
                                // countryResidenceValid: false,
                            }, () => {
                                this.updateStateParams(dataObj);
                            })
                        }
                    }
                }
                if (value === '' || value === undefined) {
                    this.setState({
                        [key + 'Msz']: this.Constants[radioTypeName],
                        [key + 'Valid']: false,
                        [key + 'Border']: true,
                        [key]: ''
                    })
                    let name = key + 'Valid'
                    dataObj = {
                        [name]: false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        [key + 'Msz']: '',
                        [key + 'Valid']: true,
                        [key + 'Border']: false,
                        [key]: value
                    })
                    let name = key + 'Valid'
                    dataObj = {
                        [name]: true
                    };
                    this.updateStateParams(dataObj);
                }
                break;

            default:
                break;
        }
    }

    //Show investor information step1 page
    proceedToBack = () => {
        this.setState({ hidePreviousFundBtn: false })
        if (this.state.investorType === 'Individual') {
            let obj = this.state.lpsubscriptionTotalObj;
            obj['currentInvestorInfoPageNumber'] = 1;
            obj['currentPage'] = 'investorInfo';
            obj['currentPageCount'] = 1;
            PubSub.publish('investorData', obj);
            this.setState({
                showIndividualStep1: true,
                enableLeftIcon: false,
                investorPageValid: true,
                showInvestorType: true,
                isFormChanged: false,
            })
        } else if (this.state.investorType === 'LLC') {
            this.setState({
                showLLCInvestorInfoPage1: true,
                enableLeftIcon: false,
                investorPageValid: true,
                showInvestorType: true,
                isFormChanged: false,
            })
        } else if (this.state.investorType === 'Trust') {
            let obj = this.state.lpsubscriptionTotalObj;
            obj['currentInvestorInfoPageNumber'] = 1;
            obj['currentPage'] = 'investorInfo';
            obj['currentPageCount'] = 1;
            PubSub.publish('investorData', obj);
            if (this.state.investorSubType == 9) {
                this.setState({
                    showTrustRevocableInvestorPage1: true,
                    enableLeftIcon: false,
                    investorPageValid: true,
                    showInvestorType: true,
                    isFormChanged: false,
                }, () => {
                    this.updateTrustData(this.state.lpsubscriptionTotalObj)
                })
            } else {
                this.setState({
                    showTrustIrrevocableInvestorPage1: true,
                    enableLeftIcon: false,
                    investorPageValid: true,
                    showInvestorType: true,
                    isFormChanged: false,
                }, () => {
                    this.updateTrustData(this.state.lpsubscriptionTotalObj)
                })
            }

        }
        setTimeout(() => {
            this.FsnetUtil.setLeftNavHeight();
        }, 100);
    }

    //Proceed to next for all investor types
    proceedToNext = () => {
        if (this.state.investorType === 'Individual') {
            this.invidualNextStep();
        } else if (this.state.investorType === 'LLC') {
            this.LLCNextStep();
        } else if (this.state.investorType === 'Trust') {
            this.trustNextStep();
        }
    }


    getTitle = () => {
        let name = '';
        if (this.state.investorType == 'LLC') {
            name = this.state.entityName
        } else if (this.state.investorType == 'Trust') {
            name = this.state.trustName
        } else if (this.state.investorType == 'Individual' && this.state.areYouSubscribingAsJointIndividual) {
            name = this.state.legalTitle
        }
        return name ? name : null
    }


    checkUniqueTitle = () => {
        let postobj = { investorType: this.state.investorType == 'Individual' && this.state.areYouSubscribingAsJointIndividual ? 'jointIndividual' : this.state.investorType, subscriptionId: this.state.lpsubscriptionTotalObj.id, fundId: this.state.fundId, title: this.getTitle(), lpId: this.state.lpsubscriptionTotalObj.lpId }
        this.open();
        this.Fsnethttp.titleUniqueCheck(postobj).then(result => {
            this.close();
            if (result.data.allowed && !result.data.investmentAvailableForRestore) {
                this.openStartConfirmationModal();
            } else if (result.data.investmentAvailableForRestore) {
                this.openinvestmentAvailableForRestoreModal();
            } else {
                this.openTitleUniqueCheckModal();
            }
        })
            .catch(error => {
                this.close();
            });
    }

    //Open modal if investor type is individual  in investor information step1
    //For step2 call the submitStep2Details method.
    invidualNextStep() {
        if (this.state.showIndividualStep1) {
            let value = this.checkCountryandStateResidenceValidations();
            if (!value) {
                this.checkLegalTitleForIndividual();
            }
        } else {
            this.submitStep2Details()
        }
    }

    //Submit investor info step2 details.
    submitStep2Details() {
        let postobj = { investorType: this.state.investorType, subscriptionId: this.state.lpsubscriptionTotalObj.id, step: 2, mailingAddressStreet: this.state.mailingAddressStreet, mailingAddressCountry: this.state.mailingAddressCountry, mailingAddressCity: this.state.mailingAddressCity, mailingAddressState: this.state.mailingAddressState, mailingAddressZip: this.state.mailingAddressZip, mailingAddressPhoneNumber: this.state.mailingAddressPhoneNumber, isFormChanged: this.state.isFormChanged }
        this.open();

        this.Fsnethttp.updateLpSubscriptionDetails(postobj).then(result => {
            this.close();
            if (result.data) {
                this.props.history.push('/lp/AccreditedInvestor/' + this.FsnetUtil._encrypt(this.state.lpsubscriptionTotalObj.id));
            }
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        investorInfoErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        investorInfoErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });

        // }
    }

    //For llc call LLC step 1 and step2 methods.
    LLCNextStep() {
        if (this.state.showLLCInvestorInfoPage1) {
            let value = this.llcStep1Validations();
            if (!value) {
                if (['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) == -1) {
                    this.checkUniqueTitle();
                } else {
                    this.openStartConfirmationModal();
                }
            }
        } else {
            this.submitStep2Details()
        }
    }

    submitTrustForm(stepType) {
        let postInvestorObj;
        postInvestorObj = {
            subscriptionId: this.state.lpsubscriptionTotalObj.id,
            step: this.state[stepType] ? 1 : 2,
            fundId: this.state.fundId,
            investorType: this.state.investorType,
            investorSubType: this.state.investorSubType,
            numberOfGrantorsOfTheTrust: this.state.numberOfGrantorsOfTheTrust ? this.state.numberOfGrantorsOfTheTrust : 0,
            countryResidenceId: this.state.countryResidenceId ? this.state.countryResidenceId : 0,
            stateResidenceId: this.state.stateResidenceId ? this.state.stateResidenceId : 0,
            trustName: this.state.trustName,
            entityName: this.state.entityName,
            entityTitle: this.state.entityTitle,
            isEntityTaxExemptForUSFederalIncomeTax: this.state.isEntityTaxExemptForUSFederalIncomeTax || false,
            releaseInvestmentEntityRequired: this.state.releaseInvestmentEntityRequired || false,
            isSubjectToDisqualifyingEvent: this.state.isSubjectToDisqualifyingEvent || false,
            fundManagerInfo: this.state.fundManagerInfo,
            isEntityUS501c3: this.state.isEntityUS501c3 || false,
            isTrust501c3: this.state.isTrust501c3 || false,
            otherInvestorAttributes: this.state.otherInvestorAttributes.join(','),
            mailingAddressStreet: this.state.mailingAddressStreet,
            mailingAddressCity: this.state.mailingAddressCity,
            mailingAddressCountry: this.state.mailingAddressCountry,
            mailingAddressState: this.state.mailingAddressState,
            mailingAddressZip: this.state.mailingAddressZip,
            legalTitleDesignation: this.state.legalTitleDesignation,
            mailingAddressPhoneNumber: this.state.mailingAddressPhoneNumber,
            isFormChanged: this.state.isFormChanged
        }
        if (this.state.releaseInvestmentEntityRequired) {
            postInvestorObj['indirectBeneficialOwnersSubjectFOIAStatutes'] = this.state.indirectBeneficialOwnersSubjectFOIAStatutes;
        }
        this.open();

        this.Fsnethttp.updateLpSubscriptionDetails(postInvestorObj).then(result => {
            this.close();
            if (result.data) {
                let obj = this.state.lpsubscriptionTotalObj;
                obj['currentInvestorInfoPageNumber'] = 2;
                obj['currentPage'] = 'investorInfo';
                obj['currentPageCount'] = 1;
                obj['mailingAddressCountry'] = obj.mailingAddressCountry ? obj.mailingAddressCountry : this.state.countryResidenceId;
                obj['mailingAddressState'] = obj.mailingAddressState ? obj.mailingAddressState : this.state.stateResidenceId;
                PubSub.publish('investorData', obj);
                this.setState({
                    lpsubscriptionTotalObj: this.FsnetUtil.decodeObj(result.data.data)
                }, () => {
                    if (this.state[stepType] == 1) {
                        if (obj.mailingAddressCountry) {
                            this.getStatesByCountry(obj.mailingAddressCountry);
                        }
                        this.setState({
                            isFormChanged: false,
                            showInvestorType: false,
                            enableLeftIcon: true,
                            investorPageValid: false,
                            [stepType]: !this.state[stepType],
                            investorInfoErrorMsz: '',
                            mailingAddressCountry: this.state.mailingAddressCountry != null && this.state.mailingAddressCountry != '' ? this.state.mailingAddressCountry : this.state.countryResidenceId,
                            mailingAddressState: this.state.mailingAddressState != null && this.state.mailingAddressState != '' ? this.state.mailingAddressState : this.state.stateResidenceId
                        }, () => {
                            this.validateTrustPages();
                        });
                    }
                    else {
                        this.props.history.push('/lp/AccreditedInvestor/' + this.FsnetUtil._encrypt(this.state.lpsubscriptionTotalObj.id));
                    }
                });
                this.closeConfirmationModal();
            }
        })
            .catch(error => {
                this.close();
                this.closeConfirmationModal();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        investorInfoErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        investorInfoErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });
    }


    trustValidation() {
        if (this.state.releaseInvestmentEntityRequired === true) {
            if (this.state.indirectBeneficialOwnersSubjectFOIAStatutes === '' || this.state.indirectBeneficialOwnersSubjectFOIAStatutes === null) {
                this.setState({
                    indirectBeneficialOwnersSubjectFOIAStatutesMsz: this.Constants.INDIRECT_BENIFICIAL_REQUIRED
                })
                return true;
            }

        }
        if (this.state.isEntityTaxExemptForUSFederalIncomeTax === true) {
            if (this.FsnetUtil._iNull(this.state.isTrust501c3)) {
                this.setState({
                    isTrust501c31Msz: this.Constants.TRUST_501_REQUIRED
                })
                return true;
            }

        }
        let value = this.checkCountryandStateResidenceValidations();
        return value ? true : false
    }

    //For trust call LLC step 1 and step2 methods.
    trustNextStep() {
        if (this.state.investorSubType == 9) {
            if (this.state.showTrustRevocableInvestorPage1) {
                let valid = this.trustValidation();
                if (!valid) {
                    if (['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) == -1) {
                        this.checkUniqueTitle();
                    } else {
                        this.openStartConfirmationModal();
                    }
                }
            } else {
                this.submitTrustForm('showTrustRevocableInvestorPage1');
            }
        } else {
            if (this.state.showTrustIrrevocableInvestorPage1) {
                let valid = this.trustValidation();
                if (!valid) {
                    if (['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) == -1) {
                        this.checkUniqueTitle();
                    } else {
                        this.openStartConfirmationModal();
                    }
                }
            } else {
                this.submitTrustForm('showTrustIrrevocableInvestorPage1');
            }
        }

    }


    confirmSubmit = () => {
        this.setState({ hidePreviousFundBtn: true })
        if (this.state.investorType && this.state.investorType.toLowerCase() == 'individual') {
            this.submitInvestorInfoStep1Details();
        }
        if (this.state.investorType && this.state.investorType.toLowerCase() == 'llc') {
            this.submitLLCInfoStep1Details();
        }
        if (this.state.investorType && this.state.investorType.toLowerCase() == 'trust') {
            this.submitTrustForm(this.state.investorSubType == 9 ? 'showTrustRevocableInvestorPage1' : 'showTrustIrrevocableInvestorPage1');
        }
    }

    llcStep1Validations() {
        if (this.state.investorSubType === 'otherEntity') {
            if (!this.state.otherInvestorSubType) {
                this.setState({
                    otherInvestorSubTypeBorder: true,
                    otherInvestorSubTypeMsz: this.Constants.ENTITY_TYPE_REQUIRED
                })
                return true;
            }
        }

        if (this.FsnetUtil._iNull(this.state.istheEntityFundOfFundsOrSimilarTypeVehicle)) {
            this.setState({
                entityFundOfFundsOrSimilarTypeVehicleMsz: this.Constants.SIMILAR_TYPE_VEHICLE_REQUIRED
            })
            return true;
        }
        if (this.state.isEntityTaxExemptForUSFederalIncomeTax === true) {
            if (this.FsnetUtil._iNull(this.state.isEntityUS501c3)) {
                this.setState({
                    isEntityUS501c3Msz: this.Constants.ENTITY_US_501_REQUIRED
                })
                return true;
            }
        }
        if (this.state.istheEntityFundOfFundsOrSimilarTypeVehicle === true) {
            if (this.FsnetUtil._iNull(this.state.isEntityTaxExemptForUSFederalIncomeTax)) {
                this.setState({
                    isEntityTaxExemptForUSFederalIncomeTaxMsz: this.Constants.ENTITY_TAX_REQUIRED
                })
                return true;
            }

        }
        if (this.state.releaseInvestmentEntityRequired === true) {
            if (this.FsnetUtil._iNull(this.state.indirectBeneficialOwnersSubjectFOIAStatutes)) {
                this.setState({
                    indirectBeneficialOwnersSubjectFOIAStatutesMsz: this.Constants.INDIRECT_BENIFICIAL_REQUIRED
                })
                return true;
            }

        }

        if (parseInt(this.state.investorSubTypeValue) == 0) {
            if (!this.state.countryDomicileId || this.state.countryDomicileId == 0) {
                this.setState({
                    countryDomicileIdMsz: this.Constants.SELECT_COUNTRY_REQUIRED
                })
                return true;
            }

        }

        if ((this.state.countryDomicileId == 231 && this.state.investorSubTypeValue == 0) || (this.state.investorSubTypeValue == 1 && (this.state.stateDomicileId === 0 || !this.state.stateDomicileId))) {
            if (!this.state.stateDomicileId || this.state.stateDomicileId == 0) {
                this.setState({
                    stateDomicileIdMsz: this.Constants.SELECT_STATE_REQUIRED
                })
                return true;
            }
        }
        if (this.state.countryResidenceId == 231) {
            if (!this.state.stateResidenceId || this.state.stateResidenceId == 0) {
                this.setState({
                    stateResidenceIdMsz: this.Constants.STATE_RESIDENCE_REQUIRED
                })
                return true;
            }
        }
        let value = this.checkCountryandStateResidenceValidations();
        return value ? true : false
    }

    //Submit LLC Step1 Details
    submitLLCInfoStep1Details = () => {
        let postInvestorObj = { subscriptionId: this.state.lpsubscriptionTotalObj.id, step: 1, fundId: this.state.fundId, investorType: this.state.investorType, investorSubType: this.state.investorSubType, entityName: this.state.entityName, istheEntityFundOfFundsOrSimilarTypeVehicle: this.state.istheEntityFundOfFundsOrSimilarTypeVehicle, releaseInvestmentEntityRequired: this.state.releaseInvestmentEntityRequired, isSubjectToDisqualifyingEvent: this.state.isSubjectToDisqualifyingEvent || false, fundManagerInfo: this.state.fundManagerInfo, isEntityTaxExemptForUSFederalIncomeTax: this.state.isEntityTaxExemptForUSFederalIncomeTax, otherInvestorAttributes: this.state.otherInvestorAttributes.join(','), isFormChanged: this.state.isFormChanged }
        if (this.state.investorSubType === 'otherEntity') {
            postInvestorObj['investorSubType'] = null;
            postInvestorObj['otherInvestorSubType'] = this.state.otherInvestorSubType;
        } else {
            postInvestorObj['otherInvestorSubType'] = null;
        }
        if (this.state.istheEntityFundOfFundsOrSimilarTypeVehicle) {
            postInvestorObj['isEntityTaxExemptForUSFederalIncomeTax'] = this.state.isEntityTaxExemptForUSFederalIncomeTax;
        }
        if (this.state.releaseInvestmentEntityRequired) {
            postInvestorObj['indirectBeneficialOwnersSubjectFOIAStatutes'] = this.state.indirectBeneficialOwnersSubjectFOIAStatutes;
        }
        if (this.state.isEntityTaxExemptForUSFederalIncomeTax === true) {
            postInvestorObj['isEntityUS501c3'] = this.state.isEntityUS501c3;
        }

        postInvestorObj['countryDomicileId'] = this.state.investorSubTypeValue == 0 ? this.state.countryDomicileId : 0;
        postInvestorObj['countryResidenceId'] = this.state.investorSubTypeValue == 0 ? this.state.countryResidenceId : 0;
        postInvestorObj['stateDomicileId'] = (this.state.investorSubTypeValue == 0 && this.state.countryDomicileId == 231) || this.state.investorSubTypeValue == 1 ? this.state.stateDomicileId : 0;
        postInvestorObj['stateResidenceId'] = (this.state.investorSubTypeValue == 0 && this.state.countryResidenceId == 231) || this.state.investorSubTypeValue == 1 ? this.state.stateResidenceId : 0;

        this.open();

        this.Fsnethttp.updateLpSubscriptionDetails(postInvestorObj).then(result => {
            this.close();
            if (result.data) {
                let obj = this.state.lpsubscriptionTotalObj;
                obj['currentInvestorInfoPageNumber'] = 2;
                obj['currentPage'] = 'investorInfo';
                obj['currentPageCount'] = 1;
                PubSub.publish('investorData', obj);
                this.setState({
                    showLLCInvestorInfoPage1: false,
                    showInvestorType: false,
                    enableLeftIcon: true,
                    investorPageValid: false,
                    isFormChanged: false,
                })
                this.validateTrustPages();
                this.closeConfirmationModal();
            }
        })
            .catch(error => {
                this.close();
            });
    }

    checkLegalTitleForIndividual() {
        if (this.state.areYouSubscribingAsJointIndividual && (this.state.legalTitle === null || this.state.legalTitle === '' || this.state.legalTitle === undefined)) {
            this.setState({
                legalTitleMsz: this.Constants.LEGAL_TITLE_REQUIRED,
                legalTitleBorder: true
            })

        } else if (this.state.releaseInvestmentEntityRequired === true) {
            if (this.state.indirectBeneficialOwnersSubjectFOIAStatutes === '' || this.state.indirectBeneficialOwnersSubjectFOIAStatutes === null) {
                this.setState({
                    indirectBeneficialOwnersSubjectFOIAStatutesMsz: this.Constants.INDIRECT_BENIFICIAL_REQUIRED
                })
            } else {
                if (['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) == -1) {
                    this.checkUniqueTitle();
                } else {
                    this.openStartConfirmationModal();
                }

            }

        } else {
            if (['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) == -1) {
                this.checkUniqueTitle();
            } else {
                this.openStartConfirmationModal();
            }
        }
    }
    //Submit Individual Step1 Details
    submitInvestorInfoStep1Details = () => {
        let postInvestorObj = {
            subscriptionId: this.state.lpsubscriptionTotalObj.id, step: 1, fundId: this.state.fundId, investorType: this.state.investorType, areYouSubscribingAsJointIndividual: this.state.areYouSubscribingAsJointIndividual, releaseInvestmentEntityRequired: this.state.releaseInvestmentEntityRequired, isSubjectToDisqualifyingEvent: this.state.isSubjectToDisqualifyingEvent || false, fundManagerInfo: this.state.fundManagerInfo, isFormChanged: this.state.isFormChanged, countryResidenceId: this.state.countryResidenceId ? this.state.countryResidenceId : 0,
            stateResidenceId: this.state.stateResidenceId ? this.state.stateResidenceId : 0
        }

        postInvestorObj['legalTitle'] = this.state.areYouSubscribingAsJointIndividual ? this.state.legalTitle : null
        postInvestorObj['typeOfLegalOwnership'] = this.state.areYouSubscribingAsJointIndividual ? this.state.typeOfLegalOwnership : null

        if (this.state.releaseInvestmentEntityRequired) {
            postInvestorObj['indirectBeneficialOwnersSubjectFOIAStatutes'] = this.state.indirectBeneficialOwnersSubjectFOIAStatutes;
        }

        this.open();

        this.Fsnethttp.updateLpSubscriptionDetails(postInvestorObj).then(result => {
            this.close();
            if (result.data) {
                let obj = this.state.lpsubscriptionTotalObj;
                obj['currentInvestorInfoPageNumber'] = 2;
                obj['currentPage'] = 'investorInfo';
                obj['currentPageCount'] = 1;
                PubSub.publish('investorData', obj);
                this.setState({
                    showIndividualStep1: false,
                    enableLeftIcon: true,
                    showInvestorType: false,
                    hidePreviousFundBtn: true,
                    isFormChanged: false,
                    mailingAddressCountry: this.state.mailingAddressCountry != 0 && this.state.mailingAddressCountry != null ? this.state.mailingAddressCountry : this.state.countryResidenceId,
                    mailingAddressState: this.state.mailingAddressState != 0 && this.state.mailingAddressState != null ? this.state.mailingAddressState : this.state.stateResidenceId,
                }, () => {
                    if (this.state.mailingAddressCountry) {
                        this.getStatesByCountry(this.state.mailingAddressCountry)
                    }
                })
                this.validateTrustPages();
                this.closeConfirmationModal();
            }
        })
            .catch(error => {
                this.close();
                this.setState({
                    showIndividualStep1: false,
                    enableLeftIcon: true
                })
                this.closeConfirmationModal();
            });
    }

    // Update state params values and login button visibility

    updateStateParams(updatedDataObject) {
        this.setState(updatedDataObject, () => {
            this.enableDisableInvestorDetailsButton();
        });
    }

    enableDisableInvestorDetailsButton() {
        let status;
        let validateValues = [];
        if (this.state.investorType === 'Individual') {
            if (this.state.showIndividualStep1) {
                status = (this.state.areYouSubscribingAsJointIndividualValid) ? true : false;
                validateValues = ['isSubjectToDisqualifyingEvent', 'areYouSubscribingAsJointIndividual'];
            } else {
                status = (this.state.mailingAddressCityValid && this.state.mailingAddressStreetValid && this.state.mailingAddressStateValid && this.state.mailingAddressZipValid) ? true : false;
                validateValues = ['mailingAddressCity', 'mailingAddressStreet', 'mailingAddressState', 'mailingAddressZip'];
            }
        } else if (this.state.investorType === 'LLC') {
            if (this.state.showLLCInvestorInfoPage1) {
                status = (this.state.investorSubTypeValid && this.state.entityNameValid && this.state.isEntityTaxExemptForUSFederalIncomeTaxValid && this.state.releaseInvestmentEntityRequiredValid) ? true : false;
                validateValues = ['investorSubType', 'entityName', 'isEntityTaxExemptForUSFederalIncomeTax', 'releaseInvestmentEntityRequired', 'isSubjectToDisqualifyingEvent'];
            } else {
                status = (this.state.mailingAddressCityValid && this.state.mailingAddressStreetValid && this.state.mailingAddressStateValid && this.state.mailingAddressZipValid) ? true : false;
                validateValues = ['mailingAddressCity', 'mailingAddressStreet', 'mailingAddressState', 'mailingAddressZip'];
            }
        }
        if (this.state.investorType != 'Trust') {
            let validCount = 0;
            for (let field of validateValues) {
                if (this.state[field] === null || this.state[field] === '' || this.state[field] === undefined || this.state[field] === 0) {
                    this.setState({
                        investorPageValid: false,
                        [field + 'Valid']: false
                    }, () => {
                        if (this.state.investorType == 'Individual') {
                            if (this.state.areYouSubscribingAsJointIndividual == true && !this.state.typeOfLegalOwnership) {
                                this.setState({
                                    investorPageValid: false
                                })
                            }
                        }
                    })
                    // break;
                } else {
                    if (field == 'countryResidenceId' && this.state[field] == 231 && (this.state['stateResidenceId'] === null || this.state['stateResidenceId'] === '' || this.state['stateResidenceId'] === undefined || this.state['stateResidenceId'] == 0)) {
                        this.setState({
                            investorPageValid: false,
                            [field + 'Valid']: false
                        }, () => {
                            if (this.state.investorType == 'Individual') {
                                if (this.state.areYouSubscribingAsJointIndividual == true && !this.state.typeOfLegalOwnership) {
                                    this.setState({
                                        investorPageValid: false
                                    })
                                }
                            }
                        })
                        // break;
                    } else if (field == 'otherInvestorAttributes' && this.state['otherInvestorAttributes'].length == 0) {
                        this.setState({
                            // investorPageValid: false,
                            [field + 'Valid']: false
                        }, () => {
                            if (this.state.investorType == 'Individual') {
                                if (this.state.areYouSubscribingAsJointIndividual == true && !this.state.typeOfLegalOwnership) {
                                    this.setState({
                                        investorPageValid: false
                                    })
                                }
                            }
                        })
                    } else {
                        this.setState({
                            investorPageValid: true,
                            [field + 'Valid']: true
                        }, () => {
                            if (this.state.investorType == 'Individual') {
                                if (this.state.areYouSubscribingAsJointIndividual == true && !this.state.typeOfLegalOwnership) {
                                    this.setState({
                                        investorPageValid: false
                                    })
                                }
                            }
                        })
                        validCount++;
                    }
                }
            }
            if (validCount != validateValues.length) {
                this.setState({
                    investorPageValid: false
                }, () => {
                    if (this.state.investorType == 'Individual') {
                        if (this.state.areYouSubscribingAsJointIndividual == true && !this.state.typeOfLegalOwnership) {
                            this.setState({
                                investorPageValid: false
                            })
                        }
                    }
                })
            }
            this.FsnetUtil.setLeftNavHeight();
        }
    }



    //  Trust Form Validations and Util Functions
    validateTrustPages() {

        let validValues = [];
        this.setState({
            investorInfoErrorMsz: ''
        })
        if (this.state.investorType == 'Trust') {
            if (this.state.investorSubType == 9) {
                if (this.state.showTrustRevocableInvestorPage1) {
                    validValues = ['trustName', 'countryResidenceId', 'isEntityTaxExemptForUSFederalIncomeTax', 'releaseInvestmentEntityRequired', 'isSubjectToDisqualifyingEvent'];
                } else {
                    validValues = ['mailingAddressCity', 'mailingAddressStreet', 'mailingAddressCountry', 'mailingAddressState', 'mailingAddressZip'];
                }
            }
            if (this.state.investorSubType == 10) {
                if (this.state.showTrustIrrevocableInvestorPage1) {
                    validValues = ['trustName', 'countryResidenceId', 'isEntityTaxExemptForUSFederalIncomeTax', 'releaseInvestmentEntityRequired', 'isSubjectToDisqualifyingEvent'];
                } else {
                    validValues = ['mailingAddressCity', 'mailingAddressStreet', 'mailingAddressCountry', 'mailingAddressState', 'mailingAddressZip', 'legalTitleDesignation'];
                }
            }
        } else {
            validValues = ['mailingAddressCity', 'mailingAddressStreet', 'mailingAddressCountry', 'mailingAddressState', 'mailingAddressZip'];
        }
        let validCount = 0;
        for (let field of validValues) {
            if (this.state[field] === null || this.state[field] === '' || this.state[field] === undefined || this.state[field] === 0) {
                this.setState({
                    investorPageValid: false,
                    [field + 'Valid']: false
                })
                break;
            } else {
                if (field == 'countryResidenceId' && this.state[field] == 231 && (this.state['stateResidenceId'] === null || this.state['stateResidenceId'] === '' || this.state['stateResidenceId'] === undefined || this.state['stateResidenceId'] == 0)) {
                    this.setState({
                        investorPageValid: false,
                        [field + 'Valid']: false
                    })
                    break;
                }
                else {
                    this.setState({
                        investorPageValid: true,
                        [field + 'Valid']: true
                    })
                    validCount++;
                }
            }
        }


    }

    //  Trust Form Validations and Util Functions
    trustOnChangeForm = (e, type, toValidate, key) => {
        let name, value;
        this.setState({
            investorInfoErrorMsz: '',
            isFormChanged: true
        })
        if (type == 'mobile') {
            name = key ? key : 'mailingAddressPhoneNumber';
            value = e ? e.trim() : '';
        } else {
            name = key ? key : e.target.name;
            if (e.target.value && e.target.value.trim() == '') {
                value = '';
            } else {
                value = e.target.value;
            }

        }
        switch (type) {
            case 'text':
                this.setState({
                    [name]: value ? ((value == 'true' || value == 'false') ? JSON.parse(value) : value) : '',
                    [name + 'Valid']: true,
                    [name + 'Touched']: true,
                    trustFormValid: toValidate ? (value ? true : false) : true
                }, () => {
                    if ((name == 'countryResidenceId' || name == 'mailingAddressCountry') && value == 231) {
                        this.getUSStates();
                    }
                    this.validateTrustPages();
                });
                break;
            case 'select':
                this.setState({
                    [name]: value ? parseInt(value) : '',
                    [name + 'Valid']: true,
                    [name + 'Touched']: true,
                    trustFormValid: toValidate ? (value ? true : false) : true
                }, () => {
                    if ((name == 'countryResidenceId' || name == 'mailingAddressCountry') && value == 0) {
                        this.setState({
                            usStatesList: [],
                            statesList: []
                        })
                    }
                    if (name == 'mailingAddressState' && value == 0) {
                        this.setState({
                            mailingAddressState: 0,
                            stateResidenceId: 0
                        })
                    }
                    if (name == 'countryResidenceId' && value == 231) {
                        this.getUSStates();
                    }
                    if (name == 'mailingAddressCountry' && value != 0) {
                        this.setState({
                            mailingAddressState: 0,
                            stateResidenceId: 0,
                            mailingAddressZip: ''
                        }, () => {
                            this.validateTrustPages();
                        })
                        this.getStatesByCountry(value);
                    } else {
                        this.validateTrustPages();
                    }
                    if (this.state.investorType == 'Individual') {
                        this.enableDisableInvestorDetailsButton();
                    }
                });
                break;
            case 'mobile':
                this.setState({
                    [name]: value ? value : '',
                    [name + 'Valid']: true,
                    [name + 'Touched']: true,
                    trustFormValid: toValidate ? (value ? true : false) : true
                }, () => {
                    this.validateTrustPages();
                });
                break;
            case 'number':
                const re = /^[1-9]*$/;
                if (re.test(value.trim())) {
                    this.setState({
                        [name]: value ? parseInt(value) : '',
                        [name + 'Valid']: true,
                        [name + 'Touched']: true,
                        trustFormValid: toValidate ? (value ? true : false) : true
                    }, () => {
                        this.validateTrustPages();
                    })
                    return true;
                } else {
                    if (parseInt(value) < 0) {
                        this.setState({
                            [name + 'Valid']: false,
                            [name + 'Touched']: true,
                            trustFormValid: toValidate ? false : true
                        }, () => {
                            this.validateTrustPages();
                        })
                        return true;
                    }
                }
                break;
            case 'checkbox':
                let investorAttr = this.state.otherInvestorAttributes;
                if (investorAttr.indexOf(name) > -1) {
                    investorAttr.splice(investorAttr.indexOf(name), 1);
                } else {
                    investorAttr.push(name)
                }
                this.setState({
                    otherInvestorAttributes: investorAttr
                }, () => {
                    if (this.state.investorType == 'LLC') {
                        if (this.state.otherInvestorAttributes.length > 0) {
                            this.setState({
                                otherInvestorAttributesValid: true
                            });
                            let name = 'otherInvestorAttributes' + 'Valid'
                            let dataObj = {
                                [name]: true
                            };
                            this.updateStateParams(dataObj);
                        } else {
                            this.setState({
                                otherInvestorAttributesValid: false
                            });
                            let name = 'otherInvestorAttributes' + 'Valid'
                            let dataObj = {
                                [name]: false
                            };
                            this.updateStateParams(dataObj);
                        }
                    } else {
                        this.validateTrustPages();
                    }
                })
                break;
            default:
                break;

        }
    }


    valueTouched(e, type) {
        const name = e.target.name;
        this.setState({
            [name + 'Touched']: true
        })
    }

    // //Show Tooltip Modal
    openSecurityTooltipModal = () => {
        PubSub.publish('openModal', { investorType: this.state.investorType, investorSubType: this.state.investorSubType, modalType: 'actModalWindow', type: 'security' });
    }

    //Show Tooltip Modal
    openRegulation506TooltipModal = () => {
        PubSub.publish('openModal', { investorType: this.state.investorType, investorSubType: this.state.investorSubType, modalType: 'actModalWindow', type: 'regulation506d' });
    }

    // //Show Tooltip Modal
    openDisqualifingEventModal = () => {
        PubSub.publish('openModal', { investorType: this.state.investorType, investorSubType: this.state.investorSubType, modalType: 'disqualifingEvent', type: 'disqualifingEventModal' });
    }

    isNumberKey(event) {
        if (this.state.mailingAddressCountry == 231) {
            if (this.state.mailingAddressZip !== null && this.state.mailingAddressZip !== '') {
                if (((event.which != 46 || (event.which == 46 && event.target.value == '')) ||
                    event.target.value.indexOf('.') >= 0) && (event.which < 48 || event.which > 57 || (this.state.mailingAddressZip.length >= 5))) {
                    event.preventDefault();
                }
            } else {
                if ((this.state.mailingAddressZip && this.state.mailingAddressZip.length >= 5) || event.which < 48 || event.which > 57) {
                    event.preventDefault();
                }
            }
        }

    }

    checkCountryandStateResidenceValidations() {
        // if((this.state.investorType === 'LLC' && this.state.investorSubTypeValue == 0 && (this.state.countryResidenceId == null || this.state.countryResidenceId == 0)) || (this.state.investorType !== 'LLC'  && (this.state.countryResidenceId == null || this.state.countryResidenceId == 0) || (this.state.investorType === 'LLC' && this.state.investorSubTypeValue == 1))) {
        if (parseInt(this.state.investorSubTypeValue) == 0) {
            if (!this.state.countryResidenceId || this.state.countryResidenceId == 0) {
                this.setState({
                    countryResidenceIdMsz: this.Constants.COUNTRY_RESIDENCE_REQUIRED,
                    countryResidenceIdTouched: true,
                    countryResidenceId: 0,
                    countryResidenceIdBorder: true
                })
                window.scrollTo(0, 0)
                return true;
            }
        }

        if ((parseInt(this.state.investorSubTypeValue) == 1 || this.state.countryResidenceId == 231) && (!this.state.stateResidenceId || this.state.stateResidenceId == 0)) {
            this.setState({
                stateResidenceIdMsz: this.Constants.STATE_RESIDENCE_REQUIRED,
                stateResidenceIdTouched: true,
                stateResidenceIdBorder: true,
                stateResidenceId: 0
            })
            window.scrollTo(0, 0)
            return true;
        }
        return false;
    }

    closeTitleUniqueCheckModal = () => { this.setState({ titleUniqueCheckModal: false }) }
    openTitleUniqueCheckModal = () => { this.setState({ titleUniqueCheckModal: true }) }
    closeinvestmentAvailableForRestoreModal = () => { this.setState({ investmentAvailableForRestoreModal: false }) }
    openinvestmentAvailableForRestoreModal = (name) => { this.setState({ investmentAvailableForRestoreModal: true }) }

    deleteSubscription = () => {
        this.open();
        const postObj = { subscriptionId: this.state.lpsubscriptionTotalObj.id }
        this.Fsnethttp.deleteSubscription(postObj).then(result => {
            this.close();
            this.closeTitleUniqueCheckModal();
            this.props.history.push('/dashboard');
        })
            .catch(error => {
                this.close();
            });
    }

    restoreInvestment = () => {
        this.open();
        const postObj = { investorType: this.state.investorType == 'Individual' && this.state.areYouSubscribingAsJointIndividual ? 'jointIndividual' : this.state.investorType, subscriptionId: this.state.lpsubscriptionTotalObj.id, fundId: this.state.fundId, title: this.getTitle(), lpId: this.state.lpsubscriptionTotalObj.lpId }
        this.Fsnethttp.restoreInvestment(postObj).then(result => {
            this.close();
            this.closeinvestmentAvailableForRestoreModal();
            this.props.history.push(`/lp/investorInfo/${result.data.data.id}`);
            setTimeout(() => {
                window.location.reload();
            }, 100);
        })
            .catch(error => {
                this.close();
            });
    }

    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement="left"
                    delayShow={300}
                    delayHide={150}
                    rootClose
                >
                    <a href={href}>{children}</a>
                </OverlayTrigger>
            );
        }

        function LinkWithTooltipAlignLeft({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement="left"
                    delayShow={300}
                    delayHide={150}
                    rootClose
                >
                    <a href={href}>{children}</a>
                </OverlayTrigger>
            );
        }

        return (
            <div className="individualForm marginTopSticky">
                {
                    ['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) == -1 &&
                    <Button className={"previousFundButton " + (this.state.disablePreviousFundBtn ? 'disabledBtnBgColor' : 'btnEnabled')} disabled={this.state.disablePreviousFundBtn} hidden={this.state.lpsubscriptionTotalObj.subscriptionStatus && (this.state.lpsubscriptionTotalObj.subscriptionStatus.name === 'Closed' || this.state.lpsubscriptionTotalObj.subscriptionStatus.name === 'Close-Ready' || this.state.hidePreviousFundBtn || (this.state.lpsubscriptionTotalObj.documentsForSignature && this.state.lpsubscriptionTotalObj.documentsForSignature[0] && this.state.lpsubscriptionTotalObj.documentsForSignature[0].isLpSigned))} onClick={this.openPreviousFundModal}>{this.state.jsonData.PREVIOUS_FUND}</Button>
                }
                <div className="form-grid formGridDivMargin min-height-400">
                    <div id="individualForm">
                        {
                            this.state.showInvestorType ?
                                <div>
                                    <div className="title">{this.state.jsonData.INVESTOR_INFORMATION}</div>
                                    <Row className="step1Form-row">
                                        <Col xs={12} md={12}>
                                            <label className="form-label width100">{this.state.jsonData.INVESTOR_TYPE}</label>
                                            <Radio name="investorType" inline checked={this.state.investorType === 'LLC'} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', 'LLC', 'investorType')}>&nbsp; {this.state.jsonData.ENTITY}
                                                <span className="radio-checkmark"></span>
                                            </Radio>
                                            <Radio name="investorType" className="marginRight10" inline checked={this.state.investorType === 'Individual'} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', 'Individual', 'investorType')}>&nbsp; {this.state.jsonData.INDIVIDUAL_JOINT}
                                                <span className="radio-checkmark"></span>
                                            </Radio>
                                            <Radio name="investorType" inline checked={this.state.investorType === 'Trust'} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', 'Trust', 'investorType')}>&nbsp; {this.state.jsonData.TRUST}
                                                <span className="radio-checkmark"></span>
                                            </Radio>
                                        </Col>
                                    </Row>

                                    <div className="individual" hidden={this.state.investorType !== 'Individual'}>
                                        <Row className="step1Form-row">
                                            <Col lg={6} md={6} sm={6} xs={12}>
                                                <label className="form-label">{this.state.jsonData.EMAIL_ADDRESS}</label>
                                                <FormControl type="email" name="email" placeholder={this.state.jsonData.EMAIL_ADDRESS_PLACEHOLDER} className="inputFormControl" readOnly value={this.state.email} />
                                            </Col>

                                        </Row>
                                        <Row className="step1Form-row">
                                            <Col lg={6} md={6} sm={6} xs={12}>
                                                <label className="form-label">{this.state.jsonData.INDIVIDUAL_LEGALLY_DOMICILED} &nbsp;
                                                                            <span>
                                                        <LinkWithTooltip tooltip={this.state.jsonData.INDIVIDUAL_LEGALLY_DOMICILED_TOOLTIP} id="tooltip-1">
                                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                        </LinkWithTooltip>
                                                    </span>
                                                </label>
                                                <FormControl name='countryResidenceId' defaultValue={0} value={this.state.countryResidenceId} className={"selectFormControl " + (this.state.countryResidenceIdTouched && this.state.countryResidenceId == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => { this.trustOnChangeForm(e, 'select', false); this.setState({ state: 0, stateTouched: false }) }} onBlur={(e) => this.valueTouched(e)}>
                                                    <option value={0}>{this.state.jsonData.SELECT_COUNTRY}</option>
                                                    {this.state.countriesList.map((record, index) => {
                                                        return (
                                                            <option value={record['id']} key={index} >{record['name']}</option>
                                                        );
                                                    })}
                                                </FormControl>
                                                {this.state.countryResidenceIdTouched && this.state.countryResidenceId == 0 ? <span className="error">{this.Constants.COUNTRY_RESIDENCE_REQUIRED}</span> : null}
                                            </Col>
                                            <Col lg={6} md={6} sm={6} xs={12} hidden={this.state.countryResidenceId != 231}>
                                                <label className="form-label">{this.state.jsonData.INDIVIDUAL_STATE_LEGALLY_DOMICILED}&nbsp;
                                                                    <span>
                                                        <LinkWithTooltip tooltip={this.state.jsonData.INDIVIDUAL_STATE_LEGALLY_DOMICILED_TOOLTIP} id="tooltip-1">
                                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                        </LinkWithTooltip>
                                                    </span>
                                                </label>
                                                <FormControl name='stateResidenceId' defaultValue={0} value={this.state.stateResidenceId} className={"selectFormControl " + (this.state.stateResidenceIdTouched && this.state.countryResidenceId == 231 && this.state.stateResidenceId == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => this.trustOnChangeForm(e, 'select', this.state.countryResidenceId == 231 ? true : false)} onBlur={(e) => this.valueTouched(e)}>
                                                    <option value={0}>{this.state.jsonData.SELECT_STATE}</option>
                                                    {this.state.usStatesList.map((record, index) => {
                                                        return (
                                                            <option value={record['id']} key={index} >{record['name']}</option>
                                                        );
                                                    })}
                                                </FormControl>
                                                {this.state.stateResidenceIdTouched && this.state.countryResidenceId == 231 && this.state.stateResidenceId == 0 ? <span className="error">{this.Constants.STATE_RESIDENCE_REQUIRED}</span> : null}
                                            </Col>
                                        </Row>
                                        <Row className="step1Form-row">
                                            <Col xs={12} md={12}>
                                                <label className="form-label width100">{this.state.jsonData.SUBSCRIBING_JOINT_INDIVIDUALS}</label>
                                                <Radio name="areYouSubscribingAsJointIndividual" inline checked={this.state.areYouSubscribingAsJointIndividual === true} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', true, 'areYouSubscribingAsJointIndividual')}>&nbsp; Yes
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                                <Radio name="areYouSubscribingAsJointIndividual" inline checked={this.state.areYouSubscribingAsJointIndividual === false} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', false, 'areYouSubscribingAsJointIndividual')}>&nbsp; No
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                            </Col>
                                        </Row>

                                        <Row className="step1Form-row" hidden={this.state.areYouSubscribingAsJointIndividual !== true}>
                                            <Col xs={12} md={12}>
                                                <label className="form-label width100">{this.state.jsonData.LEGAL_OWNERSHIP_TYPE}</label>
                                                <Radio name="typeOfLegalOwnership" className="right-gap" inline checked={this.state.typeOfLegalOwnership === 'jointTenants'} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', 'jointTenants', 'typeOfLegalOwnership')}>&nbsp; {this.state.jsonData.JOINT_TENANTS}
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                                <Radio name="typeOfLegalOwnership" className="right-gap" inline checked={this.state.typeOfLegalOwnership === 'jointTenantsRights'} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', 'jointTenantsRights', 'typeOfLegalOwnership')}>&nbsp; {this.state.jsonData.JOINT_TENANTS_RIGHTS}
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                                <Radio name="typeOfLegalOwnership" className="right-gap" inline checked={this.state.typeOfLegalOwnership === 'tenantsInCommon'} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', 'tenantsInCommon', 'typeOfLegalOwnership')}>&nbsp; {this.state.jsonData.TENANTS_COMMON}
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                                <Radio name="typeOfLegalOwnership" className="right-gap" inline checked={this.state.typeOfLegalOwnership === 'tenantsEntirety'} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', 'tenantsEntirety', 'typeOfLegalOwnership')}>&nbsp; {this.state.jsonData.TENANTS_ENTIRETY}
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                                <Radio name="typeOfLegalOwnership" className="right-gap" inline checked={this.state.typeOfLegalOwnership === 'communityProperty'} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', 'communityProperty', 'typeOfLegalOwnership')}>&nbsp; {this.state.jsonData.COMMUNITY_PROPERTY}
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                            </Col>
                                        </Row>
                                        <Row className="step1Form-row" hidden={this.state.typeOfLegalOwnership == '' || this.state.typeOfLegalOwnership == undefined}>
                                            <Col xs={6} md={6}>
                                                <label className="form-label">{this.state.jsonData.LEGAL_TITLE_LABLE} &nbsp;
                                                    <span>
                                                        <LinkWithTooltip tooltip={this.state.jsonData.LEGAL_TITLE_LABLE_TOOLTIP} id="tooltip-1">
                                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                        </LinkWithTooltip>
                                                    </span>
                                                </label>
                                                <FormControl type="text" name="legalTitle" className={"inputFormControl " + (this.state.legalTitleBorder ? 'inputError' : '')} value={this.state.legalTitle} onChange={(e) => this.investorHandleChangeEvent(e, 'legalTitle', 'LEGAL_TITLE_REQUIRED')} onBlur={(e) => this.investorHandleChangeEvent(e, 'legalTitle', 'LEGAL_TITLE_REQUIRED')} />
                                                <span className="error">{this.state.legalTitleMsz}</span>
                                            </Col>
                                        </Row>
                                        <Row className="step1Form-row">
                                            <Col xs={12} md={12}>
                                                <label className="form-label width100 block">{this.state.jsonData.INDIVIDUAL_REQUIRED_FOIA}</label>
                                                <Radio name="releaseInvestmentEntityRequired4" className="radioSmallTxtWidth" inline id="yesCheckbox" value={true} checked={this.state.releaseInvestmentEntityRequired == true} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'releaseInvestmentEntityRequired')}>&nbsp; Yes
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                                <Radio name="releaseInvestmentEntityRequired4" className="radioSmallTxtWidth" inline id="yesCheckbox" value={false} checked={this.state.releaseInvestmentEntityRequired == false} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'releaseInvestmentEntityRequired')}>&nbsp; No
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                            </Col>
                                        </Row>

                                        <Row className="step1Form-row" hidden={!this.state.releaseInvestmentEntityRequired}>
                                            <Col xs={12} md={12}>
                                                <label className="form-label width100">{this.state.jsonData.INDIVIDUAL_MANNERS_SUBJECT}</label>
                                            </Col>
                                            <Col xs={6} md={6}>
                                                <FormControl componentClass="textarea" name="indirectBeneficialOwnersSubjectFOIAStatutes" className={"inputFormControl textarea " + (this.state.indirectBeneficialOwnersSubjectFOIAStatutesBorder ? 'inputError' : '')} value={this.state.indirectBeneficialOwnersSubjectFOIAStatutes} onChange={(e) => this.investorHandleChangeEvent(e, 'indirectBeneficialOwnersSubjectFOIAStatutes', 'INDIRECT_BENIFICIAL_REQUIRED')} onBlur={(e) => this.investorHandleChangeEvent(e, 'indirectBeneficialOwnersSubjectFOIAStatutes', 'INDIRECT_BENIFICIAL_REQUIRED')} />
                                                <span className="error">{this.state.indirectBeneficialOwnersSubjectFOIAStatutesMsz}</span>
                                            </Col>
                                        </Row>

                                        <Row className="step1Form-row">
                                            <Col xs={12} md={12}>
                                                <label className="form-label width100 block" dangerouslySetInnerHTML={{
                                                    __html: this.checkTooltips(this.state.jsonData.INVESTOR_OR_BENEFICIAL_OWNER, 'obj1')
                                                }}>
                                                </label>
                                                <Radio name="isSubjectToDisqualifyingEvent2" className="radioSmallTxtWidth" inline id="yesCheckbox" value={true} checked={this.state.isSubjectToDisqualifyingEvent == true} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', true, 'isSubjectToDisqualifyingEvent')}>&nbsp; Yes
                                                <span className="radio-checkmark"></span>
                                                </Radio>
                                                <Radio name="isSubjectToDisqualifyingEvent2" className="radioSmallTxtWidth" inline id="yesCheckbox" value={false} checked={this.state.isSubjectToDisqualifyingEvent == false} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', false, 'isSubjectToDisqualifyingEvent')}>&nbsp; No
                                                <span className="radio-checkmark"></span>
                                                </Radio>
                                            </Col>
                                        </Row>
                                        <Row className="step1Form-row">
                                            <Col xs={12} md={12}>
                                                <label className="form-label width100 block">{this.state.jsonData.DISCLOSE_FUND_MANAGER}:</label>
                                                <FormControl componentClass="textarea" placeholder={this.state.jsonData.FUND_MANAGER_INFORMATION} name="fundManagerInfo" value={this.state.fundManagerInfo} className="inputFormControl textarea col-lg-6 col-md-6" onChange={(e) => this.investorHandleChangeEvent(e, 'fundManagerInfo', 'NAME_REQUIRED')} onBlur={(e) => this.valueTouched(e)} />
                                            </Col>
                                        </Row>
                                    </div>

                                </div>
                                :
                                <div>
                                    {
                                        this.state.investorType === 'Individual' &&
                                        <div className="individual">
                                            <Row className="step1Form-row">
                                                <Col xs={12} md={12}>
                                                    <label className="title">{this.state.jsonData.INVESTOR_MAIL_ADDRESS}</label>
                                                </Col>
                                            </Row>
                                            <Row className="step1Form-row">
                                                <Col xs={6} md={6}>
                                                    <label className="form-label">{this.state.jsonData.ADDRESS}</label>
                                                    <FormControl componentClass="textarea" placeholder={this.state.jsonData.ADDRESS} name="mailingAddressStreet" value={this.state.mailingAddressStreet} className={"inputFormControl textarea " + (this.state.mailingAddressStreetTouched && !this.state.mailingAddressStreet ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                    {this.state.mailingAddressStreetTouched && !this.state.mailingAddressStreet ? <span className="error">{this.Constants.STREET_REQUIRED}</span> : null}
                                                </Col>

                                            </Row>
                                            <Row className="step1Form-row">
                                                <Col xs={6} md={6}>
                                                    <label className="form-label">{this.state.jsonData.CITY}</label>
                                                    <FormControl type="text" placeholder={this.state.jsonData.CITY} name="mailingAddressCity" value={this.state.mailingAddressCity} className={"inputFormControl " + (this.state.mailingAddressCityTouched && !this.state.mailingAddressCity ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                    {this.state.mailingAddressCityTouched && !this.state.mailingAddressCity ? <span className="error">{this.Constants.CITY_REQUIRED}</span> : null}
                                                </Col>
                                                <Col md={6} xs={6}>
                                                    <label className="form-label">{this.state.jsonData.COUNTRY}</label>
                                                    <FormControl name='mailingAddressCountry' defaultValue={0} value={this.state.mailingAddressCountry} className={"selectFormControl " + (this.state.mailingAddressCountryTouched && this.state.mailingAddressCountry == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => { this.trustOnChangeForm(e, 'select', true); this.setState({ state: 0, stateTouched: false }) }} onBlur={(e) => this.valueTouched(e)}>
                                                        <option value={0}>{this.state.jsonData.SELECT_COUNTRY}</option>
                                                        {this.state.countriesList.map((record, index) => {
                                                            return (
                                                                <option value={record['id']} key={index} >{record['name']}</option>
                                                            );
                                                        })}
                                                    </FormControl>
                                                    {this.state.mailingAddressCountryTouched && this.state.mailingAddressCountry == 0 ? <span className="error">{this.Constants.COUNTRY_REQUIRED}</span> : null}
                                                </Col>

                                            </Row>
                                            <Row className="step1Form-row">
                                                <Col md={6} xs={6}>
                                                    <label className="form-label">{this.state.jsonData.STATE}</label>
                                                    <FormControl name='mailingAddressState' defaultValue={0} value={this.state.mailingAddressState} className={"selectFormControl " + (this.state.mailingAddressStateTouched && this.state.mailingAddressState == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => this.trustOnChangeForm(e, 'select', this.state.mailingAddressCountry == 231 ? true : false)} onBlur={(e) => this.valueTouched(e)}>
                                                        <option value={0}>{this.state.jsonData.SELECT_STATE}</option>
                                                        {this.state.statesList.map((record, index) => {
                                                            return (
                                                                <option value={record['id']} key={index} >{record['name']}</option>
                                                            );
                                                        })}
                                                    </FormControl>
                                                    {this.state.mailingAddressStateTouched && this.state.mailingAddressState == 0 ? <span className="error">{this.Constants.STATE_REQUIRED}</span> : null}
                                                </Col>
                                                <Col xs={6} md={6}>
                                                    <label className="form-label">{this.state.jsonData.ZIP_CODE}</label>
                                                    <FormControl type="text" placeholder={this.state.jsonData.ZIP_CODE} name="mailingAddressZip" onKeyPress={(e) => { this.isNumberKey(e) }} value={this.state.mailingAddressZip} className={"inputFormControl " + (this.state.mailingAddressZipTouched && !this.state.mailingAddressZip ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                    {this.state.mailingAddressZipTouched && !this.state.mailingAddressZip ? <span className="error">{this.Constants.ZIP_REQUIRED}</span> : null}
                                                </Col>
                                            </Row>
                                            <Row className="step1Form-row">
                                                <Col xs={6} md={6}>
                                                    <label className="form-label">{this.state.jsonData.INDIVIDUAL_BUSINESS_TELEPHONE}</label>
                                                    <PhoneInput name="mailingAddressPhoneNumber" value={this.state.mailingAddressPhoneNumber} onChange={e => { this.trustOnChangeForm(e, 'mobile', false, 'mailingAddressPhoneNumber') }} maxLength="14" placeholder={this.state.jsonData.TRUST_TELEPHONE_NUMBER_PLACEHOLDER} country="US" />
                                                </Col>
                                            </Row>
                                        </div>
                                    }
                                </div>
                        }


                        <div className="LLC" hidden={this.state.investorType !== 'LLC'}>
                            <div hidden={!this.state.showLLCInvestorInfoPage1}>
                                <Row className="step1Form-row">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.ENTITY_TYPE}</label>
                                        <FormControl defaultValue="0" value={this.state.investorSubType || "0"} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} className={"selectFormControl " + (this.state.investorSubTypeBorder ? 'inputError' : '')} onChange={(e) => this.investorHandleChangeEvent(e, 'investorSubType', 'INVESTOR_SUB_TYPE_REQUIRED')}>
                                            <option isus="" value="0">{this.state.jsonData.SELECT_ENTITY_TYPE}</option>
                                            {this.state.investorSubTypes.map((record, index) => {
                                                return (
                                                    <option isus={record['isUS']} value={record['id']} key={index} >{record['name']}</option>
                                                );
                                            })}
                                            <option value="otherEntity" isus='0'>{this.state.jsonData.OTHER}</option>
                                        </FormControl>
                                        <span className="error">{this.state.investorSubTypeMsz}</span>
                                    </Col>
                                    <Col xs={6} md={6} hidden={this.state.investorSubType !== 'otherEntity'}>
                                        <label className="form-label">{this.state.jsonData.ENTER_ENTITY_TYPE}:
                                                </label>
                                        <FormControl type="text" placeholder={this.state.jsonData.ENTER_ENTITY_TYPE} className={"inputFormControl " + (this.state.otherInvestorSubTypeBorder ? 'inputError' : '')} value={this.state.otherInvestorSubType} onChange={(e) => this.investorHandleChangeEvent(e, 'otherInvestorSubType', 'ENTITY_TYPE_REQUIRED')} onBlur={(e) => this.investorHandleChangeEvent(e, 'otherInvestorSubType', 'ENTITY_TYPE_REQUIRED')} />
                                        <span className="error">{this.state.otherInvestorSubTypeMsz}</span>
                                    </Col>

                                </Row>
                                <Row className="step1Form-row">
                                    <Col xs={6} md={6} className="paddingRightZero">
                                        {
                                            this.state.investorSubTypeValue == 1 && this.state.investorSubType !== 'otherEntity' ?
                                                <div>
                                                    <label className="form-label">{this.state.jsonData.INVESTOR_SUBTYPE_STATE}
                                                        <span>
                                                            <LinkWithTooltipAlignLeft tooltip={this.state.jsonData.INVESTOR_SUBTYPE_STATE_TOOLTIP} id="tooltip-1">
                                                                <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                            </LinkWithTooltipAlignLeft>
                                                        </span>
                                                    </label>
                                                    <FormControl defaultValue="0" componentClass="select" value={this.state.stateDomicileId} placeholder={this.state.jsonData.SELECT_STATE} className={"selectFormControl " + (this.state.stateDomicileIdBorder ? 'inputError' : '')} onChange={(e) => this.investorHandleChangeEvent(e, 'stateDomicileId', 'STATE_DOMICILE_REQUIRED')}>
                                                        <option value="0">{this.state.jsonData.SELECT_STATE}</option>
                                                        {this.state.usStatesList.map((record, index) => {
                                                            return (
                                                                <option value={record['id']} key={index}>{record['name']}</option>
                                                            );
                                                        })}
                                                    </FormControl>
                                                    <span className="error">{this.state.stateDomicileIdMsz}</span>
                                                </div>
                                                :
                                                <div>
                                                    <label className="form-label">{this.state.jsonData.INVESTOR_SUBTYPE_COUNTRY}
                                                        <span>
                                                            <LinkWithTooltip tooltip={this.state.jsonData.INVESTOR_SUBTYPE_COUNTRY_TOOLTIP} id="tooltip-1">
                                                                <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                            </LinkWithTooltip>
                                                        </span>
                                                    </label>
                                                    <FormControl defaultValue="0" value={this.state.countryDomicileId} componentClass="select" placeholder={this.state.jsonData.SELECT_JURISDICTION} className={"selectFormControl " + (this.state.countryDomicileIdBorder ? 'inputError' : '')} onChange={(e) => this.investorHandleChangeEvent(e, 'countryDomicileId', 'COUNTRY_DOMICILE_REQUIRED')}>
                                                        <option value="0">{this.state.jsonData.SELECT_JURISDICTION}</option>
                                                        {this.state.investorJurisdictionTypes.map((record, index) => {
                                                            return (
                                                                <option value={record['id']} key={index}>{record['name']}</option>
                                                            );
                                                        })}
                                                    </FormControl>
                                                    <span className="error">{this.state.countryDomicileIdMsz}</span>
                                                </div>
                                        }

                                    </Col>

                                    <Col xs={6} md={6} hidden={this.state.countryDomicileId != 231}>
                                        <label className="form-label">{this.state.jsonData.ENTITY_REGISTERED_STATE}</label>
                                        <FormControl defaultValue="0" componentClass="select" value={this.state.stateDomicileId} placeholder={this.state.jsonData.SELECT_STATE} className={"selectFormControl " + (this.state.stateDomicileIdBorder ? 'inputError' : '')} onChange={(e) => this.investorHandleChangeEvent(e, 'stateDomicileId', 'STATE_DOMICILE_REQUIRED')}>
                                            <option value="0">{this.state.jsonData.SELECT_STATE}</option>
                                            {this.state.usStatesList.map((record, index) => {
                                                return (
                                                    <option value={record['id']} key={index}>{record['name']}</option>
                                                );
                                            })}
                                        </FormControl>
                                        <span className="error">{this.state.stateDomicileIdMsz}</span>
                                    </Col>


                                </Row>
                                <Row className="step1Form-row">


                                    {
                                        this.state.investorSubTypeValue == 1 && this.state.investorSubType !== 'otherEntity' ?
                                            <Col xs={6} md={6}>
                                                <label className="form-label">What is the State of residence of the Entity?
                                                                <span>
                                                        <LinkWithTooltip tooltip="This is the State in which the Entity has its principal place of business.  In general, a principal place of business is the primary location where business is performed, and it is generally where the business keeps its books and records, and where the head of the firm (or upper management, or the majority thereof) work.  The Entity’s legal counsel should be consulted to make a proper determination about the Entity’s State of residence.  Please take note that among other things, the data in this field will be relied upon and used for State “blue sky” securities law filing purposes." id="tooltip-1">
                                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                        </LinkWithTooltip>
                                                    </span>
                                                </label>
                                                <FormControl defaultValue="0" value={this.state.stateResidenceId} componentClass="select" placeholder={this.state.jsonData.SELECT_JURISDICTION} className={"selectFormControl " + (this.state.stateResidenceIdBorder ? 'inputError' : '')} onChange={(e) => this.investorHandleChangeEvent(e, 'stateResidenceId', 'STATE_RESIDENCE_REQUIRED')}>
                                                    <option value="0">{this.state.jsonData.SELECT_STATE}</option>
                                                    {this.state.usStatesList.map((record, index) => {
                                                        return (
                                                            <option value={record['id']} key={index}>{record['name']}</option>
                                                        );
                                                    })}
                                                </FormControl>
                                                <span className="error">{this.state.stateResidenceIdMsz}</span>
                                            </Col>
                                            :
                                            this.state.investorSubTypeValue == 0 || this.state.investorSubType == 'otherEntity' ?
                                                <Col xs={6} md={6}>
                                                    <label className="form-label">What is the Country of residence of the Entity?
                                                                <span>
                                                            <LinkWithTooltip tooltip="This is the country in which the Entity has its principal place of business.  In general, a principal place of business is the primary location where business is performed, and it is generally where the business keeps its books and records, and where the head of the firm (or upper management, or the majority thereof) work.  The Entity’s legal counsel should be consulted to make a proper determination about the Entity’s country of residence." id="tooltip-1">
                                                                <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                            </LinkWithTooltip>
                                                        </span>
                                                    </label>
                                                    <FormControl defaultValue="0" value={this.state.countryResidenceId} componentClass="select" placeholder={this.state.jsonData.SELECT_JURISDICTION} className={"selectFormControl " + (this.state.countryResidenceIdBorder ? 'inputError' : '')} onChange={(e) => this.investorHandleChangeEvent(e, 'countryResidenceId', 'COUNTRY_RESIDENCE_REQUIRED')}>
                                                        <option value="0">{this.state.jsonData.SELECT_COUNTRY}</option>
                                                        {this.state.countriesList.map((record, index) => {
                                                            return (
                                                                <option value={record['id']} key={index}>{record['name']}</option>
                                                            );
                                                        })}
                                                    </FormControl>
                                                    <span className="error">{this.state.countryResidenceIdMsz}</span>
                                                </Col>
                                                : ''
                                    }

                                    <Col xs={6} md={6} hidden={this.state.countryResidenceId != 231 || this.state.investorSubTypeValue == 1}>
                                        <label className="form-label">What is the state of residence of the Entity?
                                                    <span>
                                                <LinkWithTooltipAlignLeft tooltip="This is the State in which the Entity has its principal place of business.  In general, a principal place of business is the primary location where business is performed, and it is generally where the business keeps its books and records, and where the head of the firm (or upper management, or the majority thereof) work.  The Entity’s legal counsel should be consulted to make a proper determination about the Entity’s State of residence.  Please take note that among other things, the data in this field will be relied upon and used for State “blue sky” securities law filing purposes." id="tooltip-1">
                                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                </LinkWithTooltipAlignLeft>
                                            </span>
                                        </label>
                                        <FormControl defaultValue="0" componentClass="select" value={this.state.stateResidenceId} placeholder={this.state.jsonData.SELECT_STATE} className={"selectFormControl " + (this.state.stateResidenceIdBorder ? 'inputError' : '')} onChange={(e) => this.investorHandleChangeEvent(e, 'stateResidenceId', 'STATE_RESIDENCE_REQUIRED')}>
                                            <option value="0">{this.state.jsonData.SELECT_STATE}</option>
                                            {this.state.usStatesList.map((record, index) => {
                                                return (
                                                    <option value={record['id']} key={index}>{record['name']}</option>
                                                );
                                            })}
                                        </FormControl>
                                        <span className="error">{this.state.stateResidenceIdMsz}</span>
                                    </Col>
                                </Row>

                                <Row className="step1Form-row">
                                    <Col xs={6} md={6}>
                                        <label className="form-label ">{this.state.jsonData.EMAIL_ADDRESS}</label>
                                        <FormControl type="email" name="email" placeholder={this.state.jsonData.EMAIL_ADDRESS_PLACEHOLDER} className="inputFormControl" readOnly value={this.state.email} />
                                        <span className="error"></span>
                                    </Col>
                                    <Col xs={6} md={6}>
                                        <label className="form-label ">{this.state.jsonData.ENTITY_LEGAL_NAME} &nbsp;
                                                <span>
                                                <LinkWithTooltip tooltip={this.state.jsonData.ENTER_EXACT_NAME} id="tooltip-1">
                                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                </LinkWithTooltip>
                                            </span>
                                        </label>
                                        <FormControl type="text" name="entityName" placeholder={this.state.jsonData.ENTER_ENTITY_NAME} className={"inputFormControl " + (this.state.entityNameBorder ? 'inputError' : '')} value={this.state.entityName} onChange={(e) => this.investorHandleChangeEvent(e, 'entityName', 'ENTITY_NAME_REQUIRED')} onBlur={(e) => this.investorHandleChangeEvent(e, 'entityName', 'ENTITY_NAME_REQUIRED')} />
                                        <span className="error">{this.state.entityNameMsz}</span>
                                    </Col>

                                </Row>
                                <Row className="step1Form-row">
                                    <Col xs={6} md={6}>
                                        <label className="form-label block">{this.state.jsonData.ENTITY_VEHICLE_TYPE}</label>
                                        <Radio name="entityFundOfFundsOrSimilarTypeVehicle1" className="radioSmallTxtWidth" inline checked={this.state.istheEntityFundOfFundsOrSimilarTypeVehicle === true} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', true, 'istheEntityFundOfFundsOrSimilarTypeVehicle')}>&nbsp; Yes
                                                <span className="radio-checkmark"></span>
                                        </Radio>
                                        <Radio name="entityFundOfFundsOrSimilarTypeVehicle1" className="radioSmallTxtWidth" inline checked={this.state.istheEntityFundOfFundsOrSimilarTypeVehicle === false} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', false, 'istheEntityFundOfFundsOrSimilarTypeVehicle')}>&nbsp; No
                                                <span className="radio-checkmark"></span>
                                        </Radio>
                                        <div className="error">{this.state.entityFundOfFundsOrSimilarTypeVehicleMsz}</div>
                                    </Col>

                                </Row>
                                <Row className="step1Form-row">
                                    <Col xs={6} md={6}>
                                        <label className="form-label block">{this.state.jsonData.TAX_EXEMPT_FOR_US}</label>
                                        <Radio name="taxExempt" className="radioSmallTxtWidth" inline checked={this.state.isEntityTaxExemptForUSFederalIncomeTax === true} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', true, 'isEntityTaxExemptForUSFederalIncomeTax')}>&nbsp; Yes
                                                <span className="radio-checkmark"></span>
                                        </Radio>
                                        <Radio name="taxExempt" className="radioSmallTxtWidth" inline checked={this.state.isEntityTaxExemptForUSFederalIncomeTax === false} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', false, 'isEntityTaxExemptForUSFederalIncomeTax')}>&nbsp; No
                                                <span className="radio-checkmark"></span>
                                        </Radio>
                                        <div className="error">{this.state.isEntityTaxExemptForUSFederalIncomeTaxMsz}</div>
                                    </Col>
                                    <Col xs={6} md={6} hidden={this.state.isEntityTaxExemptForUSFederalIncomeTax !== true}>
                                        <label className="form-label block paddingBottomHidden">{this.state.jsonData.ENTITY_US_501_C3}?</label>
                                        <Radio name="entityUS501" className="radioSmallTxtWidth" inline checked={this.state.isEntityUS501c3 === true} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', true, 'isEntityUS501c3')}>&nbsp; Yes
                                                <span className="radio-checkmark"></span>
                                        </Radio>
                                        <Radio name="entityUS501" className="radioSmallTxtWidth" inline checked={this.state.isEntityUS501c3 === false} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', false, 'isEntityUS501c3')}>&nbsp; No
                                                <span className="radio-checkmark"></span>
                                        </Radio>
                                        <div className="error">{this.state.isEntityUS501c3Msz}</div>
                                    </Col>
                                </Row>
                                <Row className="step1Form-row">
                                    <Col xs={12} md={12}>
                                        <label className="form-label width100">{this.state.jsonData.DIRECT_INDIRECT_BENEFICIAL_OWNERS_REQUIRED}</label>
                                        <Radio name="releaseInvestmentEntityRequired1" className="radioSmallTxtWidth" inline checked={this.state.releaseInvestmentEntityRequired === true} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', true, 'releaseInvestmentEntityRequired')}>&nbsp; Yes
                                                <span className="radio-checkmark"></span>
                                        </Radio>
                                        <Radio name="releaseInvestmentEntityRequired1" className="radioSmallTxtWidth" inline checked={this.state.releaseInvestmentEntityRequired === false} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', false, 'releaseInvestmentEntityRequired')}>&nbsp; No
                                                <span className="radio-checkmark"></span>
                                        </Radio>
                                    </Col>
                                </Row>
                                <Row className="step1Form-row" hidden={!this.state.releaseInvestmentEntityRequired}>
                                    <Col xs={12} md={12}>
                                        <label className="form-label width100">{this.state.jsonData.ENTITY_MANNERS_SUBJECT}</label>
                                    </Col>
                                    <Col xs={6} md={6}>
                                        <FormControl componentClass="textarea" name="indirectBeneficialOwnersSubjectFOIAStatutes" className={"inputFormControl textarea " + (this.state.indirectBeneficialOwnersSubjectFOIAStatutesBorder ? 'inputError' : '')} value={this.state.indirectBeneficialOwnersSubjectFOIAStatutes} onChange={(e) => this.investorHandleChangeEvent(e, 'indirectBeneficialOwnersSubjectFOIAStatutes', 'INDIRECT_BENIFICIAL_REQUIRED')} onBlur={(e) => this.investorHandleChangeEvent(e, 'indirectBeneficialOwnersSubjectFOIAStatutes', 'INDIRECT_BENIFICIAL_REQUIRED')} />
                                        <span className="error">{this.state.indirectBeneficialOwnersSubjectFOIAStatutesMsz}</span>
                                    </Col>
                                </Row>
                                <Row className="step1Form-row">
                                    <Col xs={12} md={12}>
                                        <label className="form-label width100 block" dangerouslySetInnerHTML={{
                                            __html: this.checkTooltips(this.state.jsonData.INVESTOR_OR_BENEFICIAL_OWNER, 'obj2')
                                        }}>
                                        </label>
                                        <Radio name="isSubjectToDisqualifyingEvent1" className="radioSmallTxtWidth" inline id="yesCheckbox" value={true} checked={this.state.isSubjectToDisqualifyingEvent == true} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', true, 'isSubjectToDisqualifyingEvent')}>&nbsp; Yes
                                        <span className="radio-checkmark"></span>
                                        </Radio>
                                        <Radio name="isSubjectToDisqualifyingEvent1" className="radioSmallTxtWidth" inline id="yesCheckbox" value={false} checked={this.state.isSubjectToDisqualifyingEvent == false} onChange={(e) => this.investorHandleChangeEvent(e, 'radio', false, 'isSubjectToDisqualifyingEvent')}>&nbsp; No
                                        <span className="radio-checkmark"></span>
                                        </Radio>
                                    </Col>
                                </Row>
                                <Row className="step1Form-row">
                                    <Col xs={12} md={12}>
                                        <label className="form-label width100 block">{this.state.jsonData.DISCLOSE_FUND_MANAGER}:</label>
                                        <FormControl componentClass="textarea" placeholder={this.state.jsonData.FUND_MANAGER_INFORMATION} name="fundManagerInfo" value={this.state.fundManagerInfo} className="inputFormControl textarea col-lg-6 col-md-6" onChange={(e) => this.investorHandleChangeEvent(e, 'fundManagerInfo', 'NAME_REQUIRED')} onBlur={(e) => this.valueTouched(e)} />
                                    </Col>
                                </Row>
                                <Row className="step1lastCheckBox">
                                    <label className="form-label width100 step1PaddingLeft15">{this.state.jsonData.CHOOSE_ALL_IF_ANY}</label>
                                    <Row className="step1PaddingLeft15">
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Tax-Exempt Organization" checked={this.state.otherInvestorAttributes.indexOf('Tax-Exempt Organization') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.TAX_EXEMPT_ORG}</span>
                                            </CBox>
                                        </Col>
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Employee Benefit Plan" checked={this.state.otherInvestorAttributes.indexOf('Employee Benefit Plan') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.EMPLOYEE_BENEFIT_PLAN}</span>
                                            </CBox>
                                        </Col>
                                    </Row>
                                    <Row className="step1PaddingLeft15">
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="ERISA Partner" checked={this.state.otherInvestorAttributes.indexOf('ERISA Partner') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.ERISA_PARTNER}</span>
                                            </CBox>
                                        </Col>
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Bank Holding Company" checked={this.state.otherInvestorAttributes.indexOf('Bank Holding Company') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.BANK_HOLIDAY_COMPANY}</span>
                                            </CBox>
                                        </Col>
                                    </Row>
                                    <Row className="step1PaddingLeft15">
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Foreign Government Entity" checked={this.state.otherInvestorAttributes.indexOf('Foreign Government Entity') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.FOREIGN_GOVERNMENT_ENTITY}</span>
                                            </CBox>
                                        </Col>
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Governmental Pension Plan" checked={this.state.otherInvestorAttributes.indexOf('Governmental Pension Plan') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.GOVERNMENTAL_PENSION_PLAN}</span>
                                            </CBox>
                                        </Col>
                                    </Row>
                                    <Row className="step1PaddingLeft15">
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Non-Pension Governmental Entity" checked={this.state.otherInvestorAttributes.indexOf('Non-Pension Governmental Entity') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.NON_PENSION_GOVERNMENTAL_ENTITY}</span>
                                            </CBox>
                                        </Col>
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Private Pension Plan" checked={this.state.otherInvestorAttributes.indexOf('Private Pension Plan') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.PRIVATE_PENSION_PLAN}</span>
                                            </CBox>
                                        </Col>
                                    </Row>
                                    <Row className="step1PaddingLeft15">
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Fund of Funds" checked={this.state.otherInvestorAttributes.indexOf('Fund of Funds') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.FUND_OF_FUNDS}</span>
                                            </CBox>
                                        </Col>
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Insurance Company" checked={this.state.otherInvestorAttributes.indexOf('Insurance Company') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.INSURANCE_COMPANY}</span>
                                            </CBox>
                                        </Col>
                                    </Row>
                                    <Row className="step1PaddingLeft15">
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Private Foundation" checked={this.state.otherInvestorAttributes.indexOf('Private Foundation') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.PRIVATE_FOUNDATION}</span>
                                            </CBox>
                                        </Col>
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="501(c)(3)" checked={this.state.otherInvestorAttributes.indexOf('501(c)(3)') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.C_3_501}</span>
                                            </CBox>
                                        </Col>
                                    </Row>
                                    <Row className="step1PaddingLeft15">
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Investment Company Registered with SEC" checked={this.state.otherInvestorAttributes.indexOf('Investment Company Registered with SEC') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.REGISTERED_SEC}</span>
                                            </CBox>
                                        </Col>
                                        <Col md={6} sm={6} xs={6}>
                                            <CBox inline className="cBoxFullAlign" name="Broker-Dealer" checked={this.state.otherInvestorAttributes.indexOf('Broker-Dealer') > -1} onChange={(e) => this.trustOnChangeForm(e, 'checkbox', true)}>
                                                <span className="checkbox-checkmark checkmark"></span>
                                                <span className="marginLeft6 otherInvesterFontSize">{this.state.jsonData.BROKER_DEALER}</span>
                                            </CBox>
                                        </Col>
                                    </Row>
                                </Row>
                            </div>
                            <div hidden={this.state.showLLCInvestorInfoPage1}>
                                <Row className="step1Form-row">
                                    <Col xs={12} md={12}>
                                        <label className="title">{this.state.jsonData.ENTITY_PRIMARY_MAILING}</label>
                                    </Col>
                                </Row>
                                <Row className="step1Form-row">
                                    <Col xs={6} md={6}>
                                        <label className="form-label">{this.state.jsonData.ADDRESS}</label>
                                        <FormControl componentClass="textarea" placeholder={this.state.jsonData.ADDRESS} name="mailingAddressStreet" value={this.state.mailingAddressStreet} className={"inputFormControl textarea " + (this.state.mailingAddressStreetTouched && !this.state.mailingAddressStreet ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                        {this.state.mailingAddressStreetTouched && !this.state.mailingAddressStreet ? <span className="error">{this.Constants.STREET_REQUIRED}</span> : null}
                                    </Col>

                                </Row>
                                <Row className="step1Form-row">
                                    <Col xs={6} md={6}>
                                        <label className="form-label">{this.state.jsonData.CITY}</label>
                                        <FormControl type="text" placeholder={this.state.jsonData.CITY} name="mailingAddressCity" value={this.state.mailingAddressCity} className={"inputFormControl " + (this.state.mailingAddressCityTouched && !this.state.mailingAddressCity ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                        {this.state.mailingAddressCityTouched && !this.state.mailingAddressCity ? <span className="error">{this.Constants.CITY_REQUIRED}</span> : null}
                                    </Col>
                                    <Col md={6} xs={6}>
                                        <label className="form-label">{this.state.jsonData.COUNTRY}</label>
                                        <FormControl name='mailingAddressCountry' defaultValue={0} value={this.state.mailingAddressCountry} className={"selectFormControl " + (this.state.mailingAddressCountryTouched && this.state.mailingAddressCountry == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => { this.trustOnChangeForm(e, 'select', true); this.setState({ state: 0, stateTouched: false }) }} onBlur={(e) => this.valueTouched(e)}>
                                            <option value={0}>{this.state.jsonData.SELECT_COUNTRY}</option>
                                            {this.state.countriesList.map((record, index) => {
                                                return (
                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                );
                                            })}
                                        </FormControl>
                                        {this.state.mailingAddressCountryTouched && this.state.mailingAddressCountry == 0 ? <span className="error">{this.Constants.COUNTRY_REQUIRED}</span> : null}
                                    </Col>

                                </Row>
                                <Row className="step1Form-row">
                                    <Col md={6} xs={6}>
                                        <label className="form-label">{this.state.jsonData.STATE}</label>
                                        <FormControl name='mailingAddressState' defaultValue={0} value={this.state.mailingAddressState} className={"selectFormControl " + (this.state.mailingAddressStateTouched && this.state.mailingAddressState == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => this.trustOnChangeForm(e, 'select', this.state.mailingAddressCountry == 231 ? true : false)} onBlur={(e) => this.valueTouched(e)}>
                                            <option value={0}>{this.state.jsonData.SELECT_STATE}</option>
                                            {this.state.statesList.map((record, index) => {
                                                return (
                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                );
                                            })}
                                        </FormControl>
                                        {this.state.mailingAddressStateTouched && this.state.mailingAddressState == 0 ? <span className="error">{this.Constants.STATE_REQUIRED}</span> : null}
                                    </Col>
                                    <Col xs={6} md={6}>
                                        <label className="form-label">{this.state.jsonData.ZIP_CODE}</label>
                                        <FormControl type="text" placeholder={this.state.jsonData.ZIP_CODE} onKeyPress={(e) => { this.isNumberKey(e) }} name="mailingAddressZip" value={this.state.mailingAddressZip} className={"inputFormControl " + (this.state.mailingAddressZipTouched && !this.state.mailingAddressZip ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                        {this.state.mailingAddressZipTouched && !this.state.mailingAddressZip ? <span className="error">{this.Constants.ZIP_REQUIRED}</span> : null}
                                    </Col>
                                </Row>
                                <Row className="step1Form-row">
                                    <Col xs={6} md={6}>
                                        <label className="form-label">{this.state.jsonData.ENTITY_TELEPHONE_NUMBER}</label>
                                        <PhoneInput name="mailingAddressPhoneNumber" value={this.state.mailingAddressPhoneNumber} onChange={e => { this.trustOnChangeForm(e, 'mobile', false, 'mailingAddressPhoneNumber') }} maxLength="14" placeholder={this.state.jsonData.TRUST_TELEPHONE_NUMBER_PLACEHOLDER} country="US" />
                                    </Col>
                                </Row>
                            </div>
                        </div>


                        {/* ============== Investor Trust Type ============== */}


                        <div className="trust" hidden={this.state.investorType !== 'Trust'}>
                            <Row className="step1Form-row" hidden={!this.state.showTrustRevocableInvestorPage1 || !this.state.showTrustIrrevocableInvestorPage1}>
                                <Col lg={6} md={6} sm={6} xs={12}>
                                    <label className="form-label">{this.state.jsonData.INVESTOR_SUB_TYPE}:</label>
                                    <FormControl name='investorSubType' defaultValue={0} value={this.state.investorSubType || "0"} className={"selectFormControl " + (this.state.investorSubTypeTouched && this.state.investorSubType == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => this.trustOnChangeForm(e, 'select', true)} onBlur={(e) => this.valueTouched(e)}>
                                        <option value={0}>{this.state.jsonData.SELECT_INVESTOR_SUB_TYPE}</option>
                                        {this.state.investorTrustSubTypes.map((record, index) => {
                                            return (
                                                <option value={record['id']} key={index} >{record['name']}</option>
                                            );
                                        })}
                                    </FormControl>
                                    {this.state.investorSubTypeTouched && this.state.investorSubType == 0 ? <span className="error">{this.Constants.INVESTOR_SUB_TYPE_REQUIRED}</span> : null}
                                </Col>
                            </Row>
                            {/* ====== revocable trust ========  */}
                            
                                <div id="RecoverableTrust" hidden={this.state.investorSubType !== 9}>
                                    {
                                        this.state.showTrustRevocableInvestorPage1
                                            ?
                                            <div>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label ">{this.state.jsonData.ENTER_TRUST_NAME} &nbsp;
                                                                                <span>
                                                                <LinkWithTooltip tooltip={this.state.jsonData.ENTIRE_TRUST_LEGAL_NAME} id="tooltip-1">
                                                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                                </LinkWithTooltip>
                                                            </span>
                                                        </label>
                                                        <FormControl type="text" name='trustName' placeholder={this.state.jsonData.ENTER_TRUST_NAME} value={this.state.trustName} className={"inputFormControl " + (this.state.trustNameTouched && !this.state.trustName ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                        {this.state.trustNameTouched && !this.state.trustName ? <span className="error">{this.Constants.TRUST_NAME_REQUIRED}</span> : null}
                                                    </Col>
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label ">{this.state.jsonData.EMAIL_ADDRESS}</label>
                                                        <FormControl name='email' type="email" name="email" placeholder={this.state.jsonData.EMAIL_ADDRESS_PLACEHOLDER} className="inputFormControl" readOnly value={this.state.email} />
                                                        <span className="error"></span>
                                                    </Col>

                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col lg={6} md={6} sm={6} xs={12}>
                                                        <label className="form-label">{this.state.jsonData.TRUST_LEGALLY_DOMICILED} &nbsp;
                                                                                <span>
                                                                <LinkWithTooltip tooltip={this.state.jsonData.PLANNING_ADVISOR_SUPPLIED_INFORMATION} id="tooltip-1">
                                                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                                </LinkWithTooltip>
                                                            </span>
                                                        </label>
                                                        <FormControl name='countryResidenceId' defaultValue={0} value={this.state.countryResidenceId} className={"selectFormControl " + (this.state.countryResidenceIdTouched && this.state.countryResidenceId == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => { this.trustOnChangeForm(e, 'select', true); this.setState({ state: 0, stateTouched: false }) }} onBlur={(e) => this.valueTouched(e)}>
                                                            <option value={0}>{this.state.jsonData.SELECT_COUNTRY}</option>
                                                            {this.state.countriesList.map((record, index) => {
                                                                return (
                                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                                );
                                                            })}
                                                        </FormControl>
                                                        {this.state.countryResidenceIdTouched && this.state.countryResidenceId == 0 ? <span className="error">{this.Constants.COUNTRY_RESIDENCE_REQUIRED}</span> : null}
                                                    </Col>
                                                    <Col lg={6} md={6} sm={6} xs={12} hidden={this.state.countryResidenceId != 231}>
                                                        <label className="form-label">{this.state.jsonData.STATE_RESIDENCE_TRUST}&nbsp;
                                                                                <span>
                                                                <LinkWithTooltip tooltip={this.state.jsonData.STATE_RESIDENCE_TRUST_TOOLTIP} id="tooltip-1">
                                                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                                </LinkWithTooltip>
                                                            </span>
                                                        </label>
                                                        <FormControl name='stateResidenceId' defaultValue={0} value={this.state.stateResidenceId} className={"selectFormControl " + (this.state.stateResidenceIdTouched && this.state.countryResidenceId == 231 && this.state.stateResidenceId == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => this.trustOnChangeForm(e, 'select', this.state.countryResidenceId == 231 ? true : false)} onBlur={(e) => this.valueTouched(e)}>
                                                            <option value={0}>{this.state.jsonData.SELECT_STATE}</option>
                                                            {this.state.usStatesList.map((record, index) => {
                                                                return (
                                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                                );
                                                            })}
                                                        </FormControl>
                                                        {this.state.stateResidenceIdTouched && this.state.countryResidenceId == 231 && this.state.stateResidenceId == 0 ? <span className="error">{this.Constants.STATE_RESIDENCE_REQUIRED}</span> : null}
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label block">{this.state.jsonData.TAX_EXEMPT_FEDERAL_PURPOSE}</label>
                                                        <Radio name="isEntityTaxExemptForUSFederalIncomeTax1" value={true} className="radioSmallTxtWidth" inline checked={this.state.isEntityTaxExemptForUSFederalIncomeTax} id="yesCheckbox" onChange={(e) => this.trustOnChangeForm(e, 'text', true, 'isEntityTaxExemptForUSFederalIncomeTax')}>&nbsp; Yes
                                                                    <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <Radio name="isEntityTaxExemptForUSFederalIncomeTax1" value={false} className="radioSmallTxtWidth" inline checked={this.state.isEntityTaxExemptForUSFederalIncomeTax == false} id="yesCheckbox" onChange={(e) => this.trustOnChangeForm(e, 'text', true, 'isEntityTaxExemptForUSFederalIncomeTax')}>&nbsp; No
                                                                    <span className="radio-checkmark"></span>
                                                        </Radio>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row" hidden={!this.state.isEntityTaxExemptForUSFederalIncomeTax}>
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label block">{this.state.jsonData.IS_C3_504}</label>
                                                        <Radio name="isTrust501c31" className="radioSmallTxtWidth" inline id="yesCheckbox" value={true} checked={this.state.isTrust501c3 == true} onChange={(e) => this.trustOnChangeForm(e, 'text', true, 'isTrust501c3')}>&nbsp; Yes
                                                                    <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <Radio name="isTrust501c31" className="radioSmallTxtWidth" inline id="yesCheckbox" value={false} checked={this.state.isTrust501c3 == false} onChange={(e) => this.trustOnChangeForm(e, 'text', true, 'isTrust501c3')}>&nbsp; No
                                                                    <span className="radio-checkmark"></span>
                                                        </Radio>
                                                    </Col>
                                                    <Col xs={6} md={6}></Col>
                                                    <span className="error marginLeft15">{this.state.isTrust501c31Msz}</span>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={12} md={12}>
                                                        <label className="form-label width100 block">{this.state.jsonData.TRUST_REQUIRED_FOIA}</label>
                                                        <Radio name="releaseInvestmentEntityRequired2" className="radioSmallTxtWidth" inline id="yesCheckbox" value={true} checked={this.state.releaseInvestmentEntityRequired == true} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'releaseInvestmentEntityRequired')}>&nbsp; Yes
                                                                    <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <Radio name="releaseInvestmentEntityRequired2" className="radioSmallTxtWidth" inline id="yesCheckbox" value={false} checked={this.state.releaseInvestmentEntityRequired == false} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'releaseInvestmentEntityRequired')}>&nbsp; No
                                                                    <span className="radio-checkmark"></span>
                                                        </Radio>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row" hidden={!this.state.releaseInvestmentEntityRequired}>
                                                    <Col xs={12} md={12}>
                                                        <label className="form-label width100">Please describe the manner in which the Trust or its direct or indirect beneficial owners are subject to FOIA or similar statutes.</label>
                                                    </Col>
                                                    <Col xs={6} md={6}>
                                                        <FormControl componentClass="textarea" name="indirectBeneficialOwnersSubjectFOIAStatutes" className={"inputFormControl textarea " + (this.state.indirectBeneficialOwnersSubjectFOIAStatutesBorder ? 'inputError' : '')} value={this.state.indirectBeneficialOwnersSubjectFOIAStatutes} onChange={(e) => this.investorHandleChangeEvent(e, 'indirectBeneficialOwnersSubjectFOIAStatutes', 'INDIRECT_BENIFICIAL_REQUIRED')} onBlur={(e) => this.investorHandleChangeEvent(e, 'indirectBeneficialOwnersSubjectFOIAStatutes', 'INDIRECT_BENIFICIAL_REQUIRED')} />
                                                        <span className="error">{this.state.indirectBeneficialOwnersSubjectFOIAStatutesMsz}</span>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={12} md={12}>
                                                        <label className="form-label width100 block" dangerouslySetInnerHTML={{
                                                            __html: this.checkTooltips(this.state.jsonData.INVESTOR_OR_BENEFICIAL_OWNER, 'obj3')
                                                        }}>
                                                        </label>
                                                        <Radio name="isSubjectToDisqualifyingEvent3" className="radioSmallTxtWidth" inline id="yesCheckbox" value={true} checked={this.state.isSubjectToDisqualifyingEvent == true} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'isSubjectToDisqualifyingEvent')}>&nbsp; Yes
                                                                    <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <Radio name="isSubjectToDisqualifyingEvent3" className="radioSmallTxtWidth" inline id="yesCheckbox" value={false} checked={this.state.isSubjectToDisqualifyingEvent == false} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'isSubjectToDisqualifyingEvent')}>&nbsp; No
                                                                    <span className="radio-checkmark"></span>
                                                        </Radio>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={12} md={12}>
                                                        <label className="form-label width100 block">{this.state.jsonData.DISCLOSE_FUND_MANAGER}:</label>
                                                        <FormControl componentClass="textarea" placeholder={this.state.jsonData.FUND_MANAGER_INFORMATION} name="fundManagerInfo" value={this.state.fundManagerInfo} className="inputFormControl textarea col-lg-6 col-md-6" onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                    </Col>
                                                </Row>
                                            </div>
                                            :
                                            <div>
                                                <Row className="step1Form-row">
                                                    <Col xs={12} md={12}>
                                                        <label className="title">{this.state.jsonData.TRUST_PRIMARY_MAILING}</label>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.ADDRESS}</label>
                                                        <FormControl componentClass="textarea" placeholder={this.state.jsonData.ADDRESS} name="mailingAddressStreet" value={this.state.mailingAddressStreet} className={"inputFormControl textarea " + (this.state.mailingAddressStreetTouched && !this.state.mailingAddressStreet ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                        {this.state.mailingAddressStreetTouched && !this.state.mailingAddressStreet ? <span className="error">{this.Constants.STREET_REQUIRED}</span> : null}
                                                    </Col>

                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.CITY}</label>
                                                        <FormControl type="text" placeholder={this.state.jsonData.CITY} name="mailingAddressCity" value={this.state.mailingAddressCity} className={"inputFormControl " + (this.state.mailingAddressCityTouched && !this.state.mailingAddressCity ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                        {this.state.mailingAddressCityTouched && !this.state.mailingAddressCity ? <span className="error">{this.Constants.CITY_REQUIRED}</span> : null}
                                                    </Col>
                                                    <Col md={6} xs={6}>
                                                        <label className="form-label">{this.state.jsonData.COUNTRY}</label>
                                                        <FormControl name='mailingAddressCountry' defaultValue={0} value={this.state.mailingAddressCountry} className={"selectFormControl " + (this.state.mailingAddressCountryTouched && this.state.mailingAddressCountry == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => { this.trustOnChangeForm(e, 'select', true); this.setState({ state: 0, stateTouched: false }) }} onBlur={(e) => this.valueTouched(e)}>
                                                            <option value={0}>{this.state.jsonData.SELECT_COUNTRY}</option>
                                                            {this.state.countriesList.map((record, index) => {
                                                                return (
                                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                                );
                                                            })}
                                                        </FormControl>
                                                        {this.state.mailingAddressCountryTouched && this.state.mailingAddressCountry == 0 ? <span className="error">{this.Constants.COUNTRY_REQUIRED}</span> : null}
                                                    </Col>

                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col md={6} xs={6}>
                                                        <label className="form-label">{this.state.jsonData.STATE}</label>
                                                        <FormControl name='mailingAddressState' defaultValue={0} value={!this.state.mailingAddressState ? this.state.stateResidenceId : this.state.mailingAddressState} className={"selectFormControl " + (this.state.mailingAddressStateTouched && this.state.mailingAddressState == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => this.trustOnChangeForm(e, 'select', this.state.mailingAddressCountry == 231 ? true : false)} onBlur={(e) => this.valueTouched(e)}>
                                                            <option value={0}>{this.state.jsonData.SELECT_STATE}</option>
                                                            {this.state.statesList.map((record, index) => {
                                                                return (
                                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                                );
                                                            })}
                                                        </FormControl>
                                                        {this.state.mailingAddressStateTouched && this.state.mailingAddressState == 0 ? <span className="error">{this.Constants.STATE_REQUIRED}</span> : null}
                                                    </Col>
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.ZIP_CODE}</label>
                                                        <FormControl type="text" placeholder={this.state.jsonData.ZIP_CODE} name="mailingAddressZip" onKeyPress={(e) => { this.isNumberKey(e) }} value={this.state.mailingAddressZip} className={"inputFormControl " + (this.state.mailingAddressZipTouched && !this.state.mailingAddressZip ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                        {this.state.mailingAddressZipTouched && !this.state.mailingAddressZip ? <span className="error">{this.Constants.ZIP_REQUIRED}</span> : null}
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.TRUST_TELEPHONE_NUMBER}</label>
                                                        <PhoneInput name="mailingAddressPhoneNumber" value={this.state.mailingAddressPhoneNumber} onChange={e => { this.trustOnChangeForm(e, 'mobile', false, 'mailingAddressPhoneNumber') }} maxLength="14" placeholder={this.state.jsonData.TRUST_TELEPHONE_NUMBER_PLACEHOLDER} country="US" />
                                                    </Col>
                                                </Row>

                                            </div>
                                    }
                                </div>
                            
                            
                                <div id="IrrecoverableTrust" hidden={this.state.investorSubType !== 10}>
                                    {
                                        this.state.showTrustIrrevocableInvestorPage1
                                            ?
                                            <div>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.ENTER_TRUST_NAME} &nbsp;
                                                                            <span>
                                                                <LinkWithTooltip tooltip={this.state.jsonData.ENTIRE_TRUST_LEGAL_NAME} id="tooltip-1">
                                                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                                </LinkWithTooltip>
                                                            </span>
                                                        </label>
                                                        <FormControl type="text" name='trustName' placeholder={this.state.jsonData.ENTER_TRUST_NAME} value={this.state.trustName} className="inputFormControl" onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                        <span className="error"></span>
                                                    </Col>
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.EMAIL_ADDRESS}</label>
                                                        <FormControl name='email' type="email" name="email" placeholder={this.state.jsonData.EMAIL_ADDRESS_PLACEHOLDER} className="inputFormControl" readOnly value={this.state.email} />
                                                        <span className="error"></span>
                                                    </Col>

                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col lg={6} md={6} sm={6} xs={12}>
                                                        <label className="form-label">{this.state.jsonData.TRUST_LEGALLY_DOMICILED} &nbsp;
                                                                                        <span>
                                                                <LinkWithTooltip tooltip={this.state.jsonData.PLANNING_ADVISOR_SUPPLIED_INFORMATION} id="tooltip-1">
                                                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                                </LinkWithTooltip>
                                                            </span>
                                                        </label>
                                                        <FormControl name='countryResidenceId' defaultValue={0} value={this.state.countryResidenceId} className={"selectFormControl " + (this.state.countryResidenceIdTouched && this.state.countryResidenceId == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => { this.trustOnChangeForm(e, 'select', false); this.setState({ state: 0, stateTouched: false }) }} onBlur={(e) => this.valueTouched(e)}>
                                                            <option value={0}>{this.state.jsonData.SELECT_COUNTRY}</option>
                                                            {this.state.countriesList.map((record, index) => {
                                                                return (
                                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                                );
                                                            })}
                                                        </FormControl>
                                                        {this.state.countryResidenceIdTouched && this.state.countryResidenceId == 0 ? <span className="error">{this.Constants.COUNTRY_RESIDENCE_REQUIRED}</span> : null}
                                                    </Col>
                                                    <Col lg={6} md={6} sm={6} xs={12} hidden={this.state.countryResidenceId != 231}>
                                                        <label className="form-label">{this.state.jsonData.STATE_RESIDENCE_TRUST}&nbsp;
                                                                                <span>
                                                                <LinkWithTooltip tooltip={this.state.jsonData.STATE_RESIDENCE_TRUST_TOOLTIP} id="tooltip-1">
                                                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                                </LinkWithTooltip>
                                                            </span>
                                                        </label>
                                                        <FormControl name='stateResidenceId' defaultValue={0} value={this.state.stateResidenceId} className={"selectFormControl " + (this.state.stateResidenceIdTouched && this.state.country == 231 && this.state.stateResidenceId == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => this.trustOnChangeForm(e, 'select', this.state.countryResidenceId == 231 ? true : false)} onBlur={(e) => this.valueTouched(e)}>
                                                            <option value={0}>{this.state.jsonData.SELECT_STATE}</option>
                                                            {this.state.usStatesList.map((record, index) => {
                                                                return (
                                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                                );
                                                            })}
                                                        </FormControl>
                                                        {this.state.stateResidenceIdTouched && this.state.countryResidenceId == 231 && this.state.stateResidenceId == 0 ? <span className="error">{this.Constants.STATE_RESIDENCE_REQUIRED}</span> : null}
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label block">{this.state.jsonData.TAX_EXEMPT_FEDERAL_PURPOSE}</label>
                                                        <Radio name="isEntityTaxExemptForUSFederalIncomeTax2" value={true} className="radioSmallTxtWidth" inline checked={this.state.isEntityTaxExemptForUSFederalIncomeTax} id="yesCheckbox" onChange={(e) => this.trustOnChangeForm(e, 'text', true, 'isEntityTaxExemptForUSFederalIncomeTax')}>&nbsp; Yes
                                                                                <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <Radio name="isEntityTaxExemptForUSFederalIncomeTax2" value={false} className="radioSmallTxtWidth" inline checked={this.state.isEntityTaxExemptForUSFederalIncomeTax == false} id="yesCheckbox" onChange={(e) => this.trustOnChangeForm(e, 'text', true, 'isEntityTaxExemptForUSFederalIncomeTax')}>&nbsp; No
                                                                                <span className="radio-checkmark"></span>
                                                        </Radio>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row" hidden={!this.state.isEntityTaxExemptForUSFederalIncomeTax}>
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label block">{this.state.jsonData.IS_C3_504}</label>
                                                        <Radio name="isTrust501c32" className="radioSmallTxtWidth" inline id="yesCheckbox" value={true} checked={this.state.isTrust501c3 == true} onChange={(e) => this.trustOnChangeForm(e, 'text', true, 'isTrust501c3')}>&nbsp; Yes
                                                                            <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <Radio name="isTrust501c32" className="radioSmallTxtWidth" inline id="yesCheckbox" value={false} checked={this.state.isTrust501c3 == false} onChange={(e) => this.trustOnChangeForm(e, 'text', true, 'isTrust501c3')}>&nbsp; No
                                                                            <span className="radio-checkmark"></span>
                                                        </Radio>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={12} md={12}>
                                                        <label className="form-label width100 block">{this.state.jsonData.TRUST_REQUIRED_FOIA}</label>
                                                        <Radio name="releaseInvestmentEntityRequired3" className="radioSmallTxtWidth" inline id="yesCheckbox" value={true} checked={this.state.releaseInvestmentEntityRequired == true} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'releaseInvestmentEntityRequired')}>&nbsp; Yes
                                                                                <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <Radio name="releaseInvestmentEntityRequired3" className="radioSmallTxtWidth" inline id="yesCheckbox" value={false} checked={this.state.releaseInvestmentEntityRequired == false} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'releaseInvestmentEntityRequired')}>&nbsp; No
                                                                                <span className="radio-checkmark"></span>
                                                        </Radio>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row" hidden={!this.state.releaseInvestmentEntityRequired}>
                                                    <Col xs={12} md={12}>
                                                        <label className="form-label width100">Please describe the manner in which the Trust or its direct or indirect beneficial owners are subject to FOIA or similar statutes.</label>
                                                    </Col>
                                                    <Col xs={6} md={6}>
                                                        <FormControl componentClass="textarea" name="indirectBeneficialOwnersSubjectFOIAStatutes" className={"inputFormControl textarea " + (this.state.indirectBeneficialOwnersSubjectFOIAStatutesBorder ? 'inputError' : '')} value={this.state.indirectBeneficialOwnersSubjectFOIAStatutes} onChange={(e) => this.investorHandleChangeEvent(e, 'indirectBeneficialOwnersSubjectFOIAStatutes', 'INDIRECT_BENIFICIAL_REQUIRED')} onBlur={(e) => this.investorHandleChangeEvent(e, 'indirectBeneficialOwnersSubjectFOIAStatutes', 'INDIRECT_BENIFICIAL_REQUIRED')} />
                                                        <span className="error">{this.state.indirectBeneficialOwnersSubjectFOIAStatutesMsz}</span>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={12} md={12}>
                                                        <label className="form-label width100 block" dangerouslySetInnerHTML={{
                                                            __html: this.checkTooltips(this.state.jsonData.INVESTOR_OR_BENEFICIAL_OWNER, 'obj4')
                                                        }}>
                                                        </label>
                                                        <Radio name="isSubjectToDisqualifyingEvent4" className="radioSmallTxtWidth" inline id="yesCheckbox" value={true} checked={this.state.isSubjectToDisqualifyingEvent == true} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'isSubjectToDisqualifyingEvent')}>&nbsp; Yes
                                                                            <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <Radio name="isSubjectToDisqualifyingEvent4" className="radioSmallTxtWidth" inline id="yesCheckbox" value={false} checked={this.state.isSubjectToDisqualifyingEvent == false} onChange={(e) => this.trustOnChangeForm(e, 'text', false, 'isSubjectToDisqualifyingEvent')}>&nbsp; No
                                                                            <span className="radio-checkmark"></span>
                                                        </Radio>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={12} md={12}>
                                                        <label className="form-label width100 block">{this.state.jsonData.DISCLOSE_FUND_MANAGER}:</label>
                                                        <FormControl componentClass="textarea" placeholder={this.state.jsonData.FUND_MANAGER_INFORMATION} name="fundManagerInfo" value={this.state.fundManagerInfo} className="inputFormControl textarea col-lg-6 col-md-4" onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                    </Col>
                                                </Row>
                                            </div>
                                            :
                                            <div>
                                                <Row className="step1Form-row">
                                                    <Col xs={12} md={12}>
                                                        <label className="title">{this.state.jsonData.TRUST_PRIMARY_MAILING}</label>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.ADDRESS}</label>
                                                        <FormControl componentClass="textarea" placeholder={this.state.jsonData.ADDRESS} name="mailingAddressStreet" value={this.state.mailingAddressStreet} className={"inputFormControl textarea " + (this.state.mailingAddressStreetTouched && !this.state.mailingAddressStreet ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                        {this.state.mailingAddressStreetTouched && !this.state.mailingAddressStreet ? <span className="error">{this.Constants.STREET_REQUIRED}</span> : null}
                                                    </Col>

                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.CITY}</label>
                                                        <FormControl type="text" placeholder={this.state.jsonData.CITY} name="mailingAddressCity" value={this.state.mailingAddressCity} className={"inputFormControl " + (this.state.mailingAddressCityTouched && !this.state.mailingAddressCity ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                        {this.state.mailingAddressCityTouched && !this.state.mailingAddressCity ? <span className="error">{this.Constants.CITY_REQUIRED}</span> : null}
                                                    </Col>
                                                    <Col md={6} xs={6}>
                                                        <label className="form-label">{this.state.jsonData.COUNTRY}</label>
                                                        <FormControl name='mailingAddressCountry' defaultValue={0} value={this.state.mailingAddressCountry} className={"selectFormControl " + (this.state.mailingAddressCountryTouched && this.state.mailingAddressCountry == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => { this.trustOnChangeForm(e, 'select', true); this.setState({ state: 0, stateTouched: false }) }} onBlur={(e) => this.valueTouched(e)}>
                                                            <option value={0}>{this.state.jsonData.SELECT_COUNTRY}</option>
                                                            {this.state.countriesList.map((record, index) => {
                                                                return (
                                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                                );
                                                            })}
                                                        </FormControl>
                                                        {this.state.mailingAddressCountryTouched && this.state.mailingAddressCountry == 0 ? <span className="error">{this.Constants.COUNTRY_REQUIRED}</span> : null}
                                                    </Col>

                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col md={6} xs={6}>
                                                        <label className="form-label">{this.state.jsonData.STATE}</label>
                                                        <FormControl name='mailingAddressState' defaultValue={0} value={!this.state.mailingAddressState ? this.state.stateResidenceId : this.state.mailingAddressState} className={"selectFormControl " + (this.state.mailingAddressStateTouched && this.state.mailingAddressState == 0 ? 'inputError' : '')} componentClass="select" placeholder={this.state.jsonData.SELECT_INVESTOR_SUB_TYPE} onChange={(e) => this.trustOnChangeForm(e, 'select', this.state.mailingAddressCountry == 231 ? true : false)} onBlur={(e) => this.valueTouched(e)}>
                                                            <option value={0}>{this.state.jsonData.SELECT_STATE}</option>
                                                            {this.state.statesList.map((record, index) => {
                                                                return (
                                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                                );
                                                            })}
                                                        </FormControl>
                                                        {this.state.mailingAddressStateTouched && this.state.mailingAddressState == 0 ? <span className="error">{this.Constants.STATE_REQUIRED}</span> : null}
                                                    </Col>
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.ZIP_CODE}</label>
                                                        <FormControl type="text" placeholder={this.state.jsonData.ZIP_CODE} onKeyPress={(e) => { this.isNumberKey(e) }} name="mailingAddressZip" value={this.state.mailingAddressZip} className={"inputFormControl " + (this.state.mailingAddressZipTouched && !this.state.mailingAddressZip ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                        {this.state.mailingAddressZipTouched && !this.state.mailingAddressZip ? <span className="error">{this.Constants.ZIP_REQUIRED}</span> : null}
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={12} md={12}>
                                                        <label className="form-label width100">{this.state.jsonData.LEGAL_TITLE_DESIGNATION}</label>
                                                        <Col xs={6} md={6} className="padding-left-0">
                                                            <FormControl type="text" placeholder={this.state.jsonData.LEGAL_TITLE} name="legalTitleDesignation" value={this.state.legalTitleDesignation} className={"inputFormControl " + (this.state.legalTitleDesignationTouched && !this.state.legalTitleDesignation ? 'inputError' : '')} onChange={(e) => this.trustOnChangeForm(e, 'text', true)} onBlur={(e) => this.valueTouched(e)} />
                                                            {this.state.legalTitleDesignationTouched && !this.state.legalTitleDesignation ? <span className="error">{this.Constants.LEGAL_DESIGNATION_REQUIRED}</span> : null}
                                                        </Col>
                                                    </Col>
                                                </Row>
                                                <Row className="step1Form-row">
                                                    <Col xs={6} md={6}>
                                                        <label className="form-label">{this.state.jsonData.TRUST_TELEPHONE_NUMBER}</label>
                                                        <PhoneInput name="mailingAddressPhoneNumber" value={this.state.mailingAddressPhoneNumber} onChange={e => { this.trustOnChangeForm(e, 'mobile', false, 'mailingAddressPhoneNumber') }} maxLength="14" placeholder={this.state.jsonData.TRUST_TELEPHONE_NUMBER_PLACEHOLDER} country="US" />
                                                    </Col>
                                                </Row>
                                            </div>
                                    }
                                </div>
                            
                        </div>

                    </div>
                </div>
                <div className="margin30 error">{this.state.investorInfoErrorMsz}</div>
                <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.lpsubscriptionTotalObj.subscriptionStatus && this.state.lpsubscriptionTotalObj.subscriptionStatus.name === 'Closed'}>
                        <i className={"fa fa-chevron-left " + (!this.state.enableLeftIcon ? 'disabled' : '')} onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className={"fa fa-chevron-right " + (!this.state.investorPageValid ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Modal id="confirmInvestorModal" backdrop="static" show={this.state.showConfirmationModal} onHide={this.closeConfirmationModal} dialogClassName="confirmInvestorDialog confirmInvestorModalWidth">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        {
                            this.state.investorType && this.state.investorType.toLowerCase() == 'individual'
                                ?
                                <div>
                                    <div className="title-text">{this.state.jsonData.FUND_REGISTER_INTEREST} <span className="fundNameCenter">{this.state.investorType && this.state.investorType.toLowerCase() == 'individual' && this.state.areYouSubscribingAsJointIndividual && this.state.typeOfLegalOwnership ? this.state.legalTitle : this.state.lpsubscriptionTotalObj.offlineLp ? this.FsnetUtil.getFullName(this.state.lpsubscriptionTotalObj.offlineLp) : this.FsnetUtil.userName()}</span> {this.state.jsonData.DESIGNATION_CAN_NOT_CHANGE} </div>
                                </div>
                                :
                                (this.state.investorType && this.state.investorType.toLowerCase() == 'trust'
                                    ?
                                    <div className="title-text title-text-center"><p>{this.state.jsonData.FUND_REGISTER_INTEREST} <span className="fundNameCenter">{this.state.trustName}</span> {this.state.jsonData.DESIGNATION_CAN_NOT_CHANGE}</p></div>
                                    :
                                    <div className="title-text title-text-center"><p>{this.state.jsonData.FUND_REGISTER_INTEREST} <span className="fundNameCenter">{this.state.entityName}</span> {this.state.jsonData.DESIGNATION_CAN_NOT_CHANGE}</p></div>
                                )
                        }
                        {
                            (this.state.investorType && this.state.investorType.toLowerCase() == 'individual' && this.state.areYouSubscribingAsJointIndividual && this.state.typeOfLegalOwnership) &&
                                <div className="title-text">
                                    <div className="title-text-center">Your Signatory Title for the Joint Individual will be:</div>
                                    <p className="fundNameCenter">{this.state.lpsubscriptionTotalObj.offlineLp ? this.FsnetUtil.getFullName(this.state.lpsubscriptionTotalObj.offlineLp) : this.FsnetUtil.userName()}</p>
                                </div>
                        }
                        <div className="subtext marginTop10">{this.state.jsonData.REVIEW_CONFIRM_CAREFULLY}</div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeConfirmationModal}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" onClick={this.confirmSubmit} className="fsnetSubmitButton btnEnabled" >{this.state.jsonData.CONFIRM}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <Modal id="uniqueCheckModal" backdrop="static" show={this.state.titleUniqueCheckModal} onHide={this.closeTitleUniqueCheckModal}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title">Error</h1>
                        <div className="title-md error font14 marginTop20">{this.state.investorType == 'Individual' ? 'It is not possible to add a second investment as an Individual or Joint Individual.  You may add as many distinct Trust or Entity Investors as you would like.' : 'It is not possible to add a second investment as a Trust or Entity in identical legal title to a previously established Investor in the Fund.  You may add as many distinct Trust or Entity Investors as you would like.'}</div>
                    </Modal.Body>
                </Modal>
                <Modal id="confirmInvestorModal" backdrop="static" show={this.state.openPreviousFund} onHide={this.closePreviousFundModal} dialogClassName="previousSubscriptionDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <p className="title-text marginLeftNone">Please select a Fund to which you have previously subscribed, and all Subscription Form information will be pre-populated. You will be able to review before submitting.</p>
                        <div className="display-filter marginTop10 display-filter-padding display-left-filter marginBottom10 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <span className="searchPreviousPartnership">
                                <i className="fa fa-search" aria-hidden="true"></i>
                            </span>
                            <FormControl type="text" placeholder="Search Funds" className="formFilterControl form-control" onChange={(e) => this.searchFunds(e)} />
                        </div>
                        <div className="search-container">
                            {this.state.previousFundList.length > 0 ?
                                this.state.previousFundList.map((record, index) => {
                                    return (
                                        <div key={index} className="form-label">
                                            <Radio name="previousFund" value={record['id']} inline onChange={(e) => this.setState({ prepopulateId: e.target.value })}>&nbsp; {record.legalEntity}
                                                <span className="radio-checkmark"></span>
                                            </Radio>
                                        </div>
                                    );
                                })
                                :
                                <div className="title-text height20 marginTop20 marginBottom30">{this.Constants.NO_PREPOPULATE_SEARCH_FUNDS}</div>
                            }
                        </div>
                        <span className="error">{this.state.prepopulateErrorMsz}</span>
                        <div className="populateFormButtonAlign marginTop10">
                            <Button type="button" className="fsnetSubmitButton btnEnabled" disabled={this.state.prepopulateId === null} onClick={this.submitPrePopulate}>Populate Form</Button>
                        </div>
                    </Modal.Body>
                </Modal>
                {/* Rescind From Fund Modal */}
                <Modal backdrop="static" show={this.state.investmentAvailableForRestoreModal} onHide={this.closeinvestmentAvailableForRestoreModal} dialogClassName="delteFundDocDailog">
                    <Modal.Header className="heightNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title">Restore Investment</h1>
                        <div className="subtext minHeight50 marginTop10">You are attempting to subscribe to {this.state.legalEntity} with a Legal Title that has already been registered with Vanilla, but was rescinded from the Fund.  Do you want to restore the original investment with this same legal title?</div>
                        <Row className="marginTop20">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton marginLeft30" onClick={this.closeinvestmentAvailableForRestoreModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled marginLeft40" onClick={this.restoreInvestment}>Restore</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default InvestorInformationComponent;


import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Radio, Row, Col, FormControl, OverlayTrigger, Tooltip, Modal } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';

var tooltipsCount=0,objList=[], objListArray=[];
class QualifiedPurchaserComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            showModal: false,
            investorType: 'LLC',
            areYouQualifiedPurchaser: null,
            areYouQualifiedClient: null,
            qualifiedPurchaserPageValid: false,
            investorObj: {},
            qualifiedPurchaserErrorMsz:'',
            jsonData:{},
            isFormChanged:false
        }

    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.getJsonData();
        this.getSubscriptionDetails(id);
    }

    checkTooltips(text,type) {
        let tooltipWords = ['Qualified Purchaser', 'Companies Act', 'Qualified Client', 'Advisers Act'];
        let count = 0 ;
        for(let idx=0; idx<tooltipWords.length; idx++) {
            if(text && text.indexOf(tooltipWords[idx]) > -1 && objList.indexOf(type) === -1) {
                let arrayList = this.FsnetUtil.allIndexOf(text,tooltipWords[idx])
                let splitText = [];
                let currentIndex = 0;
                for(let jdx=0; jdx<arrayList.length; jdx++) {
                    let jdxLength = arrayList[jdx]+tooltipWords[idx].length;
                    if(jdx === (arrayList.length-1)) {
                        splitText.push(text.substr(currentIndex,text.length))
                    } else {
                        splitText.push(text.substr(currentIndex,jdxLength))
                        currentIndex = jdxLength
                    }
                }
                let newText = ''
                if(splitText.length === 1) {
                    count++;
                    let indexPos = tooltipsCount+count
                    text = text.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                }else {
                    for(let pos of splitText) {
                        count++;
                        let indexPos = tooltipsCount+count
                        newText = newText+pos.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                    }
                    text = newText;
                }

            }
            if(text && idx === tooltipWords.length-1 && objList.indexOf(type) === -1) {
                objListArray.push({type:type,text:text})
                objList.push(type)
            }
        }
        tooltipsCount = tooltipsCount+count;
        for(let index of objListArray) {
            if(index['type'] && index['text']) {
                if(index['type'] === type) {
                    return index['text'];
                }
            }
        }
        return text
    }

    addClickEvent(obj) {
        if(tooltipsCount > 0) {
            for(let idx=0; idx<tooltipsCount; idx++) {
                if(document.getElementById("anchor"+(idx+1))) {
                    document.getElementById("anchor"+(idx+1)).addEventListener("click", function(){
                        let name = document.getElementById("anchor"+(idx+1)).getAttribute('tooltip_id');
                        let modalType, type;
                        if(name === 'Qualified Purchaser') {
                            modalType = 'qualifier';
                            type = 'qualifierModal'
                        } else if(name === 'Companies Act') {
                            modalType = 'actModalWindow';
                            type = 'company'
                        }else if(name === 'Qualified Client') {
                            modalType = 'qualifier';
                            type = 'qualifierClientModal'
                        }else if(name === 'Advisers Act') {
                            modalType = 'actModalWindow';
                            type = 'adviser'
                        }
                        PubSub.publish('openModal', {investorType: obj.investorType, investorSubType: obj.investorSubType, modalType: modalType, type: type});
                    });
                }
            }
        }
    }

    getJsonData() {
        this.Fsnethttp.getJson('qualifiedPurchaser').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getSubscriptionDetails(id) {
        
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    let obj = result.data.data;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    obj['currentPageCount'] = 3;
                    obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                    PubSub.publish('investorData',obj );
                    this.setState({
                        investorObj: result.data.data,
                        investorType: result.data.data.investorType?result.data.data.investorType:'LLC',
                        investorSubType: result.data.data.investorSubType ? result.data.data.investorSubType : (result.data.data.investorType == 'Trust' ? 9 : 0),
                        areYouQualifiedPurchaser: result.data.data.areYouQualifiedPurchaser,
                        areYouQualifiedClient: result.data.data.areYouQualifiedClient
                    },()=>{
                        this.updateInvestorInputFields();
                        this.addClickEvent(result.data.data);
                    })
                }
            })
            .catch(error => {
                this.close();
            });
        }
    }

    updateInvestorInputFields() {
        if(this.state.areYouQualifiedPurchaser === true) {
            this.setState({
                qualifiedPurchaserPageValid: true,
            })
        } else if(this.state.areYouQualifiedPurchaser == false && this.state.areYouQualifiedPurchaser != null) {
            this.setState({
                qualifiedPurchaserPageValid: true,
            })
        }else {
            this.setState({
                qualifiedPurchaserPageValid: false
            })
        }
    }

    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    //Show Purchaser Modal
    openTooltip = () => {
        // this.setState({ showTooltipModal: true });
        PubSub.publish('openModal', {investorType: this.state.investorType, investorSubType: this.state.investorSubType, modalType: 'qualifier', type: 'qualifierModal'});
    }

    //Show Company Act Modal
    openCompanyModal = () => {
        PubSub.publish('openModal', {investorType: this.state.investorType, investorSubType: this.state.investorSubType, modalType: 'actModalWindow', type: 'company'});
    }

    openAdvisetModal = () => {
        PubSub.publish('openModal', {investorType: this.state.investorType, investorSubType: this.state.investorSubType, modalType: 'actModalWindow', type: 'adviser'});
    }

    //Show Qualified client Modal
    qualifiedClientTooltip = () => {
        // this.setState({ showQualifiedClientModal: true });
        PubSub.publish('openModal', {investorType: this.state.investorType, investorSubType: this.state.investorSubType, modalType: 'qualifier', type: 'qualifierClientModal'});
    }

    qualifiedPurchaserChangeEvent = (event, type, value) => {
        this.setState({
            [type]:value,
            isFormChanged:true
        })

        if(type === 'areYouQualifiedPurchaser') {
            if(value === false) {
                this.setState({
                    areYouQualifiedClient:null,
                    qualifiedPurchaserPageValid: false,
                }) 
            } else {
                this.setState({
                    qualifiedPurchaserPageValid: true,
                }) 
            }
        }

        if(type === 'areYouQualifiedClient') {
            this.setState({
                qualifiedPurchaserPageValid: true,
            },()=>{
                this.updateInvestorInputFields();
            }) 
        }
    }

    proceedToNext = () => {
        let postobj = {investorType:this.state.investorType,investorSubType: this.state.investorSubType, subscriptionId:this.state.investorObj.id, step:4,areYouQualifiedPurchaser:this.state.areYouQualifiedPurchaser,isFormChanged:this.state.isFormChanged }
        this.state.areYouQualifiedPurchaser === false ? postobj['areYouQualifiedClient'] = this.state.areYouQualifiedClient:postobj['areYouQualifiedClient'] = true
        this.open();
        
        this.Fsnethttp.updateLpSubscriptionDetails(postobj).then(result => {
            this.close();
            if (result.data) {
                 if(this.state.investorType === 'Individual') {
                    if(this.state.investorObj.investorQuestions.length > 0) {
                        this.props.history.push('/lp/additionalQuestions/'+this.FsnetUtil._encrypt(this.state.investorObj.id));
                    } else {
                        this.props.history.push('/lp/capitalCommitment/'+this.FsnetUtil._encrypt(this.state.investorObj.id));
                    }
                } else {
                    this.props.history.push('/lp/companiesAct/'+this.FsnetUtil._encrypt(this.state.investorObj.id));
                }
            }
        })
        .catch(error => {
            this.close();
            if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                this.setState({
                    qualifiedPurchaserErrorMsz: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    qualifiedPurchaserErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                });
            }
        });
    }

    proceedToBack = () => {
        this.props.history.push('/lp/AccreditedInvestor/'+this.FsnetUtil._encrypt(this.state.investorObj.id));
    }


    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
              <OverlayTrigger
                trigger={['click', 'hover', 'focus']}
                overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                placement="left"
                delayShow={300}
                delayHide={150}
                rootClose
              >
                <span>{children}</span>
              </OverlayTrigger>
            );
          }
        return (
            <div className="qualifiedPurchaser width100 marginTopSticky">
                <div className="form-grid formGridDivMargin min-height-400">
                        <div className="title">{this.state.jsonData.QUALIFIED_PURCHASER}</div>
                        <Row className="step1Form-row">
                            <Col xs={12} md={12}>
                                <label className="form-label width100" hidden={this.state.investorType !== 'Individual'}  dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.IS_YOUR_INDIVIDUAL_QUALIFIED_PURCHASER_MEANING_SECTION_2A_51,'obj1')
                                    }}>
                                </label>
                                <label className="form-label width100" hidden={this.state.investorType !== 'LLC'} dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.IS_YOUR_ENTITY_PURCHASER_MEANING_SECTION_2A_51,'obj2')
                                    }}>
                                </label>
                                <label className="form-label width100" hidden={this.state.investorType !== 'Trust'} dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.IS_YOUR_TRUST_PURCHASER_MEANING_SECTION_2A_51,'obj3')
                                    }}>
                                </label>
                                <Radio name="qualifiedPurchaser" inline checked={this.state.areYouQualifiedPurchaser === true} onChange={(e) => this.qualifiedPurchaserChangeEvent(e, 'areYouQualifiedPurchaser', true)}>&nbsp; Yes
                                    <span className="radio-checkmark"></span>
                                </Radio>
                                <Radio name="qualifiedPurchaser" inline checked={this.state.areYouQualifiedPurchaser === false} onChange={(e) => this.qualifiedPurchaserChangeEvent(e, 'areYouQualifiedPurchaser', false)}>&nbsp; No
                                    <span className="radio-checkmark"></span>
                                </Radio>
                            </Col>
                        </Row>
                        {/* Qualified Client starts for individual investor type*/}
                                <Row className="step1Form-row" hidden={this.state.areYouQualifiedPurchaser != false}>
                                    <Col xs={12} md={12}>
                                        <label className="form-label width100" hidden={this.state.investorType !== 'Individual'} dangerouslySetInnerHTML={{
                                                                __html: this.checkTooltips(this.state.jsonData.IS_YOUR_INDIVIDUAL_QUALIFIED_CLIENT_MEANING_RULE_205_3,'obj4')
                                            }}>
                                        </label>
                                        <label className="form-label width100" hidden={this.state.investorType !== 'LLC'} dangerouslySetInnerHTML={{
                                                                __html: this.checkTooltips(this.state.jsonData.IS_YOUR_ENTITY_QUALIFIED_CLIENT_MEANING_RULE_205_3,'obj5')
                                            }}>
                                        </label>
                                        <label className="form-label width100" hidden={this.state.investorType !== 'Trust'} dangerouslySetInnerHTML={{
                                                                __html: this.checkTooltips(this.state.jsonData.IS_YOUR_TRUST_QUALIFIED_CLIENT_MEANING_RULE_205_3,'obj6')
                                            }}>
                                            
                                        </label>
                                        <Radio name="qualifiedClient" inline checked={this.state.areYouQualifiedClient === true} onChange={(e) => this.qualifiedPurchaserChangeEvent(e, 'areYouQualifiedClient', true)}>&nbsp; Yes
                                            <span className="radio-checkmark"></span>
                                        </Radio>
                                        <Radio name="qualifiedClient" inline checked={this.state.areYouQualifiedClient === false} onChange={(e) => this.qualifiedPurchaserChangeEvent(e, 'areYouQualifiedClient', false)}>&nbsp; No
                                            <span className="radio-checkmark"></span>
                                        </Radio>
                                    </Col>
                                </Row>
                </div>
                <div className="margin30 error">{this.state.qualifiedPurchaserErrorMsz}</div>
                <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.investorObj.subscriptionStatus && this.state.investorObj.subscriptionStatus.name === 'Closed'}>
                        <i className="fa fa-chevron-left" aria-hidden="true" onClick={this.proceedToBack}></i>
                        <i className={"fa fa-chevron-right " + (!this.state.qualifiedPurchaserPageValid ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default QualifiedPurchaserComponent;


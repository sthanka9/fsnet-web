import React, { Component } from 'react';
import './lpsubscriptionform.component.css';
import Loader from '../../widgets/loader/loader.component';
import { Constants } from '../../constants/constants';
import { Row, Col, Button, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Fsnethttp } from '../../services/fsnethttp';
import { FsnetAuth } from '../../services/fsnetauth';
import HeaderComponent from '../header/header.component';
import homeImage from '../../images/home.png';
import successImage from '../../images/success-small.png';
import FundImage from '../../images/fund-default@2x.png';
import copyImage from '../../images/copy_img.svg';
import handShakeImage from '../../images/handshake.svg';
import { Route, Link, Switch, Redirect } from "react-router-dom";
import fundDefaultImage from '../../images/fund-default.png';
import userDefaultImage from '../../images/default_user.png';
import overFlow from '../../images/overflow.svg';
import { PubSub } from 'pubsub-js';
import InvestorInformationComponent from '../lp/Investor-Information/investorinformation.component';
import LlcFormComponent from '../lp/llc/llc.component';
import AccreditedInvestorComponent from '../lp/accreditedInvestor/accreditedInvestor.component';
import capitalCommitmentComponent from '../lp/capitalCommitment/capitalCommitment.component';
import additionalQuestionsComponent from '../lp/additionalQuestions/additionalQuestions.component';
import confirmComponent from '../lp/confirm/confirm.component';
import QualifiedPurchaserComponent from '../lp/qualifiedPurchaser/qualifiedPurchaser.component';
import QualifiedClientComponent from '../lp/qualifiedClient/qualifiedClient.component';
import companiesActComponent from '../lp/companiesAct/companiesAct.component';
import equityOwnersComponent from '../lp/equityOwners/equityOwners.component';
import entityProposingComponent from '../lp/entityProposing/entityProposing.component';
import ERISAComponent from '../lp/ERISA/ERISA.component';
import reviewComponent from '../lp/Review/review.component';
import documentLockerLpComponent from '../lp/documentLocker/documentLockerLp.component';
import PendingActionsComponent from '../lp/pendingActions/pendingActions.component';
import LpDelegatesComponent from '../lp/lpDelegates/lpDelegates.component';
import LpSignatoryComponent from '../lp/lpSignatory/lpSignatory.component';
import LpModalComponent from '../lp/lpmodals/lpmodals.component';
import { FsnetUtil } from '../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import vanillaLogo from '../../images/Vanilla-white.png';
import ModalComponent from '../fundSetup/modals/modals.component';
import editImage from '../../images/edit.svg';
import BellIcon from 'react-bell-icon';
var _ = require('lodash');

var investor = {}, addLpDelegate = {}, removeDelegate = {}, leftNavLinks = {}, capitalCommitmentWin,sideLetterWin = {};
class LpSubscriptionFormComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showModal: false,
            show: false,
            showSideNav: true,
            docModal: false,
            investorType: 'LLC',
            lpsubscriptionObj: { fund: { id: '' } },
            currentPage: null,
            currentPageCount: 1,
            totalPageCount: this.FsnetUtil._getSubscriptionPagesCount('LLC'),
            currentInvestorInfoPageNumber: 1,
            investorInfoValid: false,
            qualifiedPurchaserValid: false,
            accreditedInvestorValid: false,
            capitalCommitmentValid: false,
            companiesActValid: false,
            equityOwnersValid: false,
            entityProposingValid: false,
            erisaValid: false,
            legalEntity: null,
            pagesCompleted: 0,
            status: 'Open',
            currentFundImage: FundImage,
            lpDelegatesList: [],
            subscriptionId: '',
            alertModal: false,
            enableConfirmAndSubmit: false,
            capitalCommitmentObj: null,
            showSignatureModal: false,
            actionsRequired: false,
            isLpSignedValid: false,
            addQuestionsPageValid: false,
            loggedInUserObj: this.FsnetUtil.getUserData(),
            rescindFundModal:false
        }

        //Get the data from other steps
        investor = PubSub.subscribe('investorData', (msg, data) => {
            this.FsnetUtil.setLeftNavHeight();
            this.getLpPendingActions(data.id);
            window.scrollTo(0, 0);
            this.setState({
                investorType: data.investorType ? data.investorType : 'LLC',
                lpsubscriptionObj: data,
                currentInvestorInfoPageNumber: data.currentInvestorInfoPageNumber,
                currentPage: data.currentPage,
                currentPageCount: data.currentPageCount,
                legalEntity: data.fund ? data.fund.legalEntity : null,
                status: data.subscriptionStatus ? data.subscriptionStatus.name : null,
                currentFundImage: data.fund && data.fund.fundImage ? data.fund.fundImage.url : FundImage
            }, () => {
                //Validate lef nav section based on investorType
                if (this.state.investorType === 'Individual') {
                    this.setState({
                        totalPageCount: this.FsnetUtil._getSubscriptionPagesCount(this.state.investorType)
                    }, () => {
                        this.checkAdditionalQuestions();
                    });
                    this.validateIndividualPages();
                } else if (this.state.investorType === 'LLC') {
                    this.setState({
                        totalPageCount: this.FsnetUtil._getSubscriptionPagesCount(this.state.investorType)
                    }, () => {
                        this.checkAdditionalQuestions();
                    });
                    this.validateLLCPages();
                } else if (this.state.investorType === 'Trust') {
                    this.setState({
                        totalPageCount: this.FsnetUtil._getSubscriptionPagesCount(this.state.investorType)
                    }, () => {
                        this.checkAdditionalQuestions();
                    });
                    this.validateTrustPages();
                }
            })
        });

        //Get the data from other steps
        leftNavLinks = PubSub.subscribe('leftNavPage', (msg, data) => {
            setTimeout(function () {
                window.scrollTo(0, 0);
            }, 500);
            this.setState({
                currentPage: data
            })

        });

        //Add LP Delegate to the list when we add from modal
        addLpDelegate = PubSub.subscribe('lpDelegate', (msg, data) => {
            let delegatesList = this.state.lpDelegatesList;
            delegatesList.push(data);
            this.setState({
                lpDelegatesList: delegatesList
            })
        });

        //Remove LP Delegate to the list when we add from modal
        removeDelegate = PubSub.subscribe('removeLpDelegate', (msg, data) => {
            this.lpDelegates(this.state.lpsubscriptionObj.fund.id, this.state.lpsubscriptionObj.id);
        });

    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(investor);
        PubSub.unsubscribe(addLpDelegate);
        PubSub.unsubscribe(removeDelegate);
        PubSub.unsubscribe(leftNavLinks);
    }

    //Get current page
    getPage() {
        let page = this.FsnetUtil.getCurrentPageForLP();
        this.setState({
            currentPage: page
        });
    }

    checkAdditionalQuestions() {
        if (this.state.lpsubscriptionObj.investorQuestions && this.state.lpsubscriptionObj.investorQuestions.length > 0) {
            this.setState({
                totalPageCount: this.state.totalPageCount + 1
            })
        }
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('profileModal', data);
    }

    closeSignatureModal = () => {
        this.setState({
            showSignatureModal: false
        })
    }

    openSignatureModal = () => {
        this.setState({
            showSignatureModal: true
        })
    }

    openReviewPage = () => {
        this.props.history.push('/lp/review/' + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id));
    }

    capitalCommitmentLP = (type) => {
        // let getCapitalCommitmentDoc = this.state.lpsubscriptionObj.documentsForSignature.filter(doc => doc.docType === 'CHANGE_COMMITMENT_AGREEMENT');
        if (this.state.lpsubscriptionObj.ccAgreement.isCurrentUserSigned) {
            const docUrl = `${this.state.lpsubscriptionObj.ccAgreement.filePathTempPublicUrl}?token=${this.FsnetUtil.getToken()}`
            this.FsnetUtil._openDoc(docUrl)
            //Through alert
            if (type === 'fromUrl') {
                window.location.href = `/lp/review/${this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}`;
            }
        } else {
            const url = `/signDocuments?sId=${this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}&type=CC&documentId=${this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.ccAgreement.id)}${this.FsnetUtil._isIEEdge() ? '&token='+this.FsnetUtil.getToken():''}`
            var $this = this;
            capitalCommitmentWin = window.open(url, '_blank', 'width = 1000px, height = 800px');
            var timer = setInterval(function () {
                if (capitalCommitmentWin && capitalCommitmentWin.closed) {
                    clearInterval(timer);
                    const obj = reactLocalStorage.get('subscriptionDocUrl') ? JSON.parse(reactLocalStorage.get('subscriptionDocUrl')) : '';
                    if (obj && obj.type === 'success') {
                        const docUrl = `${obj.url}?token=${$this.FsnetUtil.getToken()}`
                        $this.FsnetUtil._openDoc(docUrl)
                        reactLocalStorage.set('subscriptionDocUrl', '')
                    }
                    window.location.href = `/lp/review/${$this.FsnetUtil._encrypt($this.state.lpsubscriptionObj.id)}`;
                }
            }, 1000);
        }
    }

    sideLetterLP = (type) => {
        // let getCapitalCommitmentDoc = this.state.lpsubscriptionObj.documentsForSignature.filter(doc => doc.docType === 'CHANGE_COMMITMENT_AGREEMENT');
        if (this.state.lpsubscriptionObj.sideletterAgreement.isCurrentUserSigned) {
            const docUrl = `${this.state.lpsubscriptionObj.sideletterAgreement.sideLetterUrl}?token=${this.FsnetUtil.getToken()}`
            this.FsnetUtil._openDoc(docUrl)
            //Through alert
            if (type === 'fromUrl') {
                window.location.href = `/lp/review/${this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}`;
            }
        } else {
            const url = `/signDocuments?sId=${this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}&type=SL&documentId=${this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.sideletterAgreement.id)}${this.FsnetUtil._isIEEdge() ? '&token='+this.FsnetUtil.getToken():''}`
            var $this = this;
            sideLetterWin = window.open(url, '_blank', 'width = 1000px, height = 800px');
            var timer = setInterval(function () {
                if (sideLetterWin && sideLetterWin.closed) {
                    clearInterval(timer);
                    const obj = reactLocalStorage.get('subscriptionDocUrl') ? JSON.parse(reactLocalStorage.get('subscriptionDocUrl')) : '';
                    if (obj && obj.type === 'success') {
                        const docUrl = `${obj.url}?token=${$this.FsnetUtil.getToken()}`
                        $this.FsnetUtil._openDoc(docUrl)
                        reactLocalStorage.set('subscriptionDocUrl', '')
                    }
                    window.location.href = `/lp/review/${$this.FsnetUtil._encrypt($this.state.lpsubscriptionObj.id)}`;
                }
            }, 1000);
        }
    }

    componentDidMount() {
        //Check user is valid or not
        //If not redirect to login page
        if (!this.FsnetAuth.isAuthenticated()) {
            window.location = '/';
        } else {
            window.scrollTo(0, 0);
            //Get the id from the url
            let id = this.FsnetUtil._getId();
            this.getPage();
            if (id) {
                this.getSubscriptionDetails(id);
                window.scrollTo(0, 0);
                this.setState({
                    subscriptionId: id
                })
            } else {
                window.location = '/';
            }

        }
    }


    handledocModalClose = () => {
        this.setState({
            docModal: false
        })
    }
    handledocModalShow = () => {
        this.setState({
            docModal: true
        })
    }

    handlealertModalClose = () => {
        this.setState({
            alertModal: false
        })
    }
    handlealertModalShow = () => {
        this.setState({
            alertModal: true
        })
    }

    checkCapitalCommitmentChangeAlert = () => {
        var url = window.location.href;
        if (url.indexOf('type=') !== -1) {
            let name = url.split('type=')[1];
            if (name.indexOf('&') !== -1) {
                name = name.split('&')[0]
            }
            if (name === 'CHANGE_COMMITMENT_AGREEMENT') {
                this.capitalCommitmentLP('fromUrl');
            } else if (name === 'SUBSCRIPTION_AGREEMENT') {
                this.openSubscriptionDocument('fromUrl');
            } else if (name === 'SIDE_LETTER_AGREEMENT') {
                this.sideLetterLP('fromUrl');
                
            }
        }
    }

    //Get the fund details
    getSubscriptionDetails(id) {
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    this.lpDelegates(result.data.data.fund.id, result.data.data.id);
                    if (result.data.data.documentsForSignature) {
                        for (let index of result.data.data.documentsForSignature) {
                            if (index.docType === 'CHANGE_COMMITMENT_AGREEMENT') {
                                this.setState({
                                    capitalCommitmentObj: index
                                })
                            }
                        }
                    }
                    this.setState({
                        lpsubscriptionObj: result.data.data,
                        subscriptionId: result.data.data.id,
                        investorTypeTemp: result.data.data.investorType ? result.data.data.investorType : 'LLC',
                        investorType: result.data.data.investorType ? result.data.data.investorType : 'LLC',
                        // investorType: this.state.investorType ? this.state.investorType : (result.data.data.investorType? result.data.data.investorType:'LLC'),
                        legalEntity: result.data.data.fund.legalEntity,
                        status: result.data.data.subscriptionStatus.name,
                        currentFundImage: result.data.data.fund.fundImage ? result.data.data.fund.fundImage.url : FundImage
                    }, () => {
                        this.FsnetUtil.setLeftNavHeight();
                        //Call Pending Actions
                        this.getLpPendingActions(this.state.lpsubscriptionObj.id);
                        this.checkCapitalCommitmentChangeAlert() //This is for alerts
                        //Assing total pages count based on investor type.
                        if (result.data.data.investorType === 'Individual') {
                            this.setState({
                                totalPageCount: this.FsnetUtil._getSubscriptionPagesCount(result.data.data.investorType)
                            }, () => {
                                this.checkAdditionalQuestions();
                            });
                            this.validateIndividualPages();
                        } else if (result.data.data.investorType === 'LLC') {
                            this.setState({
                                totalPageCount: this.FsnetUtil._getSubscriptionPagesCount(result.data.data.investorType)
                            }, () => {
                                this.checkAdditionalQuestions();
                            });
                            this.validateLLCPages();
                        } else if (result.data.data.investorType === 'Trust') {
                            this.setState({
                                totalPageCount: this.FsnetUtil._getSubscriptionPagesCount(result.data.data.investorType)
                            }, () => {
                                this.checkAdditionalQuestions();
                            });
                            this.validateTrustPages();
                        } else {
                            this.checkAdditionalQuestions();
                        }
                    })
                    PubSub.publish('getLpSubscription', result.data.data);
                    //Call capital commitment change fn if it comes form alert
                }
            })
            .catch(error => {
                this.close();
            });
        }
    }

    //Close the modal
    handleClose() {
        this.setState({ show: false });
    }

    //Open the modal
    handleLpShow = () => {
        if (this.state.status !== 'Closed') {
            this.props.history.push('/lp/lpDelegate/' + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id));
            // PubSub.publish('openModal',this.state.lpsubscriptionObj);
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    //Header hamburger click
    hamburgerClick = () => {
        if (this.state.showSideNav == true) {
            this.setState({
                showSideNav: false
            })
        } else {
            this.setState({
                showSideNav: true
            })
        }
    }

    //Check Investor info page is valid
    investorPageValid() {
        let mandatoryFields = [];
        if (this.state.investorType === 'Individual') {
            mandatoryFields = ['investorType', 'areYouSubscribingAsJointIndividual', 'isSubjectToDisqualifyingEvent', 'mailingAddressCity', 'mailingAddressState', 'mailingAddressStreet', 'mailingAddressZip'];
        } else if (this.state.investorType === 'LLC') {
            mandatoryFields = ['investorType', 'entityName', 'isEntityTaxExemptForUSFederalIncomeTax', 'isSubjectToDisqualifyingEvent', 'releaseInvestmentEntityRequired', 'mailingAddressCity', 'mailingAddressState', 'mailingAddressStreet', 'mailingAddressZip', 'otherInvestorAttributes'];
        } else if (this.state.investorType === 'Trust') {
            if (this.state.lpsubscriptionObj.investorSubType == 9) {
                mandatoryFields = ['investorType', 'investorSubType', 'email', 'trustName', 'countryResidenceId', 'isEntityTaxExemptForUSFederalIncomeTax', 'releaseInvestmentEntityRequired', 'isSubjectToDisqualifyingEvent', 'mailingAddressCity', 'mailingAddressStreet', 'mailingAddressCountry', 'mailingAddressState', 'mailingAddressZip', 'otherInvestorAttributes'];
            } else {
                mandatoryFields = ['investorType', 'investorSubType', 'email', 'trustName', 'countryResidenceId', 'isEntityTaxExemptForUSFederalIncomeTax', 'releaseInvestmentEntityRequired', 'isSubjectToDisqualifyingEvent', 'mailingAddressCity', 'mailingAddressStreet', 'mailingAddressCountry', 'mailingAddressState', 'mailingAddressZip', 'legalTitleDesignation', 'otherInvestorAttributes'];
            }
        }

        let valid = this.FsnetUtil.checkNullOrEmpty(mandatoryFields, this.state.lpsubscriptionObj);
        if (valid) {
            this.setState({
                investorInfoValid: true,
                currentInvestorInfoPageNumber: 2
            })
        } else {
            this.setState({
                investorInfoValid: false
            })
        }
    }

    //Check qualified purchaser page is valid
    qualifiedPurchaserPageValid() {
        if ((this.state.lpsubscriptionObj.areYouQualifiedPurchaser === false && this.state.lpsubscriptionObj.areYouQualifiedClient !== null) || (this.state.lpsubscriptionObj.areYouQualifiedPurchaser === true)) {
            this.setState({
                qualifiedPurchaserValid: true,
            }, () => {
                this.stepsCompleted();
            })
        } else {
            this.setState({
                qualifiedPurchaserValid: false
            }, () => {
                this.stepsCompleted();
            })
        }
    }

    //Check accredited Investor page is valid
    accreditedInvestorPageValid() {
        if (this.state.lpsubscriptionObj.areYouAccreditedInvestor !== null && this.state.lpsubscriptionObj.areYouAccreditedInvestor !== undefined) {
            this.setState({
                accreditedInvestorValid: true,
            }, () => {
                this.stepsCompleted();
            })
        } else {
            this.setState({
                accreditedInvestorValid: false
            }, () => {
                this.stepsCompleted();
            })
        }
    }

    capitalCommitmentPageValid() {
        if (this.state.lpsubscriptionObj.lpCapitalCommitment !== null && this.state.lpsubscriptionObj.lpCapitalCommitment !== 0 && this.state.lpsubscriptionObj.lpCapitalCommitment !== undefined && this.state.lpsubscriptionObj.lpCapitalCommitment !== '') {
            this.setState({
                capitalCommitmentValid: true,
            }, () => {
                this.stepsCompleted();
            })
        } else {
            this.setState({
                capitalCommitmentValid: false
            }, () => {
                this.stepsCompleted();
            })
        }
    }

    //Check companies act page is valid
    companiesActPageValid() {
        // if(this.state.lpsubscriptionObj.companiesAct!== null && this.state.lpsubscriptionObj.companiesAct!== undefined && this.state.lpsubscriptionObj.numberOfDirectEquityOwners && this.state.lpsubscriptionObj.existingOrProspectiveInvestorsOfTheFund!== null) {
        if (this.state.investorType == 'LLC' && this.state.lpsubscriptionObj.companiesAct !== null && this.state.lpsubscriptionObj.companiesAct !== undefined) {

            this.setState({
                companiesActValid: true,
            }, () => {
                this.stepsCompleted();
            })
        } else if (this.state.investorType == 'Trust') {
            if ((this.state.lpsubscriptionObj.companiesAct && this.state.lpsubscriptionObj.numberOfDirectEquityOwners && this.state.lpsubscriptionObj.existingOrProspectiveInvestorsOfTheFund !== null) || this.state.lpsubscriptionObj.companiesAct == 1) {
                this.setState({
                    companiesActValid: true,
                }, () => {
                    this.stepsCompleted();
                })
            } else {
                this.setState({
                    companiesActValid: false
                }, () => {
                    this.stepsCompleted();
                })
            }
        } else {
            this.setState({
                companiesActValid: false
            }, () => {
                this.stepsCompleted();
            })
        }
    }

    //Check equity owners page is valid
    equityOwnersPageValid() {
        if (this.state.lpsubscriptionObj.numberOfDirectEquityOwners >= 0 && this.state.lpsubscriptionObj.existingOrProspectiveInvestorsOfTheFund !== null) {
            this.setState({
                equityOwnersValid: true,
            }, () => {
                this.stepsCompleted();
            })
        } else {
            this.setState({
                equityOwnersValid: false
            }, () => {
                this.stepsCompleted();
            })
        }
    }

    //Check Look-Through Issues page is valid
    enitityProposingPageValid() {
        if ((this.state.lpsubscriptionObj.entityProposingAcquiringInvestment !== null && this.state.lpsubscriptionObj.entityProposingAcquiringInvestment !== undefined) && this.state.lpsubscriptionObj.entityHasMadeInvestmentsPriorToThedate !== null && this.state.lpsubscriptionObj.partnershipWillNotConstituteMoreThanFortyPercent !== null && this.state.lpsubscriptionObj.beneficialInvestmentMadeByTheEntity !== null) {
            this.setState({
                entityProposingValid: true,
            }, () => {
                this.stepsCompleted();
            })
        } else {
            this.setState({
                entityProposingValid: false
            }, () => {
                this.stepsCompleted();
            })
        }
    }

    isEmpty(value) {
        return (value == null || value.length === 0);
    }

    //Check Erisa page is valid
    erisaPageValid() {
        if (!this.isEmpty(this.state.lpsubscriptionObj.employeeBenefitPlan) || !this.isEmpty(this.state.lpsubscriptionObj.planAsDefinedInSection4975e1) || !this.isEmpty(this.state.lpsubscriptionObj.benefitPlanInvestor)) {
            this.setState({
                erisaValid: true,
            }, () => {
                this.stepsCompleted();
            })
        } else {
            this.setState({
                erisaValid: false
            }, () => {
                this.stepsCompleted();
            })
        }
    }

    addedQuestionsValid() {
        const isQuestionsAnswered = _.filter(this.state.lpsubscriptionObj.investorQuestions, function(obj){
            return obj.questionResponse;
        })
        this.setState({
            addQuestionsPageValid: isQuestionsAnswered.length == 0 ? false : true
        })
        // if (this.state.lpsubscriptionObj && this.state.lpsubscriptionObj.investorQuestions && this.state.lpsubscriptionObj.investorQuestions.length > 0) {
        //     for (let index of this.state.lpsubscriptionObj.investorQuestions) {
        //         if (index.isRequired && (index.questionResponse === null || index.questionResponse === '' || index.questionResponse === undefined)) {
        //             this.setState({
        //                 addQuestionsPageValid: false
        //             })
        //             break;
        //         } else if (this.state.lpsubscriptionObj.investorQuestions.length == 1 && (this.state.lpsubscriptionObj.investorQuestions[0].questionResponse === null || this.state.lpsubscriptionObj.investorQuestions[0].questionResponse === '' || this.state.lpsubscriptionObj.investorQuestions[0].questionResponse === undefined)) {
        //             this.setState({
        //                 addQuestionsPageValid: false
        //             })
        //         } else {
        //             this.setState({
        //                 addQuestionsPageValid: true
        //             })
        //         }
        //     }
        // }
    }

    //Check Is LP signed
    checkLpSignedForProgressBar() {
        let subscriptionDoc;
        if (this.state.lpsubscriptionObj.documentsForSignature && this.state.lpsubscriptionObj.documentsForSignature.length > 0) {
            subscriptionDoc = this.state.lpsubscriptionObj.documentsForSignature.filter(document => (document.docType == 'SUBSCRIPTION_AGREEMENT' && document.isLpSignConfirmed === true));
        } else {
            subscriptionDoc = null
        }
        if (subscriptionDoc && subscriptionDoc.length > 0) {
            this.setState({ isLpSignedValid: true }, () => { this.stepsCompleted() })
        } else {
            this.setState({ isLpSignedValid: false }, () => { this.stepsCompleted() })
        }
    }

    //Check how many pages completed
    stepsCompleted() {
        let pages = [];
        if (this.state.investorType === 'Individual') {
            pages = ['investorInfoValid', 'accreditedInvestorValid', 'qualifiedPurchaserValid', 'capitalCommitmentValid'];
        } else if (this.state.investorType === 'LLC') {
            pages = ['investorInfoValid', 'accreditedInvestorValid', 'qualifiedPurchaserValid', 'companiesActValid', 'entityProposingValid', 'erisaValid', 'capitalCommitmentValid'];
        } else if (this.state.investorType === 'Trust') {
            pages = ['investorInfoValid', 'accreditedInvestorValid', 'qualifiedPurchaserValid', 'companiesActValid', 'entityProposingValid', 'erisaValid', 'capitalCommitmentValid'];
        }
        if (this.FsnetUtil.getUserRole() === 'LP') {
            pages.push('isLpSignedValid');
        }
        let idx = 0;
        for (let index of pages) {
            if (this.state[index] === true) {
                idx = idx + 1
            };
        }
        this.setState({
            pagesCompleted: idx
        }, () => {
            if (this.state.addQuestionsPageValid) {
                this.setState({
                    pagesCompleted: this.FsnetUtil.getUserRole() == 'LP' && this.state.lpsubscriptionObj.noOfSignaturesRequired > 0 ?  this.state.pagesCompleted + 1 : this.state.pagesCompleted + 1
                })
            }
            idx = this.FsnetUtil.getUserRole() == 'LP' && this.state.lpsubscriptionObj.noOfSignaturesRequired > 0 ? idx + 1 : idx+1
        })
        this.disableConfrimAndSubmitBtn(idx)
        // const newIdx = reactLocalStorage.get('userData') && JSON.parse(reactLocalStorage.get('userData'))['accountType'] === 'LPDelegate' ? idx-1:idx
        PubSub.publish('totalPagesCompleted', { count: idx });
    }


    disableConfrimAndSubmitBtn(count) {
        if (this.state.lpsubscriptionObj.investorType === 'Individual' && count === 4) {
            this.setState({
                enableConfirmAndSubmit: true
            })
        } else if (this.state.lpsubscriptionObj.investorType === 'LLC' && count === 8) {
            this.setState({
                enableConfirmAndSubmit: true
            })
        } else if (this.state.lpsubscriptionObj.investorType === 'Trust' && count === 6) {
            this.setState({
                enableConfirmAndSubmit: true
            })
        } else {
            this.setState({
                enableConfirmAndSubmit: false
            })
        }
    }

    //Call all the pages related to investor type individual
    //Validates all pages
    validateIndividualPages() {
        this.investorPageValid();
        this.accreditedInvestorPageValid();
        this.qualifiedPurchaserPageValid();
        this.capitalCommitmentPageValid();
        this.addedQuestionsValid();
        this.checkLpSignedForProgressBar();
    }

    //Call all the pages related to investor type LLC
    //Validates all pages
    validateLLCPages() {
        this.investorPageValid();
        this.accreditedInvestorPageValid();
        this.qualifiedPurchaserPageValid();
        this.companiesActPageValid();
        //this.equityOwnersPageValid();
        this.enitityProposingPageValid();
        this.erisaPageValid();
        this.capitalCommitmentPageValid();
        this.addedQuestionsValid();
        this.checkLpSignedForProgressBar();
    }

    validateTrustPages() {
        this.investorPageValid();
        this.accreditedInvestorPageValid();
        this.qualifiedPurchaserPageValid();
        this.companiesActPageValid();
        this.enitityProposingPageValid();
        this.erisaPageValid();
        this.capitalCommitmentPageValid();
        this.addedQuestionsValid();
        this.checkLpSignedForProgressBar();
    }

    //Get LP Delegates
    lpDelegates = (fundId, subId) => {

        if (fundId) {
            this.open();
            this.Fsnethttp.getLpDelegates(fundId, subId).then(result => {
                this.close();
                if (result.data) {
                    this.setState({
                        lpDelegatesList: result.data.data
                    })
                }
            })
            .catch(error => {
                this.close();
                this.setState({
                    lpDelegatesList: []
                })
            });
        }
    }

    //Delete LP Delegate
    deleteLpDelegate = (e, id) => {
        if (this.state.status !== 'Closed') {
            PubSub.publish('openLpDelegateModal', { data: this.state.lpsubscriptionObj, delegateId: id });
        }
    }

    openPendingDoc = (type,data) => () => {
        let docUrl;
        if(type === 'aa') {
            docUrl = `${data.url}?token=${this.FsnetUtil.getToken()}`
        } else {
            docUrl = `${this.Constants.BASE_URL}document/view/${type}/${this.state.subscriptionId}?token=${this.FsnetUtil.getToken()}`
        }
        this.FsnetUtil._openDoc(docUrl)
    }

    //Get list of Pending Actions
    getLpPendingActions = (id) => {
        if (id) {
            this.open();

            this.Fsnethttp.pendingActions(id).then(result => {
                this.close();
                if (result.data.data && result.data.data.length > 0) {
                    const resultsLength = result.data.data.length;
                    this.setState({
                        actionsRequired: resultsLength  > 0 ? true : false
                    })
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    openRescindFundModal = () => {this.setState({rescindFundModal:true})}
    closeRescindFundModal = () => {this.setState({rescindFundModal:false})}

    checkIsPrimaryInvestment = () => {
        this.openRescindFundModal()
    }

    rescindFund = () => {
        if (this.state.lpsubscriptionObj.status != 10) {
            this.open();
            this.Fsnethttp.rescindSubscription(this.state.subscriptionId).then(result => {
                this.close();
                if (result.data) {
                    this.closeRescindFundModal();
                    this.props.history.push('/dashboard');
                }
            }).catch(error => {
                this.close();
            });
        }
    }

    logout = () => {
        if (this.FsnetUtil.getAdminRole() === 'FSNETAdministrator') {
            this.FsnetUtil._removeAdminData();
            setTimeout(() => {
                window.location.href = '/adminDashboard/firmView';
            }, 200);
        } else {
            this.Fsnethttp.logout().then(result => {
                this.FsnetUtil._ClearLocalStorage();
            });
        }
    }

    navigateToTracker = () => {
        window.location.href = "/fund/view/" + this.state.lpsubscriptionObj.fund['id'];
    }


    render() {
        const { match } = this.props;
        function LinkWithTooltip({ id, children, href, tooltip, className }) {
            return (
                <OverlayTrigger
                    trigger={['click']}
                    rootClose={true}
                    overlay={<Tooltip id={id} className={`lpTableTooltip ${className}`}>{tooltip}</Tooltip>}
                    placement="bottom"
                    delayShow={300}
                    delayHide={150}
                    onClick={document.body.click()}
                >
                    <span>{children}</span>
                </OverlayTrigger>
            );
        }
        return (
            <div className="lpSubFormStyle" id="">
                <nav className="navbar navbar-custom">
                    <div className="navbar-header">
                        <div className="sidenav" >
                            <h1 className="text-left"><i className="fa fa-bars" aria-hidden="true" onClick={(e) => this.hamburgerClick()}></i>&nbsp; <img src={vanillaLogo} alt="vanilla" className="vanilla-logo" /></h1>
                        </div>
                    </div>
                    <div className="text-center navbar-collapse-custom" id="navbar-collapse" hidden={!this.state.showSideNav}>
                        <div className="sidenav" id="leftNavBar">
                            <h1 className="text-left logoHamburger"><i className="fa fa-bars" aria-hidden="true"></i><img src={vanillaLogo} alt="vanilla" className="vanilla-logo" /></h1>
                            <h2 className="text-left lpDashAlign alignMargin"><img src={homeImage} alt="home_image" className="" />&nbsp;
                            {
                                    ['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) > -1 ?
                                        <a onClick={this.navigateToTracker} className="dashboardTxtAlign cursor">Dashboard</a>
                                        :
                                        <Link to="/dashboard" className="dashboardTxtAlign">Dashboard</Link>
                                }

                            </h2>

                            {
                                this.FsnetUtil.getUserRole() !== 'LPDelegate' &&
                                <h2 className="text-left lpDashAlign marginBot20">
                                    {
                                    this.state.actionsRequired ? 
                                        <BellIcon width='40' color="white" style={{width:'16px'}} active={true} className="cursor" animate={true} />
                                        :
                                        <BellIcon width='40' color="white" style={{width:'16px'}} active={false} animate={false} />
                                    }
                                    &nbsp;
                                    <Link className={"dashboardTxtAlign "+(this.state.currentPage === 'pendingActions' ? 'active' : '')}  to={`/lp/pendingActions/${this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}`}>Action Required</Link>
                                </h2>
                            }
                            {
                                this.state.status === 'Closed' ?
                                    <div className="active-item text-left cursor marginBot20" onClick={this.openReviewPage}><label className="fund-left-pic-label"><img src={copyImage} alt="image" /></label>&nbsp;<div className="left-nav-fund-name text-left subAgrmnt">Subscription Agreement</div></div>
                                    :
                                    <div className="active-item text-left"><label className="fund-left-pic-label"><img src={copyImage} alt="image" /></label>&nbsp;<div className="left-nav-fund-name text-left subAgrmnt">Subscription Agreement</div> <span className="fsbadge fsbadgeAlign">{this.state.currentPageCount}/{this.state.totalPageCount}</span></div>
                            }
                            {
                                this.state.investorType && this.state.status !== 'Closed' ?
                                    <ul className="sidenav-menu">
                                        <li>
                                            <Link className={(this.state.currentPage === 'investorInfo' ? 'active' : '')} to={"/lp/investorInfo/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Investor Information ({this.state.currentInvestorInfoPageNumber}/2)<img src={successImage} alt="successImage" className="ptrSuccessImage successImg" hidden={this.state.investorInfoValid === false} /></Link>
                                        </li>
                                        <li>
                                            <Link className={(this.state.currentPage === 'AccreditedInvestor' ? 'active' : '')} to={"/lp/AccreditedInvestor/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Accredited Investor<img src={successImage} alt="successImage" className="ptrSuccessImage successImg" hidden={this.state.accreditedInvestorValid === false} /></Link>
                                        </li>
                                        <li>
                                            <Link className={(this.state.currentPage === 'qualifiedPurchaser' ? 'active' : '')} to={"/lp/qualifiedPurchaser/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Qualified Purchaser/Client<img src={successImage} alt="successImage" className="ptrSuccessImage successImg" hidden={this.state.qualifiedPurchaserValid === false} /></Link>
                                        </li>
                                        <li hidden={this.state.investorType === 'Individual'}>
                                            <Link className={`${this.state.currentPage === 'companiesAct' ? 'active' : ''}`} to={"/lp/companiesAct/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Companies Act<img src={successImage} alt="successImage" className="ptrSuccessImage successImg" hidden={this.state.companiesActValid === false} /></Link>
                                        </li>

                                        <li hidden={this.state.investorType === 'Individual'}>
                                            <Link className={`${this.state.currentPage === 'entityProposing' ? 'active' : ''}`} to={"/lp/entityProposing/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Look-Through Issues<img src={successImage} alt="successImage" className="ptrSuccessImage successImg" hidden={this.state.entityProposingValid === false} /></Link>
                                        </li>
                                        <li hidden={this.state.investorType === 'Individual'}>
                                            <Link className={`${this.state.currentPage === 'erisa' ? 'active' : ''}`} to={"/lp/erisa/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>ERISA<img src={successImage} alt="successImage" className="ptrSuccessImage successImg" hidden={this.state.erisaValid === false} /></Link>
                                        </li>
                                        <li hidden={this.state.lpsubscriptionObj && this.state.lpsubscriptionObj.investorQuestions && this.state.lpsubscriptionObj.investorQuestions.length == 0}>
                                            <Link className={`${this.state.currentPage === 'additionalQuestions' ? 'active' : ''}`} to={"/lp/additionalQuestions/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Additional Questions<img src={successImage} alt="successImage" className="ptrSuccessImage successImg" hidden={!this.state.addQuestionsPageValid} /></Link>
                                        </li>
                                        <li className='capitalCommitmentLp'>
                                            <img src={editImage} hidden={this.state.capitalCommitmentObj === null || this.state.capitalCommitmentObj === undefined} className="cursor editCapitalCommitment" onClick={this.capitalCommitmentLP} />
                                            <Link className={(this.state.currentPage === 'capitalCommitment' ? 'active' : '')} to={"/lp/capitalCommitment/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Capital Commitment<img src={successImage} alt="successImage" className="ptrSuccessImage successImg" hidden={this.state.capitalCommitmentValid === false} /></Link></li>

                                        {
                                            this.FsnetUtil.getUserRole() === 'LP' && ['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) == -1 ?
                                                <li><Link className={(this.state.currentPage === 'lpSignatory' ? 'active' : '')} to={"/lp/lpSignatory/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Secondary Signatories
                                        <img src={successImage} alt="successImage" className={"ptrSuccessImage successImg " + (this.state.lpsubscriptionObj.noOfSignaturesRequired == 0 || this.state.lpsubscriptionObj.noOfSignaturesRequired == null ? 'hide' : '')} />
                                                </Link></li>
                                                : ''
                                        }

                                        <li><Link className={(this.state.currentPage === 'review' ? 'active' : '')} to={"/lp/review/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Review & Confirm
                                    </Link></li>
                                    </ul> :
                                    <ul className="sidenav-menu minHeight150" hidden={this.state.status === 'Closed'}>
                                        <li>
                                            <Link className={(this.state.currentPage === 'investorInfo' ? 'active' : '')} to={"/lp/investorInfo/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Investor Information (1/2)</Link>
                                        </li>
                                    </ul>
                            }

                            {
                                ['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) == -1 &&
                                <div>
                                    <div className="text-left parnershipAgrementStyle" ><label className=""><img src={handShakeImage} alt="image" className="handShakeImageStyle" /></label>&nbsp;<div className="left-nav-fund-name text-left agrementTxtStyle" ><span className="agreementTxtAlign">VIEW EXECUTED DOCUMENTS</span>
                                    </div></div>
                                    <div className="sign-box"><Link className={(this.state.currentPage === 'documentLockerLp' ? 'active' : '')} to={"/lp/documentLockerLp/" + this.FsnetUtil._encrypt(this.state.lpsubscriptionObj.id)}>Document Locker</Link></div>
                                    <div className="text-left parnershipAgrementStyle marginTop20" ><label className=""><img src={handShakeImage} alt="image" className="handShakeImageStyle" /></label>&nbsp;<div className="left-nav-fund-name text-left agrementTxtStyle" ><span className="agreementTxtAlign">VIEW PENDING DOCUMENTS</span>
                                    </div></div>
                                    {
                                        (this.state.lpsubscriptionObj.lpSignedPendingDocuments && this.state.lpsubscriptionObj.lpSignedPendingDocuments.fundData) &&
                                        <div className="sign-box"><a onClick={this.openPendingDoc('fa')}>Fund Agreement</a></div>
                                    }
                                    {
                                        (this.state.lpsubscriptionObj.lpSignedPendingDocuments && this.state.lpsubscriptionObj.lpSignedPendingDocuments.subscriptionData) &&
                                        <div className="sign-box"><a onClick={this.openPendingDoc('sa')}>Subscription Agreement</a></div>
                                    }
                                    {
                                        (this.state.lpsubscriptionObj.lpSignedPendingDocuments && this.state.lpsubscriptionObj.lpSignedPendingDocuments.capitalCommitmentData) &&
                                        <div className="sign-box"><a onClick={this.openPendingDoc('ccfile')}>Capital Commitment Agreement</a></div>
                                    }
                                    {
                                        (this.state.lpsubscriptionObj.lpSignedPendingDocuments && this.state.lpsubscriptionObj.lpSignedPendingDocuments.sideLetterData) &&
                                        <div className="sign-box"><a onClick={this.openPendingDoc('sia')}>Side Letter Agreement</a></div>
                                    }
                                    {
                                        (this.state.lpsubscriptionObj.lpSignedPendingDocuments && this.state.lpsubscriptionObj.lpSignedPendingDocuments.amendmentData && this.state.lpsubscriptionObj.lpSignedPendingDocuments.amendmentData.length > 0)  &&

                                        this.state.lpsubscriptionObj.lpSignedPendingDocuments.amendmentData.map((record, index) => {
                                            return (
                                                <div key={index}>
                                                    <div className="sign-box ellipsis" style={{'width':'90%'}} title={record.filename}><a onClick={this.openPendingDoc('aa',record)}>{record.filename}</a></div>
                                                </div>
                                            );
                                        })
                                    }
                                    <div className="section-head text-left"><span className="sectionHeadTxt lpAlign">Delegates</span>
                                        {
                                            this.FsnetUtil.getUserRole() === 'LP' &&
                                                <span hidden={this.state.status === 'Closed'} className="btn-add pull-right edit-btn-text" onClick={this.handleLpShow}>Edit</span>
                                        }
                                    </div>
                                    <div className="section">
                                        <div className="gpDelDiv">
                                            {this.state.lpDelegatesList.length > 0 ?
                                                this.state.lpDelegatesList.map((record, index) => {
                                                    return (
                                                        <div className="gpDelegateInfo" key={index}>
                                                            <div className="dpDelImg">
                                                                {
                                                                    record['profilePic'] ?
                                                                        <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                                        : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                                }
                                                            </div>
                                                            <div className="dpDelName">{this.FsnetUtil.getFullName(record)}</div>
                                                        </div>
                                                    );
                                                })
                                                :
                                                <div className="user">
                                                    <i className="fa fa-user fa-2x" aria-hidden="true"></i>
                                                    <p className="opacity75">You haven’t added any Delegates to this Fund yet.</p>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>

                    </div>
                </nav>

                <div className="mainHead" >
                    <div className="lpHeaderResponsive">
                        {
                            this.FsnetUtil.getAdminRole() === 'FSNETAdministrator' && window.location.href.indexOf('/lp') > -1 ?
                                <div className="impersonateHeader text-center"><label className="title ellipsis">Impersonated as {this.FsnetUtil.getFullName(this.state.loggedInUserObj)}</label><Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.logout}>Go Back To Admin</Button></div>
                                : ''
                        }
                        <div className="mainHeading stickyHeader">
                            <Row className="">
                                <Col lg={6} md={7} sm={6} xs={12} id="">
                                    <Row>
                                        <Col lg={2} md={2} sm={3} xs={3} className="fundImageAllign">
                                            {
                                                this.state.lpsubscriptionObj['fund'] && this.state.lpsubscriptionObj['fund']['fundImage'] ?
                                                    <img src={this.state.lpsubscriptionObj['fund']['fundImage']['url']} alt="img" className="header-fund-image" /> :
                                                    <img src={fundDefaultImage} alt="fund_image" className="header-fund-image" />
                                            }
                                        </Col>
                                        <Col lg={10} md={10} sm={9} xs={9} className="fundNameAllign">
                                            <div className="main-title top-22">{this.state.legalEntity}</div>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={6} md={5} sm={6} xs={12}>
                                    <HeaderComponent ></HeaderComponent>
                                </Col>
                            </Row>

                            <Row>
                                <Col lg={6} md={4} sm={4} xs={12}>
                                </Col>
                                <Col lg={6} md={8} sm={8} xs={12} className="tooltip-Block">
                                    {
                                        ['GP', 'SecondaryGP', 'GPDelegate'].indexOf(this.FsnetUtil.getUserRole()) == -1 &&
                                        <LinkWithTooltip tooltip={
                                            <ul className={`rescindFund ${this.state.lpsubscriptionObj.status == 10 ? 'disabled' : ''}`}>
                                                <li onClick={this.checkIsPrimaryInvestment}>Rescind from Fund</li>
                                            </ul>
                                        } href="#" id="tooltip-1" className="rescindFundToolTip">
                                            <img src={overFlow} alt="home_image" className="cursor" />
                                        </LinkWithTooltip>
                                    }
                                    <div className="progressBarPosition">
                                        <div className="progressBarAlign"><div className="progress progressColor" style={{ width: (this.state.pagesCompleted / this.state.totalPageCount) * 100 + "%" }}></div></div>
                                        <div className="progressDetailsStyle">Fund Enrollment Steps Completed<span className="progressTxtAlign">{this.state.pagesCompleted} of {this.state.totalPageCount}</span></div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    <Row className="main-content">
                            <Switch>
                                <Route exact path={`${match.url}/investorInfo/:id`} component={InvestorInformationComponent} />
                                <Route exact path={`${match.url}/llc/:id`} component={LlcFormComponent} />
                                <Route exact path={`${match.url}/AccreditedInvestor/:id`} component={AccreditedInvestorComponent} />
                                <Route exact path={`${match.url}/confirm/:id`} component={confirmComponent} />
                                <Route exact path={`${match.url}/qualifiedPurchaser/:id`} component={QualifiedPurchaserComponent} />
                                <Route exact path={`${match.url}/qualifiedClient/:id`} component={QualifiedClientComponent} />
                                <Route exact path={`${match.url}/companiesAct/:id`} component={companiesActComponent} />
                                <Route exact path={`${match.url}/equityOwners/:id`} component={equityOwnersComponent} />
                                <Route exact path={`${match.url}/entityProposing/:id`} component={entityProposingComponent} />
                                <Route exact path={`${match.url}/erisa/:id`} component={ERISAComponent} />
                                <Route exact path={`${match.url}/capitalCommitment/:id`} component={capitalCommitmentComponent} />
                                <Route exact path={`${match.url}/additionalQuestions/:id`} component={additionalQuestionsComponent} />
                                <Route exact path={`${match.url}/review/:id`} component={reviewComponent} />
                                <Route exact path={`${match.url}/documentLockerLp/:id`} component={documentLockerLpComponent} />
                                <Route exact path={`${match.url}/pendingActions/:id`} component={PendingActionsComponent} />
                                <Route exact path={`${match.url}/lpDelegate/:id`} component={LpDelegatesComponent} />
                                <Route exact path={`${match.url}/lpSignatory/:id`} component={LpSignatoryComponent} />
                                <Redirect from={`${match.url}/*`} to='/404' />
                                <Redirect from={`${match.url}/`} to='/404' />
                            </Switch>
                        </Row>
                    </div>
                </div>
                <Modal id="subscriptionDelModal" backdrop="static" show={this.state.docModal} onHide={this.handledocModalClose}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>Submit Subscription Agreement</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">Are you sure you want to resubmit the Subscription Agreement?</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handledocModalClose}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.subscriptionSubmiToDocusign}>Submit</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="subscriptionDelModal" backdrop="static" show={this.state.alertModal} onHide={this.handlealertModalClose}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>Alert</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">Please sign the Fund Agreement before signing the Subscription Agreement.</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={12} md={12} sm={12} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.handlealertModalClose}>Ok</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal className='passwordModal' backdrop="static" show={this.state.showSignatureModal} onHide={this.closeSignatureModal}>
                    <Modal.Header closeButton />
                    <Modal.Body>
                        <div className="title">Please add authentication password using this <a href="/settings/authentication-password">link</a>.</div>
                    </Modal.Body>
                </Modal>
                {/* Rescind From Fund Modal */}
                <Modal  id="restoreModal" backdrop="static" show={this.state.rescindFundModal}  onHide={this.closeRescindFundModal} dialogClassName="delteFundDocDailog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Title className="title titleModal">Rescind from Fund</Modal.Title>
                <Modal.Body className="paddingTopNone">
                        {/* <h1 className="title">Rescind from Fund</h1> */}
                        <div className="subtext minHeight50 marginTop10">Are you sure you want to rescind this investment from this Fund? This will have the effect of removing this specific investment from the Fund, but will leave any other investments you have made intact. If you wish to restore this investment at a later date, please contact your Fund Manager.</div>
                        <Row className="marginTop20">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton marginLeft30" onClick={this.closeRescindFundModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled marginLeft40" onClick={this.rescindFund}>Proceed</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
                <LpModalComponent></LpModalComponent>
                <ModalComponent></ModalComponent>
            </div>
        );
    }
}

export default LpSubscriptionFormComponent;


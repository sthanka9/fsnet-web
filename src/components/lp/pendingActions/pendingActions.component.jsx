import React, { Component } from "react";
import Loader from '../../../widgets/loader/loader.component';
import { Row, Modal,Button,Col } from 'react-bootstrap';
import { Constants } from '../../../constants/constants';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';

var capitalCommitmentWin,sideLetterWin, modifiedSubscriptionWin,signWin = {}
class PendingActionsComponent extends Component {
  constructor(props) {
    super(props);
    this.FsnetAuth = new FsnetAuth();
    this.Constants = new Constants();
    this.Fsnethttp = new Fsnethttp();
    this.FsnetUtil = new FsnetUtil();
    this.state = {
      showModal: false,
      pendingActionsList: [],
      subscriptionId: null,
    }
  }

  //ProgressLoader : Close progress loader
  close = () =>{
    this.setState({ showModal: false });
  }

  // ProgressLoader : show progress loader
  open = () =>{
    this.setState({ showModal: true });
  }


  componentDidMount() {
    let id = this.FsnetUtil._getId();
    this.setState({
      subscriptionId: id
    })
    this.getLpPendingActions(id);
  }

  //Get list of Pending Actions
  getLpPendingActions(id) {
    if (id) {
      this.open();
      this.Fsnethttp.pendingActions(id).then(result => {
        this.close();
        window.scrollTo(0, 0);
        PubSub.publish('leftNavPage', 'pendingActions');
        this.setState({
          pendingActionsList: result.data.data
        })
      })
      .catch(error => {
        this.close();
        this.setState({
          pendingActionsList: [],
        })

      });
    }
  }

  openSignPage = (data,type) => () => {
    const url = `/signDocuments?sId=${this.FsnetUtil._encrypt(this.state.subscriptionId)}&type=${type}${this.FsnetUtil._encrypt(data.documentId) ? '&documentId='+this.FsnetUtil._encrypt(data.documentId):''}${data.changedPages ? '&fundId='+this.FsnetUtil._encrypt(data.fundId):''}${this.FsnetUtil._isIEEdge() ? '&token='+this.FsnetUtil.getToken():''}`
    signWin = window.open(url, '_blank', 'width = 1000px, height = 800px');
    var $this = this;
    var timer = setInterval(function () {
      if (signWin && signWin.closed) {
        clearInterval(timer);
        const obj = reactLocalStorage.get('signedDoc') ? JSON.parse(reactLocalStorage.get('signedDoc')):'';
        if ((obj && obj.type === 'success') || ($this.FsnetUtil._isIEEdge())) {
          if(obj && obj.url) {
            const docUrl = `${obj.url}?token=${$this.FsnetUtil.getToken()}`
            $this.FsnetUtil._openDoc(docUrl)
            localStorage.removeItem('signedDoc');
          }
          window.location.reload();
        } 
      }
    }, 1000);
  }

  redirectCommonAgreement = (data,type) => () => {
    //Status 10 : Closed.
    if(data.status === 10) {
      this.openSignPage(data,type)();
    } else {
      this.redirectToPage('review')();
    }
  }

  redirectToPage = (url) => () => {
    this.props.history.push(`/lp/${url}/${this.FsnetUtil._encrypt(this.state.subscriptionId)}?isPendingAction=true`);
  }

  render() {
    return (
      <div className="width100 form-main-div documentLockerContainer marginTopSticky">
        <div className="staticContent">
          <h2 className="title marginBottom2 marginLeft30 marginTop20">Pending Actions</h2>
        </div>
        {
          this.state.pendingActionsList.length > 0 &&
          <Row className="full-width marginTop20 marginLeft61">
            <div className="name-heading documentLockerDateCol">
              Date
                      </div>
            <div className="name-heading documentLockerTypeCol">
              Type
                      </div>
          </Row>
        }
        <div className={"documentLockerLp documentLockerLpTable " + (this.state.pendingActionsList.length === 0 ? 'borderNone' : 'marginTop30')}>
          {this.state.pendingActionsList.length > 0 ?
            this.state.pendingActionsList.map((record, index) => {
              return (
                  // record.documentType !== 'FUND_AGREEMENT' &&
                    <div className="userRow" key={index}>
                      <div className="documentLockerCol lp-name documentLockerDateCol">{record['createdAt']}
                        {
                          (record['createdAt'] && record['timezone']) ? <span> ({record['timezone']})</span> : ''
                        }
                      </div>
                      {
                        (record.documentType === 'COMMON_AGREEMENT') &&
                          <div className="documentLockerCol lp-name documentLockerTypeCol cursor ellipsis" title={record['name']} onClick={this.redirectCommonAgreement(record,'FA')}>{record['name']}</div>
                      }

                      {
                        (record.documentType === 'CHANGE_COMMITMENT_AGREEMENT') &&
                          <div className="documentLockerCol lp-name documentLockerTypeCol cursor" onClick={this.openSignPage(record,'CC')}>Capital Commitment Increase Letter</div>
                      }
                      {
                        (record.documentType === 'SUBSCRIPTION_AGREEMENT' && record.isSubscriptionContentUpdated && !record.isAdditionalQuestionAdded ) &&
                        <div className="documentLockerCol lp-name documentLockerTypeCol cursor" onClick={this.redirectToPage('review')}>Review Modified Subscription Agreement</div>
                      }
                      {
                        (record.status === 10 && (record.documentType === 'CHANGED_PAGES' || record.documentType === 'FUND_AGREEMENT'))&&
                        <div className="documentLockerCol lp-name documentLockerTypeCol cursor" onClick={this.openSignPage(record,'CPFA')}>Review Modified Fund Agreement</div>
                      }
                      {
                        (record.status !== 10 && (record.documentType === 'CHANGED_PAGES' || record.documentType === 'FUND_AGREEMENT'))&&
                        <div className="documentLockerCol lp-name documentLockerTypeCol cursor" onClick={this.openSignPage(record,'FA')}> Review Modified Fund Agreement</div>
                      }

                      {
                        (record.documentType === 'SUBSCRIPTION_AGREEMENT' && record.isAdditionalQuestionAdded && !record.isSubscriptionContentUpdated) &&
                          <div className="documentLockerCol lp-name documentLockerTypeCol cursor" onClick={this.redirectToPage('additionalQuestions')}>Subscription Agreement has been Changed</div>
                      }

                      {
                        (record.documentType === 'SUBSCRIPTION_AGREEMENT' && !record.isAdditionalQuestionAdded && !record.isSubscriptionContentUpdated) && 
                          <div className="documentLockerCol lp-name documentLockerTypeCol cursor" onClick={this.redirectToPage('review')}>Proceed To Sign</div>
                      
                      }
                      {
                        (record.documentType === 'SIDE_LETTER_AGREEMENT') &&
                          <div className="documentLockerCol lp-name documentLockerTypeCol cursor" onClick={this.openSignPage(record,'SL')}>Side Letter</div>
                      }
                      {
                        (record.documentType === 'AMENDMENT_AGREEMENT') &&
                          <div className="documentLockerCol lp-name documentLockerTypeCol cursor" onClick={this.openSignPage(record,'AA')}>Amendment to Fund Agreement</div>
                      }


                    </div>
              );
            })
            :
            <div className="title margin20 text-center">{this.Constants.NO_PENDING_ACTIONS_DOCUMENTS}</div>
          }
        </div>
        <Loader isShow={this.state.showModal}></Loader>
      </div>
    );
  }
}

export default PendingActionsComponent;

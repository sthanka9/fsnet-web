import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Radio, Row, Col, FormControl } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../../util/util';

class equityOwnersComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.equityOwnersChangeEvent = this.equityOwnersChangeEvent.bind(this);
        this.proceedToNext = this.proceedToNext.bind(this);
        this.proceedToBack = this.proceedToBack.bind(this);
        this.state = {
            showModal: false,
            investorType: 'LLC',
            equityOwnersPageValid:false,
            getInvestorObj:{},
            numberOfDirectEquityOwners:'',
            numberOfDirectEquityOwnersValid:false,
            numberOfDirectEquityOwnersBorder:false,
            numberOfDirectEquityOwnersMsz:'',
            existingOrProspectiveInvestorsOfTheFund:'',
            existingOrProspectiveInvestorsOfTheFundValid:false,
            numberOfexistingOrProspectives:'',
            numberOfexistingOrProspectivesValid:false,
            numberOfexistingOrProspectivesMsz:'',
            numberOfexistingOrProspectivesBorder:false,
            equityOwnersErrorMsz:'',
            jsonData:{}
        }

    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.getJsonData();
        this.getSubscriptionDetails(id);
    }

    getJsonData() {
        this.Fsnethttp.getJson('equityOwners').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getSubscriptionDetails(id) {
        
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    let obj = result.data.data;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    obj['currentPageCount'] = 5;
                    obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                    PubSub.publish('investorData',obj );
                    this.setState({
                        getInvestorObj: result.data.data,
                        investorType: result.data.data.investorType?result.data.data.investorType:'LLC',
                        numberOfDirectEquityOwners: result.data.data.numberOfDirectEquityOwners,
                        existingOrProspectiveInvestorsOfTheFund: result.data.data.existingOrProspectiveInvestorsOfTheFund,
                        numberOfexistingOrProspectives: result.data.data.numberOfexistingOrProspectives,
                    },()=>{
                        this.updateInvestorInputFields(this.state.getInvestorObj)
                    })
                }
            })
            .catch(error => {
                this.close();
            });
        }
    }

    

    updateInvestorInputFields(data) {
        if(this.state.numberOfDirectEquityOwners && (this.state.existingOrProspectiveInvestorsOfTheFund === false || (this.state.existingOrProspectiveInvestorsOfTheFund === true && this.state.numberOfexistingOrProspectives))) {
            this.setState({
                equityOwnersPageValid: true
            })
        }
    }

    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    equityOwnersChangeEvent(event, type, radioTypeName, blur) {
        let key = type;
        let value = !event.target.value.trim() ? '' : event.target.value;
        let dataObj = {};
        switch(type) {
            case 'radio':
                this.setState({
                    [blur]: radioTypeName,
                })
                let name = blur+'Valid'
                dataObj ={
                    [name] :true
                };
                this.updateStateParams(dataObj);
                break;
            case key:
                
                if(value === '' || value === undefined) {
                    this.setState({
                        [key+'Msz']: this.Constants[radioTypeName],
                        [key+'Valid']: false,
                        [key+'Border']: true,
                        [key]: ''
                    })
                    let name = key+'Valid'
                    dataObj ={
                        [name] :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    if (!value.trim() && key === 'numberOfDirectEquityOwners') {
                        this.setState({
                            [key]: ''
                        })
                    } else {
                        if(( parseInt(value) <= 0 || parseInt(value) >=1000 ) && key === 'numberOfDirectEquityOwners') {
                            return true;
                        }
                        this.setState({
                            [key+'Msz']: '',
                            [key+'Valid']: true,
                            [key+'Border']: false,
                            [key]: value
                        })
                        let name = key+'Valid'
                        dataObj ={
                            [name] :true
                        };
                        this.updateStateParams(dataObj);
                    }
                }
                break;
           
            default:
                break;
        }
    }

    isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            // return false;
            evt.preventDefault();
        }
        return true;
    }

    // Update state params values and login button visibility

    updateStateParams(updatedDataObject){
        this.setState(updatedDataObject, ()=>{
            this.enableDisableInvestorDetailsButton();
        });
    }

    // Enable / Disble functionality of Investor Details next Button
    enableDisableInvestorDetailsButton(){
        let status;
        status = (this.state.numberOfDirectEquityOwners && this.state.existingOrProspectiveInvestorsOfTheFund !== null) ? true : false;
        this.setState({
            equityOwnersPageValid : status,
        });
    }

    proceedToNext() {
        if(this.state.existingOrProspectiveInvestorsOfTheFund === true && (this.state.numberOfexistingOrProspectives === '' || this.state.numberOfexistingOrProspectives === null)) {
            let key = 'numberOfexistingOrProspectives'
            this.setState({
                [key+'Msz']: this.Constants.EXISTING_INVESTORS_REQUIRED,
                [key+'Valid']: false,
                [key+'Border']: true,
            })
            return true;
        }
        let postobj = {investorType:this.state.investorType,subscriptionId:this.state.getInvestorObj.id, step:6,numberOfDirectEquityOwners:this.state.numberOfDirectEquityOwners, existingOrProspectiveInvestorsOfTheFund:this.state.existingOrProspectiveInvestorsOfTheFund }
        postobj['numberOfexistingOrProspectives'] = this.state.existingOrProspectiveInvestorsOfTheFund === true ? this.state.numberOfexistingOrProspectives:null;
        this.open();
        
        this.Fsnethttp.updateLpSubscriptionDetails(postobj).then(result => {
            this.close();
            if (result.data) {
                this.props.history.push('/lp/entityProposing/'+result.data.data.id);
            }
        })
        .catch(error => {
            this.close();
            if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                this.setState({
                    equityOwnersErrorMsz: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    equityOwnersErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                });
            }
        });
    }

    proceedToBack () {
        this.props.history.push('/lp/companiesAct/'+this.state.getInvestorObj.id);
    }

    render() {
        return (
            <div className="accreditedInvestor width100 marginTopSticky">
                <div className="formGridDivMargins min-height-400">
                    {/* llc investor type block starts */}
                    <div className="title">{this.state.jsonData.EQUITY_OWNERS}</div>
                    <Row className="step1Form-row" hidden={this.state.investorType !== 'LLC'}>
                        <Col xs={12} md={12}>
                            <label className="form-label width100">{this.state.jsonData.ENTITY_SPECIFY_EQUITY_OWNERS}</label>
                            <FormControl type="text" placeholder="Enter number" className={"inputFormControl inputWidth290 " + (this.state.numberOfDirectEquityOwnersBorder ? 'inputError' : '')} value= {this.state.numberOfDirectEquityOwners}  onChange={(e) => this.equityOwnersChangeEvent(e,'numberOfDirectEquityOwners', 'EQUITY_OWNERS_REQUIRED')} onKeyPress={(e) => {this.isNumber(e)}}  onBlur={(e) => this.equityOwnersChangeEvent(e,'numberOfDirectEquityOwners','EQUITY_OWNERS_REQUIRED')}/>
                            <span className="error">{this.state.numberOfDirectEquityOwnersMsz}</span>
                        </Col>
                        <Col xs={12} md={12} className="marginTop24">
                            <label className="form-label width100">{this.state.jsonData.EXISTING_OR_PROSPECTIVE_INVESTORS}</label>
                            <Radio name="existingOrProspectiveInvestorsOfTheFund" inline checked={this.state.existingOrProspectiveInvestorsOfTheFund === true} onChange={(e) => this.equityOwnersChangeEvent(e, 'radio', true, 'existingOrProspectiveInvestorsOfTheFund')}>&nbsp; Yes
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="existingOrProspectiveInvestorsOfTheFund" inline checked={this.state.existingOrProspectiveInvestorsOfTheFund === false} onChange={(e) => this.equityOwnersChangeEvent(e, 'radio', false, 'existingOrProspectiveInvestorsOfTheFund')}>&nbsp; No
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>
                        <Col xs={12} md={12} className="marginTop24" hidden={this.state.existingOrProspectiveInvestorsOfTheFund !== true}>
                            <label className="form-label width100">{this.state.jsonData.LIST_EXISTING_OR_PROSPECTIVE_INVESTORS}</label>
                            <FormControl type="text" placeholder="Enter investors." className={"inputFormControl inputWidth290 " + (this.state.numberOfexistingOrProspectivesBorder ? 'inputError' : '')} value= {this.state.numberOfexistingOrProspectives}  onChange={(e) => this.equityOwnersChangeEvent(e,'numberOfexistingOrProspectives', 'EXISTING_INVESTORS_REQUIRED')} onBlur={(e) => this.equityOwnersChangeEvent(e,'numberOfexistingOrProspectives','EXISTING_INVESTORS_REQUIRED')}/>
                            <span className="error">{this.state.numberOfexistingOrProspectivesMsz}</span>
                        </Col>
                    </Row>
                    {/* llc investor type block ends */}
                    {/* revocableTrust investor type block starts */}
                    <Row className="step1Form-row" hidden={this.state.investorType !== 'revocableTrust'}>
                        <Col xs={12} md={12}>
                            <label className="title">{this.state.jsonData.ENTITY_SPECIFY_EQUITY_OWNERS}</label>
                        </Col>
                        <Col xs={12} md={12} className="marginTop24">
                            <label className="form-label width100">{this.state.jsonData.EXISTING_OR_PROSPECTIVE_INVESTORS}</label>
                            <Radio name="rule501" inline id="yesCheckbox">&nbsp; Yes
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="rule501" inline id="yesCheckbox">&nbsp; No
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>
                        <Col xs={12} md={12} className="marginTop24">
                            <label className="form-label width100">{this.state.jsonData.LIST_EXISTING_OR_PROSPECTIVE_INVESTORS}</label>
                            <FormControl type="text" placeholder="Enter investors." className="inputFormControl inputWidth290" />
                        <span className="error"></span>
                        </Col>
                    </Row>
                    {/* revocableTrust investor type block ends */}
                    {/* iRevocableTrust investor type block starts */}
                    <Row className="step1Form-row" hidden={this.state.investorType !== 'iRevocableTrust'}>
                        <Col xs={12} md={12}>
                            <label className="title">{this.state.jsonData.ENTITY_SPECIFY_EQUITY_OWNERS}</label>
                        </Col>
                        <Col xs={12} md={12} className="marginTop24">
                            <label className="form-label width100">{this.state.jsonData.EXISTING_OR_PROSPECTIVE_INVESTORS}</label>
                            <Radio name="rule501" inline id="yesCheckbox">&nbsp; Yes
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="rule501" inline id="yesCheckbox">&nbsp; No
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>
                        <Col xs={12} md={12} className="marginTop24">
                            <label className="form-label width100">{this.state.jsonData.LIST_EXISTING_OR_PROSPECTIVE_INVESTORS}</label>
                        <FormControl type="text" placeholder="Enter investors." className="inputFormControl inputWidth290" />
                            <span className="error"></span>
                        </Col>
                    </Row>
                    {/* iRevocableTrust investor type block ends */}
                </div>
                <div className="margin30 error">{this.state.equityOwnersErrorMsz}</div>
               <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.getInvestorObj.subscriptionStatus && this.state.getInvestorObj.subscriptionStatus.name === 'Closed'}>
                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className={"fa fa-chevron-right " + (!this.state.equityOwnersPageValid ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default equityOwnersComponent;


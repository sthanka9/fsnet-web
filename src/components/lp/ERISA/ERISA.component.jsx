import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Radio, Row, Col, FormControl, Checkbox as CBox } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';

var tooltipsCount=0,objList=[], objListArray=[];
class ERISAComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.erisaChangeEvent = this.erisaChangeEvent.bind(this);
        this.proceedToNext = this.proceedToNext.bind(this);
        this.proceedToBack = this.proceedToBack.bind(this);
        this.openErisaModal = this.openErisaModal.bind(this);
        this.openAdviserModal = this.openAdviserModal.bind(this);
        this.openExchangeModal = this.openExchangeModal.bind(this);
        this.openCodeModal = this.openCodeModal.bind(this);
        this.openPlanRegulationModal = this.openPlanRegulationModal.bind(this);
        this.state = {
            showModal: false,
            investorType: 'LLC',
            investorSubType: 0,
            erisaPageValid:false,
            getInvestorObj:{},
            employeeBenefitPlan:false,
            planAsDefinedInSection4975e1:false,
            benefitPlanInvestor:false,
            aggrement1: '',
            fiduciaryEntityIvestment:'',
            entityDecisionToInvestInFund:'',
            totalValueOfEquityInterests: '',
            totalValueOfEquityInterestsMask: '',
            totalValueOfEquityInterestsValid:false,
            totalValueOfEquityInterestsMsz: '',
            totalValueOfEquityInterestsBorder:false,
            erisaErrorMsz:'',
            showErisaNextStep: false,
            jsonData:{},
            isFormChanged:false
        }

    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.getJsonData();
        this.getSubscriptionDetails(id);
    }
    checkTooltips(text,type) {
        let tooltipWords = ['ERISA', 'Code', 'Plan Asset Regulation', 'Advisers Act', 'Exchange Act'];
        let count = 0 ;
        for(let idx=0; idx<tooltipWords.length; idx++) {
            if(text && text.indexOf(tooltipWords[idx]) > -1 && objList.indexOf(type) === -1) {
                //    count++;
                    // let index = tooltipsCount+count
                // text = text.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${index}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                // text = text.split(tooltipWords[idx]).join(`<span className="helpWord" id="anchor${index}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                let arrayList = this.FsnetUtil.allIndexOf(text,tooltipWords[idx])
                let splitText = [];
                let currentIndex = 0;
                for(let jdx=0; jdx<arrayList.length; jdx++) {
                    let jdxLength = arrayList[jdx]+tooltipWords[idx].length;
                    if(jdx === (arrayList.length-1)) {
                        splitText.push(text.substr(currentIndex,text.length))
                    } else {
                        splitText.push(text.substr(currentIndex,jdxLength))
                        currentIndex = jdxLength
                    }
                }
                let newText = ''
                if(splitText.length === 1) {
                    count++;
                    let indexPos = tooltipsCount+count
                    text = text.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                }else {
                    for(let pos of splitText) {
                        count++;
                        let indexPos = tooltipsCount+count
                        newText = newText+pos.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                    }
                    text = newText;
                }

            }
            if(text && idx === tooltipWords.length-1 && objList.indexOf(type) === -1) {
                objListArray.push({type:type,text:text})
                objList.push(type)
            }
        }
        tooltipsCount = tooltipsCount+count;
        for(let index of objListArray) {
            if(index['type'] && index['text']) {
                if(index['type'] === type) {
                    return index['text'];
                }
            }
        }
        return text
    }    

    addClickEvent(obj) {
        if(tooltipsCount > 0) {
            for(let idx=0; idx<tooltipsCount; idx++) {
                if(document.getElementById("anchor"+(idx+1))) {
                    document.getElementById("anchor"+(idx+1)).addEventListener("click", function(){
                        let name = document.getElementById("anchor"+(idx+1)).getAttribute('tooltip_id');
                        let modalType, type;
                        if(name === 'ERISA') {
                            modalType = 'actModalWindow';
                            type = 'erisa'
                        } else if(name === 'Code') {
                            modalType = 'actModalWindow';
                            type = 'code'
                        }else if(name === 'Plan Asset Regulation') {
                            modalType = 'actModalWindow';
                            type = 'planAssetRegulation'
                        }else if(name === 'Advisers Act') {
                            modalType = 'actModalWindow';
                            type = 'adviser'
                        }else if(name === 'Exchange Act') {
                            modalType = 'actModalWindow';
                            type = 'exchange'
                        }
                        PubSub.publish('openModal', {investorType: obj.investorType, investorSubType: obj.investorSubType, modalType: modalType, type: type});
                    });
                }
            }
        }
    }

    getJsonData() {
        this.Fsnethttp.getJson('erisa').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }


    getSubscriptionDetails(id) {
        
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    let obj = result.data.data;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    obj['currentPageCount'] = result.data.data.investorType == 'Trust' ? 6 : 7;
                    obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                    PubSub.publish('investorData',obj );
                    this.setState({
                        getInvestorObj: result.data.data,
                        investorType: result.data.data.investorType?result.data.data.investorType:'LLC',
                        investorSubType: result.data.data.investorSubType ? result.data.data.investorSubType : (result.data.data.investorType == 'Trust' ? 9 : 0),
                        employeeBenefitPlan:result.data.data.employeeBenefitPlan?result.data.data.employeeBenefitPlan:false,
                        planAsDefinedInSection4975e1:result.data.data.planAsDefinedInSection4975e1?result.data.data.planAsDefinedInSection4975e1:false,
                        benefitPlanInvestor:result.data.data.benefitPlanInvestor?result.data.data.benefitPlanInvestor:false,
                        aggrement1: result.data.data.aggrement1,
                        fiduciaryEntityIvestment:result.data.data.fiduciaryEntityIvestment,
                        entityDecisionToInvestInFund:result.data.data.entityDecisionToInvestInFund,
                        totalValueOfEquityInterests: result.data.data.totalValueOfEquityInterests,
                        totalValueOfEquityInterestsMask: result.data.data.totalValueOfEquityInterests ? `${result.data.data.totalValueOfEquityInterests}%`:null,
                        
                    },()=>{
                        this.enableDisableErisaDetailsButton();
                        this.addClickEvent(result.data.data);
                    })
                }
            })
            .catch(error => {
                this.close();
            });
        }
    }


    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    openErisaModal(e) {
        if(e) {
            e.preventDefault();
        }
        PubSub.publish('openModal', {investorType: this.state.investorType, modalType: 'actModalWindow', type: 'erisa'});
    }

    openCodeModal(e) {
        if(e) {
            e.preventDefault();
        }
        PubSub.publish('openModal', {investorType: this.state.investorType, modalType: 'actModalWindow', type: 'code'});
    }

    openAdviserModal(e) {
        if(e) {
            e.preventDefault();
        }
        PubSub.publish('openModal', {investorType: this.state.investorType, modalType: 'actModalWindow', type: 'adviser'});
    }

    openExchangeModal(e) {
        if(e) {
            e.preventDefault();
        }
        PubSub.publish('openModal', {investorType: this.state.investorType, modalType: 'actModalWindow', type: 'exchange'});
    }

    openPlanRegulationModal() {
        PubSub.publish('openModal', {investorType: this.state.investorType, modalType: 'actModalWindow', type: 'planAssetRegulation'});
    }

    erisaChangeEvent(event, type, radioTypeName, blur) {
        this.setState({
            erisaErrorMsz: '',
            isFormChanged:true
        });
        let key = type;
        let value = event.target.value.trim()
        let dataObj = {};
        switch(type) {
            case 'totalValueOfEquityInterests':
                const re = this.Constants._99REGEX;
                if (!re.test(value.trim()) || (blur && isNaN(value))) {
                    this.setState({
                        [key]:this.state.totalValueOfEquityInterests && this.state.totalValueOfEquityInterests != '.' ? this.state.totalValueOfEquityInterests : '',
                        totalValueOfEquityInterestsMask: this.state.totalValueOfEquityInterests && this.state.totalValueOfEquityInterests != '.' && blur ? `${parseFloat(value)}%` : this.state.totalValueOfEquityInterestsMask
                    })
                    return true;
                } else {
                    if(blur && (parseFloat(value) <= 0 || parseFloat(value) > 100)) {
                        this.setState({
                            [key]: this.state.totalValueOfEquityInterests != '0.' && parseFloat(value) != 0 ? this.state.totalValueOfEquityInterests:'',
                            totalValueOfEquityInterestsMask: this.state.totalValueOfEquityInterests != '0.' && parseFloat(value) != 0 ?  this.state.totalValueOfEquityInterestsMask:''
                        },()=>{
                            if(this.state.showErisaNextStep) {
                                this.enableDisableNextStepButton();
                            } else {
                                this.enableDisableErisaDetailsButton();
                            }
                        })
                        return true;
                    } else {
                        if((parseFloat(value) < 0 || parseFloat(value) > 100)) {
                            return true;
                        }
                    }
                }
                if(value === '' || value === undefined || value == '.') {
                    this.setState({
                        [key+'Msz']: this.state.investorType == 'LLC' ? this.Constants.TOTAL_VALUE_REQUIRED_LLC : this.Constants.TOTAL_VALUE_REQUIRED_TRUST,
                        [key+'Valid']: false,
                        [key+'Border']: true,
                        [key]: '',
                        totalValueOfEquityInterestsMask: blur && value && value != '.' ? `${parseFloat(value)}%` : ''
                    },()=>{
                        if(this.state.showErisaNextStep) {
                            this.enableDisableNextStepButton();
                        } else {
                            this.enableDisableErisaDetailsButton();
                        }
                    })
                } else {
                    this.setState({
                        [key+'Msz']: '',
                        [key+'Valid']: true,
                        [key+'Border']: false,
                        [key]: parseFloat(value), 
                        totalValueOfEquityInterestsMask: blur ? `${parseFloat(value)}%` : value
                    },()=>{
                        if(this.state.showErisaNextStep) {
                            this.enableDisableNextStepButton();
                        } else {
                            this.enableDisableErisaDetailsButton();
                        }
                    })
                }
                break;
            case 'fiduciaryEntityIvestment':
                this.setState({
                    fiduciaryEntityIvestment : event.target.checked
                });
                break;
            case 'entityDecisionToInvestInFund':
                this.setState({
                    entityDecisionToInvestInFund : event.target.checked
                });
                break;
            case 'radio':
                this.setState({
                    [blur]: radioTypeName,
                },()=>{
                    if(this.state.showErisaNextStep) {
                        this.enableDisableNextStepButton();
                    } else {
                        this.enableDisableErisaDetailsButton();
                    }
                })
                break;
           
            default:
                break;
        }
    }

    isEmpty(value){
        return (value == null || value.length === 0);
    }

    // Enable / Disble functionality of Erisa Details next Button
    enableDisableErisaDetailsButton(){
        if(!this.isEmpty(this.state.employeeBenefitPlan) || !this.isEmpty(this.state.planAsDefinedInSection4975e1) || !this.isEmpty(this.state.benefitPlanInvestor)) {
            this.setState({
                erisaPageValid : true,
            });
        } else {
            this.setState({
                erisaPageValid : false,
            });
        }
    }

    //Enable/Disable Erisa next steps next button
    enableDisableNextStepButton() {
        if(this.state.benefitPlanInvestor) {
            if(this.state.totalValueOfEquityInterests !== '' && this.state.totalValueOfEquityInterests !== null && this.state.totalValueOfEquityInterests !== undefined) {
                this.setState({
                    erisaPageValid : true,
                });
            } else {
                this.setState({
                    erisaPageValid : false,
                });
            }
        } else if(this.state.employeeBenefitPlan || this.state.planAsDefinedInSection4975e1) {
            if(this.state.aggrement1 !== '' && this.state.aggrement1 !== null && this.state.aggrement1 !== undefined) {
                this.setState({
                    erisaPageValid : true,
                });
            } else {
                this.setState({
                    erisaPageValid : false,
                });
            }
        } 
    }

    proceedToNext() {
        window.scrollTo(0, 0);
        if(this.state.benefitPlanInvestor) {
            if(!this.state.showErisaNextStep) {
                this.setState({
                    showErisaNextStep: true
                },()=>{
                    if(this.state.showErisaNextStep) {
                        this.enableDisableNextStepButton();
                    } else {
                        this.enableDisableErisaDetailsButton();
                    }
                })
            } else {
                this.submitErisaDetails();
            }
        } else {
            this.submitErisaDetails();
        }
    }

    submitErisaDetails() {
        let postobj = {investorType:this.state.investorType,investorSubType: this.state.investorSubType, subscriptionId:this.state.getInvestorObj.id, step:this.state.investorType == 'Trust' ? 7 : 7,employeeBenefitPlan:this.state.employeeBenefitPlan, planAsDefinedInSection4975e1:this.state.planAsDefinedInSection4975e1,benefitPlanInvestor:this.state.benefitPlanInvestor,isFormChanged:this.state.isFormChanged}
        if(this.state.benefitPlanInvestor) {
            postobj['totalValueOfEquityInterests'] = this.state.totalValueOfEquityInterests;
            postobj['aggrement1'] = null;
            postobj['fiduciaryEntityIvestment'] = null;
            postobj['entityDecisionToInvestInFund'] = null;
        }
        // if(this.state.employeeBenefitPlan || this.state.planAsDefinedInSection4975e1) {
        //     postobj['aggrement1'] = this.state.aggrement1;
        //     postobj['fiduciaryEntityIvestment'] = this.state.fiduciaryEntityIvestment;
        //     postobj['entityDecisionToInvestInFund'] = this.state.entityDecisionToInvestInFund;
        //     postobj['totalValueOfEquityInterests'] = null;
        // }
        this.open();
        
        this.Fsnethttp.updateLpSubscriptionDetails(postobj).then(result => {
            this.close();
            if (result.data) {
                let obj = result.data.data;
                obj['currentInvestorInfoPageNumber'] = 1;
                obj['currentPageCount'] = result.data.data.investorType == 'Trust' ? 6 : 7;
                obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                PubSub.publish('investorData',obj);
                let url = this.state.getInvestorObj.investorQuestions.length == 0 ? '/lp/capitalCommitment/' : '/lp/additionalQuestions/'
                this.props.history.push(url+this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
            }
        })
        .catch(error => {
            this.close();
            if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                this.setState({
                    erisaErrorMsz: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    erisaErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                });
            }
        });
    }

    proceedToBack () {
        window.scrollTo(0, 0);
        if(this.state.showErisaNextStep) {
            this.setState({
                showErisaNextStep: false
            })
            this.enableDisableErisaDetailsButton();
        } else {
            // let url = this.state.investorType == 'LLC' ? '/lp/entityProposing/' : '/lp/companiesAct/'
            let url = '/lp/entityProposing/';
            this.props.history.push(url+this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
        }
    }

    render() {
        return (
            <div className="accreditedInvestor width100 marginTopSticky">
                <div className="formGridDivMargins min-height-400">
                    {/* llc investor type block starts */}
                    <div className="title">{this.state.jsonData.ERISA}</div>
                    <Row className="step1Form-row" hidden={this.state.showErisaNextStep !== false}>
                        <Col xs={12} md={12}>
                            {
                                this.state.investorType == 'Trust'
                                ?
                                    <label className="form-label width100">{this.state.jsonData.REGARDING_ENTITY_PROPOSING_CHECK_TRUE_FALSE}:</label>
                                :
                                    <label className="form-label width100">{this.state.jsonData.REGARDING_LOOK_THROUGH_ISSUES_CHECK_TRUE_FALSE}:</label>
                            }
                        </Col>
                        <Col xs={12} md={12}>
                            {
                                this.state.investorType == 'Trust'
                                ?
                                    <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.TRUST_AN_EMPLOYEE_BENEFIT_PLAN_SECTION_33,'obj1')
                                    }}></label>
                                :
                                    <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.ENTITY_AN_EMPLOYEE_BENEFIT_PLAN_SECTION_33,'obj2')
                                    }}></label>
                            }
                            <div className="checkBoxText">
                                <Radio name="employeeBenefitPlan" disabled={(this.state.planAsDefinedInSection4975e1 === true && this.state.benefitPlanInvestor === true) || (this.state.planAsDefinedInSection4975e1 === false && this.state.benefitPlanInvestor === true)} inline checked={this.state.employeeBenefitPlan === true} onChange={(e) => this.erisaChangeEvent(e, 'radio', true, 'employeeBenefitPlan')}>
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">True</div>
                                </Radio>
                                <Radio name="employeeBenefitPlan" inline checked={this.state.employeeBenefitPlan === false} onChange={(e) => this.erisaChangeEvent(e, 'radio', false, 'employeeBenefitPlan')}>
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">False</div>
                                </Radio>
                            </div>
                        </Col>
                        <Col xs={12} md={12}>
                            {
                                this.state.investorType == 'Trust'
                                ?
                                    <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.TRUST_IS_A_PLAN_SECTION_4975E1,'obj3')
                                    }}></label>
                                :
                                    <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.ENTITY_IS_A_PLAN_SECTION_4975E1,'obj4')
                                    }}></label>
                            }
                            <div className="checkBoxText">
                                <Radio name="planAsDefinedInSection4975e1" inline disabled={(this.state.employeeBenefitPlan === true && this.state.benefitPlanInvestor === true) || (this.state.employeeBenefitPlan === false && this.state.benefitPlanInvestor === true)} checked={this.state.planAsDefinedInSection4975e1 === true} onChange={(e) => this.erisaChangeEvent(e, 'radio', true, 'planAsDefinedInSection4975e1')}>
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">True</div>
                                </Radio>
                                <Radio name="planAsDefinedInSection4975e1"  inline checked={this.state.planAsDefinedInSection4975e1 === false} onChange={(e) => this.erisaChangeEvent(e, 'radio', false, 'planAsDefinedInSection4975e1')}>
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">False</div>
                                </Radio>
                            </div>
                        </Col>
                        <Col xs={12} md={12}>
                            {
                                this.state.investorType == 'Trust'
                                ?
                                    <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.TRUST_AN_ENTITY_BENEFIT_PLAN_INVESTOR_PLAN_ASSET_REGULATION,'obj6')
                                    }}></label>
                                :
                                    <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.INVESTOR_AN_ENTITY_BENEFIT_PLAN_INVESTOR_PLAN_ASSET_REGULATION,'obj7')
                                    }}></label>
                            }
                            <div className="checkBoxText">
                                <Radio name="benefitPlanInvestor" inline disabled={(this.state.planAsDefinedInSection4975e1 === true && this.state.employeeBenefitPlan === true) || (this.state.planAsDefinedInSection4975e1 === false && this.state.employeeBenefitPlan === true) || (this.state.employeeBenefitPlan === false && this.state.planAsDefinedInSection4975e1 === true)} checked={this.state.benefitPlanInvestor === true} onChange={(e) => this.erisaChangeEvent(e, 'radio', true, 'benefitPlanInvestor')}>
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">True</div>
                                </Radio>
                                <Radio name="benefitPlanInvestor" inline checked={this.state.benefitPlanInvestor === false} onChange={(e) => this.erisaChangeEvent(e, 'radio', false, 'benefitPlanInvestor')}>
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">False</div>
                                </Radio>
                            </div>
                        </Col>
                    </Row>
                    <Row hidden={this.state.showErisaNextStep !== true}>
                        <div hidden={this.state.benefitPlanInvestor !== true}>
                            <Col xs={12} md={12}>
                                <label className="form-label width100 erisa-heading">{this.state.investorType == 'LLC' ? this.state.jsonData.INPUT_LABEL_LLC : this.state.jsonData.INPUT_LABEL_TRUST}</label>
                            </Col>
                            <Col xs={12} md={12}>
                                <FormControl type="text" placeholder="Enter percentage" className={"inputFormControl inputWidth290 marginTop20 " + (this.state.totalValueOfEquityInterestsBorder ? 'inputError' : '')} value= {this.state.totalValueOfEquityInterestsMask}  onChange={(e) => this.erisaChangeEvent(e,'totalValueOfEquityInterests', 'TOTAL_VALUE_REQUIRED')} onBlur={(e) => this.erisaChangeEvent(e,'totalValueOfEquityInterests','TOTAL_VALUE_REQUIRED',true)} onFocus={(e) => this.setState({totalValueOfEquityInterestsMask:this.state.totalValueOfEquityInterests})}/>
                                <span className="error">{this.state.totalValueOfEquityInterestsMsz}</span>
                            </Col>
                        </div>
                            
                        <Col xs={12} md={12} className="marginTop10" hidden={this.state.benefitPlanInvestor !== false}>
                            <label className="form-label width100 erisa-heading">{this.state.jsonData.CONFIRM_FOLLOWING_TO_CONTINUE}:</label>
                            <CBox inline className="cBoxFullAlign" checked={this.state.fiduciaryEntityIvestment === true} onChange={(e) => this.erisaChangeEvent(e, 'fiduciaryEntityIvestment', 1)}>
                                <span className="checkbox-checkmark checkmark"></span>
                                {
                                    this.state.investorType == 'Trust'
                                ?
                                    // <div className="marginLeft6">The person executing this agreement is a fiduciary of the Trust making the investment.</div>
                                    <div className="marginLeft6">{this.state.jsonData.EXECUTING_AGREEMENT_IS_FIDUCIARY_OF_INVESTOR}</div>
                                :
                                    // <div className="marginLeft6">The person executing this agreement is a fiduciary of the Entity making the investment.</div>
                                    <div className="marginLeft6">{this.state.jsonData.EXECUTING_AGREEMENT_IS_FIDUCIARY_OF_INVESTOR}</div>
                                }
                                {/* <div className="marginLeft6">The person executing this agreement is a fiduciary of the Entity making the investment.</div> */}
                            </CBox>
                            <CBox inline className="cBoxFullAlign" checked={this.state.entityDecisionToInvestInFund === true} onChange={(e) => this.erisaChangeEvent(e, 'entityDecisionToInvestInFund', 2)}>
                                <span className="checkbox-checkmark checkmark"></span>
                                {
                                    this.state.investorType == 'Trust'
                                ?
                                    // <div className="marginLeft6">The Trust’s decision to invest in the Fund was made by the person executing this agreement and such person (i) is a fiduciary under <span className="helpWord" onClick={(e) => {this.openErisaModal(e)}}>ERISA</span> or Section 4975 of the <span className="helpWord" onClick={(e) => {this.openCodeModal(e)}}>Code</span>, or both, with respect to the Trust’s decision to invest in the Fund; (ii) is responsible for exercising independent judgment in evaluating the investment in the Fund; (iii) is independent of the Fund, its general partner or similar manager and any and all affiliates of the preceding; and (iv) is capable of evaluating investment risks independently, both in general and with regard to particular transactions and investment strategies, including the decision on behalf of the Trust to invest in the Fund.</div>
                                    <div className="marginLeft6" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.INVESTORS_DECISION_INVEST_FUND_MADE_BY_PERSON_EXECUTING_AGREEMENT,'obj8')
                                    }}></div>
                                :
                                    // <div className="marginLeft6">The Entity’s decision to invest in the Fund was made by the person executing this agreement and such person (i) is a fiduciary under <span className="helpWord" onClick={(e) => {this.openErisaModal(e)}}>ERISA</span> or Section 4975 of the <span className="helpWord" onClick={(e) => {this.openCodeModal(e)}}>Code</span>, or both, with respect to the Entity’s decision to invest in the Fund; (ii) is responsible for exercising independent judgment in evaluating the investment in the Fund; (iii) is independent of the Fund, its general partner or similar manager and any and all affiliates of the preceding; and (iv) is capable of evaluating investment risks independently, both in general and with regard to particular transactions and investment strategies, including the decision on behalf of the Entity to invest in the Fund.</div>
                                    <div className="marginLeft6"dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.INVESTORS_DECISION_INVEST_FUND_MADE_BY_PERSON_EXECUTING_AGREEMENT,'obj9')
                                    }}></div>
                                }
                                {/* <div className="marginLeft6">The Entity’s decision to invest in the fund was made by the person executing this agreement and such person (i) is a fiduciary under <span className="helpWord" onClick={this.openErisaModal}>ERISA</span> or Section 4975 of the <span className="helpWord" onClick={this.openCodeModal}>Code</span>, or both, with respect to the Entity’s decision to invest in the Fund; (ii) is responsible for exercising independent judgment in evaluating the investment in the Fund; (iii) is independent of the Fund, its general partner or similar manager and any and all affiliates of the preceding; and (iv) is capable of evaluating investment risks independently, both in general and with regard to particular transactions and investment strategies, including the decision on behalf of the Entity to invest in the Fund.</div> */}
                            </CBox>
                        </Col>

                        {/* <Row className="step1Form-row"> */}
                            <Col xs={12} md={12} className="step1Form-row" hidden={this.state.benefitPlanInvestor !== false}>
                                <label className="form-label width100 erisa-heading">{this.state.jsonData.PERSON_EXECUTING_THIS_AGREEMENT_ONE_OF_THE_FOLLOWING}:</label>
                                <div className="checkBoxText" hidden={this.state.employeeBenefitPlan !== true}>
                                    <Radio name="aggrement1" className="width100 left-gap" inline checked={this.state.aggrement1 === 1} onChange={(e) => this.erisaChangeEvent(e, 'radio',  1, 'aggrement1')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText">{this.state.jsonData.INDEPENDENT_FIDUCIARY_THAT_HOLDS_OR_MANAGEMENT_OR_CONTROL};</div>
                                    </Radio>
                                </div>
                                <div className="checkBoxText">
                                    <Radio name="aggrement1" className="width100 left-gap" inline checked={this.state.aggrement1 === 2} onChange={(e) => this.erisaChangeEvent(e, 'radio', 2, 'aggrement1')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BANK_AS_DEFINED_SECTION_202_ADVISERS_ACT,'obj10')
                                        }}></div>
                                    </Radio>
                                </div>
                                <div className="checkBoxText">
                                    <Radio name="aggrement1" className="width100 left-gap" inline checked={this.state.aggrement1 === 3} onChange={(e) => this.erisaChangeEvent(e,'radio', 3, 'aggrement1')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText">{this.state.jsonData.INSURANCE_CARRIER_QUALIFIED_UNDER_LAWS_MORE_THAN_ONE_US_STATE}</div>
                                    </Radio>
                                </div>
                                <div className="checkBoxText">
                                    <Radio name="aggrement1" className="width100 left-gap" inline checked={this.state.aggrement1 === 4} onChange={(e) => this.erisaChangeEvent(e,'radio', 4,'aggrement1')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.INVESTMENT_ADVISER_REGISTERED_UNDER_ADVISERS_ACT,'obj11')
                                    }}></div>
                                    </Radio>
                                </div>
                                <div className="checkBoxText">
                                    <Radio name="aggrement1"  className="width100 left-gap" inline checked={this.state.aggrement1 === 5} onChange={(e) => this.erisaChangeEvent(e,'radio', 5,'aggrement1')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BROKER_DEALER_REGISTERED_EXCHANGE_ACT,'obj12')
                                    }}></div>
                                    </Radio>
                                </div>
                            </Col>
                        {/* </Row> */}
                    </Row>
                    {/* llc investor type block ends */}
                    {/* revocableTrust investor type block starts */}
                    <Row className="step1Form-row" hidden={this.state.investorType !== 'revocableTrust'}>
                    <div className="col-md-12 col-xs-12 title"></div>
                        <Col xs={12} md={12}>
                            <label className="form-label width100">{this.state.jsonData.REGARDING_LOOK_THROUGH_ISSUES_CHECK_TRUE_FALSE}:</label>
                        </Col>
                        <Col xs={12} md={12}>
                            <label className="form-label width100"  dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.TRUST_AN_EMPLOYEE_BENEFIT_PLAN_SECTION_33,'obj13')
                                    }}></label>
                            <div className="checkBoxText">
                                <Radio name="benefitPlan" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">True</div>
                                </Radio>
                                <Radio name="benefitPlan" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">False</div>
                                </Radio>
                            </div>    
                        </Col>
                        <Col xs={12} md={12}>
                            <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.TRUST_IS_A_PLAN_SECTION_4975E1,'obj14')
                                    }}></label>
                            <div className="checkBoxText">
                                <Radio name="section4975" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">True</div>
                                </Radio>
                                <Radio name="section4975" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">False</div>
                                </Radio>
                            </div>
                        </Col>
                        <Col xs={12} md={12}>
                            <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.TRUST_AN_ENTITY_BENEFIT_PLAN_INVESTOR_PLAN_ASSET_REGULATION,'obj15')
                                    }}></label>
                            <div className="checkBoxText">
                                <Radio name="benefitPlanInvestor" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">True</div>
                                </Radio>
                                <Radio name="benefitPlanInvestor" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">False</div>
                                </Radio>
                            </div>
                        </Col>
                        <Col xs={12} md={12}>
                            <label className="subtext">{this.state.jsonData.INPUT_TOTAL_VALUE_EQUITY_INTEREST_TRUST_HELD_BENEFIT_PLAN_INVESTORS}</label>
                        </Col>
                        <Col xs={12} md={12}>
                            <FormControl type="text" placeholder="Enter percentage" className="inputFormControl inputWidth290 marginForInput" />
                            <span className="error"></span>
                        </Col>

                        {/* <Row className="step1Form-row"> */}
                            <Col xs={12} md={12} className="step1Form-row">
                                <label className="title width100">{this.state.jsonData.PERSON_EXECUTING_THIS_AGREEMENT_ONE_OF_THE_FOLLOWING}:</label>
                                <CBox inline className="cBoxFullAlign"> 
                                    <span className="checkbox-checkmark checkmark"></span>
                                    <div className="checkBoxText">{this.state.jsonData.INDEPENDENT_FIDUCIARY_THAT_HOLDS_OR_MANAGEMENT_OR_CONTROL};</div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BANK_AS_DEFINED_SECTION_202_ADVISERS_ACT,'obj16')
                                    }}></div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText">{this.state.jsonData.INSURANCE_CARRIER_QUALIFIED_UNDER_LAWS_MORE_THAN_ONE_US_STATE}</div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.INVESTMENT_ADVISER_REGISTERED_UNDER_ADVISERS_ACT,'obj17')
                                    }}></div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BROKER_DEALER_REGISTERED_EXCHANGE_ACT,'obj18')
                                    }}></div>
                                </CBox>
                            </Col>
                        {/* </Row> */}
                        {/* <Row className="step1Form-row"> */}
                            <Col xs={12} md={12} className="step1Form-row">
                                <label className="title width100">{this.state.jsonData.PERSON_EXECUTING_THIS_AGREEMENT_ONE_OF_THE_FOLLOWING}:</label>
                                <CBox inline className="cBoxFullAlign">
                                    <span className="checkbox-checkmark checkmark"></span>
                                    <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BANK_AS_DEFINED_SECTION_202_ADVISERS_ACT,'obj19')
                                    }}></div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign"> 
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText">{this.state.jsonData.INSURANCE_CARRIER_QUALIFIED_UNDER_LAWS_MORE_THAN_ONE_US_STATE}</div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign"> 
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.INVESTMENT_ADVISER_REGISTERED_UNDER_ADVISERS_ACT,'obj20')
                                    }}></div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BROKER_DEALER_REGISTERED_EXCHANGE_ACT,'obj21')
                                    }}></div>
                                </CBox>
                            </Col>
                        {/* </Row> */}
                        
                    </Row>
                    {/* revocableTrust investor type block ends */}
                    {/* iRevocableTrust investor type block starts */}
                    <Row className="step1Form-row" hidden={this.state.investorType !== 'iRevocableTrust'}>
                    <div className="col-md-12 col-xs-12 title"></div>
                        <Col xs={12} md={12}>
                            <label className="subtext margin18">{this.state.jsonData.REGARDING_LOOK_THROUGH_ISSUES_CHECK_TRUE_FALSE}:</label>
                        </Col>
                        <Col xs={12} md={12}>
                            <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.TRUST_AN_EMPLOYEE_BENEFIT_PLAN_SECTION_33,'obj22')
                                    }}></label>
                            <div className="checkBoxText">
                                <Radio name="benefitPlan" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">True</div>
                                </Radio>
                                <Radio name="benefitPlan" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">False</div>
                                </Radio>
                            </div>
                        </Col>
                        <Col xs={12} md={12}>
                            <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.TRUST_IS_A_PLAN_SECTION_4975E1_INCLUDING_RETIREMENT_ACCOUNT,'obj23')
                                    }}></label>
                            <div className="checkBoxText">
                                <Radio name="section4975" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">True</div>
                                </Radio>
                                <Radio name="section4975" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">False</div>
                                </Radio>
                            </div>
                        </Col>
                        <Col xs={12} md={12}>
                            <label className="form-label width100" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.TRUST_AN_ENTITY_BENEFIT_PLAN_INVESTOR_PLAN_ASSET_REGULATION_PARTNERSHIP_OTHER_ENTITY,'obj24')
                                    }}></label>
                            <div className="checkBoxText">
                                <Radio name="benefitPlanInvestor" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">True</div>
                                </Radio>
                                <Radio name="benefitPlanInvestor" inline id="yesCheckbox">
                                    <span className="radio-checkmark"></span>
                                    <div className="radioText">False</div>
                                </Radio>
                            </div>
                        </Col>
                        <Col xs={12} md={12}>
                            <label className="form-label width100">{this.state.jsonData.INPUT_TOTAL_VALUE_EQUITY_INTEREST_TRUST_HELD_BENEFIT_PLAN_INVESTORS}</label>
                        </Col>
                        <Col xs={12} md={12}>
                            <FormControl type="text" placeholder="Enter percentage" className="inputFormControl inputWidth290 marginForInput" />
                            <span className="error"></span>
                        </Col>


                        {/* <Row className="step1Form-row"> */}
                            <Col xs={12} md={12} className="step1Form-row">
                                <label className="title width100">{this.state.jsonData.PERSON_EXECUTING_THIS_AGREEMENT_ONE_OF_THE_FOLLOWING}:</label>
                                <CBox inline className="cBoxFullAlign"> 
                                    <span className="checkbox-checkmark checkmark"></span>
                                    <div className="checkBoxText">{this.state.jsonData.INDEPENDENT_FIDUCIARY_THAT_HOLDS_OR_MANAGEMENT_OR_CONTROL};</div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BANK_AS_DEFINED_SECTION_202_ADVISERS_ACT,'obj25')
                                    }}></div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText">{this.state.jsonData.INSURANCE_CARRIER_QUALIFIED_UNDER_LAWS_MORE_THAN_ONE_US_STATE}</div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.INVESTMENT_ADVISER_REGISTERED_UNDER_ADVISERS_ACT,'obj26')
                                    }}></div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BROKER_DEALER_REGISTERED_EXCHANGE_ACT,'obj27')
                                    }}></div>
                                </CBox>
                            </Col>
                        {/* </Row> */}
                        {/* <Row className="step1Form-row"> */}
                            <Col xs={12} md={12} className="step1Form-row">
                                <label className="title width100">{this.state.jsonData.PERSON_EXECUTING_THIS_AGREEMENT_ONE_OF_THE_FOLLOWING}:</label>
                                <CBox inline className="cBoxFullAlign">
                                    <span className="checkbox-checkmark checkmark"></span>
                                    <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BANK_AS_DEFINED_SECTION_202_ADVISERS_ACT,'obj28')
                                    }}></div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign"> 
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText">{this.state.jsonData.INSURANCE_CARRIER_QUALIFIED_UNDER_LAWS_MORE_THAN_ONE_US_STATE}</div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign"> 
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.INVESTMENT_ADVISER_REGISTERED_UNDER_ADVISERS_ACT,'obj29')
                                    }}></div>
                                </CBox>
                                <CBox inline className="cBoxFullAlign">
                                   <span className="checkbox-checkmark checkmark"></span>
                                   <div className="checkBoxText" dangerouslySetInnerHTML={{
                                        __html: this.checkTooltips(this.state.jsonData.BROKER_DEALER_REGISTERED_EXCHANGE_ACT,'obj29')
                                    }}></div>
                                </CBox>
                            </Col>
                        {/* </Row> */}
                        
                    </Row>
                    {/* iRevocableTrust investor type block ends */}
                   
                </div>
                <div className="margin30 error">{this.state.erisaErrorMsz}</div>
                <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.getInvestorObj.subscriptionStatus && this.state.getInvestorObj.subscriptionStatus.name === 'Closed'}>
                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className={"fa fa-chevron-right " + (!this.state.erisaPageValid ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default ERISAComponent;


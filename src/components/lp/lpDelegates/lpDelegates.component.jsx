import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import { Button, Checkbox as CBox, Row, Col, FormControl,Tooltip,OverlayTrigger } from 'react-bootstrap';
import userDefaultImage from '../../../images/default_user.png';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetUtil } from '../../../util/util';
import {Constants} from '../../../constants/constants';
import Loader from '../../../widgets/loader/loader.component';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';

var addLpDelegate={}, removeDelegate={};
class LpDelegatesComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.Constants = new Constants();
        this.handleLpShow = this.handleLpShow.bind(this);
        this.deleteLpDelegate = this.deleteLpDelegate.bind(this);
        this.state = {
            firmId:null,
            subId: null,
            lpsubscriptionObj: {},
            lpDelegatesList:[],
            jsonData:{}
        }
        

        //Add LP Delegate to the list when we add from modal
        addLpDelegate = PubSub.subscribe('lpDelegate', (msg, data) => {
            let delegatesList = this.state.lpDelegatesList;
            delegatesList.unshift(data);
            this.setState({
                lpDelegatesList: delegatesList
            })
        });


        //Remove LP Delegate to the list when we add from modal
        removeDelegate = PubSub.subscribe('removeLpDelegate', (msg, data) => {
            this.lpDelegates(this.state.lpsubscriptionObj.fund.id,this.state.lpsubscriptionObj.id);
        });
    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(addLpDelegate);
        PubSub.unsubscribe(removeDelegate);
    }


     //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('showProfileModal', data);
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    componentDidMount() { 
        this.getJsonData();
        window.scrollTo(0, 0);
        var url = window.location.href;
        var parts = url.split("/");
        var urlSplitSubId = parts[parts.length - 1];
        let subId = urlSplitSubId;
        this.setState({
            subId: subId
        },()=>{
            this.getSubscriptionDetails(this.state.subId);
        })
    }


    //Get the fund details
    getSubscriptionDetails(id) {
        
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    this.lpDelegates(result.data.data.fund.id,result.data.data.id);
                    this.setState({
                        lpsubscriptionObj: result.data.data
                    },()=>{
                        this.FsnetUtil.setLeftNavHeight();
                    })
                    PubSub.publish('getLpSubscription', result.data.data);
                    //Call capital commitment change fn if it comes form alert
                }
            })
            .catch(error => {
                this.close();
            });
        }
    }

    getJsonData() {
        this.Fsnethttp.getJson('assignDelegates').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    //Get LP Delegates
    lpDelegates(fundId,subId) {
        
        if (fundId) {
            this.open();
            this.Fsnethttp.getLpDelegates(fundId,subId).then(result => {
                this.close();
                if (result.data) {
                    this.setState({
                        lpDelegatesList: result.data.data
                    })
                }
            })
            .catch(error => {
                this.close();
                this.setState({
                    lpDelegatesList: []
                })
            });
        }
    }

    //Open the modal
    handleLpShow() {
        // if(this.state.status !== 'Closed') {
            PubSub.publish('openModal',{obj:this.state.lpsubscriptionObj,type:'lpDelegate'});
        // }
    }

    //Delete LP Delegate
    deleteLpDelegate(e, id) {
        // if(this.state.status !== 'Closed') {
            PubSub.publish('openLpDelegateModal', { data: this.state.lpsubscriptionObj, delegateId: id });
        // }
    }

    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
              <OverlayTrigger
                overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                placement="left"
                delayShow={300}
                delayHide={150}
                rootClose
              >
                <a href={href}>{children}</a>
              </OverlayTrigger>
            );
          }
        return (
            <div className="step2Class marginTop6">
            <div className="GpDelegatesContainer" >
                <h1 className="title">{this.state.jsonData.ASSIGN_DELEGATES}</h1>
                <p className="subtext marginBottom44">{this.state.jsonData.LP_DELEGATES_ARE_PARTIES}</p>
                <Button className="fsnetButton" onClick={this.handleLpShow}><i className="fa fa-plus"></i> {this.state.jsonData.DELEGATE}</Button>
                <div className="gpDelegateTableHeader" hidden={this.state.lpDelegatesList.length ===0}>
                    <div className="gpDelegateHeadaerTitle">Delegate Name</div>
                    {/* <div className="gpDelegateHeadaerTitle"><div>{this.state.jsonData.ENABLE_FOR_THIS_FUND}
                        &nbsp;
                        <span>
                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_ENABLE_FOR_THIS_FUND}>
                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                            </LinkWithTooltip>
                        </span> 
                    </div></div>
                    <div className="gpDelegateHeadaerTitle"><div>{this.state.jsonData.DELEGATE_REQUIRED_CONSENT_HOLD_CLOSING}
                        &nbsp;
                        <span>
                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_DELEGATE_REQUIRED_CONSENT_HOLD_CLOSING}>
                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                            </LinkWithTooltip>
                        </span>      
                    </div></div> */}
                </div>
                <div className={"userContainer " + (this.state.lpDelegatesList.length ===0 ? 'borderNone' : '')} >
                    {this.state.lpDelegatesList.length >0 ?
                        this.state.lpDelegatesList.map((record, index)=>{
                            return(
                                <div className="userRow" key={index}>
                                    <label className="userImageAlt">
                                    {
                                        record['profilePic']  ?
                                        <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                        : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                    }
                                    </label>
                                    <div className="fund-user-name">{this.FsnetUtil.getFullName(record)}</div>
                                    <div className="lpDelegateCheckbox paddingLeft30" onClick={(e) => this.deleteLpDelegate(e, record['id'])}>
                                        <i className="fa fa-trash cursor-pointer"></i>
                                    </div>
                                </div>
                            );
                        })
                        :
                        <div className="title margin20 text-center">{this.state.noDelegatesMsz}</div>
                    } 
                </div>
                <div className="error">{this.state.gpDelegateScreenError}</div>
                {/* <div className="footer-nav"> 
                    <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.proceedToNext}>{this.state.jsonData.SAVE_CHANGES}</Button>
                </div> */}
                <Loader isShow={this.state.showModal}></Loader>
            </div>
            </div>
        );
    }
}

export default LpDelegatesComponent;




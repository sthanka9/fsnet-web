import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Radio, Row, Col, FormControl,Button, Checkbox as CBox, OverlayTrigger, Tooltip } from 'react-bootstrap';
import userDefaultImage from '../../../images/default_user.png';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';
var _ = require('lodash');

var addLpSignatory,removeDelegate = {}
class lpSignatoryComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            showModal: false,
            investorType: 'LLC',
            jsonData:{},
            pageValid:false,
            lpSignatoriesList:[],
            lpSignatoriesListTemp:[],
            lpSignatoriesSelectedList:[],
            noSigantoriesMsz: this.Constants.NO_LP_SIGNATORIES,
            isFormChanged:false
        }

        //Add LP Signatory to the list when we add from modal
        addLpSignatory = PubSub.subscribe('lpSignatory', (msg, data) => {
            let list = this.state.lpSignatoriesList;
            list.unshift(data);
            this.setState({
                lpSignatoriesList: list
            },()=>{
                this.selectedMembersPushToList()
            })
        });

        //Remove LP Signatory to the list when we add from modal
        removeDelegate = PubSub.subscribe('removeLpSignatory', (msg, data) => {
            this.removeLpSignatory(data.signatoryId);
        });

    }


    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }


    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.getJsonData();
        this.getSubscriptionDetails(id);
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('capitalCommitment').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getSubscriptionDetails = (id) => {
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    let obj = result.data.data;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    if(obj.investorQuestions && obj.investorQuestions.length > 0) {
                        obj['currentPageCount'] = result.data.data.investorType == 'Trust' ? 9 : (result.data.data.investorType == 'LLC' ? 10 : 6);
                    } else {
                        obj['currentPageCount'] = result.data.data.investorType == 'Trust' ? 8 : (result.data.data.investorType == 'LLC' ? 9 : 5);
                    }
                    obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                    PubSub.publish('investorData', obj);
                    this.setState({
                        getInvestorObj: result.data.data,
                        investorType: result.data.data.investorType?result.data.data.investorType:'LLC',
                        investorSubType: result.data.data.investorSubType ? result.data.data.investorSubType : (result.data.data.investorType == 'Trust' ? 9 : 0),
                        lpSignatoriesList:result.data.data.lpSignatories,
                        lpSignatoriesListTemp:_.filter(result.data.data.lpSignatories, function(obj) {return obj.selected})
                    },()=>{
                        this.updateLpSignatories(result.data.data)
                    })
                }
            })
            .catch(error => {
                this.close();
            });
        }
    }

    updateLpSignatories = (data) => {
        if(data && data.lpSignatories.length >0) {
            this.setState({ lpSignatoriesList: data.lpSignatories}, () => this.selectedMembersPushToList());
        } else {
            this.setState({
                lpSignatoriesList: [],
                noSigantoriesMsz: this.Constants.NO_LP_SIGNATORIES
            })
        }
    }

    selectedMembersPushToList = () => {
        if (this.state.lpSignatoriesList.length > 0) {
            let list = [];
            for (let index of this.state.lpSignatoriesList) {
                if (index['selected'] === true) {
                    list.push(index['id'])
                }
                this.setState({
                    lpSignatoriesSelectedList: list,
                })
            }
        }
    }

    removeLpSignatory = (id) => {
        let newList = this.state.lpSignatoriesList.filter(data => data.id !== id)
        let selectedId = this.state.lpSignatoriesSelectedList.indexOf(id);
        if(selectedId > -1) {
            this.state.lpSignatoriesSelectedList.splice(selectedId,1)
        }
        this.setState({
            lpSignatoriesList:newList,
        })
    }

    proceedToNext = () => {
        const temp = [...this.state.lpSignatoriesListTemp]
        const finalList = [...this.state.lpSignatoriesSelectedList]
        const finalSelectedList = _.sortBy(finalList);
        const originalList = _.sortBy(_.filter(temp.map(obj=> {if(obj.selected == true) {return obj.id}})))
        const lpSignatoryChangeEvent = _.isEqual(finalSelectedList,originalList)
        let postobj = { subscriptionId: this.state.getInvestorObj.id, fundId:this.state.getInvestorObj.fund.id,lpSignatories:this.state.lpSignatoriesSelectedList, isFormChanged:this.state.isFormChanged, lpSignatoryChangeEvent:!lpSignatoryChangeEvent }
        this.open();
        this.Fsnethttp.assignLpSignatoryToFund(postobj).then(result => {
            this.close();
            if (result.data) {
                this.props.history.push('/lp/review/' + this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
            }
        })
        .catch(error => {
            this.close();
            if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                this.setState({
                    errorMsz: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    errorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                });
            }
        });

    }

    proceedToBack = () => {
        this.props.history.push('/lp/capitalCommitment/'+this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
    }

    handleLpSignatoryShow = () => {
        PubSub.publish('openModal',{obj:this.state.getInvestorObj,type:'lpSignatory'});
    }

    deleteLpSignatory = (e,id) => {
        PubSub.publish('openLpSignatoryModal', { data: this.state.getInvestorObj, signatoryId: id });
    }

    handleInputChangeEvent = (event,obj) => {
        let getSelectedList = this.state.lpSignatoriesSelectedList;
        let selectedId = obj.id;
        let value = event.target.checked
        if (value) {
            if (getSelectedList.indexOf(selectedId) === -1) {
                    getSelectedList.push(selectedId);
            }
            this.updateSelectedValueToList(obj.id, value);
        } else {
            var index = getSelectedList.indexOf(selectedId);
            if (index !== -1) {
                getSelectedList.splice(index, 1);
            }
            this.updateSelectedValueToList(obj.id, value)
        }
        this.setState({
            lpSignatoriesSelectedList: getSelectedList,
            isFormChanged:true
        })
    }

    updateSelectedValueToList = (id,value) => {
        if (this.state.lpSignatoriesList.length > 0) {
            let getList = this.state.lpSignatoriesList;
            for (let index of getList) {
                if (index['id'] === id && this.state.lpSignatoriesList.indexOf(index['id']) === -1) {
                    index['selected'] = value
                }
                this.setState({
                    lpSignatoriesList: getList
                })
            }
        }
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('showProfileModal', data);
    }

    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                    trigger={['click', 'hover', 'focus']}
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement="left"
                    delayShow={300}
                    delayHide={150}
                >
                    <span>{children}</span>
                </OverlayTrigger>
            );
        }
        return (
            <div className="width100 marginTopSticky">
                <div className="formGridDivMargins min-height-400" id="createFund">
                    <div className="title">{this.state.jsonData.LP_SIGNATORY}</div>
                    <p className="subtext marginBottom20 marginTop10">{this.state.jsonData.LP_SIGNATORIES_CONTENT}</p>
                    <div className="marginTop20">
                        <Button type="button" className="fsnetSubmitButton btnEnabled adminButton marginRight20" onClick={this.handleLpSignatoryShow}>Create Secondary Signatory</Button>
                    </div>
                    <Row className="full-width marginTop30" hidden={this.state.lpSignatoriesList.length === 0 }>
                        <div className="name-heading marginLeft75 width295">
                                Name
                        </div>
                        <div className="name-heading width150">
                            Select/Deselect
                        </div>
                        <div className="name-heading">
                            Remove
                        </div>
                    </Row>

                    <div className={"tableContainer marginTop10 width600 " + (this.state.lpSignatoriesList.length ===0 ? 'borderNone' : '')} >
                        {this.state.lpSignatoriesList.length >0 ?
                            this.state.lpSignatoriesList.map((record, index)=>{
                                return(
                                    <div className="userRow" key={index}>
                                        <label className="userImageAlt">
                                        {
                                            record['profilePic']  ?
                                            <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                            : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                        }
                                        </label>
                                        <div className="lp-name width320">{this.FsnetUtil.getFullName(record)}</div>
                                        <div className="width100Px inline-block">
                                            <CBox className="marginLeft10" key={record['selected']} checked={record['selected']} onChange={(e) => this.handleInputChangeEvent(e, record)}>
                                                <span className="checkmark"></span>
                                            </CBox>
                                        </div>
                                        <div className="marginLeft30 inline-block">
                                            <i className="lp-trash-icon fa fa-trash cursor" aria-hidden="true" onClick={(e) => this.deleteLpSignatory(e, record['id'])}></i>
                                        </div>
                                    </div>
                                );
                            })
                            :
                            <div className="title margin20 text-center">{this.state.noSigantoriesMsz}</div>
                        } 
                    </div>
                    <div className="margin30 error">{this.state.errorMsz}</div>
                </div>
                <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.getInvestorObj && this.state.getInvestorObj.subscriptionStatus && this.state.getInvestorObj.subscriptionStatus.name === 'Closed'}>
                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className="fa fa-chevron-right" onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default lpSignatoryComponent;


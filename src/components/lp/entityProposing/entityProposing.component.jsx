import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Radio, Row, Col, FormControl } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../../util/util';

class entityProposingComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.equityProposingChangeEvent = this.equityProposingChangeEvent.bind(this);
        this.proceedToNext = this.proceedToNext.bind(this);
        this.proceedToBack = this.proceedToBack.bind(this);
        this.state = {
            showModal: false,
            investorType: 'LLC',
            equityProposingPageValid:false,
            getInvestorObj:{},
            entityProposingAcquiringInvestment: true,
            entityHasMadeInvestmentsPriorToThedate: true,
            partnershipWillNotConstituteMoreThanFortyPercent: true,
            beneficialInvestmentMadeByTheEntity: true,
            entityProposingErrorMsz:'',
            jsonData:{},
            isFormChanged:false,
            // isAnyFiledFalse:false,
        }

    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.getJsonData();
        this.getSubscriptionDetails(id);
    }

    getJsonData() {
        this.Fsnethttp.getJson('lookThroughIssues').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getSubscriptionDetails(id) {
        
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    let obj = result.data.data;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    obj['currentPageCount'] = 5;
                    obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                    PubSub.publish('investorData',obj );
                    this.setState({
                        getInvestorObj: result.data.data,
                        investorType: result.data.data.investorType?result.data.data.investorType:'LLC',
                        entityProposingAcquiringInvestment: result.data.data.entityProposingAcquiringInvestment != null ? result.data.data.entityProposingAcquiringInvestment :true,
                        entityHasMadeInvestmentsPriorToThedate: result.data.data.entityHasMadeInvestmentsPriorToThedate != null ? result.data.data.entityHasMadeInvestmentsPriorToThedate : true,
                        partnershipWillNotConstituteMoreThanFortyPercent: result.data.data.partnershipWillNotConstituteMoreThanFortyPercent != null  ? result.data.data.partnershipWillNotConstituteMoreThanFortyPercent: true,
                        beneficialInvestmentMadeByTheEntity: result.data.data.beneficialInvestmentMadeByTheEntity != null ? result.data.data.beneficialInvestmentMadeByTheEntity: true,
                    },()=>{
                        this.updateInvestorInputFields(this.state.getInvestorObj);
                    })
                }
            })
            .catch(error => {
                this.close();
            });
        }
    }

    

    updateInvestorInputFields(data) {
        if(this.state.entityProposingAcquiringInvestment !== null  && this.state.entityHasMadeInvestmentsPriorToThedate !== null && this.state.partnershipWillNotConstituteMoreThanFortyPercent !== null && this.state.beneficialInvestmentMadeByTheEntity !== null) {
            this.setState({
                equityProposingPageValid: true
            })
        }
    }

    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    equityProposingChangeEvent(event, type, radioTypeName, blur) {
        let key = type;
        let dataObj = {};
        switch(type) {
            case 'radio':
                this.setState({
                    [blur]: radioTypeName,
                    isFormChanged:true
                })
                let name = blur+'Valid'
                dataObj ={
                    [name] :true
                };
                this.updateStateParams(dataObj);
                break;
           
            default:
                break;
        }
    }

    formEventChanges = (e, type) => {
        let name = e.target.name;
        let value = e.target.value;
        switch (type) {
            case 'number':
                const re = this.Constants.NUMBER_HUNDRED_REGEX;
                if (re.test(value.trim())) {
                    this.setState({
                        [name]: value ? parseInt(value) : '',
                        [name + 'Valid']: true,
                        [name + 'Touched']: true
                    },()=>{
                        this.enableDisableInvestorDetailsButton();
                    })
                    return true;
                } else {
                    if (parseInt(value) < 0) {
                        this.setState({
                            [name + 'Valid']: false,
                            [name + 'Touched']: true
                        },()=>{
                            this.enableDisableInvestorDetailsButton();
                        })
                        return true;
                    } else if(isNaN(parseInt(value))) {
                        this.setState({
                            [name]: '',
                            [name + 'Valid']: false,
                            [name + 'Touched']: true
                        },()=>{
                            this.enableDisableInvestorDetailsButton();
                        })
                        return true;
                    }
                }
                break;
                
            default:
                break;    
        }
    }

    valueTouched = (e, type) => {
        const name = e.target.name;
        this.setState({
            [name + 'Touched']: true
        })
    }

    // Update state params values and login button visibility

    updateStateParams(updatedDataObject){
        this.setState(updatedDataObject, ()=>{
            this.enableDisableInvestorDetailsButton();
        });
    }

    // Enable / Disble functionality of Investor Details next Button
    enableDisableInvestorDetailsButton(){
        let status;
        status = (this.state.entityProposingAcquiringInvestment !==null && this.state.entityHasMadeInvestmentsPriorToThedate !==null && this.state.partnershipWillNotConstituteMoreThanFortyPercent !==null && this.state.beneficialInvestmentMadeByTheEntity !==null) ? true : false;
        this.setState({
            equityProposingPageValid : status,
        });
    }


    proceedToNext() {
        let postobj = {investorType:this.state.investorType,subscriptionId:this.state.getInvestorObj.id, step:6,entityProposingAcquiringInvestment:this.state.entityProposingAcquiringInvestment, entityHasMadeInvestmentsPriorToThedate:this.state.entityHasMadeInvestmentsPriorToThedate, partnershipWillNotConstituteMoreThanFortyPercent:this.state.partnershipWillNotConstituteMoreThanFortyPercent, beneficialInvestmentMadeByTheEntity:this.state.beneficialInvestmentMadeByTheEntity,isFormChanged:this.state.isFormChanged}
        this.open();
        
        this.Fsnethttp.updateLpSubscriptionDetails(postobj).then(result => {
            this.close();
            if (result.data) {
                this.props.history.push('/lp/erisa/'+this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
            }
        })
        .catch(error => {
            this.close();
            if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                this.setState({
                    entityProposingErrorMsz: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    entityProposingErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                });
            }
        });
        
    }

    proceedToBack () {
        this.props.history.push('/lp/companiesAct/'+this.state.getInvestorObj.id);
    }

    render() {
        return (
            <div className="accreditedInvestor width100 marginTopSticky">
                <div className="formGridDivMargins min-height-400">
                    {/* llc investor type block starts */}
                    <div className="title">{this.state.jsonData.LOOK_THROUGH_ISSUES}</div>
                    <Row className="step1Form-row" hidden={this.state.investorType == 'Individual'}>
                        <Col xs={12} md={12}>
                            <label className="form-label width100">{this.state.jsonData.CHECK_TRUE_FALSE_STATEMENT}</label>
                        </Col>
                        <Col xs={12} md={12} className="marginTop15">
                            <label className="form-label width100">{this.state.jsonData.ENTITY_NOT_ORGANISED_ACQUIRING_INTEREST}</label>
                            <Radio name="entityProposingAcquiringInvestment" inline checked={this.state.entityProposingAcquiringInvestment === true} onChange={(e) => this.equityProposingChangeEvent(e, 'radio', true, 'entityProposingAcquiringInvestment')}>&nbsp; True
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="entityProposingAcquiringInvestment" inline checked={this.state.entityProposingAcquiringInvestment === false} onChange={(e) => this.equityProposingChangeEvent(e, 'radio', false, 'entityProposingAcquiringInvestment')}>&nbsp; False
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>
                        <Col xs={12} md={12} className="marginTop15">
                            <label className="form-label width100">{this.state.jsonData.ENTITY_MADE_INVESTMENT_PRIOR_DATE}</label>
                            <Radio name="entityHasMadeInvestmentsPriorToThedate" inline checked={this.state.entityHasMadeInvestmentsPriorToThedate === true} onChange={(e) => this.equityProposingChangeEvent(e, 'radio',true, 'entityHasMadeInvestmentsPriorToThedate')}>&nbsp; True
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="entityHasMadeInvestmentsPriorToThedate" inline checked={this.state.entityHasMadeInvestmentsPriorToThedate === false} onChange={(e) => this.equityProposingChangeEvent(e, 'radio',false, 'entityHasMadeInvestmentsPriorToThedate')}>&nbsp; False
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>
                        <Col xs={12} md={12} className="marginTop15">
                            <label className="form-label width100">{this.state.jsonData.ENTITY_INVESTMENT_PARTNERSHIP_PERCENT}</label>
                            <Radio name="partnershipWillNotConstituteMoreThanFortyPercent" inline checked={this.state.partnershipWillNotConstituteMoreThanFortyPercent === true} onChange={(e) => this.equityProposingChangeEvent(e, 'radio',true, 'partnershipWillNotConstituteMoreThanFortyPercent')}>&nbsp; True
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="partnershipWillNotConstituteMoreThanFortyPercent" inline checked={this.state.partnershipWillNotConstituteMoreThanFortyPercent === false} onChange={(e) => this.equityProposingChangeEvent(e, 'radio',false, 'partnershipWillNotConstituteMoreThanFortyPercent')}>&nbsp; False
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>
                        <Col xs={12} md={12} className="marginTop15">
                            <label className="form-label width100">{this.state.jsonData.ENTITY_GOVERNING_DOCUMENTS} </label>
                            <Radio name="beneficialInvestmentMadeByTheEntity" inline checked={this.state.beneficialInvestmentMadeByTheEntity === true} onChange={(e) => this.equityProposingChangeEvent(e, 'radio',true, 'beneficialInvestmentMadeByTheEntity')}>&nbsp; True
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="beneficialInvestmentMadeByTheEntity" inline checked={this.state.beneficialInvestmentMadeByTheEntity === false} onChange={(e) => this.equityProposingChangeEvent(e, 'radio',false, 'beneficialInvestmentMadeByTheEntity')}>&nbsp; False
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>

                    </Row>
                    {/* llc investor type block ends */}
                   
                </div>
                <div className="margin30 error">{this.state.entityProposingErrorMsz}</div>
                <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.getInvestorObj.subscriptionStatus && this.state.getInvestorObj.subscriptionStatus.name === 'Closed'}>
                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className={"fa fa-chevron-right " + (!this.state.equityProposingPageValid ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default entityProposingComponent;


import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Radio, Row, Col, FormControl, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';

var tooltipsCount=0,objList=[], objListArray=[];
class AccreditedInvestorComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.accreditedInvestorChangeEvent = this.accreditedInvestorChangeEvent.bind(this);
        this.openTooltip = this.openTooltip.bind(this);
        this.closeTooltipModal = this.closeTooltipModal.bind(this);
        this.closeSecurityTooltipModal = this.closeSecurityTooltipModal.bind(this);
        this.openSecurityTooltipModal = this.openSecurityTooltipModal.bind(this);
        this.proceedToNext = this.proceedToNext.bind(this);
        this.proceedToBack = this.proceedToBack.bind(this);
        this.state = {
            showModal: false,
            investorType: 'LLC',
            investorSubType: 0,
            areYouAccreditedInvestor: null,
            accreditedInvestorPageValid: false,
            getInvestorObj: {},
            showTooltipModal: false,
            accreditedInvestorErrorMsz:'',
            showSecurityTooltipModal:false,
            jsonData:{},
            isFormChanged:false
        }

        

    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.getJsonData();
        //To fix tooltip issue timeout has been added.
        //To DO Find fix for this.
        setTimeout(() => {
            this.getSubscriptionDetails(id);
        }, 200);
    }

    checkTooltips(text,type) {
        let tooltipWords = ['Accredited Investor', 'Securities Act'];
        let count = 0 ;
        for(let idx=0; idx<tooltipWords.length; idx++) {
            if(text && text.indexOf(tooltipWords[idx]) > -1 && objList.indexOf(type) === -1) {
                let arrayList = this.FsnetUtil.allIndexOf(text,tooltipWords[idx])
                let splitText = [];
                let currentIndex = 0;
                for(let jdx=0; jdx<arrayList.length; jdx++) {
                    let jdxLength = arrayList[jdx]+tooltipWords[idx].length;
                    if(jdx === (arrayList.length-1)) {
                        splitText.push(text.substr(currentIndex,text.length))
                    } else {
                        splitText.push(text.substr(currentIndex,jdxLength))
                        currentIndex = jdxLength
                    }
                }
                let newText = ''
                if(splitText.length === 1) {
                    count++;
                    let indexPos = tooltipsCount+count
                    text = text.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                }else {
                    for(let pos of splitText) {
                        count++;
                        let indexPos = tooltipsCount+count
                        newText = newText+pos.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                    }
                    text = newText;
                }

            }
            if(text && idx === tooltipWords.length-1 && objList.indexOf(type) === -1) {
                objListArray.push({type:type,text:text})
                objList.push(type)
            }
        }
        tooltipsCount = tooltipsCount+count;
        for(let index of objListArray) {
            if(index['type'] && index['text']) {
                if(index['type'] === type) {
                    return index['text'];
                }
            }
        }
        return text
    }

    getJsonData() {
        this.Fsnethttp.getJson('accreditedInvestor').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getSubscriptionDetails(id) {
        
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    let obj = result.data.data;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    obj['currentPageCount'] = 2;
                    obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                    PubSub.publish('investorData', obj);
                    this.setState({
                        getInvestorObj: result.data.data,
                        investorType: result.data.data.investorType?result.data.data.investorType:'LLC',
                        investorSubType: result.data.data.investorSubType ? result.data.data.investorSubType : (result.data.data.investorType == 'Trust' ? 9 : 0),
                        areYouAccreditedInvestor: result.data.data.areYouAccreditedInvestor
                    }, () => {
                        this.updateInvestorInputFields(this.state.getInvestorObj)
                        this.addClickEvent(result.data.data)
                    })
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    updateInvestorInputFields(data) {
        if (this.state.areYouAccreditedInvestor !== null && this.state.areYouAccreditedInvestor !== undefined) {
            this.setState({
                accreditedInvestorPageValid: true
            })
        }
    }



    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    // //Show Tooltip Modal
    openTooltip() {
        PubSub.publish('openModal', {investorType: this.state.investorType, investorSubType: this.state.investorSubType, modalType: 'accredited', type: 'accreditedModal'});
        // this.setState({ showTooltipModal: true });
    }

    //Close Tooltip Modal
    closeTooltipModal() {
        this.setState({ showTooltipModal: false });
    }

    // //Show Tooltip Modal
    openSecurityTooltipModal() {
        PubSub.publish('openModal', {investorType: this.state.investorType, investorSubType: this.state.investorSubType, modalType: 'actModalWindow', type: 'security'});
        // this.setState({ showSecurityTooltipModal: true });
    }

    //Close Tooltip Modal
    closeSecurityTooltipModal() {
        this.setState({ showSecurityTooltipModal: false });
    }

    accreditedInvestorChangeEvent(event, type, value) {
        this.setState({
            [type]: value,
            accreditedInvestorPageValid: true,
            isFormChanged:true
        })
    }

    proceedToNext() {
        let postobj = { investorType: this.state.investorType, investorSubType: this.state.investorSubType, subscriptionId: this.state.getInvestorObj.id, step: 3, areYouAccreditedInvestor: this.state.areYouAccreditedInvestor,isFormChanged:this.state.isFormChanged }
        this.open();
        
        this.Fsnethttp.updateLpSubscriptionDetails(postobj).then(result => {
            this.close();
            if (result.data) {
                this.props.history.push('/lp/qualifiedPurchaser/' + this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
            }
        })
        .catch(error => {
            this.close();
            if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                this.setState({
                    accreditedInvestorErrorMsz: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    accreditedInvestorErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                });
            }
        });

    }

    proceedToBack() {
        this.props.history.push('/lp/investorInfo/' + this.FsnetUtil._encrypt(this.state.getInvestorObj.id));
    }

    addClickEvent(obj) {
        if(tooltipsCount > 0) {
            for(let idx=0; idx<tooltipsCount; idx++) {
                if(document.getElementById("anchor"+(idx+1))) {
                    document.getElementById("anchor"+(idx+1)).addEventListener("click", function(){
                        let name = document.getElementById("anchor"+(idx+1)).getAttribute('tooltip_id');
                        let modalType, type;
                        if(name === 'Accredited Investor') {
                            modalType = 'accredited';
                            type = 'accreditedModal'
                        } else if(name === 'Securities Act') {
                            modalType = 'actModalWindow';
                            type = 'security'
                        }
                        PubSub.publish('openModal', {investorType: obj.investorType, investorSubType: obj.investorSubType, modalType: modalType, type: type});
                    });
                }
            }
        }
    }

    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                    trigger={['click', 'hover', 'focus']}
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement="left"
                    delayShow={300}
                    delayHide={150}
                    rootClose
                >
                    <span>{children}</span>
                </OverlayTrigger>
            );
        }
        return (
            <div className="accreditedInvestor width100 marginTopSticky">
                <div className="formGridDivMargins min-height-400">
                    {/* individual investor type block starts */}
                    <div className="title">{this.state.jsonData.ACCREDITED_INVESTOR}</div>
                    <Row className="step1Form-row" hidden={this.state.investorType !== 'Individual'}>
                        <Col xs={12} md={12}>
                            {/* <label className="form-label width100">{this.state.jsonData.ARE_YOU_AN} <span className="helpWord" onClick={this.openTooltip}>{this.state.jsonData.ACCREDITED_INVESTOR}</span> {this.state.jsonData.MEANING_RULE_501} <span className="helpWord" onClick={this.openSecurityTooltipModal}>{this.state.jsonData.SECURITIES_ACT}</span>{this.state.jsonData.QUESITION_MARK}
                            </label> */}
                            <label className="form-label width100" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.ARE_YOU_AN_ACCREDITED_INVESTOR,'obj1')
                            }}>
                            </label>
                            {/* <label className="form-label width100"  dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips('1Are you an Accredited Investor within the meaning of Rule 501 under the Securities Act?','obj1')
                                                      }}></label>
                            <label className="form-label width100"  dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips('2Are you an Accredited Investor within the meaning of Rule 501 under the Securities Act?','obj2')
                                                      }}></label> */}
                            <Radio name="individualAccredited" inline checked={this.state.areYouAccreditedInvestor === true} onChange={(e) => this.accreditedInvestorChangeEvent(e, 'areYouAccreditedInvestor', true)}>&nbsp; Yes
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="individualAccredited" inline checked={this.state.areYouAccreditedInvestor === false} onChange={(e) => this.accreditedInvestorChangeEvent(e, 'areYouAccreditedInvestor', false)}>&nbsp; No
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>
                    </Row>
                    {/* individual investor type block ends */}
                    {/* llc investor type block starts */}
                    <Row className="step1Form-row" hidden={this.state.investorType !== 'LLC'}>
                        <Col xs={12} md={12}>
                            {/* <label className="form-label width100">{this.state.jsonData.IS_ENTITY} <span className="helpWord" onClick={this.openTooltip}>{this.state.jsonData.ACCREDITED_INVESTOR}</span> {this.state.jsonData.MEANING_RULE_501} <span className="helpWord" onClick={this.openSecurityTooltipModal}>{this.state.jsonData.SECURITIES_ACT}</span>{this.state.jsonData.QUESITION_MARK}</label> */}
                            <label className="form-label width100" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.IS_ENTITY_AN_ACCREDITED_INVESTOR,'obj2')
                            }}>
                            </label>
                            <Radio name="llcAccredited" inline checked={this.state.areYouAccreditedInvestor === true} onChange={(e) => this.accreditedInvestorChangeEvent(e, 'areYouAccreditedInvestor', true)}>&nbsp; Yes
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="llcAccredited" inline checked={this.state.areYouAccreditedInvestor === false} onChange={(e) => this.accreditedInvestorChangeEvent(e, 'areYouAccreditedInvestor', false)}>&nbsp; No
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>
                    </Row>
                    {/* llc investor type block ends */}
                    
                      
                    <Row className="step1Form-row" hidden={this.state.investorType !== 'Trust' }>
                        <Col xs={12} md={12}>
                            {/* <label className="form-label width100">{this.state.jsonData.IS_TRUST} <span className="helpWord" onClick={this.openTooltip}>{this.state.jsonData.ACCREDITED_INVESTOR}</span> {this.state.jsonData.MEANING_RULE_501} <span className="helpWord" onClick={this.openSecurityTooltipModal}>{this.state.jsonData.SECURITIES_ACT}</span>{this.state.jsonData.QUESITION_MARK}</label> */}
                            <label className="form-label width100"  dangerouslySetInnerHTML={{
                                                __html: this.checkTooltips(this.state.jsonData.IS_TRUST_AN_ACCREDITED_INVESTOR,'obj3')
                                }}>
                            </label>
                            <Radio name="trustAccredited" inline checked={this.state.areYouAccreditedInvestor === true} onChange={(e) => this.accreditedInvestorChangeEvent(e, 'areYouAccreditedInvestor', true)}>&nbsp; Yes
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="trustAccredited" inline checked={this.state.areYouAccreditedInvestor === false} onChange={(e) => this.accreditedInvestorChangeEvent(e, 'areYouAccreditedInvestor', false)}>&nbsp; No
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Col>
                    </Row>
                 
                    <div className="vanilla-note" hidden={this.state.showModal || this.state.areYouAccreditedInvestor !== false}>
                        {this.state.jsonData.SUBSCRIBE_FUND_AFFIRMATIVE}
                    </div>
                </div>
                <div className="margin30 error">{this.state.accreditedInvestorErrorMsz}</div>
                <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.getInvestorObj.subscriptionStatus && this.state.getInvestorObj.subscriptionStatus.name === 'Closed'}>
                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className={"fa fa-chevron-right " + (!this.state.accreditedInvestorPageValid ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default AccreditedInvestorComponent;


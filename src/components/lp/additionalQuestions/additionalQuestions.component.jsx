import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Radio, Row, Col, FormControl, Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';

class additionalQuestionsComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.additionalQuestionsHandleEvent = this.additionalQuestionsHandleEvent.bind(this);
        this.proceedToNext = this.proceedToNext.bind(this);
        this.proceedToBack = this.proceedToBack.bind(this);
        this.state = {
            showModal: false,
            investorType: 'LLC',
            investorSubType: 0,
            lpCapitalCommitment: '',
            showTooltipModal: false,
            showSecurityTooltipModal:false,
            jsonData:{},
            addedQuestionList:[],
            addedQuestionsPageValid:true,
            isFormChanged:false
        }

    }


    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }


    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.getJsonData();
        this.getSubscriptionDetails(id);
    }

    getJsonData() {
        this.Fsnethttp.getJson('capitalCommitment').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    additionalQuestionsHandleEvent(e,data,type, radioValue) {
        let actualList = this.state.addedQuestionList;
        //Get list of questions and filter by selected id
        let questionsList = this.state.addedQuestionList.filter(record => record.id == data.id);
        questionsList[0]['isTouched'] = true
        if(type === 1) {
            const value = e.target.value;
            if((value.trim() === '' || value === undefined) && data['isRequired']) {
                questionsList[0]['typeOfQuestionMsz'] = this.Constants.REQUIRED_FIELD;
                this.setState({
                    addedQuestionsPageValid:false
                })

            } else {
                questionsList[0]['typeOfQuestionMsz'] = '';
            }
            //Update the value 
            questionsList[0]['questionResponse'] = value.trim() != '' ? value : value.trim();
        }
        else {
            //Update the value 
            questionsList[0]['questionResponse'] = radioValue;
        }
        for(let index of actualList) {
            if(index.id === data.id) {
                index = questionsList[0];
            }
        }
        this.setState({
            addedQuestionList:actualList,
            isFormChanged:true
        },()=>{
            this.addedQuestionsPageRightArrowEnable();
        })                
    }

    getSubscriptionDetails(id) {
        
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    let obj = result.data.data;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    obj['currentPageCount'] = result.data.data.investorType == 'Trust' ? 7 : (result.data.data.investorType == 'LLC' ? 7 : 4);
                    obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                    PubSub.publish('investorData', obj);
                    this.setState({
                        lpsubscriptionTotalObj: result.data.data,
                        investorType: result.data.data.investorType?result.data.data.investorType:'LLC',
                    }, () => {
                        if(this.state.lpsubscriptionTotalObj.investorQuestions.length > 0) {
                            this.addedQuestionsValidations();
                        } else {
                            this.props.history.push('/lp/investorInfo/' + this.state.lpsubscriptionTotalObj.id);
                        }
                    })
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    addedQuestionsValidations() {
        let questionsList = this.state.lpsubscriptionTotalObj.investorQuestions;
        if(questionsList.length >0) {
            // for(let index of questionsList) {
            //     if(index['isRequired'] && (index['typeOfQuestion'] === 2 || index['typeOfQuestion'] === 3) && (index['questionResponse'] === null || index['questionResponse'] === '')) {
            //         index['questionResponse'] = 0;
            //     } else if(index['questionResponse'] == '') {
            //         index['questionResponse'] = null;
            //     }
            // }
            this.setState({
                addedQuestionList:questionsList
            },()=>{
                this.addedQuestionsPageRightArrowEnable()
            })
        }
    }

    addedQuestionsPageRightArrowEnable() {
        let questionsList = this.state.addedQuestionList;
        if(questionsList.length >0) {
            for(let index of questionsList) {
                if(index['isRequired'] && (this.FsnetUtil._iNull(index['questionResponse']))) {
                    this.setState({
                        addedQuestionsPageValid:false
                    })
                    break;
                } else {
                    this.setState({
                        addedQuestionsPageValid:true
                    })
                }
            }
        }
    }

    proceedToNext() {
        let postobj = { investorType: this.state.investorType, investorQuestions: this.state.addedQuestionList, subscriptionId: this.state.lpsubscriptionTotalObj.id, step: this.state.investorType == 'Trust' || this.state.investorType == 'LLC' ? 9 : 6, isFormChanged:this.state.isFormChanged}
        this.open();
        this.Fsnethttp.updateLpSubscriptionDetails(postobj).then(result => {
            this.close();
            if (result.data) {
                this.props.history.push('/lp/capitalCommitment/' + this.state.lpsubscriptionTotalObj.id);
            }
        })
        .catch(error => {
            this.close();
        });

    }

    proceedToBack() {
        //console.log('id:::', this.state.lpsubscriptionTotalObj);
        if(this.state.investorType == 'LLC' || this.state.investorType == 'Trust') {
            this.props.history.push('/lp/erisa/'+this.FsnetUtil._encrypt(this.state.lpsubscriptionTotalObj.id));
        } else {
            this.props.history.push('/lp/qualifiedPurchaser/'+this.state.lpsubscriptionTotalObj.id);
        }
    }

    render() {
        return (
            <div className="accreditedInvestor width100 marginTopSticky">
                <div className="formGridDivMargins min-height-400">
                    <div className="title">Additional Questions</div>
                    {this.state.addedQuestionList.map((record, index) => {
                        return (
                            <Row className="step1Form-row marginBot20" key={index}>
                                <Col xs={12} md={12}>
                                    <label className="form-label word-wrap">{record['question']} 
                                    </label>
                                    {
                                        record['typeOfQuestion'] === 1 ?
                                            <Col xs={6} md={6} className="paddingLeftZero">
                                                <div>
                                                    <FormControl type="text" className={"inputFormControl " + ((record['questionResponse'] === '' || record['questionResponse'] === null) && record['isRequired'] && record['isTouched'] ? 'inputError' : '')} value={record['questionResponse']} onChange={(e) => this.additionalQuestionsHandleEvent(e,record,record['typeOfQuestion'])} onBlur={(e) => this.additionalQuestionsHandleEvent(e,record,record['typeOfQuestion'])} />
                                                    <div className="error">{record['typeOfQuestionMsz']}</div>
                                                </div>
                                            </Col>
                                        :
                                        record['typeOfQuestion'] === 2 ?
                                            <div>
                                                <Radio name={record['questionTitle']} className="radioSmallTxtWidth" inline checked={record['questionResponse'] == 1} id="yesCheckbox" onChange={(e) => this.additionalQuestionsHandleEvent(e,record,record['typeOfQuestion'],1)}>&nbsp; True
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                                <Radio name={record['questionTitle']} className="radioSmallTxtWidth" inline id="yesCheckbox" checked={record['questionResponse'] == 0} onChange={(e) => this.additionalQuestionsHandleEvent(e,record,record['typeOfQuestion'],0)}>&nbsp; False
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                            </div>
                                        :
                                            <div>
                                                <Radio name={record['questionTitle']} className="radioSmallTxtWidth" inline id="yesCheckbox" checked={record['questionResponse'] == 1} onChange={(e) => this.additionalQuestionsHandleEvent(e,record,record['typeOfQuestion'],1)}>&nbsp; Yes
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                                <Radio name={record['questionTitle']} className="radioSmallTxtWidth" inline id="yesCheckbox" checked={record['questionResponse'] == 0} onChange={(e) => this.additionalQuestionsHandleEvent(e,record,record['typeOfQuestion'],0)}>&nbsp; No
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                            </div>
                                    }
                                </Col>
                        </Row>
                        );
                    })}
                </div>
                <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.lpsubscriptionTotalObj && this.state.lpsubscriptionTotalObj.subscriptionStatus && this.state.lpsubscriptionTotalObj.subscriptionStatus.name === 'Closed'}>
                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className={"fa fa-chevron-right " + (!this.state.addedQuestionsPageValid ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default additionalQuestionsComponent;


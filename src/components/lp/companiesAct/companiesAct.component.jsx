import React, { Component } from 'react';
import '../lpsubscriptionform.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Radio, Row, Col, FormControl, OverlayTrigger, Tooltip,Modal } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../../util/util';

var tooltipsCount=0,objList=[], objListArray=[];
class companiesActComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.companiesActChangeEvent = this.companiesActChangeEvent.bind(this);
        this.proceedToNext = this.proceedToNext.bind(this);
        this.proceedToBack = this.proceedToBack.bind(this);
        this.openTooltip = this.openTooltip.bind(this);
        this.closeTooltipModal = this.closeTooltipModal.bind(this);
        this.state = {
            showModal: false,
            investorType: 'LLC',
            investorSubType: 0,
            companyActPageValid: false,
            numberOfDirectEquityOwners: '',
            numberOfexistingOrProspectives: '',
            investorObj: {},
            companiesAct:'',
            showTooltipModal: false,
            companiesActErrorMsz:'',
            jsonData:{},
            isFormChanged:false
        }

    }

    //Show Modal
    openTooltip(e, modalType, type) {
        // this.setState({ showTooltipModal: true });
        e.preventDefault();
        PubSub.publish('openModal', {investorType: this.state.investorType, modalType: modalType, type: type});
    }


    //Close  Modal
    closeTooltipModal() {
        this.setState({ showTooltipModal: false });
    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.getJsonData();
        this.getSubscriptionDetails(id);
    }
    checkTooltips(text,type) {
        let tooltipWords = ['investment company', 'Companies Act', 'Section 3(c)(1)', 'Section 3(c)(7)'];
        let count = 0 ;
        for(let idx=0; idx<tooltipWords.length; idx++) {
            if(text && text.indexOf(tooltipWords[idx]) > -1 && objList.indexOf(type) === -1) {
                let arrayList = this.FsnetUtil.allIndexOf(text,tooltipWords[idx])
                let splitText = [];
                let currentIndex = 0;
                for(let jdx=0; jdx<arrayList.length; jdx++) {
                    let jdxLength = arrayList[jdx]+tooltipWords[idx].length;
                    if(jdx === (arrayList.length-1)) {
                        splitText.push(text.substr(currentIndex,text.length))
                    } else {
                        splitText.push(text.substr(currentIndex,jdxLength))
                        currentIndex = jdxLength
                    }
                }
                let newText = ''
                if(splitText.length === 1) {
                    count++;
                    let indexPos = tooltipsCount+count
                    text = text.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                }else {
                    for(let pos of splitText) {
                        count++;
                        let indexPos = tooltipsCount+count
                        newText = newText+pos.replace(tooltipWords[idx], `<span className="helpWord" id="anchor${indexPos}" tooltip_id="${tooltipWords[idx]}" style="font-weight:bold; text-decoration:underline">${tooltipWords[idx]}</span>`)
                    }
                    text = newText;
                }

            }
            if(text && idx === tooltipWords.length-1 && objList.indexOf(type) === -1) {
                objListArray.push({type:type,text:text})
                objList.push(type)
            }
        }
        tooltipsCount = tooltipsCount+count;
        for(let index of objListArray) {
            if(index['type'] && index['text']) {
                if(index['type'] === type) {
                    return index['text'];
                }
            }
        }
        return text
    }
    

    addClickEvent(obj) {
        if(tooltipsCount > 0) {
            for(let idx=0; idx<tooltipsCount; idx++) {
                if(document.getElementById("anchor"+(idx+1))) {
                    document.getElementById("anchor"+(idx+1)).addEventListener("click", function(){
                        let name = document.getElementById("anchor"+(idx+1)).getAttribute('tooltip_id');
                        let modalType, type;
                        if(name === 'investment company') {
                            modalType = 'company';
                            type = 'companyActModal'
                        } else if(name === 'Companies Act') {
                            modalType = 'actModalWindow';
                            type = 'company'
                        }else if(name === 'Section 3(c)(1)') {
                            modalType = 'company';
                            type = 'section3c1'
                        }else if(name === 'Section 3(c)(7)') {
                            modalType = 'company';
                            type = 'section3c7'
                        }
                        PubSub.publish('openModal', {investorType: obj.investorType, investorSubType: obj.investorSubType, modalType: modalType, type: type});
                    });
                }
            }
        }
    }


    getJsonData() {
        this.Fsnethttp.getJson('companiesAct').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getSubscriptionDetails(id) {
        
        if (id) {
            this.open();
            this.Fsnethttp.getLpSubscriptionDetails(id).then(result => {
                this.close();
                if (result.data) {
                    let obj = result.data.data;
                    obj['currentInvestorInfoPageNumber'] = 1;
                    obj['currentPageCount'] = 4;
                    obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                    PubSub.publish('investorData',obj );
                    this.setState({
                        investorObj: result.data.data,
                        investorType: result.data.data.investorType?result.data.data.investorType:'LLC',
                        investorSubType: result.data.data.investorSubType ? result.data.data.investorSubType : (result.data.data.investorType == 'Trust' ? 9 : 0),
                        companiesAct: result.data.data.companiesAct,
                        numberOfDirectEquityOwners: result.data.data.numberOfDirectEquityOwners != 1 ? result.data.data.numberOfDirectEquityOwners:'',
                        numberOfexistingOrProspectives: result.data.data.numberOfexistingOrProspectives || '',
                        existingOrProspectiveInvestorsOfTheFund: result.data.data.existingOrProspectiveInvestorsOfTheFund ? result.data.data.existingOrProspectiveInvestorsOfTheFund : false
                    },()=>{
                        this.validateCompanyPages();
                        // this.updateInvestorInputFields(this.state.investorObj)
                        this.addClickEvent(result.data.data);
                    })
                }
            })
            .catch(error => {
                this.close();
            });
        }
    }

    updateInvestorInputFields(data) {
        if(this.state.companiesAct) {
            this.setState({
                companyActPageValid: true
            })
        }
    }

    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    validateCompanyPages() {
        let validValues = ['companiesAct', 'numberOfDirectEquityOwners', 'existingOrProspectiveInvestorsOfTheFund'];
        // if (this.state.investorType == 'Trust' || this.state.investorType == 'LLC') {
        //     if(this.state.investorSubType == 9 || this.state.investorType == 'LLC') {
        //         if(this.state.companiesAct == 2 || this.state.companiesAct == 3 || this.state.companiesAct == 4) {
        //             validValues = ['companiesAct', 'numberOfDirectEquityOwners', 'existingOrProspectiveInvestorsOfTheFund'];
        //         } else {
        //             validValues = ['companiesAct'];
        //         }
        //     } else {
        //         if(this.state.companiesAct == 2 || this.state.companiesAct == 3 || this.state.companiesAct == 4) {
        //             validValues = ['companiesAct', 'numberOfDirectEquityOwners', 'existingOrProspectiveInvestorsOfTheFund'];
        //         } else {
        //             validValues = ['companiesAct'];
        //         }
        //     }
        // } else {
        //     validValues = ['companiesAct'];
        // }

        for (let field of validValues) {
            if (this.state[field] === null || this.state[field] === '' || this.state[field] === undefined || this.state[field] === 0) {
                this.setState({
                    companyActPageValid: false,
                    [field + 'Valid']: false
                })
                break;
            } else {
                if (field == 'existingOrProspectiveInvestorsOfTheFund' && this.state['existingOrProspectiveInvestorsOfTheFund'] && (this.state['numberOfexistingOrProspectives'] === null || this.state['numberOfexistingOrProspectives'] === '' || this.state['numberOfexistingOrProspectives'] === undefined || this.state['numberOfexistingOrProspectives'] == 0)) {
                    this.setState({
                        companyActPageValid: false,
                        [field + 'Valid']: false
                    })
                    break;
                }
                this.setState({
                    companyActPageValid: true,
                    [field + 'Valid']: true
                })
            }
        }
    }

    formEventChanges(e, type) {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            isFormChanged:true
        })
        switch (type) {
            case 'radio':
                this.setState({
                    [name]: value ? ((value == 'true' || value == 'false') ? JSON.parse(value) : value) : '',
                    [name + 'Valid']: true,
                    [name + 'Touched']: true
                }, () => {
                    if ((name == 'trustLegallyDomiciled' || name == 'mailingAddressCountry') && value == 231) {
                        this.getUSStates();
                    }
                    this.validateCompanyPages();
                });
                break;

            case 'number':
                const re = this.Constants.NUMBER_HUNDRED_REGEX;
                if (re.test(value.trim())) {
                    this.setState({
                        [name]: value ? parseInt(value) : '',
                        [name + 'Valid']: true,
                        [name + 'Touched']: true
                    }, () => {
                        this.validateCompanyPages();
                    })
                    return true;
                } else {
                    if (parseInt(value) < 0) {
                        this.setState({
                            [name + 'Valid']: false,
                            [name + 'Touched']: true
                        }, () => {
                            this.validateCompanyPages();
                        })
                        return true;
                    } else if(isNaN(parseInt(value))) {
                        this.setState({
                            [name]: '',
                            [name + 'Valid']: false,
                            [name + 'Touched']: true
                        }, () => {
                            this.validateCompanyPages();
                        })
                        return true;
                    }
                }
                break;
            case 'numberOfexistingOrProspectives':
                if (value.trim() != '') {
                    this.setState({
                        [name]: value,
                        [name + 'Valid']: true,
                        [name + 'Touched']: true
                    }, () => {
                        this.validateCompanyPages();
                    })
                    return true;
                } else {
                    this.setState({
                        [name]: '',
                        [name + 'Valid']: false,
                        [name + 'Touched']: true
                    }, () => {
                        this.validateCompanyPages();
                    })
                    return true;
                }
                break;
                
            default:
                break;    
        }
    }

    valueTouched(e, type) {
        const name = e.target.name;
        this.setState({
            [name + 'Touched']: true
        })
    }

    companiesActChangeEvent(event, type, value) {
        this.setState({
            [type]:value,
            isFormChanged:true
        }, () => {
            this.validateCompanyPages();
        }) 
    }

    checkLookThroughIssuesValue() {
        let fileds = ['entityProposingAcquiringInvestment','entityHasMadeInvestmentsPriorToThedate','partnershipWillNotConstituteMoreThanFortyPercent','beneficialInvestmentMadeByTheEntity'];
        for(let index of fileds) {
            if(this.state.investorObj[index] === false) {
               return false
            }
        }
        return true;
    }
    
    

    proceedToNext() {
        // const lookThroughCondition = this.checkLookThroughIssuesValue();
        let postobj = {
            investorType:this.state.investorType,
            investorSubType: this.state.investorSubType, 
            subscriptionId:this.state.investorObj.id, 
            step:5,
            companiesAct: this.state.companiesAct, 
            numberOfDirectEquityOwners: this.state.numberOfDirectEquityOwners, 
            existingOrProspectiveInvestorsOfTheFund:  this.state.existingOrProspectiveInvestorsOfTheFund,
            numberOfexistingOrProspectives: this.state.existingOrProspectiveInvestorsOfTheFund === true ? this.state.numberOfexistingOrProspectives : null,
            isFormChanged:this.state.isFormChanged
        }
        this.open();
        
        this.Fsnethttp.updateLpSubscriptionDetails(postobj).then(result => {
            this.close();
            if (result.data) {
                let obj = result.data.data;
                obj['currentInvestorInfoPageNumber'] = 1;
                obj['currentPageCount'] = 4;
                obj['currentPage'] = this.FsnetUtil.getCurrentPageForLP();
                PubSub.publish('investorData', obj);
                // if(this.state.investorType == 'Trust') {
                //     this.props.history.push('/lp/erisa/'+this.state.investorObj.id);
                // } else {
                    this.props.history.push('/lp/entityProposing/'+this.FsnetUtil._encrypt(this.state.investorObj.id));
                // }
                
            }
        })
        .catch(error => {
            this.close();
            if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                this.setState({
                    companiesActErrorMsz: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    companiesActErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                });
            }
        });
    }

    proceedToBack () {
        this.props.history.push('/lp/qualifiedPurchaser/'+this.state.investorObj.id);
    }

    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                trigger={['click', 'hover', 'focus']}
                overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                placement='left'
                delayShow={300}
                delayHide={150}
                rootClose
              >
                <span >{children}</span>
              </OverlayTrigger>
            );
        }
        return (
            <div className="accreditedInvestor width100 marginTopSticky">
                <div className="formGridDivMargins min-height-400">
                    <div className="title">{this.state.jsonData.COMPANIES_ACT}</div>
                    {
                        ( this.state.investorType == 'Trust' || this.state.investorType == 'LLC' ) &&
                        
                        <div>
                            {
                                this.state.investorType  === 'LLC' ?
                                <Row className="step1Form-row" hidden={this.state.investorType !== 'LLC'}>
                        <Col xs={12} md={12}>
                            <label className="form-label width100" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.WHETHER_ENTITY_AN_INVESTMENT_COMPANY,'obj1')
                            }}></label>
                            <Radio name="companiesAct" inline className="companiesActRadio" checked={this.state.companiesAct === '1'} onChange={(e) => this.companiesActChangeEvent(e, 'companiesAct', '1')}>
                                <span className="radio-checkmark"></span>
                                <div className="radioText"  dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.ENTITY_NOT_AN_INVESTMENT_COMPANY_DOES_NOT_RELY_SECTION_3C1_OR_3C7,'obj2')
                                }}></div>
                            </Radio>

                            <Radio name="companiesAct" inline className="companiesActRadio" checked={this.state.companiesAct === '2'} onChange={(e) => this.companiesActChangeEvent(e, 'companiesAct', '2')}>
                                <span className="radio-checkmark"></span>
                                <div className="radioText" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.ENTITY_WOULD_BE_AN_INVESTMENT_COMPANY_NOT_TREATED_RELIES_SECTION_3C1,'obj3')
                                }}></div>
                            </Radio>

                            <Radio name="companiesAct" inline className="companiesActRadio" checked={this.state.companiesAct === '3'} onChange={(e) => this.companiesActChangeEvent(e, 'companiesAct', '3')}>
                                <span className="radio-checkmark"></span>
                                <div className="radioText" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.ENTITY_WOULD_BE_AN_INVESTMENT_COMPANY_NOT_TREATED_RELIES_SECTION_3C7,'obj4')
                                }}></div>
                            </Radio>

                            <Radio name="companiesAct" inline className="companiesActRadio" checked={this.state.companiesAct === '4'} onChange={(e) => this.companiesActChangeEvent(e, 'companiesAct', '4')}>
                                <span className="radio-checkmark"></span>
                                <div className="radioText" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.ENTITY_AN_INVESTMENT_COMPANY_PURSUANT_COMPANIES_ACT,'obj5')
                                }}></div>
                            </Radio>
                        </Col>
                    </Row>
                    :
                            <Row className="step1Form-row">
                                <Col xs={12} md={12}>
                                    <label className="form-label width100" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.WHETHER_TRUST_AN_INVESTMENT_COMPANY,'obj6')
                            }}></label>
                                    <Radio name="companiesActTrust" inline className="companiesActRadio" checked={this.state.companiesAct === '1'} onChange={(e) => this.companiesActChangeEvent(e, 'companiesAct', '1')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.TRUST_NOT_AN_INVESTMENT_COMPANY_DOES_NOT_RELY_SECTION_3C1_OR_3C7,'obj7')
                                        }}></div>
                                    </Radio>

                                    <Radio name="companiesActTrust" inline className="companiesActRadio" checked={this.state.companiesAct === '2'} onChange={(e) => this.companiesActChangeEvent(e, 'companiesAct', '2')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.TRUST_WOULD_BE_AN_INVESTMENT_COMPANY_NOT_TREATED_RELIES_SECTION_3C1,'obj8')
                                        }}></div>
                                    </Radio>

                                    <Radio name="companiesActTrust" inline className="companiesActRadio" checked={this.state.companiesAct === '3'} onChange={(e) => this.companiesActChangeEvent(e, 'companiesAct', '3')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.TRUST_WOULD_BE_AN_INVESTMENT_COMPANY_NOT_TREATED_RELIES_SECTION_3C7,'obj9')
                                        }}></div>
                                    </Radio>

                                    <Radio name="companiesActTrust" inline className="companiesActRadio" checked={this.state.companiesAct === '4'} onChange={(e) => this.companiesActChangeEvent(e, 'companiesAct', '4')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText" dangerouslySetInnerHTML={{
                                                        __html: this.checkTooltips(this.state.jsonData.TRUST_AN_INVESTMENT_COMPANY_PURSUANT_COMPANIES_ACT,'obj10')
                            }}></div>
                                    </Radio>
                                </Col>
                            </Row>
                            }
                          
                            <Row className="step1Form-row">
                                
                                {
                                    this.state.investorSubType == 9
                                ?
                                    <div>
                                            <div className="title marginLeft15">{this.state.jsonData.NUM_OFF_EQUITY_OWNERS}</div>
                                            <Col xs={12} md={12} className="step1Form-row marginTop10">
                                            <label className="form-label width100">{this.state.jsonData.SPECIFY_NUMBER_OF_GRANTORS_TRUST}</label>
                                            <FormControl type="text" placeholder="Enter number" name="numberOfDirectEquityOwners" className={"inputFormControl inputWidth290 " + (this.state.numberOfDirectEquityOwnersTouched && !this.state.numberOfDirectEquityOwners ? 'inputError' : '')} value={this.state.numberOfDirectEquityOwners} onChange={(e) => this.formEventChanges(e, 'number')} onBlur={(e) => this.valueTouched(e)} />
                                            {this.state.numberOfDirectEquityOwnersTouched && !this.state.numberOfDirectEquityOwners ? <span className="error">{this.state.jsonData.ENTER_NUMBER_OF_GRANTORS_TRUST}</span> : null}
                                        </Col>  
                                    </div>  
                                :
                                    <div>
                                        <div className="title marginLeft15">{this.state.investorType == 'LLC' ? this.state.jsonData.RELATED_INVESTORS :this.state.jsonData.NUM_OFF_EQUITY_OWNERS}</div>
                                        <Col xs={12} md={12} className="step1Form-row marginTop10" hidden={this.state.investorType == 'LLC'}>
                                            <label className="form-label width100">{this.state.jsonData.SPECIFY_NUMBER_OF_GRANTORS_TRUST}</label>
                                            <FormControl type="text" placeholder="Enter number" name="numberOfDirectEquityOwners" className={"inputFormControl inputWidth290 " + (this.state.numberOfDirectEquityOwnersTouched && !this.state.numberOfDirectEquityOwners ? 'inputError' : '')} value={this.state.numberOfDirectEquityOwners} onChange={(e) => this.formEventChanges(e, 'number')} onBlur={(e) => this.valueTouched(e)} />
                                            {this.state.numberOfDirectEquityOwnersTouched && !this.state.numberOfDirectEquityOwners ? <span className="error">{this.state.jsonData.ENTER_NUMBER_OF_BENEFICIARIES_TRUST}</span> : null}
                                        </Col> 
                                    </div>
                                }
                                
                                <Col xs={12} md={12} className="marginTop10">
                                    {
                                        this.state.investorType === 'LLC' ?
                                        <label className="form-label width100">{this.state.jsonData.EXISTING_OR_PROSPECTIVE_INVESTORS}</label>
                                        :
                                        <div>
                                            <div className="title marginTop10">{this.state.jsonData.RELATED_INVESTORS}</div>
                                            <label className="form-label width100">{this.state.jsonData.THERE_ANY_EXISTING_OR_PROSPECTIVE_INVESTORS}?</label>
                                        </div>

                                    }
                                    <Radio name="rule501" inline id="yesCheckbox" value={true} name="existingOrProspectiveInvestorsOfTheFund" checked={this.state.existingOrProspectiveInvestorsOfTheFund == true} onChange={(e) => this.formEventChanges(e, 'radio')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText">Yes</div>
                                    </Radio>
                                    <Radio name="rule501" inline id="yesCheckbox" value={false} name="existingOrProspectiveInvestorsOfTheFund" checked={this.state.existingOrProspectiveInvestorsOfTheFund == false} onChange={(e) => this.formEventChanges(e, 'radio')}>
                                        <span className="radio-checkmark"></span>
                                        <div className="radioText">No</div>
                                    </Radio>
                                </Col>
                                
                                <Col xs={12} md={12} className="marginTop24" hidden={!this.state.existingOrProspectiveInvestorsOfTheFund}>
                                {
                                        this.state.investorType === 'LLC' ?
                                        <label className="form-label width100">{this.state.jsonData.LIST_EXISTING_OR_PROSPECTIVE_INVESTORS}</label>
                                        :
                                        <label className="form-label width100">{this.state.jsonData.HOW_MANY_EXISTING_OR_PROSPECTIVE_INVESTORS}?</label>

                                    }

                                    <FormControl type="text" placeholder="Enter Names" name="numberOfexistingOrProspectives" className={"inputFormControl inputWidth290 " + (this.state.numberOfexistingOrProspectivesTouched && !this.state.numberOfexistingOrProspectives ? 'inputError' : '')} value={this.state.numberOfexistingOrProspectives} onChange={(e) => this.formEventChanges(e, 'numberOfexistingOrProspectives')} onBlur={(e) => {this.state.existingOrProspectiveInvestorsOfTheFund ? this.valueTouched(e) : this.setState({ numberOfexistingOrProspectives: '' })}} />
                                        {this.state.numberOfexistingOrProspectivesTouched && !this.state.numberOfexistingOrProspectives ? <span className="error">{this.state.jsonData.ENTER_NUMBER_OF_EXISTING_PROSPECTIVE_INVESTORS}</span> : null}
                                </Col>
                                    <div className="title marginLeft15 marginTop20" hidden={this.state.investorType !== 'LLC'}>{this.state.jsonData.NUM_OFF_EQUITY_OWNERS}</div> 
                                <Col xs={12} md={12} className="step1Form-row marginTop10" hidden={this.state.investorType !== 'LLC'}>
                                    <label className="form-label width100">{this.state.jsonData.ENTITY_SPECIFY_EQUITY_OWNERS}</label>
                                    <FormControl type="text" placeholder="Enter number" name="numberOfDirectEquityOwners" className={"inputFormControl inputWidth290 " + (this.state.numberOfDirectEquityOwnersTouched && !this.state.numberOfDirectEquityOwners ? 'inputError' : '')} value={this.state.numberOfDirectEquityOwners} onChange={(e) => this.formEventChanges(e, 'number')} onBlur={(e) => this.valueTouched(e)} />
                                    {this.state.numberOfDirectEquityOwnersTouched && !this.state.numberOfDirectEquityOwners ? <span className="error">{this.Constants.EQUITY_OWNERS_REQUIRED}</span> : null}
                                </Col> 
                            </Row>
                            
                        </div>
                    }

                </div>
                <div className="margin30 error">{this.state.companiesActErrorMsz}</div>
                <div className="footer-nav footerDivAlign">
                    <div hidden={this.state.investorObj.subscriptionStatus && this.state.investorObj.subscriptionStatus.name === 'Closed'}>
                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className={"fa fa-chevron-right " + (!this.state.companyActPageValid ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default companiesActComponent;


import React, { Component } from 'react';
import '../fundSetup.component.css';
import { Button, Checkbox as CBox, Row, Col, FormControl,Tooltip,OverlayTrigger,Modal } from 'react-bootstrap';
import userDefaultImage from '../../../images/default_user.png';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetUtil } from '../../../util/util';
import { FundUtil } from '../util/fundUtil';
import {Constants} from '../../../constants/constants';
import Loader from '../../../widgets/loader/loader.component';
import { reactLocalStorage } from 'reactjs-localstorage';
import UserModalComponent from '../modals/userModal.component'
import ActionsComponent from '../fundActions/fundActions';
import { PubSub } from 'pubsub-js';
import {CopyToClipboard} from 'react-copy-to-clipboard';

var _ = require('lodash');

var fundInfo = {}, pageInfo={};
class GpDelegatesComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.FundUtil = new FundUtil();
        this.Constants = new Constants();
        this.state = {
            showAddGpDelegateModal: false,
            getGpDelegatesList: [],
            firmId:null,
            fundId:null,
            isGpDelgateFormValid: false,
            firstNameBorder: false,
            firstNameMsz: '',
            firstName: '',
            firstNameValid: false,
            lastNameBorder: false,
            lastNameMsz: '',
            lastName: '',
            lastNameValid: false,
            emailBorder: false,
            emailMsz: '',
            email: '',
            emailValid: false,
            gpDelegateErrorMsz: '',
            gpDelegatesSelectedList:[],
            gpDelegateScreenError:'',
            noDelegatesMsz:'',
            fundObj: {},
            gPDelegateRequiredDocuSignBehalfGPList : [],
            gPDelegateRequiredConsentHoldClosingList : [],
            fundStatus:'Open',
            jsonData:{},
            accountType: this.FsnetUtil.getUserRole(),
            showDelegateModal:false,
            userModal:false
        }
        fundInfo = PubSub.subscribe('fundData', (msg, data) => {
            this.updateGps(data)
        });
        pageInfo = PubSub.publish('pageNumber', {type:'sideNav', page: 'gpDelegate'});
    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(fundInfo);
        PubSub.unsubscribe(pageInfo);
    }


     //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('profileModal', data);
    }

    //Onchange event for all input text boxes.
    handleInputChangeEvent = (event,type, obj) => {
        let dataObj = {}; 
        this.setState({
            gpDelegateErrorMsz: ''
        })
        switch(type) {
            case 'firstName':
                if(!event.target.value.trim()) {
                    this.setState({
                        firstNameMsz: this.Constants.FIRST_NAME_REQUIRED,
                        firstNameValid: false,
                        firstNameBorder: true,
                        firstName: ''
                    })
                    dataObj ={
                        firstNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        firstName: event.target.value,
                        firstNameMsz: '',
                        firstNameValid: true,
                        firstNameBorder: false,
                    })
                    dataObj ={
                        firstNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'lastName':
                if(!event.target.value.trim()) {
                    this.setState({
                        lastNameMsz: this.Constants.LAST_NAME_REQUIRED,
                        lastNameValid: false,
                        lastNameBorder: true,
                        lastName: ''
                    })
                    dataObj ={
                        lastNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        lastName: event.target.value,
                        lastNameMsz: '',
                        lastNameValid: true,
                        lastNameBorder: false,
                    })
                    dataObj ={
                        lastNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'email':
                if(!event.target.value.trim()) {
                    this.setState({
                        emailMsz: this.Constants.VALID_EMAIL,
                        emailValid: false,
                        emailBorder: true,
                        email: ''
                    })
                    dataObj ={
                        emailValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        email: event.target.value,
                        emailMsz: '',
                        emailValid: true,
                        emailBorder: false,
                    })
                    dataObj ={
                        emailValid :true
                    };
                this.updateStateParams(dataObj);
                }
                break;
            case 'user':
                let getSelectedList = this.state.gpDelegatesSelectedList;
                let behalfList =  this.state.gPDelegateRequiredDocuSignBehalfGPList;
                let closingList = this.state.gPDelegateRequiredConsentHoldClosingList;
                let selectedId = obj.record.id;
                this.selectHoldClosing(selectedId,event.target.checked)
                if(event.target.checked) {
                    if(getSelectedList.indexOf(selectedId) === -1) {
                        getSelectedList.push(selectedId);
                    }
                    if(closingList.indexOf(selectedId) === -1) {
                        closingList.push(selectedId);
                    }
                } else {
                    var index = getSelectedList.indexOf(selectedId);
                    var closingIndex = closingList.indexOf(selectedId);
                    if (index !== -1) {
                        getSelectedList.splice(index, 1);
                    }
                    if (closingIndex !== -1) {
                        closingList.splice(index, 1);
                    }
                }
                var behalfIndex = behalfList.indexOf(selectedId);
                if (behalfIndex !== -1) {
                    behalfList.splice(index, 1);
                }
                this.updateSelectedValueToList(obj.record.id,event.target.checked)
                this.setState({
                    gpDelegatesSelectedList: getSelectedList,
                    gPDelegateRequiredDocuSignBehalfGPList:behalfList,
                    gPDelegateRequiredConsentHoldClosingList: closingList,
                    gpDelegateScreenError:''
                })
                break;
            case type:
                let getList = this.state[type+'List'];
                let id = obj.record.id
                if(event.target.checked) {
                    if(getList.indexOf(id) === -1) {
                        getList.push(id);
                    }
                } else {
                    var index = getList.indexOf(id);
                    if (index !== -1) {
                        getList.splice(index, 1);
                    }
                }
                this.selectHoldClosing(id,event.target.checked,type)
                this.setState({
                    type: getList,
                })
                break;
            default:
                // do nothing
                break;
        }
    }

    selectHoldClosing = (id,value,type) => {
        if(type) {
            for(let index of this.state.getGpDelegatesList) {
                if(index['id'] === id) {
                    index[type] = value
                }
            }
        } else {
            for(let index of this.state.getGpDelegatesList) {
                if(index['id'] === id) {
                    index['gPDelegateRequiredConsentHoldClosing'] = value
                    index['gPDelegateRequiredDocuSignBehalfGP'] = value ? !value:value
                }
            }
        }
    }

    addDelegateFn = () => {
        let firstName = this.state.firstName;
        let lastName = this.state.lastName;
        let email = this.state.email;
        let error = false;
        let emailRegex = this.Constants.EMAIL_REGEX
        if(email && !emailRegex.test(email)) {
            this.setState({
                emailMsz: this.Constants.VALID_EMAIL,
                emailBorder: true,
            
            })
            error = true;
        } 
        
        if(!error) {
            let postObj = {firstName:firstName, lastName:lastName, email:email, fundId: this.state.fundId};
            this.open()
            this.Fsnethttp.addGpDelegate(postObj).then(result=>{
                this.close();
                if(result.data) {
                    //Get list of delegates
                    let gpDelegateList = this.state.getGpDelegatesList;
                    let data = result.data.data;
                    //Add checked as true to auto select the user
                    data['selected'] = true;
                    gpDelegateList.push(data);
                    //Get list of selected list of users
                    let selectedList = this.state.gpDelegatesSelectedList;
                    //Push the newly created gp delegate id to selected list of users
                    selectedList.push(data.id);
                    this.setState({
                        getGpDelegatesList:gpDelegateList,
                        showAddGpDelegateModal: false,
                        gpDelegatesSelectedList: selectedList
                    });
                    this.clearFormFileds();
                }
            })
            .catch(error=>{
                this.close();
                if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        gpDelegateErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        gpDelegateErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });
        }
    }

    //Clear the fileds
    clearFormFileds = () => {
        this.setState({
            gpDelegateErrorMsz: '',
            firstName: '',
            lastName: '',
            email:''
        });
    }


    // Update state params values and login button visibility

    updateStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, ()=>{
            this.enableDisableSubmitButton();
        });
    }

    // Enable / Disble functionality of Submit Button
    enableDisableSubmitButton = () => {
        let status = (this.state.firstNameValid && this.state.lastNameValid && this.state.emailValid) ? true : false;
        this.setState({
            isGpDelgateFormValid : status
        });
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    proceedToNext = () => {
        this.open();
        let gpDelegates = [];
        for(let index of this.state.gpDelegatesSelectedList) {
            let obj = {};
            obj['delegateId'] = index;
            if(this.state.gPDelegateRequiredDocuSignBehalfGPList.indexOf(index) !== -1) {
                obj['gPDelegateRequiredDocuSignBehalfGP'] = 1
            } else {
                obj['gPDelegateRequiredDocuSignBehalfGP'] = 0
            }
            if(this.state.gPDelegateRequiredConsentHoldClosingList.indexOf(index) !== -1) {
                obj['gPDelegateRequiredConsentHoldClosing'] = 1
            } else {
                obj['gPDelegateRequiredConsentHoldClosing'] = 0
            }
            gpDelegates.push(obj);
        }
        let delegateObj = {fundId:parseInt(this.state.fundId), vcfirmId:this.state.firmId, gpDelegates:gpDelegates }
        this.Fsnethttp.assignDelegatesToFund(delegateObj).then(result=>{
            this.close();
            this.props.history.push('/fundSetup/editSubForm/'+this.FsnetUtil._encrypt(this.state.fundId));
        })
        .catch(error=>{
            this.close();
        });
    }
    
    proceedToBack = () => {
        if(this.FsnetUtil.getUserRole () != 'SecondaryGP') {
            this.props.history.push('/fundSetup/gpSignatories/'+this.FsnetUtil._encrypt(this.state.fundId));
        } else {
            this.props.history.push('/fundSetup/funddetails/'+this.FsnetUtil._encrypt(this.state.fundId));
        }
    }

    navigateToNextScreen = () => {
        this.props.history.push('/fundSetup/editSubForm/'+this.FsnetUtil._encrypt(this.state.fundId));
    }

    addGpDelegateBtn = () => {
        PubSub.publish('openGpModal', this.state.fundObj);
    }

    closeGpDelegateModal = () => {
        this.setState({
            showAddGpDelegateModal: false
        })
        this.clearFormFileds();
    }

    selectedMembersPushToList = () => {
        if(this.state.getGpDelegatesList.length >0) {
            let list = [];
            let holdClosingList = [];
            let requiredDocuSignBehalfGP = [];
            for(let index of this.state.getGpDelegatesList) {
                if(index['selected'] === true) {
                    list.push(index['id'])
                }
                if(index['gPDelegateRequiredConsentHoldClosing'] === true) {
                    holdClosingList.push(index['id'])
                }
                if(index['gPDelegateRequiredDocuSignBehalfGP'] === true) {
                    requiredDocuSignBehalfGP.push(index['id'])
                }
                this.setState({
                    gpDelegatesSelectedList: list,
                    gPDelegateRequiredConsentHoldClosingList: holdClosingList,
                    gPDelegateRequiredDocuSignBehalfGPList: requiredDocuSignBehalfGP
                })
            }
        }
    }
    

    updateSelectedValueToList = (id, value) => {
        if(this.state.getGpDelegatesList.length >0) {
            let getList = this.state.getGpDelegatesList;
            let list = this.state.gPDelegateRequiredConsentHoldClosingList;
            for(let index of getList) {
                if(index['id'] === id) {
                    index['selected'] = value
                    
                }
                this.setState({
                    gpDelegatesSelectedList: getList
                })
            }
            for(let index of list) {
                if(index['id'] === id) {
                    index['gPDelegateRequiredConsentHoldClosing'] = value
                }
                this.setState({
                    gPDelegateRequiredConsentHoldClosingList: list
                })
            }
        }
    }

    componentDidMount() { 
        if(['GP','FSNETAdministrator','SecondaryGP'].indexOf(this.FsnetUtil.getUserRole()) > -1) {
            this.getJsonData();
            let firmId = reactLocalStorage.getObject('firmId');
            let fundId = this.FsnetUtil._getId();
            this.setState({
                fundId: fundId,
                firmId: firmId
            },()=>{
                this.getGpListFromApi(fundId)
            })
        } else {
            this.props.history.push('/dashboard');
        }
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('assignDelegates').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getGpListFromApi = (fundId) => {
        this.open();
        this.Fsnethttp.getFund(fundId).then(result=>{
            this.close();
            this.FsnetUtil.setLeftNavHeight();
            PubSub.publish('fundData', result.data.data);
            this.updateGps(result.data.data)
        })
        .catch(error=>{
                this.close();
                this.setState({
                    getGpDelegatesList: [],
                    noDelegatesMsz: this.Constants.NO_GP_DELEGATES
                })
           
        });
    }

    updateGps = (data) => {
        if(data && data.gpDelegates.length >0) {
            this.setState({ fundObj:data, getGpDelegatesList: data.gpDelegates,fundStatus:data.status }, () => this.selectedMembersPushToList());
        } else {
            this.setState({
                getGpDelegatesList: [],
                fundObj:data,
                noDelegatesMsz: this.Constants.NO_GP_DELEGATES
            })
        }
    }

    openDelegateModal = () => {this.setState({showDelegateModal:true})}
    closeDelegateModal = () => {this.setState({showDelegateModal:false})}


    closeUserModalComponent = (data) => {
        if(data) {
            this.setState({
                showUserModal:false
            })
        }
    }

    closeActionComponent = (data) => {
        setTimeout(() => {
            this.setState({
                showActions:false
            },()=>{
                if(data) {
                    this.getGpListFromApi(this.state.fundId);
                } 
            })
        }, 1000);
    }


    /**
     * Update user data,
     * add selected value to edited user,
     * find index and splice the user and update the obj
     */

    updateUser = (data) => {
        let userObj = data.user;
        let list = [...this.state.getGpDelegatesList]
        //Edit user
        if(data.type === 2) {
            let user = list.filter((obj)=> obj.id === userObj.id);
            userObj['selected'] = user[0]['selected'];
            userObj['gPDelegateRequiredConsentHoldClosing'] = user[0]['gPDelegateRequiredConsentHoldClosing'];
            userObj['profilePic'] = user[0]['profilePic'];
            let index = _.findIndex(this.state.getGpDelegatesList, {'id':userObj.id});
            list.splice(index, 1, userObj);
            this.setState({
                getGpDelegatesList:list
            })
        } else if(data.type === 1){
            //add user
            userObj['selected'] = true;
            userObj['gPDelegateRequiredConsentHoldClosing'] = true;
            list.unshift(userObj)
            this.setState({
                getGpDelegatesList:list
            },()=>{
                this.selectedMembersPushToList()
            })
        } else if(data.type === 3){
            //Delete user
            let users = list.filter((obj)=> obj.id !== userObj);
            this.setState({
                getGpDelegatesList:users
            })
        }
        
    }


    editUser = (data) => () => {
        data['type'] = 2;
        data['accountType'] = 'GPDelegate';
        this.setState({
            userObj:data,
            showUserModal:true
        })
    }

    deleteUser = (data) => () => {
        data['type'] = 3;
        data['accountType'] = 'GPDelegate';
        this.setState({
            userObj:data,
            showUserModal:true
        })
    }

    addUser = () => {
        let data = {};
        data['accountType'] = 'GPDelegate';
        data['type'] = 1;
        this.setState({
            userObj:data,
            showUserModal:true
        })
    }

    userAction = (data,type) => () => {
        data['accountType'] = 'GPDelegate'
        data['type'] = type;
        this.setState({
            userObj:data,
            showActions:true
        })
    }

    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
              <OverlayTrigger
                overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                placement="left"
                delayShow={300}
                delayHide={150}
                rootClose
              >
                <a href={href}>{children}</a>
              </OverlayTrigger>
            );
          }
        return (
            <div className="step2Class marginTop6">
            <div className="GpDelegatesContainer fundSetup" >
                <h1 className="title">{this.state.jsonData.ASSIGN_DELEGATES}</h1>
                <p className="subtext marginBottom44">{this.state.jsonData.DELEGATES_ARE_PARTIES}</p>
                {
                    ['GP','FSNETAdministrator','SecondaryGP'].indexOf(this.FsnetUtil.getUserRole()) > -1 &&
                        <Button className="fsnetButton" onClick={this.addUser}><i className="fa fa-plus"></i>{this.state.jsonData.DELEGATE}</Button>
                }
                <div className="gpDelegateTableHeader width1150" hidden={this.state.getGpDelegatesList.length ===0}>
                    <div className="gpDelegateHeadaerNoText"></div>
                    <div className="gpDelegateHeadaerTitle width210 text-left"><div>{this.state.jsonData.ENABLE_FOR_THIS_FUND}
                        &nbsp;
                        <span>
                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_ENABLE_FOR_THIS_FUND}>
                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                            </LinkWithTooltip>
                        </span> 
                    </div></div>
                    <div className="gpDelegateHeadaerTitle width210 text-left"><div className="closingText">{this.state.jsonData.DELEGATE_REQUIRED_CONSENT_HOLD_CLOSING}
                        &nbsp;
                        <span>
                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_DELEGATE_REQUIRED_CONSENT_HOLD_CLOSING}>
                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                            </LinkWithTooltip>
                        </span>      
                    </div></div>

                    <div className="gpDelegateHeadaerTitle width100Px text-left"><div className="closingText"><p>Status</p>   
                    </div></div>

                    <div className="gpDelegateHeadaerTitle width210 lastChildWidth text-left paddingLeft15"><div className="closingText">
                        <p>Actions</p>
                    </div></div>

                    
                </div>
                <div className={"userContainer " + (this.state.getGpDelegatesList.length ===0 ? 'borderNone' : '')} style={{'width':'1050px'}}>
                    {this.state.getGpDelegatesList.length >0 ?
                        this.state.getGpDelegatesList.map((record, index)=>{
                            return(
                                <div className="userRow" key={index}>
                                    <label className="userImageAlt">
                                    {
                                        record['profilePic']  ?
                                        <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                        : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                    }
                                    </label>
                                    <div className="fund-user-name">{this.FsnetUtil.getFullName(record)}</div>
                                    <div className="gpDelegateCheckbox paddingLeft30">
                                        <CBox checked={record['selected']} onChange={(e) => this.handleInputChangeEvent(e,'user',{record})}>
                                            <span className="checkmark"></span>
                                        </CBox>
                                    </div>
                                    <div className="gpDelegateCheckbox paddingLeft30">
                                        <CBox checked={record['gPDelegateRequiredConsentHoldClosing']} onChange={(e) => this.handleInputChangeEvent(e,'gPDelegateRequiredConsentHoldClosing',{record})}>
                                            <span className="checkmark"></span>
                                        </CBox>
                                    </div>

                                    {
                                        record.status ? 
                                            <div className="gpDelegateCheckbox statesText width100Px pr10">
                                                <a className="addLink" onClick={this.userAction(record,3)}>{this.FundUtil._actionStatusNameMask(record.status)}</a>
                                                <h6>{this.FsnetUtil.dateFormat(record.updatedAt)}</h6>
                                            </div>
                                            :
                                            <div className="gpDelegateCheckbox statesText width100Px">
                                                <button className="statusMgsBtn disabled"><i className="fa fa-envelope-o"></i></button>
                                            </div>
                                    }
                                        
                                    <div className="gpDelegateCheckbox actionsWidth">
                                        <button title={this.Constants.EDIT_GP_DELEGATE_TITLE} className="actionsPencilBtn" onClick={this.editUser(record)}><i className="fa fa-pencil"></i></button>
                                        <button className="actionsEnvelopeBtn" title={this.Constants.RESEND_EMAIL_TITLE} onClick={this.userAction(record,2)}><i className="fa fa-envelope-o"></i></button>
                                        <CopyToClipboard text={`${this.Constants._registerLink}${record.emailConfirmCode}`}
                                            onCopy={this.userAction(record,1)}>
                                            <button title={this.Constants.COPY_LINK_TITLE} className={"actionsLinkBtn "+(!record['copyLink'] && 'disabled')}><i className="fa fa-link"></i></button>
                                        </CopyToClipboard>
                                        <button title={this.Constants.DELETE_GP_DELEGATE_TITLE} className="actionsTimesBtn" onClick={this.deleteUser(record)}><i className="fa fa-times"></i></button>
                                    </div>
                                </div>
                            );
                        })
                        :
                        <div className="title margin20 text-center">{this.Constants.NO_GP_DELEGATES}</div>
                    } 
                </div>
                <div className="error">{this.state.gpDelegateScreenError}</div>
                <div className="addRoleModal" hidden={!this.state.showAddGpDelegateModal}>
                    <div>
                        <div className="croosMarkStyle"><span className="cursor-pointer" onClick={this.closeGpDelegateModal}>x</span></div>
                        <h3 className="title">{this.state.jsonData.ADD_DELEGATE}</h3>
                    </div>
                    <div className="subtext modal-subtext">{this.state.jsonData.FILL_FORM_ADD_NEW_DELEGATE}</div>         <div className="form-main-div">                  
                        <Row className="marginBot20">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <label className="form-label">{this.state.jsonData.FIRST_NAME}*</label>
                                <FormControl type="text" name="firstName" placeholder={this.state.jsonData.PLACEHOLDER_FIRST_NAME} className={"inputFormControl " + (this.state.firstNameBorder ? 'inputError' : '')} value= {this.state.firstName} onChange={(e) => this.handleInputChangeEvent(e,'firstName')} onBlur={(e) => this.handleInputChangeEvent(e,'firstName')}/>   
                                <span className="error">{this.state.firstNameMsz}</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <label className="form-label">{this.state.jsonData.LAST_NAME}*</label>
                                <FormControl type="text" name="lastName" placeholder={this.state.jsonData.PLACEHOLDER_LAST_NAME} className={"inputFormControl " + (this.state.lastNameBorder ? 'inputError' : '')} value= {this.state.lastName} onChange={(e) => this.handleInputChangeEvent(e,'lastName')} onBlur={(e) => this.handleInputChangeEvent(e,'lastName')}/>   
                                <span className="error">{this.state.lastNameMsz}</span>
                            </Col>
                        </Row> 
                        <Row className="marginBot20">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <label className="form-label">{this.state.jsonData.EMAIL_ADDRESS}*</label>
                                <FormControl type="email" name="email" placeholder={this.state.jsonData.PLACEHOLDER_EMAIL_ADDRESS} className={"inputFormControl " + (this.state.emailBorder ? 'inputError' : '')} value= {this.state.email} onChange={(e) => this.handleInputChangeEvent(e,'email')} onBlur={(e) => this.handleInputChangeEvent(e,'email')}/>   
                                <span className="error">{this.state.emailMsz}</span>            
                            </Col>
                        </Row> 
                        <div className="error">{this.state.gpDelegateErrorMsz}</div>
                    </div>
                    <Row>
                        <Col lg={6} md={6} sm={6} xs={12}>
                        <Button type="button" className="fsnetCancelButton" onClick={this.closeGpDelegateModal}>{this.state.jsonData.CANCEL}</Button>
                        </Col>
                        <Col lg={6} md={6} sm={6} xs={12}>
                        <Button className={"fsnetSubmitButton "+ (this.state.isGpDelgateFormValid ? 'btnEnabled' : '') } disabled={!this.state.isGpDelgateFormValid} onClick={this.addDelegateFn}>{this.state.jsonData.SUBMIT}</Button>
                        </Col>
                    </Row> 
                </div>
                {
                    ['Open','New-Draft','Admin-Draft'].indexOf(this.state.fundStatus) > -1 ?
                    <div className="footer-nav footerNavStep2">
                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                        <i className="fa fa-chevron-right" onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                    :
                        ['GP','FSNETAdministrator','SecondaryGP'].indexOf(this.FsnetUtil.getUserRole()) > -1 &&
                            <div className="footer-nav"> 
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.proceedToNext}>{this.state.jsonData.SAVE_CHANGES}</Button>
                            </div>
                }
               
                <Loader isShow={this.state.showModal}></Loader>
                <Modal id="confirmFundModal" backdrop="static" show={this.state.showDelegateModal}  onHide={this.closeDelegateModal} dialogClassName="confirmFundDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="title-md ulpoadSignText">Would you like to send email invitation(s) to the delegate(s) you have selected?</div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width250 marginLeft30" onClick={this.navigateToNextScreen}>No - Do not Send</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width250" onClick={this.proceedToNext}>Yes - Send Invitation</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                {
                    this.state.showUserModal &&
                    <UserModalComponent userdata={{...this.state.userObj}} closeUser={this.closeUserModalComponent} updateUserData={this.updateUser} fundId={this.state.fundId}> </UserModalComponent>
                }
                {
                        this.state.showActions &&
                        <ActionsComponent userdata={{...this.state.userObj}} closeAction ={this.closeActionComponent} fundId={this.state.fundId}> </ActionsComponent>
                    }
            </div>
            </div>
        );
    }
}

export default GpDelegatesComponent;




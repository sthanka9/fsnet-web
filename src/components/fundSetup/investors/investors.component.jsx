import React, { Component } from 'react';
import '../fundSetup.component.css';
import { Button, Checkbox as CBox, Row, Col, FormControl, Tooltip, OverlayTrigger, Modal, Table } from 'react-bootstrap';
import userDefaultImage from '../../../images/default_user.png';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetUtil } from '../../../util/util';
import { Fundhttp } from '../../../services/fundhttp';
import { FundUtil } from '../util/fundUtil';
import { Constants } from '../../../constants/constants';
import Loader from '../../../widgets/loader/loader.component';
import { reactLocalStorage } from 'reactjs-localstorage';
import PhoneInput from 'react-phone-number-input';
import ToastComponent from '../../toast/toast.component';
import plusImg from '../../../images/plus.svg';
import 'react-phone-number-input/rrui.css';
import 'react-phone-number-input/style.css';
import LpModalComponent from '../../lp/lpmodals/lpmodals.component';
import UserModalComponent from '../modals/userModal.component';
import ActionsComponent from '../fundActions/fundActions';
import { PubSub } from 'pubsub-js';
import { CopyToClipboard } from 'react-copy-to-clipboard';
var _ = require('lodash');

var close = {}
class InvestorsComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.FundUtil = new FundUtil();
        this.Constants = new Constants();
        this.Fundhttp = new Fundhttp();
        this.state = {
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            showAddLpModal: false,
            showNameAsc: true,
            showOrgAsc: true,
            dateAsc: false,
            investmentAsc: true,
            getLpList: [],
            firmId: null,
            fundId: null,
            lpErrorMsz: '',
            lpSelectedList: [],
            lpScreenError: '',
            orgName: '',
            step5PageValid: false,
            selectedLpObj: {},
            noDelegatesMsz: '',
            fundStatus: 'Open',
            fundObj: {},
            jsonData: {},
            assignedLps: [],
            unAssignedLps: [],
            selectedLps: [],
            unSelectedLps: [],
            lpType: null,
            modalType: null,
            inviteConfirmModal: false,
            fundSetupModal: false
        }
        PubSub.subscribe('fundData', (msg, data) => {
            this.setState({
                fundId: data.id
            }, () => {
                this.showLps(data)
            })

        });

        PubSub.publish('pageNumber', { type: 'sideNav', page: 'lp' });

        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('lpProfileModal', data);
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    handleLpDelegateShow = (data) => () => {
        if (this.FundUtil._hasNullValue(this.Constants.FUND_FIELDS, this.state.fundObj)) {
            this.openFundSetupModal()
        } else {
            const name = this.FsnetUtil.getFullName(data);
            let fundObj = this.state.fundObj;
            fundObj['lpName'] = name
            fundObj['lpStatus'] = data.subscriptionstatus
            PubSub.publish('openModal', { obj: { id: data.subscriptionId, lpId: data.id, fundId: this.state.fundId, fundObj: fundObj, lpStatus: data.subscriptionstatus }, type: 'lpDelegate', isDelegateAddingFromFund: true });
        }
    }

    addLpBtn = (type) => () => {
        if (this.FundUtil._hasNullValue(this.Constants.FUND_FIELDS, this.state.fundObj)) {
            this.openFundSetupModal()
        } else {
            PubSub.publish('openLpModal', { fundObj: this.state.fundObj, type: type });
        }
    }

    editLpBtn = (lpObj, type) => () => {
        const value = lpObj.accountType == 'OfflineLP' ? true : false
        this.setState({
            lpType: type,
            selectedLpObj: lpObj,
            modalType: 'edit'
        }, () => {
            PubSub.publish('openLpEditModal', { fundObj: this.state.fundObj, lpObj: lpObj, type: type, isOfflineLP: value });
        })
    }

    deleteLp = (data, type) => () => {
        const value = data.accountType == 'OfflineLP' ? true : false
        this.setState({
            lpType: type,
            selectedLpObj: data,
            modalType: 'delete'
        }, () => {
            PubSub.publish('openLpDelFundModal', { data: this.state.fundObj, delegateId: data.id, type: type, subscriptionId: data.subscriptionId, isOfflineLP: value });
        })
    }

    deleteLpDelegate = (data, record) => () => {
        let fundObj = this.state.fundObj;
        fundObj['fundId'] = this.state.fundId;
        fundObj['subscriptionId'] = record.subscriptionId;
        this.setState({
            selectedLpObj: data,
            modalType: 'delete'
        }, () => {
            PubSub.publish('openLpDelegateModal', { data: this.state.fundObj, delegateId: data.id, subscriptionId: record.subscriptionId, fundId: this.state.fundId, isDelegateDeletingFromFund: true });
        })
    }

    navigateToNextPage = () => {
        if (['Open', 'New-Draft', 'Admin-Draft'].indexOf(this.state.fundStatus) > -1) {
            this.props.history.push('/dashboard');
        } else {
            //Using window location because of pubsub issue.
            window.location.href = `/fund/view/${this.FsnetUtil._encrypt(this.state.fundId)}`;
        }
    }

    proceedToBack = () => {
        this.props.history.push('/fundSetup/upload/' + this.FsnetUtil._encrypt(this.state.fundId));
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    componentDidMount() {
        this.getJsonData();
        let firmId = reactLocalStorage.getObject('firmId');
        let fundId = this.FsnetUtil._getId();
        this.setState({
            fundId: fundId,
            firmId: firmId
        }, () => {
            this.getLPApi()
        })
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('assignInvestors').then(result => {
            this.setState({
                jsonData: result.data
            })
        });

    }

    getLPApi = () => {
        this.open();
        this.Fsnethttp.getFund(this.state.fundId).then(result => {
            this.close();
            PubSub.publish('fundData', result.data.data);
            const responseData = result.data.data;
            const sortArray = responseData.lps.fundLps.length > 0 ? this.FsnetUtil._sortArrayWithDate(responseData.lps.fundLps) : []
            responseData.lps.fundLps = sortArray;
            this.setState({
                fundObj: responseData,
                fundStatus: responseData.status,
                assignedLps: responseData.lps.fundLps,
                unAssignedLps: responseData.lps.firmLps,
            })
        })
            .catch(error => {
                this.close();
                this.setState({
                    noDelegatesMsz: this.Constants.NO_LPS
                })
            });
    }

    showLps = (data) => {
        this.setState({ fundObj: data, assignedLps: data.lps.fundLps, unAssignedLps: data.lps.firmLps, fundStatus: data.status, noDelegatesMsz: this.Constants.NO_LPS });
    }


    sortLp = (e, colName, sortVal) => {

        let firmId = this.state.firmId;
        let fundId = this.state.fundId;
        if (this.state.assignedLps && this.state.assignedLps.length > 1) {
            this.open();
            this.Fsnethttp.getLpSort(firmId, fundId, colName, sortVal).then(result => {
                if (result.data && result.data.data.length > 0) {
                    this.close();
                    this.setState({ assignedLps: result.data.data });
                    if (colName === 'firstName') {
                        if (sortVal === 'desc') {
                            this.setState({
                                showNameAsc: true
                            })
                        } else {
                            this.setState({
                                showNameAsc: false
                            })
                        }
                    } else if (colName === 'date') {
                        if (sortVal === 'desc') {
                            this.setState({
                                dateAsc: true
                            })
                        } else {
                            this.setState({
                                dateAsc: false
                            })
                        }
                    } else {
                        if (sortVal === 'desc') {
                            this.setState({
                                showOrgAsc: true
                            })
                        } else {
                            this.setState({
                                showOrgAsc: false
                            })
                        }
                    }

                } else {
                    this.close();
                    this.setState({
                        getLpList: [],
                        showNameAsc: false
                    })
                }

            })
                .catch(error => {
                    this.close();
                    this.setState({
                        getLpList: []
                    })

                });
        }
    }


    checkInviteModalToShow = () => {
        if (this.FundUtil._hasNullValue(this.Constants.FUND_FIELDS, this.state.fundObj)) {
            this.openFundSetupModal()
        } else {
            this.openInviteConfirmModal();
        }
    }

    inviteInvestors = () => () => {
        this.open();
        this.Fundhttp.sendAllLpInvitations(this.state.fundId).then(result => {
            this.close();
            this.closeInviteConfirmModal();
            this.setState({
                showToast: true,
                toastMessage: 'Fund Invitations have been sent to the Investors successfully.',
                toastType: 'success',
            }, () => {
                this.getLPApi();
            })
        })
            .catch(error => {
                this.close();
            });

    }

    closeToast(timed) {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    sortLpByDate = (sort) => () => {
        if (this.state.assignedLps.length > 1) {
            const sortData = sort == 0 ? this.FsnetUtil._sortArrayWithDateAsc(this.state.assignedLps) : this.FsnetUtil._sortArrayWithDate(this.state.assignedLps)
            this.setState({
                dateAsc: sort == 0 ? true : false,
                assignedLps: sortData
            })
        }

    }

    sortInvestmentByName = (sort, type) => () => {
        let sortData;
        if (this.state.assignedLps.length > 1) {
            if (type == 'firstName') {
                sortData = sort == 0 ? this.FsnetUtil._sortUsingfirstNameAsc(this.state.assignedLps) : this.FsnetUtil._sortUsingfirstNameDesc(this.state.assignedLps)
            } else {
                sortData = sort == 0 ? this.FsnetUtil._sortUsingtrackerTitleAsc(this.state.assignedLps) : this.FsnetUtil._sortUsingtrackerTitleDesc(this.state.assignedLps)
            }
            this.setState({
                assignedLps: sortData
            }, () => {
                if (type == 'firstName') {
                    this.setState({
                        firstNameAsc: sort == 0 ? true : false,
                    })
                } else {
                    this.setState({
                        investmentAsc: sort == 0 ? true : false,
                    })
                }
            })
        }

    }

    openInviteConfirmModal = () => {
        if (this.state.assignedLps.length > 0) {
            this.setState({
                inviteConfirmModal: true
            })
        } else {
            this.setState({
                lpScreenError: this.Constants.LP_REQUIRED
            })
        }
    }

    closeInviteConfirmModal = () => {
        this.setState({
            inviteConfirmModal: false
        })
    }

    handleSendInvitation = (data) => (event) => {
        const status = data.status != 'Invitation-Pending' ? true : event.target.checked;
        const assignedLpsTemp = [...this.state.assignedLps];
        const getIndex = assignedLpsTemp.findIndex(obj => obj.id == data.id)
        let selectedLp = assignedLpsTemp.filter(obj => obj.id == data.id);
        selectedLp[0]['isInvestorInvited'] = status ? 1 : 0
        assignedLpsTemp[getIndex] = selectedLp[0]
        this.setState({
            assignedLps: assignedLpsTemp
        }, () => {
            this.updateFundData()
        })
    }

    closeUserModalComponent = (data) => {
        if (data) {
            this.setState({
                showUserModal: false
            })
        }
    }


    closeActionComponent = (data) => {
        setTimeout(() => {
            this.setState({
                showActions: false
            }, () => {
                if (data) {
                    this.getLPApi();
                }
            })
        }, 1000);
    }


    /**
     * Update user data,
     * add selected value to edited user,
     * find index and splice the user and update the obj
     */

    updateUser = (data) => {
        let userObj = data.user;
        let list = [...this.state.assignedLps]
        //Edit user
        if (data.type === 2) {
            if (data.user.accountType !== 'LPDelegate') {
                let user = list.filter((obj) => obj.id === userObj.id);
                /**
                 * FirstName, LastName,MiddleName Email, Organization Name
                 */
                user[0]['email'] = userObj.email;
                user[0]['firstName'] = userObj.firstName;
                user[0]['lastName'] = userObj.lastName;
                user[0]['middleName'] = userObj.middleName;
                user[0]['organizationName'] = userObj.organizationName;
                if (data.user.accountType === 'OfflineLP') {
                    user[0]['cellNumber'] = userObj.cellNumber;
                }
                let index = _.findIndex(this.state.assignedLps, { 'id': userObj.id });
                list.splice(index, 1, user[0]);
                this.setState({
                    assignedLps: list
                })
            } else {
                let lpUser = list.filter((obj) => obj.subscriptionId === userObj.subscriptionId);
                if (lpUser[0]) {
                    let delegateList = lpUser[0].hasOwnProperty('lpDelegatesList') ? lpUser[0].lpDelegatesList : []
                    let lpDelegateUser = delegateList.filter((obj) => obj.id === userObj.id);
                    /**
                     * FirstName, LastName,MiddleName Email, Organization Name
                     */
                    lpDelegateUser[0]['email'] = userObj.email;
                    lpDelegateUser[0]['firstName'] = userObj.firstName;
                    lpDelegateUser[0]['lastName'] = userObj.lastName;
                    lpDelegateUser[0]['middleName'] = userObj.middleName;
                    let idx = _.findIndex(delegateList, { 'id': userObj.id });
                    delegateList.splice(idx, 1, lpDelegateUser[0]);
                }
                this.setState({
                    assignedLps: list
                })

            }
        } else if (data.type === 1) {
            //add user
            userObj['selected'] = true;
            list.unshift(userObj)
            this.setState({
                assignedLps: list
            })
        } else if (data.type === 3) {
            //Delete user
            let users = list.filter((obj) => obj.id !== userObj);
            this.setState({
                assignedLps: users
            })
        }

    }


    updateFundLps = (data) => {
        if (data) {
            let obj = { ...this.state.fundObj }
            let fundLps = obj.lps.fundLps;
            fundLps.unshift(...data)
            this.setState({
                fundObj: obj
            })
        }
    }


    editUser = (data, record) => () => {
        if (data.accountType === 'OfflineLP') {
            this.editLpBtn(data, 1)()
        } else {
            data['type'] = 2;
            if (record) {
                data['lp'] = record
            }
            this.setState({
                userObj: data,
                showUserModal: true
            })
        }
    }

    deleteUser = (data) => () => {
        data['type'] = 3;
        this.setState({
            userObj: data,
            showUserModal: true
        })
    }

    addUser = (isOfflineLP) => () => {
        if (this.FundUtil._hasNullValue(this.Constants.FUND_FIELDS, this.state.fundObj)) {
            this.openFundSetupModal()
        } else {
            let data = {};
            data['accountType'] = isOfflineLP ? 'OfflineLP' : 'LP';
            data['type'] = 1;
            this.setState({
                userObj: data,
                showUserModal: true
            })
        }
    }

    userAction = (data, type, lpData) => () => {
        data['type'] = type;
        data['lpData'] = lpData;
        this.setState({
            userObj: data,
            showActions: true
        })
    }

    openFundSetupModal = () => {
        this.setState({
            fundSetupModal: true
        })
    }

    closefundSetupModal = () => {
        this.setState({
            fundSetupModal: false
        })
    }


    render() {
        function LinkWithTooltip({ id, children, href, placement, tooltip }) {
            return (
                <OverlayTrigger
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement={placement ? placement : "right"}
                    delayShow={300}
                    delayHide={150}
                    rootClose
                >
                    <a href={href}>{children}</a>
                </OverlayTrigger>
            );
        }

        return (
            <div className="LpDelegatesContainer marginTop6">
                <h1 className="assignGp">{this.state.jsonData.ASSIGN_INVESTORS}</h1>
                <p className="Subtext marginBot20">{this.state.jsonData.SELECT_INVESTORS_LIST_BELOW}</p>
                <Button className="vanilla-button-md" onClick={this.addUser(false)}><i className="fa fa-plus paddingRight20"></i>{this.state.jsonData.INVESTOR}</Button>
                <Button className="vanilla-button-md marginLeft20" onClick={this.addLpBtn('offlineLp')}><i className="fa fa-plus paddingRight20"></i>Add Offline Investor</Button>
                <Button className="vanilla-button-md marginLeft20" onClick={this.checkInviteModalToShow}><i className="fa fa-plus paddingRight20"></i>Invite All</Button>
                <span className="paddingLeft5">
                    <LinkWithTooltip tooltip={this.Constants.INVITE_ALL} placement="top">
                        <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                    </LinkWithTooltip>
                </span>
                <h1 className="assignGp marginTop20" hidden={this.state.assignedLps.length == 0}>Assigned Investors</h1>
                {this.state.assignedLps.length > 0 &&
                    <Row className="width1100 marginTop10">
                        <div className="name-heading marginLeft75 width200 cursor" onClick={this.sortInvestmentByName(1, 'firstName')} hidden={!this.state.firstNameAsc}>
                            {this.state.jsonData.INVESTOR_NAME}
                            <i className="fa fa-sort-asc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading marginLeft75 width200 cursor" onClick={this.sortInvestmentByName(0, 'firstName')} hidden={this.state.firstNameAsc}>
                            {this.state.jsonData.INVESTOR_NAME}
                            <i className="fa fa-sort-desc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading width250 cursor" onClick={this.sortInvestmentByName(1)} hidden={!this.state.investmentAsc}>
                            Investment
                            <i className="fa fa-sort-asc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading width250 cursor" onClick={this.sortInvestmentByName(0)} hidden={this.state.investmentAsc}>
                            Investment
                            <i className="fa fa-sort-desc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading width150">
                            {this.state.jsonData.ORGANIZATION}
                        </div>
                        <div className="name-heading width100Px">
                            Status
                        </div>
                        <div className="name-heading width100Px">
                            Add Delegate
                        </div>
                        <div className="name-heading width200 text-center">
                            Actions
                        </div>
                    </Row>
                }

                {this.state.assignedLps.length > 0 &&
                    <div className={"userLPContainer " + (this.state.assignedLps.length === 0 ? 'borderNone' : 'marginTop10')} style={{ 'width': '1080px', 'maxHeight': '50vh', 'height': 'inherit' }}>
                        {(this.state.assignedLps.length > 0) &&
                            this.state.assignedLps.map((record, index) => {
                                return (
                                    <div>
                                        <div className="userRow lineHeight40" key={index}>
                                            <label className="userImageAlt">
                                                {
                                                    record['profilePic'] ?
                                                        <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                        : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                }
                                            </label>
                                            <div className="lp-name width200 relative">
                                                <div className="ellipsis" title={this.FsnetUtil.getFullName(record)}>{this.FsnetUtil.getFullName(record)}</div>
                                                <span className="accountTypeName">{record['status'] == 'Invitation-Pending' ? 'Investor - Pending Invitation' : this.FsnetUtil.getAccountTypeName(record['accountType'])}</span>
                                            </div>
                                            <div className="lp-name width250">{record['trackerTitle'] ? record['trackerTitle'] : '(New Investor Pending Input)'}</div>
                                            <div className="lp-name width150">{record['organizationName']}</div>
                                            {
                                                record.accountType === 'LP' ?
                                                    record.status ?
                                                        <div className="lp-name width100Px statesText paddingLeftZero text-center">
                                                            <a className="addLink" onClick={this.userAction(record, 3)}>{this.FundUtil._actionStatusNameMask(record.status)}</a>
                                                            <h6>{this.FsnetUtil.dateFormat(record.updatedAt)}</h6>
                                                        </div>
                                                        :
                                                        <div className="lp-name width100Px">
                                                            <button className="statusMgsBtn disabled"><i className="fa fa-envelope-o"></i></button>
                                                        </div>
                                                    :
                                                    <div className="lp-name width100Px">
                                                    </div>
                                            }
                                            <div className="lp-name width100Px">
                                                <img src={plusImg} alt="add" className="cursor paddingLeft10" onClick={this.handleLpDelegateShow(record)} />
                                            </div>
                                            <div className="lp-name width200 paddingLeft30 marginLeft15 text-center">
                                                <button title={this.Constants.EDIT_LP_TITLE} className="actionsPencilBtn paddingLeft10" onClick={this.editUser(record, record)}><i className="fa fa-pencil"></i></button>
                                                {
                                                    record.accountType === 'LP' &&
                                                    <button className="actionsEnvelopeBtn" title={this.Constants.RESEND_EMAIL_TITLE} onClick={this.userAction(record, 2)}><i className="fa fa-envelope-o"></i></button>
                                                }
                                                {
                                                    record.accountType === 'LP' &&
                                                    <CopyToClipboard text={`${this.Constants._registerLink}${record.emailConfirmCode}`}
                                                        onCopy={this.userAction(record, 1)}>
                                                        <button title={this.Constants.COPY_LINK_TITLE} className={"actionsLinkBtn " + (!record['copyLink'] && 'disabled')}><i className="fa fa-link"></i></button>
                                                    </CopyToClipboard>
                                                }
                                                <button title={this.Constants.DELETE_LP_TITLE} className="actionsTimesBtn" onClick={this.deleteLp(record, 1)}><i className="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                        {(record.lpDelegatesList && record.lpDelegatesList.length > 0) &&
                                            record.lpDelegatesList.map((delegate, delegateIndex) => {
                                                return (
                                                    <div>
                                                        <div className="userRow lineHeight40" key={delegateIndex}>
                                                            <label className="userImageAlt">
                                                                {
                                                                    delegate['profilePic'] ?
                                                                        <img src={delegate['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(delegate)} />
                                                                        : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(delegate)} />
                                                                }
                                                            </label>
                                                            <div className="lp-name width200 relative">
                                                                <div className="ellipsis" title={this.FsnetUtil.getFullName(delegate)}>{this.FsnetUtil.getFullName(delegate)}</div>
                                                                <span className="accountTypeName">{delegate['status'] == 'Invitation-Pending' ? 'Investor - Pending Invitation' : this.FsnetUtil.getAccountTypeName(delegate['accountType'])}</span>
                                                            </div>
                                                            <div className="lp-name width250 paddingLeft20">{delegate['trackerTitle'] ? delegate['trackerTitle'] : '(New Investor Pending Input)'}</div>
                                                            <div className="lp-name width150"></div>
                                                            {
                                                                ['Resend Email', 'Copy Link'].indexOf(delegate.status) > -1 ?
                                                                    <div className="lp-name width100Px statesText paddingLeftZero text-center">
                                                                        <a className="addLink" onClick={this.userAction(delegate, 3, record)}>{delegate.status === 'Copy Link' ? 'Link Copied' : 'Invited'}</a>
                                                                        <h6>{this.FsnetUtil.dateFormat(delegate.updatedAt)}</h6>
                                                                    </div>
                                                                    :
                                                                    <div className="lp-name width100Px">
                                                                        <button className="statusMgsBtn disabled"><i className="fa fa-envelope-o"></i></button>
                                                                    </div>
                                                            }
                                                            <div className="lp-name width100Px">
                                                            </div>
                                                            <div className="lp-name width200 paddingLeft30 marginLeft15">
                                                                <button className="actionsPencilBtn" title={this.Constants.EDIT_LPDELEGATE_TITLE} onClick={this.editUser(delegate, record)}><i className="fa fa-pencil"></i></button>
                                                                <button className="actionsEnvelopeBtn" title={this.Constants.RESEND_EMAIL_TITLE} onClick={this.userAction(delegate, 2, record)}><i className="fa fa-envelope-o"></i></button>
                                                                <CopyToClipboard text={`${this.Constants._registerLink}${delegate.emailConfirmCode}`}
                                                                    onCopy={this.userAction(delegate, 1, record)}>
                                                                    <button title={this.Constants.COPY_LINK_TITLE} className={"actionsLinkBtn " + (!delegate['copyLink'] && 'disabled')}><i className="fa fa-link"></i></button>
                                                                </CopyToClipboard>
                                                                <button title={this.Constants.DELETE_LP_DELEGATE_LINK} className="actionsTimesBtn" onClick={this.deleteLpDelegate(delegate, record)}><i className="fa fa-times"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                );
                                            })
                                        }
                                    </div>
                                );
                            })
                        }
                    </div>
                }

                {this.state.assignedLps.length == 0 && <div className="title minHeight100 marginTop50 text-center">{this.Constants.NO_LPS}</div>}

                <div className="error">{this.state.lpScreenError}</div>

                {
                    ['Open', 'New-Draft', 'Admin-Draft'].indexOf(this.state.fundStatus) > -1 ?
                        <div className="footer-nav">
                            <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                            <i className="fa fa-chevron-right" onClick={this.navigateToNextPage} aria-hidden="true"></i>

                        </div>
                        :
                        <div className="footer-nav">
                            <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.navigateToNextPage}>Proceed to Tracker</Button>
                        </div>
                }
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Modal id="GPDelModal" backdrop="static" show={this.state.inviteConfirmModal} onHide={this.closeInviteConfirmModal} className="customWidth">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Title> Invite All</Modal.Title>
                    <Modal.Body>
                        <div className="error-md marginBottom44">Warning: You are about to send email invitations to this Fund to ALL investors that have not yet been invited. To proceed, click "Send Invitations" below.</div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12} className="text-left">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeInviteConfirmModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.inviteInvestors(false)}>Send Invitations</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal backdrop="static" className="" show={this.state.fundSetupModal} onHide={this.closefundSetupModal} dialogClassName="investorForSign">
                    <Modal.Header closeButton />
                    <Modal.Body>
                        <label className="title-md marginBot12 width100">
                            All steps in Fund Setup must be completed before invite investors.  If not complete, display modal that Fund setup is not complete, cannot invite investors.
                        </label>
                        <Row className="marginTop10">
                            <Col lg={12} md={12} sm={12} xs={12} className="text-center">
                                <Button type="button" className="fsnetSubmitButton btnEnabled width100Px" onClick={this.closefundSetupModal}>Ok</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <LpModalComponent></LpModalComponent>
                {
                    this.state.showUserModal &&
                    <UserModalComponent userdata={{ ...this.state.userObj }} closeUser={this.closeUserModalComponent} updateUserData={this.updateUser} updateFundData={this.updateFundLps} fundId={this.state.fundId}> </UserModalComponent>
                }
                {
                    this.state.showActions &&
                    <ActionsComponent userdata={{ ...this.state.userObj }} closeAction={this.closeActionComponent} fundId={this.state.fundId}> </ActionsComponent>
                }
            </div>
        );
    }
}

export default InvestorsComponent;




import React, { Component } from 'react';
import '../fundSetup.component.css';
import { Button, Checkbox as CBox, Row, Col, FormControl, Tooltip, OverlayTrigger, Modal } from 'react-bootstrap';
import userDefaultImage from '../../../images/default_user.png';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetUtil } from '../../../util/util';
import { FundUtil } from '../util/fundUtil';
import { Constants } from '../../../constants/constants';
import Loader from '../../../widgets/loader/loader.component';
import UserModalComponent from '../modals/userModal.component';
import ActionsComponent from '../fundActions/fundActions';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';
import {CopyToClipboard} from 'react-copy-to-clipboard';
var _ = require('lodash');

var fundInfo = {}, pageInfo = {};
class GpSignatories extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.FundUtil = new FundUtil();
        this.Constants = new Constants();
        this.state = {
            fundStatus: 'Open',
            jsonData: {},
            accountType: this.FsnetUtil.getUserRole(),
            fundObj: {},
            gpSignatoriesList: [],
            noSigantoriesMsz: null,
            gpSignatoriesSelectedList: [],
            noOfSignaturesRequiredForClosing: 1,
            noOfSignaturesRequiredForCapitalCommitment: 1,
            noOfSignaturesRequiredForSideLetter: 1,
            noOfSignaturesRequiredForFundAmmendments: 1,
            signatoriesError: '',
            uploadFileName: null,
            uploadDocFile: {},
            fundObj: {},
            manualSignatoryObj: {},
            showGPModal: false,
            primaryGpObj: {},
            showSignatoryModal: false,
            gprequestid: null,
            showUserModal:false
        }
        fundInfo = PubSub.subscribe('fundData', (msg, data) => {
            this.updateGpSignatories(data)
        });
        pageInfo = PubSub.publish('pageNumber', { type: 'sideNav', page: 'gpSignatories' });
    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(fundInfo);
        PubSub.unsubscribe(pageInfo);
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    proceedToNext = () => {
        // let errorStatus = this.checkSettingsValidations();
        // if(!errorStatus) {
        if (this.state.gpSignatoriesSelectedList.length <= 3) {
            this.open();
            let postObj = { fundId: this.state.fundId, vcfirmId: this.state.firmId, gpSignatories: this.state.gpSignatoriesSelectedList }
            this.Fsnethttp.assignGpSignatoryToFund(postObj).then(result => {
                this.close();
                PubSub.publish('fundData', this.state.fundObj);
                this.saveSettingsForSigningGpSignatories();
            })
                .catch(error => {
                    this.close();
                    if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                        this.setState({
                            signatoriesError: error.response.data.errors[0].msg,
                        });
                    } else {
                        this.setState({
                            signatoriesError: this.Constants.INTERNAL_SERVER_ERROR,
                        });
                    }
                });
        } else {
            let status = this.checkOnlyThreeUsersSelected();
            if (!status) {
                this.navigateToNext();
            }
        }
        // }
    }

    checkSettingsValidations = () => {
        if (this.state.noOfSignaturesRequiredForClosing < this.state.noOfSignaturesRequiredForSideLetter) {
            this.setState({
                signatoriesError: this.Constants.SIDE_LETTER_SETTING_COUNT,
            });
            return true
        }
        return false
    }

    saveSettingsForSigningGpSignatories = () => {
        this.open();
        let postObj = { fundId: this.state.fundId, vcfirmId: this.state.firmId, noOfSignaturesRequiredForClosing: this.state.noOfSignaturesRequiredForClosing, noOfSignaturesRequiredForCapitalCommitment: this.state.noOfSignaturesRequiredForCapitalCommitment, noOfSignaturesRequiredForSideLetter: this.state.noOfSignaturesRequiredForSideLetter, noOfSignaturesRequiredForFundAmmendments: this.state.noOfSignaturesRequiredForFundAmmendments }
        this.Fsnethttp.signatorySettings(postObj).then(result => {
            this.navigateToNext();
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        signatoriesError: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        signatoriesError: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });
    }

    navigateToNext = () => {
        //IF GP then navigate to GP Delegate page else subscription edit page.
        if (['GP', 'FSNETAdministrator'].indexOf(this.FsnetUtil.getUserRole()) > -1) {
            this.props.history.push('/fundSetup/gpDelegate/' + this.FsnetUtil._encrypt(this.state.fundId));
        } else {
            this.props.history.push('/fundSetup/editSubForm/' + this.FsnetUtil._encrypt(this.state.fundId));
        }
    }

    checkOnlyThreeUsersSelected = (status) => {
        if (status) {
            alert(this.Constants.LIMIT_GP_SIGNATORIES)
        } else {
            if (this.state.gpSignatoriesSelectedList.length > 3) {
                this.setState({
                    signatoriesError: this.Constants.LIMIT_GP_SIGNATORIES
                })
                return true;
            }
            return false;
        }
    }

    proceedToBack = () => {
        this.props.history.push('/fundSetup/funddetails/' + this.FsnetUtil._encrypt(this.state.fundId));
    }

    componentDidMount() {
        if (['GP', 'FSNETAdministrator'].indexOf(this.FsnetUtil.getUserRole()) > -1) {
            this.getJsonData();
            let firmId = reactLocalStorage.getObject('firmId');
            let fundId = this.FsnetUtil._getId();
            this.setState({
                fundId: fundId,
                firmId: firmId
            }, () => {
                this.getGpListFromApi(fundId)
            })
        } else {
            this.props.history.push('/dashboard');
        }
    }

    getGpListFromApi = (fundId) => {
        this.open();
        this.Fsnethttp.getFund(fundId).then(result => {
            this.close();
            this.FsnetUtil.setLeftNavHeight();
            PubSub.publish('fundData', result.data.data);
            this.setState({
                fundObj: result.data.data,
                manualSignatoryObj: result.data.data.additionalSignatoryPages,
                noOfSignaturesRequiredForClosing: result.data.data.noOfSignaturesRequiredForClosing,
                noOfSignaturesRequiredForSideLetter: result.data.data.noOfSignaturesRequiredForSideLetter,
                noOfSignaturesRequiredForFundAmmendments: result.data.data.noOfSignaturesRequiredForFundAmmendments,
                noOfSignaturesRequiredForCapitalCommitment: result.data.data.noOfSignaturesRequiredForCapitalCommitment
            }, () => {
                this.updateGpSignatories(result.data.data)
            })
        })
            .catch(error => {
                this.close();
                this.setState({
                    gpSignatoriesList: [],
                    noSigantoriesMsz: this.Constants.NO_GP_SIGNATORIES
                })
            });
    }

    updateGpSignatories = (data) => {
        if (data && data.gpSignatories.length > 0) {
            this.setState({ fundObj: data, gpSignatoriesList: data.gpSignatories, fundStatus: data.status }, () => this.selectedMembersPushToList());
        } else {
            this.setState({
                gpSignatoriesList: [],
                fundObj: data,
                noSigantoriesMsz: this.Constants.NO_GP_SIGNATORIES
            })
        }
    }

    selectedMembersPushToList = () => {
        if (this.state.gpSignatoriesList.length > 0) {
            let list = [];
            for (let index of this.state.gpSignatoriesList) {
                if (index['selected'] === true) {
                    list.push(index['id'])
                }
                this.setState({
                    gpSignatoriesSelectedList: list,
                })
            }
        }
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('assignDelegates').then(result => {
            this.setState({
                jsonData: result.data
            })
        });

    }

    addSignatories = () => {
        if (this.state.gpSignatoriesSelectedList.length >= 3) {
            alert(this.Constants.LIMIT_GP_SIGNATORIES)
        } else {
            PubSub.publish('openGpSignatoryModal', this.state.fundObj);
        }
    }

    deleteSignatory = (e, id) => {
        PubSub.publish('openGPSignatoryDelFundModal', { data: this.state.fundObj, signatoryId: id });
    }

    handleInputChangeEvent = (event, obj) => {
        let getSelectedList = this.state.gpSignatoriesSelectedList;
        let selectedId = obj.id;
        let value = event.target.checked
        if (value) {
            if (getSelectedList.indexOf(selectedId) === -1) {
                if (this.state.gpSignatoriesSelectedList.length >= 3) {
                    value = false;
                    this.checkOnlyThreeUsersSelected(true)
                } else {
                    getSelectedList.push(selectedId);
                }
            }
            this.updateSelectedValueToList(obj.id, value);
        } else {
            var index = getSelectedList.indexOf(selectedId);
            if (index !== -1) {
                getSelectedList.splice(index, 1);
            }
            this.updateSelectedValueToList(obj.id, value)
        }
        this.setState({
            gpSignatoriesSelectedList: getSelectedList,
            noOfSignaturesRequiredForClosing: 1,
            noOfSignaturesRequiredForSideLetter: 1,
            noOfSignaturesRequiredForCapitalCommitment: 1,
            noOfSignaturesRequiredForFundAmmendments: 1
        })
    }

    updateSelectedValueToList = (id, value) => {
        if (this.state.gpSignatoriesList.length > 0) {
            let getList = this.state.gpSignatoriesList;
            for (let index of getList) {
                if (index['id'] === id && this.state.gpSignatoriesList.indexOf(index['id']) === -1) {
                    index['selected'] = value
                }
                this.setState({
                    gpSignatoriesList: getList
                })
            }
        }
    }

    handleDropdownChange = (type) => (event) => {
        let value = event.target.value;
        this.setState({
            [type]: value,
            signatoriesError: null
        })
    }

    uploadBtnClick = () => {
        document.getElementById('uploadBtn').click();
    }

    //Upload patnership document.
    handleChange = (event, type) => {
        let obj = event.target.files;
        let reader = new FileReader();
        if (obj && obj.length > 0) {
            this.uploadFile = obj[0];
            let sFileName = obj[0].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            if (sFileExtension !== 'pdf') {
                document.getElementById('uploadBtn').value = "";
                alert(this.Constants.UPLOAD_DOC_REQUIRED)
                return true;
            }
            //File 10 MB limit
            if (this.uploadFile.size <= this.Constants.SIZE_LIMIT) {
                this.setState({
                    uploadDocFile: obj[0],
                    uploadDocSize: (this.uploadFile.size / this.Constants.SIZE_MB).toFixed(2) + ' MB'
                }, () => {
                    this.showConfirmSignatureModal('upload');
                });
                reader.readAsDataURL(this.uploadFile);
                this.setState({
                    uploadFileName: obj[0].name,
                    manualSignatoryObj: { 'originalname': obj[0].name }
                });
            } else {
                document.getElementById('uploadBtn').value = "";
                alert('Fund document should be less than 10 MB.')
            }
        }
    }

    showConfirmSignatureModal = (type) => {
        this.setState({ showConfirmSignature: true, type: type })
    }

    closeConfirmSignatureModal = () => {
        if (this.state.type === 'upload') {
            document.getElementById('uploadBtn').value = "";
        }
        this.setState({ showConfirmSignature: false, manualSignatoryObj: this.state.type === 'upload' ? {} : this.state.manualSignatoryObj })
    }

    submitFn = () => {
        if (this.state.type === 'upload') {
            this.addManualSignFn()
        } else {
            this.removeManualSign();
        }
    }

    addManualSignFn = () => {
        this.open();
        //call the upload doc api
        var formData = new FormData();
        formData.append("fundId", this.state.fundId);
        formData.append("additionalSignatoryPages", this.state.uploadDocFile);
        this.Fsnethttp.uploadManualSignatureDocumentToFund(this.state.fundId, formData).then(result => {
            this.close();
            document.getElementById('uploadBtn').value = "";
            this.setState({ manualSignatoryObj: result.data.data.additionalSignatoryPages ? result.data.data.additionalSignatoryPages : {}, showConfirmSignature: false })
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        errorMessage: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        errorMessage: this.Constants.INTERNAL_SERVER_ERROR,
                    });

                }
            });
    }

    removeManualSign = () => {
        this.open();
        const postObj = { fundId: this.state.fundId }
        this.Fsnethttp.deleteManualSignatureDocumentToFund(postObj).then(result => {
            this.close();
            this.closeConfirmSignatureModal();
            this.setState({
                uploadDocFile: {},
                uploadFileName: null,
                manualSignatoryObj: null
            })
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        errorMessage: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        errorMessage: this.Constants.INTERNAL_SERVER_ERROR,
                    });

                }
            });
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('profileModal', data);
    }

    isPrimaryGPChange = (event, data) => {
        if (data.isEmailConfirmed) {
            if (event.target.checked) {
                this.setState({
                    primaryGpObj: data
                }, () => {
                    this.checkoruncheckPrimaryGp(false)
                    this.openGPModal();
                })
            }
        } else {
            const getList = this.state.gpSignatoriesList
            for (let index of getList) {
                if (index.id == data.id && data.accountType != 'SecondaryGP') {
                    index['isPrimaryGp'] = true;
                }
            }
            this.setState({
                gpSignatoriesList: getList
            })
        }
    }

    convertToGp = () => {
        const postObj = { fundId: parseInt(this.state.fundId), userId: this.state.primaryGpObj.id, vcfirmIdOld: parseInt(this.FsnetUtil.getFirmId()) }
        this.open();
        this.Fsnethttp.convertToGp(postObj).then(result => {
            this.close();
            this.setState({
                gprequestid: result.data.gprequestid
            })
            this.closeGPModal();
            this.openSignatoryModal();
        })
            .catch(error => {
                this.close();

            });
    }

    convertToSignatory = (status) => () => {
        let primaryGpObj = this.state.gpSignatoriesList.filter(obj => obj['isPrimaryGp'] == true)
        const postObj = { fundId: parseInt(this.state.fundId), oldGpId: primaryGpObj[0].id, gpId: this.state.primaryGpObj.id, status: status ? 1 : 0, gprequestid: this.state.gprequestid }
        this.open();
        this.Fsnethttp.convertToSignatory(postObj).then(result => {
            this.close();
            this.checkoruncheckPrimaryGp(true)
            this.closeSignatoryModal();
        })
            .catch(error => {
                this.close();

            });
    }

    checkoruncheckPrimaryGp = (value) => {
        for (let index of this.state.gpSignatoriesList) {
            if (index.id == this.state.primaryGpObj.id) {
                index['isPrimaryGp'] = value
            }
        }
    }

    openGPModal = () => { this.setState({ showGPModal: true }) }
    closeGPModal = () => { this.setState({ showGPModal: false }) }

    openSignatoryModal = () => { this.setState({ showSignatoryModal: true }) }
    closeSignatoryModal = () => { this.setState({ showSignatoryModal: false, primaryGpObj: {}, gprequestid: null }, () => { window.location.reload(); }) }

    getEmail = () => {
        const getGPObj = this.state.gpSignatoriesList.filter(obj => obj.accountType == 'GP');
        let email = '';
        if (getGPObj) {
            email = getGPObj[0].email
        }
        return email ? email : null
    }

    closeUserModalComponent = (data) => {
        if(data) {
            this.setState({
                showUserModal:false,
                userObj:{}
            })
        }
    }

    closeActionComponent = (data) => {
        setTimeout(() => {
            this.setState({
                showActions:false
            },()=>{
                if(data) {
                    this.getGpListFromApi(this.state.fundId);
                }
            })
        }, 1000);
    }

    /**
     * Update user data,
     * add selected value to edited user,
     * find index and splice the user and update the obj
     */

    updateUser = (data) => {
        let userObj = data.user;
        let list = [...this.state.gpSignatoriesList]
        //Edit user
        if(data.type === 2) {
            let user = list.filter((obj)=> obj.id === userObj.id);
            userObj['selected'] = user[0]['selected'];
            userObj['profilePic'] = user[0]['profilePic'];
            let index = _.findIndex(this.state.gpSignatoriesList, {'id':userObj.id});
            list.splice(index, 1, userObj);
            this.setState({
                gpSignatoriesList:list
            })
        } else if(data.type === 1){
            //add user
            userObj['selected'] = true;
            list.unshift(userObj)
            this.setState({
                gpSignatoriesList:list
            },()=>{
                this.selectedMembersPushToList()
            })
        } else if(data.type === 3){
            //Delete user
            let users = list.filter((obj)=> obj.id !== userObj);
            this.setState({
                gpSignatoriesList:users
            },()=>{
                this.selectedMembersPushToList()
            })
        }
    }


    editUser = (data) => () => {
        data['type'] = 2;
        this.setState({
            userObj:data,
            showUserModal:true
        })
    }

    deleteUser = (data) => () => {
        data['type'] = 3;
        this.setState({
            userObj:data,
            showUserModal:true
        })
    }

    addUser = () => {
        let data = {};
        data['accountType'] = 'SecondaryGP'
        data['type'] = 1;
        this.setState({
            userObj:data,
            showUserModal:true
        })
    }

    userAction = (data,type) => () => {
        data['accountType'] = 'SecondaryGP'
        data['type'] = type;
        this.setState({
            userObj:data,
            showActions:true
        })
    }

    render() {
        function LinkWithTooltip({ id, children, href, placement, tooltip }) {
            return (
                <OverlayTrigger
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement={placement ? placement : "right"}
                    delayShow={300}
                    delayHide={150}
                    rootClose
                >
                    <a href={href}>{children}</a>
                </OverlayTrigger>
            );
        }
        return (
            <div className="step2Class marginTop6">
                <div className="GpSignatoriesContainer marginLeft30" >
                    <h1 className="title">{this.state.jsonData.ASSIGN_SIGNATORIES}</h1>
                    <p className="subtext marginBottom20">{this.state.jsonData.GP_SIGNATORIES_CONTENT}</p>
                    {
                        (['GP', 'FSNETAdministrator'].indexOf(this.state.accountType) > -1) &&
                        <Button className="fsnetButton marginTop20" onClick={this.addUser}><i className="fa fa-plus"></i>{this.state.jsonData.SIGNATORY}</Button>
                    }

                    <Row className="marginTop30 " hidden={this.state.gpSignatoriesList.length === 0} style={{'width':'1000px'}}>
                        <div className="name-heading marginLeft75 width300">
                            Name
                    </div>
                        <div className="name-heading width130">
                            Primary Fund Manager
                    </div>
                        <div className="name-heading width170">
                            Enable for this Fund
                        <span className="paddingLeft5">
                                <LinkWithTooltip tooltip={this.state.jsonData.SELECT_TOOLTIP} placement="top">
                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                </LinkWithTooltip>
                            </span>
                        </div>

                        <div className="name-heading width150">
                            Status
                        </div>

                        <div className="name-heading width150 paddingLeft30">
                            Actions
                        </div>
                    </Row>

                    <div className={"tableContainer marginTop10 tableWidthHundred " + (this.state.gpSignatoriesList.length === 0 ? 'borderNone' : '')} style={{'width':'970px'}} >
                        {this.state.gpSignatoriesList.length > 0 ?
                            this.state.gpSignatoriesList.map((record, index) => {
                                return (
                                    <div className="userRow" key={index} hidden={this.getEmail() == record['email'] && !record['isPrimaryGp']}>
                                        <label className="userImageAlt">
                                            {
                                                record['profilePic'] ?
                                                    <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                    : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                            }
                                        </label>
                                        <div className="lp-name width320 relative">
                                            <div className="ellipsis">{this.FsnetUtil.getFullName(record)}</div>
                                            <span className="accountTypeName">{this.FsnetUtil.getAccountTypeName(record['accountType'])}</span>
                                        </div>
                                        <div className="width130 inline-block" >
                                            {
                                                (this.getEmail() != record['email'] || record['isPrimaryGp']) &&
                                                <CBox title={!record['isEmailConfirmed'] ? 'Account not yet activated' : ''} className="marginLeft10" disabled={this.getEmail() == record['email'] || record['accountType'] == 'GP'} key={record['isPrimaryGp']} checked={record['accountType'] == 'GP'} onChange={(e) => this.isPrimaryGPChange(e, record)}>
                                                    <span className="checkmark"></span>
                                                </CBox>
                                            }
                                        </div>
                                        <div className="width100Px inline-block">
                                            {
                                                !record['isPrimaryGp'] &&
                                                <CBox className="marginLeft10" key={record['selected']} checked={record['selected']} disabled={record['isPrimaryGp']} onChange={(e) => this.handleInputChangeEvent(e, record)}>
                                                    <span className="checkmark"></span>
                                                </CBox>
                                            }
                                        </div>
                                        {
                                            record.status ? 
                                                <div className={"width100Px marginLeft40Pr inline-block "+(record['isPrimaryGp'] ? 'visibilityHidden' : '')}>
                                                    <a className="addLink" onClick={this.userAction(record,3)}>{this.FundUtil._actionStatusNameMask(record.status)}</a>
                                                    <h6>{this.FsnetUtil.dateFormat(record.updatedAt)}</h6>
                                                </div>
                                                :
                                                <div className={"width100Px marginLeft7Pr inline-block "+(record['isPrimaryGp'] ? 'visibilityHidden' : '')}>
                                                    <button className="statusMgsBtn disabled"><i className="fa fa-envelope-o"></i></button>
                                                </div>
                                        }
                                        
                                        <div className={"marginLeft40 inline-block "+(record['isPrimaryGp'] ? 'visibilityHidden' : '')}>
                                            <button title={this.Constants.EDIT_GP_SIGNATORY_TITLE} className={"actionsPencilBtn "+(record['isPrimaryGp'] ?'visibilityHidden' : '')} onClick={this.editUser(record)}><i className="fa fa-pencil"></i></button>
                                            <button className="actionsEnvelopeBtn" title={this.Constants.RESEND_EMAIL_TITLE} onClick={this.userAction(record,2)}><i className="fa fa-envelope-o"></i></button>
                                            <CopyToClipboard text={`${this.Constants._registerLink}${record.emailConfirmCode}`}
                                                onCopy={this.userAction(record,1)}>
                                                <button title={this.Constants.COPY_LINK_TITLE} className={"actionsLinkBtn "+(!record['copyLink'] && 'disabled')}><i className="fa fa-link"></i></button>
                                            </CopyToClipboard>
                                            <button title={this.Constants.DELETE_GP_SIGNATORY_TITLE} className={"actionsTimesBtn "+(record['isPrimaryGp'] ? 'visibilityHidden' : '')} onClick={this.deleteUser(record)}><i className="fa fa-times"></i></button>
                                        </div>

                                    </div>
                                );
                            })
                            :
                            <div className="title margin20 text-center">{this.Constants.NO_GP_SIGNATORIES}</div>
                        }
                    </div>

                    <div>
                        <h1 className="subtext">Please select the required quantity of Signatories for designated process.</h1>
                        <div>
                            <label className="marginRight20 width300 subtext text-right paddingRight15">Closing (including Side Letters):</label>
                            <FormControl name='filterFund' defaultValue={1} className="selectFormControl width100Px inline-block" value={this.state.noOfSignaturesRequiredForClosing} componentClass="select" onChange={this.handleDropdownChange('noOfSignaturesRequiredForClosing')}>
                                <option value={1}>1</option>
                                {this.state.gpSignatoriesSelectedList.map((record, index) => {
                                    return (
                                        <option value={index + 2} key={index}>{index + 2}</option>
                                    );
                                })}
                            </FormControl>
                        </div>
                        <div>
                            <label className="marginRight20 width300 subtext text-right paddingRight15">Capital Commitment Increase:</label>
                            <FormControl name='filterFund' defaultValue={1} className="selectFormControl width100Px inline-block" value={this.state.noOfSignaturesRequiredForCapitalCommitment} componentClass="select" onChange={this.handleDropdownChange('noOfSignaturesRequiredForCapitalCommitment')}>
                                <option value={1}>1</option>
                                {this.state.gpSignatoriesSelectedList.map((record, index) => {
                                    return (
                                        <option value={index + 2} key={index}>{index + 2}</option>
                                    );
                                })}
                            </FormControl>
                        </div>
                        <div>
                            <label className="marginRight20 width300 subtext text-right paddingRight15">Side Letters (issued post-closing):</label>
                            <FormControl name='filterFund' defaultValue={1} className="selectFormControl width100Px inline-block" value={this.state.noOfSignaturesRequiredForSideLetter} componentClass="select" onChange={this.handleDropdownChange('noOfSignaturesRequiredForSideLetter')}>
                                <option value={1}>1</option>
                                {this.state.gpSignatoriesSelectedList.map((record, index) => {
                                    return (
                                        <option value={index + 2} key={index}>{index + 2}</option>
                                    );
                                })}
                            </FormControl>
                        </div>
                        <div>
                            <label className="marginRight20 width300 subtext text-right paddingRight15">Effectuate Fund Agreement Amendments:</label>
                            <FormControl name='filterFund' defaultValue={1} className="selectFormControl width100Px inline-block" value={this.state.noOfSignaturesRequiredForFundAmmendments} componentClass="select" onChange={this.handleDropdownChange('noOfSignaturesRequiredForFundAmmendments')}>
                                <option value={1}>1</option>
                                {this.state.gpSignatoriesSelectedList.map((record, index) => {
                                    return (
                                        <option value={index + 2} key={index}>{index + 2}</option>
                                    );
                                })}
                            </FormControl>
                        </div>
                    </div>


                    <div className="error marginTop20">{this.state.signatoriesError}</div>


                    <h1 className="title marginTop10">{this.state.jsonData.ADD_Manual}</h1>
                    <p className="subtext marginBottom20">{this.state.jsonData.ADD_Manual_DESC}</p>
                    <div className="marginTop20 width600 flex">
                        <input type="file" id="uploadBtn" className="hide" onChange={(e) => this.handleChange(e)} />
                        <div className="width300 inline-block">
                            {
                                this.state.manualSignatoryObj && this.state.manualSignatoryObj.originalname ?
                                    <div className="upload-doc-name width300 text-ellipsis marginTop10" title={this.state.manualSignatoryObj.originalname}>{this.state.manualSignatoryObj.originalname} </div>
                                    :
                                    <div>
                                        <Button className="fsnetSubmitButton btnEnabled adminButton" onClick={this.uploadBtnClick}>Add Additional Page(s)</Button>
                                    </div>
                            }
                        </div>
                        <div className="width300 marginLeft30">
                            {
                                this.state.manualSignatoryObj && this.state.manualSignatoryObj.originalname ?
                                    <Button className="fsnetSubmitButton btnEnabled adminButton" disabled={!this.state.manualSignatoryObj || this.state.manualSignatoryObj == {}} onClick={() => this.showConfirmSignatureModal('delete')}>Delete Additional Page(s)</Button>
                                    :
                                    <Button className="fsnetSubmitButton btnEnabled adminButton" disabled>Delete Additional Page(s)</Button>
                            }
                        </div>
                    </div>



                    {
                        ['Open', 'New-Draft', 'Admin-Draft'].indexOf(this.state.fundStatus) > -1 ?
                            <div className="footer-nav footerNavStep2 marginTop20">
                                <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                                <i className="fa fa-chevron-right" onClick={this.proceedToNext} aria-hidden="true"></i>
                            </div>
                            :
                            this.state.accountType === 'GP' &&
                            <div className="footer-nav marginTop20">
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.proceedToNext}>{this.state.jsonData.SAVE_CHANGES}</Button>
                            </div>

                    }
                    <div className="error">{this.state.errorMessage}</div>
                    <Loader isShow={this.state.showModal}></Loader>
                    <Modal id="confirmFundModal" backdrop="static" show={this.state.showConfirmSignature} onHide={this.closeConfirmSignatureModal} dialogClassName="confirmFundDialog">
                        <Modal.Header className="headerNone" closeButton>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="title ulpoadSignText">Note: Any changes will impact future closings only. Documents already in the Document Locker will not be altered.</div>
                            <Row className="fundBtnRow">
                                <Col lg={6} md={6} sm={6} xs={12}>
                                    <Button type="button" className="fsnetCancelButton" onClick={this.closeConfirmSignatureModal}>Cancel</Button>
                                </Col>
                                <Col lg={6} md={6} sm={6} xs={12}>
                                    <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.submitFn}>Proceed</Button>
                                </Col>
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal id="confirmFundModal" backdrop="static" show={this.state.showGPModal} onHide={this.closeGPModal} dialogClassName="confirmFundDialog">
                        <Modal.Header className="headerNone" closeButton>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="title-md ulpoadSignText">Are you sure you want to make the selected Fund Manager the Primary Signatory? This will have the effect of making the selected Signatory the Primary Signatory for the Fund.</div>
                            <Row className="fundBtnRow">
                                <Col lg={6} md={6} sm={6} xs={12}>
                                    <Button type="button" className="fsnetCancelButton" onClick={this.closeGPModal}>Cancel</Button>
                                </Col>
                                <Col lg={6} md={6} sm={6} xs={12}>
                                    <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.convertToGp}>Proceed</Button>
                                </Col>
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Modal id="confirmFundModal" backdrop="static" show={this.state.showSignatoryModal} onHide={this.closeSignatoryModal} dialogClassName="confirmFundDialog">
                        <Modal.Header className="headerNone" closeButton>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="title-md ulpoadSignText">Would you like to make the original Primary Signatory a Secondary Signatory for {this.state.fundObj.legalEntity}? This will allow for the original Primary Signatory to remain a Signatory for the Fund. Click Proceed to have the user remain on the Fund as a Secondary Signatory, or click Delete to remove them from the Fund entirely.</div>
                            <Row className="fundBtnRow">
                                <Col lg={6} md={6} sm={6} xs={12}>
                                    <Button type="button" className="fsnetCancelButton" onClick={this.convertToSignatory(false)}>Delete</Button>
                                </Col>
                                <Col lg={6} md={6} sm={6} xs={12}>
                                    <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.convertToSignatory(true)}>Proceed</Button>
                                </Col>
                            </Row>
                        </Modal.Body>
                    </Modal>
                    {
                        this.state.showUserModal &&
                        <UserModalComponent userdata={{...this.state.userObj}} closeUser={this.closeUserModalComponent} updateUserData={this.updateUser} fundId={this.state.fundId}> </UserModalComponent>
                    }
                    {
                        this.state.showActions &&
                        <ActionsComponent userdata={{...this.state.userObj}} closeAction ={this.closeActionComponent} fundId={this.state.fundId}> </ActionsComponent>
                    }
                </div>
            </div>
        );
    }
}

export default GpSignatories;




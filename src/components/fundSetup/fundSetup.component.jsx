
import React, { Component } from 'react';
import './fundSetup.component.css';
import { Row } from 'react-bootstrap';
import { reactLocalStorage } from 'reactjs-localstorage';
import { FsnetAuth } from '../../services/fsnetauth';
import { Route, Link, Switch, Redirect } from "react-router-dom";
import FundDetailsComponent from '../fundSetup/fundDetails/fundDetails.component';
import GpSignatoriesComponent from '../fundSetup/gpSignatories/gpSignatories.component';
import GpDelegatesComponent from './gpDelegates/gpDelegates.component';
import UploadFAComponent from './uploadFA/uploadFA.component';
import SAComponent from './SA/sa.component';
import InvestorsComponent from './investors/investors.component';
import ModalComponent from '../fundSetup/modals/modals.component';
import HeaderComponent from '../header/header.component';
import { Fsnethttp } from '../../services/fsnethttp';
import { FsnetUtil } from '../../util/util';
import { FundUtil } from './util/fundUtil';
import userDefaultImage from '../../images/default_user.png';
import homeImage from '../../images/home.png';
import fundImage from '../../images/picture.png';
import successImage from '../../images/success-small.png';
import Loader from '../../widgets/loader/loader.component';
import { Constants } from '../../constants/constants';
import { PubSub } from 'pubsub-js';
import vanillaLogo from '../../images/Vanilla-white.png';

var fundInfo = {}, pageInfo = {};
class FundSetupComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.FundUtil = new FundUtil();
        this.state = {
            showSideNav: true,
            loggedInUserObj: [],
            getGpDelegatesList: [],
            getLpList: [],
            currentPage: 'funddetails',
            currentPageNumber: 1,
            totalPageCount: this.FsnetUtil.getUserRole() === 'GP' ? 7 : 6,
            fundId: null,
            firmId: null,
            createdFundDataObj: {},
            show: false,
            fundName: 'Create New Fund',
            fundImage: fundImage,
            showLeftNavLinks: false,
            startFundValid: false,
            gpDelegatesSelectedUsers: false,
            gpSignatoriesSelectedUsers: false,
            lpSelectedUsers: false,
            userType: this.FsnetUtil.getUserRole(),
            getGpSignatoriesList: [],
            showPromptModal:false
        }
        fundInfo = PubSub.subscribe('fundData', (msg, data) => {
            window.scrollTo(0, 0);
            this.setState({
                fundId: data.id
            }, () => {
                this.updateObjandNavLinks(data);
            })
        });
        pageInfo = PubSub.subscribe('pageNumber', (msg, data) => {
            this.getCurrentPageNumber(data.type, data.page)
        });

    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(fundInfo);
        PubSub.unsubscribe(pageInfo);
    }

    emitStartBtnModal = () => {
        PubSub.publish('startBtnEmit', true);
    }
    hamburgerClick = () => {
        if (this.state.showSideNav == true) {
            this.setState({
                showSideNav: false
            })
        } else {
            this.setState({
                showSideNav: true
            })
        }
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('profileModal', data);
    }

    navigateToTracker = (isDashboard) => () => {
        if(!this.state.fundId) {
            PubSub.publish('showPromptModal', {showPromptModal:true, redirectUrl: this.state.userType === 'FSNETAdministrator' ? "/adminDashboard/firmView" : "/dashboard"});
            // this.setState({
            //     showPromptModal:true,
            //     redirectUrl: this.state.userType === 'FSNETAdministrator' ? "/adminDashboard/firmView" : "/dashboard"
            // })
        } else {
            if(isDashboard) {
                let url = this.state.userType === 'FSNETAdministrator' ? "/adminDashboard/firmView" : "/dashboard";
                this.props.history.push(url)
            } else {
                window.location.href = '/fund/view/' + this.FsnetUtil._encrypt(this.state.fundId);
            }
        }
    }

    componentDidMount() {
        if (this.FsnetAuth.isAuthenticated()) {
            //Get user obj from local storage.
            let userObj = reactLocalStorage.getObject('userData');
            let firmId = reactLocalStorage.getObject('firmId');
            // var url = window.location.href;
            // var parts = url.split("/");
            // var urlSplitFundId = parts[parts.length - 1];
            var urlSplitFundId = this.FsnetUtil._getId();

            this.getCurrentPageNumber();
            if (userObj) {
                this.setState({
                    loggedInUserObj: userObj,
                    currentPage: urlSplitFundId,
                    firmId: firmId
                })
            }

            let fundId = this.FsnetUtil._getId();
            this.FsnetUtil.setLeftNavHeight();
            if (fundId !== 'funddetails' && fundId !== 'fundsetup') {
                this.setState({ fundId: fundId }, () => this.getFundDetails());
            }
            window.scrollTo(0, 0);
        } else {
            window.location = '/';
        }
    }

    getFundDetails = () => {
        let fundId = this.state.fundId
        if (fundId) {
            this.open();
            this.Fsnethttp.getFund(fundId).then(result => {
                this.close();
                this.FsnetUtil.setLeftNavHeight();
                if (result.data) {
                    let userData = JSON.parse(reactLocalStorage.get('userData'));
                    userData['vcfirmId'] = result.data.data.vcfirmId;
                    reactLocalStorage.set('userData', JSON.stringify(userData));
                    reactLocalStorage.set('firmId', result.data.data.vcfirmId);
                    this.updateObjandNavLinks(result.data.data)
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    updateObjandNavLinks = (data) => {
        this.setState({
            createdFundDataObj: data,
            fundId: data.id,
            fundName: data.legalEntity,
            fundImage: data.fundImage ? data.fundImage.url : fundImage,
            getGpDelegatesList: data.gpDelegates ? data.gpDelegates : [],
            getGpSignatoriesList: data.gpSignatories ? data.gpSignatories : [],
            getLpList: data.lps ? data.lps.fundLps : [],
            showLeftNavLinks: true,
            gpDelegatesSelectedUsers: false,
            gpSignatoriesSelectedUsers: false,
            lpSelectedUsers: data.lps && data.lps.fundLps.length > 0 ? true : false,
        }, () => {
            this.gpSelectedFromFund();
            // this.lpSelectedFromFund();
            this.FsnetUtil.setLeftNavHeight();
        })
    }


    //Check GP list selected true 
    gpSelectedFromFund = () => {
        for (let index of this.state.getGpDelegatesList) {
            if (index['selected'] === true) {
                this.setState({
                    gpDelegatesSelectedUsers: true
                })
            }
        }
        for (let index of this.state.getGpSignatoriesList) {
            if (index['selected'] === true) {
                this.setState({
                    gpSignatoriesSelectedUsers: true
                })
            }
        }
    }

    //Check lP list selected true 
    lpSelectedFromFund = () => {
        for (let index of this.state.getLpList) {
            if (index['selected'] === true) {
                this.setState({
                    lpSelectedUsers: true
                }, () => {
                    this.enableStartFundButton();
                })
            }
        }
    }



    enableStartFundButton = () => {
        if (this.state.fundId && this.state.createdFundDataObj.partnershipDocument !== null && this.state.lpSelectedUsers && this.state.createdFundDataObj.status === 'New-Draft' && this.state.currentPage === 'review') {
            this.setState({
                startFundValid: true
            })
        } else {
            this.setState({
                startFundValid: false
            })
        }
    }


    handleClose = () => {
        this.setState({ show: false });
    }

    handleShow = () => {
        this.props.history.push(`/fundSetup/investors/${this.FsnetUtil._encrypt(this.state.fundId)}`);
        // PubSub.publish('openLpModal', this.state.createdFundDataObj );
    }

    handleGpShow = () => {
        this.props.history.push(`/fundSetup/gpDelegate/${this.FsnetUtil._encrypt(this.state.fundId)}`);
        // PubSub.publish('openGpModal', this.state.createdFundDataObj);
    }

    deleteLp = (e, id) => {
        PubSub.publish('openLpDelModal', { data: this.state.createdFundDataObj, delegateId: id });
    }

    deleteGp = (e, id) => {
        PubSub.publish('openGpDelModal', { data: this.state.createdFundDataObj, delegateId: id });
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    // emitSubscriptionFormModal = (type)=>{
    //     PubSub.publish('editSubscriptionModal', type);
    // }

    getCurrentPageNumber = (type, fundPage) => {
        let page;
        if (type === 'sideNav') {
            page = fundPage
        } else {
            let url = window.location.href;
            let splitUrl = url.split('/fundSetup/');
            if (splitUrl[1] !== undefined) {
                page = splitUrl[1].split('/')[0];
            }
        }
        let number;
        if (page === 'funddetails') {
            number = 1;
        } else if (page === 'gpSignatories') {
            number = 2;
        } else if (page === 'gpDelegate') {
            number = 3;
        } else if (page === 'editSubForm') {
            number = 4;
        } else if (page === 'upload') {
            number = 5;
        } else if (page === 'lp') {
            number = 6;
        } else if (page === 'review') {
            number = 7;
        }
        this.setState({
            currentPageNumber: this.state.userType === 'GP' || number === 1 ? number : number - 1,
            currentPage: page
        }, () => {
            this.enableStartFundButton();
        })
    }

    fundCreate = (data) => {
        if(data.fundData) {
            this.updateObjandNavLinks(data.fundData)
        }
    }

    render() {
        const { match } = this.props;
        return (
            <div id="fundSetup">
                <nav className="navbar navbar-custom">
                    <div className="navbar-header">
                        <div className="sidenav">
                            <h1 className="text-left"><i className="fa fa-bars" aria-hidden="true" onClick={(e) => this.hamburgerClick()}></i>&nbsp; <img src={vanillaLogo} alt="vanilla" className="vanilla-logo" /></h1>
                        </div>
                    </div>
                    <div className="text-center navbar-collapse-custom" id="navbar-collapse" hidden={!this.state.showSideNav}>
                        <div className="sidenav" id="leftNavBar">
                            <h1 className="text-left logoHamburger"><i className="fa fa-bars" aria-hidden="true"></i><img src={vanillaLogo} alt="vanilla" className="vanilla-logo" /></h1>
                            <h2 className="text-left alignMargin"><img src={homeImage} alt="home_image" className="" />&nbsp; <a className="cursor" onClick={this.navigateToTracker(true)} >Dashboard</a></h2>
                            <h2 className="text-left marginTopNone" hidden={!this.state.fundId}><img src={this.state.fundImage} alt="home_image" className="" />&nbsp; <a className="cursor fontSizeOpacity" onClick={this.navigateToTracker(false)}>Fund Tracker</a></h2>
                            <div className="active-item text-left"><label className="fund-left-pic-label"><img src={this.state.fundImage} alt="fund_image" /></label>&nbsp;<div className="left-nav-fund-name text-left">Fund Setup</div></div>
                            {
                                this.state.fundId && this.state.showLeftNavLinks ?
                                    <ul className="sidenav-menu">
                                        <li>
                                            <Link to={"/fundSetup/funddetails/" + this.FsnetUtil._encrypt(this.state.fundId)} onClick={(e) => this.getCurrentPageNumber('sideNav', 'funddetails')} className={(this.state.currentPage === 'funddetails' ? 'active' : '')}>Fund Details
                                            {
                                                (!this.FundUtil._hasNullValue(this.Constants.FUND_SETUP_MANDATORY_1,this.state.createdFundDataObj) && this.FundUtil._hasValue(this.Constants.FUND_SETUP_MANDATORY_2,this.state.createdFundDataObj) && this.state.fundId) &&
                                                <span className="checkIcon"><img src={successImage} alt="successImage" /></span>
                                            }
                                            </Link>
                                        </li>
                                        {
                                            ['GP', 'FSNETAdministrator'].indexOf(this.FsnetUtil.getUserRole()) > -1 &&
                                            <li>
                                                <Link to={"/fundSetup/gpSignatories/" + this.FsnetUtil._encrypt(this.state.fundId)} onClick={(e) => this.getCurrentPageNumber('sideNav', 'gpSignatories')} className={(this.state.currentPage === 'gpSignatories' ? 'active' : '')}>Manage Signatories<span className="checkIcon"><img src={successImage} alt="successImage" hidden={!this.state.gpSignatoriesSelectedUsers} /></span></Link>
                                            </li>
                                        }
                                        {
                                            ['GP', 'FSNETAdministrator', 'SecondaryGP'].indexOf(this.FsnetUtil.getUserRole()) > -1 &&
                                            <li>
                                                <Link to={"/fundSetup/gpDelegate/" + this.FsnetUtil._encrypt(this.state.fundId)} onClick={(e) => this.getCurrentPageNumber('sideNav', 'gpDelegate')} className={(this.state.currentPage === 'gpDelegate' ? 'active' : '')}>Manage Delegates<span className="checkIcon" hidden={!this.state.gpDelegatesSelectedUsers}><img src={successImage} alt="successImage" /></span></Link>
                                            </li>
                                        }

                                        <li>
                                            <Link to={"/fundSetup/editSubForm/" + this.FsnetUtil._encrypt(this.state.fundId)} onClick={(e) => this.getCurrentPageNumber('sideNav', 'editSubForm')} className={(this.state.currentPage === 'editSubForm' ? 'active' : '')}>Subscription Agreement
                                        {
                                                    (this.state.createdFundDataObj.subscriptionAgreementPath || (this.state.createdFundDataObj.investorQuestions && this.state.createdFundDataObj.investorQuestions.length > 0)) &&
                                                    <span className="checkIcon"><img src={successImage} alt="successImage" /></span>
                                                }
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to={"/fundSetup/upload/" + this.FsnetUtil._encrypt(this.state.fundId)} onClick={(e) => this.getCurrentPageNumber('sideNav', 'upload')} className={(this.state.currentPage === 'upload' ? 'active' : '')}>Fund Agreement<span className="checkIcon" hidden={this.state.createdFundDataObj.partnershipDocument === null}><img src={successImage} alt="successImage" /></span></Link>
                                        </li>
                                        <li>
                                            <Link to={"/fundSetup/investors/" + this.FsnetUtil._encrypt(this.state.fundId)} onClick={(e) => this.getCurrentPageNumber('sideNav', 'lp')} className={(this.state.currentPage === 'lp' ? 'active' : '')}>Manage Investors<span className="checkIcon" hidden={!this.state.lpSelectedUsers}><img src={successImage} alt="successImage" /></span></Link>
                                        </li>
                                    </ul>

                                    :
                                    <ul className="sidenav-menu">
                                        <li><a className={(this.state.currentPage === 'funddetails' ? 'active' : '')}>Fund Details</a></li>
                                        {
                                            ['GP', 'FSNETAdministrator'].indexOf(this.FsnetUtil.getUserRole()) > -1 &&
                                            <li><a>Manage Signatories</a></li>
                                        }
                                        {
                                            ['GP', 'FSNETAdministrator', 'SecondaryGP'].indexOf(this.FsnetUtil.getUserRole()) > -1 &&
                                            <li><a>Manage Delegates</a></li>
                                        }
                                        <li><a>Subscription Agreement</a></li>
                                        <li><a>Fund Agreement</a></li>
                                        <li><a>Manage Investors</a></li>
                                    </ul>
                            }

                            <div className="section-head text-left"><span className="sectionHeadTxt">Delegates</span>
                                {
                                    ['GP', 'FSNETAdministrator', 'SecondaryGP'].indexOf(this.FsnetUtil.getUserRole()) > -1 &&
                                    <span className={"btn-add pull-right edit-btn-text " + (!this.state.fundId ? 'disabledAddIcon' : '')} onClick={this.handleGpShow}>Edit</span>
                                }
                            </div>
                            <div className="section">
                                <div className="gpDelDiv">
                                    {this.state.gpDelegatesSelectedUsers === true ?
                                        this.state.getGpDelegatesList.map((record, index) => {
                                            return (
                                                <div className="gpDelegateInfo" key={index} hidden={record['selected'] === false}>
                                                    <div className="dpDelImg">
                                                        {
                                                            record['profilePic'] ?
                                                                <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                                : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                        }
                                                    </div>
                                                    <div className="dpDelName" title={this.FsnetUtil.getFullName(record)}>{this.FsnetUtil.getFullName(record)}</div>
                                                </div>
                                            );
                                        })
                                        :
                                        <div className="user">
                                            <i className="fa fa-user fa-2x" aria-hidden="true"></i>
                                            <p className="opacity75">You haven’t added any Delegates to this Fund yet</p>
                                        </div>
                                    }
                                </div>
                            </div>

                            <div className="section-head text-left"><span className="sectionHeadTxt">Investors</span><span className={"btn-add pull-right edit-btn-text " + (!this.state.fundId ? 'disabledAddIcon' : '')} onClick={this.handleShow}>Edit</span></div>

                            <div className="section">
                                <div className="gpDelDiv">
                                    {this.state.lpSelectedUsers === true ?
                                        this.state.getLpList.map((record, index) => {
                                            return (
                                                <div className="gpDelegateInfo" key={index} hidden={record['selected'] === false}>
                                                    <div className="dpDelImg">
                                                        {
                                                            record['profilePic'] ?
                                                                <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                                : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                        }
                                                    </div>
                                                    <div className="dpDelName">{this.FsnetUtil.getFullName(record)}</div>
                                                </div>
                                            );
                                        })
                                        :
                                        <div className="user">
                                            <i className="fa fa-user fa-2x" aria-hidden="true"></i>
                                            <p className="opacity75">You haven’t added any Investors to this Fund yet</p>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>

                    </div>
                </nav>

                <div className="main">
                    <div>
                        <HeaderComponent ></HeaderComponent>
                    </div>
                    <div className="contentWidth">
                        <div className="main-heading">
                            <span className="main-title pl40">Fund Setup</span>
                            <a onClick={this.navigateToTracker(true)} className="cancel-fund cursor">Cancel</a>
                        </div>
                        <Row className="main-content">
                            <Switch>
                                <Route exact path={`${match.url}/funddetails`} component={FundDetailsComponent} />
                                <Route exact path={`${match.url}/funddetails/:id`} component={FundDetailsComponent}/>
                                <Route exact path={`${match.url}/gpSignatories/:id`} component={GpSignatoriesComponent} />
                                <Route exact path={`${match.url}/gpDelegate/:id`} component={GpDelegatesComponent} />
                                <Route exact path={`${match.url}/upload/:id`} component={UploadFAComponent} />
                                <Route exact path={`${match.url}/editSubForm/:id`} component={SAComponent} />
                                <Route exact path={`${match.url}/investors/:id`} component={InvestorsComponent} />
                                <Redirect from={`${match.url}/*`} to='/404' />
                                <Redirect from={`${match.url}/`} to='/404' />
                            </Switch>
                        </Row>
                    </div>
                    <Loader isShow={this.state.showModal}></Loader>
                </div>
                <ModalComponent></ModalComponent>

                {/* <Footer></Footer> */}
            </div>
        );
    }
}

export default FundSetupComponent;


import React, { Component } from 'react';
import '../fundSetup.component.css';
import { Button, Checkbox as CBox, Row, Col,Modal } from 'react-bootstrap';
import { Link } from "react-router-dom";
import staticImage from '../../../images/profilePic.jpg';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetUtil } from '../../../util/util';
import { FundUtil } from '../../fundSetup/util/fundUtil';

import { Constants } from '../../../constants/constants';
import Loader from '../../../widgets/loader/loader.component';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';
import userDefaultImage from '../../../images/default_user.png';
import FundImage from '../../../images/fund-default@2x.png';

class Step6Component extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.FundUtil = new FundUtil();
        this.Constants = new Constants();
        this.state = {
            getLpList: [],
            fundId: null,
            firmId: null,
            showNameAsc: true,
            showOrgAsc: true,
            showStartFundModal: false,
            startFundErrorMsz: '',
            currentFundDataObj: [],
            fundImage: FundImage,
            fundImageName: 'fund_Pic.jpg',
            documentLink:'',
            startFundValid: false,
            jsonData:{},
            userType:this.FsnetUtil.getUserRole(),
        }
        PubSub.subscribe('fundData', (msg, data) => {
            this.setState({
                fundId: data.id
            }, () => {
                this.getCurrentFundData();
            })
        });
        PubSub.subscribe('startBtnEmit', (msg, data) => {
            this.openStartFundModal();
        });
        PubSub.publish('pageNumber', {type:'sideNav', page: 'review'});
    }

    openDocument = () => {
        if(this.state.documentLink) {
            const docUrl = `${this.state.documentLink}?token=${this.FsnetUtil.getToken()}`
            this.FsnetUtil._openDoc(docUrl)
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.getJsonData();
        let firmId = reactLocalStorage.getObject('firmId');
        let fundId = this.FsnetUtil._getId();
        this.setState({
            fundId: fundId,
            firmId: firmId,
        }, () => this.getCurrentFundData());
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('reviewConfirmGP').then(result=>{
            if(result && result.data) {
                this.setState({
                    jsonData:result.data
                })
            }
        });
        
    }

    openStartFundModal = () => {
        if(this.state.userType === 'FSNETAdministrator') {
            this.adminSendEmail()
        } else {
            this.setState({
                showStartFundModal: true,
                startFundErrorMsz: ''
            })
        }
    }

    closeStartFundModal = () => {
        this.setState({
            showStartFundModal: false,
            startFundErrorMsz: ''
        })
    }

    getCurrentFundData = () => {
        this.open();
        let fundId = this.state.fundId;
        this.Fsnethttp.getFund(fundId).then(result => {
            this.close();
            this.FsnetUtil.setLeftNavHeight();
            if (result.data && result.data.data != undefined) {
                this.setState({
                    currentFundDataObj: result.data.data,
                    fundImage: result.data.data.fundImage ? result.data.data.fundImage.url : FundImage,
                    fundImageName: result.data.data.fundImage ? result.data.data.fundImage.originalname : 'fund_Pic.jpg',
                    documentLink: result.data.data.partnershipDocument ? result.data.data.partnershipDocument.url: '',
                    getLpList: result.data.data.lps ? result.data.data.lps : []
                },()=>{
                    this.enableStartFundButton();
                })
            }
        })
        .catch(error => {
            this.close();
        });
    }

    enableStartFundButton = () => {
        let selectedLps = this.state.currentFundDataObj.lps.fundLps;
        let isFundDetailsMandatory = this.Constants.FUND_SETUP_MANDATORY_1;
        let isAnyValueInGPCC = this.Constants.FUND_SETUP_MANDATORY_2;
        if (!this.FundUtil._hasNullValue(isFundDetailsMandatory,this.state.currentFundDataObj) && this.FundUtil._hasValue(isAnyValueInGPCC,this.state.currentFundDataObj) && this.state.fundId && this.state.currentFundDataObj.partnershipDocument !== null && selectedLps && selectedLps.length > 0 && (this.state.currentFundDataObj.status === 'New-Draft' || this.state.currentFundDataObj.status === 'Admin-Draft')) {
            this.setState({
                startFundValid: true
            })
        } else {
            this.setState({
                startFundValid: false
            })
        }
    }

    startBtnFn = () => {
        this.open();
        this.gpStartFund();
        
    }

    adminSendEmail = () => {
        let fundObj = { fundId: this.state.fundId }
        this.Fsnethttp.triggerAccountAndFundDetailsEmail(fundObj).then(result => {
            this.close();
            this.props.history.push('/dashboard');
        })
        .catch(error => {
            this.close();
        });
    }

    redirectToDashboard = () => {
        this.props.history.push('/dashboard');
    }

    gpStartFund = () => {
        let fundObj = { fundId: this.state.fundId }
        this.Fsnethttp.startFund(fundObj).then(result => {
            this.close();
            if (result) {
                this.props.history.push('/dashboard');
            }
        })
        .catch(error => {
            this.close();
            if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                this.setState({
                    startFundErrorMsz: error.response.data.errors[0].msg
                })
            }
        });
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    proceedToBack = () => {
        this.props.history.push('/fundSetup/investors/' + this.FsnetUtil._encrypt(this.state.fundId));
    }

    render() {
        return (
            <div className="step6Class marginTop6">
                <div className="step6ClassAboveFooter">
                    <div className="staticContent">
                        <h2 className="title marginBottom2">{this.state.jsonData.REVIEW_CONFIRM}</h2>
                        <h4 className="subtext marginBottom30">{this.state.jsonData.VERIFY_BEFORE_STARTING_YOUR_FUND}</h4>
                    </div>
                    <Row id="step6-rows1" >
                        <Col md={2} sm={2} xs={6} className="step6-col-pad">
                            <div className="col1">{this.state.jsonData.FUND_DETAILS}</div>
                        </Col>
                        <Col md={7} sm={7} xs={6}>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2">{this.state.jsonData.FUND_LEGAL_ENTITY_NAME}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent">{this.state.currentFundDataObj.legalEntity}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2">{this.state.jsonData.FUND_COMMON_NAME}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent">{this.state.currentFundDataObj.fundCommonName}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2">{this.state.jsonData.FUND_MANAGER_LEGAL_ENTITY_NAME}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent">{this.state.currentFundDataObj.fundManagerLegalEntityName}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2">{this.state.jsonData.FUND_MANAGER_COMMON_NAME}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent">{this.state.currentFundDataObj.fundManagerCommonName}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2">{this.state.jsonData.FUND_MANAGER_TITLE}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent">{this.state.currentFundDataObj.fundManagerTitle}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2" hidden={this.state.currentFundDataObj.fundHardCap === null}>{this.state.jsonData.HARD_CAP}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent" hidden={this.state.currentFundDataObj.fundHardCap === null}>{this.FsnetUtil.convertToCurrency(this.state.currentFundDataObj.fundHardCap)}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2" hidden={this.state.currentFundDataObj.fundTargetCommitment === null}>{this.state.jsonData.FUND_TARGET_COMMITMENT}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent" hidden={this.state.currentFundDataObj.fundTargetCommitment === null}>{this.FsnetUtil.convertToCurrency(this.state.currentFundDataObj.fundTargetCommitment)}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2">{this.state.jsonData.FUND_TYPE}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent">{this.state.currentFundDataObj.fundType == 1 ? 'U.S. Fund' : 'Non-U.S. Fund'}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2">{this.state.jsonData.HARD_CAP_APLLICATION_RULE}: </div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent">{this.state.currentFundDataObj.hardCapApplicationRule == 1 ? 'Investor Capital Commitments only' : 'Investor Capital Commitments plus the Fund Manager Capital Commitment in the aggregate.'}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 subtext margin5">{this.state.jsonData.GENERAL_PARTNER_COMMITMENT}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                {/* <div className="col2 subtext margin5">{this.state.jsonData.GENERAL_PARTNER_COMMITMENT}:</div> */}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2" hidden={this.state.currentFundDataObj.percentageOfLPCommitment == 0}>{this.state.jsonData.PERCENTAGE_INVESTOR_COMMITMENTS}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent" hidden={this.state.currentFundDataObj.percentageOfLPCommitment == 0}>{this.state.currentFundDataObj.percentageOfLPCommitment}%</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2" hidden={this.state.currentFundDataObj.percentageOfLPAndGPAggregateCommitment == 0}>{this.state.jsonData.PERCENTAGE_ALL_COMMITMENTS}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent" hidden={this.state.currentFundDataObj.percentageOfLPAndGPAggregateCommitment == 0}>{this.state.currentFundDataObj.percentageOfLPAndGPAggregateCommitment}%</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2" hidden={this.state.currentFundDataObj.capitalCommitmentByFundManager == 0}>{this.state.jsonData.FIXED_COMMITMENT_DOLLARS}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent" hidden={this.state.currentFundDataObj.capitalCommitmentByFundManager == 0}>{this.FsnetUtil.convertToCurrency(this.state.currentFundDataObj.capitalCommitmentByFundManager)}</div>
                                </Col>
                            </Row>
                            {/* <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2" hidden={this.state.currentFundDataObj.generalPartnersCapitalCommitmentindicated == 2}>{this.state.jsonData.FUND_MANAGER_CAPITAL_COMMITMENT_INDICATED}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent" hidden={this.state.currentFundDataObj.generalPartnersCapitalCommitmentindicated == 2}>{this.state.jsonData.FUND_MANAGER_CAPITAL_COMMITMENT_INDICATED_MINIMUM_AMOUNT_REVIEW}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2" hidden={this.state.currentFundDataObj.generalPartnersCapitalCommitmentindicated == 1}>{this.state.jsonData.FUND_MANAGER_CAPITAL_COMMITMENT_INDICATED}:</div>
                                </Col>
                                <Col md={6} sm={6} xs={6}>
                                <div className="col2 lightContent" hidden={this.state.currentFundDataObj.generalPartnersCapitalCommitmentindicated == 1}>{this.state.jsonData.FUND_MANAGER_CAPITAL_COMMITMENT_INDICATED_EXACT_AMOUNT_REVIEW}</div>
                                </Col>
                            </Row> */}
                            {/* <div className="col2">{this.state.jsonData.FUND_LEGAL_ENTITY_NAME}:  {this.state.currentFundDataObj.legalEntity}</div>
                            <div className="col2">{this.state.jsonData.FUND_COMMON_NAME}:  {this.state.currentFundDataObj.fundCommonName}</div>
                            <div className="col2">{this.state.jsonData.FUND_MANAGER_LEGAL_ENTITY_NAME}:</div>
                            <div className="col2">{this.state.currentFundDataObj.fundManagerLegalEntityName}</div>
                            <div className="col2">{this.state.jsonData.FUND_MANAGER_COMMON_NAME}:  {this.state.currentFundDataObj.fundManagerCommonName}</div>
                            <div className="col2">{this.state.jsonData.FUND_MANAGER_TITLE}:  {this.state.currentFundDataObj.fundManagerTitle}</div>
                            <div className="col2" hidden={this.state.currentFundDataObj.fundHardCap === null}>{this.state.jsonData.HARD_CAP}: {this.FsnetUtil.convertToCurrency(this.state.currentFundDataObj.fundHardCap)}</div>
                            <div className="col2" hidden={this.state.currentFundDataObj.fundTargetCommitment === null}>{this.state.jsonData.FUND_TARGET_COMMITMENT}: {this.FsnetUtil.convertToCurrency(this.state.currentFundDataObj.fundTargetCommitment)}</div>
                            <div className="col2">{this.state.jsonData.FUND_TYPE}: {this.state.currentFundDataObj.fundType == 1 ? 'U.S. Fund' : 'Non-U.S. Fund'}</div>
                            <div className="col2">{this.state.jsonData.HARD_CAP_APLLICATION_RULE}: </div>
                            <div className="col2">{this.state.currentFundDataObj.hardCapApplicationRule == 1 ? 'Investor Capital Commitments only' : 'Investor Capital Commitments plus the Fund Manager Capital Commitment in the aggregate.Investor Capital Commitments plus the Fund Manager Capital Commitment in the aggregate.'}</div>
                            <div className="col2 subtext margin5">{this.state.jsonData.GENERAL_PARTNER_COMMITMENT}:</div>
                            <div className="col2" hidden={this.state.currentFundDataObj.percentageOfLPCommitment == 0}>{this.state.jsonData.PERCENTAGE_INVESTOR_COMMITMENTS}: {this.state.currentFundDataObj.percentageOfLPCommitment}%</div>
                            <div className="col2" hidden={this.state.currentFundDataObj.percentageOfLPAndGPAggregateCommitment == 0}>{this.state.jsonData.PERCENTAGE_ALL_COMMITMENTS}: {this.state.currentFundDataObj.percentageOfLPAndGPAggregateCommitment}%</div>
                            <div className="col2" hidden={this.state.currentFundDataObj.capitalCommitmentByFundManager == 0}>{this.state.jsonData.FIXED_COMMITMENT_DOLLARS}: {this.FsnetUtil.convertToCurrency(this.state.currentFundDataObj.capitalCommitmentByFundManager)}</div>
                            <div className="col2" hidden={this.state.currentFundDataObj.generalPartnersCapitalCommitmentindicated == 2}>{this.state.jsonData.FUND_MANAGER_CAPITAL_COMMITMENT_INDICATED_MINIMUM_AMOUNT_REVIEW}</div>
                            <div className="col2" hidden={this.state.currentFundDataObj.generalPartnersCapitalCommitmentindicated == 1}>{this.state.jsonData.FUND_MANAGER_CAPITAL_COMMITMENT_INDICATED_EXACT_AMOUNT_REVIEW}</div> */}
                        </Col>
                        <Col md={2} sm={2} xs={6}>
                            <div className="col3 text-center">{this.state.jsonData.FUND_IMAGE}:</div>
                            <div className="col3 text-center"><img src={this.state.fundImage} alt="profile-pic" className="profile-pic" /></div>
                            {/* <div className="col3 text-center">{this.state.fundImageName}</div> */}
                        </Col>
                        <Col md={1} sm={1} xs={6}>
                            <span className="col4 col4GPReview"><Link to={"/fundSetup/funddetails/" + this.FsnetUtil._encrypt(this.state.fundId)}>{this.state.jsonData.CHANGE}</Link></span>
                        </Col>
                    </Row>
                    {/* Remaining rows items============ */}
                    <Row className="step6-rows" hidden={this.state.userType === 'GPDelegate' || this.state.userType === 'SecondaryGP'} >
                        <Col md={2} sm={2} xs={6} className="step-col-pad">
                            <span className="col1">{this.state.jsonData.SIGNATORIES}</span>
                        </Col>
                        <Col md={9} sm={9} xs={6}>
                            <span className="col2">{this.state.jsonData.REVIEW_SIGNATORIES_SIDEBAR}</span>
                        </Col>
                        <Col md={1} sm={1} xs={6}>
                            <span className="col4 col4GPReview"><Link to={"/fundSetup/gpSignatories/" + this.FsnetUtil._encrypt(this.state.fundId)}>{this.state.jsonData.CHANGE}</Link></span>
                        </Col>
                    </Row>
                    <Row className="step6-rows" hidden={this.state.userType === 'GPDelegate'}>
                        <Col md={2} sm={2} xs={6} className="step-col-pad">
                            <span className="col1">{this.state.jsonData.DELEGATES}</span>
                        </Col>
                        <Col md={9} sm={9} xs={6}>
                            <span className="col2">{this.state.jsonData.REVIEW_DELEGATES_SIDEBAR}</span>
                        </Col>
                        {/* <Col md={3} sm={3} xs={6}>
                            <span className="col3"></span>
                        </Col> */}
                        <Col md={1} sm={1} xs={6}>
                            <span className="col4 col4GPReview"><Link to={"/fundSetup/gpDelegate/" + this.FsnetUtil._encrypt(this.state.fundId)}>{this.state.jsonData.CHANGE}</Link></span>
                        </Col>
                    </Row>
                    <Row className="step6-rows" >
                        <Col md={2} sm={2} xs={6} className="step-col-pad">
                            <span className="col1">{this.state.jsonData.FUND_AGREEMENT}</span>
                        </Col>
                        <Col md={9} sm={9} xs={6}>
                            {
                                this.state.currentFundDataObj.partnershipDocument ?
                                <span className="col2"><a className="patnership-link" onClick={this.openDocument}>{this.state.jsonData.VIEW_FUND_DOCUMENTS}</a></span>:
                                <span className="col2">{this.state.jsonData.DOCUMENT_NOT_UPLOADED}</span>
                            }
                        </Col>
                        {/* <Col md={3} sm={3} xs={6}>
                            <span className="col3"></span>
                        </Col> */}
                        <Col md={1} sm={1} xs={6}>
                            <span className="col4 col4GPReview"><Link to={"/fundSetup/upload/" + this.FsnetUtil._encrypt(this.state.fundId)}>{this.state.jsonData.CHANGE}</Link></span>
                        </Col>
                    </Row>
                    <Row className="step6-rows" >
                        <Col md={2} sm={2} xs={6} className="step-col-pad">
                            <span className="col1">{this.state.jsonData.INVESTORS}</span>
                        </Col>
                        <Col md={9} sm={9} xs={6}>
                            <span className="col2">{this.state.jsonData.REVIEW_INVESTORS_SIDEBAR}</span>
                        </Col>
                        {/* <Col md={3} sm={3} xs={6}>
                            <span className="col3"></span>
                        </Col> */}
                        <Col md={1} sm={1} xs={6}>
                            <span className="col4 col4GPReview"><Link to={"/fundSetup/investors/" + this.FsnetUtil._encrypt(this.state.fundId)}>{this.state.jsonData.CHANGE}</Link></span>
                        </Col>
                    </Row>
                    <div className="staticTextBelowTable">
                        <div className="staticTextBelowText"> 
                            {
                                this.state.userType === 'FSNETAdministrator'? 
                                this.state.jsonData.ADMIN_SEND_EMAIL_GP
                                :
                                this.state.jsonData.ONCE_EVERYTHING_CONFIRMED_START_FUND
                            }
                    </div>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
                <div className="text-center">
                    {
                        this.state.userType === 'FSNETAdministrator'? 
                            <div className="start-fund marginRight20" onClick={this.redirectToDashboard}>{this.state.jsonData.SAVE_EXIT}</div>
                            :''
                    }
                    <div className={"start-fund " + (!this.state.startFundValid ? 'disableStartFund' : '')} onClick={this.openStartFundModal}><i className="fa fa-check strtFndChk" aria-hidden="true"></i>&nbsp;
                            {this.state.userType === 'FSNETAdministrator'? 'Send Email':'Invite Investors'}
                    </div>
                </div>
                <div className="footer-nav">
                    <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                    <i className="fa fa-chevron-right disabled" aria-hidden="true"></i>
                </div>
                <Modal id="GPDelModal" show={this.state.showStartFundModal}  onHide={this.closeStartFundModal}>
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Title> {this.state.jsonData.ARE_YOU_SURE_TO_START_FUND}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext">{this.state.jsonData.ALL_INVESTORS_INVITED_RECEIVE_EMAIL_NOTIFICATIONS}</div>
                        <div className="error">{this.state.startFundErrorMsz}</div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.closeStartFundModal}>{this.state.jsonData.NO_TAKE_ME_BACK}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.startBtnFn}>{this.state.jsonData.YES_START_THE_FUND}</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default Step6Component;




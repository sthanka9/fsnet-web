import React, { Component } from 'react';
import '../fundSetup.component.css';
import { Row, Modal } from 'react-bootstrap';
import Loader from '../../../widgets/loader/loader.component';
import { Fundhttp } from '../../../services/fundhttp';
import { FsnetUtil } from '../../../util/util';
import { Constants } from '../../../constants/constants';
import ToastComponent from '../../toast/toast.component';
import userDefaultImage from '../../../images/default_user.png';


class FundActions extends Component {

    constructor(props) {
        super(props);
        this.Fundhttp = new Fundhttp();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            user: {

            },
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            actionsHistoryList: []

        }
    }


    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () => {
        this.setState({ showModal: true });
    }

    closeToast = (timed) => {
        this.props.closeAction(true)
        setTimeout(() => {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }, timed);
    }

    componentDidMount() {
        this.setState({
            fundId: parseInt(this.props.fundId),
            user: this.props.userdata
        }, () => {
            /**
             * Type 3 is to get actions track list
             */
            if (this.props.userdata.type === 3) {
                this.getActionsTrack();
            } else {
                this.actionsFn();
            }
        })

    }


    getActionsTrack = () => {
        this.open()
        let postObj = { fundId: this.state.fundId, userId: this.state.user.id, accountType: this.state.user.accountType };
        this.Fundhttp.actionsHistory(postObj).then(result => {
            this.close();
            this.setState({
                trackModal: true,
                actionsHistoryList: result.data
            })
        })
            .catch(error => {
                this.close();
            });
    }

    actionsFn = () => {
        let postObj = { fundId: this.state.fundId, id: this.state.user.id, accountType: this.state.user.accountType };
        //For Lp Delegate send LP ID
        if(this.state.user.accountType === 'LPDelegate') {
            postObj['investorId'] = this.state.user.lpData.id
        }
        this.open()
        this.Fundhttp.fundUserActions(postObj, this.state.user.type).then(result => {
            this.close();
            this.setState({
                showToast: true,
                toastMessage: this.state.user.type === 1 ? this.Constants.LINK_COPIED : this.Constants.RESEND_EMAIL,
                toastType: 'success',
            })
            this.closeToast(2000)
        })
            .catch(error => {
                this.close();
                this.setState({
                    showToast: true,
                    toastMessage: this.state.user.type === 1 ? this.Constants.LINK_COPIED_ERROR : this.Constants.RESEND_EMAIL_ERROR,
                    toastType: 'danger',
                })
                this.closeToast(2000)
            });

    }

    closetrackModal = () => {
        this.setState({
            trackModal: false
        })
        this.props.closeAction(false)
    }

    render() {
        return (
            <React.Fragment>
                <Modal id="fundAggrementChangeModal" backdrop="static" className="investorApprovedModal" show={this.state.trackModal} onHide={this.closetrackModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="heightNone" closeButton>
                    </Modal.Header>
                    <Modal.Title>
                        <Row className="approved-header" id="fund-history-track">
                            <div>
                                <label>Track List</label>
                            </div>

                        </Row>
                    </Modal.Title>
                    <Modal.Body>

                        {
                            this.state.actionsHistoryList.length > 0 ?
                                this.state.actionsHistoryList.map((record, index) => {
                                    return (
                                        <div className="row odd" key={index}>
                                            <div className="col-md-8 col-xs-8 col-sm-8">
                                                <div className="user-img">
                                                <img src={record.profile ? record.profile.url : userDefaultImage} alt="img" className="user-image" />
                                                </div>
                                                <div className="user-text">
                                                    <h6 className="ellipsis">{this.FsnetUtil.getFullName(record)}</h6>
                                                    <p>{this.FsnetUtil.getAccountTypeName(record.accountType)} ({record.action})</p>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-4 col-sm-4 ">
                                                <div className="user-timing">
                                                    <p>{this.FsnetUtil.getDateFromFormat(record.updatedAt)}</p>
                                                    <p>{this.FsnetUtil.getTimeFromFormat(record.updatedAt)} ({record.timeZone})</p>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                                :
                                <h1 className="title-md marginTop30 text-center">There are no actions.</h1>
                        }
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
            </React.Fragment>
        );
    }
}

export default FundActions;

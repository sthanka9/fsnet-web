var _ = require('lodash');

export class FundUtil{

    _getUserTitleName(data) {
        let label = '';
        if(data.accountType === 'SecondaryGP' && data.type === 1) {
            label = 'Add Signatory'
        } else if(data.accountType === 'SecondaryGP' && data.type === 2) {
            label = 'Edit Signatory'
        } else if(data.accountType === 'GPDelegate' && data.type === 1) {
            label = 'Add Delegate'
        } else if(data.accountType === 'GPDelegate' && data.type === 2) {
            label = 'Edit Delegate'
        } else if(data.accountType === 'LP' && data.type === 1) {
            label = 'Add Investor'
        } else if(data.accountType === 'LP' && data.type === 2) {
            label = 'Edit Investor'
        } else if(data.accountType === 'OfflineLP' && data.type === 1) {
            label = 'Add Offline Investor'
        } else if(data.accountType === 'OfflineLP' && data.type === 2) {
            label = 'Edit Offline Investor'
        } else if(data.accountType === 'LPDelegate' && data.type === 2) {
            label = 'Edit Delegate'
        }
        return label;

    }


    _getUserTitleNameLPDelegate(user) {
        const name = user['middleName'] ? `${user['firstName']} ${user['middleName']} ${user['lastName']}` : `${user['firstName']} ${user['lastName']}`;
        const desc = `Edit Delegate - ${name}`;
        return desc;
    }

    _getUserDeleteTitleName(data) {
        let label = '';
        if(data.accountType === 'SecondaryGP') {
            label = 'Delete Signatory'
        } else if(data.accountType === 'GPDelegate') {
            label = 'Delete Delegate'
        } else if(['LP','OfflineLP'].indexOf(data.accountType) > -1) {
            label = 'Delete Investor'
        }
        return label;

    }

    _getUserDesc (data) {
        let desc = '';
        if(data.accountType === 'SecondaryGP' && data.type === 1) {
            desc = 'Fill in the form below to add a new Signatory to the Fund. Fields marked with an * are required.'
        } else if(data.accountType === 'SecondaryGP' && data.type === 2) {
            desc = 'Fill in the form below to edit a new Signatory to the Fund. Fields marked with an * are required.'
        } else if(data.accountType === 'GPDelegate' && data.type === 1) {
            desc = 'Fill in the form below to add a new Delegate to the Fund. Fields marked with an * are required.'
        } else if(data.accountType === 'GPDelegate' && data.type === 2) {
            desc = 'Fill in the form below to edit a new Delegate to the Fund. Fields marked with an * are required.'
        } else if(data.accountType === 'LP' && data.type === 1) {
            desc = 'Fill in the form below to add a new Investor to the Fund. Fields marked with an * are required.'
        } else if(data.accountType === 'OfflineLP' && data.type === 1) {
            desc = 'If the investor you are entering manually on their behalf (i.e., “offline”) does not have an active email address, please supply a unique email address ending in “@temporary.com”. For example, if you are manually entering John Smith you might enter johnsmith@temporary.com. This will become the unique identifier for this Investor. Vanilla understands not to dispatch actual emails under this circumstance as long as the @temporary.com designation is utilized as described. In the future, if the Investor would like to receive emails and/or “take over” the account (for example because they wish to vote on amendments, handle capital calls, receive notifications or otherwise use Vanilla for the Fund’s future affairs) you may change this email field to their actual email address, and at that time when the @temporary.com designator is no longer in place, Vanilla will automatically begin to treat them as it does other Investors.'
        } else if(data.accountType === 'LP' && data.type === 2) {
            desc = 'Fill in the form below to edit a Investor to the Fund. Fields marked with an * are required.'
        } else if(data.accountType === 'OfflineLP' && data.type === 2) {
            desc = 'Fill in the form below to edit a Offline Investor to the Fund. Fields marked with an * are required.'
        } else if(data.accountType === 'LPDelegate' && data.type === 2) {
            desc = 'Fill in the form below to edit a Delegate to the Fund. Fields marked with an * are required.'
        }
        return desc;
    }

    _getUserDeleteDesc (data) {
        let desc = '';
        if(data.accountType === 'LP' && data.editThroughModal) {
            desc = 'This option will delete the Investor’s information for use in future funds. This option is appropriate when there is no or little chance the Investor will participate in the Fund Manager’s funds in the future. Note that any active investments by the Investor will not be deleted, even if selecting this option.'
        }else if(data.accountType === 'SecondaryGP') {
            desc = 'This option will permanently delete the secondary signatory from your profile. If you wish to save the secondary signatory for future use, you may simply disable the secondary signatory instead of deleting the secondary signatory entirely. Do you wish to proceed to delete the secondary signatory entirely?'
        } else if(data.accountType === 'GPDelegate') {
            desc = 'This option will permanently delete the Delegate from your profile. If you wish to save the Delegate for future use, you may simply disable the Delegate instead of deleting the Delegate entirely. Do you wish to proceed to delete the Delegate entirely?'
        } else if(['LP','OfflineLP'].indexOf(data.accountType) > -1) {
            desc = 'This option will remove the Investor, including all in-progress investments, from this Fund.'
        }
        return desc;
    }

    _hasNullValue (mandatoryFields, obj) {
        for(let index of mandatoryFields) {
            if(obj && !obj[index]) {
                return true;
            }
        }
    }
    
    _hasValue(array,obj) {
        if(obj) {
            for(let index of array) {
                if(obj && obj[index]) {
                    return true;
                }
            }
        } else {
            return false
        }
    }

    _actionStatusNameMask(name) {
        let nameMask = name;
        if(name === 'Resend Email') {
            nameMask = 'Invited'
        } else if(name === 'Copy Link') {
            nameMask = 'Link Copied'
        } 
        return nameMask;

    }
}
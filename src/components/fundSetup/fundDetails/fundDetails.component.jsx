
import React, { Component } from 'react';
import '../fundSetup.component.css';
import { Button, Radio, Row, Col, FormControl, Tooltip, OverlayTrigger, Modal, Checkbox as CBox, Table } from 'react-bootstrap';
import Loader from '../../../widgets/loader/loader.component';
import { Fsnethttp } from '../../../services/fsnethttp';
import { Fundhttp } from '../../../services/fundhttp';
import { FundUtil } from '../../fundSetup/util/fundUtil';
import { Constants } from '../../../constants/constants';
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../../util/util';
import FundImage from '../../../images/fund-default@2x.png';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ActionsComponent from '../fundActions/fundActions';
var _ = require('lodash');

var autoSave, showPromptModal = {}
class FundDetailsComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.FundUtil = new FundUtil();
        this.Fundhttp = new Fundhttp();
        this.state = {
            jsonData: {},
            firmIdsList: [],
            fundDetailsPageValid: false,
            fundImage: FundImage,
            fundData: {
                status: 'Open',
                isNotificationsEnabled: false,
                gpInfo: {
                    firstName: '',
                    lastName: ''
                }
            },
            userObj: {},
            fundPicFile: {},
            imageRemoved: false,
            showModal: false,
            showPromptModal: false,
            promtModal: true,
            dollarFileds: ['fundTargetCommitment', 'fundHardCap', 'capitalCommitmentByFundManager'],
            perFileds: ['percentageOfLPAndGPAggregateCommitment', 'percentageOfLPCommitment'],
            fundMandatoryFields: ['legalEntity', 'fundCommonName', 'fundManagerTitle', 'fundManagerCommonName', 'fundManagerLegalEntityName', 'fundEntityType', 'vcfirmId'],
            isFormChanged: false,
            timeZonesList: []
        }
        autoSave = setInterval(() => {
            if (this.state.isFormChanged && this.state.fundId) {
                this.proceedToNext(true, false)();
            }
        }, this.Constants.AUTO_SAVE_TIME);
        showPromptModal = PubSub.subscribe('showPromptModal', (msg, data) => {
            if (data.showPromptModal && this.state.isFormChanged) {
                this.openPromptModal();
            } else if (data.redirectUrl) {
                this.navigateToDashboard();
            }
        });
    }


    componentDidMount() {
        this.getJsonData();
        PubSub.publish('pageNumber', { type: 'sideNav', page: 'funddetails' });
        let id = this.FsnetUtil._getId();
        if (this.FsnetUtil.getUserRole() != 'GP' && !parseInt(id)) {
            this.getListOfFirmIds();
        }
        this.getTimeZones();
        if (parseInt(id)) {
            this.setState({
                fundId: id,
                vcfirmId: parseInt(this.FsnetUtil.getFirmId()),
            }, () => {
                this.open();
                this.Fsnethttp.getFund(this.state.fundId).then(result => {
                    this.close();
                    this.FsnetUtil.setLeftNavHeight();
                    if (result.data) {
                        let temp = this.modifyFundData(result.data.data)
                        this.setState({
                            fundData: temp,
                            fundImage: result.data.data.fundImage ? result.data.data.fundImage.url : FundImage,
                        }, () => {
                            this.addMaskValueToFundData();
                        })
                    }
                })
                    .catch(error => {
                        this.close();
                    });

            })
        } else {
            let obj = {...this.state.fundData};
            obj['vcfirmId'] = parseInt(this.FsnetUtil.getFirmId())
            this.setState({
                fundData:obj
            })
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }


    getJsonData = () => {
        this.Fsnethttp.getJson('fundDetails').then(result => {
            this.setState({
                jsonData: result.data
            })
        });

    }

    getListOfFirmIds = () => {
        const delegateId = this.FsnetUtil.getUserId();
        if (delegateId) {
            this.Fsnethttp.getVcFirms(delegateId).then(result => {
                this.close();
                this.setState({
                    firmIdsList: result.data
                })
            })
                .catch(error => {
                    this.close();
                });
        }
    }


    handleFocusEvent = (key) => () => {
        let temp = this.state.fundData;
        temp[key + 'Mask'] = temp[key];
        this.setState({
            fundData: temp
        })
    }


    /**
     * This method is to handle for enable/disable GP Notifications.
     */
    handleGpNotify = (event) => {
        let data = this.state.fundData;
        data['isNotificationsEnabled'] = event.target.checked;
        this.setState({
            fundData: data,
            isFormChanged: true
        })
    }

    handleChangeEvent = (key, allowOnlyNumber, isPercentage) => (event) => {
        let value = '';
        const allowNumberRegex = this.Constants.NUMBER_DOT_REGEX;
        let temp = this.state.fundData;
        if (allowOnlyNumber) {
            if (isPercentage) {
                if ((!event.target.value.trim() || allowNumberRegex.test(event.target.value)) && (parseInt(event.target.value) > 0 && parseInt(event.target.value) <= 100)) {
                    value = event.target.value.trim()
                } else {
                    value = event.target.value ? temp[key] : '';
                }
            } else {
                if (!event.target.value.trim() || allowNumberRegex.test(event.target.value)) {
                    value = event.target.value.trim()
                } else {
                    value = temp[key];
                }
            }
        } else {
            value = event.target.value;
        }

        if (value) {
            temp[key] = value;
            temp[key + 'Mask'] = value;
            temp['Touched'] = false;
        } else {
            temp[key] = value.trim();
            temp[key + 'Mask'] = value.trim();
            temp['Touched'] = false
        }

        let finalObj = this.checkIsFundManagerFileds(key, temp);
        this.setState({
            fundData: finalObj
        })
    }

    checkIsFundManagerFileds = (key, temp) => {
        if (key === 'percentageOfLPCommitment') {
            temp['capitalCommitmentByFundManager'] = '';
            temp['percentageOfLPAndGPAggregateCommitment'] = '';
        } else if (key === 'capitalCommitmentByFundManager') {
            temp['percentageOfLPCommitment'] = '';
            temp['percentageOfLPAndGPAggregateCommitment'] = '';
        } else if (key === 'percentageOfLPAndGPAggregateCommitment') {
            temp['capitalCommitmentByFundManager'] = '';
            temp['percentageOfLPCommitment'] = '';
        }
        return temp;
    }

    modifyFundData = (temp) => {
        if (temp['percentageOfLPCommitment'] > 0) {
            temp['capitalCommitmentByFundManager'] = '';
            temp['percentageOfLPAndGPAggregateCommitment'] = '';
        } else if (temp['capitalCommitmentByFundManager'] > 0) {
            temp['percentageOfLPCommitment'] = '';
            temp['percentageOfLPAndGPAggregateCommitment'] = '';
        } else if (temp['percentageOfLPAndGPAggregateCommitment'] > 0) {
            temp['capitalCommitmentByFundManager'] = '';
            temp['percentageOfLPCommitment'] = '';
        } else {
            temp['vcfirmId'] = this.FsnetUtil.getFirmId() ? parseInt(this.FsnetUtil.getFirmId()) : null
        }
        return temp;
    }

    valueTouched = (e) => {
        const name = e.target.name;
        let temp = this.state.fundData;
        let dollarFileds = [...this.state.dollarFileds]
        let perFileds = [...this.state.perFileds]
        let value = !e.target.value.trim() ? '' : _.indexOf(dollarFileds, name) > -1 ? this.FsnetUtil.convertToCurrency(e.target.value.trim()) : _.indexOf(perFileds, name) > -1 ? `${e.target.value.trim()}%` : e.target.value
        temp[name + 'Mask'] = value;
        temp[name] = e.target.value.trim();
        temp[name + 'Touched'] = true;
        this.setState({
            fundData: temp,
            isFormChanged: true,
            fundDetailspageError: null
        }, () => {
            this.checkIsMandatoryFieldsAreFilled(name);
        })
    }

    checkIsMandatoryFieldsAreFilled = (key) => {
        let hasKey = _.indexOf(this.state.fundMandatoryFields, key)
        if (!this.state.fundId && (hasKey > -1 || key === 'vcfirmId') && !this.FundUtil._hasNullValue(this.state.fundMandatoryFields, this.state.fundData)) {
            this.proceedToNext(true, true)()
        }
    }


    addMaskValueToFundData = () => {
        let temp = this.state.fundData;
        for (let index of this.state.dollarFileds) {
            temp[index + 'Mask'] = temp[index] ? this.FsnetUtil.convertToCurrency(temp[index]) : temp[index];
        }
        for (let index of this.state.perFileds) {
            temp[index + 'Mask'] = temp[index] ? `${temp[index]}%` : temp[index];
        }
        this.setState({
            fundData: temp
        },()=>{
            this.setDefaultTimezone();
        })




        
    }

    setDefaultTimezone = () => {
        let getIsDefault = this.state.timeZonesList.filter((zone) => zone.isDefault === true);
        let obj = {...this.state.fundData};
        if(!obj.timezone && getIsDefault[0]) {
            obj['timezone'] = getIsDefault[0]['id'];
            this.setState({
                fundData:obj
            })
        }
    }


    checkFundDetailsNotValid = () => {
        let fundCreateMandatoryFileds = this.state.fundMandatoryFields;
        let obj = this.state.fundData;
        let isHaveNull = _.some(fundCreateMandatoryFileds, (item) => _.isEmpty(obj[item]));
        return isHaveNull;
    }


    navigateToDashboard = () => {
        window.location.href = this.FsnetUtil.getUserRole() === 'FSNETAdministrator' ? "/adminDashboard/firmView" : "/dashboard"
    }

    checkFundValidations = () => {
        let error = false
        if (this.state.fundData.fundHardCap && this.state.fundData.fundTargetCommitment && (parseFloat(this.state.fundData.fundHardCap) < parseFloat(this.state.fundData.fundTargetCommitment)) || (this.FundUtil._hasNullValue(this.state.fundMandatoryFields, this.state.fundData))) {
            error = true
        }
        return error;
    }

    proceedToNext = (isAutoSave, isFundCreate) => () => {
        let $this = this;
        if (this.state.fundData.vcfirmId) {
            let isError = this.checkFundValidations();
            if (isError) {
                return;
            }
            if (!isAutoSave) {
                this.open()
            } else {
                this.setState({
                    isFormChanged: false
                })
            }
            var formData = new FormData();
            if (this.state.fundId) {
                formData.append("fundId", parseInt(this.state.fundId))
            }
            formData.append("vcfirmId", parseInt(this.state.fundData.vcfirmId));
            formData.append("legalEntity", this.state.fundData.legalEntity);
            formData.append("fundCommonName", this.state.fundData.fundCommonName);
            formData.append("fundManagerTitle", this.state.fundData.fundManagerTitle);
            formData.append("fundManagerCommonName", this.state.fundData.fundManagerCommonName);
            formData.append("fundEntityType", this.state.fundData.fundEntityType);
            formData.append("fundManagerLegalEntityName", this.state.fundData.fundManagerLegalEntityName);
            formData.append("fundTargetCommitment", this.state.fundData.fundTargetCommitment || "");
            formData.append("fundHardCap", this.state.fundData.fundHardCap || "");
            formData.append("fundType", this.state.fundData.fundType || "");
            formData.append("hardCapApplicationRule", this.state.fundData.hardCapApplicationRule || "");
            formData.append("percentageOfLPCommitment", parseFloat(this.state.fundData.percentageOfLPCommitment) || "");
            formData.append("capitalCommitmentByFundManager", parseFloat(this.state.fundData.capitalCommitmentByFundManager) || "");
            formData.append("percentageOfLPAndGPAggregateCommitment", parseFloat(this.state.fundData.percentageOfLPAndGPAggregateCommitment) || "");
            formData.append("timezone", parseFloat(this.state.fundData.timezone) || "");
            formData.append("isNotificationsEnabled", this.state.fundData.isNotificationsEnabled ? true : false);
            if (Object.keys(this.state.fundPicFile).length === 0 && this.state.fundPicFile.constructor === Object && this.state.imageRemoved) {
                formData.append("requestContainImage", false);
            } else {
                formData.append("requestContainImage", true);
            }
            formData.append("fundImage", this.state.fundPicFile);
            this.Fsnethttp.fundStore(formData).then(result => {
                this.close();
                //First five fileds
                if (isFundCreate) {
                    // $this.props.fundCreate({fundData:result.data.data})
                    PubSub.publish('fundData', result.data.data);
                    this.setState({
                        fundId: result.data.data.id
                    })
                    //$this.props.history.push(`/fundSetup/funddetails/${$this.FsnetUtil._encrypt(result.data.data.id)}`);
                } else if (!isAutoSave) {
                    if (['GP', 'FSNETAdministrator'].indexOf($this.FsnetUtil.getUserRole()) > -1) {
                        $this.props.history.push(`/fundSetup/gpSignatories/${$this.FsnetUtil._encrypt(result.data.data.id)}`);
                    } else if ($this.FsnetUtil.getUserRole() == 'SecondaryGP') {
                        $this.props.history.push(`/fundSetup/gpDelegate/${$this.FsnetUtil._encrypt(result.data.data.id)}`);
                    } else {
                        $this.props.history.push(`/fundSetup/editSubForm/${$this.FsnetUtil._encrypt(result.data.data.id)}`);
                    }
                }

            })
                .catch(error => {
                    this.close();
                    if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                        this.setState({
                            fundDetailspageError: error.response.data.errors[0].msg,
                        });
                    } else {
                        this.setState({
                            fundDetailspageError: this.Constants.INTERNAL_SERVER_ERROR,
                        });
                    }
                });
        } else {
            this.setState({
                fundDetailspageError: this.Constants.NO_FIRM_ID,
            });
        }
    }

    //Clear the image when user click on remove button
    //Clear the image name from state
    //Clear the input file value
    removeImageBtn = () => {
        this.setState({
            fundImageName: 'fund_Pic.jpg',
            fundImage: FundImage,
            fundPicFile: {},
            imageRemoved: true,
            isFormChanged: true
        });
        document.getElementById('uploadBtn').value = "";
    }

    uploadBtnClick = () => {
        document.getElementById('uploadBtn').click();
    }

    //USer profile pic upload.
    handleChange = (event) => {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            this.imageFile = event.target.files[0];
            let imageName = event.target.files[0].name
            var sFileExtension = imageName.split('.')[imageName.split('.').length - 1].toLowerCase();
            const imageFormats = ['png', 'jpg', 'jpeg', 'gif', 'tif', 'svg'];
            if (imageFormats.indexOf(sFileExtension) === -1) {
                document.getElementById('uploadBtn').value = "";
                alert('Please upload valid image.')
                return true;
            }
            //Change user profile image size limit here.
            if (this.imageFile.size <= 1600000) {
                this.setState({
                    fundPicFile: event.target.files[0],
                });
                reader.readAsDataURL(this.imageFile);
                this.setState({
                    fundImageName: event.target.files[0].name,
                    imageRemoved: false
                });
                reader.onload = () => {
                    this.setState({
                        fundImage: reader.result,
                        isFormChanged: true
                    });
                }
            } else {
                alert('Please upload image Maximum file size : 512X512')
            }
        }
    }

    closePromptModal = () => {
        this.setState({ showPromptModal: false })
    }

    openPromptModal = () => {
        this.setState({ showPromptModal: true })
    }

    componentWillUnmount() {
        PubSub.unsubscribe(showPromptModal);
        clearInterval(autoSave);
        if (this.state.fundId && this.state.isFormChanged) {
            this.proceedToNext(true, false)()
        }
    }

    getTimeZones = () => {
        this.open();
        this.Fundhttp.getTimeZones().then(result => {
            this.close();
            this.setState({
                timeZonesList: result.data
            },()=>{
                this.setDefaultTimezone();
            })
        })
            .catch(error => {
                this.close();
            });
    }

    userAction = (data, type) => () => {
        data['type'] = type;
        data['accountType'] = 'GP';
        data['id'] = this.state.fundData.gpId
        this.setState({
            userObj: data,
            showActions: true
        })
    }

    closeActionComponent = () => {
        setTimeout(() => {
            this.setState({
                showActions: false
            })
        }, 1000);
    }

    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                    overlay={<Tooltip id="tooltip">{tooltip}</Tooltip>}
                    placement="left"
                    delayShow={300}
                    delayHide={150}
                    rootClose
                >
                    <a href={href}>{children}</a>
                </OverlayTrigger>
            );
        }
        return (
            <div className="step1FormClass">
                {/* <Prompt
                    when={this.state.promtModal}
                    message=""
                />    */}
                <div className="form-grid formGridDivMargin">
                    <h2 className="title">{this.state.jsonData.FUND_DETAILS}</h2>
                    <h4 className="subtext">{this.Constants.FUND_ASTERIK}</h4>
                    <div id="step1Form">
                        {
                            this.state.firmIdsList.length > 0 &&
                            <Row className="step1Form-row">
                                <Col xs={6} md={6}>
                                    <label className="form-label">Fund Manager*</label>
                                    <FormControl defaultValue={""} name="vcfirmId" className={"selectFormControl " + ((this.state.fundData.vcfirmIdTouched && this.state.fundData.vcfirmId < 1) && 'inputError')} componentClass="select" value={this.state.fundData.vcfirmId}
                                        onChange={this.handleChangeEvent('vcfirmId')}
                                        onBlur={this.valueTouched}
                                    >
                                        <option value="">Select Fund Manager</option>
                                        {this.state.firmIdsList.map((record, index) => {
                                            return (
                                                <option value={record['id']} key={index} >{record['name']}</option>
                                            );
                                        })}
                                    </FormControl>
                                    {
                                        (this.state.fundData.vcfirmIdTouched && this.state.fundData.vcfirmId < 1) &&
                                        <span className="error">{this.Constants.FIRM_ID_REQUIRED}</span>
                                    }
                                </Col>
                            </Row>



                        }
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_LEGAL_ENTITY_NAME}*
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_LEGAL_ENTITY_NAME}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="legalEntity" placeholder={this.state.jsonData.PLACEHOLDER_FUND_LEGAL_ENTITY_NAME} className={"inputFormControl " + ((this.state.fundData.legalEntityTouched && !this.state.fundData.legalEntity) && 'inputError')} value={this.state.fundData.legalEntity}
                                    onChange={this.handleChangeEvent('legalEntity')}
                                    onBlur={this.valueTouched}
                                />
                                {
                                    (this.state.fundData.legalEntityTouched && !this.state.fundData.legalEntity) &&
                                    <span className="error">{this.Constants.LEGAL_ENTITY_REQUIRED}</span>
                                }
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_COMMON_NAME}*
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_COMMON_NAME}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="fundCommonName" placeholder={this.state.jsonData.FUND_COMMON_NAME} className={"inputFormControl " + ((this.state.fundData.fundCommonNameTouched && !this.state.fundData.fundCommonName) && 'inputError')} value={this.state.fundData.fundCommonName}
                                    onChange={this.handleChangeEvent('fundCommonName')}
                                    onBlur={this.valueTouched} />
                                {
                                    (this.state.fundData.fundCommonNameTouched && !this.state.fundData.fundCommonName) &&
                                    <span className="error">{this.Constants.FUND_COMMON_NAME_REQUIRED}</span>
                                }
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_MANAGER_LEGAL_ENTITY_NAME}*
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_MANAGER_LEGAL_ENTITY_NAME}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="fundManagerLegalEntityName" placeholder={this.state.jsonData.PLACEHOLDER_FUND_MANAGER_LEGAL_ENTITY_NAME} className={"inputFormControl " + ((this.state.fundData.fundManagerLegalEntityNameTouched && !this.state.fundData.fundManagerLegalEntityName) && 'inputError')} value={this.state.fundData.fundManagerLegalEntityName}
                                    onChange={this.handleChangeEvent('fundManagerLegalEntityName')}
                                    onBlur={this.valueTouched} />
                                {
                                    (this.state.fundData.fundManagerLegalEntityNameTouched && !this.state.fundData.fundManagerLegalEntityName) &&
                                    <span className="error">{this.Constants.LEGAL_ENTITY_NAME_REQUIRED}</span>
                                }
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_MANAGER_TITLE}*
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_MANAGER_TITLE}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="fundManagerTitle" placeholder={this.state.jsonData.FUND_MANAGER_TITLE} className={"inputFormControl " + ((this.state.fundData.fundManagerTitleTouched && !this.state.fundData.fundManagerTitle) && 'inputError')} value={this.state.fundData.fundManagerTitle}
                                    onChange={this.handleChangeEvent('fundManagerTitle')} onBlur={this.valueTouched} />
                                {
                                    (this.state.fundData.fundManagerTitleTouched && !this.state.fundData.fundManagerTitle) &&
                                    <span className="error">{this.Constants.FUND_MANAGER_TITLE_REQUIRED}</span>
                                }
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_MANAGER_COMMON_NAME}*
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_MANAGER_COMMON_NAME}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="fundManagerCommonName" placeholder={this.state.jsonData.FUND_MANAGER_COMMON_NAME} className={"inputFormControl " + ((this.state.fundData.fundManagerCommonNameTouched && !this.state.fundData.fundManagerCommonName) && 'inputError')} value={this.state.fundData.fundManagerCommonName}
                                    onChange={this.handleChangeEvent('fundManagerCommonName')}
                                    onBlur={this.valueTouched} />
                                {
                                    (this.state.fundData.fundManagerCommonNameTouched && !this.state.fundData.fundManagerCommonName) &&
                                    <span className="error">{this.Constants.FUND_MANAGER_COMMON_NAME_REQUIRED}</span>
                                }
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">Fund Entity Type*
                                </label>
                                <FormControl defaultValue={""} name="fundEntityType" className={"selectFormControl " + ((this.state.fundData.fundEntityTypeTouched && this.state.fundData.fundEntityType < 1) && 'inputError')} componentClass="select" placeholder={this.state.jsonData.SELECT_FUND_TYPE} value={this.state.fundData.fundEntityType}
                                    onChange={this.handleChangeEvent('fundEntityType')}
                                    onBlur={this.valueTouched}
                                >
                                    <option value="">Select type of Equity Holder</option>
                                    <option value="1">Limited Partnership</option>
                                    <option value="2">Limited Liability company</option>
                                </FormControl>
                                {
                                    (this.state.fundData.fundEntityTypeTouched && this.state.fundData.fundEntityType < 1) &&
                                    <span className="error">{this.Constants.TYPE_OF_EQUITY_REQUIRED}</span>
                                }
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUNDS_TARGET_COMMITMENTS}**
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUNDS_TARGET_COMMITMENTS}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="fundTargetCommitment" placeholder={this.state.jsonData.PLACEHOLDER_FUNDS_TARGET_COMMITMENTS} className="inputFormControl" value={this.state.fundData.fundTargetCommitmentMask}
                                    onBlur={this.valueTouched}
                                    onChange={this.handleChangeEvent('fundTargetCommitment', true)}
                                    onFocus={this.handleFocusEvent('fundTargetCommitment')}
                                />
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUNDS_HARD_CAP}**
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUNDS_HARD_CAP}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="fundHardCap" className={"inputFormControl " + (this.state.fundData.fundHardCapTouched && (parseFloat(this.state.fundData.fundHardCap) < parseFloat(this.state.fundData.fundTargetCommitment)) && 'inputError')} placeholder={this.state.jsonData.PLACEHOLDER_FUNDS_HARD_CAP} value={this.state.fundData.fundHardCapMask}
                                    onChange={this.handleChangeEvent('fundHardCap', true)}
                                    onFocus={this.handleFocusEvent('fundHardCap')}
                                    onBlur={this.valueTouched}
                                />
                                {
                                    (this.state.fundData.fundHardCapTouched && (parseFloat(this.state.fundData.fundHardCap) < parseFloat(this.state.fundData.fundTargetCommitment))) &&
                                    <span className="error">{this.Constants.FUND_HARD_CAP_MSZ}</span>
                                }
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_TYPE}**</label>
                                <FormControl defaultValue={""} name="fundType" className={"selectFormControl " + ((this.state.fundData.fundTypeTouched && this.state.fundData.fundType < 1) && 'inputError')} componentClass="select" placeholder={this.state.jsonData.SELECT_FUND_TYPE} value={this.state.fundData.fundType}
                                    onChange={this.handleChangeEvent('fundType')}
                                    onBlur={this.valueTouched}
                                >
                                    <option value="">{this.state.jsonData.SELECT_FUND_TYPE}</option>
                                    <option value="1">{this.state.jsonData.US_FUND}</option>
                                    <option value="2">{this.state.jsonData.NON_US_FUND}</option>
                                </FormControl>
                                {
                                    (this.state.fundData.fundTypeTouched && this.state.fundData.fundType < 1) &&
                                    <span className="error">{this.Constants.FUND_TYPE_REQUIRED}</span>
                                }
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.HARD_CAP_APPLICATION_RULE}**
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_HARD_CAP_APPLICATION_RULE}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl defaultValue={""} name="hardCapApplicationRule" className={"selectFormControl " + ((this.state.fundData.hardCapApplicationRuleTouched && this.state.fundData.hardCapApplicationRule < 1) && 'inputError')} componentClass="select" placeholder={this.state.jsonData.SELECT_HARD_CAP_APPLICTION_RULE} value={this.state.fundData.hardCapApplicationRule}
                                    onChange={this.handleChangeEvent('hardCapApplicationRule')}
                                    onBlur={this.valueTouched}
                                >
                                    <option value="">{this.state.jsonData.SELECT_HARD_CAP_APPLICTION_RULE}</option>
                                    <option value="1">{this.state.jsonData.INVESTOR_CAPITAL_COMMITMENT_ONLY}</option>
                                    <option value="2">{this.state.jsonData.INVESTOR_CAPITAL_COMMITMENT_PLUS_FUND_CAPITAL_COMMITMENT}</option>
                                </FormControl>
                                {
                                    (this.state.fundData.hardCapApplicationRuleTouched && this.state.fundData.hardCapApplicationRule < 1) &&
                                    <span className="error">{this.Constants.HARDCAP_VALIDATION_RULE_REQUIRED}</span>
                                }
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">Time zone**
                                    <span>
                                        <LinkWithTooltip tooltip={this.Constants.TIME_ZONE_TOOLTIP}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                
                                </label>
                                <FormControl name="timezone" className={"selectFormControl " + ((this.state.fundData.timezoneTouched && this.state.fundData.timezone < 1) && 'inputError')} componentClass="select" value={this.state.fundData.timezone}
                                    onChange={this.handleChangeEvent('timezone')}
                                    onBlur={this.valueTouched}
                                >
                                    {this.state.timeZonesList.map((record, index) => {
                                        return (
                                            <option value={record['id']} selected={(this.state.fundData.timezone) ? this.state.fundData.timezone === record['id'] : record['isDefault']===true} key={index} >{record['displayName']}</option>
                                        );
                                    })}

                                </FormControl>
                                {
                                    (this.state.fundData.timezoneTouched && this.state.fundData.timezone < 1) &&
                                    <span className="error">{this.Constants.TIME_ZONE_REQUIRED}</span>
                                }
                            </Col>
                        </Row>
                        <h2 className="title marginTop40">{this.state.jsonData.FUND_MANAGER_CAPITAL_COMMITMENT}</h2>
                        <h4 className="subtext">{this.state.jsonData.SPECIFY_FUND_MANAGER_CAPITAL_COMMITMENT}</h4>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.PERCENTAGE_INVESTOR_COMMITMENTS}**
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_PERCENTAGE_INVESTOR_COMMITMENTS} id="tooltip-1">
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="percentageOfLPCommitment" placeholder="1.00%" className="inputFormControl" value={this.state.fundData.percentageOfLPCommitmentMask}
                                    onChange={this.handleChangeEvent('percentageOfLPCommitment', true, true)}
                                    onFocus={this.handleFocusEvent('percentageOfLPCommitment')}
                                    onBlur={this.valueTouched}
                                    disabled={this.state.fundData.capitalCommitmentByFundManager || this.state.fundData.percentageOfLPAndGPAggregateCommitment}
                                />
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FIXED_COMMITMENT_DOLLARS}**
                                        <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FIXED_COMMITMENT_DOLLARS} id="tooltip-1">
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="capitalCommitmentByFundManager" placeholder={this.state.jsonData.PLACEHOLDER_FUNDS_TARGET_COMMITMENTS} className="inputFormControl" value={this.state.fundData.capitalCommitmentByFundManagerMask}
                                    onChange={this.handleChangeEvent('capitalCommitmentByFundManager', true)}
                                    onFocus={this.handleFocusEvent('capitalCommitmentByFundManager')}
                                    onBlur={this.valueTouched}
                                    disabled={this.state.fundData.percentageOfLPCommitment || this.state.fundData.percentageOfLPAndGPAggregateCommitment}
                                />
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.PERCENTAGE_ALL_COMMITMENTS}**
                                    <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_PERCENTAGE_ALL_COMMITMENTS} id="tooltip-1">
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span>
                                </label>
                                <FormControl type="text" name="percentageOfLPAndGPAggregateCommitment" placeholder="1.00%" className="inputFormControl" value={this.state.fundData.percentageOfLPAndGPAggregateCommitmentMask}
                                    onFocus={this.handleFocusEvent('percentageOfLPAndGPAggregateCommitment')}
                                    onChange={this.handleChangeEvent('percentageOfLPAndGPAggregateCommitment', true, true)}
                                    onBlur={this.valueTouched}
                                    disabled={this.state.fundData.percentageOfLPCommitment || this.state.fundData.capitalCommitmentByFundManager}
                                />
                            </Col>
                        </Row>
                        {
                            (this.FsnetUtil.getUserRole() === 'GPDelegate' && this.state.fundId) &&
                            <Row className="marginLeftNone">
                                <h2 className="title marginTop20 block">Invite Fund Manager</h2>
                                <div className="width600">
                                    <Table className="resendInvitationTable">
                                        <thead>
                                            <tr>
                                                <th><span>Fund Manager</span></th>
                                                <th><span>Actions</span></th>
                                                <th className="text-center"><span>Email Notifications</span>
                                                    <LinkWithTooltip tooltip="Enable/Disable email notifications to Fund manager" id="tooltip-1">
                                                        <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                                    </LinkWithTooltip>
                                                </th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> <label className="title-md width200 text-left paddingRight20 ellipsis pr12 pr7" title={this.FsnetUtil.getFullName(this.state.fundData.gpInfo)}>{this.FsnetUtil.getFullName(this.state.fundData.gpInfo)}</label>
                                                </td>
                                                <td> <button className="actionsEnvelopeBtn" title={this.Constants.RESEND_EMAIL_TITLE} onClick={this.userAction(this.state.fundData, 2)}><i className="fa fa-envelope-o"></i></button>
                                                    <CopyToClipboard text={`${this.Constants._registerLink}${this.state.fundData.gpInfo.emailConfirmCode}`}
                                                        onCopy={this.userAction(this.state.fundData, 1)}>
                                                        <button title={this.Constants.COPY_LINK_TITLE} className={"actionsLinkBtn " + (!this.state.fundData['gpInfo']['copyLink'] && 'disabled')}><i className="fa fa-link"></i></button>
                                                    </CopyToClipboard>
                                                </td>

                                                <td className="text-center">
                                                    <CBox className="marginTop0" title="Enable/Disable email notifications to Fund manager" onChange={this.handleGpNotify} checked={this.state.fundData.isNotificationsEnabled}>
                                                        <span className="checkmark"></span>
                                                    </CBox>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </Table>




                                </div>
                            </Row>
                        }
                        <label className="profile-text">{this.state.jsonData.FUND_IMAGE_DIMENSIONS}</label>
                        <Row className="profile-Row profileMargin marginBottom30 marginLeft15">
                            <img src={this.state.fundImage} alt="profile-pic" className="profile-pic" />
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <input type="file" id="uploadBtn" className="hide" onChange={(e) => this.handleChange(e)} />
                                <Button className="uploadFile" onClick={this.uploadBtnClick}>{this.state.jsonData.UPLOAD_FILE}</Button> <br />
                                <label className="removeBtn" onClick={this.removeImageBtn}>{this.state.jsonData.REMOVE}</label>
                            </Col>
                        </Row>
                        {
                            this.state.fundDetailspageError &&
                            <div className="marginTop20 error">{this.state.fundDetailspageError}</div>
                        }
                    </div>
                </div>
                {
                    ['Open', 'New-Draft', 'Admin-Draft'].indexOf(this.state.fundData.status) > -1 ?
                        <div className="footer-nav">
                            <i className="fa fa-chevron-left disabled" aria-hidden="true"></i>
                            <i className={"fa fa-chevron-right " + (this.FundUtil._hasNullValue(this.state.fundMandatoryFields, this.state.fundData) ? 'disabled' : '')} onClick={this.proceedToNext(false, false)} aria-hidden="true"></i>
                        </div>
                        :
                        <div className="footer-nav">
                            <Button type="button" className={"fsnetSubmitButton " + (this.FundUtil._hasNullValue(this.state.fundMandatoryFields, this.state.fundData) ? 'disabled' : 'btnEnabled')} onClick={this.proceedToNext(false, false)}>{this.state.jsonData.SAVE_CHANGES}</Button>
                        </div>
                }

                <Modal id="confirmFundModal" backdrop="static" show={this.state.showPromptModal} onHide={this.closePromptModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="title-md ulpoadSignText"> Warning: If you leave this screen all entered information will be lost.  Please fill out the six fields marked with an asterisk to save your Fund Setup.</div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closePromptModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.navigateToDashboard}>Proceed</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="LPDelModal" backdrop="static" show={this.state.createFundModal} onHide={(e) => this.setState({ createFundModal: false })} dialogClassName="LPDelModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>Create Fund</Modal.Title>
                    <Modal.Body>
                        <div className="title-md ulpoadSignText">Please create a Fund. You have entered mandatory fileds for creating a Fund.</div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={(e) => this.setState({ createFundModal: false })}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.proceedToNext(false, true)}>Save</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
                {
                    this.state.showActions &&
                    <ActionsComponent userdata={{ ...this.state.userObj }} closeAction={this.closeActionComponent} fundId={this.state.fundId}> </ActionsComponent>
                }

            </div>
        );
    }
}

export default FundDetailsComponent;





import React, { Component } from 'react';
import '../fundSetup.component.css';
import { Button, Modal, Row, Col, FormControl, Checkbox as CBox, Table } from 'react-bootstrap';
import { Constants } from '../../../constants/constants';
import { Fsnethttp } from '../../../services/fsnethttp';
import { Fundhttp } from '../../../services/fundhttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { FundUtil } from '../../fundSetup/util/fundUtil';
import Loader from '../../../widgets/loader/loader.component';
import userDefaultImage from '../../../images/default_user.png';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/rrui.css'
import 'react-phone-number-input/style.css'
var _ = require('lodash');

class userModalComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.Fundhttp = new Fundhttp();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.FsnetAuth = new FsnetAuth();
        this.FundUtil = new FundUtil();
        this.state = {
            userModal: false,
            mandatoryFileds: ['firstName', 'lastName', 'email'],
            user: {
                firstName: '',
                lastName: '',
                middleName: '',
                email: '',
                userStatus: 0,
                organizationName: '',
                cellNumber: ''
            },
            tempObj: {},
            isLpOfflineToOnline: false,
            fundId: null,
            invalidEmail: false,
            isOfflineLP: false,
            exisitingInvestorModal: false,
            firmInvestorsList: []

        }

    }

    componentWillUnmount() {

    }



    componentDidMount() {
        let userData = this.props.userdata;
        userData['userStatus'] = 0;

        this.setState({
            fundId: parseInt(this.props.fundId)
        }, () => {
            if (userData.type === 3) {
                this.setState({
                    user: userData,
                    userDeleteModal: true
                })

            } else if (userData.type === 2) {
                this.setState({
                    user: userData,
                    userModal: true,
                    tempObj: { ...userData }
                })
            } else {

                let data = this.state.user;
                data['type'] = userData.type
                data['accountType'] = userData.accountType
                this.setState({
                    user: data,
                    userModal: true
                })
            }
        })
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () => {
        this.setState({ showModal: true });
    }

    /**
     * 
     * 
     */
    handleFocusEvent = (key) => () => {
        let temp = this.state.user;
        temp[key + 'Mask'] = temp[key];
        this.setState({
            user: temp
        })
    }

    /**
     * convert offline to online
     * This is for only offline LP in edit mode
     */
    handleConvertToOnline = (event) => {
        this.setState({
            isLpOfflineToOnline: event.target.checked
        })
    }


    //To Do: Add event handling from common util methods.
    /**
    * 
    * 
    */

    handleChangeEvent = (key, allowOnlyNumber, isPercentage) => (event) => {
        let value = key === 'cellNumber' ? event : event.target.value;
        const allowNumberRegex = this.Constants.NUMBER_DOT_REGEX;
        let temp = this.state.user;
        if (allowOnlyNumber) {
            if (isPercentage) {
                if ((!value.trim() || allowNumberRegex.test(value)) && (parseInt(value) > 0 && parseInt(value) <= 100)) {
                    value = value.trim()
                } else {
                    value = value ? temp[key] : '';
                }
            } else {
                if (!value.trim() || allowNumberRegex.test(value)) {
                    value = value.trim()
                } else {
                    value = temp[key];
                }
            }
        }

        if (value) {
            temp[key] = value;
            temp[key + 'Mask'] = value;
            temp['Touched'] = false;
        } else {
            temp[key] = value.trim();
            temp[key + 'Mask'] = value.trim();
            temp['Touched'] = false
        }

        this.setState({
            user: temp,
            invalidEmail: false
        })
    }


    valueTouched = (e) => {
        const name = e.target.name;
        let temp = this.state.user;
        temp[name] = e.target.value.trim();
        temp[name + 'Touched'] = true;
        this.setState({
            user: temp,
            isFormChanged: true,
            userErrorMsz:''
        }, () => {
            /**
             * For add user only check user exists
             * Check valid email
             * 
             */
            if (name === 'email' && this.state.user.email && !this.FsnetUtil._emailValidation(this.state.user.email) && this.state.user.type === 1) {
                this.checkEmail();
            }
        })
    }

    closeUserModal = () => {
        this.setState({
            userModal: false
        })
        this.props.closeUser(true)
    }

    checkEmail = () => {
        let type = this.state.user.accountType;
        let postObj = { email: this.state.user.email, fundId: this.state.fundId };
        this.open()
        let url = type === 'GPDelegate' ? 'delegate/gp/check' : (type === 'LPDelegate' ? 'delegate/lp/check' : type === 'SecondaryGP' ? 'signatory/gp/check' : type === 'OfflineLP' ? '/lp/check/isNewOfflineLP' : 'lp/check');
        this.Fsnethttp.checkEmail(url, postObj).then(result => {
            this.close();
            if (result.data) {
                let userObj = this.state.user;
                let data = result.data;
                if (result.data.status !== 0) {
                    userObj['firstName'] = data.firstName;
                    userObj['lastName'] = data.lastName;
                    userObj['middleName'] = data.middleName;
                }
                userObj['userStatus'] = data.status
                this.setState({
                    user: userObj
                })

            }
        })
            .catch(error => {
                this.close();
                if (error.response && error.response.data && error.response.data.errors) {
                    this.setState({
                        userErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        userErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });
    }

    submitUserFn = () => {
        const { email, firstName, lastName, middleName, accountType, id, type, lp } = this.state.user;
        if (accountType === 'OfflineLP') {
            if (this.FsnetUtil._cellNumberValidation(this.state.user.cellNumber)) {
                this.setState({
                    userErrorMsz: this.Constants.CELL_NUMBER_VALID,
                });
                return true;
            }
        }
        if (!this.FsnetUtil._emailValidation(email)) {
            let postObj = { firstName: firstName, lastName: lastName, middleName: middleName, email: email, fundId: this.state.fundId, vcfirmId: this.FsnetUtil.getFirmId(), userId: id, accountType: accountType };
            if (type === 2) {
                let tempObj = {};
                tempObj['email'] = this.state.tempObj.email
                tempObj['firstName'] = this.state.tempObj.firstName
                tempObj['lastName'] = this.state.tempObj.lastName
                tempObj['middleName'] = this.state.tempObj.middleName
                if (['LP', 'OfflineLP'].indexOf(accountType) > -1) {
                    postObj['lpId'] = lp.id;
                    tempObj['subscriptionId'] = lp ? lp.subscriptionId:null;
                    tempObj['organizationName'] = lp.organizationName;
                }
                if (accountType === 'OfflineLP') {
                    tempObj['cellNumber'] = lp.cellNumber;
                    postObj['isLpOfflineToOnline'] = this.state.isLpOfflineToOnline
                }
                postObj['tempObj'] = tempObj;
            }

            if (['LP', 'OfflineLP'].indexOf(accountType) > -1) {
                postObj['organizationName'] = this.state.user.organizationName
            }
            if (accountType === 'OfflineLP') {
                postObj['cellNumber'] = this.state.user.cellNumber
            }
            this.open()
            this.Fundhttp.addoreditUser(postObj, this.state.user.type).then(result => {
                this.close();
                let response = result.data.data;
                //This is through from this component using edit modal. 
                //For unassigned investors edit
                if(this.state.user.editThroughModal) {
                    let list = [...this.state.firmInvestorsList];
                    let user = list.filter((obj)=> obj.id === response.id);
                    let index = _.findIndex(list, {'id':response.id});
                    user[0]['exisitingInvestorModal'] = false;
                    list.splice(index, 1,  user[0]);
                    this.setState({
                        firmInvestorsList:list,
                        exisitingInvestorModal: true,
                        userModal: false,
                    })

                } else {
                    this.closeUserModal();
                    response['copyLink'] = response.hasOwnProperty('copyLink') ? response['copyLink'] : true;
                    response['status'] = response.hasOwnProperty('status') ? response['status'] : this.state.user.status;
                    response['updatedAt'] = response.hasOwnProperty('updatedAt') ? response['updatedAt'] : this.state.user.updatedAt;
                    if (accountType === 'LPDelegate' && type === 2) {
                        response['subscriptionId'] = lp.subscriptionId;
                    }
                    if (this.state.isLpOfflineToOnline && accountType === 'OfflineLP' && type === 2) {
                        response['accountType'] = 'LP';
                        response['id'] = response.id;
                    }
    
                    let data = { user: response, type: type }
                    this.props.updateUserData(data)
                }
               
            })
                .catch(error => {
                    this.close();
                    if (error.response && error.response.data && error.response.data.errors) {
                        this.setState({
                            userErrorMsz: error.response.data.errors[0].msg,
                        });
                    } else {
                        this.setState({
                            userErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                        });
                    }
                });
        } else {
            this.setState({
                invalidEmail: true,
            })
        }

    }

    closeUserDeleteModal = () => {
        this.setState({
            userDeleteModal: false
        })
        this.props.closeUser(true)
    }

    deleteUser = () => {
        let postObj = { fundId: this.state.fundId, userId: this.state.user.id }
        let isFormModal = this.state.user.editThroughModal ? true : false;
        if(isFormModal) {
            postObj['lpId'] = this.state.user.id;
        }
        this.open()
        this.Fundhttp.removeUser(postObj, this.state.user.accountType,isFormModal).then(result => {
            this.close();
            //This is through from this component using edit modal. 
            //For unassigned investors edit
            if(this.state.user.editThroughModal) {
                let list = [...this.state.firmInvestorsList];
                let index = _.findIndex(list, {'id':this.state.user.id});
                list.splice(index, 1);
                this.setState({
                    firmInvestorsList:list,
                    exisitingInvestorModal: true,
                    userModal: false,
                    userDeleteModal:false
                })

            } else {
                this.closeUserDeleteModal();
                let data = { user: this.state.user.id, type: 3 }
                this.props.closeUser(true)
                this.props.updateUserData(data)
            }
            
           
        })
            .catch(error => {
                this.close();
            });

    }

    /**
     * Get existing investors
    */
    getExistingInvestors = () => {
        this.open()
        this.Fundhttp.getFirmInvestors(this.state.fundId).then(result => {
            this.close();
            this.setState({
                userModal: false,
                firmInvestorsList: result.data.data.lps.firmLps,
                exisitingInvestorModal: true
            })
        })
            .catch(error => {
                this.close();
            });
    }


    closeExisitingInvestorModal = () => {
        this.setState({
            userModal: true,
            firmInvestorsList: [],
            user: {
                firstName: '',
                lastName: '',
                middleName: '',
                email: '',
                userStatus: 0,
                organizationName: '',
                cellNumber: '',
                type:1,
                accountType: 'LP'
            },
            exisitingInvestorModal: false
        })
    }


    handleSelectInvestors = (record) => (event) => {
        const value = event.target.checked;
        let list = [...this.state.firmInvestorsList]
        let getInvestorObj = list.filter((lp) => lp.id === record.id);
        getInvestorObj[0]['selected'] = value;
        let index = _.findIndex(list, { 'id': record.id });
        list.splice(index, 1, getInvestorObj[0]);
        this.setState({
            firmInvestorsList: list
        })
    }

    getSelctedInvestors = () => {
        let investorIds = [];
        let list = [...this.state.firmInvestorsList]
        let selectedInvestors = list.filter((lp) => lp.selected === true);
        if (selectedInvestors.length > 0) {
            selectedInvestors.forEach(investor => {
                investorIds.push(investor.id)
            });
        }
        return investorIds;
    }


    addExisitingInvestors = () => {
        const postObj = { fundId: this.state.fundId, lpIds: this.getSelctedInvestors() }
        this.open()
        this.Fundhttp.addExisitingInvestors(postObj).then(result => {
            this.close();
            this.setState({
                userModal: false,
                firmInvestorsList: [],
                exisitingInvestorModal: false
            })
            this.props.updateFundData(result.data);
            this.props.closeUser(true)
        })
            .catch(error => {
                this.close();
            });

    }


    editUser = (record) => () => {
        let userData = record;
        userData['userStatus'] = 0;
        userData['type'] = 2;
        userData['lp'] = record;
        userData['editThroughModal'] = true
        this.setState({
            user: userData,
            userModal: true,
            tempObj: { ...userData },
            exisitingInvestorModal: false
        })
    }

    deleteLPUser = (record) => () => {
        let userData = record;
        userData['type'] = 3;
        userData['lp'] = record;
        userData['editThroughModal'] = true
        this.setState({
            user: userData,
            exisitingInvestorModal: false,
            userDeleteModal:true
        })
    }



    render() {
        return (
            <React.Fragment>
                <Modal id="GPModal" backdrop="static" show={this.state.userModal} onHide={this.closeUserModal} dialogClassName="GPModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.user.accountType === 'LPDelegate' ? this.FundUtil._getUserTitleNameLPDelegate(this.state.user.lp) : this.FundUtil._getUserTitleName(this.state.user)}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.FundUtil._getUserDesc(this.state.user)}</div>
                        <div className="form-main-div add-delegate">
                            <form>
                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.Constants.EMAIL}*</label>
                                        <FormControl type="text" name="email" placeholder={this.Constants.PLACEHOLDER_EMAIL} className={"inputFormControl " + ((this.state.user.emailTouched && !this.state.user.email) || (this.state.invalidEmail) && 'inputError')} value={this.state.user.email}
                                            onChange={this.handleChangeEvent('email')}
                                            onBlur={this.valueTouched}
                                        />
                                        {
                                            ((this.state.user.emailTouched && !this.state.user.email) || (this.state.invalidEmail)) &&
                                            <span className="error">{this.Constants.VALID_EMAIL}</span>
                                        }
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.Constants.FIRST_NAME}*</label>
                                        {
                                            this.state.user.userStatus == 0 ?
                                                <FormControl type="text" name="firstName" placeholder={this.Constants.PLACEHOLDER_FIRST_NAME} className={"inputFormControl " + ((this.state.user.firstNameTouched && !this.state.user.firstName) && 'inputError')} value={this.state.user.firstName}
                                                    onChange={this.handleChangeEvent('firstName')}
                                                    onBlur={this.valueTouched}
                                                />
                                                :
                                                <FormControl type="text" name="firstName" className="inputFormControl" value="" disabled />
                                        }

                                        {
                                            (this.state.user.firstNameTouched && !this.state.user.firstName) &&
                                            <span className="error">{this.Constants.FIRST_NAME_REQUIRED}</span>
                                        }
                                    </Col>
                                </Row>
                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.Constants.LAST_NAME}*</label>
                                        {
                                            this.state.user.userStatus == 0 ?
                                                <FormControl type="text" name="lastName" placeholder={this.Constants.PLACEHOLDER_LAST_NAME} className={"inputFormControl " + ((this.state.user.lastNameTouched && !this.state.user.lastName) && 'inputError')} value={this.state.user.lastName}
                                                    onChange={this.handleChangeEvent('lastName')}
                                                    onBlur={this.valueTouched}
                                                />
                                                :
                                                <FormControl type="text" name="lastName" className="inputFormControl" value="" disabled />
                                        }

                                        {
                                            (this.state.user.lastNameTouched && !this.state.user.lastName) &&
                                            <span className="error">{this.Constants.VALID_EMAIL}</span>
                                        }
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.Constants.MIDDLE_NAME}</label>
                                        {
                                            this.state.user.userStatus == 0 ?
                                                <FormControl type="text" name="middleName" className="inputFormControl" value={this.state.user.middleName}
                                                    onChange={this.handleChangeEvent('middleName')}
                                                    onBlur={this.valueTouched}
                                                />
                                                :
                                                <FormControl type="text" name="middleName" className="inputFormControl" value="" disabled />
                                        }
                                    </Col>
                                </Row>
                                {
                                    ['OfflineLP', 'LP'].indexOf(this.state.user.accountType) > -1 &&
                                    <Row className="marginBot20">
                                        <Col lg={6} md={6} sm={6} xs={12}>
                                            <label className="form-label">{this.Constants.ORG_NAME}</label>
                                            <FormControl type="text" name="organizationName" placeholder={this.Constants.PLACEHOLDER_ORG_NAME} className="inputFormControl" value={this.state.user.organizationName} onChange={this.handleChangeEvent('organizationName')} onBlur={this.valueTouched} />
                                        </Col>
                                        {
                                            this.state.user.accountType === 'OfflineLP' &&
                                            <Col lg={6} md={6} sm={6} xs={12} className="offlinePhone">
                                                <label className="form-label">{this.Constants.PHONE_NUMBER}</label>
                                                <PhoneInput maxLength="14" placeholder="(123) 456-7890" country="US" value={this.state.user.cellNumber} onChange={this.handleChangeEvent('cellNumber')} />
                                            </Col>
                                        }
                                    </Row>
                                }

                                {
                                    (this.state.user.accountType === 'OfflineLP' && this.state.user.type === 2) &&
                                    <Row className="marginBot20">
                                        <Col lg={6} md={6} sm={6} xs={12} className="isLpOfflineToOnline">
                                            <label className="form-label">Convert to Online Investor</label>
                                            <CBox onChange={this.handleConvertToOnline}>
                                                <span className="checkmark"></span>
                                            </CBox>
                                        </Col>
                                    </Row>
                                }
                            </form>
                            <div className="error paddingBot20">{this.state.userErrorMsz}</div>
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeUserModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button className={"fsnetSubmitButton " + (!this.FundUtil._hasNullValue(this.state.mandatoryFileds, this.state.user) ? 'btnEnabled' : 'disabled')} onClick={this.submitUserFn}>Save</Button>
                            </Col>
                        </Row>
                        {
                            (this.state.user.accountType === 'LP' && this.state.user.type === 1) &&
                            <Row className="marginLeftNone marginTop30">
                                <hr className="width250 marginLeftNone marginTop10" /> <span className="font14-bold">or</span> <hr className="width250 marginTop10" />
                                <h4 className="font20-bold">{this.Constants.EXISTING_INVESTORS}</h4>
                                <div className="subtext modal-subtext">{this.Constants.EXISTING_INVESTORS_TITLE}</div>
                                <Button className="fsnetSubmitButton btnEnabled" onClick={this.getExistingInvestors}>Select Investors</Button>
                            </Row>
                        }
                    </Modal.Body>
                </Modal>
                <Modal id="GPModal" backdrop="static" show={this.state.exisitingInvestorModal} onHide={this.closeExisitingInvestorModal} dialogClassName="GPModalDialog">
                    <Modal.Header closeButton> 
                    </Modal.Header>
                    <Modal.Title>{this.Constants.ADD_EXISTING_INVESTORS}</Modal.Title>
                    <Modal.Body className="fundInvesiorsPopup">
                        {
                            this.state.firmInvestorsList.length > 0 ?
                            <div>
                                <div className="tableEdit tableMarginTop mostly-customized-scrollbar">
                                    <Table striped bordered className="tableMargin mainTable" className="fundInvestorsTable">
                                        <thead>
                                            <tr>
                                                <th colspan="1"></th>
                                                <th colspan="3">Investor Name</th>
                                                <th colspan="3">Organization</th>
                                                <th colspan="3">Add to Fund</th>
                                                <th colspan="2">Edit</th>
                                                <th colspan="2">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody className="fundInvestors fundInvestorsBorder">
                                            {
                                                this.state.firmInvestorsList.map((record, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td colspan="1" style={{ paddingRight: '0px' }} className="userImageAlt">
                                                                {
                                                                    record['profilePic'] ?
                                                                        <img src={record['profilePic']['url']} alt="img" className="user-image" />
                                                                        :
                                                                        <img src={userDefaultImage} alt="img" className="user-image" />
                                                                        
                                                                        
                                                                }
                                                                
                                                            </td>
                                                            <td colspan="3"><h4 className="ellipsis" title={this.FsnetUtil.getFullName(record)}>{this.FsnetUtil.getFullName(record)}</h4> <h6>{this.FsnetUtil.getAccountTypeName(record.accountType)}</h6></td>
                                                            <td colspan="3"><h4>{record.organizationName}</h4></td>
                                                            <td colspan="3">
                                                                <CBox className="marginLeft10" key={index} checked={record['selected']} onChange={this.handleSelectInvestors(record)}>
                                                                    <span className="checkmark"></span>
                                                                </CBox>
                                                            </td>
                                                            <td colspan="2"><i className="font20 fa fa-pencil cursor paddingLeft10" aria-hidden="true" onClick={this.editUser(record)}></i></td>
                                                            <td colspan="2"><i className="lp-trash-icon fa fa-trash cursor paddingLeft10" aria-hidden="true" onClick={this.deleteLPUser(record)}></i></td>
                                                        </tr>
                                                    );
                                                })
                                            }
                                        </tbody>
                                    </Table>

                                </div>
                                <Row>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <Button type="button" className="fsnetCancelButton" onClick={this.closeExisitingInvestorModal}>Cancel</Button>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <Button className={"fsnetSubmitButton " + (this.getSelctedInvestors().length > 0 ? 'btnEnabled' : 'disabled')} onClick={this.addExisitingInvestors}>Add to Fund</Button>
                                    </Col>
                                </Row>
                            </div>

                            :
                            <h1 className="title text-center">{this.Constants.NO_EXISTING_INVESTORS}</h1>
                        }

                    </Modal.Body>
                </Modal>
                <Modal id="LPDelModal" backdrop="static" show={this.state.userDeleteModal} onHide={this.closeUserDeleteModal} dialogClassName="LPDelModalDialog lpDelFundModalDialog delModal">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.FundUtil._getUserDeleteTitleName(this.state.user)}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.FundUtil._getUserDeleteDesc(this.state.user)}</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeUserDeleteModal}>{this.Constants.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.deleteUser}>{this.Constants.CONFIRM_DELETION}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
            </React.Fragment>
        );
    }
}

export default userModalComponent;


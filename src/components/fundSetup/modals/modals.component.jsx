
import React, { Component } from 'react';
import '../fundSetup.component.css';
import SAComponent from '../SA/sa.component';
import { PubSub } from 'pubsub-js';
import { Button, Modal, Row, Col, Checkbox as CBox, FormControl } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { Constants } from '../../../constants/constants';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { reactLocalStorage } from 'reactjs-localstorage';
import Loader from '../../../widgets/loader/loader.component';
import LpTableComponent from '../../editfund/lptable/lptable.component';
import userDefaultImage from '../../../images/default_user.png';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';
import profilePictureLarge from '../../../images/profile-picture-large.svg';
import letterImage from '../../../images/letter.svg';
import smartPhone from '../../../images/smartphone.svg';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/rrui.css'
import 'react-phone-number-input/style.css'

var close, profileModal, lpProfileModal, openLpModal, removeClosedLpModal, openLpEditModal, openGpModal, openLpDelModal, openLpDelFundModal, openGpDelModal, openfundDelModal, openfundDetailModal, openCloseFundModal, editSubscriptionModal, approveLpsModal, openGpSignatoryModal, openGPSignatoryDelFundModal = {}
class ModalComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.FsnetAuth = new FsnetAuth();
        this.state = {
            showFundAgreementUploadText: true,
            fundAgreementUploadProceedBtnDisabled: false,
            show: false,
            GPshow: false,
            LPDelshow: false,
            GPDelshow: false,
            lpId: null,
            lpSelected: false,
            lpOrganizationName: '',
            isLpFormValid: false,
            firstNameBorder: false,
            fundDeactivateModal: false,
            fundCloseModal: false,
            lpApproveModal: false,
            signatureModal: false,
            errorModal: false,
            firstNameMsz: '',
            firstName: '',
            middleName: '',
            reason: '',
            closedLpObj: '',
            firstNameValid: false,
            lastNameBorder: false,
            lastNameMsz: '',
            lastName: '',
            lastNameValid: false,
            emailBorder: false,
            emailMsz: '',
            email: '',
            emailValid: false,
            cellNumberBorder: false,
            cellNumberMsz: '',
            cellNumber: '',
            status: 0,
            reasonValid: true,
            isValidated: false,
            cellNumberValid: false,
            closedLpModalshow: false,
            lpErrorMsz: '',
            isClosedLPError: '',
            confirmLPDelFundshow: false,
            lpSelectedList: [],
            lpScreenError: '',
            orgName: '',
            orgNameTemp: '',
            fundId: null,
            fundName: '',
            gpDelegateErrorMsz: '',
            isGpDelgateFormValid: false,
            lpDelegateId: '',
            gpDelegateId: '',
            fundObj: {},
            closedReadyUsersList: [],
            showCapitalCommitmentError: false,
            closedReadyUsersSelectedList: [],
            enableFundBtn: false,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            closeFundError: '',
            enableSignatureBtn: false,
            signaturePassword: '',
            signatureErrorMsz: '',
            lpNameProfileModal: false,
            selectedLpData: {},
            hasAuthenticationpassword: false,
            hasSignature: true,
            validationRuleError: false,
            showCloseFundAlert: false,
            showFundExceedError: false,
            closeFundValidationErrorMsz: '',
            closeFundProceedTitleTxt: '',
            INVESTORS_COUNT_EXCESS_99_MSZ: '',
            CLOSED_INVESTORS_COUNT_EXCESS_99: '',
            INVESTORS_COUNT_EXCESS_499_MSZ: '',
            ERISA_WARNINGS_MSZ: '',
            DISQUALIFYING_EVENT_MSZ: '',
            EEA_WARNINGS_MSZ: '',
            EQUITY_OWNERS_EVENT_MSZ: '',
            ACCREDITED_EVENT_MSZ: '',
            finalClosing: false,
            subscriptionFormModal: false,
            jsonData: {},
            closedReadyError: 'no_closed_ready_lps',
            accountType: null,
            GPSignatoryshow: false,
            gpSignatoryDelFundshow: false,
            checkSelectAll: false,
            lpType: null,
            isLpOfflineToOnline: false,
        }

        openLpModal = PubSub.subscribe('openLpModal', (msg, data) => {
            this.setState({
                fundId: data.fundObj.id,
                fundObj: data.fundObj,
                isOfflineLP: data.type == 'offlineLp' ? true : false,
            }, () => {
                this.handleShow();
            })
        });

        openLpEditModal = PubSub.subscribe('openLpEditModal', (msg, data) => {
            this.setState({
                fundId: data.fundObj.id,
                fundObj: data.fundObj,
                lpObj: data.lpObj,
                lpId: data.lpObj ? data.lpObj.id : '',
                email: data.lpObj ? data.lpObj.email : '',
                firstName: data.lpObj ? data.lpObj.firstName : '',
                middleName: data.lpObj ? data.lpObj.middleName : '',
                lastName: data.lpObj ? data.lpObj.lastName : '',
                lpSelected: data.lpObj ? data.lpObj.selected : '',
                lpOrganizationName: data.lpObj ? data.lpObj.organizationName : '',
                orgName: data.lpObj ? data.lpObj.organizationName : '',
                cellNumber: data.lpObj ? data.lpObj.cellNumber : '',
                lpType: data.type,
                isOfflineLP: data.isOfflineLP

            }, () => {
                ['email', 'firstName', 'lastName', 'orgName', 'cellNumber'].forEach(key => {
                    if (key == 'cellNumber') {
                        this.handleInputChangeEvent(this.state[key], key, null, null, true);
                    } else {
                        this.handleInputChangeEvent({ target: { value: this.state[key] } }, key, null, null, true);
                    }
                });
                this.handleEditShow();
            })
        });
        openGpModal = PubSub.subscribe('openGpModal', (msg, data) => {
            this.setState({
                fundId: data.id,
                fundObj: data
            }, () => {
                this.handleGpShow();
            })
        });
        openGpSignatoryModal = PubSub.subscribe('openGpSignatoryModal', (msg, data) => {
            this.setState({
                fundId: data.id,
                fundObj: data
            }, () => {
                this.handleGPSignatoryShow();
            })
        });
        openLpDelModal = PubSub.subscribe('openLpDelModal', (msg, data) => {
            this.setState({
                fundId: data.data.id,
                fundObj: data.data,
                lpDelegateId: data.delegateId
            }, () => {
                this.handleLpDelShow();
            })
        });
        openLpDelFundModal = PubSub.subscribe('openLpDelFundModal', (msg, data) => {
            this.setState({
                fundId: data.data.id,
                fundObj: data.data,
                lpDelegateId: data.delegateId,
                subscriptionId: data.subscriptionId,
                lpType: data.type,
                isOfflineLP: data.isOfflineLP
            }, () => {
                this.handleLpDelFundShow();
            })
        });
        openGPSignatoryDelFundModal = PubSub.subscribe('openGPSignatoryDelFundModal', (msg, data) => {
            this.setState({
                fundId: data.data.id,
                fundObj: data.data,
                gpSignatoryId: data.signatoryId
            }, () => {
                this.handleGpSignatoryDelFundShow();
            })
        });
        openGpDelModal = PubSub.subscribe('openGpDelModal', (msg, data) => {
            this.setState({
                fundId: data.data.id,
                fundObj: data.data,
                gpDelegateId: data.delegateId
            }, () => {
                this.handleGpDelShow();
            })
        });
        openfundDelModal = PubSub.subscribe('openfundDelModal', (msg, data) => {
            if (!this.state.fundDeactivateModal) {
                this.setState({
                    fundId: data.fundId,
                    fundName: data.fundName,
                    fundStatus: data.fundStatus
                }, () => {
                    this.handlefundDelShow();
                })
            }
        });

        openfundDetailModal = PubSub.subscribe('openfundDetailModal', (msg, data) => {
            if (!this.state.fundDeactivateModal) {
                this.setState({
                    fundId: data.fundId,
                    fundName: data.fundName,
                    fundStatus: data.fundStatus
                }, () => {
                    this.handlefundShow();
                })
            }
        });
        openCloseFundModal = PubSub.subscribe('openCloseFundModal', (msg, data) => {
            if (!this.state.fundCloseModal) {
                this.setState({
                    fundId: data.fundId
                }, () => {
                    this.handlefundCloseShow();
                })
            }
        });

        approveLpsModal = PubSub.subscribe('approveLpsModal', (msg, data) => {
            if (!this.state.lpApproveModal) {
                this.setState({
                    fundId: data.fundId
                }, () => {
                    this.handlefundLpApprovalCloseShow();
                })
            }
        })

        removeClosedLpModal = PubSub.subscribe('openRemoveClosedLpModal', (msg, data) => {
            this.setState({
                fundId: data.data.id,
                fundObj: data.data,
                closedLpObj: data.closedLpObj,
                closedLpObjId: data.closedLpId
            }, () => {
                this.handleRemoveClosedLpShow();
            })
        })

        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

        profileModal = PubSub.subscribe('profileModal', (msg, data) => {
            // let obj = data.record ? data.record : data
            // let type = data.record ? 'leftProfile' : 'lpProfile'
            this.lpNameProfile(data)
        });

        lpProfileModal = PubSub.subscribe('lpProfileModal', (msg, data) => {
            this.lpNameProfile(data)
        });

        editSubscriptionModal = PubSub.subscribe('editSubscriptionModal', (msg, data) => {
            this.openSubscriptionFormModal()
        });
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
        PubSub.unsubscribe(profileModal);
        PubSub.unsubscribe(lpProfileModal);
        PubSub.unsubscribe(openLpModal);
        PubSub.unsubscribe(openLpEditModal);
        PubSub.unsubscribe(openGpModal);
        PubSub.unsubscribe(openLpDelModal);
        PubSub.unsubscribe(openLpDelFundModal);
        PubSub.unsubscribe(openGpDelModal);
        PubSub.unsubscribe(openfundDelModal);
        PubSub.unsubscribe(openfundDetailModal);
        PubSub.unsubscribe(openCloseFundModal);
        PubSub.unsubscribe(editSubscriptionModal);
        PubSub.unsubscribe(openGpSignatoryModal);
        PubSub.unsubscribe(openGPSignatoryDelFundModal);
    }

    closeToast = (timed) => {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }



    //lpNameProfile Modal
    lpNameProfile = (data) => {
        this.setState({
            selectedLpData: data
        })
        this.getPublicProfile(data);
    }

    handleLpNameProfileClose = () => {
        this.setState({ lpNameProfileModal: false }, () => {
            setTimeout(() => {
                this.setState({
                    lpProfileData: {}
                })
            }, 200);
        });
    }

    componentDidMount() {
        if (this.FsnetAuth.isAdmin()) {
            this.setState({
                accountType: this.FsnetUtil.getUserRole()
            })
        } else {
            this.setState({
                accountType: this.FsnetUtil.getAdminRole()
            })
        }
        this.getJsonData();
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('modals').then(result => {
            this.setState({
                jsonData: result.data
            })
        });

    }

    getPublicProfile = (user, type) => {
        if (user) {

            this.open();
            this.Fsnethttp.getPublicProfile(user).then(result => {
                this.close();
                if (result.data) {
                    this.setState({
                        lpProfileData: result.data.data,
                        'lpNameProfileModal': true
                    })
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    deleteLp = () => {
        let lpId = this.state.lpDelegateId;

        let postObj = { fundId: this.state.fundId, lpId: lpId }
        this.open()
        this.Fsnethttp.removeLp(postObj).then(result => {
            this.close();
            if (result) {
                this.updateDeletedLpUserInFundObj(lpId)
            }
        })
            .catch(error => {
                this.close();
            });
    }

    deleteLpFromFund = (deleteClosedLpConfirm) => () => {
        this.setState({ isClosedLPError: '' });
        let lpId = this.state.lpDelegateId;
        let postObj = { fundId: this.state.fundId, lpId: lpId, subscriptionId: this.state.subscriptionId }
        this.open()
        const URL = this.state.lpType == 1 && !this.state.isOfflineLP && !deleteClosedLpConfirm ? 'lp/delete/soft' : this.state.isOfflineLP ? `fund/delete/offlineLp${deleteClosedLpConfirm ? '?deleteClosedLpConfirm=true' : ''}` : `fund/delete/lp${deleteClosedLpConfirm ? '?deleteClosedLpConfirm=true' : ''}`;
        this.Fsnethttp.removeLpFromFund(postObj, URL).then(result => {
            this.close();
            if (result.data) {
                let getFundObj = this.state.fundObj;
                const checkInFundLps = getFundObj.lps.fundLps.findIndex(obj => obj.subscriptionId == this.state.subscriptionId)
                const getObj = getFundObj.lps.fundLps[checkInFundLps]
                const checkInFirmLps = getFundObj.lps.firmLps.findIndex(obj => obj.id == lpId)
                if (this.state.lpType == 0) {
                    getFundObj.lps.firmLps.splice(checkInFirmLps, 1)
                } else {
                    getFundObj.lps.fundLps.splice(checkInFundLps, 1)
                    if (checkInFundLps != -1) {
                        if (!result.data.subscriptions && !this.state.isOfflineLP) {
                            getFundObj.lps.firmLps.push(getObj);
                        }
                    }
                }

                this.setState({
                    fundObj: getFundObj,
                    subscriptionId: null
                }, () => {
                    PubSub.publish('fundData', getFundObj);
                    this.handleLpDelFundClose();
                    this.handleConfirmLpDelFundClose();
                })

            }
        })
            .catch(error => {
                this.close();
                if (error.response && error.response.data && error.response.data.errors) {
                    this.setState({
                        confirmLPDelFundshow: true
                        // isClosedLPError: error.response.data.errors.isClosed ? this.Constants.LP_CLOSED_SUBSCRIPTION : ''
                    }, () => {
                        this.handleLpDelFundClose();
                    })
                }
            });
    }


    deleteGpSignatoryFromFund = () => {
        let postObj = { fundId: this.state.fundId, signatoryId: this.state.gpSignatoryId }
        this.open()
        this.Fsnethttp.removeGPSignatoryFromFund(postObj).then(result => {
            this.close();
            this.updateDeletedGpSignatoryUserInFundObj(this.state.gpSignatoryId)

        })
            .catch(error => {
                this.close();
                this.updateDeletedGpSignatoryUserInFundObj(this.state.gpSignatoryId)
            });
    }

    removeClosedLp = () => {
        let closedLpObjId = this.state.closedLpObjId;
        let postObj = { fundId: this.state.fundId, lpId: closedLpObjId }

        this.open();
        this.Fsnethttp.removeClosedLpFromFund(postObj).then(result => {
            this.close();
            if (result.data) {
                let closedLpObjId = this.state.closedLpObjId;
                let getFundObj = this.state.fundObj;
                if (getFundObj.lps.length) {
                    for (var i = 0; i < getFundObj.lps.length; i++) {
                        var lp = getFundObj.lps[i];
                        if (closedLpObjId == lp.id) {
                            lp.selected = false;
                            lp.disabled = true;
                        }
                    }
                }
                this.setState({
                    fundObj: getFundObj
                }, () => {
                    PubSub.publish('fundData', this.state.fundObj);
                    this.handleRemoveClosedLpClose();
                })
            }
        })
            .catch(error => {
                this.close();
            });

    }

    deleteGp = () => {
        let gpDelegateId = this.state.gpDelegateId

        let postObj = { fundId: this.state.fundId, delegateId: gpDelegateId }
        this.open()
        this.Fsnethttp.removeGp(postObj).then(result => {
            this.close();
            if (result) {
                this.updateDeletedGpUserInFundObj(gpDelegateId)
            }
        })
            .catch(error => {
                this.close();
            });
    }

    deactivateFund = () => {
        if (this.state.fundStatus != 'New-Draft' && this.state.fundStatus !== 'Admin-Draft') {
            if (this.state.reason == null || this.state.reason == undefined || this.state.reason == '') {
                this.setState({
                    reasonValid: false
                }, () => {
                    this.deactivateFundStatus();
                })
            } else {
                this.setState({
                    reasonValid: true
                }, () => {
                    this.deactivateFundStatus();
                })
            }
        } else {
            this.setState({
                reasonValid: true
            }, () => {
                this.deactivateFundStatus();
            })
        }
    }

    closeFundBtn = () => {
        this.setState({
            signatureModal: true,
            enableSignatureBtn: false,
            INVESTORS_COUNT_EXCESS_99_MSZ: '',
            CLOSED_INVESTORS_COUNT_EXCESS_99: '',
            INVESTORS_COUNT_EXCESS_499_MSZ: '',
            ERISA_WARNINGS_MSZ: '',
            DISQUALIFYING_EVENT_MSZ: '',
            EEA_WARNINGS_MSZ: '',
            EQUITY_OWNERS_EVENT_MSZ: '',
            ACCREDITED_EVENT_MSZ: '',
            fundAgreementUpload: false
        }, () => {
            this.handleCloseFundClose();
        });

    }

    approveClosingBtn = () => {
        this.setState({
            INVESTORS_COUNT_EXCESS_99_MSZ: '',
            CLOSED_INVESTORS_COUNT_EXCESS_99: '',
            INVESTORS_COUNT_EXCESS_499_MSZ: '',
            ERISA_WARNINGS_MSZ: '',
            DISQUALIFYING_EVENT_MSZ: '',
            EEA_WARNINGS_MSZ: '',
            EQUITY_OWNERS_EVENT_MSZ: '',
            ACCREDITED_EVENT_MSZ: ''
        }, () => {
            this.approveClosing();
        });
    }


    approveClosing = () => {
        let postObj = {};
        let filteredObj = this.state.closedReadyUsersList.map(user => {
            return { checked: user.checked, lpId: user.lpId, lpCapitalCommitment: user.lpCapitalCommitment, subscriptionId: user.subscriptionId, fundId: +this.state.fundId }
        });
        postObj.lpClosingData = filteredObj.filter(user => user.checked);
        postObj.fundId = parseInt(this.state.fundId);
        this.open();
        this.Fsnethttp.approveClosing(postObj).then(result => {
            this.close();
            this.setState({
                showToast: true,
                toastMessage: 'Successfully Approved Investors for Closing.',
                toastType: 'success',
                lpApproveModal: false,
                closeFundError: '',
                enableFundBtn: false
            }, () => {
            })
        })
            .catch(error => {
                this.close();
                this.setState({
                    showToast: true,
                    toastMessage: 'Error Approving Investors for Closing.',
                    toastType: 'danger',
                    lpApproveModal: false,
                    closeFundError: ''
                })
            });
    }

    handleSingaturePwd = (e) => {
        if (e.target.value.trim() !== '') {
            this.setState({
                signaturePassword: e.target.value.trim(),
                enableSignatureBtn: true
            })
        } else {
            this.setState({
                enableSignatureBtn: false
            })
        }
    }

    verifySignature = () => {
        if (this.state.signaturePassword !== '' && this.state.signaturePassword !== null) {
            this.open();
            let obj = { signaturePassword: this.state.signaturePassword }
            this.Fsnethttp.verifySignature(obj).then(result => {
                this.close();
                this.uploadSignatureBtn(false, false);
            })
                .catch(error => {
                    this.close();
                    if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                        this.setState({
                            signatureErrorMsz: error.response.data.errors[0].msg,
                        });
                    }
                });
        }
        else {
            this.setState({
                signatureErrorMsz: this.Constants.SIGNATURE_PWD,
            });
        }
    }

    closeFundSubmit = () => {
        this.showCloseFundProceedAlert();
    }

    showCloseFundProceedAlert = () => {
        this.setState({
            showCloseFundAlert: true
        })
    }

    closeCloseFundProceedAlert = () => {
        this.setState({
            showCloseFundAlert: false
        })
    }

    showFundAgreementModal = () => {
        this.setState({
            fundCloseModal: false,
            signatureModal: false,
        }, () => {
            this.fundAgreementUploadShow()
        })
    }

    uploadSignatureBtn = (proceedValue, withOutNewFundAgreement) => {
        let postObj;
        if (this.FsnetUtil.getUserRole() !== 'GP') {
            let selectedIds = [];
            for (let index of this.state.closedReadyUsersList) {
                if (index['checked'] === true) {
                    selectedIds.push(index.subscriptionId)
                }
            }
            postObj = { fundId: this.state.fundId, subscriptionIds: selectedIds, date: new Date(), timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone, signaturePassword: this.state.signaturePassword }
        } else {
            let list = [];
            for (let index of this.state.closedReadyUsersList) {
                if (index['checked'] === true) {
                    let obj = { subscriptionId: index.subscriptionId, fundId: parseInt(this.state.fundId), lpCapitalCommitment: index.lpCapitalCommitment, lpId: index.lpId, gpConsumed: index.gpConsumed ? index.gpConsumed : index.lpCapitalCommitment };
                    list.push(obj)
                }
            }
            var formData = new FormData();
            formData.append("fundClosingData", JSON.stringify(list));
            formData.append("fundId", parseInt(this.state.fundId));
            formData.append("proceed", proceedValue);
            formData.append("withOutNewFundAgreement", withOutNewFundAgreement);
            formData.append("finalClosing", this.state.finalClosing);
            if (proceedValue && this.state.uploadDocFile) {
                formData.append("fundDoc", this.state.uploadDocFile);
            }
            postObj = formData;
        }
        this.open();
        const url = this.FsnetUtil.getUserRole() === 'GP' ? 'closeFund' : 'gp/signatory/signature'
        this.Fsnethttp.closeFund(postObj, url).then(result => {
            this.close();
            this.setState({
                showToast: true,
                toastMessage: this.FsnetUtil.getUserRole() === 'GP' ? 'Fund has been closed successfully.' : 'Investors has been approved successfully.',
                toastType: 'success',
                fundCloseModal: false,
                signatureModal: false,
                closeFundError: '',
                showCloseFundAlert: false,
                fundAgreementUpload: false,
                uploadDocName: null,
                uploadDocFile: {}
            }, () => {
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            })
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    if (error.response.data.errors[0].CODES === 'DO_YOU_WANTTO_CHANGE_FA') {
                        this.showFundAgreementModal();
                    }
                    if (error.response.data.errors[0].CODE === 'AMOUNT_EXCESS_HARD_CAP') {
                        this.setState({
                            validationRuleError: true,
                            showFundExceedError: true
                        });
                    } else {
                        if (error.response.data.errors[0].CODES.includes('INVESTORS_COUNT_EXCESS_99')) {
                            this.updateCloseFundError(this.Constants.INVESTORS_COUNT_EXCESS_99, 'INVESTORS_COUNT_EXCESS_99_MSZ')
                        }
                        if (error.response.data.errors[0].CODES.includes('CLOSED_INVESTORS_COUNT_EXCESS_99')) {
                            this.updateCloseFundError(this.Constants.CLOSED_INVESTORS_COUNT_EXCESS_99 + `<a>${this.Constants.ACCREDITED_EVENT_2}</a>`, 'CLOSED_INVESTORS_COUNT_EXCESS_99_MSZ')
                        }
                        if (error.response.data.errors[0].CODES.includes('INVESTORS_COUNT_EXCESS_499')) {
                            this.updateCloseFundError(this.Constants.INVESTORS_COUNT_EXCESS_499, 'INVESTORS_COUNT_EXCESS_499_MSZ')
                        }
                        if (error.response.data.errors[0].CODES.includes('ERISA_WARNINGS')) {
                            this.updateCloseFundError(this.Constants.ERISA_WARNINGS, 'ERISA_WARNINGS_MSZ')
                        }
                        if (error.response.data.errors[0].CODES.includes('DISQUALIFYING_EVENT')) {
                            this.updateCloseFundError(this.Constants.DISQUALIFYING_EVENT, 'DISQUALIFYING_EVENT_MSZ')
                        }
                        if (error.response.data.errors[0].CODES.includes('ACCREDITED_EVENT')) {
                            let msz = this.Constants.ACCREDITED_EVENT_1.replace('non-accredited investor', '<b>non-accredited investor</b>') + `<a>${this.Constants.ACCREDITED_EVENT_2}</a>`
                            this.updateCloseFundError(msz, 'ACCREDITED_EVENT_MSZ')
                        }
                        if (error.response.data.errors[0].CODES.includes('EEA_WARNINGS')) {
                            let text = this.Constants.EEA_WARNINGS;
                            let warningsLength = error.response.data.errors[0].CODES.indexOf('EEA_WARNINGS') + 1;
                            const getLpNames = error.response.data.errors[0].CODES[warningsLength].lpNames.length
                            let names = this.concatInvestorNames('EEA_WARNINGS', error.response.data.errors[0].CODES)
                            let investorText = text.replace('[Investor Name]', names)
                            let fundText = investorText.replace('[Fund Name]', error.response.data.errors[0].CODES[warningsLength].fundName)
                            let replaceVerbal = fundText.replace('[verbal]', getLpNames > 1 ? 'are' : 'is')
                            let replaceVerbal1 = replaceVerbal.replace('[verbal1]', getLpNames > 1 ? 'are' : 'is')
                            this.updateCloseFundError(replaceVerbal1, 'EEA_WARNINGS_MSZ')
                        }
                        if (error.response.data.errors[0].CODES.includes('EQUITY_OWNERS_EVENT')) {
                            let equityText = this.Constants.EQUITY_OWNERS_EVENT;
                            let names = this.addInvestorNames('EQUITY_OWNERS_EVENT', error.response.data.errors[0].CODES)
                            let investorNames = equityText.replace('[Investor Name]', names)
                            let existingProspectivesText = this.mergeNumberOfexistingOrProspectives('EQUITY_OWNERS_EVENT', error.response.data.errors[0].CODES)
                            let finalText = investorNames.replace('[text_value]', existingProspectivesText)
                            this.updateCloseFundError(finalText, 'EQUITY_OWNERS_EVENT_MSZ')
                        }
                    }
                }
            });
    }

    addInvestorNames(type, obj) {
        let names = '';
        let codesLength = obj.indexOf(type) + 1;
        const lpNamesCount = obj[codesLength].investorNameArray.length
        for (let index = 0; index < lpNamesCount; index++) {
            if (index === 0) {
                names = names + obj[codesLength].investorNameArray[index]
            } else if (index != (lpNamesCount - 1)) {
                names = names + ', ' + obj[codesLength].investorNameArray[index]
            } else {
                names = names + ' and ' + obj[codesLength].investorNameArray[index]
            }
        }
        return names ? names : null
    }

    mergeNumberOfexistingOrProspectives(type, obj) {
        let names = '';
        let codesLength = obj.indexOf(type) + 1;
        const lpNamesCount = obj[codesLength].numberOfexistingOrProspectives.length
        for (let index = 0; index < lpNamesCount; index++) {
            if (index === 0) {
                names = names + obj[codesLength].numberOfexistingOrProspectives[index]
            } else if (index != (lpNamesCount - 1)) {
                names = names + ', ' + obj[codesLength].numberOfexistingOrProspectives[index]
            } else {
                names = names + ' and ' + obj[codesLength].numberOfexistingOrProspectives[index]
            }
        }
        return names ? names : null
    }



    concatInvestorNames(type, obj) {
        let names = '';
        let codesLength = obj.indexOf(type) + 1;
        const lpNamesCount = obj[codesLength].lpNames.length
        for (let index = 0; index < lpNamesCount; index++) {
            if (index === 0) {
                names = names + obj[codesLength].lpNames[index]
            } else if (index != (lpNamesCount - 1)) {
                names = names + ', ' + obj[codesLength].lpNames[index]
            } else {
                names = names + ' and ' + obj[codesLength].lpNames[index]
            }
        }
        return names ? names : null
    }

    updateCloseFundError = (msz, type) => {
        this.setState({
            validationRuleError: true,
            showFundExceedError: false,
            [type]: msz,
        });
    }

    proceedCloseFund = () => {
        this.uploadSignatureBtn(false, true);
    }

    gpSignatoryApproveClosing = () => {
        const users = this.state.closedReadyUsersList.map(user => { return user.subscriptionId })
        const postObj = {
            fundId: this.state.fundId, subscriptionIds: users, date: new Date(),
            timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone, signaturePassword: this.state.signaturePassword
        }
        this.open();
        this.Fsnethttp.approveClosingGpSignatory(postObj).then(result => {
            this.close();
            this.setState({
                showToast: true,
                toastMessage: 'Investors has been approved successfully.',
                toastType: 'success',
                fundCloseModal: false,
                signatureModal: false,
                closeFundError: ''
            }, () => {
                setTimeout(() => {
                    // window.location.reload();
                }, 1000);
            })
        })
            .catch(error => {
                this.close();
            })
    }

    handleInputCloseFund = (event, data, type) => {
        let getList = this.state.closedReadyUsersList;
        let value = event.target.value.trim();
        let status = 0;
        for (let index of getList) {
            if (index['subscriptionId'] === data.record['subscriptionId'] && value <= index['lpCapitalCommitment']) {
                index['gpConsumed'] = value;
                if (value !== '') {
                    index['gpConsumedCurrency'] = type === 'blur' ? this.FsnetUtil.convertToCurrency(value) : value;
                } else {
                    index['gpConsumedCurrency'] = value;
                }
                this.setState({
                    closeFundError: ''
                });
            } else if (index['subscriptionId'] === data.record['subscriptionId'] && value > index['lpCapitalCommitment']) {
                index['gpConsumed'] = value
                if (value !== '') {
                    index['gpConsumedCurrency'] = type === 'blur' ? this.FsnetUtil.convertToCurrency(value) : value;
                } else {
                    index['gpConsumedCurrency'] = value;
                }
                // this.errorModalShow();
                this.setState({
                    closeFundError: 'Capital Commitment accepted cannot exceed capital commitment offered.'
                });
                status++
            }
        }

        this.setState({
            closedReadyUsersList: getList
        }, () => {
            this.enableCloseFundWhileCBox();
        })
    }

    handleFocusCloseFund = (event, data) => {
        let getList = this.state.closedReadyUsersList;
        for (let index of getList) {
            if (index['subscriptionId'] === data.record['subscriptionId']) {
                index['gpConsumedCurrency'] = index['gpConsumed']
            }
        }
        this.setState({
            closedReadyUsersList: getList
        });
    }

    handleCheckBoxChange = (event, data) => {
        let count = 0;
        if (data === 'finalClosing') {
            this.setState({
                finalClosing: event.target.checked
            })
        } else {
            let getSelectedList = this.state.closedReadyUsersList;
            let subscriptionId = data.record.subscriptionId
            if (event.target.checked) {
                for (let index of getSelectedList) {
                    if (index['subscriptionId'] === subscriptionId) {
                        count++;
                        index['checked'] = true
                    }
                }
            } else {
                for (let index of getSelectedList) {
                    if (index['subscriptionId'] === subscriptionId) {
                        index['checked'] = false
                    }
                }
            }

            const getSelectedCount = getSelectedList.filter(obj => obj.checked == true)

            this.setState({
                closedReadyUsersList: getSelectedList,
                checkSelectAll: this.state.closedReadyUsersList.length === getSelectedCount.length ? true : false
            }, () => {
                this.enableCloseFundWhileCBox();
            })
        }

    }

    enableCloseFundWhileCBox = () => {
        let length = this.state.closedReadyUsersList.length;
        let count = 0;
        let checked = 0;
        //For GP only we need to check this.
        if (this.FsnetUtil.getUserRole() === 'GP') {
            // for (let index of this.state.closedReadyUsersList) {
            //     if (index['checked'] === true && (index['gpConsumed'] > index['lpCapitalCommitment'])) {
            //         count++;
            //     } else if (index['checked'] === false || index['checked'] === undefined) {
            //         checked++;
            //     }
            // }
            // if (count > 0 || checked === length) {
            //     this.setState({
            //         enableFundBtn: false
            //     })
            // } else {
            //     this.setState({
            //         enableFundBtn: true,
            //     })
            // }
            let getCheckedUser = this.state.closedReadyUsersList.filter(data => data.checked == true && data['gpConsumed'] <= data['lpCapitalCommitment']);
            this.setState({
                enableFundBtn: getCheckedUser.length > 0 ? true : false
            })

        } else {
            let getCheckedUser = this.state.closedReadyUsersList.filter(data => data.checked == true);
            this.setState({
                enableFundBtn: getCheckedUser.length > 0 ? true : false
            })

        }
    }

    enableCloseFundBtn = () => {
        let length = this.state.closedReadyUsersList.length;
        let count = 0;
        for (let index of this.state.closedReadyUsersList) {
            if (index['checked'] === false || index['checked'] === undefined) {
                count++;
            }
        }
        if (count === length) {
            this.setState({
                enableFundBtn: false
            })
        } else {
            this.setState({
                enableFundBtn: true,
            })
        }
    }

    deactivateFundStatus = () => {

        let postObj = { fundId: this.state.fundId, deactivateReason: this.state.reason };
        if (this.state.reasonValid) {
            this.open()
            this.Fsnethttp.deactivateFund(postObj).then(result => {
                this.close();
                if (result) {
                    this.handlefundClose(true);
                    // this.updateDeletedGpUserInFundObj(gpDelegateId)
                }
            })
                .catch(error => {
                    this.close();
                    // this.handlefundClose(true);
                });
        }
    }

    updateDeletedGpUserInFundObj = (id) => {
        let obj = this.state.fundObj;
        let deletedId = id;
        for (let index of obj.gpDelegates) {
            if (index['id'] === deletedId) {
                index['selected'] = false
                this.setState({
                    fundObj: obj
                }, () => {
                    PubSub.publish('fundData', this.state.fundObj);
                    this.handleGpDelClose();
                })
            }
        }
    }


    updateDeletedLpUserInFundObj = (id) => {
        let obj = this.state.fundObj;
        let deletedId = id;
        for (let index of obj.lps) {
            if (index['id'] === deletedId) {
                index['selected'] = false
                this.setState({
                    fundObj: obj
                }, () => {
                    PubSub.publish('fundData', this.state.fundObj);
                    this.handleLpDelClose();
                })
            }
        }
    }


    updateDeletedGpSignatoryUserInFundObj = (id) => {
        let obj = this.state.fundObj;
        let deletedId = id;
        let newList = obj.gpSignatories.filter(obj => obj.id != deletedId);
        obj['gpSignatories'] = newList;
        this.setState({
            fundObj: obj
        }, () => {
            PubSub.publish('fundData', this.state.fundObj);
            this.handleGpSignatoryDelFundClose();
        })
    }



    handleClose = () => {
        this.clearFormFileds();
        this.setState({ show: false }, () => {
            let touchedArr = ['firstName', 'lastName', 'cellNumber', 'orgName', 'email'];
            touchedArr.forEach(key => {
                this.setState({
                    [key + 'Touched']: false
                })
            })
        });
    }

    closeOfflineLpModal = () => {
        this.clearFormFileds();
        this.setState({ offlineLpModal: false }, () => {
            let touchedArr = ['firstName', 'lastName', 'cellNumber', 'orgName', 'email'];
            touchedArr.forEach(key => {
                this.setState({
                    [key + 'Touched']: false
                })
            })
        });
    }

    handleEditClose = () => {
        this.clearFormFileds();
        this.setState({ showEdit: false }, () => {
            let touchedArr = ['firstName', 'lastName', 'email', 'orgName'];
            touchedArr.forEach(key => {
                this.setState({
                    [key + 'Touched']: false
                })
            })
        });
    }

    handleShow = () => {
        this.setState({ show: true, isLpFormValid: false }, () => {
            this.offLineLpModalCss();
        });
    }

    offLineLpModalCss = () => {
        setTimeout(() => {
            if (window.innerHeight <= 700 && this.state.isOfflineLP) {
                document.getElementsByClassName('modal-body')[0].style.maxHeight = '500px';
                document.getElementsByClassName('modal-body')[0].style.overflowY = 'scroll';
                document.getElementById('LPModal', 'modal-header').classList.add("offlineLpStyle");
            }
        }, 200);
    }

    handleOfflineLpShow = () => {
        this.setState({ offlineLpModal: true, isLpFormValid: false });
    }

    handleEditShow = () => {
        this.setState({ showEdit: true, isLpFormValid: false });
    }

    handleGpClose = () => {
        this.clearFormFileds();
        this.setState({ GPshow: false });
    }
    handleGPSignatoryClose = () => {
        this.clearFormFileds();
        this.setState({ GPSignatoryshow: false });
    }

    handleGpShow = () => {
        this.setState({ GPshow: true });
    }
    handleGPSignatoryShow = () => {
        this.setState({ GPSignatoryshow: true });
    }
    handleLpDelShow = () => {
        this.setState({ LPDelshow: true });
    }
    handleLpDelFundShow = () => {
        this.setState({ LPDelFundshow: true });
    }
    handleGpSignatoryDelFundShow = () => {
        this.setState({ gpSignatoryDelFundshow: true });
    }
    handleGpSignatoryDelFundClose = () => {
        this.setState({ gpSignatoryDelFundshow: false });
    }
    handlefundShow = () => {
        this.setState({ lpFundModal: true });
    }
    restCloseFundErrors = () => {
        this.setState({ fundCloseModal: false, closedReadyUsersList: [], closeFundError: '', enableFundBtn: false, validationRuleError: false, finalClosing: false, checkSelectAll: false });
    }
    resetLpApprovalErrors = () => {
        this.setState({ lpApproveModal: false, closedReadyUsersList: [], closeFundError: '', enableFundBtn: false, validationRuleError: false, finalClosing: false });
    }
    handleCloseFundClose = () => {
        this.setState({ fundCloseModal: false });
    }

    handleApproveClosingClose = () => {
        this.setState({
            lpApproveModal: false, showToast: true,
            toastMessage: 'Error Approving Investors for Closing.',
            toastType: 'danger'
        });
    }

    handleCloseFundOpen = () => {
        this.setState({ fundCloseModal: true });
    }
    handleSignatureClose = () => {
        this.setState({ signatureModal: false, signatureErrorMsz: '', validationRuleError: false }, () => {
            this.handleCloseFundOpen();
        });
    }
    errorModalClose = () => {
        this.setState({ errorModal: false });
    }
    errorModalShow = () => {
        this.setState({
            errorModal: true,
            enableFundBtn: false
        })
    }
    handlefundClose = (redirect) => {
        this.setState({ lpFundModal: false }, () => {
            if (redirect) {
                PubSub.publish('goToDashboard');
                // this.props.history.push('/dashboard');
            }
        });
    }

    handleLpDelClose = () => {
        this.setState({ LPDelshow: false });
    }

    handleLpDelFundClose = () => {
        this.setState({ LPDelFundshow: false, isClosedLPError: '' });
    }

    handleConfirmLpDelFundClose = () => {
        this.setState({ confirmLPDelFundshow: false, isClosedLPError: '', subscriptionId: null });
    }

    handleRemoveClosedLpShow = () => {
        this.setState({ closedLpModalshow: true });
    }

    handleRemoveClosedLpClose = () => {
        this.setState({ closedLpModalshow: false });
    }

    handleGpDelShow = () => {
        this.setState({ GPDelshow: true });
    }
    handlefundDelShow = () => {
        this.setState({ fundDeactivateModal: true });
    }
    handlefundCloseShow = () => {
        this.checkCloseReadyUsers();
    }
    handlefundLpApprovalCloseShow = () => {
        this.getCloseReadyUsersforLpApproval();
        // this.checkCloseReadyUsers();
    }


    handleGpDelClose = () => {
        this.setState({ GPDelshow: false });
    }
    handlefundDelClose = () => {
        this.setState({ reasonTouched: false, reasonValid: true, fundDeactivateModal: false });
    }

    getCloseReadyUsers = () => {
        this.open();
        this.setState({
            finalClosing: false,
            checkSelectAll: false,
            enableFundBtn: false
        })
        this.Fsnethttp.getClosedReadyLps(this.state.fundId).then(result => {
            this.close();
            let data = result.data;
            for (let index of data) {
                index['gpConsumed'] = index['lpCapitalCommitment'];
                index['gpConsumedCurrency'] = this.FsnetUtil.convertToCurrency(index['lpCapitalCommitment']);
            }
            this.setState({
                closedReadyUsersList: data,
                fundCloseModal: true
            }, () => {
                if (this.state.closedReadyUsersList.length > 0) {
                    this.checkSignature();
                }
            })

        })
            .catch(error => {
                this.close();
                this.setState({
                    closedReadyUsersList: [],
                    fundCloseModal: true,
                })
            });
    }

    checkCloseReadyUsers = () => {
        this.getCloseReadyUsers();
        // this.open();
        // this.Fsnethttp.checkClosedReadyLps(this.state.fundId).then(result => {
        //     this.close();
        // })
        // .catch(error => {
        //     this.close();
        //     this.setState({
        //         closedReadyUsersList: [],
        //         fundCloseModal: true,
        //         closedReadyError: error.response && error.response.data.errors && error.response.data.errors[0] ? error.response.data.errors[0].code : 'no_closed_ready_lps'
        //     })
        // });
    }

    getCloseReadyUsersforLpApproval = () => {
        this.open();

        this.Fsnethttp.getClosedReadyLps(this.state.fundId).then(result => {
            this.close();
            let data = result.data;
            for (let index of data) {
                index['fundId'] = +this.state.fundId;
            }
            this.setState({
                closedReadyUsersList: data,
                lpApproveModal: true
            })

        })
            .catch(error => {
                this.close();
                this.setState({
                    closedReadyUsersList: [],
                    lpApproveModal: true
                })
            });
    }

    updateStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, () => {
            this.enableDisableSubmitButton();
        });
    }
    updateEditStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, () => {
            this.enableDisableEditSubmitButtion();
        });
    }
    enableDisableSubmitButton = () => {
        let status = (this.state.firstNameValid && this.state.lastNameValid && this.state.emailValid) ? true : false;
        this.setState({
            isLpFormValid: status
        });
    }

    enableDisableEditSubmitButtion = () => {
        let status = (this.state.firstNameValid && this.state.lastNameValid && this.state.emailValid) ? true : false;
        this.setState({
            isLpFormValid: status
        });
    }

    handleConvertToOnline = (event) => {
        this.setState({
            isLpOfflineToOnline: event.target.checked
        })
    }

    //Onchange event for all input text boxes.
    handleInputChangeEvent = (event, type, obj, eventType, isLpEdit) => {
        window.scrollTo(0, 0);
        let dataObj = {};
        this.setState({
            lpErrorMsz: ''
        })
        switch (type) {
            case 'firstName':
                if (event.target.value.trim() === '' || event.target.value === undefined) {
                    this.setState({
                        firstNameMsz: this.Constants.FIRST_NAME_REQUIRED,
                        firstNameValid: false,
                        firstNameBorder: true,
                        firstName: ''
                    })
                    dataObj = {
                        firstNameValid: false
                    };
                    if (isLpEdit) {
                        this.updateEditStateParams(dataObj);
                    } else {
                        this.updateStateParams(dataObj);
                    }
                } else {
                    this.setState({
                        firstName: event.target.value,
                        firstNameValid: true,
                        firstNameBorder: false,
                        firstNameMsz: ''

                    })
                    dataObj = {
                        firstNameValid: true
                    };
                    if (isLpEdit) {
                        this.updateEditStateParams(dataObj);
                    } else {
                        this.updateStateParams(dataObj);
                    }
                }
                break;
            case 'lastName':
                if (event.target.value.trim() === '' || event.target.value === undefined) {
                    this.setState({
                        lastNameMsz: this.Constants.LAST_NAME_REQUIRED,
                        lastNameValid: false,
                        lastNameBorder: true,
                        lastName: ''
                    })
                    dataObj = {
                        lastNameValid: false
                    };
                    if (isLpEdit) {
                        this.updateEditStateParams(dataObj);
                    } else {
                        this.updateStateParams(dataObj);
                    }
                } else {
                    this.setState({
                        lastName: event.target.value,
                        lastNameMsz: '',
                        lastNameValid: true,
                        lastNameBorder: false,
                    })
                    dataObj = {
                        lastNameValid: true
                    };
                    if (isLpEdit) {
                        this.updateEditStateParams(dataObj);
                    } else {
                        this.updateStateParams(dataObj);
                    }
                }
                break;
            case 'address':
                if (!event.target.value) {
                    this.setState({
                        address: ''
                    })
                } else {
                    this.setState({
                        address: event.target.value,
                    })
                }
                break;
            case 'email':
                if (event.target.value === undefined || event.target.value.trim() === '') {
                    this.setState({
                        emailMsz: this.Constants.VALID_EMAIL,
                        emailValid: false,
                        emailBorder: true,
                        email: ''
                    })
                    dataObj = {
                        emailValid: false
                    };
                    if (isLpEdit) {
                        this.updateEditStateParams(dataObj);
                    } else {
                        this.updateStateParams(dataObj);
                    }
                } else {
                    let emailRegex = this.Constants.EMAIL_REGEX;
                    if (event.target.value !== '' && emailRegex.test(event.target.value)) {
                        this.setState({
                            email: event.target.value,
                            emailMsz: '',
                            emailValid: true,
                            emailBorder: false,

                        }, () => {
                            // //console.log('make call');
                            if (eventType == 'blur' && !isLpEdit) {
                                this.checkEmail('lp');
                            }
                        })
                        dataObj = {
                            emailValid: true
                        };
                    } else {
                        this.setState({
                            email: event.target.value,
                            emailMsz: this.Constants.VALID_EMAIL,
                            isValidated: false,
                            emailValid: false,
                            emailBorder: true,
                        })
                        dataObj = {
                            emailValid: false
                        };
                    }
                    if (isLpEdit) {
                        this.updateEditStateParams(dataObj);
                    } else {
                        this.updateStateParams(dataObj);
                    }
                }
                break;
            case 'orgName':
                if (!event.target.value) {
                    this.setState({
                        orgName: null
                    })
                } else {
                    this.setState({
                        orgName: event.target.value.trim() != '' ? event.target.value : event.target.value.trim()
                    })
                }
                break;
            case 'cellNumber':
                if (event === '' || event === undefined) {
                    this.setState({
                        cellNumberMsz: this.Constants.CELL_NUMBER_VALID,
                        cellNumberValid: false,
                        cellNumberBorder: true,
                        cellNumber: ''
                    })
                    dataObj = {
                        cellNumberValid: false
                    };
                    if (isLpEdit) {
                        this.updateEditStateParams(dataObj);
                    } else {
                        this.updateStateParams(dataObj);
                    }
                } else {
                    this.setState({
                        cellNumber: event,
                        cellNumberMsz: '',
                        cellNumberValid: true,
                        cellNumberBorder: false,
                    })
                    dataObj = {
                        cellNumberValid: true
                    };
                    if (isLpEdit) {
                        this.updateEditStateParams(dataObj);
                    } else {
                        this.updateStateParams(dataObj);
                    }
                }
                break;
            default:
                // do nothing
                break;
        }
    }

    handleInputEventChange = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name]: value,
            [name + 'Touched']: true
        })
    }

    handleInputChangeEventGP = (event, type, obj, eventType) => {
        let dataObj = {};
        this.setState({
            gpDelegateErrorMsz: ''
        })
        switch (type) {
            case 'firstName':
                if (event.target.value.trim() === '' || event.target.value === undefined) {
                    this.setState({
                        firstNameMsz: this.Constants.FIRST_NAME_REQUIRED,
                        firstNameValid: false,
                        firstNameBorder: true,
                        firstName: ''
                    })
                    dataObj = {
                        firstNameValid: false
                    };
                    this.updateGPStateParams(dataObj);
                } else {
                    this.setState({
                        firstName: event.target.value,
                        firstNameMsz: '',
                        firstNameValid: true,
                        firstNameBorder: false,
                    })
                    dataObj = {
                        firstNameValid: true
                    };
                    this.updateGPStateParams(dataObj);
                }
                break;
            case 'middleName':
                this.setState({
                    middleName: (event.target.value && !event.target.value.trim()) || !event.target.value ? null : event.target.value
                })
                break;
            case 'lastName':
                if (event.target.value.trim() === '' || event.target.value === undefined) {
                    this.setState({
                        lastNameMsz: this.Constants.LAST_NAME_REQUIRED,
                        lastNameValid: false,
                        lastNameBorder: true,
                        lastName: ''
                    })
                    dataObj = {
                        lastNameValid: false
                    };
                    this.updateGPStateParams(dataObj);
                } else {
                    this.setState({
                        lastName: event.target.value,
                        lastNameMsz: '',
                        lastNameValid: true,
                        lastNameBorder: false,
                    })
                    dataObj = {
                        lastNameValid: true
                    };
                    this.updateGPStateParams(dataObj);
                }
                break;
            case 'email':
                if (event.target.value.trim() === '' || event.target.value === undefined) {
                    this.setState({
                        emailMsz: this.Constants.VALID_EMAIL,
                        emailValid: false,
                        emailBorder: true,
                        email: ''
                    })
                    dataObj = {
                        emailValid: false
                    };
                    this.updateGPStateParams(dataObj);
                } else {
                    let emailRegex = this.Constants.EMAIL_REGEX;
                    if (event.target.value !== '' && emailRegex.test(event.target.value)) {
                        this.setState({
                            email: event.target.value,
                            emailMsz: '',
                            emailValid: true,
                            emailBorder: false,

                        }, () => {
                            //console.log('make call');
                            if (eventType == 'blur' && eventType != 'signatory') {
                                this.checkEmail('gpDelegate');
                            } else if (eventType == 'signatory' && eventType == 'blur') {
                                this.checkEmail('gpSignatory');
                            }
                        })
                        dataObj = {
                            emailValid: true
                        };
                    } else {
                        this.setState({
                            email: event.target.value,
                            emailMsz: this.Constants.VALID_EMAIL,
                            isValidated: false,
                            emailValid: false,
                            emailBorder: true,
                        })
                        dataObj = {
                            emailValid: false
                        };
                    }

                    this.updateGPStateParams(dataObj);
                }
                break;
            case 'user':
                let getSelectedList = this.state.gpDelegatesSelectedList;
                let selectedId = obj.record.id
                if (event.target.checked) {
                    if (getSelectedList.indexOf(selectedId) === -1) {
                        getSelectedList.push(selectedId);
                    }
                } else {
                    var index = getSelectedList.indexOf(selectedId);
                    if (index !== -1) {
                        getSelectedList.splice(index, 1);
                    }
                }
                this.updateSelectedValueToList(obj.record.id, event.target.checked)
                this.setState({
                    gpDelegatesSelectedList: getSelectedList,
                    gpDelegateScreenError: ''
                })
                break;
            default:
                // do nothing
                break;
        }
    }

    checkEmail = (type) => {
        let postObj = { email: this.state.email, fundId: this.state.fundId };

        this.open()
        let url = type == 'gpDelegate' ? 'delegate/gp/check' : (type == 'lpDelegate' ? 'delegate/lp/check' : type == 'gpSignatory' ? 'signatory/gp/check' : this.state.isOfflineLP ? '/lp/check/isNewOfflineLP' : 'lp/check');
        this.Fsnethttp.checkEmail(url, postObj).then(result => {
            this.close();
            if (result.data) {
                //console.log('data::::', result.data);
                result.data.cellNumber = result.data.cellNumber;
                result.data.firstName = result.data.status == 0 ? this.state.firstName : result.data.firstName;
                result.data.lastName = result.data.status == 0 ? this.state.lastName : result.data.lastName;
                result.data.middleName = result.data.status == 0 ? this.state.middleName : result.data.middleName;
                this.setState({
                    status: result.data.status,
                    isValidated: true,
                    firstName: result.data.firstName,
                    lastName: result.data.lastName,
                    middleName: result.data.middleName,
                    cellNumber: result.data.cellNumber,
                    orgName: result.data.organizationName || null,
                    orgNameTemp: result.data.organizationName || null,
                }, () => {
                    if (this.state.status === 2) {
                        if (type == 'gpDelegate') {
                            this.setState({
                                gpDelegateErrorMsz: this.Constants.DELEGATE_EXISTS,
                            });
                        } else {
                            this.setState({
                                lpErrorMsz: this.Constants.DELEGATE_EXISTS,
                            });
                        }
                    }
                    Object.keys(result.data).forEach(key => {
                        // //console.log(key, obj[data])
                        if (key != 'email') {
                            if (type == 'gpDelegate') {
                                if (key == 'cellNumber') {
                                    this.handleInputChangeEventGP(result.data[key], key);
                                } else {
                                    this.handleInputChangeEventGP({ target: { value: result.data[key] } }, key);
                                }
                            } else {
                                if (key == 'cellNumber') {
                                    this.handleInputChangeEvent(result.data[key], key);
                                } else {
                                    this.handleInputChangeEvent({ target: { value: result.data[key] } }, key);
                                }
                            }
                        } else {
                            // this.setState({
                            //     email: result.data.email,
                            //     isValidated: false,
                            //     emailMsz: '',
                            //     emailValid: true,
                            //     emailBorder: false,
                            // })
                            // let dataObj ={
                            //     emailValid :true
                            // };
                            // this.updateGPStateParams(dataObj);
                        }
                    })
                    //console.log('this.state::::', this.state);
                })
            }
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    if (type == 'gpDelegate') {
                        this.setState({
                            gpDelegateErrorMsz: error.response.data.errors[0].msg,
                            status: true
                        });
                    } else {
                        this.setState({
                            lpErrorMsz: error.response.data.errors[0].msg,
                            status: true
                        });
                    }
                } else {
                    if (type == 'gpDelegate') {
                        this.setState({
                            gpDelegateErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                            status: true
                        });
                    } else {
                        this.setState({
                            lpErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                            status: true
                        });
                    }

                }
            });
    }

    clearFormFileds = () => {
        this.setState({
            gpDelegateErrorMsz: '',
            firstName: '',
            lastName: '',
            middleName: '',
            email: '',
            orgName: '',
            cellNumber: '',
            firstNameMsz: '',
            firstNameBorder: false,
            lastNameBorder: false,
            lastNameMsz: '',
            emailBorder: false,
            emailMsz: '',
            cellNumberBorder: false,
            cellNumberMsz: '',
            lpErrorMsz: '',
            isLpFormValid: false,
            isGpDelgateFormValid: false,
            status: 0,
            isLpOfflineToOnline: false,
        });
    }

    // Update state params values and login button visibility

    updateGPStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, () => {
            this.enableGPDisableSubmitButtion();
        });
    }

    // Enable / Disble functionality of Submit Button
    enableGPDisableSubmitButtion = () => {
        let status = (this.state.firstNameValid && this.state.lastNameValid && this.state.emailValid) ? true : false;
        this.setState({
            isGpDelgateFormValid: status
        });
    }

    addLpFn = () => {
        let firstName = this.state.firstName;
        let lastName = this.state.lastName;
        let middleName = this.state.middleName;
        let email = this.state.email;
        let error = false;
        let emailRegex = this.Constants.EMAIL_REGEX;
        if (email !== '' && !emailRegex.test(email)) {
            this.setState({
                emailMsz: this.Constants.VALID_EMAIL,
                emailBorder: true,
            })
            error = true;
        }

        if (this.state.cellNumber && this.state.isOfflineLP) {
            if (this.state.cellNumber.length < 12 || this.state.cellNumber.length > 13) {
                this.setState({
                    lpErrorMsz: this.Constants.CELL_NUMBER_VALID
                })
                error = true;
            }
        }

        if (!error) {
            let postObj = { firstName: firstName, lastName: lastName, middleName: middleName, email: email, fundId: this.state.fundId, vcfirmId: this.FsnetUtil.getFirmId(), organizationName: this.state.orgName };
            if (this.state.isOfflineLP) {
                postObj['cellNumber'] = this.state.cellNumber;
            }
            this.open()
            const url = this.state.isOfflineLP ? 'fund/add/offlineLp' : 'fund/add/lp'
            this.Fsnethttp.addLp(postObj, url).then(result => {
                this.close();

                if (this.state.isOfflineLP && result.data.data.subscription[0] && this.state.fundObj.status != 'New-Draft' && this.state.fundObj.status !== 'Admin-Draft') {
                    window.location.href = `/lp/investorInfo/${result.data.data.subscription[0].id}`
                } else {
                    if (result.data) {
                        let lpObj = result.data.data;
                        lpObj['profilePic'] = null;
                        lpObj['isInvestorInvited'] = 1;
                        lpObj['subscriptionId'] = result.data.data.subscriptionId;
                        lpObj['lpDelegatesList'] = [];
                        let getFundObj = this.state.fundObj;
                        getFundObj.lps.fundLps.unshift(lpObj);
                        this.setState({
                            fundObj: getFundObj
                        }, () => {
                            PubSub.publish('fundData', this.state.fundObj);
                            this.handleClose();
                        })
                    }
                }
            })
                .catch(error => {
                    this.close();
                    if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                        this.setState({
                            lpErrorMsz: error.response.data.errors[0].msg,
                        });
                    } else {
                        this.setState({
                            lpErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                        });
                    }
                });
        }
    }

    editLpFn = () => {
        let lpId = this.state.lpId;
        let firstName = this.state.firstName;
        let lastName = this.state.lastName;
        let middleName = this.state.middleName;
        let email = this.state.email;
        let cellNumber = this.state.cellNumber;
        let orgName = this.state.orgName;
        let error = false;
        let emailRegex = this.Constants.EMAIL_REGEX;
        if (email !== '' && !emailRegex.test(email)) {
            this.setState({
                emailMsz: this.Constants.VALID_EMAIL,
                emailBorder: true,
            })
            error = true;
        }

        if (this.state.cellNumber && this.state.isOfflineLP) {
            if (this.state.cellNumber.length < 12 || this.state.cellNumber.length > 13) {
                this.setState({
                    lpErrorMsz: this.Constants.CELL_NUMBER_VALID
                })
                error = true;
            }
        }

        if (!error) {
            let postObj = { fundId: this.state.fundId, lpId: lpId, firstName: firstName, lastName: lastName, middleName:middleName, organizationName: this.state.orgName, email: email, tempObj: this.state.lpObj };
            this.open()
            if (this.state.isOfflineLP) {
                postObj['cellNumber'] = this.state.cellNumber;
                postObj['isLpOfflineToOnline'] = this.state.isLpOfflineToOnline
            }
            this.open()
            const url = this.state.isOfflineLP ? 'fund/update/offlineLp' : 'fund/update/lp'
            this.Fsnethttp.updateLp(postObj, url).then(result => {
                this.close();
                if (result.data) {
                    let getFundObj = this.state.fundObj;
                    const checkInFundLps = getFundObj.lps.fundLps.findIndex(obj => obj.id == lpId)
                    let lpType = ''
                    if (checkInFundLps != -1 || this.state.isOfflineLP) {
                        lpType = 'fundLps'
                    } else {
                        lpType = 'firmLps'
                    }
                    getFundObj.lps[lpType].forEach((lp) => {
                        if (lp && lp.id == this.state.lpId) {
                            lp.id = this.state.isLpOfflineToOnline ? result.data.id : this.state.lpId;
                            lp.email = email;
                            lp.firstName = firstName;
                            lp.lastName = lastName;
                            lp.middleName = middleName;
                            lp.organizationName = orgName
                            lp.cellNumber = this.state.isOfflineLP ? this.state.cellNumber : null
                            lp.accountType = !this.state.isLpOfflineToOnline && this.state.isOfflineLP ? 'OfflineLP' : 'LP'
                            if (this.state.isLpOfflineToOnline) {
                                lp.isInvestorInvited = result.data.isInvestorInvited
                                lp.status = result.data.status
                            }
                        }
                    })
                    this.setState({
                        fundObj: getFundObj
                    }, () => {
                        PubSub.publish('fundData', this.state.fundObj);
                        this.handleEditClose();
                    })
                }
            })
                .catch(error => {
                    this.close();
                    if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                        this.setState({
                            lpErrorMsz: error.response.data.errors[0].msg,
                        });
                    } else {
                        this.setState({
                            lpErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                        });
                    }
                });
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    addDelegateFn = () => {
        let firstName = this.state.firstName;
        let lastName = this.state.lastName;
        let middleName = this.state.middleName;
        let email = this.state.email;
        let error = false;
        let emailRegex = this.Constants.EMAIL_REGEX;
        if (email !== '' && !emailRegex.test(email)) {
            this.setState({
                emailMsz: this.Constants.VALID_EMAIL,
                emailBorder: true,

            })
            error = true;
        }

        if (!error) {
            let postObj = { firstName: firstName, lastName: lastName, middleName: middleName, email: email, fundId: this.state.fundId, vcfirmId: this.FsnetUtil.getFirmId() };
            this.open()
            this.Fsnethttp.addGpDelegate(postObj).then(result => {
                this.close();
                if (result.data) {
                    let gpObj = result.data.data;
                    gpObj['selected'] = true;
                    gpObj['profilePic'] = null;
                    gpObj['gPDelegateRequiredConsentHoldClosing'] = true;
                    gpObj['gPDelegateRequiredDocuSignBehalfGP'] = false;
                    let getFundObj = this.state.fundObj;
                    getFundObj.gpDelegates.unshift(gpObj);
                    this.setState({
                        fundObj: getFundObj
                    }, () => {
                        // //console.log(this.state.fundObj)
                        PubSub.publish('fundData', this.state.fundObj);
                        this.handleGpClose();
                    })

                }
            })
                .catch(error => {
                    this.close();
                    if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                        this.setState({
                            gpDelegateErrorMsz: error.response.data.errors[0].msg,
                        });
                    } else {
                        this.setState({
                            gpDelegateErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                        });
                    }
                });
        }
    }

    addSignatoryFn = () => {
        let firstName = this.state.firstName;
        let lastName = this.state.lastName;
        let email = this.state.email;
        let middleName = this.state.middleName;
        let error = false;
        let emailRegex = this.Constants.EMAIL_REGEX;
        if (email !== '' && !emailRegex.test(email)) {
            this.setState({
                emailMsz: this.Constants.VALID_EMAIL,
                emailBorder: true,
            })
            error = true;
        }

        if (!error) {
            let postObj = { firstName: firstName, lastName: lastName, middleName: middleName, email: email, fundId: this.state.fundId, vcfirmId: this.FsnetUtil.getFirmId() };
            this.open()
            this.Fsnethttp.addGpSignatory(postObj).then(result => {
                this.close();
                if (result.data) {
                    let gpSignatoryObj = result.data.data;
                    gpSignatoryObj['selected'] = true;
                    gpSignatoryObj['profilePic'] = null;
                    let getFundObj = this.state.fundObj;
                    getFundObj.gpSignatories.unshift(gpSignatoryObj);
                    this.setState({
                        fundObj: getFundObj
                    }, () => {
                        PubSub.publish('fundData', this.state.fundObj);
                        this.handleGPSignatoryClose();
                    })
                }
            })
                .catch(error => {
                    this.close();
                    if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                        this.setState({
                            gpDelegateErrorMsz: error.response.data.errors[0].msg,
                        });
                    } else {
                        this.setState({
                            gpDelegateErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                        });
                    }
                });
        }
    }

    handleFocus = (key) => {
        this.setState({
            [key + 'Touched']: true
        })
    }

    checkPassword = () => {

        this.Fsnethttp.checkSignaturePassword().then(result => {
            if (result.data) {
                this.setState({
                    hasAuthenticationpassword: !result.error
                })
            }
        })
            .catch(error => {
                this.close();
            });
    }

    checkSignature = () => {
        const userId = this.FsnetUtil.getUserId()
        this.Fsnethttp.getUserProfile(userId).then(result => {
            this.close();
            this.setState({
                hasSignature: result.data.signaturePic ? true : false,
            })
        })
            .catch(error => {
                this.close();
            });
    }

    closeSubscriptionFormModal = () => {
        this.setState({ subscriptionFormModal: false })
    }

    openSubscriptionFormModal = () => {
        this.setState({ subscriptionFormModal: true })
    }

    handleSelectAll = () => {
        let idx = 0;
        let getSelectedList = this.state.closedReadyUsersList;
        for (let index of getSelectedList) {
            if (index.canSign) {
                index['checked'] = !this.state.checkSelectAll
                idx++;
            } else {
                index['checked'] = false
            }
        }
        this.setState({
            closedReadyUsersList: getSelectedList,
            checkSelectAll: idx == 0 ? false : !this.state.checkSelectAll
        }, () => {
            this.enableCloseFundWhileCBox();
        })
    }

    fundAgreementUploadClose = () => { this.uploadSignatureBtn(true, true); }
    fundAgreementUploadShow = () => { this.setState({ fundAgreementUpload: true }) }

    closeFundAgreementUpload = () => { this.setState({ fundAgreementUpload: false, showFundAgreementUploadText: true, validationRuleError: false, hasSignature: true }, () => { this.handleCloseFundOpen() }) }

    fundAgreementUploadProceed = () => { this.setState({ showFundAgreementUploadText: !this.state.showFundAgreementUploadText, uploadDocName: null, uploadDocFile: {} }) }

    uploadDoc = () => {
        document.getElementById('uploadBtn').click();
    }

    getDocTempFile = () => {
        this.open();
        var formData = new FormData();
        formData.append("fundDoc", this.state.uploadDocFile);
        this.Fsnethttp.getDocTempFile(formData).then(result => {
            this.close();
            this.setState({
                uploadDocFileUrl: result.data.url
            })
        })
            .catch(error => {
                this.close();
            });
    }

    //Upload patnership document.
    handleChange = (event) => {
        let obj = event.target.files
        let reader = new FileReader();
        if (obj && obj.length > 0) {
            this.uploadFile = obj[0];
            let sFileName = obj[0].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            if (sFileExtension !== 'pdf') {
                this.clearFile()
                alert(this.Constants.UPLOAD_DOC_REQUIRED)
                return true;
            }
            //File 100 MB limit
            if (this.uploadFile.size <= this.Constants.SIZE_LIMIT) {
                reader.readAsDataURL(this.uploadFile);
                this.setState({
                    uploadDocFile: obj[0],
                    // fundAgreementUploadProceedBtnDisabled: false,
                    // fundAgreementUpload: false,
                    uploadDocName: obj[0].name
                }, () => {
                    this.clearFile()
                    this.getDocTempFile();
                    // this.uploadSignatureBtn(true, true);
                });

            } else {
                this.clearFile()
                alert(this.Constants.UPLOAD_DOC_REQUIRED)
            }
        }
    }

    proceedClosingWithFundDoc = () => {
        this.setState({
            fundAgreementUploadProceedBtnDisabled: false,
            fundAgreementUpload: false,
        }, () => {
            this.uploadSignatureBtn(true, true);
        })
    }

    showFundUploadMainPage = () => {
        this.setState({
            uploadDocFile: {},
            uploadDocName: null
        })
    }

    openUploadDoc = () => {
        this.FsnetUtil._openDoc(this.state.uploadDocFileUrl)
    }

    clearFile = () => {
        document.getElementById('uploadBtn').value = "";
    }

    deleteFile = () => {
        this.setState({
            uploadDocFile: {},
            uploadDocName: null
        })
    }

    render() {
        return (
            <div>
                {/* add Lp */}
                <Modal id="LPModal" backdrop="static" className={(this.state.isOfflineLP ? 'offlineModal' : '')} show={this.state.show} onHide={this.handleClose} dialogClassName={"LPModalDialog " + (this.state.isOfflineLP ? 'offlineDialog' : '')}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.isOfflineLP ? this.state.jsonData.ADD_OFFLINE_INVESTOR : this.state.jsonData.ADD_INVESTOR}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.isOfflineLP ? this.state.jsonData.ADD_OFFLINE_INVESTOR_TITLE : this.state.jsonData.FILL_FORM_TO_ADD_INVESTOR}</div>
                        <div className="form-main-div add-delegate">
                            <form>
                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.EMAIL_ADDRESS}*</label>
                                        <FormControl type="email" name="email" placeholder={this.state.jsonData.PLACEHOLDER_EMAIL_ADDRESS} className={"inputFormControl " + (this.state.emailBorder ? 'inputError' : '')} value={this.state.email} onChange={(e) => this.handleInputChangeEvent(e, 'email')} onBlur={(e) => this.handleInputChangeEvent(e, 'email', null, 'blur')} onFocus={(e) => this.handleFocus('email')} />
                                        <span className="error">{this.state.emailMsz}</span>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.FIRST_NAME}*</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="firstName" placeholder={this.state.jsonData.PLACEHOLDER_FIRST_NAME} className={"inputFormControl " + (this.state.firstNameTouched && this.state.firstNameBorder ? 'inputError' : '')} value={this.state.firstName} onChange={(e) => this.handleInputChangeEvent(e, 'firstName')} onBlur={(e) => this.handleInputChangeEvent(e, 'firstName')} onFocus={(e) => this.handleFocus('firstName')} />
                                                :
                                                <FormControl type="text" name="firstName" className="inputFormControl" value="" disabled />
                                        }
                                        <span className="error" hidden={!this.state.firstNameTouched}>{this.state.firstNameMsz}</span>
                                    </Col>

                                </Row>
                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.LAST_NAME}*</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="lastName" placeholder={this.state.jsonData.PLACEHOLDER_LAST_NAME} className={"inputFormControl " + (this.state.lastNameTouched && this.state.lastNameBorder ? 'inputError' : '')} value={this.state.lastName} onChange={(e) => this.handleInputChangeEvent(e, 'lastName')} onBlur={(e) => this.handleInputChangeEvent(e, 'lastName')} onFocus={(e) => this.handleFocus('lastName')} />
                                                :
                                                <FormControl type="text" name="lastName" className="inputFormControl" value="" disabled />
                                        }
                                        <span className="error" hidden={!this.state.lastNameTouched}>{this.state.lastNameMsz}</span>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.MIDDLE_NAME}</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="middleName" placeholder={this.state.jsonData.PLACEHOLDER_MIDDLE_NAME} className="inputFormControl" value={this.state.middleName} onChange={(e) => this.handleInputChangeEventGP(e, 'middleName')} onBlur={(e) => this.handleInputChangeEventGP(e, 'middleName')} />
                                                :
                                                <FormControl type="text" name="lastName" className="inputFormControl" value="" disabled />
                                        }
                                    </Col>

                                </Row>

                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.ORGANIZATION_NAME}</label>
                                        <FormControl type="text" name="orgName" placeholder={this.state.jsonData.ORGANIZATION_NAME} className="inputFormControl" value={this.state.orgName} onChange={(e) => this.handleInputChangeEvent(e, 'orgName')} onFocus={(e) => this.handleFocus('orgName')} />
                                    </Col>
                                    {
                                        this.state.isOfflineLP &&
                                        <Col lg={6} md={6} sm={6} xs={12} className="offlinePhone">
                                            <label className="form-label">Phone Number (Cell)</label>
                                            <PhoneInput maxLength="14" placeholder="(123) 456-7890" country="US" value={this.state.cellNumber || ''} onChange={phone => this.handleInputChangeEvent(phone, 'cellNumber')} />
                                        </Col>
                                    }
                                </Row>
                            </form>
                            <div className="error marginBot20">{this.state.lpErrorMsz}</div>
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button className={"fsnetSubmitButton " + (this.state.isLpFormValid ? 'btnEnabled' : '')} disabled={!this.state.isLpFormValid} onClick={this.addLpFn}>{this.state.jsonData.SUBMIT}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* edit Lp */}
                <Modal id="LPModal" className={(this.state.isOfflineLP ? 'offlineEditModal' : '')} backdrop="static" show={this.state.showEdit} onHide={this.handleEditClose} dialogClassName="LPModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.EDIT_INVESTOR}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.FILL_FORM_TO_ADD_INVESTOR}</div>
                        <div className="form-main-div add-delegate">
                            <form>
                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.EMAIL_ADDRESS}*</label>
                                        <FormControl type="email" name="email" placeholder={this.state.jsonData.PLACEHOLDER_EMAIL_ADDRESS} className={"inputFormControl " + (this.state.emailBorder ? 'inputError' : '')} value={this.state.email} onChange={(e) => this.handleInputChangeEvent(e, 'email', null, null, true)} onBlur={(e) => this.handleInputChangeEvent(e, 'email', null, 'blur', true)} onFocus={(e) => this.handleFocus('email')} />
                                        <span className="error">{this.state.emailMsz}</span>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.FIRST_NAME}*</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="firstName" placeholder={this.state.jsonData.PLACEHOLDER_FIRST_NAME} className={"inputFormControl " + (this.state.firstNameTouched && this.state.firstNameBorder ? 'inputError' : '')} value={this.state.firstName} onChange={(e) => this.handleInputChangeEvent(e, 'firstName', null, null, true)} onBlur={(e) => this.handleInputChangeEvent(e, 'firstName', null, null, true)} onFocus={(e) => this.handleFocus('firstName')} />
                                                :
                                                <FormControl type="text" name="firstName" className="inputFormControl" value="" disabled />
                                        }
                                        <span className="error" hidden={!this.state.firstNameTouched}>{this.state.firstNameMsz}</span>
                                    </Col>
                                </Row>
                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.LAST_NAME}*</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="lastName" placeholder={this.state.jsonData.PLACEHOLDER_LAST_NAME} className={"inputFormControl " + (this.state.lastNameTouched && this.state.lastNameBorder ? 'inputError' : '')} value={this.state.lastName} onChange={(e) => this.handleInputChangeEvent(e, 'lastName', null, null, true)} onBlur={(e) => this.handleInputChangeEvent(e, 'lastName', null, null, true)} onFocus={(e) => this.handleFocus('lastName')} />
                                                :
                                                <FormControl type="text" name="lastName" className="inputFormControl" value="" disabled />
                                        }
                                        <span className="error" hidden={!this.state.lastNameTouched}>{this.state.lastNameMsz}</span>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.MIDDLE_NAME}</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="middleName" placeholder={this.state.jsonData.PLACEHOLDER_MIDDLE_NAME} className="inputFormControl" value={this.state.middleName} onChange={(e) => this.handleInputChangeEventGP(e, 'middleName')} onBlur={(e) => this.handleInputChangeEventGP(e, 'middleName')} />
                                                :
                                                <FormControl type="text" name="middleName" className="inputFormControl" value="" disabled />
                                        }
                                    </Col>
                                   
                                </Row>
                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.ORGANIZATION_NAME}</label>
                                        <FormControl type="text" name="orgName" placeholder={this.state.jsonData.ORGANIZATION_NAME} className="inputFormControl" value={this.state.orgName} onChange={(e) => this.handleInputChangeEvent(e, 'orgName', null, null, true)} onBlur={(e) => this.handleInputChangeEvent(e, 'orgName', null, null, true)} onFocus={(e) => this.handleFocus('orgName')} />
                                    </Col>
                                    {
                                        this.state.isOfflineLP &&
                                        <Col lg={6} md={6} sm={6} xs={12} className="offlinePhone">
                                            <label className="form-label">Phone Number (Cell)</label>
                                            <PhoneInput maxLength="14" placeholder="(123) 456-7890" country="US" value={this.state.cellNumber || ''} onChange={phone => this.handleInputChangeEvent(phone, 'cellNumber')} />
                                        </Col>
                                    }

                                </Row>
                                {
                                    this.state.isOfflineLP &&
                                    <Row className="marginBot20">
                                        <Col lg={6} md={6} sm={6} xs={12} className="isLpOfflineToOnline">
                                            <label className="form-label">Convert to Online Investor</label>
                                            <CBox onChange={this.handleConvertToOnline}>
                                                <span className="checkmark"></span>
                                            </CBox>
                                        </Col>
                                    </Row>
                                }
                            </form>
                            <div className="error">{this.state.lpErrorMsz}</div>
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleEditClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button className={"fsnetSubmitButton " + (this.state.isLpFormValid ? 'btnEnabled' : '')} disabled={!this.state.isLpFormValid} onClick={this.editLpFn}>{this.state.jsonData.SUBMIT}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <Modal id="GPModal" backdrop="static" show={this.state.GPshow} onHide={this.handleGpClose} dialogClassName="GPModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.ADD_DELEGATE}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.FILL_FORM_TO_ADD_NEW_DELEGATE}</div>
                        <div className="form-main-div add-delegate">
                            <form>

                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.EMAIL_ADDRESS}*</label>
                                        <FormControl type="email" name="email" placeholder={this.state.jsonData.PLACEHOLDER_EMAIL_ADDRESS} className={"inputFormControl " + (this.state.emailBorder ? 'inputError' : '')} value={this.state.email} onChange={(e) => this.handleInputChangeEventGP(e, 'email')} onBlur={(e) => this.handleInputChangeEventGP(e, 'email', null, 'blur')} />
                                        <span className="error">{this.state.emailMsz}</span>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.FIRST_NAME}*</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="firstName" placeholder={this.state.jsonData.PLACEHOLDER_FIRST_NAME} className={"inputFormControl " + (this.state.firstNameTouched && this.state.firstNameBorder ? 'inputError' : '')} value={this.state.firstName} onChange={(e) => this.handleInputChangeEventGP(e, 'firstName')} onBlur={(e) => this.handleInputChangeEventGP(e, 'firstName')} onFocus={(e) => this.handleFocus('firstName')} />
                                                :
                                                <FormControl type="text" name="firstName" className="inputFormControl" value="" disabled />
                                        }

                                        <span className="error" hidden={!this.state.firstNameTouched}>{this.state.firstNameMsz}</span>
                                    </Col>

                                </Row>
                                <Row className="marginBot20">

                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.LAST_NAME}*</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="lastName" placeholder={this.state.jsonData.PLACEHOLDER_LAST_NAME} className={"inputFormControl " + (this.state.lastNameTouched && this.state.lastNameBorder ? 'inputError' : '')} value={this.state.lastName} onChange={(e) => this.handleInputChangeEventGP(e, 'lastName')} onBlur={(e) => this.handleInputChangeEventGP(e, 'lastName')} onFocus={(e) => this.handleFocus('lastName')} />
                                                :
                                                <FormControl type="text" name="lastName" className="inputFormControl" value="" disabled />
                                        }
                                        <span className="error" hidden={!this.state.lastNameTouched}>{this.state.lastNameMsz}</span>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.MIDDLE_NAME}</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="middleName" placeholder={this.state.jsonData.PLACEHOLDER_MIDDLE_NAME} className="inputFormControl " value={this.state.middleName} onChange={(e) => this.handleInputChangeEventGP(e, 'middleName')} onBlur={(e) => this.handleInputChangeEventGP(e, 'middleName')} />
                                                :
                                                <FormControl type="text" name="middleName" className="inputFormControl" value="" disabled />
                                        }
                                    </Col>
                                </Row>
                            </form>
                            <div className="error">{this.state.gpDelegateErrorMsz}</div>
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleGpClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button className={"fsnetSubmitButton " + (this.state.isGpDelgateFormValid ? 'btnEnabled' : '')} disabled={!this.state.isGpDelgateFormValid} onClick={this.addDelegateFn}>{this.state.jsonData.SUBMIT}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <Modal id="GPModal" backdrop="static" show={this.state.GPSignatoryshow} onHide={this.handleGPSignatoryClose} dialogClassName="GPModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.ADD_SIGNATORY}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.FILL_FORM_TO_ADD_NEW_SIGNATORY}</div>
                        <div className="form-main-div add-delegate">
                            <form>

                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.EMAIL_ADDRESS}*</label>
                                        <FormControl type="email" name="email" placeholder={this.state.jsonData.PLACEHOLDER_EMAIL_ADDRESS} className={"inputFormControl " + (this.state.emailBorder ? 'inputError' : '')} value={this.state.email} onChange={(e) => this.handleInputChangeEventGP(e, 'email', null, 'signatory')} onBlur={(e) => this.handleInputChangeEventGP(e, 'email', null, 'blur')} />
                                        <span className="error">{this.state.emailMsz}</span>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.FIRST_NAME}*</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="firstName" placeholder={this.state.jsonData.PLACEHOLDER_FIRST_NAME} className={"inputFormControl " + (this.state.firstNameTouched && this.state.firstNameBorder ? 'inputError' : '')} value={this.state.firstName} onChange={(e) => this.handleInputChangeEventGP(e, 'firstName')} onBlur={(e) => this.handleInputChangeEventGP(e, 'firstName')} onFocus={(e) => this.handleFocus('firstName')} />
                                                :
                                                <FormControl type="text" name="firstName" className="inputFormControl" value="" disabled />
                                        }

                                        <span className="error" hidden={!this.state.firstNameTouched}>{this.state.firstNameMsz}</span>
                                    </Col>
                                </Row>
                                <Row className="marginBot20">
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.LAST_NAME}*</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="lastName" placeholder={this.state.jsonData.PLACEHOLDER_LAST_NAME} className={"inputFormControl " + (this.state.lastNameTouched && this.state.lastNameBorder ? 'inputError' : '')} value={this.state.lastName} onChange={(e) => this.handleInputChangeEventGP(e, 'lastName')} onBlur={(e) => this.handleInputChangeEventGP(e, 'lastName')} onFocus={(e) => this.handleFocus('lastName')} />
                                                :
                                                <FormControl type="text" name="lastName" className="inputFormControl" value="" disabled />
                                        }

                                        <span className="error" hidden={!this.state.lastNameTouched}>{this.state.lastNameMsz}</span>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12}>
                                        <label className="form-label">{this.state.jsonData.MIDDLE_NAME}</label>
                                        {
                                            this.state.status == 0 ?
                                                <FormControl type="text" name="middleName" placeholder={this.state.jsonData.PLACEHOLDER_MIDDLE_NAME} className="inputFormControl" value={this.state.middleName} onChange={(e) => this.handleInputChangeEventGP(e, 'middleName')} onBlur={(e) => this.handleInputChangeEventGP(e, 'middleName')} />
                                                :
                                                <FormControl type="text" name="middleName" className="inputFormControl" value="" disabled />
                                        }
                                    </Col>
                                </Row>
                            </form>
                            <div className="error">{this.state.gpDelegateErrorMsz}</div>
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleGPSignatoryClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button className={"fsnetSubmitButton " + (this.state.isGpDelgateFormValid ? 'btnEnabled' : '')} disabled={!this.state.isGpDelgateFormValid} onClick={this.addSignatoryFn}>{this.state.jsonData.SUBMIT}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* deleet lp selected for fund */}
                <Modal id="LPDelModal" backdrop="static" show={this.state.LPDelshow} onHide={this.handleLpDelClose} dialogClassName="LPDelModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.DELETE_INVESTOR}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.ARE_YOU_SURE_DELETE_INVESTOR}</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleLpDelClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.deleteLp}>{this.state.jsonData.DELETE}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* delete lp in fund */}
                <Modal id="LPDelModal" backdrop="static" show={this.state.LPDelFundshow} onHide={this.handleLpDelFundClose} dialogClassName={"LPDelModalDialog lpDelFundModalDialog " + (this.state.lpType == 1 ? '' : 'delModal')}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.lpType == 1 ? 'Remove Investor' : this.state.jsonData.DELETE_INVESTOR}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.lpType == 1 ? 'This option will remove the Investor, including all in-progress investments, from this Fund.' : this.state.jsonData.OPTION_WILL_DELETE_INVESTOR}</div>
                        <div className="form-main-div">
                        </div>
                        <p className="error">{this.state.isClosedLPError}</p>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleLpDelFundClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.deleteLpFromFund(false)}>{this.state.jsonData.CONFIRM_DELETION}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* delete GP Secondary Signatory in fund */}
                <Modal id="LPDelModal" backdrop="static" show={this.state.gpSignatoryDelFundshow} onHide={this.handleGpSignatoryDelFundClose} dialogClassName="LPDelModalDialog lpDelFundModalDialog delModal">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.DELETE_SIGNATORY}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.SIGNATORY_DELETE}</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleGpSignatoryDelFundClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.deleteGpSignatoryFromFund}>{this.state.jsonData.CONFIRM_DELETION}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* confirm delete lp in fund if closed */}
                <Modal id="LPDelModal" backdrop="static" show={this.state.confirmLPDelFundshow} onHide={this.handleConfirmLpDelFundClose} dialogClassName="LPDelModalDialog lpdelCnfrmModal lpDelFundModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.lpType == 1 ? 'Remove Investor' : this.state.jsonData.DELETE_INVESTOR}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.Constants.LP_CLOSED_SUBSCRIPTION}</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleConfirmLpDelFundClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.deleteLpFromFund(true)}>{this.state.jsonData.DELETE}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* confirm remove closed lp from fund */}
                <Modal id="LPDelModal" backdrop="static" show={this.state.closedLpModalshow} onHide={this.handleRemoveClosedLpClose} dialogClassName="LPDelModalDialog lpdelCnfrmModal lpDelFundModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.DELETE_INVESTOR}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.CONFIRM_DELETE_CLOSED_INVESTOR}</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleRemoveClosedLpClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled proceed-btn" onClick={this.removeClosedLp}>{this.state.jsonData.PROCEED_REMOVAL}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <Modal id="GPDelModal" backdrop="static" show={this.state.GPDelshow} onHide={this.handleGpDelClose} dialogClassName="GPDelModalDialog gpDelegateDelete">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.DELETE_DELEGATE}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.ARE_YOU_SURE_DELETE_DELEGATE}</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handleGpDelClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.deleteGp}>{this.state.jsonData.DELETE}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <Modal id="GPDelModal" backdrop="static" className="GPDelModalDeactivate" show={this.state.fundDeactivateModal} onHide={this.handlefundDelClose} dialogClassName="GPDelModalDialog fundModalDialog ">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.DEACTIVATE} {this.state.fundName}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">{this.state.jsonData.ARE_YOU_SURE_WANT_DEACTIVATE} {this.state.fundName}?</div>
                        <div className="form-main-div">
                            {/* { this.state.fundStatus != 'Open' && this.state.fundStatus != 'Open-Ready'
                            ? */}
                            <form>
                                <div className="marginBot20">
                                    <label className="form-label">{this.state.jsonData.REASON}*:</label>
                                    <FormControl componentClass="textarea" placeholder={this.state.jsonData.PLACEHOLDER_REASON} name="reason" value={this.state.reason} className={"inputFormControl textarea col-md-11 " + (!this.state.reasonValid ? 'inputError' : '')} onChange={(e) => this.handleInputEventChange(e)} onFocus={(e) => { this.setState({ reasonValid: true }) }} />
                                    {!this.state.reasonValid ? <span className="error height10">{this.Constants.REASON_REQUIRED}</span> : null}
                                </div>
                            </form>
                            {/* :
                                null
                            } */}

                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handlefundDelClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.deactivateFund}>{this.state.jsonData.DEACTIVATE}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/*  Close Fund Modal */}
                <Modal id="GPDelModal" backdrop="static" show={this.state.fundCloseModal} onHide={this.restCloseFundErrors} dialogClassName="GPDelModalDialog closeFundModal fundModalDialog approveGpModal reponsiveFundModal" className={"GPDelModalMarginLeft " + (this.state.closedReadyUsersList.length === 0 && this.state.closedReadyError !== 'no_closed_ready_lps' ? 'height230' : '') + (this.state.closedReadyUsersList.length === 0 && this.state.closedReadyError === 'no_closed_ready_lps' ? 'height130' : '')}>
                    <Modal.Header className="heightNone" closeButton>
                    </Modal.Header>
                    <Modal.Title className={this.state.closedReadyUsersList.length === 0 ? 'marginTopN20' : ''}>{this.FsnetUtil.getUserRole() === 'SecondaryGP' ? 'Approve For Closing' : this.state.jsonData.CLOSE_FUND}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext closeFund-Title" hidden={this.state.closedReadyUsersList.length === 0}>{this.FsnetUtil.getUserRole() === 'SecondaryGP' ? 'Are you sure you want to Approve For Closing?' : this.state.jsonData.ARE_YOU_SURE_WANT_CLOSE_FUND}</div>
                        <div className="subtext modal-subtext finalClosing" hidden={this.state.closedReadyUsersList.length === 0 || this.FsnetUtil.getUserRole() !== 'GP'}>{this.state.jsonData.IS_THIS_FINAL_CLOSING}  <CBox className="marginLeft8" checked={this.state.finalClosing} onChange={(e) => this.handleCheckBoxChange(e, 'finalClosing')}>
                            <span className="checkmark"></span>
                        </CBox></div>
                        <div className="form-main-div">
                            <Row className="full-width marginTop20 marginLeft61" hidden={this.state.closedReadyUsersList.length === 0}>
                                <div className="name-heading marginLeft75" >
                                    {this.state.jsonData.NAME}
                                </div>

                                <div className="name-heading" >
                                    Status
                                </div>
                                <div className="name-heading" >
                                    {this.state.jsonData.CAPITAL_COMMITMENT_OFFERED}
                                </div>
                                <div className="name-heading" >
                                    {this.state.jsonData.CAPITAL_COMMITMENT_ACCEPTED}
                                </div>
                                <div className="selectAll-div" >
                                    <CBox onChange={this.handleSelectAll} checked={this.state.checkSelectAll}>
                                        <span className="checkmark"></span>
                                    </CBox>
                                    <span className="selectAll-text"> Select All</span>
                                </div>
                            </Row>
                            <div className="vanillaTableContainer marginTop10" hidden={this.state.closedReadyUsersList.length === 0}>

                                {
                                    this.state.closedReadyUsersList.map((record, index) => {
                                        return (
                                            <div className="userRow" key={index}>
                                                <label className="userImageAlt paddingBottom8">
                                                    {
                                                        record['profilePic'] ?
                                                            <img src={record['profilePic']['url']} alt="img" className="user-image" />
                                                            : <img src={userDefaultImage} alt="img" className="user-image" />
                                                    }
                                                </label>
                                                <div className="closefundtitle">
                                                    <div className={"closefund-name " + (record['warnnings'] ? 'marginTopN15' : '')} title={record['trackerTitle']}>{record['trackerTitle']}</div>
                                                    <span className="warning">{record['warnnings']}</span>
                                                </div>
                                                <div className="lp-name lp-name-width lp-name-pad width150 closeFundStatus">{record['status']}</div>
                                                <div className="lp-name lp-name-width lp-name-pad lp-name-pad-5 width150 marginLeft30">{this.FsnetUtil.convertToCurrency(record['lpCapitalCommitment'])}</div>
                                                <div className="input-capital-accepted width150 marginLeft5">
                                                    <FormControl type="text" name="firmName" disabled={this.FsnetUtil.getUserRole() !== 'GP' || !record['canSign']} className="inputFormControl inputFormControlCloseFund" placeholder={this.state.jsonData.PLACEHOLDER_AMOUNT} value={record['gpConsumedCurrency']} onChange={(e) => this.handleInputCloseFund(e, { record })} onFocus={(e) => this.handleFocusCloseFund(e, { record })} onBlur={(e) => this.handleInputCloseFund(e, { record }, 'blur')} />
                                                </div>
                                                <div className="checkbox-main-div" hidden={!record['canSign']}>
                                                    <CBox className="marginLeft8 marginLeft50" checked={record['checked']} onChange={(e) => this.handleCheckBoxChange(e, { record })}>
                                                        <span className="checkmark"></span>
                                                    </CBox>
                                                </div>
                                            </div>
                                        );
                                    })
                                }
                                <div className="title margin20 text-center">{this.state.noDelegatesMsz}</div>
                            </div>

                            {
                                this.state.closedReadyUsersList.length === 0 ?
                                    this.state.closedReadyError === 'no_closed_ready_lps' ?
                                        <div className="title marginAll5 text-center 1">{this.Constants.NO_INVESTORS_CLOSED_READY_STATUS}</div> :
                                        <div className="title noUserTitle marginAll5 text-center 2"><span>Please note carefully:</span>  {this.state.jsonData.NO_INVESTORS_CLOSED_READY_STATUS}</div>
                                    : ''
                            }
                            <div className="error marginBot12 height10">{this.state.closeFundError}</div>
                        </div>
                        <Row hidden={this.state.closedReadyUsersList.length === 0}>
                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                <Button type="button" className="fsnetCancelButton marginLeft50" onClick={this.restCloseFundErrors}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                <Button type="button" className={"fsnetSubmitButton closeFundSubmitBtn " + (this.state.enableFundBtn && this.state.accountType !== 'FSNETAdministrator' ? 'btnEnabled' : 'disabled')} disabled={!this.state.enableFundBtn && this.state.accountType === 'FSNETAdministrator'} onClick={this.closeFundBtn}>{this.FsnetUtil.getUserRole() === 'SecondaryGP' ? 'Approve Closing' : this.state.jsonData.CLOSE_FUND}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                {/* Approve Lp for Closing Modal */}
                <Modal id="GPDelModal" backdrop="static" show={this.state.lpApproveModal} onHide={this.resetLpApprovalErrors} dialogClassName="GPDelModalDialog fundModalDialog reponsiveFundModal approvelpModal" className={"GPDelModalMarginLeft " + (this.state.closedReadyUsersList.length === 0 ? 'height230' : '')}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.APPROVE_CLOSING}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext" hidden={this.state.closedReadyUsersList.length === 0}>{this.state.jsonData.APPROVE_INVESTOR_FOR_CLOSING}</div>
                        <div className="form-main-div">
                            <Row className="full-width marginTop20 marginLeft61" hidden={this.state.closedReadyUsersList.length === 0}>
                                <div className="name-heading marginLeft75">
                                    {this.state.jsonData.NAME}
                                </div>
                                <div className="name-heading width150">
                                    Status
                                </div>
                                <div className="name-heading">
                                    {this.state.jsonData.CAPITAL_COMMITMENT_OFFERED}
                                </div>
                                <div className="selectAll-div paddingLeft15">
                                    <div className="name-heading" >
                                        {this.state.jsonData.APPROVE}
                                    </div>
                                </div>
                            </Row>
                            <div className="vanillaTableContainer marginTop10" hidden={this.state.closedReadyUsersList.length === 0}>

                                {
                                    this.state.closedReadyUsersList.map((record, index) => {
                                        return (
                                            <div className="userRow" key={index}>
                                                <label className="userImageAlt paddingBottom8">
                                                    {
                                                        record['profilePic'] ?
                                                            <img src={record['profilePic']['url']} alt="img" className="user-image" />
                                                            : <img src={userDefaultImage} alt="img" className="user-image" />
                                                    }
                                                </label>
                                                <div className="lp-name" title={record['trackerTitle']}>{record['trackerTitle']}</div>
                                                <div className="lp-name closeFundStatus" title={record['status']}>{record['status']}</div>
                                                <div className="lp-name lp-name-width lp-name-pad marginLeft30 width150">{this.FsnetUtil.convertToCurrency(record['lpCapitalCommitment'])}</div>
                                                <div className="checkbox-main-div marginLeft10" hidden={!record['canSign']}>
                                                    <CBox className="marginLeft8 marginLeft50" checked={record['checked']} onChange={(e) => this.handleCheckBoxChange(e, { record })}>
                                                        <span className="checkmark"></span>
                                                    </CBox>
                                                </div>
                                            </div>
                                        );
                                    })
                                }
                                <div className="title margin20 text-center">{this.state.noDelegatesMsz}</div>
                            </div>
                            <div className="title noUserTitle marginAll5 text-center" hidden={this.state.closedReadyUsersList.length !== 0}>{this.state.jsonData.NO_INVESTORS_APPROVAL_PROCESS_STATUS}</div>
                            <div className="error marginBot12 height10">{this.state.closeFundError}</div>
                        </div>
                        <Row hidden={this.state.closedReadyUsersList.length === 0}>
                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                <Button type="button" className="fsnetCancelButton marginLeft50" onClick={this.resetLpApprovalErrors}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                <Button type="button" className={"fsnetSubmitButton closeFundSubmitBtn " + (this.state.enableFundBtn ? 'btnEnabled' : 'disabled')} disabled={!this.state.enableFundBtn} onClick={this.approveClosingBtn}>{this.state.jsonData.APPROVE_CLOSING}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <Modal id="GPDelModal" backdrop="static" show={this.state.signatureModal} onHide={this.handleSignatureClose} dialogClassName="GPDelModalDialog gpDelFundModalDialog" className="GPSignModal GPCloseSignModal">
                    <Modal.Header closeButton className="headerNone">
                    </Modal.Header>
                    <Modal.Title hidden={this.state.validationRuleError}>{this.state.jsonData.CLOSE_FUND}</Modal.Title>
                    <Modal.Title hidden={!this.state.validationRuleError}>{this.state.jsonData.ERROR}</Modal.Title>
                    <Modal.Body hidden={(!this.state.hasSignature)}>
                        <div hidden={this.state.validationRuleError}>
                            <div className="subtext modal-subtext" hidden={this.state.closedReadyUsersList.length === 0}>{this.state.jsonData.PASSWORD_MESSAGE}</div>
                            <div className="subtext modal-subtext" hidden={this.state.closedReadyUsersList.length === 0}>{this.state.jsonData.ENTER_SIGNATURE_AUTHENTICATION_PASSWORD}</div>
                            <div className="form-main-div signatureModalBody">
                                <FormControl type="password" name="signaturePassword" className="inputFormControl inputFormControlCloseFund inputFormSignatuteAuth" placeholder={this.state.jsonData.PLACEHOLDER_PASSWORD} onChange={(e) => this.handleSingaturePwd(e)} />
                            </div>
                            <div className="error marginBot12 height10">{this.state.signatureErrorMsz}</div>
                            <Row hidden={this.state.closedReadyUsersList.length === 0}>
                                <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                    <Button type="button" className="fsnetCancelButton marginLeft50  marginLeft15Ipad" onClick={this.handleSignatureClose}>{this.state.jsonData.CANCEL}</Button>
                                </Col>
                                <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                    <Button type="button" className={"fsnetSubmitButton closeFundSubmitBtn " + (this.state.enableSignatureBtn ? 'btnEnabled' : 'disabled')} disabled={!this.state.enableSignatureBtn} onClick={this.verifySignature}>{this.state.jsonData.SUBMIT}</Button>
                                </Col>
                            </Row>
                        </div>
                        <div hidden={!this.state.validationRuleError}>
                            <p className="error validationError" hidden={!this.state.showFundExceedError}>
                                <p>{this.state.jsonData.AMOUNT_ATTEMPTING_CLOSE_EXCEEDS_SPECIFIED_HARD_CAP}</p>
                                <p>{this.state.jsonData.DESELECT_INVESTORS} </p>
                                <p>{this.state.jsonData.ADJUST_ACCEPTED_CAPITAL_COMMITMENT}</p>
                                <p>{this.state.jsonData.NAVIGATE_EDIT_FUND_DETAILS}</p>
                            </p>
                            <div hidden={this.state.showFundExceedError}>
                                <div className="validationErrorContainer">
                                    <p className="error validationError" dangerouslySetInnerHTML={{ __html: this.state.CLOSED_INVESTORS_COUNT_EXCESS_99_MSZ }}></p>
                                    <p className="error validationError">{this.state.INVESTORS_COUNT_EXCESS_99_MSZ}</p>
                                    <p className="error validationError">{this.state.INVESTORS_COUNT_EXCESS_499_MSZ}</p>
                                    <p className="error validationError">{this.state.ERISA_WARNINGS_MSZ}</p>
                                    <p className="error validationError">{this.state.DISQUALIFYING_EVENT_MSZ}</p>
                                    <p className="error validationError">{this.state.EEA_WARNINGS_MSZ}</p>
                                    <p className="error validationError" dangerouslySetInnerHTML={{ __html: this.state.ACCREDITED_EVENT_MSZ }}></p>
                                    <p className="error validationError">{this.state.EQUITY_OWNERS_EVENT_MSZ}</p>
                                </div>
                                <Row className="marginTop10">
                                    <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                        <Button type="button" className="fsnetCancelButton marginLeft50  marginLeft15Ipad" onClick={this.handleSignatureClose}>{this.state.jsonData.CANCEL}</Button>
                                    </Col>
                                    <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                        <Button type="button" className={"fsnetSubmitButton closeFundSubmitBtn btnEnabled"} onClick={this.showFundAgreementModal}>{this.state.jsonData.PROCEED_REGARDLESS}</Button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Body hidden={this.state.hasSignature}>
                        {/* <div className="title" hidden={this.state.hasAuthenticationpassword}>Please add authentication password using this <a href="/settings/authentication-password">link</a>.</div> */}
                        <div className="title" hidden={this.state.hasSignature}>Please Configure Signature using this <a href={"/settings/uploadSignature/" + this.FsnetUtil._encrypt(this.state.fundId)}>link</a>.</div>
                    </Modal.Body>
                </Modal>
                <Modal id="lpFundModal" backdrop="static" className="lpFundModalAlert" show={this.state.errorModal} onHide={this.errorModalClose} dialogClassName="GPDelModalDialog">
                    <Modal.Header closeButton>
                        <span className="capitalCommitmentError">{this.state.jsonData.ERROR}</span>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="viewFund">
                            <h1 className="title viewFundErrorText">{this.state.jsonData.CAPITAL_COMMITMENT_ACCEPTED_NOT_EXCEED_OFFERED}</h1>
                        </div>
                    </Modal.Body>
                </Modal>
                <Modal id="GPDelModal" backdrop="static" show={this.state.showCloseFundAlert} onHide={this.closeCloseFundProceedAlert} dialogClassName="GPDelModalDialog fundModalDialog" className="GPDelModalMarginLeft GPSignModal">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.jsonData.PROCEED_CLOSED_FUND}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext">{this.state.closeFundProceedTitleTxt}</div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                <Button type="button" className="fsnetCancelButton marginLeft50" onClick={this.closeCloseFundProceedAlert}>No</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                <Button type="button" className={"fsnetSubmitButton closeFundSubmitBtn btnEnabled"} onClick={this.proceedCloseFund}>Yes</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="lpFundModal" backdrop="static" show={this.state.lpFundModal} onHide={() => { this.handlefundClose(null) }} dialogClassName="GPDelModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>{this.state.fundName}</Modal.Title>
                    <Modal.Body>
                        <div className="viewFund">
                            <LpTableComponent fundId={this.state.fundId}></LpTableComponent>
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handlefundClose}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            {/* <Col lg={6} md={6} sm={6} xs={12}>
                            <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.handlefundClose}>Deactivate</Button>
                            </Col> */}
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="lPNameProfileModal" backdrop="static" show={this.state.lpNameProfileModal} onHide={this.handleLpNameProfileClose} dialogClassName="GPDelModalDialog fundModalDialog" className="">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title></Modal.Title>
                    {this.state.lpProfileData ?
                        <Modal.Body>
                            {
                                this.state.lpProfileData['profilePic'] ?
                                    <img src={this.state.lpProfileData['profilePic']['url']} alt="img" className="profilePicLarge" />
                                    :
                                    <img src={profilePictureLarge} alt="img" className="profilePicLarge" />
                            }
                            <div className="profileNameModal ellipsis" title={this.FsnetUtil.getFullName(this.state.lpProfileData)}>{this.FsnetUtil.getFullName(this.state.lpProfileData)}</div>
                            <div className="profileNameDelegate">{this.FsnetUtil.getAccountTypeName(this.state.lpProfileData.accountType)}</div>

                            <Row className="marginLpUserMail">
                                <Col lg={2} md={2} sm={2} xs={2}>
                                    <img src={letterImage} className="marginRight15"></img>
                                </Col>
                                <Col lg={10} md={10} sm={10} xs={10} className="paddingLpUserMailId">
                                    <span className="LPUserMailId">{this.state.lpProfileData.email}</span>
                                </Col>
                            </Row>
                            {this.state.lpProfileData['cellNumber'] ? <div className="marginLeft20"><img src={smartPhone} className="marginLeft5 marginRight22"></img><span className="LPUserMailId">{this.state.lpProfileData.cellNumber}</span></div>
                                : ''}
                        </Modal.Body>
                        : ''}
                </Modal>
                {/* Edit Subscription Aggrement Modal */}
                <Modal id="editSubModal" backdrop="static" show={this.state.subscriptionFormModal} onHide={this.closeSubscriptionFormModal} dialogClassName="subscriptionFormModal">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <SAComponent/>
                    </Modal.Body>
                </Modal>


                {/* Fund agreement upload while closing the Fund */}
                <Modal id="GPDelModal" backdrop="static" show={this.state.fundAgreementUpload} onHide={this.closeFundAgreementUpload} dialogClassName="GPDelModalDialog gpDelFundModalDialog closeFundAgreementModal" className="GPSignModal GPCloseSignModal">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Title> Upload New Fund Agreement</Modal.Title>
                    <Modal.Body>
                        <input type="file" id="uploadBtn" className="hide" onChange={(e) => this.handleChange(e)} />
                        {
                            this.state.showFundAgreementUploadText ?
                                <div className="preclosing">
                                    <div className="subtext">WARNING – If you effectuate the closing, Investors will be notified immediately, and all of the fully executed documents will be generated and placed in the Document Locker, with Investors able to download them effective immediately. Sometimes, at this stage you may wish to upload a revised Fund Agreement to address clerical insertions pre-closing (for example, with the closing date inserted instead of a blank/bracketed date). This functionality is not available post-closing once documents have been generated. Do you wish to upload a revised Fund Agreement prior to closing?</div>
                                    <Row className="marginTop20">
                                        <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                            <Button type="button" className="fsnetSubmitButton btnEnabled width250 marginLeft50" onClick={this.fundAgreementUploadClose}>No - Proceed with Closing</Button>
                                        </Col>
                                        <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                            <Button type="button" className="fsnetSubmitButton btnEnabled width300" disabled={this.state.fundAgreementUploadProceedBtnDisabled} onClick={this.fundAgreementUploadProceed}>Yes - Upload New Fund Agreement</Button>
                                        </Col>
                                    </Row>
                                </div>
                                :
                                this.state.uploadDocName ?
                                    <div className="upload-doc-name marginTop10 preclosing">
                                        <div className="subtext marginBotNone">Do you wish to upload a revised Fund Agreement prior to closing?</div>
                                        <div className="title-md marginLeft10 marginTop20 width600 text-center breakWord inline-block"><a className="cursor" onClick={this.openUploadDoc}>{this.state.uploadDocName}</a><i className="fa fa-trash cursor-pointer marginLeft10" onClick={this.deleteFile}></i></div>
                                        <Row className="marginTop20">
                                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                                <Button type="button" className="fsnetCancelButton marginLeft50" onClick={this.showFundUploadMainPage}>Cancel</Button>
                                            </Col>
                                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                                <Button type="button" className="fsnetSubmitButton btnEnabled width300" onClick={this.proceedClosingWithFundDoc}>Upload New Fund Agreement</Button>
                                            </Col>
                                        </Row>
                                    </div>

                                    :
                                    <div className="preclosing">
                                        <div className="subtext">Do you wish to upload a revised Fund Agreement prior to closing?</div>
                                        <div className="text-center">
                                            <Button className="fsnetSubmitButton width300 btnEnabled" onClick={this.uploadDoc}>Upload New Fund Agreement</Button>
                                        </div>
                                    </div>
                        }

                    </Modal.Body>
                </Modal>



                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default ModalComponent;


import React, { Component } from "react";
import {
  Button,
  Modal,
  FormControl,
  Row,
  Col,
  ControlLabel
} from "react-bootstrap";
import "./counterSign.component.css";
import Loader from '../../widgets/loader/loader.component';
import { Fsnethttp } from "../../services/fsnethttp";
import { FsnetAuth } from '../../services/fsnetauth';
import { FsnetUtil } from '../../util/util';
import { Constants } from '../../constants/constants';
import { reactLocalStorage } from "reactjs-localstorage";

class CounterSign extends Component {
  constructor(props) {
    super(props);
    this.FsnetAuth = new FsnetAuth();
    this.Constants = new Constants();
    this.FsnetUtil = new FsnetUtil();
    this.Fsnethttp = new Fsnethttp();
    this.state = {
      showModal: false,
      pdfUrl: null,
      signatureModal: false,
      password: null,
      fundId: null,
      subscriptionId: null,
      documentId: null,
      type: null,
      ccIncreaseShow: false
    };
  }

  componentDidMount() {
    this.checkIsIEEdge();
    if(this.FsnetAuth.isAuthenticated()) {
      let fundId = this.FsnetUtil._getId();
      let url = this.FsnetUtil.decodeUrl();
      let folderName = url.type === 'SA' ? 'sia' : 'ccgpsign'
      if(['GP','FSNETAdministrator','SecondaryGP'].indexOf(this.FsnetUtil.getUserRole()) > -1 && ['SA','CA'].indexOf(url.type) > -1) {
        this.open();
        this.setState({
          fundId: fundId,
          subscriptionId: url.subscriptionId ? this.FsnetUtil._decrypt(url.subscriptionId) : null,
          documentId: url.documentId ? this.FsnetUtil._decrypt(url.documentId) : null,
          type: url.type ? url.type : null,
          pdfUrl: `${this.Constants.BASE_URL}document/view/${folderName}/${this.FsnetUtil._decrypt(url.subscriptionId)}?token=${url.token ? url.token : this.FsnetUtil.getToken()}`
        }, () => {
          if (!this.state.documentId) {
            window.close();
          }
          setTimeout(() => {
            this.close()
          }, 3000);
          //Check doc has permission.
          this.Fsnethttp.checkDocForSigning(this.state.pdfUrl).then(result => {
          })
          .catch(error => {
          });
        })
      } else {
        window.location.href = '/403'
      }
    } else {
      window.location.href = '/'
    }
    
  }

  checkIsIEEdge = () => {
    if (this.FsnetUtil._isIEEdge()) {
      const getTokenObj = this.FsnetUtil.decodeUrl();
      localStorage.setItem('token', JSON.stringify(getTokenObj.token));
      this.getUserDataThroughToken()
    }
  }


  getUserDataThroughToken = () => {
    this.Fsnethttp.getUserInfo().then(result => {
      localStorage.setItem('userData', JSON.stringify(result.data));
    })
      .catch(error => {
      });
  }



  close() {
    this.setState({ showModal: false });
  }

  // ProgressLoader : show progress loade
  open() {
    this.setState({ showModal: true });
  }

  openSignatureModal = () => {
    this.setState({
      signatureModal: true
    })
  }

  closeSignatureModal = () => {
    this.setState({
      signatureModal: false
    })
  }

  CounterSignDoc = () => {

    if (this.state.type == 'SA') {
      this.sideLetterSign()
    } else {
      this.capitalLetterSign(false)
    }
  }


  sideLetterSign = () => {
    this.open();
    let obj = { documentId: this.state.documentId, subscriptionId:this.state.subscriptionId, date: new Date(), timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone }
    this.Fsnethttp.signSideLetter(obj).then(result => {
      this.close();
      if (result.data) {
        localStorage.setItem("signed",true);
        const docUrl = `${result.data.url}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
        window.close();
      }
    })
      .catch(error => {
        this.close();
        if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
          this.setState({
            signatureErrorMsz: error.response.data.errors[0].msg,
          });
        }
      });
  }

  capitalLetterSign = (proceed) => {
    let obj = { documentId: this.state.documentId,subscriptionId:this.state.subscriptionId, proceed: proceed, date: new Date(), timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone }
    this.open();
    this.Fsnethttp.signFundCommitment(obj).then(result => {
      this.close();
      if (result.data) {
        localStorage.setItem("signed",true);
        const docUrl = `${result.data.url}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
        window.close();
      }
    })
      .catch(error => {
        this.close();
        if (error.response) {
          this.setState({
            ccIncreaseShow: true,
            signatureModal:false,
            ccErrorMsz: error && error.response && error.response.data && error.response.data.errors.CODE === 'CAPITAL_COMMITMENT_ERROR' ? error.response.data.errors.msg : error.response.data.errors[0].msg
          });
          if (error && error.response && error.response.data && error.response.data.errors[0] && error.response.data.errors[0].proceed == true) {
            this.setState({
              showProceed: true,
            })
          } else {
            this.setState({
              showProceed: false,
            })
          }
        }
      });
  }


  verifySignature = () => {
    this.open();
    let obj = { signaturePassword: this.state.password }
    this.Fsnethttp.verifySignature(obj).then(result => {
        this.close();
        this.CounterSignDoc();
    })
    .catch(error => {
        this.close();
        if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
            this.setState({
                signatureErrorMsz: error.response.data.errors[0].msg,
            });
        }
    });
}


  ccIncreaseClose = () => {
    this.setState({
      ccIncreaseShow: false,
      signatureModal:true
    });
  }

  ccIncreaseProceed = () => {
    this.capitalLetterSign(true);
    this.setState({
      ccIncreaseShow: false,
    });

  }



  render() {
    return (
      <React.Fragment>
        <div className="pdf">
          <h1 className="title pdf-title">
            {this.state.type === 'SA' ? "Please review and sign the investor's executed Side Letter Agreement." : "Please review and sign the investor's executed Capital Commitment Agreement."}
          </h1>
          <Button type="button" className="fsnetSubmitButton btnEnabled fontFamilyBold" onClick={this.openSignatureModal}>
            Confirm and Sign
            </Button>
          <div className="iframe-div">
            <iframe className="pdf-frame" src={this.state.pdfUrl} scrolling="no" frameBorder="0" id="pdf-view" />
          </div>
        </div>
        <Modal id="GPDelModal" backdrop="static" show={this.state.signatureModal} onHide={this.closeSignatureModal} dialogClassName="GPDelModalDialog fundModalDialog" className="GPDelModalMarginLeft GPSignModal topZero">
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Title>Authentication Password</Modal.Title>
          <Modal.Body>
            <div>
              <div className="subtext modal-subtext">By entering your password, you represent that you have signing authority for the applicable Fund and/or Document.</div>
              <div className="subtext modal-subtext">Please re-enter your password</div>
              <div className="form-main-div signatureModalBody">
                <FormControl type="password" name="signaturePassword" className="inputFormControl inputFormControlCloseFund inputFormSignatuteAuth" placeholder="Password" onChange={e => this.setState({ password: e.target.value.trim() })} value={this.state.password} />
              </div>
              <div className="error marginBot12 height10">{this.state.signatureErrorMsz}</div>
              <Row>
                <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                  <Button type="button" className="fsnetCancelButton marginLeft50" onClick={this.closeSignatureModal}>Cancel</Button>
                </Col>
                <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                  <Button type="button" className={"fsnetSubmitButton closeFundSubmitBtn " + (this.state.password ? 'btnEnabled' : 'disabled')} disabled={!this.state.password} onClick={this.verifySignature}>Submit</Button>
                </Col>
              </Row>
            </div>
          </Modal.Body>
        </Modal>
        <Modal id="CCIncrease" backdrop="static" show={this.state.ccIncreaseShow} onHide={this.ccIncreaseClose} dialogClassName="GPDelModalDialog fundModalDialog" className="GPDelModalMarginLeft GPSignModal">
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Title className="errorHeadAlign titleModal ">Error</Modal.Title>
          <Modal.Body>

            <div className="error marginBot12 height10 errMsgAlign">{this.state.ccErrorMsz}</div>
            <Row>
              <Col lg={12} md={12} sm={12} xs={12} className="paddingZero text-center" hidden={!this.state.showProceed}>
                <Button type="button" className="fsnetCancelButton marginLeft50 btnEnabled proceedWidth" onClick={this.ccIncreaseProceed}>Proceed Regardless</Button>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>
        <Loader isShow={this.state.showModal} />
      </React.Fragment>
    );
  }
}

export default CounterSign;

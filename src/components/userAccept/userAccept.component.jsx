import React, { Component } from 'react';
import '../register/register.component.css';
import { Row, Col, Button, Checkbox as CBox, FormControl, Modal } from 'react-bootstrap';
import { Fsnethttp } from '../../services/fsnethttp';
import { Constants } from '../../constants/constants';
import { Route, Link } from "react-router-dom";
import { FsnetUtil } from '../../util/util';
import userDefaultImage from '../../images/default_user.png';
import Loader from '../../widgets/loader/loader.component';
import HeaderComponent from '../authheader/header.component'
import successImage from '../../images/success.png';
import { reactLocalStorage } from 'reactjs-localstorage';

class UserAcceptComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            showSuccesScreen: false,
            errorMsz: null
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    componentDidMount() {
        let urlValues = this.FsnetUtil.decodeUrl();
        this.open();
        const postObj = { code: urlValues.code, gprequestid: urlValues.gprequestid }
        this.Fsnethttp.userAcceptCode(postObj).then(result => {
            this.close();
            this.setState({
                showSuccesScreen: true
            })

        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        errorMsz: error.response.data.errors[0].msg,
                    })
                } else {
                    this.setState({
                        errorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });
    }


    navigateToLogin = () => {
        //If user is logged in deleting the local storage data.
        const token = this.FsnetUtil.getToken();
        if(token) {
            this.logout()
        } else {
            window.location.href = '/';
        }
    }

    logout = () => {
        this.Fsnethttp.logout().then(result => {
            this.FsnetUtil._ClearLocalStorage();
        });
    }


    render() {
        return (
            <div className="parentContainer" id="registerContainer">
                <div>
                    <HeaderComponent ></HeaderComponent>
                </div>
                {
                    this.state.showSuccesScreen &&
                    <Row className="registerContainer">
                        <div className="topBorder"></div>
                        <Col lg={12} md={12} sm={12} xs={12}>
                            <label className="register-success-text">Success</label>
                            <label className="register-reset-text">Fund Manager role has been accepted.</label>
                            <div className="success-icon">
                                <img src={successImage} alt="success" />
                            </div>
                            <div className="text-center">
                                <Button className="register-submit" onClick={this.navigateToLogin}>Proceed to Login</Button> <br />
                            </div>
                        </Col>
                    </Row>
                }
                {
                    this.state.errorMsz &&
                    <div className="text-center">
                        <h1>{this.state.errorMsz}</h1>
                    </div>
                }
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default UserAcceptComponent;


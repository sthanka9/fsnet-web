import React, { Component } from 'react';
import './register.component.css';
import { Row,Col, Button, Checkbox as CBox, FormControl,Modal} from 'react-bootstrap';
import {Fsnethttp} from '../../services/fsnethttp';
import { FsnetUtil } from '../../util/util';
import {Constants} from '../../constants/constants';
import { Route, Link } from "react-router-dom";
import userDefaultImage from '../../images/default_user.png';
import Loader from '../../widgets/loader/loader.component';
import  HeaderComponent from '../authheader/header.component'
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/rrui.css'
import 'react-phone-number-input/style.css'
import successImage from '../../images/success.png';
import { reactLocalStorage } from 'reactjs-localstorage';

class RegisterComponent extends Component{

    constructor(props){
        super(props);
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        this.state = {
            userImageName: 'Profile_Pic.jpg',
            currentUserImage: userDefaultImage,
            profilePicFile: {},
            email:'',
            userName:'',
            middleName:'',
            mobileNumber:'',
            cellNumber:'',
            confirmPassword:'',
            password:'',
            errorMessage:'',
            fsnetTermsandServices: false,
            userNameExists:'',
            userAccessId: '',
            invalidInvitationCode:'',
            showRegisterScreen: false,
            passwordEmptyMsz: '',
            confirmPasswordEmptyMsz: '',
            cellNumberEmptyMsz:'',
            termsandConditionsRequired:'',
            showSuccesScreen:false,
            isFormValid: false,
            isUserNameValid: false,
            isPasswordValid: false,
            isCnfrmPasswordValid: false,
            isCellNumberValid: false,
            fsnetTermsandServicesValid: false,
            userNameBorder: false,
            passwordBorder: false,
            confirmPasswordBorder: false,
            cellNumberBorder: false,
            confirmationModal:false
        }
    }

    //Navigate to login
    navigateToLogin = () => {
        window.location = '/';
    }

    //Reset all state values to default value.
    componentWillUnmount() {
        this.setState({
            userImageName: 'Profile_Pic.jpg',
            currentUserImage: userDefaultImage,
            profilePicFile: {},
            email:'',
            userName:'',
            mobileNumber:'',
            cellNumber:'',
            confirmPassword:'',
            password:'',
            errorMessage:'',
            fsnetTermsandServices: false,
            userNameExists:'',
            userAccessId: '',
            invalidInvitationCode:'',
            showRegisterScreen: false,
            passwordEmptyMsz: '',
            confirmPasswordEmptyMsz: '',
            cellNumberEmptyMsz:'',
            termsandConditionsRequired:'',
            showSuccesScreen:false,
            isFormValid: false,
            isUserNameValid: false,
            isPasswordValid: false,
            isCnfrmPasswordValid: false,
            isCellNumberValid: false,
            userNameBorder: false,
            passwordBorder: false,
            confirmPasswordBorder: false,
            cellNumberBorder: false,
        });
    }


     // Update state params values and login button visibility

     updateStateParams(updatedDataObject){
        this.setState(updatedDataObject, ()=>{
            this.enableDisableLoginButtion();
        });
    }

    // Enable / Disble functionality of Login Button

    enableDisableLoginButtion(){
        let status = (this.state.isPasswordValid && this.state.isCnfrmPasswordValid && this.state.isCellNumberValid && this.state.fsnetTermsandServicesValid && this.state.islastNameValid && this.state.isfirstNameValid) ? true : false;
        this.setState({
            isFormValid : status
        });
    }

    logout = () => {
        this.Fsnethttp.logout().then(result => {
            this.FsnetUtil._ClearLocalStorage(true);
        });
    }

    //USer profile pic upload.
    handleChange = (event) => {
        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
            this.imageFile = event.target.files[0];
            let imageName = event.target.files[0].name
            var sFileExtension = imageName.split('.')[imageName.split('.').length - 1].toLowerCase();
            const imageFormats = ['png','jpg','jpeg','gif','tif','svg'];
            if(imageFormats.indexOf(sFileExtension) === -1) {
                document.getElementById('uploadBtn').value = "";
                alert('Please upload valid image.')
                return true;
            }
            //Change user profile image size limit here.
            if(this.imageFile.size <=1600000) {
                    this.setState({
                        profilePicFile : event.target.files[0],
                    });
                reader.readAsDataURL(this.imageFile);
                this.setState({
                    userImageName: event.target.files[0].name
                });
                reader.onload = () => {
                    this.setState({
                        currentUserImage : reader.result,
                    });
                }
            } else {
               alert('Please upload image Maximum file size : 512X512')
            }
        }
    }

    //Onchange event for all input text boxes.
    handleInputChangeEvent=(event,type)=>{
        let dataObj = {}; 
        switch(type) {
            case 'username':
                //Add username value to state
                //Clear the username exists message
                if(event.target.value.trim() === '' || event.target.value === '' || event.target.value === undefined) {
                    this.setState({
                        userNameExists: this.Constants.LOGIN_USER_NAME_REQUIRED,
                        isUserNameValid: false,
                        userNameBorder: true,
                        userName: ''
                    })
                    window.scrollTo(300, 0)                    
                    dataObj ={
                        isUserNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        userName: event.target.value,
                        userNameExists: '',
                        isUserNameValid: true,
                        userNameBorder: false,
                    })
                    dataObj ={
                        isUserNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'middleName':
                if(event.target.value.trim() === '' || event.target.value === '' || event.target.value === undefined) {
                    this.setState({
                        middleName:''
                    })
                } else {
                    this.setState({
                        middleName: event.target.value,
                    })
                   
                }
                break;
            case 'lastName':
                if(event.target.value.trim() === '' || event.target.value === '' || event.target.value === undefined) {
                    this.setState({
                        lastNameMsz: this.Constants.LAST_NAME_REQUIRED,
                        islastNameValid: false,
                        lastNameBorder: true,
                        lastName:'',
                    })
                    dataObj ={
                        islastNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        lastName: event.target.value,
                        lastNameMsz: '',
                        islastNameValid: true,
                        lastNameBorder: false,
                    })
                    dataObj ={
                        islastNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'firstName':
                if(event.target.value.trim() === '' || event.target.value === '' || event.target.value === undefined) {
                    this.setState({
                        firstNameMsz: this.Constants.FIRST_NAME_REQUIRED,
                        isfirstNameValid: false,
                        firstNameBorder: true,
                        firstName: '',
                    })
                    dataObj ={
                        isfirstNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        firstName: event.target.value,
                        firstNameMsz: '',
                        isfirstNameValid: true,
                        firstNameBorder: false,
                    })
                    dataObj ={
                        isfirstNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'password':
                if(event.target.value.trim() === '' || event.target.value === '' || event.target.value === undefined) {
                    this.setState({
                        passwordEmptyMsz: this.Constants.LOGIN_PASSWORD_REQUIRED,
                        isPasswordValid: false,
                        passwordBorder: true,
                    })
                    dataObj ={
                        isPasswordValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        password: event.target.value,
                        passwordEmptyMsz: '',
                        isPasswordValid: true,
                        passwordBorder: false,
                    })
                    dataObj ={
                        isPasswordValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'confirmPassword':
                if(event.target.value.trim() === '' || event.target.value === '' || event.target.value === undefined) {
                    this.setState({
                        confirmPasswordEmptyMsz: this.Constants.CNFRM_PWD_REQUIRED,
                        isCnfrmPasswordValid: false,
                        confirmPasswordBorder: true,
                    })
                    dataObj ={
                        isCnfrmPasswordValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        confirmPassword: event.target.value,
                        confirmPasswordEmptyMsz: '',
                        isCnfrmPasswordValid: true,
                        confirmPasswordBorder: false,
                    })
                    dataObj ={
                        isCnfrmPasswordValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'cellNumber':
                if(event === '' || event === undefined) {
                    this.setState({
                        cellNumberEmptyMsz: this.Constants.CELL_NUMBER_REQUIRED,
                        isCellNumberValid: false,
                        cellNumberBorder: true,
                    })
                    dataObj ={
                        isCellNumberValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        cellNumber: event,
                        cellNumberEmptyMsz: '',
                        isCellNumberValid: true,
                        cellNumberBorder: false,
                    })
                    dataObj ={
                        isCnfrmPasswordValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'fsnetTermsandServices':
                if(event.target.checked) {
                    this.setState({
                        fsnetTermsandServices : event.target.checked,
                        termsandConditionsRequired:'',
                        fsnetTermsandServicesValid: true
                    })
                    dataObj ={
                        fsnetTermsandServicesValid :true
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        termsandConditionsRequired: this.Constants.TERMS_CONDITIONS,
                        fsnetTermsandServices : event.target.checked,
                        fsnetTermsandServicesValid:false,
                    })
                    dataObj ={
                        fsnetTermsandServicesValid :false
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            default:
                // do nothing
        }
        this.setState({
            errorMessage: ''
        })
    }

    //signup button click functionality
    signUpFn =()=> {
        this.open();
        //call the signup api
        var formData = new FormData();
        formData.append("password", this.state.password);
        formData.append("email", this.state.email);
        formData.append("middleName", this.state.middleName);
        formData.append("firstName", this.state.firstName);
        formData.append("lastName", this.state.lastName);
        formData.append("emailConfirmCode", this.state.userAccessId);
        formData.append("cellNumber", this.state.cellNumber);
        if(this.state.userImageName !== '') {
            formData.append("profilePic", this.state.profilePicFile);
        }
        if(this.state.homeNumber !== '' && this.state.homeNumber !== undefined) {
            formData.append("homeNumber", this.state.mobileNumber);
        }
        this.Fsnethttp.register(formData).then(result=>{
            this.close();
            this.setState({confirmationModal:false},()=>{this.setState({showSuccesScreen: true})})
        })
        .catch(error=>{
            this.close();
            if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                this.setState({
                    errorMessage: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    errorMessage: this.Constants.INTERNAL_SERVER_ERROR,
                });

            }
        });
            
    }

    checkValidations = () => {
        let errosArr = [];
        let registerError = false;
        if(this.state.password.trim() === '') {
            registerError = true;
            this.setState({
                passwordEmptyMsz: this.Constants.LOGIN_PASSWORD_REQUIRED
            })
        }
        if(this.state.confirmPassword.trim() === '') {
            registerError = true;
            this.setState({
                confirmPasswordEmptyMsz: this.Constants.LOGIN_PASSWORD_REQUIRED
            })
        }
        if(this.state.cellNumber === '' || this.state.cellNumber === undefined) {
            registerError = true;
            this.setState({
                cellNumberEmptyMsz: this.Constants.CELL_NUMBER_REQUIRED
            })
        } 
        else if(this.state.cellNumber.length < 12 || this.state.cellNumber.length > 13) {
            registerError = true;
            this.setState({
                cellNumberEmptyMsz: this.Constants.CELL_NUMBER_VALID
            })
        }

        let passwordRegex = this.Constants.PASSWORD_REGEX;
        //Check password and confirm password is same.
        //If both passwords are not same then show the error.
        if((this.state.password.trim() !== '' && this.state.confirmPassword.trim() !== '') &&(this.state.password !== this.state.confirmPassword)) {
            errosArr.push(this.Constants.REQUIRED_PASSWORD_AGAINPASSWORD_SAME)
        } 
        //Check password contains min 8 letters with letters, numbers and symbols.
        else if((!passwordRegex.test(this.state.password) || !passwordRegex.test(this.state.confirmPassword)) && (this.state.password.trim() !== '' && this.state.confirmPassword.trim() !== '')) {
            errosArr.push(this.Constants.PASSWORD_RULE_MESSAGE)
        }

        //Need to agree FSNET's Terms of service checkbox
        if(!this.state.fsnetTermsandServices) {
            registerError = true;
            this.setState({
                termsandConditionsRequired: this.Constants.TERMS_CONDITIONS
            })
        }

        //append all the errors to one string.
        if(errosArr.length >0) {
            let error = '';
            let idx = 1;
            for(let index of errosArr) {
                error = error+index+"\n";
                idx++;
            }
            this.setState({
                errorMessage: error
            })
            return true;
        } else if(registerError){
            return true;
        } else {
            return false;
        }
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    componentDidMount() {
        const token = this.FsnetUtil.getToken();
        if(token) {
            this.logout()
        }
        this.open();
        let url = window.location.href;
        let getId = url.split('register/');
        let userAccessId = getId[1];
        //In register url get the id to get the user details
        //If id is not present in url then redirect to 404.
        if(userAccessId) {
            this.Fsnethttp.getInvitationData(userAccessId).then(result=>{
                if(result.data) {
                    this.setState({
                        email:result.data.data.email,
                        firstName:result.data.data.firstName ? result.data.data.firstName : '',
                        isfirstNameValid:result.data.data.firstName ? true : false,
                        lastName:result.data.data.lastName ? result.data.data.lastName : '',
                        middleName:result.data.data.middleName ? result.data.data.middleName : '',
                        islastNameValid:result.data.data.lastName ? true : false,
                        userAccessId:userAccessId,
                        showRegisterScreen:true
                    })
                }
                this.close();
            })
            .catch(error=>{
                this.close();
                if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        invalidInvitationCode: error.response.data.errors[0].msg,
                        showRegisterScreen:false
                    });
                } else {
                    this.setState({
                        invalidInvitationCode: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });
        } else {
            this.close();
        }
    }

    userNameValidation(event) {
        var regex = new RegExp("^[A-Za-z0-9?._-]+$");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
    }
    }

    //Clear the image when user click on remove button
    //Clear the image name from state
    //Clear the input file value
    removeImageBtn = () => {
        this.setState({
            userImageName: 'Profile_Pic.jpg',
            currentUserImage : userDefaultImage,
            profilePicFile:{}
        });
        document.getElementById('uploadBtn').value = "";
    }

    uploadBtnClick = () => {
        document.getElementById('uploadBtn').click();
    }

    closeConfirmationModal =()=> {this.setState({confirmationModal:false})}
    showConfirmationModal =()=> {
        let error = this.checkValidations();
        if(!error && this.state.email.trim() !== '' && this.state.userAccessId !== '') {
            this.setState({confirmationModal:true})
        }
    }
    

    render(){
        return(
            <div className="parentContainer" id="registerContainer">            
                <div className="text-center" hidden={this.state.showRegisterScreen}>
                    <h1>{this.state.invalidInvitationCode}</h1>
                </div>
                <div hidden={!this.state.showRegisterScreen}>
                    <HeaderComponent ></HeaderComponent>
                </div>
                <Row className="registerContainer" hidden={!this.state.showRegisterScreen || this.state.showSuccesScreen}>
                    <div className="topBorder"></div>
                    <div className="parentDiv">
                        <h1 className="register-text">Vanilla Account Registration</h1>
                        <div className="mandatory-content">Fill in the form below to register for your account. Fields marked with an * are mandatory.</div>
                        <div className="mandatory-content">Password should contain a minimum of eight characters with at least one letter, one number and one special character.</div>
                        <form>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">Email Address</label>
                                <FormControl type="text" name="email" disabled className="inputFormControl opacityInput" id="email" placeholder="JohnDoe@gmail.com" value={this.state.email}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">First Name*</label>
                                <FormControl type="text" name="firstName" className={"inputFormControl " + (this.state.firstNameBorder ? 'inputError' : '')} value= {this.state.firstName} maxLength="200" placeholder="Charles" onChange={(e) => this.handleInputChangeEvent(e,'firstName')} onBlur={(e) => this.handleInputChangeEvent(e,'firstName')} />
                                <span className="error">{this.state.firstNameMsz}</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">Last Name*</label>
                                <FormControl type="text" name="lastName" className={"inputFormControl " + (this.state.lastNameBorder ? 'inputError' : '')} value= {this.state.lastName} maxLength="200" placeholder="Xavier" onChange={(e) => this.handleInputChangeEvent(e,'lastName')} onBlur={(e) => this.handleInputChangeEvent(e,'lastName')} />
                                <span className="error">{this.state.lastNameMsz}</span>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">Middle Name or Initial</label>
                                <FormControl type="text" name="middleName" className={"inputFormControl " + (this.state.middleNameBorder ? 'inputError' : '')} value= {this.state.middleName} maxLength="200" placeholder="Elisha" onChange={(e) => this.handleInputChangeEvent(e,'middleName')} onBlur={(e) => this.handleInputChangeEvent(e,'middleName')} />
                                <span className="error">{this.state.middleNameMsz}</span>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">Password*</label>
                                <FormControl type="password" name="password" className={"inputFormControl " + (this.state.passwordBorder ? 'inputError' : '')} placeholder="Password" onChange={(e) => this.handleInputChangeEvent(e,'password')} onBlur={(e) => this.handleInputChangeEvent(e,'password')}/>
                                <span className="error">{this.state.passwordEmptyMsz}</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">Confirm password*</label>
                                <FormControl type="password" name="confirmPassword" className={"inputFormControl " + (this.state.confirmPasswordBorder ? 'inputError' : '')} placeholder="Password" onChange={(e) => this.handleInputChangeEvent(e,'confirmPassword')} onBlur={(e) => this.handleInputChangeEvent(e,'confirmPassword')}/>
                                <span className="error">{this.state.confirmPasswordEmptyMsz}</span>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">Phone Number (Cell)*</label>
                                <PhoneInput className={(this.state.cellNumberBorder ? 'inputError' : '')} maxLength="14" placeholder="(123) 456-7890" value={ this.state.cellNumber } country="US" onChange={phone => this.handleInputChangeEvent(phone,'cellNumber')} />
                                <span className="error">{this.state.cellNumberEmptyMsz}</span>
                            </Col>
                        </Row>
                        <label className="profile-text width100">Profile Picture: (Image must not exceed 512 x 512)</label>
                        <Row className="profile-Row profileMargin">
                                <img src={this.state.currentUserImage} alt="profile-pic" className="profile-pic"/>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <input type="file" id="uploadBtn" className="hide" onChange={ (e) => this.handleChange(e) } />
                                <Button className="uploadFile fsnetBtn" onClick={this.uploadBtnClick}>Upload File</Button> <br/>
                                <label className="removeBtn" onClick={this.removeImageBtn}>Remove</label>
                            </Col>
                        </Row>
                        <CBox checked={this.state.fsnetTermsandServices} className="" onChange={(e) => this.handleInputChangeEvent(e,'fsnetTermsandServices')}>
                        <span className="checkmark"></span>
                        </CBox>
                        <label className="rememberLabel remember-text">By checking this box you agree to  <Link to="/terms-and-conditions" target="_blank">Cooley&apos;s Terms of Service</Link></label><br/>
                        <div className="error marginTopminus10">{this.state.termsandConditionsRequired}</div>
                        <div className="error">{this.state.errorMessage}</div>
                        <Button className={"signupBtn "+ (this.state.isFormValid ? 'btnEnabled' : '') } disabled={!this.state.isFormValid} onClick={this.showConfirmationModal}>Sign Up</Button>
                        <label className="signIn-text"> <a href="/login">Already have an account? Sign In</a></label>
                        </form>
                    </div>
                    <div className="topBorder bottomBorder"></div>
                    <Loader isShow={this.state.showModal}></Loader>
                </Row>
                <Row hidden={!this.state.showSuccesScreen} className="sucess-row">
                    <div className="topBorder"></div>
                    <Col lg={12} md={12} sm={12} xs={12}>
                        <label className="register-success-text">Success</label>
                        <label className="register-reset-text">Your account has been created</label>
                        <div className="success-icon">
                            <img src={successImage} alt="success"/>
                        </div>
                        <div className="text-center">
                            <Button className="register-submit" onClick={this.navigateToLogin}>Proceed to Login</Button> <br/>
                        </div>
                    </Col>
                </Row>
                

                <Modal id="confirmInvestorModal" show={this.state.confirmationModal} onHide={this.closeConfirmationModal} dialogClassName="confirmInvestorDialog confirmInvestorModalWidth">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                         <div className="title-text">
                            <div className="title-text-center">Please confirm your name below.  This will be the signatory name that will be used to electronically sign all documents in Vanilla</div>
                            <p className="fundNameCenter">{this.state.firstName} {this.state.middleName} {this.state.lastName}</p>
                        </div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeConfirmationModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" onClick={this.signUpFn} className="fsnetSubmitButton btnEnabled" >Confirm</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>


            </div>
        );
    }
}

export default RegisterComponent;


import React, { Component } from 'react';
import { Constants } from '../../constants/constants';

class FooterComponent extends Component {

    constructor(props) {
        super(props);
        this.Constants = new Constants();
        this.state = {
        }
    }

    componentDidMount() {

    }


    render() {
        return (
                <div className="footer">
                    <div className="footerTitle">{this.Constants.VERSION}</div>
                </div>
        );
    }
}

export default FooterComponent;




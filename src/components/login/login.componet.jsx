import React, { Component } from 'react';
import './login.component.css';
import { Row, Col, Form, FormGroup, Checkbox as CBox, FormControl, ControlLabel, Button } from 'react-bootstrap';
import Loader from '../../widgets/loader/loader.component';
import { Constants } from '../../constants/constants';
import { Fsnethttp } from '../../services/fsnethttp';
import { FsnetAuth } from '../../services/fsnetauth';
import { FsnetUtil } from '../../util/util';
import vanillaLogo from '../../images/Vanilla.png';
import ReactDOM from 'react-dom';
 

class LoginComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            showModal: false,
            loginErrorMsz: '',
            isFormValid: false,
            jsonData: {},
            email:null,
            validEmail:false,
            password:null
        }
    }

    componentDidMount() {
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.getJsonData()
        //Check if user already logged in
        //If yes redirect to dashboard page
        if (this.FsnetAuth.isAuthenticated()) {
            this.props.history.push('/dashboard');
        }
    }

    getJsonData() {
        this.Fsnethttp.getJson('login').then(result => {
            this.setState({
                jsonData: result.data
            })
        });

    }

    //Reset all state values to default value.
    componentWillUnmount() {
        this.setState({
            showModal: false,
            email: '',
            password: '',
            isRememberMe: false,
            loginErrorMsz: '',
        });
    }

    // On enter key call auto login.
    autoLoginOnEnterKey = (event) => {
        if (event.key === 'Enter') {
            let currentElem = ReactDOM.findDOMNode(this.login)
            currentElem.click();
        }
    }

    loginFn = () => {
        if (this.state.email.trim() && this.state.password.trim()) {
            this.open();
            const {email, password, isRememberMe} = this.state;
            let loginObj = { email: email, password: password, rememberMe: isRememberMe };
            this.Fsnethttp.login(loginObj).then(result => {
                this.close();
                if (result.data) {
                    this.FsnetUtil._userSetData(result.data);
                    setTimeout(() => {
                        let url = this.FsnetUtil.getUserRole() === 'FSNETAdministrator' ? "/adminDashboard/firmView" : result.data.allusers.length > 1 ? "/userRoles" : "/dashboard";
                        this.props.history.push(url);
                    }, 200);
                }
            }).catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        loginErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        loginErrorMsz: this.Constants.INVALID_LOGIN,
                    });
                }
            });
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    enableLoginButton() {
        let status = (this.state.email && this.state.validEmail && this.state.password) ? true : false;
        this.setState({
            isFormValid: status
        });
    }

    //New Validation Handler
    handleChangeEvent = (event, type, blur) => {
        if (type === 'isRememberMe') {
            this.setState({
                [type]: event.target.checked
            })
        }
        const value = event.target.value.trim();
        if(type === 'email') {
            const error = this.FsnetUtil._emailValidation(value);
            if(error) {
                this.setState({
                    [type + 'Touched']: true,
                    validEmail:false
                },()=>{
                    this.enableLoginButton();
                })
               return; 
            }
        }
        if (value) {
            
            this.setState({
                [type]: value,
                [type + 'Touched']: false,
                validEmail: type === 'email' ? true : this.state.validEmail
            }, () => {
                this.enableLoginButton();
            })
        } else {
            this.setState({
                [type]: '',
                [type + 'Touched']: true,
                validEmail: type === 'email' ? false : this.state.validEmail
            }, () => {
                this.enableLoginButton();
            })
        }

    }

    // common function :: To display error messages
    ErrorMsg(props) {
        return (
            <div className={props.errObj.class}>{props.errObj.msg}</div>
        );
    }

    render() {
        return (
            <Row id="loginContainer" className="marginNone">
                <Row className="mainContainer">
                    <img src={vanillaLogo} alt="vanilla" className="vanilla-logo marginLeft30" />
                </Row>
                <Row className="loginContainer">
                    <Col lg={6} md={6} sm={6} xs={12}>
                        <p className="content-heading">{this.state.jsonData.LOGIN_LEFT_SECTION_TEXT_PARAGRAPH_1}</p>
                        <p className="content-text">{this.state.jsonData.LOGIN_LEFT_SECTION_TEXT_PARAGRAPH_2}<br /><br />{this.state.jsonData.LOGIN_LEFT_SECTION_TEXT_PARAGRAPH_3}<br /><br />{this.state.jsonData.LOGIN_LEFT_SECTION_TEXT_PARAGRAPH_4}</p>
                    </Col>
                    <Col lg={6} md={6} sm={6} xs={12} className="paddingLeft7 paddingZero">
                        <div className="formContainer">
                            <p className="labelSignIn">{this.state.jsonData.LOGIN_RIGHT_SECTION_HEADER}</p>
                            <this.ErrorMsg errObj={{ msg: this.state.loginErrorMsz, class: "error marginLeft20" }} />
                            <Form horizontal id="loginForm" autoComplete="off">
                                <FormGroup controlId="email">
                                    <ControlLabel className="labelFormControl">Email</ControlLabel>
                                    <FormControl type="text" placeholder="Email" autoComplete="off" className={"formControl " + (this.state.emailTouched && !this.state.email ? 'inputError' : '')} inputRef={(input) => { this.email = input }} onChange={(e) => this.handleChangeEvent(e, 'email')} onBlur={(e) => this.handleChangeEvent(e, 'email',true)} onKeyPress={this.autoLoginOnEnterKey} />
                                    <this.ErrorMsg errObj={{ msg: this.state.emailTouched && !this.state.email ? this.Constants.LOGIN_EMAIL_REQUIRED : this.state.emailTouched && !this.state.validEmail ? this.Constants.VALID_EMAIL:'', class: 'error' }} />
                                </FormGroup>
                                <FormGroup controlId="password">
                                    <ControlLabel className="labelFormControl">{this.state.jsonData.PASSWORD}</ControlLabel>
                                    <FormControl type="password" placeholder={this.state.jsonData.PASSWORD} className={"formControl " + (this.state.passwordTouched && !this.state.password ? 'inputError' : '')} inputRef={(input) => { this.password = input }} autoComplete="off" onChange={(e) => this.handleChangeEvent(e, 'password')} onBlur={(e) => this.handleChangeEvent(e, 'password')} onKeyPress={this.autoLoginOnEnterKey} />
                                    <this.ErrorMsg errObj={{ msg: this.state.passwordTouched && !this.state.password ? this.Constants.LOGIN_PASSWORD_REQUIRED : '', class: 'error' }} />
                                </FormGroup>
                                <CBox onChange={(e) => this.handleChangeEvent(e, 'isRememberMe')}>
                                    <span className="checkmark"></span>
                                </CBox>
                                <label className="remember-text">{this.state.jsonData.REMEMBER_USERNAME}</label>
                                <Button ref={(button) => { this.login = button }} className={"signinBtn " + (this.state.isFormValid ? 'btnEnabled' : 'disabled')} disabled={!this.state.isFormValid} onClick={this.loginFn}>{this.state.jsonData.SIGN_IN_TEXT}</Button>
                                <div>
                                    <span className="forgot-pwd paddingLeft15"><a href="/forgot-password">{this.state.jsonData.FORGOT_PASSWORD}</a></span>
                                    {/* <span className="forgot-pwd padding12">|</span> */}
                                    {/* <span className="forgot-pwd"><a href="/forgot-username">{this.state.jsonData.FORGOT_USERNAME}</a></span> */}
                                    <span className="forgot-pwd padding12">|</span>
                                    <span className="forgot-pwd"><a href="mailto:vanillavc@cooley.com" target="_top">{this.state.jsonData.CONTACT_US}</a></span>
                                </div>
                            </Form>
                        </div>
                        <Loader isShow={this.state.showModal}></Loader>
                    </Col>
                </Row>
            </Row>
        );
    }
}

export default LoginComponent;


import React, { Component } from 'react';
import { Row, Col, Button, FormControl } from 'react-bootstrap';
import HeaderComponent from '../authheader/header.component'
import './setpassword.component.css';
import { FsnetAuth } from '../../services/fsnetauth';
import { Fsnethttp } from '../../services/fsnethttp';
import { FsnetUtil } from '../../util/util';
import 'react-phone-number-input/rrui.css';
import 'react-phone-number-input/style.css';
import { Constants } from '../../constants/constants';
import Loader from '../../widgets/loader/loader.component';
import successImage from '../../images/success.png';
import { reactLocalStorage } from 'reactjs-localstorage';


class SetPasswordComponent extends Component {
    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.Constants = new Constants();
        this.state = {
            showSuccesScreen: false,
            isFormValid: false,
            errorMsz: ''
        }
    }

    componentDidMount() {
        let urlValues = this.FsnetUtil.decodeUrl();
        console.log(urlValues)
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () => {
        this.setState({ showModal: true });
    }

    proceedToLogin = () => {
        this.props.history.push('/login')
    }

    handleChangeEvent = (event, type) => {
        const value = event.target.value.trim();
        if (value) {
            this.setState({
                [type]: value,
                [type + 'Touched']: false
            }, () => {
                this.enableSetPwdBtn()
            })
        } else {
            this.setState({
                [type]: '',
                [type + 'Touched']: true
            }, () => {
                this.enableSetPwdBtn()
            })
        }

    }

    enableSetPwdBtn() {
        this.setState({
            errorMsz: ''
        }, () => {
            if(this.state.password && this.state.confirmPassword) {
                if (this.state.password !== this.state.confirmPassword) {
                    this.setState({
                        errorMsz: this.Constants.REQUIRED_PASSWORD_AGAINPASSWORD_SAME,
                    });
                } else {
                    let passwordRegex = this.Constants.PASSWORD_REGEX;
                    if (!passwordRegex.test(this.state.password) || !passwordRegex.test(this.state.confirmPassword)) {
                        this.setState({
                            errorMsz: this.Constants.PASSWORD_NOT_MATCH,
                        });
                    } else {
                        this.setState({
                            isFormValid: this.state.password && this.state.confirmPassword ? true : false
                        })
                    }
                }
            }
        })
    }

    logout = () => {
        this.Fsnethttp.logout().then(result => {
            this.FsnetUtil._ClearLocalStorage(true);
        });
    }

    setPasswordFn = () =>{
        let urlValues = this.FsnetUtil.decodeUrl();
        if(urlValues) {
            this.open();
            const postObj = {password:this.state.password}
            this.Fsnethttp.setPassword(urlValues,postObj).then(result => {
                this.close();
                //If user is logged in deleting the local storage data.
                const token = this.FsnetUtil.getToken();
                if(token) {
                    this.logout()
                }
                if(urlValues.gprequestid) {
                    this.gpRoleAcceptFn();
                } else {
                    this.setState({
                        showSuccesScreen:true
                    })
                }
               
            })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                   this.setState({
                       errorMsz: error.response.data.errors[0].msg,
                   })
                }else {
                   this.setState({
                        errorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                   });
                }
            });
        }
    }


    gpRoleAcceptFn = () => {
        let urlValues = this.FsnetUtil.decodeUrl();
        this.open();
        const postObj = { code: urlValues.code, gprequestid: urlValues.gprequestid }
        this.Fsnethttp.userAcceptCode(postObj).then(result => {
            this.close();
            this.setState({
                showSuccesScreen: true
            })

        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        errorMsz: error.response.data.errors[0].msg,
                    })
                } else {
                    this.setState({
                        errorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });
    }


    render() {
        return (
            <div className="parentContainer" id="forgotContainer">
                <HeaderComponent></HeaderComponent>
                <Row className="forgotParentDiv">
                    {
                        !this.state.showSuccesScreen ?
                            <Row>
                                <Col className="width40" lg={6} md={6} sm={6} xs={12}>
                                    <label className="forgot-label-text">Password</label>
                                    <input type="password" name="password" onChange={(e) => this.handleChangeEvent(e, 'password')} onBlur={(e) => this.handleChangeEvent(e, 'password')} className={"forgotFormControl inputMarginBottom " + (this.state.passwordTouched && !this.state.password ? 'inputError' : '')} /><br />
                                    {(this.state.passwordTouched && !this.state.password)&&<span className="error">{this.Constants.LOGIN_PASSWORD_REQUIRED}</span>}
                                </Col>
                                <Col className="width40" lg={6} md={6} sm={6} xs={12}>
                                    <label className="forgot-label-text">Confirm password</label>
                                    <input type="password" name="confirmPassword" onChange={(e) => this.handleChangeEvent(e, 'confirmPassword')} onBlur={(e) => this.handleChangeEvent(e, 'confirmPassword')} name="confirmPassword" className={"forgotFormControl " + (this.state.confirmPasswordTouched && !this.state.confirmPassword ? 'inputError' : '')} />
                                    {(this.state.confirmPasswordTouched && !this.state.confirmPassword) && <span className="error">{this.Constants.CNFRM_PWD_REQUIRED} </span>}
                                </Col>
                                <Row>
                                    <div className="error marginLeft30 marginTop20">{this.state.errorMsz} </div>
                                </Row>
                                <Row>
                                    <Button className={"forgot-submit " + (this.state.isFormValid ? 'btnEnabled' : 'disabled')} onClick={this.setPasswordFn}>Set Password</Button> <br />
                                </Row>
                                <label className="cancel-text cancelBtn"> <a href="/login">Cancel</a></label>
                            </Row>
                            :
                            <Row>
                                <Col lg={12} md={12} sm={12} xs={12}>
                                    <label className="forgot-success-text">Success</label>
                                    <label className="forgot-reset-text">Your password has been set.</label>
                                    <div className="success-icon">
                                        <img src={successImage} alt="success" />
                                    </div>
                                    <div className="text-center">
                                        <Button className="forgot-submit fsnetBtn" onClick={this.proceedToLogin}>Sign In</Button> <br />
                                    </div>
                                </Col>
                            </Row>
                    }
                </Row>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default SetPasswordComponent;


import React, { Component } from 'react';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Row, Col, Button, Checkbox as CBox, Modal } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import FileDrop from 'react-file-drop';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import userDefaultImage from '../../../images/default_user.png';
import documentImage from '../../../images/documentsWhite.svg';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';

var close = {}
class addendumsComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        
        
        this.handleChange = this.handleChange.bind(this);
        this.uploadBtnClick = this.uploadBtnClick.bind(this);
        this.proceedToNext = this.proceedToNext.bind(this);
        this.proceedToBack = this.proceedToBack.bind(this);
        this.newAddendumFn = this.newAddendumFn.bind(this);
        this.closeAddendumModal = this.closeAddendumModal.bind(this);
        this.handleInputChangeEvent = this.handleInputChangeEvent.bind(this);
        this.showModal = this.showModal.bind(this);
        this.startBtnFn = this.startBtnFn.bind(this);
        this.deleteFile = this.deleteFile.bind(this);
        this.sortAddendum = this.sortAddendum.bind(this);
        this.selectAllChange = this.selectAllChange.bind(this);
        this.openAddendumdoc = this.openAddendumdoc.bind(this);
        this.state = {
            showModal: false,
            showSideNav: true,
            uploadDocFile: {},
            uploadFileName: '',
            uploadDocSize:'',
            showFooterButtons:false,
            showUploadPage: false,
            showAddendumPage: true,
            showLpPage:false,
            showAddendumModal:false,
            getAddendumsList: [],
            getLpList: [],
            noAddendumsMsz: '',
            noLpsMsz: '',
            showNameAsc: true,
            showOrgAsc: true,
            fundId:'',
            lpSelectedList:[],
            nextIconDisabled:true,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
        }

        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('profileModal', data);
    }

    closeToast(timed) {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.setState({
            fundId:id
        })
        this.getAddendums(id);
        this.getLps(id);

    }

    //Get list of LPS
    getLps(id) {
        if(id) {
            this.open();
            
            this.Fsnethttp.getFundLps(id).then(result => {
                this.close();
                this.setState({
                    getLpList: result.data.data
                })
            })
            .catch(error => {
                this.close();
                this.setState({
                    getLpList: [],
                    noLpsMsz: this.Constants.NO_LPS
                })
    
            });
        }
    }


    //Get list of Addendums
    getAddendums(id) {
        if(id) {
            this.open();
            
            this.Fsnethttp.getAddendums(id).then(result => {
                this.close();
                this.setState({
                    getAddendumsList: result.data.data
                },()=>{
                    // //console.log(this.state.getAddendumsList)
                })
            })
            .catch(error => {
                this.close();
                this.setState({
                    getAddendumsList: [],
                    noAddendumsMsz: this.Constants.NO_ADDENDUMS
                })
    
            });
        }
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    hamburgerClick() {
        if (this.state.showSideNav === true) {
            this.setState({
                showSideNav: false
            })
        } else {
            this.setState({
                showSideNav: true
            })
        }
    }

    uploadBtnClick() {
        document.getElementById('uploadBtn').click();
    }

    //Upload patnership document.
    handleChange(event, type) {
        let obj;
        if(type=== 'drop') {
            obj = event;
        } else {
            obj = event.target.files
        }
        let reader = new FileReader();
        if(obj && obj.length > 0) {
            this.uploadFile = obj[0];
            let sFileName = obj[0].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            if(sFileExtension !== 'pdf') {
                document.getElementById('uploadBtn').value = "";
                alert('Fund Agreement Change document must be in PDF format and smaller than 10MB.')
                return true;
            }
            //File 10MB limit
            if(this.uploadFile.size <= this.Constants.SIZE_LIMIT) {
                this.setState({
                    uploadDocFile : obj[0],
                    uploadDocSize: (this.uploadFile.size / this.Constants.SIZE_MB).toFixed(2)+' MB',
                    nextIconDisabled:false
                });
                reader.readAsDataURL(this.uploadFile);
                this.setState({
                    uploadFileName: obj[0].name,
                });
            } else {
                document.getElementById('uploadBtn').value = "";
               alert('Fund document should be less than 10 MB.')
            }
        }
    }

    deleteFile() {
        document.getElementById('uploadBtn').value = "";
        this.setState({
            uploadDocFile : {},
            uploadDocSize: '',
            uploadFileName:'',
            nextIconDisabled:true
        });
    }

    enableNextIcon() {
        if(this.state.lpSelectedList.length >=1) {
            this.setState({
                nextIconDisabled:false
            })
        } else {
            this.setState({
                nextIconDisabled:true
            })
        }
    }

    proceedToNext() {
        if(this.state.showUploadPage) {
            this.setState({
                showLpPage:true,
                showUploadPage:false,
                
            },()=>{
                this.enableNextIcon();
            })
        } else {
            this.showModal();
        }
    }

    proceedToBack() {
        if(!this.state.showUploadPage) {
            this.setState({
                showLpPage:false,
                showUploadPage:true,
                nextIconDisabled:false
            })
        } else {
            this.setState({
                showFooterButtons:false,
                showUploadPage:false,
                showAddendumPage:true,
                nextIconDisabled:false
            })
        }
    }

    newAddendumFn() {
        this.setState({
            showUploadPage:true,
            showAddendumPage:false,
            showFooterButtons:true
        })
    }

    closeAddendumModal() {
        this.setState({
            showAddendumModal: false,
        })
    }

    showModal() {
        this.setState({
            showAddendumModal: true,
        })
    }

    startBtnFn() {
        if (this.state.lpSelectedList.length > 0) {
            this.open();
            
            var formData = new FormData();
            formData.append("lpIds", JSON.stringify(this.state.lpSelectedList));
            formData.append("fundId", this.state.fundId);
            formData.append("addendumDetails", this.state.uploadDocFile);
            this.Fsnethttp.addAddendums(formData).then(result => {
                this.close();
                this.closeAddendumModal();
                this.setState({
                    showToast: true,
                    toastMessage: 'Fund Agreement Change has been added successfully.',
                    toastType: 'success',
                },()=>{
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                })
            })
            .catch(error => {
                this.close();
            });
        } 
    }

    //Sorting for name and organization
    sortAddendum(e, colName, sortVal) {
        
        let data = JSON.parse(reactLocalStorage.get('userData'));
        let firmId = data['vcfirmId'];
        let fundId = this.state.fundId;
        if (this.state.getLpList && this.state.getLpList.length > 1) {
            this.open();
            this.Fsnethttp.getLpSort(firmId, fundId, colName, sortVal).then(result => {
                if (result.data && result.data.data.length > 0) {
                    this.close();
                    this.setState({ getLpList: result.data.data });
                    if (colName === 'firstName') {
                        if (sortVal === 'desc') {
                            this.setState({
                                showNameAsc: true
                            })
                        } else {
                            this.setState({
                                showNameAsc: false
                            })
                        }
                    } else {
                        if (sortVal === 'desc') {
                            this.setState({
                                showOrgAsc: true
                            })
                        } else {
                            this.setState({
                                showOrgAsc: false
                            })
                        }
                    }

                } else {
                    this.close();
                    this.setState({
                        getLpList: [],
                        showNameAsc: false
                    }, )
                }

            })
            .catch(error => {
                this.close();
                this.setState({
                    getLpList: []
                })

            });
        }
    }

    selectAllChange(e) {
        if(e.target.checked) {
            let getSelectedList = this.state.getLpList;
            let list=[]
            for(let index of getSelectedList) {
                if (list.indexOf(index['id']) === -1) {
                    list.push(index['id']);
                }
            }
            this.setState({
                lpSelectedList:list
            },()=>{
                this.enableNextIcon();
            });
        } else {
            this.setState({
                lpSelectedList:[]
            });
        }
    }

    openAddendumdoc(e,data) {
        if(data.record.addendumLps[0].documentsForSignature && data.record.addendumLps[0].documentsForSignature.isSigned) {
            
            this.open();
            this.Fsnethttp.getAddendumAgreement(data.record.addendumLps[0].documentsForSignature.envelopeId, data.record.addendumLps[0].subscriptionId).then(result => {
                this.close();
                if (result.data && result.data.url) {
                    const docUrl = `${result.data.url}`
                    this.FsnetUtil._openDoc(docUrl)
                }
            })
            .catch(error => {
                this.close();
            });
        } else if(data.record.addendumDetails) {
            const docUrl = `${data.record.addendumDetails.url}`
            this.FsnetUtil._openDoc(docUrl)
        }
    }

    handleInputChangeEvent(event,data) {
        let getSelectedList = this.state.lpSelectedList;
        let selectedId = data.record.lpId;
        if (event.target.checked) {
            let obj = {subscriptionId:data.record.subscriptionId, lpId:data.record.lpId}
            getSelectedList.push(obj);
        } else {
            getSelectedList = getSelectedList.filter(person => person.lpId !== selectedId);
        }
        // this.updateSelectedValueToList(obj.record.id, event.target.checked)
        this.setState({
            lpSelectedList: getSelectedList,
        },()=>{
           this.enableNextIcon();
        })
    }

    render() {
        return (
            <div className="lpSubFormStyle addendumContainer">
                <div className="main-heading" hidden={this.state.showAddendumPage}><span className="main-title">Add New Fund Agreement Change</span><Link to="/dashboard" className="cancel-fund">Cancel</Link></div>
                <div className="main-heading" hidden={!this.state.showAddendumPage}><span className="main-title">Fund Agreement Change</span></div>
                <div className="LpDelegatesContainer marginTop20 marginLeft25 paddingLeft15" hidden={!this.state.showAddendumPage}>
                    <p className="Subtext">Upload documents to create a new one.</p>
                    <Button className="newAddendumButton" onClick={this.newAddendumFn}><i className="fa fa-plus paddingRight5"></i>New Fund Agreement Change</Button>
                    {this.state.getAddendumsList.length > 0 ?
                    <Row className="full-width marginTop20 marginLeft61">
                        <div className="name-heading marginLeft75">
                            Investor Name
                        </div>
                        <div className="name-heading">
                            Organization
                        </div>
                    </Row>
                    : ''
                    } 
                    {/* {this.state.getAddendumsList.length > 0 ?
                    <Row className="full-width marginTop20 marginLeft61">
                        <div className="name-heading marginLeft75" hidden={!this.state.showNameAsc} onClick={(e) => this.sortAddendum(e, 'firstName', 'asc')}>
                            Investor Name
                                    <i className="fa fa-sort-asc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading marginLeft75" onClick={(e) => this.sortAddendum(e, 'firstName', 'desc')} hidden={this.state.showNameAsc}>
                            Investor Name
                                <i className="fa fa-sort-desc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading" onClick={(e) => this.sortAddendum(e, 'organizationName', 'asc')} hidden={!this.state.showOrgAsc}>
                            Organization
                                <i className="fa fa-sort-asc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading" onClick={(e) => this.sortAddendum(e, 'organizationName', 'desc')} hidden={this.state.showOrgAsc}>
                            Organization
                                <i className="fa fa-sort-desc" aria-hidden="true" ></i>
                        </div>
                    </Row> : ''
                } */}
                    <div className={"userAddendumContainer " + (this.state.getAddendumsList.length === 0 ? 'borderNone' : 'marginTop10')}>
                        {this.state.getAddendumsList.length > 0 ?
                            this.state.getAddendumsList.map((record, index) => {
                                return (
                                    <div key={index}>
                                    {
                                        record.addendumLps.map((data, idx) => {
                                            return (
                                                <div className="userRow" key={idx}>
                                                    <label className="userImageAlt">
                                                    {
                                                        data.lp['profilePic'] ?
                                                            <img src={data.lp['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                                            : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                                    }
                                                    </label>
                                                    <div className="lp-name">{this.FsnetUtil.getFullName(data.lp)}</div>
                                                    <div className="lp-name lp-name-pad">{data.lp['organizationName']}</div>
                                                    <Button className="btnViewPrint" onClick={(e)=>this.openAddendumdoc(e,{record})}><img src={documentImage} alt="home_image"/><span className="viewPrintText">View + Print</span></Button>
                                                </div>
                                            )

                                        })
                                    }
                                    </div>
                                );
                            })
                            :
                            <div className="title margin20 text-center">You haven’t added any Fund Agreement Change to this Fund yet.</div>
                        }
                    </div>
                </div>
                <div className="LpDelegatesContainer marginTop6 marginLeft25 marginBot20 paddingLeft15" id="createFund" hidden={!this.state.showUploadPage}>
                    <h1 className="uploadFundDocument">Upload Fund Agreement Change Documents</h1>
                    <div>
                        <h1 className="title marginBottom2">Choose files to upload</h1>
                        {/* <div className="subtext">Choose document files to upload. Accepted files information appears here.</div> */}
                        <div className="uplodFileContainer marginTop46">
                            <input type="file" id="uploadBtn" className="hide" onChange={ (e) => this.handleChange(e) } />
                            <FileDrop onDrop={(e) => this.handleChange(e, 'drop')}>
                                <Button className="uploadFileBox" onClick={this.uploadBtnClick}></Button>
                                <span className="uploadFileSubtext">{this.Constants.UPLOAD_TEXT}</span>
                            </FileDrop>
                            <div className="docNameDivAlign"><div className="upload-doc-name">{this.state.uploadFileName} </div> </div>
                            <div className="filesize" hidden={this.state.uploadFileName ===''}>{this.state.uploadDocSize}  <i className="fa fa-trash cursor-pointer" onClick={this.deleteFile}></i></div>
                        </div>
                    </div>
                </div>
                <div className="LpDelegatesContainer marginTop6 marginLeft25 paddingLeft15" hidden={!this.state.showLpPage}>
                    <h1 className="title marginBottom2">Choose Investors</h1>
                    <div className="subtext">Check off the Investors that you want to send this Fund Agreement Change to.</div>
                    {/* <Row className="full-width marginTop20">
                        <div className="name-heading marginLeft30">
                            Investor Name
                                    <i className="fa fa-sort-asc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading">
                            Organization
                                <i className="fa fa-sort-asc" aria-hidden="true"></i>
                        </div>
                    </Row>
                    <div className="userLPContainer marginTop10 marginBottom12">
                        <div className="userRow">
                            <div className="lp-name paddingLeft0 lpNameTextAlign"><img src={userDefaultImage} alt="img" className="user-image marginRight12" />SarahDouglas</div>
                            <div className="lp-name lp-name-pad lpNameTextAlign">Menlo Technologies</div>
                            <CBox className="marginLeft8">
                                <span className="checkmark"></span>
                            </CBox>
                        </div>
                    </div> */}
                    {this.state.getLpList.length > 0 ?
                    <Row className="full-width marginTop20 marginLeft61">
                        <div className="name-heading marginLeft75" hidden={!this.state.showNameAsc} onClick={(e) => this.sortAddendum(e, 'firstName', 'asc')}>
                            Investor Name
                                    <i className="fa fa-sort-asc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading marginLeft75" onClick={(e) => this.sortAddendum(e, 'firstName', 'desc')} hidden={this.state.showNameAsc}>
                            Investor Name
                                <i className="fa fa-sort-desc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading" onClick={(e) => this.sortAddendum(e, 'organizationName', 'asc')} hidden={!this.state.showOrgAsc}>
                            Organization
                                <i className="fa fa-sort-asc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading" onClick={(e) => this.sortAddendum(e, 'organizationName', 'desc')} hidden={this.state.showOrgAsc}>
                            Organization
                                <i className="fa fa-sort-desc" aria-hidden="true" ></i>
                        </div>
                    </Row> : ''
                }
                    <div className={"userAddendumContainer " + (this.state.getLpList.length === 0 ? 'borderNone' : 'marginTop10')}>
                    {this.state.getLpList.length > 0 ?
                        this.state.getLpList.map((record, index) => {
                            return (

                                <div className="userRow" key={index}>
                                    <label className="userImageAlt">
                                    {
                                        record['profilePic'] ?
                                            <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                            : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                    }
                                    </label>
                                    <div className="lp-name">{this.FsnetUtil.getFullName(record)}</div>
                                    <div className="lp-name lp-name-pad">{record['organizationName']}</div>
                                    <CBox className="marginLeft8" checked={record['checked']} onChange={(e) => this.handleInputChangeEvent(e, { record })}>
                                        <span className="checkmark"></span>
                                    </CBox>
                                </div>
                            );
                        })
                        :
                        <div className="title margin20 text-center">{this.state.noDelegatesMsz}</div>
                    }
                    
                    </div>
                    <div className="selectAllDiv">
                        {/* <CBox className="marginRight8" onChange={(e) =>this.selectAllChange(e)}>
                                    <span className="checkmark"></span>
                        </CBox>
                        <span className="lpSelectAll">Select All</span> */}
                    </div>
                </div>
                <div className="footer-nav" hidden={!this.state.showFooterButtons}>
                    <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                    <i className={"fa fa-chevron-right " + (this.state.nextIconDisabled ? 'disabled' : '')} onClick={this.proceedToNext}  aria-hidden="true"></i>
                </div>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>   
                <Loader isShow={this.state.showModal}></Loader>
                <Modal id="confirmAddendumModal" show={this.state.showAddendumModal}  onHide={this.closeAddendumModal} dialogClassName="confirmFundDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title">Are you sure you want to add this Fund Agreement Change?</h1>
                        <div className="subtext">All Investors assigned to the Fund Agreement Change will receive an email invitation and in app notification of the new document.</div>
                        <div className="error"></div>
                        <Row className="fundBtnRow">
                            <Col lg={5} md={5} sm={5} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.closeAddendumModal}>No, take me back</Button>
                            </Col>
                            <Col lg={7} md={7} sm={7} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.startBtnFn}>Yes, add Fund Agreement Change</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
            </div>
            

        );
    }
}

export default addendumsComponent;


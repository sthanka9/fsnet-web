import React, { Component } from 'react';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Row, Col, Button, Checkbox as CBox, FormControl } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';
import userDefaultImage from '../../../images/default_user.png';

var close = {}
class documentLockerComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.handleOnChange = this.handleOnChange.bind(this);
        this.state = {
            showModal: false,
            showSideNav: true,
            gpDocumentLockerList: [],
            gpDocumentLockerListOrg: [],
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            getLpList:[],
            getDocumentTypes:[],
            selectedDocs: [],
            showDocumentLocker:false,
            lpNameFilterId:0,
            documentTypeFilter:0,
            showNameAsc:false,
            showTypeAsc:false,
            documentLockerMsz:''
        }

        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast(timed) {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.setState({
            fundId:id
        })
        this.getGpDocumentLocker(id);
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        //PubSub.publish('profileModal', data['record']['details']);
    }


    //Get list of Addendums
    getGpDocumentLocker(id) {
        if(id) {
            this.open();
            
            this.Fsnethttp.getGpDocumentLocker(id).then(result => {
                this.close();
                window.scrollTo(0, 0);
                let totalList = result.data && result.data.data.length ===1 &&  result.data.data[0].documentType === 'Consolidated Fund Agreement' ? []:result.data.data
                this.setState({
                    gpDocumentLockerList: totalList,
                    gpDocumentLockerListOrg:totalList,
                    documentLockerMsz: this.Constants.NO_DOCUMENTLOCKER_DOCUMENTS
                },()=>{
                    if(result.data.data.length >0) {
                        this.getLPList();
                        this.getDocumentTypes();
                    }
                    // //console.log(this.state.gpDocumentLockerList)
                })
                //this.checkTypeForallLps(result.data.data.lps)
            })
            .catch(error => {
                this.close();
                this.setState({
                    gpDocumentLockerList: [],
                    documentLockerMsz: this.Constants.NO_DOCUMENTLOCKER_DOCUMENTS
                })
    
            });
        }
    }

    getLPList() {
        this.open();
        
        let vcfirmId = JSON.parse(reactLocalStorage.get('firmId'));
        let fundId = this.state.fundId;
        if(vcfirmId && fundId) {
            this.Fsnethttp.getLpList(vcfirmId,fundId).then(result => {
                this.close();
                this.setState({
                    getLpList: result.data.data
                })
            })
            .catch(error => {
                this.close();
                this.setState({
                    getLpList: []
                })
    
            });
        }
    }

    getDocumentTypes() {
        this.open();
        
        this.Fsnethttp.getDocumentTypes().then(result => {
            this.close();
            this.setState({
                getDocumentTypes: result.data.data.documentTypes
            })
        })
        .catch(error => {
            this.close();
            this.setState({
                getDocumentTypes: []
            })

        });
    }

    // filterDocumentLocker(fundId,lpId, lastName, filter) {
    filterBy(lpId, name, filter,sortType) {
        if(this.state.gpDocumentLockerListOrg.length > 0) {
            this.open();
            
            this.Fsnethttp.filterDocumentLocker(this.state.fundId,lpId,name,filter,sortType).then(result => {
                this.close();
                let filterList = result.data.data;
                if(filter !== '' && filter !== 0) {
                    filterList = result.data.data;
                }
                this.setState({
                    gpDocumentLockerList: filterList
                },()=>{
                    if(filterList.length === 0 ) {
                        this.setState({
                            documentLockerMsz: this.Constants.NO_DOCUMENTLOCKER_FILTER_DOCUMENTS
                        })
                    }
                })
            // this.checkTypeForallLps(result.data.data.lps)
            })
            .catch(error => {
                this.close();
                this.setState({
                    gpDocumentLockerList: [],
                    documentLockerMsz: this.Constants.NO_DOCUMENTLOCKER_FILTER_DOCUMENTS
                })

            });
        } else {
            this.setState({
                documentLockerMsz: this.Constants.NO_DOCUMENTLOCKER_FILTER_DOCUMENTS
            })
        }
    }


    handleOnChange(e,type, sortType) {
        let value = e.target.value == 0 ? '' : e.target.value
        if(type === 'filterLpName') {
            this.setState({
                lpNameFilterId:value,
                selectedDocs: [],
                includeAllFile: false,
                showNameAsc: false,
                showTypeAsc: false,
            },()=>{
                this.filterBy(this.state.lpNameFilterId, '', this.state.documentTypeFilter) 
            })
        } else if(type === 'filterDocumentType') {
            this.setState({
                documentTypeFilter:value,
                selectedDocs: [],
                includeAllFile: false,
                showNameAsc: false,
                showTypeAsc: false,
            },()=>{
                this.filterBy(this.state.lpNameFilterId, '', this.state.documentTypeFilter) 
            })
        } else if(type === 'firstName') {
            this.setState({
                showNameAsc: !this.state.showNameAsc
            },()=>{
                this.filterBy(this.state.lpNameFilterId, 'firstName', this.state.documentTypeFilter,sortType) 
            })
        }else if(type === 'docType') {
            this.setState({
                showTypeAsc: !this.state.showTypeAsc
            },()=>{
                this.filterBy(this.state.lpNameFilterId, 'docType', this.state.documentTypeFilter,sortType) 
            })
        }
         
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    hamburgerClick() {
        if (this.state.showSideNav == true) {
            this.setState({
                showSideNav: false
            })
        } else {
            this.setState({
                showSideNav: true
            })
        }
    }

    checkTypeForallLps(obj) {
        if(obj && obj.length >0) {
            for(let index of obj) {
                if(index.fundInvestor.documentsForSignature.length >0 || index.fundInvestor.commitmentHistory) {
                    this.setState({
                        showDocumentLocker:true
                    })
                    return true
                }
            }
        }
        this.setState({
            showDocumentLocker:false,
            documentLockerMsz: this.state.documentTypeFilter == '' && this.state.lpNameFilterId == '' ? this.Constants.NO_DOCUMENTLOCKER_DOCUMENTS:this.Constants.NO_DOCUMENTLOCKER_FILTER_DOCUMENTS
        })
    }

    openDoc = (data) => () => {
        const docUrl = `${data.documentUrl}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    getDocumentName = (data) => {
        let name = '';
        if(data) {
            let type = data.documentType;
            name = (['FUND_AGREEMENT','CONSOLIDATED_FUND_AGREEMENT','CONSOLIDATED_AMENDMENT_AGREEMENT','AMENDMENT_AGREEMENT'].indexOf(type) > -1 && data.name) ? data.name : type === 'SUBSCRIPTION_AGREEMENT' ? 'Subscription Agreement': type === 'CHANGE_COMMITMENT_AGREEMENT' ? 'Capital Commitment Increase Letter' : type === 'SIDE_LETTER_AGREEMENT' ? 'Side Letter' : type;
        }
        return name;
    }

    selectDocuments(e, isAll, obj) {
        let selectedDocs = this.state.selectedDocs;
        let idx = selectedDocs.filter(x => x.id === obj.id );
        let findIndex = selectedDocs.findIndex(x => x.id === obj.id );
        if(isAll) {
            this.setState({ 
                includeAllFile: e.target.checked 
            });
        } else {
            if(e.target.checked && !isAll) {
                if(idx.length === 0) {
                    selectedDocs.push(obj);
                } else {
                    selectedDocs.splice(findIndex, 1);
                }
            } else {
                selectedDocs.splice(findIndex, 1);
            }
        }
        this.setState({
            selectedDocs
        });
    }

    downloadDocuments = () => {
        this.open();
        let postObj = {
            fileIds: this.state.selectedDocs.map((k) => {return {id:k.id,name:k.name}}),
            includeAllFile: this.state.includeAllFile,
            subscriptionId: null,
            fundId: +this.state.fundId
        }
        
        this.Fsnethttp.downloadFiles(postObj).then(res => {
            this.close();
            if(res) {
                if (res.data && res.data.url) {
                    const docUrl = `${res.data.url}`
                    this.FsnetUtil._openNewTab(docUrl)
                }
            }
        }).catch(error => {
            this.close();
        })
    }

    render() {
        return (
            <div className="lpSubFormStyle addendumContainer">
                <div className="main-heading"><span className="main-title">Document Locker</span></div>
                <div className="LpDelegatesContainer marginTop20 marginLeft25 paddingLeft15">
                    <Row className="documnet-locker-options marginTop20">
                        <Col xs={12} md={4} lg={4} sm={8}>
                            <FormControl name='filterLpName' defaultValue={0} className="selectFormControl" componentClass="select" onChange={(e) => { this.handleOnChange(e, 'filterLpName')}}>
                                <option value={0}>Filter by: Investor Name</option>
                                {this.state.getLpList.map((record, index) => {
                                    return (
                                        <option value={record['id']} key={index} hidden={!record['selected']}>{this.FsnetUtil.getFullName(record)}</option>
                                    );
                                })}
                            </FormControl>
                        </Col>
                        <Col xs={12} md={4} lg={4} sm={8}>
                            <FormControl name='filterLpName' defaultValue={0} className="selectFormControl" componentClass="select" onChange={(e) => { this.handleOnChange(e, 'filterDocumentType')}}>
                                <option value={0}>Filter by: Document Type</option>
                                {this.state.getDocumentTypes.map((record, index) => {
                                    return (
                                        <option value={record['type']} key={index} >{record['name']}&nbsp;&nbsp;&nbsp;</option>
                                    );
                                })}
                            </FormControl>
                        </Col>
                        <Col xs={12} md={4} lg={4} sm={8}>
                            <Button disabled={!this.state.selectedDocs.length && !this.state.includeAllFile} className="newAddendumButton download-btn" onClick={this.downloadDocuments}><i className="fa fa-download paddingRight5"></i>Download Selected Docs</Button>
                        </Col>
                    </Row>
                    <Row className="full-width marginTop20 marginLeft61" hidden={this.state.gpDocumentLockerList.length === 0}>
                        <div className="name-heading marginLeft75" hidden={!this.state.showNameAsc} onClick={(e) => this.handleOnChange(e, 'firstName', 'asc')}>
                            Investor Name
                                    <i className="fa fa-sort-asc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading marginLeft75" onClick={(e) => this.handleOnChange(e, 'firstName', 'desc')} hidden={this.state.showNameAsc}>
                            Investor Name
                                <i className="fa fa-sort-desc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading date-name-heading">
                            Date
                        </div>
                        <div className="name-heading paddingLeft30" hidden={!this.state.showTypeAsc} onClick={(e) => this.handleOnChange(e, 'docType', 'asc')}>
                            Type<i className="fa fa-sort-asc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading paddingLeft30 width-210" hidden={this.state.showTypeAsc} onClick={(e) => this.handleOnChange(e, 'docType', 'desc')}>
                            Type<i className="fa fa-sort-desc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading">
                            Download
                        </div>
                    </Row>
                    <div className={"userAddendumContainer " + (this.state.gpDocumentLockerList.length === 0 ? 'borderNone' : 'marginTop10')}>
                            {this.state.gpDocumentLockerList.length >0 ?
                                this.state.gpDocumentLockerList.map((record, index) => {
                                    return (
                                        
                                        <div className="userRow" key={index}>
                                        <label className="userImageAlt">
                                        {
                                            record['profilePic'] ?
                                                <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)}/>
                                                :
                                                <img src={userDefaultImage} alt="img" className="user-image"/>
                                        }
                                        </label>
                                        {
                                            ['CONSOLIDATED_FUND_AGREEMENT','CONSOLIDATED_AMENDMENT_AGREEMENT'].indexOf(record.documentType) > -1 ?
                                                <div className="lp-name">All</div>
                                            :
                                                <div className="lp-name">{this.FsnetUtil.getFullName(record)}</div>
                                        }
                                        {/*<div className="lp-name lp-name-pad date-lp-name-pad">{this.FsnetUtil.dateFormat(record['createdAt'])}</div>*/}
                                        <div className="lp-name lp-name-pad date-lp-name-pad">{record['createdAt']}
                                        {
                                            record['timezone'] ?
                                                <span> ({record['timezone']})</span>
                                                : ''
                                        }
                                        </div>
                                        <div className="lp-name lp-name-pad type-lp-name-pad cursor"  onClick={this.openDoc(record)}>{this.getDocumentName(record)} </div>
                                        <div style={{'display': 'inline-block', 'padding': '0 35px', width: '65px !important'}}>
                                            <CBox checked={record['selected']} onChange={(e) => this.selectDocuments(e, false, record)} style={{ 'marginRight': '0px' }}>
                                                <span className="checkmark"></span>
                                            </CBox>
                                        </div>
                                    </div>
                                            
                                    );
                                })
                                :
                                <div className="title margin20 paddingLeft20">{this.state.documentLockerMsz}</div>
                            }
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default documentLockerComponent;


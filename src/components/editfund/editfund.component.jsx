import React, { Component } from 'react';
import './editfund.component.css';
import Loader from '../../widgets/loader/loader.component';
import { Constants } from '../../constants/constants';
import { Row, Col } from 'react-bootstrap';
import { Fsnethttp } from '../../services/fsnethttp';
import { FsnetAuth } from '../../services/fsnetauth';
import HeaderComponent from '../header/header.component';
import homeImage from '../../images/home.png';
import ModalComponent from '../fundSetup/modals/modals.component';
import fundImage from '../../images/picture.png';
import ToastComponent from '../toast/toast.component';
import handShakeImage from '../../images/handshake_2.svg';
import currencyImage from '../../images/currency.svg';
import documentImage from '../../images/document.svg';
import infoImage from '../../images/info.svg';
import transferImage from '../../images/transfer.png';
import letterImage from '../../images/letter.svg';
import { Route, Link, Switch, Redirect } from "react-router-dom";
import userDefaultImage from '../../images/default_user.png';
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../util/util';
import addendumsComponent from './addendums/addendums.component';
import documentLockerComponent from './documentLocker/documentLocker.component';
import fundAgreementChangeComponent from './fundAgreementChange/fundAgreementChange.component';
import sideLettersComponent from './sideLetter/sideLetters.component';
import AmendmentComponent from './amendments/amendment.component';
import dashboardComponent from './dashboard/dashboard.component';
import expandLpTableComponent from './expandLpTable/expandLpTable.component';
import { reactLocalStorage } from 'reactjs-localstorage';
import vanillaLogo from '../../images/Vanilla-white.png';
import vanillaDarkLogo from '../../images/Vanilla.png';

var fundInfo, goToDashboard, toastNotification, closeToast = {}, subScriptDocWin;
class editFundComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();


        this.handleShow = this.handleShow.bind(this);
        this.hamburgerClick = this.hamburgerClick.bind(this)
        this.handleGpShow = this.handleGpShow.bind(this);
        this.openDocument = this.openDocument.bind(this);
        this.getFundDetails = this.getFundDetails.bind(this);
        this.navigateToFund = this.navigateToFund.bind(this);
        this.navigateToTracker = this.navigateToTracker.bind(this);
        this.state = {
            fundObj: {},
            isConsentForClose: false,
            showModal: false,
            isMounted: false,
            isExpanded: false,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            showSideNav: true,
            gpDelegatesSelectedUsers: false,
            fundImage: fundImage,
            alertLpData: {},
            userType: null
        }
        fundInfo = PubSub.subscribe('fundData', (msg, data) => {
            //console.log('hkdjalsdkjasdjasd', data);
            this.setState({
                fundId: data.id,
                fundIdMask: this.FsnetUtil._decrypt(data.id),
            }, () => {
                this.updateObjandNavLinks(data);
            })
        });

        goToDashboard = PubSub.subscribe('goToDashboard', () => {
            this.props.history.push('/dashboard');
        })

        toastNotification = PubSub.subscribe('toastNotification', (msg, data) => {
            this.setState({
                showToast: data.showToast,
                toastMessage: data.toastMessage,
                toastType: data.toastType
            })
        })

        closeToast = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
    }

    closeToast(timed) {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    componentDidMount() {
        if (this.FsnetAuth.isAuthenticated()) {
            this.setState({
                isMounted: true
            })
            //Get user obj from local storage.
            let userObj = reactLocalStorage.getObject('userData');
            let firmId = reactLocalStorage.getObject('firmId');
            var url = window.location.href;
            var parts = url.split("/");
            //console.log('parts:::', parts);
            var urlString = parts[parts.length - 2];
            var urlSplitFundId = parts[parts.length - 1];
            if (parts.indexOf('expandTable') > -1) {
                this.setState({
                    isExpanded: true
                })
            } else {
                this.setState({
                    isExpanded: false
                })
            }
            if (userObj) {
                this.setState({
                    loggedInUserObj: userObj,
                    currentPage: urlSplitFundId,
                    firmId: firmId,
                    userType: this.FsnetUtil.getUserRole()
                })
            }
            let fundId = this.FsnetUtil._getId();
            if (fundId !== 'funddetails' && fundId !== 'fundsetup') {
                this.setState({ fundId: fundId, fundIdMask: this.FsnetUtil._encrypt(fundId) }, () => {
                    this.getFundDetails();
                    if (userObj && userObj.accountType == 'GPDelegate') {
                        this.checkConsentForClose();
                    }
                });
            }
            window.scrollTo(0, 0);
        } else {
            window.location = '/';
        }
    }

    checkConsentForClose() {

        let fundId = this.state.fundId
        if (fundId) {
            this.open();
            this.Fsnethttp.checkConsent(fundId).then(result => {
                this.close();
                if (result.data) {
                    console.log(result.data);
                    this.setState({
                        isConsentForClose: result.data.gPDelegateRequiredConsentHoldClosing
                        // isConsentForClose: false
                    })
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    getFundDetails() {
        //console.log('calling fund data');

        let fundId = this.state.fundId
        if (fundId) {
            this.open();
            this.Fsnethttp.getFund(fundId).then(result => {
                this.close();
                if (result.data) {
                    let userData = JSON.parse(reactLocalStorage.get('userData'));
                    userData['vcfirmId'] = result.data.data.vcfirmId;
                    reactLocalStorage.set('userData', JSON.stringify(userData));
                    reactLocalStorage.set('firmId', result.data.data.vcfirmId);
                    this.updateObjandNavLinks(result.data.data)
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    updateObjandNavLinks(data) {
        const sortArray = data.lps.fundLps.length > 0 ? this.FsnetUtil._sortArrayWithDate(data.lps.fundLps) : []
        data.lps.fundLps = sortArray;
        this.setState({
            fundName: data.legalEntity,
            fundStatus: data.status,
            fundObj: data,
            documentLink: data.partnershipDocument ? data.partnershipDocument.url : null,
            getGpDelegatesList: data.gpDelegates ? data.gpDelegates : [],
            getLpList: data.lps ? data.lps.fundLps : [],
            gpDelegatesSelectedUsers: false,
            lpSelectedUsers: data.lps && data.lps.fundLps.length > 0 ? true : false,
            fundImage: data.fundImage ? data.fundImage.url : fundImage,
        }, () => {
            this.gpSelectedFromFund();
            // this.lpSelectedFromFund();
        })
    }

    //Check GP list selected true 
    gpSelectedFromFund() {
        for (let index of this.state.getGpDelegatesList) {
            if (index['selected'] === true) {
                this.setState({
                    gpDelegatesSelectedUsers: true
                })
            }
        }
    }

    //Check lP list selected true 
    lpSelectedFromFund() {
        for (let index of this.state.getLpList) {
            if (index['selected'] === true) {
                this.setState({
                    lpSelectedUsers: true
                })
            }
        }
    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(fundInfo);
        PubSub.unsubscribe(goToDashboard);
        PubSub.unsubscribe(toastNotification);
        PubSub.unsubscribe(closeToast);
    }




    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    handleShow() {
        // this.props.history.push(`/createfund/investors/${this.state.fundId}`);
        window.location.href = `/fundSetup/investors/${this.FsnetUtil._encrypt(this.state.fundId)}`;
        // PubSub.publish('openLpModal', this.state.fundObj );
    }

    handleGpShow() {
        // this.props.history.push(`/createfund/gpDelegate/${this.state.fundId}`);
        window.location.href = `/fundSetup/gpDelegate/${this.FsnetUtil._encrypt(this.state.fundId)}`;
        // PubSub.publish('openGpModal', this.state.fundObj );
    }

    hamburgerClick() {
        if (this.state.showSideNav == true) {
            this.setState({
                showSideNav: false
            })
        } else {
            this.setState({
                showSideNav: true
            })
        }
    }

    hamburgerClick() {
        if (this.state.showSideNav == true) {
            this.setState({
                showSideNav: false
            })
        } else {
            this.setState({
                showSideNav: true
            })
        }
    }

    closeFund() {
        if ((this.FsnetUtil.getUserRole() == 'SecondaryGP' && this.state.fundObj.noOfSignaturesRequiredForClosing > 1) || (this.FsnetUtil.getUserRole() == 'GP')) {
            PubSub.publish('openCloseFundModal', { fundId: this.state.fundId })
        }
    }

    approveLps() {
        PubSub.publish('approveLpsModal', { fundId: this.state.fundId })
    }

    confirmDeactivate() {
        //console.log('clicked modal');
        PubSub.publish('openfundDelModal', { fundId: this.state.fundId, fundName: this.state.fundName, fundStatus: this.state.fundStatus })
    }

    deleteLp(e, id) {
        PubSub.publish('openLpDelModal', { data: this.state.fundObj, delegateId: id });
    }

    deleteGp(e, id) {
        PubSub.publish('openGpDelModal', { data: this.state.fundObj, delegateId: id });
    }

    openDocument() {
        if (this.state.fundObj.consolidateFundAgreementURL) {
            const docUrl = `${this.state.fundObj.consolidateFundAgreementURL}`
            this.FsnetUtil._openDoc(docUrl)
        }
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('profileModal', data);
    }

    navigateToFund() {
        window.location.href = '/fundSetup/funddetails/' + this.state.fundIdMask;
    }

    navigateToTracker() {
        window.location.href = '/fund/view/' + this.state.fundIdMask;
    }

    downloadTracker = () => {
        if (this.state.fundId && this.state.fundObj.lps.fundLps.length > 0) {
            this.open();
            this.Fsnethttp.downloadTracker(this.state.fundId).then(res => {
                this.close();
                if (res.data && res.data.url) {
                    const docUrl = `${res.data.url}`
                    this.FsnetUtil._openNewTab(docUrl)
                }
            }).catch(error => {
                this.close();
            })
        }
    }


    render() {
        const { match } = this.props;
        return (
            <div className="lpSubFormStyle editFundContainer" id="">
                <nav className="navbar navbar-custom" hidden={this.state.isExpanded}>
                    <div className="navbar-header">
                        <div className="sidenav">
                            <h1 className="text-left"><i className="fa fa-bars" aria-hidden="true" onClick={(e) => this.hamburgerClick()}></i>&nbsp; <img src={vanillaLogo} alt="vanilla" className="vanilla-logo" /></h1>
                        </div>
                    </div>
                    <div className="text-center navbar-collapse-custom" id="navbar-collapse" hidden={!this.state.showSideNav}>
                        <div className="sidenav">
                            <h1 className="text-left logoHamburger"><i className="fa fa-bars" aria-hidden="true"></i><img src={vanillaLogo} alt="vanilla" className="vanilla-logo" /></h1>
                            <h2 className="text-left lpDashAlign alignMargin"><img src={homeImage} alt="home_image" className="paddingRight5" />&nbsp;
                            <Link to={this.state.userType === 'FSNETAdministrator' ? "/adminDashboard/firmView" : "/dashboard"} className="dashboardTxtAlign">Dashboard</Link></h2>
                            <h2 className="text-left marginTopNone marginBottom8 marginLeft15" hidden={this.state.fundId === null}><img src={this.state.fundImage} alt="home_image" className="" />&nbsp; <a className="cursor fontSizeOpacity" onClick={this.navigateToTracker}>Fund Tracker</a></h2>
                            {
                                <ul className="sidenav-menu">
                                    <li>
                                        <a onClick={this.navigateToFund} className="cursor"><img src={infoImage} alt="home_image" className="" />&nbsp;<span>Fund Details</span></a>
                                    </li>
                                    <li>
                                        <a className="cursor" onClick={this.downloadTracker}><i className="fa fa-cloud-download blue paddingRight5" aria-hidden="true"></i>&nbsp;<span>Fund Summary (Excel)</span></a>
                                    </li>
                                    <li>
                                        {
                                             

                                            ['Open','New-Draft','Admin-Draft'].indexOf(this.state.fundStatus) > -1 ?
                                                <a title="Fund not yet started"><img src={documentImage} alt="home_image" className="" />&nbsp;<span className="comingSoon">Fund Agreement Change</span></a>
                                            :
                                                <Link to={"/fund/fundAgreementChange/" + this.state.fundIdMask}><img src={documentImage} alt="home_image" className="" />&nbsp;<span>Fund Agreement Change</span></Link>
                                        }
                                    </li>
                                    <li>
                                        <a className="cursor" onClick={this.state.fundObj.consolidateFundAgreementURL ? this.openDocument : null}><img src={handShakeImage} alt="home_image" className="" />&nbsp;<span>Fund Agreements</span></a>
                                    </li>
                                    <li>
                                        <Link className="cursor" to={"/fund/sideLetters/" + this.state.fundIdMask}><img src={letterImage} alt="home_image" className="" />&nbsp;<span>Side Letters</span></Link>
                                    </li>
                                    <li>
                                        <Link className="cursor" to={"/fund/amendments/" + this.state.fundIdMask}><img src={letterImage} alt="home_image" className="" />&nbsp;<span>Amendments</span></Link>
                                    </li>
                                    <li>
                                        <a title="Coming Soon"><img src={currencyImage} alt="home_image" className="" />&nbsp;<span className="comingSoon">Capital Call</span></a>
                                    </li>
                                    <li>
                                        <a title="Coming Soon"><img src={transferImage} alt="home_image" className="" />&nbsp;<span className="comingSoon">Transfers</span></a>
                                    </li>
                                    {
                                        this.FsnetUtil.getUserRole() != 'GPDelegate' ?
                                            <li>
                                                <a className={this.FsnetUtil.getUserRole() == 'SecondaryGP' && this.state.fundObj.noOfSignaturesRequiredForClosing <= 1 ? 'disabled' : 'cursor'} onClick={() => { this.closeFund() }}><img src={null} alt={null} className="" style={{ visibility: 'hidden' }} />&nbsp;<span className={this.FsnetUtil.getUserRole() == 'SecondaryGP' && this.state.fundObj.noOfSignaturesRequiredForClosing <= 1 ? 'comingSoon' : ''}>
                                                    {this.FsnetUtil.getUserRole() == "GP" ? 'Close Fund' : 'Approve for Closing'}</span></a>
                                            </li>
                                            :
                                            this.state.isConsentForClose &&
                                            <li className="long-text">
                                                <a className="cursor" onClick={() => { this.approveLps() }}>&nbsp;<span>Approve Investors for Closing</span></a>
                                            </li>
                                    }
                                    {/* <li>
                                        <a className="cursor" onClick={() => { this.confirmDeactivate()}}><img src={null} alt={null} className="" style={{ visibility: 'hidden' }}/>&nbsp;<span>Deactivate Fund</span></a>
                                    </li> */}
                                </ul>
                            }
                            <div className="section-head text-left"><Link to={"/fund/documentLocker/" + this.state.fundIdMask}><span className="lpAlign dLSectionHeadTxt">Document Locker</span></Link></div>
                            <div className="section-head text-left"><span className="sectionHeadTxt lpAlign">Delegates</span>
                                {
                                    (['GP', 'FSNETAdministrator'].indexOf(this.FsnetUtil.getUserRole()) > -1) &&
                                    <span className={"btn-add pull-right edit-btn-text " + (this.state.fundId === null ? 'disabledAddIcon' : '')} onClick={this.handleGpShow}>Edit</span>
                                }
                            </div>
                            <div className="section">
                                <div className="gpDelDiv">
                                    {this.state.gpDelegatesSelectedUsers === true ?
                                        this.state.getGpDelegatesList.map((record, index) => {
                                            return (
                                                <div className="gpDelegateInfo" key={index} hidden={record['selected'] === false}>
                                                    <div className="dpDelImg">
                                                        {
                                                            record['profilePic'] ?
                                                                <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                                : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                        }
                                                    </div>
                                                    <div className="dpDelName" title={this.FsnetUtil.getFullName(record)}>{this.FsnetUtil.getFullName(record)}</div>
                                                    {/* {
                                                        reactLocalStorage.get('userData') && JSON.parse(reactLocalStorage.get('userData'))['accountType'] === 'GP'?
                                                        <div className="dpDelgDel"><i className="fa fa-minus" onClick={(e) => this.deleteGp(e, record['id'])}></i></div>:''
                                                    } */}
                                                </div>
                                            );
                                        })
                                        :
                                        <div className="user">
                                            <i className="fa fa-user fa-2x" aria-hidden="true"></i>
                                            <p className="opacity75">You haven’t added any Delegates to this Fund yet</p>
                                        </div>
                                    }
                                </div>
                            </div>
                            <div className="section-head text-left"><span className="sectionHeadTxt lpAlign">Investors</span><span className={"btn-add pull-right edit-btn-text " + (this.state.fundId === null ? 'disabledAddIcon' : '')} onClick={this.handleShow}>Edit</span></div>
                            <div className="section">
                                <div className="gpDelDiv">
                                    {this.state.lpSelectedUsers === true ?
                                        this.state.getLpList.map((record, index) => {
                                            return (
                                                <div className="gpDelegateInfo" key={index} hidden={record['selected'] === false}>
                                                    <div className="dpDelImg">
                                                        {
                                                            record['profilePic'] ?
                                                                <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                                : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                        }
                                                    </div>
                                                    <div className="dpDelName">{this.FsnetUtil.getFullName(record)}</div>
                                                    {/* <div className="dpDelgDel"><i className="fa fa-minus" onClick={(e) => this.deleteLp(e, record['id'])}></i></div> */}
                                                </div>
                                            );
                                        })
                                        :
                                        <div className="user">
                                            <i className="fa fa-user fa-2x" aria-hidden="true"></i>
                                            <p className="opacity75">You haven’t added any Investors to this Fund yet</p>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>

                    </div>
                </nav>

                <div className={"main " + (this.state.isExpanded ? 'marginHorizontal30 expanded' : '')}>
                    <div className={"headerAlign " + (this.state.isExpanded ? 'marginTop20' : '')}>
                        <div className="pull-left" hidden={!this.state.isExpanded}>
                            <img src={vanillaDarkLogo} alt="vanilla" className="vanilla-logo marginLeft30" />
                        </div>
                        <HeaderComponent></HeaderComponent>
                    </div>
                    <div className="contentWidth">
                        <Row className="main-content">
                            <Switch>
                                <Route exact path={`${match.url}/view/:id`} component={dashboardComponent} />
                                <Route exact path={`${match.url}/expandTable/:id`} component={expandLpTableComponent} />
                                <Route exact path={`${match.url}/addendums/:id`} component={addendumsComponent} />
                                <Route exact path={`${match.url}/documentLocker/:id`} component={documentLockerComponent} />
                                <Route exact path={`${match.url}/fundAgreementChange/:id`} component={fundAgreementChangeComponent} />
                                <Route exact path={`${match.url}/sideLetters/:id`} component={sideLettersComponent} />
                                <Route exact path={`${match.url}/amendments/:id`} component={AmendmentComponent} />
                                <Redirect from={`${match.url}/*`} to='/404' />
                                <Redirect from={`${match.url}/`} to='/404' />
                            </Switch>
                        </Row>
                    </div>
                </div>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <ModalComponent hidden={!this.state.isMounted}></ModalComponent>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default editFundComponent;


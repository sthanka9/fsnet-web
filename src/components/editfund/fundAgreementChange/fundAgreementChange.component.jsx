import React, { Component } from 'react';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Row, Col, Button, Radio, Checkbox as CBox, Modal } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';
import InvestorConsentComponent from './without-investor-consent/investorConsent.component';
import NonClosedComponent from './non-closed/nonClosed.component';
import ClosedInvestorsComponent from './closed/closedInvestors.component';
import AmmendmentTrackerComponent from './tracker/ammendmentTracker.component';

class fundAgreementChangeComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();

        this.state = {
            addendumList:[],
            fundStatus:{},
            selectedButtonType:0 
        }

    }

    componentDidMount() {
        if (this.FsnetAuth.isAuthenticated()) {
            let id = this.FsnetUtil._getId();
            this.setState({
                fundId: id
            })
            this.getFundAgreementStatus(id);
        } else {
            window.location = '/';
        }

    }

    //Get Fund Status
    getFundAgreementStatus(id) {
        if (id) {
            this.open();
            this.Fsnethttp.checkFundStatus(id).then(result => {
                this.close();
                this.setState({
                    fundStatus: result.data
                })
            })
            .catch(error => {
                this.close();
            });
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    changeToDefault = (val) => {
        this.setState({selectedButtonType:0},()=>{
            if(val && val.type) {
                window.location.reload()
            }
        })
    }

    render() {
        return (
            <div className="lpSubFormStyle addendumContainer fundAgreementChange">
                <div className="main-heading"><span className="main-title">Fund Agreement Change</span></div>
                <h1 className="title-md padding10 marginLeft15">You may use this functionality to modify the fund agreement either before or after closing.  You have the option of using either a stand-alone amendment, or a fully amended and restated fund agreement (and in the latter case, of electing to use a “changed pages” file to easily illustrate the modifications to investors).  You may also modify the fund agreement without seeking investor consent, for example if on the terms of the fund agreement you are permitted to make clerical or other changes without investor consent.  Note, if you are modifying the fund agreement after the first closing and while fund raising is still in process, you may need to run multiple processes simultaneously.  Please contact your legal counsel to discuss.</h1>
                {/* Three buttons Initial Screen */}
                <div className="margin20 width97">
                    <Row className="marginLeftNone">
                        <Button className={"fundAggrementChangeBtn marginRight10 marginTop10 " + (this.state.fundStatus.atleastOnefundSubscriptionClosing ? 'btnEnabled' : 'disabled')} disabled={!this.state.fundStatus.atleastOnefundSubscriptionClosing} onClick={()=>this.setState({selectedButtonType:1})}>Change as to closed investors</Button>
                        <Button className={"fundAggrementChangeBtn marginRight10 marginTop10 " + (!this.state.fundStatus.fundFinalClosing ? 'btnEnabled' : 'disabled')} disabled={this.state.fundStatus.fundFinalClosing} onClick={()=>this.setState({selectedButtonType:2})}>Change as to non-closed investors</Button>
                        <Button className={"fundAggrementChangeBtn btnEnabled marginTop10 " + (!this.state.fundStatus.isFundLocked ? 'btnEnabled' : 'disabled')} disabled={this.state.fundStatus.fundFinalClosing} disabled={this.state.fundStatus.isFundLocked} onClick={()=>this.setState({selectedButtonType:3})}>Change without investor consent</Button>
                    </Row>
                </div>
                <AmmendmentTrackerComponent></AmmendmentTrackerComponent>
                {
                    this.state.selectedButtonType === 3 &&
                    <InvestorConsentComponent onCancel={this.changeToDefault} fundId={this.state.fundId}></InvestorConsentComponent>
                }
                {
                    this.state.selectedButtonType === 2 &&
                    <NonClosedComponent onCancel={this.changeToDefault} fundId={this.state.fundId} fundStatus={this.state.fundStatus}></NonClosedComponent>
                }
                {
                    this.state.selectedButtonType === 1 &&
                    <ClosedInvestorsComponent onCancel={this.changeToDefault} fundId={this.state.fundId} fundStatus={this.state.fundStatus}></ClosedInvestorsComponent>
                }

                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Loader isShow={this.state.showModal}></Loader>

            </div>


        );
    }
}

export default fundAgreementChangeComponent;


import React, { Component } from 'react';
import { Row, Col, Button, Modal, Radio } from 'react-bootstrap';
import Loader from '../../../../widgets/loader/loader.component';
import { Constants } from '../../../../constants/constants';
import { Fsnethttp } from '../../../../services/fsnethttp';
import { FsnetAuth } from '../../../../services/fsnetauth';
import { FsnetUtil } from '../../../../util/util';
import  UploadFileComponent from '../upload/uploadFile.component'
import  ChangedPagesComponent from '../changedPagesUpload/changedPagesUpload.component'
import ToastComponent from '../../../toast/toast.component';
import { PubSub } from 'pubsub-js';

var close={}
class NonClosedComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showOneClosedInvestor:false,
            showSecondModal:false,
            fundId:null,
            fundStatus:{},
            changedPagesRadioValue:null,
            isFormValid:false,
            cpDocFile:null,
            FADocFile:null,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
        }
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast = (timed) => {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }


    componentDidMount() {
        this.setState({ fundId: this.props.fundId,fundStatus:this.props.fundStatus },()=>{
            if(this.state.fundStatus.atleastOnefundSubscriptionClosing) {
                this.openOneClosedInvestorModal()
            } else {
                this.openSecondModal();
            }
        });
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    openSecondModal = () => {
        this.setState({showSecondModal:true,showOneClosedInvestor:false})
    }

    openOneClosedInvestorModal = () => {
        this.setState({showOneClosedInvestor:true})
    }

    closeOneClosedInvestorModal = () => {
        this.setState({showOneClosedInvestor:false},()=>{
            this.props.onCancel(false);  
        })
    }

    closeSecondModal = () => {
        this.setState({showSecondModal:false},()=>{
            this.props.onCancel(false);  
        })
        
    }

    closeUploadModal = (val) => () => {
        this.setState({showUploadModal:false},()=>{
            this.props.onCancel(val?{type:true}:false);  
        })
    }

    openUploadModal = () => {
        this.setState({showUploadModal:true,showSecondModal:false})
    }

    updateFileName = (value) => {
        this.setState({
            cpDocFile: value && value.type === 'CP' ? value.docFile : this.state.cpDocFile ? this.state.cpDocFile : null,
            FADocFile: value && value.type === 'FA' ? value.docFile : this.state.FADocFile ? this.state.FADocFile : null
        },()=>{
            this.enableOrDisableButton()
        })
    }

    enableOrDisableButton = () => {
        if((this.state.changedPagesRadioValue && this.state.cpDocFile && this.state.FADocFile) || (!this.state.changedPagesRadioValue && this.state.FADocFile)) {
            this.setState({
                isFormValid:true
            })
        } else {
            this.setState({
                isFormValid:false
            })
        }
    }

    handleOnRadioChange = (val) => () => {
        this.setState({
            changedPagesRadioValue:val,
            cpDocFile:!val ? null : this.state.cpDocFile
        },()=>{
            this.enableOrDisableButton()
        })
    }

    uploadNonClosedDoc = () => {
        this.open();
        var formData = new FormData();
        formData.append("fundId", this.state.fundId);
        formData.append("fundagreement", this.state.FADocFile);
        if(this.state.changedPagesRadioValue) {
            formData.append("changedpages", this.state.cpDocFile);
        }
        this.Fsnethttp.uploadFANonClosedInvestors(formData,this.state.fundId).then(result => {
            this.close();
            this.setState({
                showUploadModal:false,
                showToast: true,
                toastMessage: this.Constants.DOC_UPLOAD_MSZ,
                toastType: 'success',
            },()=>{
                setTimeout(() => {
                    this.closeUploadModal(true)();
                }, 2000);
            })
        })
        .catch(error => {
            this.close();
        });
    }

    render() {
        return (
            <div>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.showOneClosedInvestor}  onHide={this.closeOneClosedInvestorModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title-md">Please be mindful that this functionality only solicits consent to the proposed changes from non-closed Investors.  If you wish to formally amend the Fund Agreement with closed Investor, please select “Amend Fund Agreement as to Closed Investors” to do so.  In the time between the first and final closing, as now, you may need to run both processes and there are important strategic and technical decisions that arise in determining the ordering of doing so.  Please consult legal counsel to ensure you are taking the appropriate steps.</h1>
                        <Row className="fundBtnRow">
                            <Col lg={5} md={5} sm={5} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeOneClosedInvestorModal}>Cancel</Button>
                            </Col>
                            <Col lg={7} md={7} sm={7} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.openSecondModal}>I Agree – Please Proceed</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.showSecondModal}  onHide={this.closeSecondModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title-md">This functionality permits you to propose to change the Fund Agreement as to non-closed Investors. Upon initiating this process, all Investors in close-ready status are moved out of close-ready status.  They, and any other Investors which had previously electronically signed the Fund Agreement but had not yet taken all the steps to become close-ready, will receive a message indicating that the Fund Agreement they signed has been modified, and asking them to consent by reauthorizing their electronic signatures.  Investors who have not yet electronically signed the Fund Agreement will receive a notification to the extent that the Fund Agreement has been modified but will not need to take further action, given that they did not sign the Fund Agreement in its prior form.</h1>
                        <Row className="fundBtnRow">
                            <Col lg={5} md={5} sm={5} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeSecondModal}>Cancel</Button>
                            </Col>
                            <Col lg={7} md={7} sm={7} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.openUploadModal}>I Agree – Please Proceed</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.showUploadModal}  onHide={this.closeUploadModal(false)} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title-md">You are required to upload the revised Fund Agreement.  Would you like to additionally upload a “changed pages” file?  Note, a “changed pages” file is a shorter summary document showing solely the relevant modified pages of the longer agreement.  If you select this option, Investors will review and approve changes based solely on such shorter file.  The shorter “changed pages” file will be used solely as a convenience to gain Investor consent to the revised Fund Agreement, and will not be stored permanently in the Fund’s document locker.  If you have questions, please discuss this option with your legal counsel.</h1>
                        <Row className="marginTop10">
                            <Col lg={6} md={6} sm={6} xs={6}>
                                <Radio name="changedPageDoc" inline onChange={this.handleOnRadioChange(true)}>
                                    <span className="radio-checkmark"></span>
                                </Radio>
                                <span className="radio-span">Yes, use a "changed pages" file</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={6}>
                                <Radio name="changedPageDoc" inline onChange={this.handleOnRadioChange(false)}>
                                    <span className="radio-checkmark"></span>
                                </Radio>
                                <span className="radio-span">No, do not use a "changed pages" file</span>
                            </Col>
                        </Row>
                        {
                            this.state.changedPagesRadioValue &&
                            <div className="marginTop20 fundAgreementChange marginBot20">
                                <h1 className="title">Upload Changed Pages File</h1>
                                {/* <h1 className="subtext">Choose document files to upload. Accepted files information appears here.</h1> */}
                                <ChangedPagesComponent cPFileName={this.updateFileName}></ChangedPagesComponent>
                            </div>
                        }
                        {
                            this.state.changedPagesRadioValue !== null &&
                            <div className="marginTop20 fundAgreementChange marginBot20">
                                <h1 className="title">Upload Revised Fund Agreement File</h1>
                                {/* <h1 className="subtext">Choose document files to upload. Accepted files information appears here.</h1> */}
                                <UploadFileComponent fileName={this.updateFileName}></UploadFileComponent>
                            </div>
                        }
                        <Row className="fundBtnRow">
                            <Col lg={7} md={7} sm={7} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeUploadModal(false)}>Cancel</Button>
                            </Col>
                            <Col lg={5} md={5} sm={5} xs={12}>
                                <Button type="button" className={"fsnetSubmitButton " + (this.state.isFormValid ? 'btnEnabled':'disabled')} onClick={this.uploadNonClosedDoc}>Submit</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
               
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
            </div>


        );
    }
}

export default NonClosedComponent;


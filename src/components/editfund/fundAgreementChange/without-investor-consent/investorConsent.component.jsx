import React, { Component } from 'react';
import { Row, Col, Button, Modal, Radio } from 'react-bootstrap';
import Loader from '../../../../widgets/loader/loader.component';
import { Constants } from '../../../../constants/constants';
import { Fsnethttp } from '../../../../services/fsnethttp';
import { FsnetAuth } from '../../../../services/fsnetauth';
import { FsnetUtil } from '../../../../util/util';
import  UploadFileComponent from '../upload/uploadFile.component'
import ToastComponent from '../../../toast/toast.component';
import { PubSub } from 'pubsub-js';

var close={}
class InvestorConsentComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showInvestorConsent:true,
            showConsentUpload:false,
            fundId:null,
            selectedDocId:null,
            existingFAList:{fundAgreementsData:{path:''}},
            amendmentsList:[],
            docType:null,
            fileName:null,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
        }
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })


    }

    componentDidMount() {
        this.setState({ fundId: this.props.fundId });
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    closeToast = (timed) => {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    closeInvestorConsentModal = (val) => () => {
        this.props.onCancel(val?{type:true}:false);  
    }

    closeConsentUploadModal = (val) => () => {
        this.closeInvestorConsentModal(val)();
    }

    getListOfDocs = () => {
        this.open();
        this.Fsnethttp.existingFAandAmmendments(this.state.fundId).then(result => {
            this.close();
            this.setState({
                existingFAList: result.data.fundAgreements[0],
                amendmentsList: result.data.ammendments,
                showConsentUpload:true,
                showInvestorConsent:false
            })
        })
        .catch(error => {
            this.close();
        });
    }

    openDoc = (url) => () =>{
        const docUrl = `${url}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    uploadConsent = () => {
        this.open();
        var formData = new FormData();
        formData.append("fundDoc", this.state.fundDoc);
        if(this.state.docType === 'Amendment') {
            formData.append("amendmentId", this.state.selectedDocId.id);
        }
        this.Fsnethttp.uploadWithoutInvestorConsent(formData,this.state.fundId,this.state.docType,'uploadWithoutInvestorConsent').then(result => {
            this.close();
            this.setState({
                showConsentUpload:false,
                showToast: true,
                toastMessage: `${this.state.docType === 'Amendment' ? 'Amendment document' : 'Fund agrement document'} has been uploaded successfully.`,
                toastType: 'success',
            },()=>{
                setTimeout(() => {
                    this.closeConsentUploadModal(true)();
                }, 2000);
            })
        })
        .catch(error => {
            this.close();
        });
        
    }

    updateFileName = (value) => {
        this.setState({
            fileName: value ? value.name:null,
            fundDoc:value?value.docFile:null
        })
    }

    render() {
        return (
            <div>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.showInvestorConsent}  onHide={this.closeInvestorConsentModal(false)} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title-md">Please be mindful of the provisions of the Fund Agreement when using this functionality. You may elect to upload and override an existing version of the Fund Agreement (or any Fund Agreement Amendment). Investors will not be asked to consent to the new Fund Agreement (or Fund Agreement Amendment) using this option, but the Fund Agreement (or Fund Agreement Amendment) in the document locker will be updated and the existing Fund Agreement (or Fund Agreement Amendment) will be deleted. Previous electronic signatures will be applied to the modified document. This functionality is intended especially for situations where the Fund Manager is expressly permitted to modify the Fund Agreement without the consent of Investors, which is not always permitted (for example to correct typographical errors or ambiguities). We recommend you consult with legal counsel prior to using this functionality.</h1>
                        <Row className="fundBtnRow">
                            <Col lg={5} md={5} sm={5} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeInvestorConsentModal(false)}>Cancel</Button>
                            </Col>
                            <Col lg={7} md={7} sm={7} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.getListOfDocs}>I Agree – Please Proceed</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.showConsentUpload}  onHide={this.closeConsentUploadModal(false)} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title-md-noBold">You are uploading a revised {this.state.amendmentsList.length > 0 ? 'Fund Agreement/Amendment' : 'Fund Agreement'} without any Investor consent. This feature should be used only for clerical corrections or in other situations where Investor consent is not required as a legal matter.  Please consult your legal counsel prior to utilizing this option.  Note, you should upload all pages of the document except the signature pages.  The signature pages currently appended to the document now in effect will be appended automatically to the revised document you are uploading here.</h1>
                        
                        <h1 className="title marginTop10 marginBotNone">Fund Agreement:</h1>
                        <div className="consentFAandAmendents">
                             <Row>
                                <Col lg={12} md={12} sm={12} xs={12}>
                                    <Radio name="changedPageDoc" inline onChange={()=>this.setState({selectedDocId:this.state.existingFAList,docType:'Fund_Agreement'})}>
                                        <span className="radio-checkmark"></span>
                                    </Radio>
                                    <span className="radio-span cursor" onClick={this.openDoc(this.state.existingFAList.fundAgreementsData.path)}>{this.state.existingFAList.fundAgreementsData ? this.state.existingFAList.fundAgreementsData.originalname:null}</span>
                                </Col>
                            </Row>
                                
                        </div>
                        {
                           this.state.amendmentsList.length > 0 && 
                           <div>
                                <h1 className="title marginTop10 marginBotNone">Amendments:</h1>
                                <div className="consentFAandAmendents">
                                    {
                                        this.state.amendmentsList.map((record, index) => {
                                            return (
                                                <Row className="marginTop10" key={index}>
                                                    <Col lg={12} md={12} sm={12} xs={12} className="height40">
                                                        <Radio name="changedPageDoc" inline onChange={()=>this.setState({selectedDocId:record,docType:'Amendment'})}>
                                                            <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <span className="radio-span cursor" onClick={this.openDoc(record.document.url)}>{record['document']['originalname']}</span>
                                                    </Col>
                                                </Row>
                                                    
                                            );
                                        })
                                    }
                                </div>
                           </div>
                        }
                        {
                            this.state.selectedDocId &&
                            <div className="marginTop20 fundAgreementChange marginBot20">
                                <h1 className="title">Upload Document</h1>
                                <UploadFileComponent fileName={this.updateFileName} docType={this.state.docType}></UploadFileComponent>
                            </div>
                        }
                        
                        <Row className="fundBtnRow">
                            <Col lg={7} md={7} sm={7} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeConsentUploadModal(false)}>Cancel</Button>
                            </Col>
                            <Col lg={5} md={5} sm={5} xs={12}>
                                <Button type="button" className={"fsnetSubmitButton " + (this.state.fileName ? 'btnEnabled':'disabled')} onClick={this.uploadConsent}>Submit</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
            </div>


        );
    }
}

export default InvestorConsentComponent;


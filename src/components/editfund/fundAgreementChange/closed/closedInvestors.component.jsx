import React, { Component } from 'react';
import { Row, Col, Button, Modal, Radio,FormControl, ControlLabel } from 'react-bootstrap';
import Loader from '../../../../widgets/loader/loader.component';
import { Constants } from '../../../../constants/constants';
import { Fsnethttp } from '../../../../services/fsnethttp';
import { FsnetAuth } from '../../../../services/fsnetauth';
import { FsnetUtil } from '../../../../util/util';
import  UploadFileComponent from '../upload/uploadFile.component';
import ToastComponent from '../../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import ChangedPagesComponent from "../changedPagesUpload/changedPagesUpload.component";

var close={}
class closedInvestorsComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showClosedModal:false,
            fundId:null,
            targetPercentage:null,
            targetPercentageMask:null,
            considerOnlyLpOfferedAmount:null,
            showOneClosedInvestor:false,
            fundStatus:{},
            showSecondModal:false,
            docFileName:null,
            step1:true,
            docFileType:null,
            isChangedPages: null,
            docFile:{},
            fileTypeName:'Fund Agreement',
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            cpDocFile:null,
            FADocFile:null,
        }
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast = (timed) => {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }


    componentDidMount() {
        this.setState({ fundId: this.props.fundId,fundStatus:this.props.fundStatus},()=>{
            this.openOneClosedInvestorModal()
        });
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    openClosedModal = () => {
        this.setState({
            showClosedModal:true,
            showSecondModal:false
        })
    }

    closeClosedModal = (val) => () => {
        this.setState({
            showClosedModal:false
        })
        this.props.onCancel(val ? {type:true}:false);  
    }

    openSecondModal = () => {
        if(this.state.fundStatus.fundFinalClosing) {
            this.setState({showSecondModal:true,showOneClosedInvestor:false})
        } else {
            this.setState({showOneClosedInvestor:false,showClosedModal:true})
        }
    }

    openOneClosedInvestorModal = () => {
        this.setState({showOneClosedInvestor:true})
    }

    closeOneClosedInvestorModal = () => {
        this.setState({showOneClosedInvestor:false})
        this.props.onCancel(false);  
    }

    closeSecondModal = () => {
        this.setState({showSecondModal:false})
        this.props.onCancel(false)
    }

    enableOrDisableButton = () => {
        //if(this.state.docFileType && this.state.docFile   Name) { // previous code
        if((this.state.docFileType==='Fund_Agreement' && this.state.FADocFile && !this.state.isChangedPages) ||
            (this.state.docFileType==='Fund_Agreement' && this.state.FADocFile && this.state.isChangedPages  && this.state.cpDocFile) ||
            (this.state.docFileType != 'Fund_Agreement' && this.state.FADocFile))
        {
            this.setState({
                isFormValid:true
            })
        } else {
            this.setState({
                isFormValid:false
            })
        }
    }

    handleOnRadioChange = (val) => () => {
        this.setState({
            docFileType:val,
            cpDocFile: val === 'Fund_Agreement'? null : this.state.cpDocFile,
            fileTypeName: val === 'Fund_Agreement' ? 'Fund Agreement' : 'Amendment',
            isChangedPages: val === 'Fund_Agreement' ? true : null
        },()=>{
            this.enableOrDisableButton()
        })
    }

    handleOnIsChangedPagesRadioChange = (val) => () => {
        this.setState({
            isChangedPages: val,
            cpDocFile: !val ? null : this.state.cpDocFile
        },()=>{
            this.enableOrDisableButton()
        })
    }

    clearFileds = () => {
        this.setState({
            FADocFile:null,
            cpDocFile:null,
            fileTypeName:null,
            docFileType:null,
            step1:true
        })
    }

    submitFn = () => {
        this.open();
        var formData = new FormData();
        formData.append("fundDoc", this.state.FADocFile);
        if(this.state.isChangedPages && this.state.cpDocFile) {
            formData.append("changedpages", this.state.cpDocFile);
        }

        formData.append("targetPercentage", this.state.targetPercentage);
        formData.append("considerOnlyLpOfferedAmount", this.state.considerOnlyLpOfferedAmount);
        this.Fsnethttp.uploadDocsToClosedInvestors(formData,this.state.fundId,this.state.docFileType).then(result => {
            this.close();
            this.setState({
                showClosedModal:false,
                showToast: true,
                toastMessage: this.Constants.DOC_UPLOAD_MSZ,
                toastType: 'success',
            },()=>{
                setTimeout(() => {
                    this.closeClosedModal(true)();
                }, 2000);
            })
        })
        .catch(error => {
            this.close();
        });
    }

    updateFileName = (value) => { console.log(value);
        this.setState({
            cpDocFile: value && value.type === 'CP' ? value.docFile : this.state.cpDocFile ? this.state.cpDocFile : null,
            FADocFile: value && value.type === 'FA' ? value.docFile : this.state.FADocFile ? this.state.FADocFile : null
           /* docFile: value.docFile,
            docFileName:value.name ? value.name : null,*/
        },()=>{
            this.enableOrDisableButton()
        })
    }

    handleInputChangeEvent = (blur) => (event) => {
        let value = event.target.value.trim()
        const re = this.Constants._99REGEX;
        if (!re.test(value.trim()) || (blur && isNaN(value))) {
            this.setState({
                targetPercentage:this.state.targetPercentage && this.state.targetPercentage != '.' ? this.state.targetPercentage : '',
                targetPercentageMask: this.state.targetPercentage && this.state.targetPercentage != '.' && blur ? `${parseFloat(value)}%` : this.state.targetPercentageMask
            })
            return true;
        } else {
            if(blur && (parseFloat(value) <= 0 || parseFloat(value) > 100)) {
                this.setState({
                    targetPercentage: this.state.targetPercentage != '0.' && parseFloat(value) != 0 ? this.state.targetPercentage:'',
                    targetPercentageMask: this.state.targetPercentage != '0.' && parseFloat(value) != 0 ?  this.state.targetPercentageMask:''
                })
                return true;
            } else {
                if((parseFloat(value) < 0 || parseFloat(value) > 100)) {
                    return true;
                }
            }
            if(value === '' || value === undefined || value == '.') {
                this.setState({
                    targetPercentage: '',
                    targetPercentageMask: blur && value && value != '.' ? `${parseFloat(value).toFixed(2)}%` : ''
                })
            } else {
                this.setState({
                    targetPercentage: parseFloat(value), 
                    targetPercentageMask: blur ? `${parseFloat(value).toFixed(2)}%` : value
                })
            }
        }
    }

    render() {
        return (
            <div>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.showOneClosedInvestor}  onHide={this.closeOneClosedInvestorModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title-md">Please be mindful that this functionality seeks consent to amend the Fund Agreement with closed Investors only.  Following effectiveness of the proposed Amendment, any and all new Investors will be required to electronically sign the Amendment prior to their admission as a condition thereof.  If this Amendment process has been initiated as to some Investors but the Amendment is not yet effective, and in that interim period of time you close upon new Investors, following closing they will receive a notification of the pending Amendment and be asked to electronically sign it.  There are important strategic and technical decisions that arise in determining the ordering of these events and the potential to use the Change Fund Agreement as to Non-Closed Investors functionality separately or together with this functionality.  Please consult legal counsel to ensure you are taking the appropriate steps.</h1>
                        <Row className="fundBtnRow">
                            <Col lg={5} md={5} sm={5} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeOneClosedInvestorModal}>Cancel</Button>
                            </Col>
                            <Col lg={7} md={7} sm={7} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.openSecondModal}>I Agree – Please Proceed</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.showSecondModal}  onHide={this.closeSecondModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title-md">This functionality allows you to upload and circulate a stand-alone Amendment or a full amended and restated Fund Agreement.  In general, a stand-alone Amendment is a separate document that will coexist and be read together with the existing Fund Agreement, and a full amended and restated Fund Agreement supersedes entirely the existing Fund Agreement.  Please consult your legal counsel to understand the differences in those options.</h1>
                        <Row className="fundBtnRow">
                            <Col lg={5} md={5} sm={5} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeSecondModal}>Cancel</Button>
                            </Col>
                            <Col lg={7} md={7} sm={7} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.openClosedModal }>I Agree – Please Proceed</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.showClosedModal}  onHide={this.closeClosedModal(false)} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    {
                        this.state.step1 ? 
                        <Modal.Body style={{'padding':'10px 30px 20px 30px'}}>
                            <h1 className="title-md">Please select whether you would like to track the percentage of investor approval by total investor commitments or total aggregate commitments, including the Fund Manager's commitment.</h1>
                            <Row className="marginTop10">
                                <Col lg={6} md={6} sm={6} xs={6}>
                                    <Radio name="changedPageDoc" inline checked={this.state.considerOnlyLpOfferedAmount} onChange={()=>this.setState({considerOnlyLpOfferedAmount:true})}>
                                        <span className="radio-checkmark"></span>
                                    </Radio>
                                    <span className="radio-span">% of Investor Commitments</span>
                                </Col>
                                <Col lg={6} md={6} sm={6} xs={6}>
                                    <Radio name="changedPageDoc" inline checked={this.state.considerOnlyLpOfferedAmount === false} onChange={()=>this.setState({considerOnlyLpOfferedAmount:false})}>
                                        <span className="radio-checkmark"></span>
                                    </Radio>
                                    <span className="radio-span">% of Total Aggregate Commitments</span>
                                </Col>
                            </Row>
                            <h1 className="title-md marginTop30">Please indicate the percentage in interest of Investors which must approve the Amendment.  For “majority in interest” please enter 50.01%.  For “two-third in interest” please enter 66.67%.  You may enter any amount.  The value you enter is used solely for purposes of tracking the status of the Amendment.</h1>
                            <ControlLabel className="labelFormControl marginTop10">% in Interest</ControlLabel>
                            <FormControl type="text" placeholder="50.01" autoComplete="off" maxLength="100" value={this.state.targetPercentageMask} className="formControl inputControl" onChange={this.handleInputChangeEvent(false)} onBlur={this.handleInputChangeEvent(true)} onFocus={()=>{this.setState({targetPercentageMask:this.state.targetPercentage})}}/>

                            <Row className="fundBtnRow">
                                <Col lg={7} md={7} sm={7} xs={12} className="text-center">
                                    <Button type="button" className="fsnetCancelButton" onClick={this.closeClosedModal(false)}>Cancel</Button>
                                </Col>
                                <Col lg={5} md={5} sm={5} xs={12}>
                                    <Button type="button" className={"fsnetSubmitButton " + (this.state.targetPercentage && this.state.considerOnlyLpOfferedAmount !== null ? 'btnEnabled':'disabled')} onClick={()=>this.setState({step1:false})}>Next</Button>
                                </Col>
                            </Row>   
                        </Modal.Body>
                        :
                        <Modal.Body style={{'padding':'10px 30px 20px 30px'}}>
                             <h1 className="title-md">Please select whether you would like to upload a new, fully-revised Fund Agreement or an Amendment to the existing Fund Agreement.</h1>
                            <Row className="marginTop10">
                                <Col lg={6} md={6} sm={6} xs={6}>
                                    <Radio name="changedPageDoc" inline checked={this.state.docFileType === 'Fund_Agreement'} onChange={this.handleOnRadioChange('Fund_Agreement')}>
                                        <span className="radio-checkmark"></span>
                                    </Radio>
                                    <span className="radio-span">Upload Fund Agreement File</span>
                                </Col>
                                <Col lg={6} md={6} sm={6} xs={6}>
                                    <Radio name="changedPageDoc" inline checked={this.state.docFileType === 'Amendment'} onChange={this.handleOnRadioChange('Amendment')}>
                                        <span className="radio-checkmark"></span>
                                    </Radio>
                                    <span className="radio-span">Upload Amendment File</span>
                                </Col>
                            </Row>
                            
                            {
                                this.state.docFileType === 'Fund_Agreement' &&
                                <Row>
                                    <h1 className="title-md marginTop20 marginBot10 marginLeft15">Would you like to additionally upload a “changed pages” file? Note, a “changed pages” file is a shorter summary document showing solely the relevant modified pages of the longer agreement. If you select this option, Investors will review and approve changes based solely on such shorter file. The shorter “changed pages” file will be used solely as a convenience to gain Investor consent to the revised Fund Agreement, and will not be stored permanently in the Fund’s document locker. If you have questions, please discuss this option with your legal counsel.</h1>
                                    <Row className="marginLeftNone">
                                        <Col lg={6} md={6} sm={6} xs={6}>
                                            <Radio name="isChangedPages" inline checked={this.state.isChangedPages === true} onChange={this.handleOnIsChangedPagesRadioChange(true)}>
                                                <span className="radio-checkmark"></span>
                                            </Radio>
                                            <span className="radio-span">With Changed pages</span>
                                        </Col>
                                        <Col lg={6} md={6} sm={6} xs={6}>
                                            <Radio name="isChangedPages" inline checked={this.state.isChangedPages === false} onChange={this.handleOnIsChangedPagesRadioChange(false)}>
                                                <span className="radio-checkmark"></span>
                                            </Radio>
                                            <span className="radio-span">Without Changed pages</span>
                                        </Col>
                                    </Row>
                                </Row>
                            }

                            {
                                this.state.docFileType === 'Fund_Agreement' && this.state.isChangedPages &&
                                <div className="marginTop20 fundAgreementChange marginBot20">
                                    <h1 className="title">Upload Changed Pages File</h1>
                                    <ChangedPagesComponent cPFileName={this.updateFileName}></ChangedPagesComponent>
                                </div>
                            }
                            {
                                this.state.docFileType &&
                                <div className="marginTop20 fundAgreementChange marginBot20">
                                    <h1 className="title">Upload {this.state.docFileType==='Fund_Agreement'?'Fund Agreement':'Amendment'} File</h1>
                                    <UploadFileComponent fileTypeName={this.state.fileTypeName} fileName={this.updateFileName}></UploadFileComponent>
                                </div>

                            }

                            <Row className="fundBtnRow marginTop40">
                                <Col lg={7} md={7} sm={7} xs={12} className="text-center">
                                    <Button type="button" className="fsnetCancelButton" onClick={this.clearFileds}>Cancel</Button>
                                </Col>
                                <Col lg={5} md={5} sm={5} xs={12}>
                                    <Button type="button" className={"fsnetSubmitButton " + (this.state.isFormValid ? 'btnEnabled':'disabled')} onClick={this.submitFn}>Submit</Button>
                                </Col>
                            </Row>   
                        </Modal.Body>
                    }
                    
                </Modal>
               
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
            </div>


        );
    }
}

export default closedInvestorsComponent;


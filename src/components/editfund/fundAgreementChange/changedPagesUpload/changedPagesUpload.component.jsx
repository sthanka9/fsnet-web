import React, { Component } from 'react';
import { Row, Col, Button, Modal, Radio } from 'react-bootstrap';
import { Constants } from '../../../../constants/constants';
import { Fsnethttp } from '../../../../services/fsnethttp';
import { FsnetAuth } from '../../../../services/fsnetauth';
import { FsnetUtil } from '../../../../util/util';
import FileDrop from 'react-file-drop';
import Loader from '../../../../widgets/loader/loader.component';

class ChangedPagesComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showModal:false,
            uploadFileName:null
        }

    }

    componentDidMount() {

    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    changedPageUploadBtnClick = () => {
        document.getElementById('changedPageUploadBtn').click();
    }

    //Upload patnership document.
    handleChange = (event, type) => {
        let obj;
        if(type=== 'drop') {
            obj = event;
        } else {
            obj = event.target.files
        }
        const fileTypeName = 'Changed pages document';
        let reader = new FileReader();
        if(obj && obj.length > 0) {
            this.uploadFile = obj[0];
            let sFileName = obj[0].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            if(sFileExtension !== 'pdf') {
                document.getElementById('changedPageUploadBtn').value = "";
                alert(`${fileTypeName} must be in PDF format and smaller than 10MB.`)
                return true;
            }
            //File 10MB limit
            if(this.uploadFile.size <=this.Constants.SIZE_LIMIT) {
                this.setState({
                    uploadDocFile : obj[0],
                    uploadDocSize: (this.uploadFile.size / this.Constants.SIZE_MB).toFixed(2)+' MB',
                    uploadFileName: obj[0].name,
                },()=>{
                    this.clearFile();
                    this.getDocTempFile();
                });
                reader.readAsDataURL(this.uploadFile);
            } else {
                document.getElementById('changedPageUploadBtn').value = "";
               alert(`${fileTypeName} should be less than 10 MB.`)
            }
        }
    }

    getDocTempFile = () => {
        this.open();
        var formData = new FormData();
        formData.append("fundDoc", this.state.uploadDocFile);
        this.Fsnethttp.getDocTempFile(formData).then(result => {
            this.close();
            this.setState({
                uploadDocFileUrl: result.data.url,
            })
            this.props.cPFileName({name:this.state.uploadFileName,docFile:this.state.uploadDocFile,type:'CP'});  
        })
            .catch(error => {
                this.close();
            });
    }

    openUploadDoc = () => {
        this.FsnetUtil._openDoc(this.state.uploadDocFileUrl)
    }

    deleteFile = () => {
        this.setState({
            uploadDocFileUrl:null,
            uploadFileName:null,
            uploadDocFile:{},
        })
        this.props.cPFileName({docFile:null,type:'CP'});  
    }

    clearFile = () => {
        document.getElementById('changedPageUploadBtn').value = "";
    }

    render() {
        return (
            <div>
                <input type="file" id="changedPageUploadBtn" className="hide" onChange={this.handleChange} />
                    <div className="uplodFileContainer width100">
                        <FileDrop onDrop={(e) => this.handleChange(e, 'drop')}>
                            <div>
                                <Button className="uploadBox left" onClick={this.changedPageUploadBtnClick}>{this.state.uploadFileName ? 'Replace File':'Upload File'}</Button>
                            </div>
                            {
                                this.state.uploadFileName ?
                                <div className="title-md paddingTop10">
                                    <div className="ellipsis width530" title={this.state.uploadFileName}><a className="cursor" onClick={this.openUploadDoc}>{this.state.uploadFileName}</a></div>
                                    <div className="opac5">{this.state.uploadDocSize}<i className="fa fa-trash cursor-pointer marginLeft10" onClick={this.deleteFile}></i></div>
                                </div>
                            :
                             <div className="uploadFileText width700">{this.Constants.DROP_YOUR_FILES_FROM_DESKTOP}</div>
                            }
                        </FileDrop>  
                    </div>                               
                    <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default ChangedPagesComponent;


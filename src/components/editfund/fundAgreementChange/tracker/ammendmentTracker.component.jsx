import React, { Component } from 'react';
import { Row, Col, Button, Modal, FormControl } from 'react-bootstrap';
import { Constants } from '../../../../constants/constants';
import { Fsnethttp } from '../../../../services/fsnethttp';
import { FsnetAuth } from '../../../../services/fsnetauth';
import { FsnetUtil } from '../../../../util/util';
import Loader from '../../../../widgets/loader/loader.component';
import documentImage from '../../../../images/documentsWhite.svg';
import userDefaultImage from '../../../../images/default_user.png';
import ToastComponent from '../../../toast/toast.component';
import { PubSub } from 'pubsub-js';
var _ = require('lodash');

var close = {} 
class AmmendmentTrackerComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            investorStatusModal: false,
            trackerList:[],
            fundId:null,
            currentSort:null,
            sortType:null,
            infoModal:false,
            approvedList:[],
            hasSignature:false,
            signatureModal:false,
            selectedObj:{},
            showToast: false,
            toastMessage: '',
            toastType: 'success',
        }
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }


    componentDidMount() {
        this.setState({ fundId: this.FsnetUtil._getId() },()=>{
            this.getListOfDocs();
            this.checkSignature();
        });
        
    }

    closeToast = (timed) => {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    open = () => {
        this.setState({ showModal: true });
    }

    closeinvestorStatusModal = () => {
        this.setState({ investorStatusModal: false,approvedList:[],selectedObj:{} });
    }

    openInvestorApprovedModal = () => {
        this.setState({ investorStatusModal: true });
    }

    getAppovedInvestors = (data) => () => {
        this.open();
        this.Fsnethttp.investorsApprovedDetails(this.state.fundId,data.amendmentId).then(result => {
            this.close();
            this.setState({
                approvedList: result.data,
                investorStatusModal:true,
                selectedObj: data
            })
        })
        .catch(error => {
            this.close();
        });
    }

    getListOfDocs = () => {
        this.open();
        this.Fsnethttp.displayAmmendmentTracker(this.state.fundId).then(result => {
            this.close();
            this.setState({
                trackerList: result.data
            })
        })
        .catch(error => {
            this.close();
        });
    }

    openDoc = (url) => () =>{
        const docUrl = `${url}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    sortDocs = (type,key) => () => {
        if(this.state.trackerList.length > 1) {
            let sortList = [];
            this.setState({
                currentSort: type,
                sortType: this.state.sortType === 'desc' || !this.state.sortType ? 'asc' : 'desc'
            },()=>{
                sortList = _.orderBy(this.state.trackerList, [key], [this.state.sortType]);
                this.setState({
                    trackerList:sortList
                })
            })
        }
    }

    closeinfoModal = () => {
        this.setState({
            infoModal:false
        })
    }

    openInfoModal = (val,data) => () => {
        this.setState({
            isEffectuate:val,
            selectedDoc:data,
            infoModal:true
        })
    }

    proceed = () => {
        this.open();
        let url = this.state.isEffectuate ? `effectuateAmendment` : `terminateAmendment`;
        let obj = {signaturePassword: this.state.signaturePassword,amendmentId:this.state.selectedDoc.amendmentId,fundId:this.state.fundId}
        this.Fsnethttp.effectuateOrTerminate(url,obj).then(result => {
            this.close();
            this.closeinfoModal();
            this.setState({
                showToast: true,
                signaturePasswordModal:false,
                toastMessage: this.state.isEffectuate && this.FsnetUtil.getUserRole() === 'GP' && !this.state.selectedDoc.isAmmendment ? 'Fund agreement change successfully effectuated.' : this.state.isEffectuate && this.FsnetUtil.getUserRole() === 'GP' && this.state.selectedDoc.isAmmendment ? 'Amendment to the Fund Agreement successfully effectuated.' : this.state.isEffectuate && this.FsnetUtil.getUserRole() === 'SecondaryGP' && !this.state.selectedDoc.isAmmendment  ? 'Fund agreement change successfully approved.' : this.state.isEffectuate && this.FsnetUtil.getUserRole() === 'SecondaryGP' && this.state.selectedDoc.isAmmendment ? 'Amendment to the Fund Agreement successfully approved.' : this.state.selectedDoc.isAmmendment ? 'Amendment to Fund Agreement has been terminated successfully.':'Fund Agreement has been terminated.',
                toastType: 'success',
            })
            this.closeToast(2000)
            this.getListOfDocs(); 
        })
        .catch(error => {
            this.close();
            if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                this.setState({
                    signatureErrorMsz: error.response.data.errors[0].msg,
                });
            }
        });
        
    }

    checkSignature = () => {
        let userId = this.FsnetUtil.getUserId();
        this.Fsnethttp.getUserProfile(userId).then(result => {
            this.close();
            this.setState({
                hasSignature: result.data.signaturePic ? true : false,
            })
        });
    }

    handleSignatureClose = () => {
        this.setState({signatureModal:false})
    }

    sendReminder = (data) => () => {
        this.open();
        this.Fsnethttp.sendRemainderToClosedInvestors(this.state.fundId,data.amendmentId).then(result => {
            this.close();
            this.setState({
                showToast: true,
                toastMessage: 'Reminder has been sent to the investor successfully.',
                toastType: 'success',
            })
            this.closeToast(2000)
        })
        .catch(error => {
            this.close();
        });
    }


    handleSignatureClose = () => {
        this.setState({
            signaturePasswordModal:false,
            signaturePassword:null,
            signatureErrorMsz:''
        })
    }

    handleSignatureOpen = () => {
        if(this.state.hasSignature) {
            this.setState({
                signaturePasswordModal:true,
                infoModal:false
            })
        } else {
            this.setState({
                signatureModal:true,
                infoModal:false
            })
        }
    }

    verifySignature = () => {
        if (this.state.signaturePassword) {
                this.proceed();
        }
        else {
            this.setState({
                signatureErrorMsz: this.Constants.SIGNATURE_PWD,
            });
        }
    }

    render() {
        return (
            <div className="margin30">
                {
                    this.state.trackerList.length > 0 &&
                    <Row className="marginLeft30">
                        <div className="name-heading width210 cursor" onClick={this.sortDocs('date','issuedDate')}>
                            Date Issued
                            {
                                this.state.currentSort === 'date' ?
                                <i className={"fa "+(this.state.sortType === 'asc' ? 'fa-sort-asc':'fa-sort-desc')} aria-hidden="true"></i>
                                :
                                <i className="fa fa-sort-desc" aria-hidden="true"></i>
                            }
                        </div>
                        {/* <div className="name-heading width150" onClick={this.sortDocs('date','dateEffectuated')}>
                            Date Effectuated
                            {
                                this.state.currentSort === 'dateEffectuated' ?
                                <i className={"fa "+(this.state.sortType === 'asc' ? 'fa-sort-asc':'fa-sort-desc')} aria-hidden="true"></i>
                                :
                                <i className="fa fa-sort-desc" aria-hidden="true"></i>
                            }
                        </div> */}
                        <div className="name-heading width100Px cursor" onClick={this.sortDocs('fundPer','approvedPercentage')}>
                            % Approved
                            {
                                this.state.currentSort === 'fundPer' ?
                                <i className={"fa "+(this.state.sortType === 'asc' ? 'fa-sort-asc':'fa-sort-desc')} aria-hidden="true"></i>
                                :
                                <i className="fa fa-sort-desc" aria-hidden="true"></i>
                            }
                        </div>
                        <div className="name-heading width100Px cursor" onClick={this.sortDocs('targetPer','targetPercentage')}>
                            Required %
                            {
                                this.state.currentSort === 'targetPer' ?
                                <i className={"fa "+(this.state.sortType === 'asc' ? 'fa-sort-asc':'fa-sort-desc')} aria-hidden="true"></i>
                                :
                                <i className="fa fa-sort-desc" aria-hidden="true"></i>
                            }
                        </div>
                    </Row>
                }
                <div className={"userLPContainer " + (this.state.trackerList.length === 0 ? 'borderNone' : '')} style={(this.state.trackerList.length > 0 ? {'width':'970px'} : {'width':'100%'})}>
                    {
                        this.state.trackerList.length > 0 ?

                        this.state.trackerList.map((record, index) => {
                            return (
                                <div className="userRow" key={index}>
                                    <div className="lp-name width210 paddingLeft30">{record.issuedDate}</div>
                                    <div className="lp-name width100Px paddingLeft30 cursor text-center" onClick={this.getAppovedInvestors(record)}><a className="showLink">{record.approvedPercentage}%</a></div>
                                    <div className="lp-name width100Px paddingLeft30 text-center">{record.targetPercentage}%</div>
                                    <Button className="btnViewPrint marginLeft30" onClick={this.openDoc(record.documentPath)}><img src={documentImage} alt="home_image" /><span className="viewPrintText">View + Print</span></Button>
                                    <Button className="btnViewPrint marginLeft30" disabled={((this.FsnetUtil.getUserRole() === 'GP' && record.isGpEffectuate) || (this.FsnetUtil.getUserRole() === 'SecondaryGP' && record.isSecondaryGpEffectuate ) ) && (record.approvedPercentage > 0) ? false : true} onClick={this.openInfoModal(true,record)}>{this.FsnetUtil.getUserRole() === 'GP' ? 'Effectuate' : this.FsnetUtil.getUserRole() === 'SecondaryGP' && record.isSecondaryGpApproved ? 'Approved':'Approve'}</Button>
                                    <Button className="btnViewPrint marginLeft30" disabled={this.FsnetUtil.getUserRole() !== 'GP'} onClick={this.openInfoModal(false,record)}>Terminate</Button>
                                    <Button className="btnViewPrint marginLeft30" onClick={this.sendReminder(record)}>Send Reminder</Button>
                                </div>
                            );
                        })
                        
                        :
                        <div className="title minHeight100 marginTop50 text-center">There are no documents to display.</div>
                    }
                </div>

                <Modal id="fundAggrementChangeModal" className="investorApprovedModal" show={this.state.investorStatusModal} onHide={this.closeinvestorStatusModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="heightNone" closeButton>
                    </Modal.Header>
                    <Modal.Title>
                        <Row className="approved-header">
                            <div>
                                <label>Date Amendment Issued</label>
                                <span>{this.state.selectedObj.issuedDate}</span>
                            </div>
                            <div>
                                <label># Investors Approved</label>
                                <span>{this.state.selectedObj.investorsApproved ? this.state.selectedObj.investorsApproved : 0}</span>
                            </div>
                        </Row>
                    </Modal.Title>
                    <Modal.Body>
                        {
                            this.state.approvedList.length > 0 ? 
                            this.state.approvedList.map((record, index) => {
                                    return (
                                        <div className="row odd" key={index}>
                                            <div className="col-md-8 col-xs-8 col-sm-8">
                                                <div className="user-img">
                                                    <img src={record.profilePic ? record.profilePic.url : userDefaultImage} alt="img" className="user-image" />
                                                </div> 
                                                <div className="user-text">
                                                    <h6 className="ellipsis" title={record.investorName}>{record.investorName}</h6>
                                                    <p>approved {record.docType === 'FUND_AGREEMENT' ? 'Restated Fund Agreement.':'Amendment to Fund Agreement.'}</p>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-4 col-sm-4 ">
                                                <div className="user-timing">
                                                <p>{this.FsnetUtil.getDateFromFormat(record.ammendmentSignedDate)}</p>
                                                <p>{this.FsnetUtil.getTimeFromFormat(record.ammendmentSignedDate)} ({record.timeZone})</p>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            :
                            <h1 className="title-md marginTop30 text-center">There are no investors approved.</h1>
                        }
                    </Modal.Body>
                </Modal>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.infoModal}  onHide={this.closeinfoModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Title className="fac-title">
                    {
                            this.state.isEffectuate ?  
                            'Effectuate'
                            :
                            'Terminate'
                    }
                    </Modal.Title>
                    <Modal.Body>
                        {
                            this.state.isEffectuate ? 
                                <h1 className="title-md">Once the required level of consent is obtained, you may at any time thereafter effectuate the amendment.  You may wish to wait to obtain a greater level of consent for client relations or other reasons.  Please discuss with your legal counsel.  Upon selecting this option and completing the signature process on behalf of the Fund Manager, all investors will be notified of passage and the amended document will be placed in the Document Locker.  The process of seeking additional investor consent will be terminated, even if less than 100% of investor consent has been obtained.</h1>
                            :
                                <h1 className="title-md">This option will discontinue the amendment process.  The amendment will be abandoned, regardless of level of consent obtained.  The process of seeking additional investor consent will be terminated.  No documents will be placed in the Document Locker and the previously effective document will remain in effect.  You may wish to use this option if, for example, it has become clear that the requisite approval will not be obtained or if the reason for seeking amendment no longer exists.  Please consult carefully with your legal counsel prior to proceeding.  This action is not reversible.</h1>
                        }
                        <Row className="fundBtnRow">
                            <Col lg={5} md={5} sm={5} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeinfoModal}>Cancel</Button>
                            </Col>
                            <Col lg={7} md={7} sm={7} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.handleSignatureOpen}>I Agree – Please Proceed</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                <Modal id="GPDelModal" backdrop="static" show={this.state.signatureModal} onHide={this.handleSignatureClose} dialogClassName="GPDelModalDialog fundModalDialog" className="GPDelModalMarginLeft GPSignModal topZero">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>Configure Signature</Modal.Title>
                    <Modal.Body>
                        <div className="title">Please Configure Signature using this <a href={"/settings/uploadSignature/" + this.FsnetUtil._encrypt(this.state.fundId)}>link</a>.</div>
                    </Modal.Body>
                </Modal>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.signaturePasswordModal} onHide={this.handleSignatureClose} dialogClassName="confirmFundDialog signatureModal">
                    <Modal.Header closeButton className="headerNone">
                    </Modal.Header>
                    <Modal.Body>
                        <div>
                            <div className="title-md">{this.state.isEffectuate ? this.Constants.PASSWORD_MESSAGE : this.Constants.TERMINATE_MESSAGE}</div>
                            <div className="title-md marginTop10">{this.Constants.ENTER_SIGNATURE_AUTHENTICATION_PASSWORD}</div>
                            <div className="form-main-div signatureModalBody">
                                <FormControl type="password" name="signaturePassword" className="inputFormControl inputFormControlCloseFund inputFormSignatuteAuth" placeholder="Password" onChange={(e) => this.setState({signaturePassword:e.target.value.trim(),signatureErrorMsz:null})} />
                            </div>
                            <div className="error marginBot12 height10">{this.state.signatureErrorMsz}</div>
                            <Row >
                                <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                    <Button type="button" className="fsnetCancelButton marginLeft50  marginLeft15Ipad" onClick={this.handleSignatureClose}>Cancel</Button>
                                </Col>
                                <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                    <Button type="button" className={"fsnetSubmitButton closeFundSubmitBtn " + (this.state.signaturePassword ? 'btnEnabled' : 'disabled')} disabled={!this.state.signaturePassword} onClick={this.verifySignature}>Submit</Button>
                                </Col>
                            </Row>
                        </div>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
            </div>
        );
    }
}

export default AmmendmentTrackerComponent;


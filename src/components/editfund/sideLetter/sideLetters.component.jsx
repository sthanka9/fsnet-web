import React, { Component } from 'react';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Row, Col, Button, Checkbox as CBox, Modal } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import userDefaultImage from '../../../images/default_user.png';
import { reactLocalStorage } from 'reactjs-localstorage';

var close = {}
class sideLettersComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showModal: false,
            fundId:null,
            gpSideLetterList:[],
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            selectedDocs: [],
        }

        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast(timed) {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.setState({
            fundId:id
        },()=>{
            this.getSideLetters();
        })

    }

    getSideLetters = () => {
        if(this.state.fundId) {
            this.open();
            this.Fsnethttp.gpSideLetter(this.state.fundId).then(result => {
                this.close();
                this.setState({
                    gpSideLetterList: result.data.data
                })
            })
            .catch(error => {
                this.close();
                this.setState({
                    gpSideLetterList: [],
                })
    
            });
        }
    }

    selectDocuments = (isAll, id) => (e) => {
        let selectedDocs = this.state.selectedDocs;
        let idx = selectedDocs.indexOf(id);
        if(isAll) {
            this.setState({ 
                includeAllFile: e.target.checked 
            });
        } else {
            if(e.target.checked && !isAll) {
                if(idx == -1) {
                    selectedDocs.push(id);
                } else {
                    selectedDocs.splice(idx, 1);
                }
            } else {
                selectedDocs.splice(idx, 1);
            }
        }
        this.setState({
            selectedDocs
        });
    }

    // openSideLetterDoc = (data) => {
    //     window.open(`${data.documentUrl}&token=${JSON.parse(reactLocalStorage.get('token'))}`, '_blank', 'width = 1000px, height = 600px')
    // }

    downloadDocuments = () => {
        this.open();
        let postObj = {
            fileIds: this.state.selectedDocs.join(','),
            includeAllFile: this.state.includeAllFile,
            subscriptionId: null,
            fundId: +this.state.fundId
        }
        
        this.Fsnethttp.downloadFiles(postObj).then(res => {
            this.close();
            if(res) {
                if (res.data && res.data.url) {
                    const docUrl = `${res.data.url}`
                    this.FsnetUtil._openNewTab(docUrl)
                }
            }
        }).catch(error => {
            this.close();
        })
    }

    openSideLetterDoc = (documentUrl) => (data) => {
        const docUrl = `${documentUrl}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    render() {
        return (
            <div className="lpSubFormStyle addendumContainer">
                <div className="main-heading"><span className="main-title">Side Letters</span></div>
                <Row className="documnet-locker-options marginTop20">
                    <Button disabled={!this.state.selectedDocs.length && !this.state.includeAllFile} className="newAddendumButton download-btn marginLeft30" onClick={this.downloadDocuments}><i className="fa fa-download paddingRight5"></i>Download Selected Docs</Button>
                </Row>
                <Row className="full-width marginTop30 marginLeft61" hidden={this.state.gpSideLetterList.length === 0}>
                        <div className="name-heading marginLeft75">
                            Investor Name
                        </div>
                        <div className="name-heading date-name-heading width-210">
                            Date
                        </div>
                        <div className="name-heading paddingLeft30 width-210">
                            Type
                        </div>
                        <div className="name-heading marginLeftMinus10">
                            Download
                        </div>
                    </Row>
                <div className={"userAddendumContainer width820 " + (this.state.gpSideLetterList.length === 0 ? 'borderNone' : 'marginTop10')}>
                    {this.state.gpSideLetterList.length > 0 ?
                        this.state.gpSideLetterList.map((record, index) => {
                            return (
                                <div className="userRow" key={index}>
                                    <label className="userImageAlt">
                                    {
                                        record['profilePic'] ?
                                            <img src={record['profilePic']['url']} alt="img" className="user-image"/>
                                            :   
                                            <img src={userDefaultImage} alt="img" className="user-image"/>
                                    }
                                    </label>
                                    <div className="lp-name">{this.FsnetUtil.getFullName(record)}</div>
                                    <div className="lp-name lp-name-pad width207">{record['timezone_date']} <span class="text-center"> ({record['timezone']})</span> </div>
                                    <div className="lp-name lp-name-pad cursor width200 paddingLeft50" onClick={this.openSideLetterDoc(record.documentUrl)}>Side Letter</div>
                                    <div style={{'display': 'inline-block', 'padding': '0 55px', width: '65px !important'}}>
                                    <CBox checked={this.state.selectedDocs.indexOf(record.id) > -1} onChange={this.selectDocuments(false, record.id)} style={{ 'marginRight': '0px' }}>
                                        <span className="checkmark"></span>
                                    </CBox>
                                </div>
                                </div>
                            );
                        })
                        :
                        <div className="title margin20 text-center">{this.Constants.NO_SIDELETTER_DOCUMENTS}</div>
                    }
                </div>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>   
                <Loader isShow={this.state.showModal}></Loader>
            </div>
            

        );
    }
}

export default sideLettersComponent;


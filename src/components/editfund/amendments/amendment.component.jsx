import React, { Component } from 'react';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Row, Col, Button, Checkbox as CBox, Modal } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import userDefaultImage from '../../../images/default_user.png';

var close = {}
class AmendmentComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showModal: false,
            fundId:null,
            amendmentList:[],
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            selectedDocs: [],
        }

        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast(timed) {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        this.setState({
            fundId:id
        },()=>{
            this.getAmendments();
        })

    }

    getAmendments = () => {
        if(this.state.fundId) {
            this.open();
            this.Fsnethttp.gpAmendment(this.state.fundId).then(result => {
                this.close();
                this.setState({
                    amendmentList: result.data.data
                })
            })
            .catch(error => {
                this.close();
                this.setState({
                    amendmentList: [],
                })
    
            });
        }
    }

    selectDocuments = (isAll, id) => (e) => {
        let selectedDocs = this.state.selectedDocs;
        let idx = selectedDocs.indexOf(id);
        if(isAll) {
            this.setState({ 
                includeAllFile: e.target.checked 
            });
        } else {
            if(e.target.checked && !isAll) {
                if(idx == -1) {
                    selectedDocs.push(id);
                } else {
                    selectedDocs.splice(idx, 1);
                }
            } else {
                selectedDocs.splice(idx, 1);
            }
        }
        this.setState({
            selectedDocs
        });
    }

    downloadDocuments = () => {
        this.open();
        let postObj = {
            fileIds: this.state.selectedDocs.join(','),
            includeAllFile: this.state.includeAllFile,
            subscriptionId: null,
            fundId: +this.state.fundId
        }
        
        this.Fsnethttp.downloadFiles(postObj,'amendmentfiles').then(res => {
            this.close();
            if(res) {
                if (res.data && res.data.url) {
                    const docUrl = `${res.data.url}`
                    this.FsnetUtil._openNewTab(docUrl)
                }
            }
        }).catch(error => {
            this.close();
        })
    }

    openAmendmentDoc = (documentUrl) => (data) => {
        const docUrl = `${documentUrl}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    render() {
        return (
            <div className="lpSubFormStyle addendumContainer">
                <div className="main-heading"><span className="main-title">Amendments</span></div>
                <Row className="documnet-locker-options marginTop20">
                    <Button disabled={!this.state.selectedDocs.length && !this.state.includeAllFile} className="newAddendumButton download-btn marginLeft30" onClick={this.downloadDocuments}><i className="fa fa-download paddingRight5"></i>Download Selected Docs</Button>
                </Row>
                <div className="marginLeft30">

                    {
                        this.state.amendmentList.length > 0 &&
                        <div className="marginTop30" style={(this.state.amendmentList.length > 0 ? {'width':'1000px'} : {'width':'100%'})}>
                            <div className="name-heading inline-block width120 paddingLeft20">
                                Date Issued
                            </div>
                            <div className="name-heading inline-block width150 paddingLeft20">
                                Effectuated Date
                            </div>
                            <div className="name-heading inline-block width260">
                                Type
                            </div>
                            <div className="name-heading inline-block width200">
                                Name
                            </div>
                            <div className="name-heading inline-block">
                                Download
                            </div>
                        </div>
                    }
                    <div className={"userAddendumContainer " + (this.state.amendmentList.length === 0 ? 'borderNone' : 'marginTop10')} style={(this.state.amendmentList.length > 0 ? {'width':'840px'} : {'width':'100%'})}>
                        {this.state.amendmentList.length > 0 ?
                            this.state.amendmentList.map((record, index) => {
                                return (
                                    <div className="userRow" key={index}>
                                        <div className="lp-name width120">
                                            {(record['createdAt'] && record['timezone']) ? record['createdAt'] +`(` +record['timezone']+ `)` : record['createdAt']}
                                        </div>
                                        <div className="lp-name width120">
                                            {(record['affectuatedDate'] && record['timezone']) ? record['affectuatedDate'] +`(` +record['timezone']+ `)` : record['affectuatedDate']}
                                        </div>
                                        <div className="lp-name width260 paddingLeft30">{record.isAmendment ? 'Amendment to Fund Agreement':'Revised Fund Agreement'}</div>
                                        <div className="lp-name cursor width200 ellipsis showLink paddingLeft30" title={record.filename} onClick={this.openAmendmentDoc(record.url)}>{record.filename}</div>
                                        <div style={{'display': 'inline-block', 'padding': '0 45px', width: '65px !important'}}>
                                            <CBox checked={this.state.selectedDocs.indexOf(record.id) > -1} onChange={this.selectDocuments(false, record.id)} style={{ 'marginRight': '0px' }}>
                                                <span className="checkmark"></span>
                                            </CBox>
                                        </div>
                                    </div>
                                );
                            })
                            :
                            <div className="title margin20 text-center">{this.Constants.NO_AMENDMENT_DOCUMENTS}</div>
                        }
                    </div>
                </div>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>   
                <Loader isShow={this.state.showModal}></Loader>
            </div>
            

        );
    }
}

export default AmendmentComponent;


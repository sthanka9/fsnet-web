import React, { Component } from 'react';
import { Row, Col, Button, Modal, Radio } from 'react-bootstrap';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import FileDrop from "react-file-drop";

var close={}
class UploadFileModelComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showConsentUpload:true,
            fundDoc:null,
            showToast: false,
            fileName:null
        }
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })


    }

    componentDidMount() {
        this.setState({
            documentType: (this.props.documentType == 'SUBSCRIPTION_AGREEMENT')? 'Subscription Agreement' : (this.props.documentType == 'CHANGE_COMMITMENT_AGREEMENT')? 'Change Commitment Agreement' : (this.props.documentType == 'SIDE_LETTER_AGREEMENT')? 'Side Letter Agreement' : (this.props.documentType == 'AMENDMENT_AGREEMENT')? 'Amendment Agreement' : (this.props.documentType == 'CONSOLIDATED_AMENDMENT_AGREEMENT')? 'Consolidated Amendment Agreement' : 'Document',
            fundId: this.props.fundId,
            id: this.props.id,
            url: this.props.url
        });
    }

    componentWillUnmount() {
        //PubSub.unsubscribe(close);
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    };
    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    };

    closeUploadModal = () => { console.log('closeUploadModal');
        this.props.onCancel();
    };

    closeUploadModalPopup = () => {  console.log('closeUploadModalPopup');
        setTimeout(() => {
            this.closeUploadModal();
        }, 200);
    }

    closeToast = (timed) => {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    uploadDocumentFile = () => {
        this.open();
        /*var formData = new FormData();
        formData.append("file", this.state.uploadDocFileUrl);
        formData.append("fundId", this.state.fundId);
        formData.append("id", this.state.id);*/
        let data = {
            file: this.state.uploadDocFileUrl,
            fundId: this.state.fundId,
            id: this.state.id,
            docType: this.props.documentType
        };
        this.Fsnethttp.uploadFile(data,this.state.fundId,this.state.documentType,this.state.url).then(result => {
            this.close();
            this.setState({
                showConsentUpload:false,
                showToast: true,
                toastMessage: `${this.state.docType === 'Amendment' ? 'Amendment document' : 'Fund agrement document'} has been uploaded successfully.`,
                toastType: 'success',
            },()=>{
                setTimeout(() => {
                    this.closeUploadModal();
                }, 2000);
            })
        })
            .catch(error => {
                this.close();
            });

    }

    // functions related to Upload  document. ****
    handleChange = (event, type) => {
        let obj;
        if(type=== 'drop') {
            obj = event;
        } else {
            obj = event.target.files
        }
        const fileTypeName = this.state.documentType;
        let reader = new FileReader();
        if(obj && obj.length > 0) {
            this.uploadFile = obj[0];
            let sFileName = obj[0].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            if(sFileExtension !== 'pdf') {
                document.getElementById('uploadBtn').value = "";
                alert(`${fileTypeName} must be in PDF format and smaller than 10MB.`)
                return true;
            }
            //File 10MB limit
            if(this.uploadFile.size <=this.Constants.SIZE_LIMIT) {
                this.setState({
                    uploadDocFile : obj[0],
                    uploadDocSize: (this.uploadFile.size / this.Constants.SIZE_MB).toFixed(2)+' MB',
                    uploadFileName: obj[0].name,
                },()=>{
                    this.clearFile();
                    this.getDocTempFile();
                });
                reader.readAsDataURL(this.uploadFile);
            } else {
                document.getElementById('uploadBtn').value = "";
                alert(`${fileTypeName} should be less than 10 MB.`)
            }
        }
    }

    uploadBtnClick = () => {
        document.getElementById('uploadBtn').click();
    }

    openUploadDoc = () => {
        this.FsnetUtil._openDoc(this.state.uploadDocFileUrl)
    }

    deleteFile = () => {
        this.setState({
            uploadDocFileUrl:null,
            uploadFileName:null,
            uploadDocFile:{},
            fileName: null
        })
    }

    clearFile = () => {
        document.getElementById('uploadBtn').value = "";
    }

    getDocTempFile = () => {
        this.open();
        var formData = new FormData();
        formData.append("fundDoc", this.state.uploadDocFile);
        this.Fsnethttp.getDocTempFile(formData).then(result => {
            this.close();
            this.setState({
                uploadDocFileUrl: result.data.url,
            })
            //this.state.fileName({name:this.state.uploadFileName,docFile:this.state.uploadDocFile,type:'FA'});
            this.setState({
                fileName: this.state.uploadFileName
            })
            console.log(this.setState)
        })
            .catch(error => {
                this.close();
            });
    }

    render() {
        return (
            <div>
                <Modal id="fundAggrementChangeModal" backdrop="static" show={this.state.showConsentUpload}  dialogClassName="confirmFundDialog">
                    <Modal.Body>
                        {
                            <div className="marginTop20 fundAgreementChange marginBot20">
                                <h1 className="title">Replace {this.state.documentType} - {this.props.name}</h1>
                                <div>
                                    <input type="file" id="uploadBtn" className="hide" onChange={this.handleChange} />
                                    <div className="uplodFileContainer width100">
                                        <FileDrop onDrop={(e) => this.handleChange(e, 'drop')}>
                                            <div>
                                                <Button className="uploadBox left" onClick={this.uploadBtnClick}>{this.state.uploadFileName ? 'Replace File':'Upload File'}</Button>
                                            </div>
                                            {
                                                this.state.uploadFileName ?
                                                    <div className="title-md paddingTop10">
                                                        <div className="ellipsis width530" title={this.state.uploadFileName}><a className="cursor" onClick={this.openUploadDoc}>{this.state.uploadFileName}</a></div>
                                                        <div className="opac5">{this.state.uploadDocSize}<i className="fa fa-trash cursor-pointer marginLeft10" onClick={this.deleteFile}></i></div>
                                                    </div>
                                                    :
                                                    <div className="uploadFileText width700">{this.Constants.DROP_YOUR_FILES_FROM_DESKTOP}</div>
                                            }
                                        </FileDrop>
                                    </div>
                                    <Loader isShow={this.state.showModal}></Loader>
                                </div>
                            </div>
                        }

                        <Row className="fundBtnRow">
                            <Col lg={7} md={7} sm={7} xs={12} className="text-center">
                                <Button type="button" className="fsnetCancelButton"  onClick={ () => { this.closeUploadModalPopup(); }}>Cancel</Button>
                            </Col>
                            <Col lg={5} md={5} sm={5} xs={12}>
                                <Button type="button" className={"fsnetSubmitButton " + (this.state.fileName ? 'btnEnabled':'disabled')} onClick={this.uploadDocumentFile}>Save</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
            </div>


        );
    }
}

export default UploadFileModelComponent;


import React, { Component } from 'react';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Row, Col,Table,Button, OverlayTrigger, Tooltip} from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import HeaderComponent from '../../header/header.component';
import homeImage from '../../../images/home.png';
import fundLogoSample from '../../../images/fund_logo_sample.png';
import fundDefault from '../../../images/fund-default.png';
import successImage from '../../../images/success-small.png';
import helpImage from '../../../images/help.png';
import signFundImg from '../../../images/edit-grey.svg';
import copyImage from '../../../images/copy_img.svg';
import handShakeImage from '../../../images/handshake.svg';
import { Route, Link } from "react-router-dom";
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import vanillaLogo from '../../../images/Vanilla-white.png';
import userDefaultImage from '../../../images/default_user.png';
import notificationIcon from '../../../images/notifications.png';
import boxNotificationImage from '../../../images/boxNotiImage.svg';
import LpTableComponent from '../lptable/lptable.component';
import arrowDownImage from '../../../images/arrow-down.svg';


class dashboardComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showModal: false,
            showSideNav: true,
            fundId: null,
            fundData: {},
            summaryData:{},
            rightSideSummaryData:{},
            jsonData:{},
            leftSideSummaryStatus:null
        }

    }

    componentWillUnmount() {

    }

    componentDidMount() {
        let id = this.FsnetUtil._getId();
        //console.log('fund ID::::', id);
        this.setState({
            fundId: id
        }, () => {
            this.getJsonData();
            this.getFundDetails(id);
            this.getSummaryDetails();
            this.getRightSideSummaryDetails('all');
        })
    }

    getJsonData() {
        this.Fsnethttp.getJson('summaryScreen').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getSummaryDetails() {
        
        this.open();
        this.Fsnethttp.fundSummaryDetails(this.state.fundId).then(result => {
            this.close();
            this.setState({
                summaryData:result.data.total,
                leftSideSummaryStatus:result.data.status
            })
        })
        .catch(error => {
            this.close();
        });
    }

    getRightSideSummaryDetails(status) {
        
        this.open();
        this.Fsnethttp.fundRightSideSummaryDetails(this.state.fundId,status).then(result => {
            this.close();
            this.setState({
                rightSideSummaryData:result.data.total
            })
        })
        .catch(error => {
            this.close();
        });
    }


    getFundDetails(fundId) {
        if (fundId) {
            this.open();
            this.Fsnethttp.getFundInfo(fundId).then(result => {
                this.close();
                if (result.data) {
                    this.setState({
                        fundData: result.data.data
                    })
                }
            })
            .catch(error => {
                this.close();
            });
        }
    }
 

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    hamburgerClick() {
        if (this.state.showSideNav == true) {
            this.setState({
                showSideNav: false
            })
        } else {
            this.setState({
                showSideNav: true
            })
        }
    }

    changeSummaryData = (value) => {
        //console.log('*********',value)
        // this.getSummaryDetails(value);
        this.getRightSideSummaryDetails(value);
    }

    
    render() {
        const { match } = this.props;
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                    trigger={['click', 'hover', 'focus']}
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement="left"
                    delayShow={300}
                    delayHide={150}
                >
                    <span>{children}</span>
                </OverlayTrigger>
            );
        }
        return (
            <div className="viewFund">
                <Row className="fNameHeaderMargin">
                    <Col md={6} sm={6}>
                        <Row>
                            <Col md={3}>
                                <img src={this.state.fundData.fundImage ? this.state.fundData.fundImage.url : fundDefault} alt="home_image" height="96" width="96" className="fundImageLarge"/>
                            </Col>
                            <Col md={8} className="fNameText">
                                <p className="fundName">{this.state.fundData.legalEntity}&nbsp;
                                    {/* <span className="statusTxtStyle" hidden={!this.state.fundData.status}>{this.state.fundData.status}</span> */}
                                </p>
                                {/* <p className="fundDate">Fund Start Date: 6/18/2018</p>
                                <p className="fundDate">Fund End Date: 6/18/2021</p> */}
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5}>
                        
                        <div className="fund-progress-bar">
                            <div className="price-percentage" style={{ left: (this.state.fundData['fundTargetCommitmentPercent'] ? this.state.fundData['fundTargetCommitmentPercent']+'%' : '0%') }} hidden={!this.state.fundData['fundTargetCommitment']}>
                                {/* <div className="pull-right bold tPriceStyle price-meter">T: {this.FsnetUtil.convertToCurrency(this.state.fundData['fundTargetCommitment'])}</div> */}
                                <div className="caretDownAlign"><span className="targetLetterSize">T</span><i className="fa fa-caret-down caretDownTarget"></i></div>
                            </div>
                            <div className="progress-bar progress-bar-margin">
                                <div className={"progress progress-green progress-radius-left "+ (!this.state.fundData['closeReadyPercent'] && !this.state.fundData['inProgressPercent'] ? 'progress-radius-left progress-radius-right' : '')} style={{ width: (this.state.fundData['closedPercent'] ? this.state.fundData['closedPercent']+'%' : '0%')}}></div>
                                <div className={"progress progress-yellow "+ (!this.state.fundData['inProgressPercent'] ? 'progress-radius-right ' : ' ') + (!this.state.fundData['closedPercent'] ? 'progress-radius-left' : '')} style={{ width: (this.state.fundData['closeReadyPercent'] ? this.state.fundData['closeReadyPercent']+'%' : '0%')}}></div>
                                <div className={"progress progress-pink progress-radius-right "+ (!this.state.fundData['closeReadyPercent'] && !this.state.fundData['closedPercent'] ? 'progress-radius-left progress-radius-right' : '')} style={{ width: (this.state.fundData['inProgressPercent'] ? this.state.fundData['inProgressPercent']+'%' : '0%')}}></div>
                            </div>
                            <div className="price-percentage" style={{ left: (this.state.fundData['fundHardCapPercent'] ? this.state.fundData['fundHardCapPercent']+'%' : '70%') }} hidden={!this.state.fundData['fundHardCap']}>
                                <div className="caretUpAlign"><i className="fa fa-caret-up"></i><span className="caretUpTarget">HC</span></div>
                                {/* <div className="pull-right bold hcPriceStyle price-meter">HC: {this.FsnetUtil.convertToCurrency(this.state.fundData['fundHardCap'])}</div> */}
                            </div>
                        </div>
                        <div className="bold tPriceStyle price-meter marginTop25">Target (T):&nbsp;<span>{this.state.fundData['fundTargetCommitment'] ? this.FsnetUtil.convertToCurrency(this.state.fundData['fundTargetCommitment']) : '$0'}</span></div>
                        <div className="bold hcPriceStyle price-meter marginTop4">Hard Cap (HC):&nbsp;<span>{this.state.fundData['fundHardCap'] ? this.FsnetUtil.convertToCurrency(this.state.fundData['fundHardCap']) : '$0'}</span></div>
                    </Col>
                    {/* <Col md={1}>
                        <img src={boxNotificationImage} alt="home_image" className="notificationIcon" />
                        <span className="notificationCount">11</span>
                    </Col> */}
                </Row>
                <Row className="fNameHeaderMargin fNameHeaderMarginText summaryContainer">
                {
                    this.state.leftSideSummaryStatus == 1 ?
                    <Col lg={6} md={6} sm={6} xs={12}>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9}>
                                <div className="titleMd titleMdEdit alignRight">In Process Investor Commitments:</div> 
                            </Col>
                            <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                <div className="alertCount">{this.state.summaryData.inProgressCommitment ?this.FsnetUtil.convertToCurrency(this.state.summaryData.inProgressCommitment):this.state.summaryData.inProgressCommitment}</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9}>
                                <div className="titleMd titleMdEdit alignRight">Close-Ready Investor Commitments:</div>
                            </Col>
                            <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                <div className="alertCount">{this.state.summaryData.closedReadyCommitment ?this.FsnetUtil.convertToCurrency(this.state.summaryData.closedReadyCommitment):this.state.summaryData.closedReadyCommitment}</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9}>
                                <div className="titleMd titleMdEdit alignRight">Hypothetical Investor Commitments:</div>
                            </Col>
                            <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                <div className="alertCount">{this.state.summaryData.hypotheticalInvestorCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.hypotheticalInvestorCommitment):this.state.summaryData.hypotheticalInvestorCommitment}</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9}>
                                <div className="titleMd titleMdEdit alignRight">Hypothetical Fund Manager Commitment:</div>
                            </Col>
                            <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                <div className="alertCount">{this.state.summaryData.hypotheticalFundmanagerCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.hypotheticalFundmanagerCommitment):this.state.summaryData.hypotheticalFundmanagerCommitment}</div>
                            </Col>
                        </Row>
                        <hr className="margin5"/>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9}>
                                <div className="titleMd titleMdEdit alignRight">Hypothetical Aggregate Commitments :</div>
                            </Col>
                            <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                <div className="alertCount">{this.state.summaryData.hypotheticalAggregateCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.hypotheticalAggregateCommitment):this.state.summaryData.hypotheticalAggregateCommitment}</div>
                            </Col>
                        </Row>
                    </Col>
                    : 

                    this.state.leftSideSummaryStatus == 2 ?
                        <Col lg={6} md={6} sm={6} xs={12}>
                            <Row>
                                <Col lg={9} md={9} sm={9} xs={9}>
                                    <div className="titleMd titleMdEdit alignRight">In Process Investor Commitments:</div> 
                                </Col>
                                <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                    <div className="alertCount">{this.state.summaryData.inProgressCommitment ?this.FsnetUtil.convertToCurrency(this.state.summaryData.inProgressCommitment):this.state.summaryData.inProgressCommitment}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={9} md={9} sm={9} xs={9}>
                                    <div className="titleMd titleMdEdit alignRight">Close-Ready Investor Commitments:</div>
                                </Col>
                                <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                    <div className="alertCount">{this.state.summaryData.closedReadyCommitment ?this.FsnetUtil.convertToCurrency(this.state.summaryData.closedReadyCommitment):this.state.summaryData.closedReadyCommitment}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={9} md={9} sm={9} xs={9}>
                                    <div className="titleMd titleMdEdit alignRight">Closed Investor Commitments:</div>
                                </Col>
                                <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                    <div className="alertCount">{this.state.summaryData.closedCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.closedCommitment):this.state.summaryData.closedCommitment}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={9} md={9} sm={9} xs={9}>
                                    <div className="titleMd titleMdEdit alignRight">Hypothetical Investor Commitments:</div> 
                                </Col>
                                <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                    <div className="alertCount">{this.state.summaryData.hypotheticalInvestorCommitment ?this.FsnetUtil.convertToCurrency(this.state.summaryData.hypotheticalInvestorCommitment):this.state.summaryData.hypotheticalInvestorCommitment}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={9} md={9} sm={9} xs={9}>
                                    <div className="titleMd titleMdEdit alignRight">Closed Fund Manager Commitment:</div>
                                </Col>
                                <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                    <div className="alertCount">{this.state.summaryData.closedFundmanagerCommitment ?this.FsnetUtil.convertToCurrency(this.state.summaryData.closedFundmanagerCommitment):this.state.summaryData.closedFundmanagerCommitment}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={9} md={9} sm={9} xs={9}>
                                    <div className="titleMd titleMdEdit alignRight">Hypothetical Fund Manager Commitment:</div>
                                </Col>
                                <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                    <div className="alertCount">{this.state.summaryData.hypotheticalFundmanagerCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.hypotheticalFundmanagerCommitment):this.state.summaryData.hypotheticalFundmanagerCommitment}</div>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={9} md={9} sm={9} xs={9}>
                                    <div className="titleMd titleMdEdit alignRight">Closed Aggregate Commitments:</div>
                                </Col>
                                <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                    <div className="alertCount">{this.state.summaryData.closedAggregateCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.closedAggregateCommitment):this.state.summaryData.closedAggregateCommitment}</div>
                                </Col>
                            </Row>
                            <hr className="margin5"/>
                            <Row>
                                <Col lg={9} md={9} sm={9} xs={9}>
                                    <div className="titleMd titleMdEdit alignRight">Hypothetical Aggregate Commitments :</div>
                                </Col>
                                <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                    <div className="alertCount">{this.state.summaryData.hypotheticalAggregateCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.hypotheticalAggregateCommitment):this.state.summaryData.hypotheticalAggregateCommitment}</div>
                                </Col>
                            </Row>
                        </Col>
                    :
                    
                    <Col lg={6} md={6} sm={6} xs={12}>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9}>
                                <div className="titleMd titleMdEdit alignRight">Closed Investor Commitments:</div>
                            </Col>
                            <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                <div className="alertCount">{this.state.summaryData.closedCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.closedCommitment):this.state.summaryData.closedCommitment}</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9}>
                                <div className="titleMd titleMdEdit alignRight">Closed Fund Manager Commitment:</div>
                            </Col>
                            <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                <div className="alertCount">{this.state.summaryData.closedFundmanagerCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.closedFundmanagerCommitment):this.state.summaryData.closedFundmanagerCommitment}</div>
                            </Col>
                        </Row>
                        <hr className="margin5"/>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9}>
                                <div className="titleMd titleMdEdit alignRight">Closed Aggregate Commitments :</div>
                            </Col>
                            <Col lg={3} md={3} sm={3} xs={3} className="paddingZero">
                                <div className="alertCount">{this.state.summaryData.closedAggregateCommitment?this.FsnetUtil.convertToCurrency(this.state.summaryData.closedAggregateCommitment):this.state.summaryData.closedAggregateCommitment}</div>
                            </Col>
                        </Row>
                    </Col>
                }
                
                
                    <Col lg={5} md={5} sm={5} xs={12}>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9} className="">
                                <div className="titleMd titleMdEdit">1940 Act Investor Count: </div>
                            </Col>
                            <Col lg={2} md={2} sm={2} xs={2} className="paddingNone">
                                <div className="alertCount">{this.state.rightSideSummaryData.beneficalOwnerCount}</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9} className="">
                                <div className="titleMd titleMdEdit">Total Investor Count: </div>
                            </Col>
                            <Col lg={2} md={2} sm={2} xs={2} className="paddingNone">
                                <div className="alertCount">{this.state.rightSideSummaryData.investorCount || this.state.rightSideSummaryData.investorCount ==0? this.state.rightSideSummaryData.investorCount:1}</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9} className="">
                                <div className={"titleMd titleMdEdit "+ (this.state.rightSideSummaryData.erisaParticipationCount > 25 ? 'redColor' : '')}>Total ERISA Participation %:</div>
                            </Col>
                            <Col lg={2} md={2} sm={2} xs={2} className="paddingNone">
                                <div className={"alertCount "+ (this.state.rightSideSummaryData.erisaParticipationCount > 25 ? 'redColor' : '')}>{this.state.rightSideSummaryData.erisaParticipationCount ? this.state.rightSideSummaryData.erisaParticipationCount+'%':0}
                                </div>
                            </Col>
                            {
                                this.state.rightSideSummaryData.erisaParticipationCount > 25 &&
                                    <LinkWithTooltip tooltip={this.state.jsonData.ERISA_TOOLTIP_TEXT}>
                                        <i className="fa fa-exclamation-circle toolTipIconAlign" aria-hidden="true"></i>
                                    </LinkWithTooltip>
                            }
                           
                        </Row>
                        <Row>
                            <Col lg={9} md={9} sm={9} xs={9}>
                                <div className={"titleMd titleMdEdit "+ ((this.state.rightSideSummaryData.qualifiedPurchaser == '3(c)(1)' &&  this.state.rightSideSummaryData.beneficalOwnerCount > 99 ) ||  (this.state.rightSideSummaryData.qualifiedPurchaser == '3(c)(7)' &&  this.state.rightSideSummaryData.beneficalOwnerCount > 499 ) ? 'redColor' : '')}>Securities Exemption Eligibility:</div>
                            </Col>
                            <Col lg={2} md={2} sm={2} xs={2} className="paddingNone">
                                <div className={"alertCount "+ ((this.state.rightSideSummaryData.qualifiedPurchaser == '3(c)(1)' &&  this.state.rightSideSummaryData.beneficalOwnerCount > 99 ) ||  (this.state.rightSideSummaryData.qualifiedPurchaser == '3(c)(7)' &&  this.state.rightSideSummaryData.beneficalOwnerCount > 499 ) ? 'redColor' : '')}>{this.state.rightSideSummaryData.qualifiedPurchaser}
                                </div>
                            </Col>
                            {
                                ((this.state.rightSideSummaryData.qualifiedPurchaser == '3(c)(1)' &&  this.state.rightSideSummaryData.beneficalOwnerCount > 99 ) ||  (this.state.rightSideSummaryData.qualifiedPurchaser == '3(c)(7)' &&  this.state.rightSideSummaryData.beneficalOwnerCount > 499 ) ) &&
                                    <LinkWithTooltip tooltip={this.state.jsonData.SECURITY_EXEMPTION_TOOLTIP_TEXT}>
                                        <i className="fa fa-exclamation-circle toolTipIconAlign" aria-hidden="true"></i>
                                    </LinkWithTooltip>
                            }
                        </Row>
                    </Col>
                </Row>
                <LpTableComponent fundId={this.state.fundId} getFilter={this.changeSummaryData}></LpTableComponent>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default dashboardComponent;


import React, { Component } from 'react';
import './lptable.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Constants } from '../../../constants/constants';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { Row, Col, Table, Button, FormControl, Modal, OverlayTrigger, Tooltip,Checkbox as CBox } from 'react-bootstrap';
import userDefaultImage from '../../../images/default_user.png';
import editImage from '../../../images/edit.svg';
import popoutButton from '../../../images/popout-button.svg';
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../../util/util';
import profilePictureLarge from '../../../images/profile-picture-large.svg';
import overFlow from '../../../images/overflow.svg';
import $ from 'jquery';
var _ = require('lodash');

var fundInfo,subSignwin = {}, zindexValue = '-1';
class LpTableComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            lpProfileData: {},
            showModal: false,
            showSideNav: true,
            fundId: null,
            status: 0,
            offset: 0,
            pageCount: 20,
            isExpanded: false,
            orderCol: 'firstName',
            showDisclosureModal: false,
            order: 'asc',
            ccIncreaseShow: false,
            showProceed: false,
            hasSignature: false,
            sideLetterModal: false,
            sideLetterDocFile: {},
            sideLetterFileName: null,
            sideLetterErrorMsz: null,
            tableHeader: [
                { 'key': 'Investor Name', 'property': 'firstName', 'direction': null },
                { 'key': 'Signatory', 'property': 'firstName1', 'direction': null },
                { 'key': 'Investor Type', 'property': 'investorType', 'direction': null },
                { 'key': 'Investor Sub Type', 'property': 'investorSubType', 'direction': null },
                { 'key': 'Capital Commitment Offered', 'property': 'lpCapitalCommitment', 'direction': null },
                { 'key': 'Capital Commitment Accepted', 'property': 'gpConsumed', 'direction': null },
                { 'key': 'Prior Commitment Amounts', 'property': 'commitmentHistoryPretty', 'direction': null },
                { 'key': '1940 Act Investor Count', 'property': 'investorCount', 'direction': null },
                { 'key': 'Place of Residence', 'property': 'placeOfResidence', 'direction': null },
                { 'key': 'Place of Organization', 'property': 'placeOfOrganization', 'direction': null },
                { 'key': 'Investors Addresses', 'property': 'investorAddress', 'direction': null },
                { 'key': '% of the Fund', 'property': 'fundPercentage', 'direction': null },
                { 'key': 'Accredited Investor', 'property': 'areYouAccreditedInvestor', 'direction': null },
                { 'key': 'Qualified Purchaser', 'property': 'areYouQualifiedPurchaser', 'direction': null },
                { 'key': 'Qualified Client', 'property': 'areYouQualifiedClient', 'direction': null },
                { 'key': 'Investment Company Act Matters', 'property': 'companiesAct', 'direction': null },
                { 'key': 'No. of Beneficial Owners', 'property': 'numberOfDirectEquityOwners', 'direction': null },
                { 'key': 'Look-Through Issues', 'property': 'lookThorughIssues', 'direction': null },
                { 'key': 'ERISA Representation', 'property': 'erisaPlan', 'direction': null },
                { 'key': 'Benefit Plan %', 'property': 'totalValueOfEquityInterests', 'direction': null },
                { 'key': 'FOIA and Similar Disclosure Obligations', 'property': 'releaseInvestmentEntityRequired', 'direction': null },
                { 'key': 'Disqualifying Event', 'property': 'isSubjectToDisqualifyingEvent', 'direction': null },
                { 'key': 'Special Disclosures', 'property': 'fundManagerInfo', 'direction': null },
                { 'key': 'Other Investor Attributes', 'property': 'otherInvestorAttr', 'direction': null },
                { 'key': 'Closed Time', 'property': 'lpClosedDate', 'direction': null },

            ],
            notSortedColumns: ['otherInvestorAttr', 'companiesAct', 'lookThorughIssues', 'erisaPlan'],
            filterArr: ['all', 'closed', 'closeReady', 'open', 'new', 'rejected', 'notInterested'],
            fundDetailList: [],
            gpDelegatesSelectedUsers: false,
            lpNameProfileModal: false,
            lpProfileFundsModal: false,
            lpMessageModal: false,
            lpMessageText: '',
            amount: null,
            amountCurrency: '',
            hideSendMessage: false,
            selectedLpData: { lp: {} },
            modalTitle: '',
            modalContent: [],
            signatureModal: false,
            enableSignatureBtn: false,
            signaturePassword: '',
            fundCommitmentEnvelopeId: '',
            sendMessageError: null,
            loggedInUserName: this.FsnetUtil.userName(),
            accountType: null,
            sendMessageUrl: this.Constants.URL,
            offlineLpDocModal: false,
            documentUploadType: null,
            capitalCommitmentFile:{},
            offlineExcutedDocuments:[],
            amendmentsList:[],
            showAmendments:false,
            selectedAmendments:[],
            validOfflineUploadDocs:false
        }
    }

    numbersOnly = (event) => {
        if (((event.which != 46 || (event.which == 46 && event.target.value == '')) ||
            event.target.value.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    }

    handleChangeCapitalEvent = (e) => {
        const allowNumberRegex = this.Constants.NUMBER_REGEX;
        // if value is not blank, then test the regex
        if (e.target.value.trim() === '' || allowNumberRegex.test(e.target.value)) {
            this.setState({ amount: e.target.value.trim() !== '' ? e.target.value.trim() : e.target.value, amountCurrency: e.target.value.trim() !== '' ? e.target.value.trim() : e.target.value })
        }

    }

    componentDidMount() {
        if (this.FsnetAuth.isAdmin()) {
            this.setState({
                accountType: this.FsnetUtil.getUserRole()
            })
        } else {
            this.setState({
                accountType: this.FsnetUtil.getAdminRole()
            })
        }
        var url = window.location.href;
        var parts = url.split("/");
        var urlString = parts[parts.length - 2];
        if (parts.indexOf('expandTable') > -1) {
            this.setState({
                isExpanded: true
            })
        } else {
            this.setState({
                isExpanded: false
            })
        }
        $('tbody').scroll(function (e) { //detect a scroll event on the tbody
            /*
          Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
          of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.    
          */
            $('thead').css("left", -$("tbody").scrollLeft()); //fix the thead relative to the body scrolling
            $('thead th:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first cell of the header
            $('tbody td:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first column of tdbody
        });
        // element should be replaced with the actual target element on which you have applied scroll, use window in case of no target element.
        var lastScrollLeft = 0;
        var lastScrollTop = 0;
        this.refs.fundScroll.addEventListener('scroll', () => {
            var documentScrollLeft = this.refs.fundScroll.scrollLeft;
            var documentScrollTop = this.refs.fundScroll.scrollTop;
            if (lastScrollLeft != documentScrollLeft) {
                lastScrollLeft = documentScrollLeft;
            } else if ((lastScrollTop != documentScrollTop) && (this.refs.fundScroll.scrollTop + this.refs.fundScroll.clientHeight >=
                this.refs.fundScroll.scrollHeight) && !this.state.showModal && this.state.fundDetailList.length) {
                if (!this.state.finishedScroll) {
                    this.getLazyLoadedFundDetails(true);
                }
            }
        })
    }

    //Open Doc
    openViewPrintDoc = (data) => {
        if (data.data.documentsForSignature.length > 0) {
            let docs = data.data.documentsForSignature;
            docs = docs.filter(document => (document.docType !== 'FUND_AGREEMENT' && document.docType !== 'ADDENDUM_AGREEMENT'));
            for (let idx = 0; idx < docs.length; idx++) {
                if (docs[idx]['docType'] === 'SUBSCRIPTION_AGREEMENT' && docs[idx]['isSigned'] === true && idx === (docs.length - 1)) {

                    this.open();
                    this.Fsnethttp.getAggrement(docs[idx].envelopeId, data.data.id).then(result => {
                        this.close();
                        if (result.data && result.data.url) {
                            const docUrl = `${result.data.url}?token=${this.FsnetUtil.getToken()}`
                            this.FsnetUtil._openDoc(docUrl)
                        }
                    })
                        .catch(error => {
                            this.close();
                        });
                }
            }
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }
    ccIncreaseShow = () => {
        this.setState({ ccIncreaseShow: true });
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('profileModal', data);
    }

    lpProfileFunds = () => {
        this.setState({ lpProfileFundsModal: true });
    }

    handleLpProfileFundsCLose = () => {
        this.setState({ lpProfileFundsModal: false });
    }


    closeCancelCapitalCommitmentModal = () => {
        this.setState({cancelCapitalCommitmentModal:false,cancelCommitmentId:null})
    }

    openCancelCapitalCommitmentModal = (data) => () => {
        this.setState({
            cancelCommitmentId:data.id,
            cancelCapitalCommitmentModal:true
        })
    }

    cancelCapitalCommitment = () => {
        this.open();
        this.Fsnethttp.gpCancelCommitmentChange(this.state.cancelCommitmentId).then(result => {
            this.close();
            this.closeCancelCapitalCommitmentModal();
            PubSub.publish('toastNotification', { showToast: true, toastMessage: 'Capital Commitment to the investor has been cancelled succesfully.', toastType: 'success' });
            setTimeout(() => {
                window.location.reload();
            }, 1000);

        })
        .catch(error => {
            this.close();
        });
    }

    handlelpMessageModalClose = () => {
        this.setState({ lpMessageModal: false, lpProfileData: {}, amount: '', lpMessageText: '', selectedLpData: { lp: {} }, amountCurrency: '', fundCommitmentErrorMsz: '', sendMessageError: null });
    }
    handlelpMessageModalShow = (e, data, modalName) => {
        this.setState({
            sendMessageUrl: data.data.lp && !data.data.lp.isEmailConfirmed && data.data.lp.emailConfirmCode != null ? `${this.Constants.URL}/register/${data.data.lp.emailConfirmCode}` : this.Constants.URL
        }, () => {
            if (modalName === 'lpSendMessage') {
                this.setState({
                    hideSendMessage: false
                })
            } else {
                this.setState({
                    hideSendMessage: true
                })
            }
            this.setState({
                selectedLpData: data.data
            })
            this.getPublicProfile(data.data.lp, 'lpMessageModal');
        })

    }

    resendInvite = (data) => {
        const obj = { subscriptionId: data.id, fundId: data.fundId };
        this.open();
        this.Fsnethttp.reInviteLPToFund(obj).then(result => {
            this.close();
            PubSub.publish('toastNotification', { showToast: true, toastMessage: 'Re Invite to the investor has been sent succesfully.', toastType: 'success' });
            setTimeout(() => {
                window.location.reload();
            }, 2000);

        })
            .catch(error => {
                this.close();
            });
    }

    notifyMessageToLp = () => {
        let text = $('.lpMessageTextBox').html();
        if (text.length > 0) {
            text = `Hello ${this.FsnetUtil.getFullName(this.state.selectedLpData.lp)}, <br/><br/>This message was sent by ${this.FsnetUtil.getFullName(this.state.selectedLpData.fund.gp)} in respect of ${this.state.selectedLpData.fund.legalEntity}. <br/><br/>` + text + `<br/><br/><a href=${this.state.sendMessageUrl}>Click here</a> to access your Vanilla account.<br/><br/>Thank you for your support.<br/><br/>Best regards,<br/>The Vanilla Team`;
            let obj = { message: text, toUserID: this.state.lpProfileData.id }
            this.open();
            this.Fsnethttp.sendMessage(obj).then(result => {
                this.close();
                if (result.data) {
                    this.handlelpMessageModalClose();
                }
            })
                .catch(error => {
                    this.close();
                });
        } else {
            this.setState({
                sendMessageError: this.Constants.SEND_MESSAGE_ERROR
            })
        }
    }

    changeCurrentFundCommitment = () => {
        if (parseFloat(this.state.amount) < this.state.selectedLpData.gpConsumed) {
            this.setState({
                fundCommitmentErrorMsz: this.Constants.NEW_FUND_COMMITMENT
            })
        } else {
            
            if (this.state.selectedLpData.lp.accountType == 'OfflineLP') {
                if(!this.state.capitalCommitmentFileName) {
                    this.setState({
                        fundCommitmentErrorMsz: 'Please upload capital commitment document.'
                    })
                } else {
                    var formData = new FormData();
                    formData.append("fundId", this.state.selectedLpData.fundId);
                    formData.append("subscriptionId", this.state.selectedLpData.id);
                    formData.append("capital", this.state.capitalCommitmentFile);
                    formData.append("amount", parseFloat(this.state.amount));
                    formData.append("date", new Date());
                    formData.append("timeZone", Intl.DateTimeFormat().resolvedOptions().timeZone);
                    this.open();
                    this.Fsnethttp.offlineChangeCommitment(formData).then(result => {
                        this.close();
                        PubSub.publish('toastNotification', { showToast: true, toastMessage: 'The offer to increase the Capital Commitment has been approved.', toastType: 'success' });
                        if (result.data) {
                            this.handlelpMessageModalClose();
                            setTimeout(() => {
                                window.location.reload();
                            }, 1000);
                        }
                    })
                    .catch(error => {
                        this.close();
                        this.setState({
                            fundCommitmentErrorMsz: error.response.data.errors[0].msg
                        })
                    });
                }
                
            } else {
                this.open();
                let obj = { fundId: this.state.selectedLpData.fundId, subscriptionId: this.state.selectedLpData.id, lpId: this.state.selectedLpData.lp.id, amount: parseFloat(this.state.amount), comments: this.state.lpMessageText }
                this.Fsnethttp.lpCommitmentChange(obj).then(result => {
                    this.close();
                    PubSub.publish('toastNotification', { showToast: true, toastMessage: 'The offer to increase the Capital Commitment has been sent to the investor for signature.', toastType: 'success' });
                    if (result.data) {
                        this.handlelpMessageModalClose();
                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);
                    }
                })
                    .catch(error => {
                        this.close();
                        this.setState({
                            fundCommitmentErrorMsz: error.response.data.errors[0].msg
                        })
                    });
            }

        }
    }

    filterChange = (e) => {
        if (this.refs.fundScroll.scrollLeft > 0) {
            this.refs.fundScroll.scrollLeft = 0;
        }
        this.filterFun(e.target.value)
        this.props.getFilter(e.target.value);
    }

    hideDisclosureModal = () => {
        zindexValue = 1;
        this.setState({
            showDisclosureModal: false
        })
    }

    viewPatnerShipDoc = (e, data) => {
        const docUrl = `${this.Constants.BASE_URL}document/view/fa/${data.data.id}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    viewSubscriptionDoc = (e, data) => {
        const docUrl = `${this.Constants.BASE_URL}document/view/sa/${data.data.id}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    editSubDoc = (e, data) => {
        const url = '/fundsetup/editSubForm/' + this.FsnetUtil._encrypt(data.data.fundId) + '&id=' + data.data.id
        window.location.href = url
    }

    counterSignSideLetter = () => {
        let obj = { documentId: this.state.fundCommitmentEnvelopeId, date: new Date(), timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone }
        this.open();
        this.Fsnethttp.signSideLetter(obj).then(result => {
            this.close();
            if (result.data) {
                var tableHeader = this;
                this.handleSignatureClose();
                const docUrl = `${result.data.url}?token=${this.FsnetUtil.getToken()}`
                this.FsnetUtil._openDoc(docUrl)
                setTimeout(() => {
                    window.location.href = '/fund/view/' + tableHeader.state.fundId
                }, 1000);
            }
        })
            .catch(error => {
                this.close();
            });
    }

    counterSignCaptialCommitment = (proceed) => {
        if (this.state.fundCommitmentEnvelopeId) {

            let obj = { documentId: this.state.fundCommitmentEnvelopeId, proceed: proceed, date: new Date(), timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone }
            this.open();
            this.Fsnethttp.signFundCommitment(obj).then(result => {
                this.close();
                if (result.data) {
                    var tableHeader = this;
                    this.handleSignatureClose();
                    const docUrl = `${result.data.url}?token=${this.FsnetUtil.getToken()}`
                    this.FsnetUtil._openDoc(docUrl)
                    setTimeout(() => {
                        window.location.href = '/fund/view/' + tableHeader.state.fundId
                    }, 1000);
                }
            })
                .catch(error => {
                    this.close();
                    if (error.response) {
                        this.setState({
                            ccIncreaseShow: true,
                            ccErrorMsz: error && error.response && error.response.data && error.response.data.errors.CODE === 'CAPITAL_COMMITMENT_ERROR' ? error.response.data.errors.msg : error.response.data.errors[0].msg
                        });
                        if (error && error.response && error.response.data && error.response.data.errors[0] && error.response.data.errors[0].proceed == true) {
                            this.setState({
                                showProceed: true,
                                signatureModal: false,
                            })
                        } else {
                            this.setState({
                                showProceed: false,
                                signatureModal: false,
                            })
                        }
                    }
                });
        }
    }

    handleSignatureShow = (data,fromAlert) => {
        if (data.changeCommitmentAgreement && data.changeCommitmentAgreement.isCurrentUserSigned) {
            const docUrl = `${data.changeCommitmentAgreement.filePathTempPublicUrl}?token=${this.FsnetUtil.getToken()}`
            this.FsnetUtil._openDoc(docUrl)
        } else {
            if (this.state.accountType !== 'FSNETAdministrator') {
                this.checkSignature(data,'capitalCommitment')
            }
        }
    }

    sideLetterSignature = (data) => () => {
        if (data.sideletterAgreement && data.sideletterAgreement.isCurrentUserSigned) {
            const docUrl = `${data.sideletterAgreement.filePathTempPublicUrl}?token=${this.FsnetUtil.getToken()}`
            this.FsnetUtil._openDoc(docUrl)
        } else {
            if (this.state.accountType !== 'FSNETAdministrator') {
                this.checkSignature(data, 'sideLetter')
            }
        }
    }

    handleSignatureClose = () => {
        this.setState({
            signatureModal: false,
            fundCommitmentEnvelopeId: '',
            signaturePassword: '',
            enableSignatureBtn: false,
            signType: null
        })
    }

    ccIncreaseClose = () => {
        this.setState({
            ccIncreaseShow: false,
        });
        this.handleSignatureClose();
    }

    ccIncreaseProceed = () => {
        this.counterSignCaptialCommitment(true);
        this.setState({
            ccIncreaseShow: false,
        });
        this.handleSignatureClose();
    }

    handleSingaturePwd = (e) => {
        if (e.target.value.trim() !== '') {
            this.setState({
                signaturePassword: e.target.value.trim(),
                enableSignatureBtn: true
            })
        } else {
            this.setState({
                enableSignatureBtn: false
            })
        }
    }

    verifySignature = () => {
        this.open();
        let obj = { signaturePassword: this.state.signaturePassword }
        this.Fsnethttp.verifySignature(obj).then(result => {
            this.close();
            if (this.state.signType == 'sideLetter') {
                this.counterSignSideLetter();
            } else {
                this.counterSignCaptialCommitment(false);
            }
            this.setState({
                signatureModal: false
            })
        })
            .catch(error => {
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        signatureErrorMsz: error.response.data.errors[0].msg,
                        showModal: false
                    });
                }
            });
    }

    openDisclosureModal = (type, data, id) => {
        zindexValue = -1;
        if (id === 'otherInvestorAttribute') {
            if (data.data.otherInvestorAttributes && data.data.otherInvestorAttributes.length) {
                this.setState({
                    showDisclosureModal: true,
                    modalTitle: type,
                    modalContent: data.data.otherInvestorAttributes.join(', ')
                })
            }
        } else if (id === 'investorAttribute') {
            if (data.data.fundManagerInfo) {
                this.setState({
                    showDisclosureModal: true,
                    modalTitle: 'Special Disclosures',
                    modalContent: data.data.fundManagerInfo
                })
            }
        } else if (id === 'question') {
            if (data.record.questionResponse && data.record.typeOfQuestion === 1) {
                this.setState({
                    showDisclosureModal: true,
                    modalTitle: data.record.questionTitle,
                    modalContent: data.record.questionResponse
                })
            }
        } else if (id === 'commitmentHistoryPretty') {
            if (data.data.commitmentHistoryPretty && data.data.commitmentHistoryPretty.length > 0) {
                this.setState({
                    showDisclosureModal: true,
                    modalTitle: type,
                    modalContent: data.data.commitmentHistoryPretty
                })
            }
        } else if (id === 'investorAddress') {
            if (data.data.investorAddress) {
                this.setState({
                    showDisclosureModal: true,
                    modalTitle: type,
                    modalContent: data.data.investorAddress
                })
            }
        }

    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(fundInfo);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.fundId !== this.props.fundId) {
            this.setState({
                fundId: nextProps.fundId,
                status: 'all', //Default show closed status users only.
                offset: 0,
                orderCol: 'firstName',
                order: 'asc'
            }, () => {
                this.getDetails()
            });
        }
    }

    getLazyLoadedFundDetails = () => {

        let fundId = this.state.fundId;
        let params = {
            fundId: this.state.fundId,
            status: this.state.status,
            offset: this.state.offset,
            orderCol: this.state.orderCol,
            order: this.state.order
        }
        if (fundId) {
            this.open();
            this.Fsnethttp.getFundDetails(params).then(result => {
                this.close();
                if (result.data) {
                    this.setState({
                        offset: this.state.offset + result.data.data.length,
                        fundDetailList: this.state.fundDetailList.concat(result.data.data)
                    }, () => {
                        this.setState({
                            finishedScroll: result.data.itemCount < 6 ? true : false
                        })
                    })
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    getFundDetails = (lazyLoaded) => {

        let fundId = this.state.fundId;
        let params = {
            fundId: this.state.fundId,
            status: this.state.status,
            offset: this.state.offset,
            orderCol: this.state.orderCol,
            order: this.state.order
        }
        if (fundId) {
            this.open();
            this.Fsnethttp.getFundDetails(params).then(result => {
                this.close();
                if (result.data) {
                    zindexValue = 1;
                    if (result.data.data.length) {
                        let dataArr = [];
                        for (let i = 0; i < 10; i++) {
                            dataArr.push(result.data.data[0]);
                        }
                        this.setState({
                            offset: this.state.offset + result.data.data.length,
                            fundDetailList: result.data.data,
                            fundDetailsEmpty: false
                        }, () => {
                            this.addQuestionsHeader();
                            this.openDocFromAlert();
                            this.setState({
                                finishedScroll: result.data.itemCount < 6 ? true : false
                            })
                        })
                    } else {
                        this.setState({
                            fundDetailsEmpty: true
                        })
                    }
                }
            })
                .catch(error => {
                    this.close();
                    zindexValue = 1;
                });
        }
    }

    filterFun = (statusId) => {
        this.setState({
            status: statusId ? statusId : 'all',
            offset: 0
        }, () => {
            this.getFundDetails();
        })

    }

    getPublicProfile = (user, type) => {
        if (user) {

            this.open();
            this.Fsnethttp.getPublicProfile(user).then(result => {
                this.close();
                if (result.data) {
                    this.setState({
                        lpProfileData: result.data.data,
                        [type]: true
                    })
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    getDetails = () => {
        this.getFundDetails();
    }

    addQuestionsHeader = () => {
        let headers = this.state.tableHeader;
        if (this.state.fundDetailList && this.state.fundDetailList.length > 0) {
            for (let index of this.state.fundDetailList[0].investorQuestions) {
                let obj = {};
                obj['key'] = index['questionTitle'];
                obj['property'] = 'questions';
                obj['direction'] = '';
                headers.push(obj)
            }
            this.setState({
                tableHeader: headers
            })
        }
    }

    openDocFromAlert = () => {
        const id = this.FsnetUtil.checkEnvelopNameExists();
        if (id !== -1) {
            const type = this.FsnetUtil.getType();
            const subscriptionId = this.FsnetUtil.getAlertId();
            if (type === 'SUBSCRIPTION_AGREEMENT') {
                let getObj = this.state.fundDetailList.filter(obj => obj.documentsForSignature && obj.documentsForSignature.length > 0 && obj.documentsForSignature[0].subscriptionId == subscriptionId);
                if (getObj[0]) {
                    let getSubScriptionDocs = getObj[0].documentsForSignature.filter(doc => doc.docType === 'SUBSCRIPTION_AGREEMENT');
                    const docUrl = `${getSubScriptionDocs[0].filePathTempPublicUrl}?token=${this.FsnetUtil.getToken()}`
                    this.FsnetUtil._openDoc(docUrl)
                    window.location.href = `/fund/view/${this.state.fundDetailList[0].fundId}`;
                }
            } else if (['capitalCommitment','CHANGE_COMMITMENT_AGREEMENT','SIDE_LETTER_AGREEMENT'].indexOf(type) > -1) {
                const subscriptionId = this.FsnetUtil.getAlertId()
                this.setState({
                    subscriptionId:subscriptionId
                },()=>{
                    let filterData = this.state.fundDetailList.filter(obj => obj.id == this.FsnetUtil._decrypt(subscriptionId))
                    let newData  = filterData[0]
                    type == 'SIDE_LETTER_AGREEMENT' ? this.sideLetterSignature(newData)() : this.handleSignatureShow(newData,false)
                })
            } 
        }

    }

    openFundDetailsModal = () => {
        const docUrl = `/fund/expandTable/${this.state.fundId}`
        this.FsnetUtil._openNewTab(docUrl)
    }

    sortViewFund = (header) => () => {
        const noSortFileds = ['firstName1', 'firstName', 'questions'];
        const isSort = noSortFileds.indexOf(header.property)
        if (this.state.fundDetailList && this.state.fundDetailList.length && isSort == -1) {
            let tableHeaders = this.state.tableHeader;
            let selectedIdx;
            tableHeaders.forEach((header1, idx) => {
                if (header1.property === header.property) {
                    header1.direction = header1.direction && header1.direction == 'asc' ? 'desc' : 'asc';
                    selectedIdx = idx;
                }
                if (header1.property !== header.property) {
                    header1.direction = null;
                }
            });
            this.setState({
                tableHeader: tableHeaders,
                orderCol: tableHeaders[selectedIdx] ? tableHeaders[selectedIdx].property : 'firstName',
                offset: 0,
                order: tableHeaders[selectedIdx] ? tableHeaders[selectedIdx].direction : 'asc'
            }, () => {
                this.getFundDetails();
            })
        }
    }

    sendReminder = (lpId) => {

        let postObj = { fundId: this.state.fundId, lpId: lpId };
        this.open()
        this.Fsnethttp.sendReminder(postObj).then(result => {
            this.close();
            if (result) {
                // this.handlefundClose(true);
                PubSub.publish('toastNotification', { showToast: true, toastMessage: 'Reminder sent successfully', toastType: 'success' });
            }
        })
            .catch(error => {
                this.close();
                PubSub.publish('toastNotification', { showToast: true, toastMessage: 'Error while sending reminder', toastType: 'danger' });
                // this.handlefundClose(true);
            });
    }

    addedQuestionAlignment = () => {
        $('th.questionRow').each(function () {
            var tableHeader = $(this);
            $('td.questionRow').each(function () {
                var tableRow = $(this);
                if (tableRow.attr('header') == tableHeader.attr('title') && (tableRow.attr('title').length > 20 || tableHeader.attr('title').length > 20)) {
                    tableRow.removeClass('minWidth100');
                    tableHeader.removeClass('minWidth100');
                } else if (tableRow.attr('header') == tableHeader.attr('title') && (tableRow.attr('title').length < 20 && tableHeader.attr('title').length < 20)) {
                    tableRow.addClass('minWidth100');
                    tableHeader.addClass('minWidth100');
                }
            })
        })

    }

    counterSignCaptialCommitmentThroughAlert = (data) => {
        this.setState({
            hasSignature: true,
            signatureModal: true,
            signType: 'capitalCommitment',
            fundCommitmentEnvelopeId: data.envelopeId
        })
    }

    checkSignature = (data, type) => {
        this.open();
        this.Fsnethttp.getUserProfile(data.lp.id).then(result => {
            this.close();
            this.setState({
                hasSignature: result.data.signaturePic ? true : false,
                signType: type == 'sideLetter' ? 'sideLetter' : 'capitalCommitment'
            }, () => {
                this.setState({
                    signatureModal: result.data.signaturePic ? false : true
                },()=>{
                    if(result.data.signaturePic) {
                        var $this = this;
                        const documentId = this.state.signType === 'sideLetter' ? data.sideletterAgreement.id : data && data.type === 'capitalCommitment' ? data.envelopeId : data.changeCommitmentAgreement.id;
                        const subscriptionId = data && data.id ? data.id:this.state.subscriptionId;
                        const docType = this.state.signType == 'sideLetter' ? 'SA' : 'CA';
                        const url = this.FsnetUtil._isIEEdge() ? `/counterSign/${this.FsnetUtil._encrypt(this.state.fundId)}?token=${this.FsnetUtil.getToken()}&documentId=${this.FsnetUtil._encrypt(documentId)}&subscriptionId=${this.FsnetUtil._encrypt(subscriptionId)}&type=${docType}`:`/counterSign/${this.FsnetUtil._encrypt(this.state.fundId)}?documentId=${this.FsnetUtil._encrypt(documentId)}&subscriptionId=${this.FsnetUtil._encrypt(subscriptionId)}&type=${docType}`
                        subSignwin = window.open(url, '_blank', 'width = 1200px, height = 800px');
                        var timer = setInterval(function () {
                            if (subSignwin && subSignwin.closed) {
                                const value = localStorage.getItem('signed')
                                clearInterval(timer);
                                localStorage.removeItem('signed')
                                if(value || $this.FsnetUtil._isIEEdge()) {
                                    window.location.href = '/fund/view/'+$this.FsnetUtil._encrypt($this.state.fundId)
                                }
                            }
                        }, 1000);
                    }
                })
            })
        })
        .catch(error => {
            this.close();
        });
    }

    sideLetters = (data) => () => {
        this.setState({
            selectedLpData: data
        }, () => {
            this.getPublicProfile(data.lp, 'sideLetterModal');
        })
    }

    deleteFile = (type) => () => {
        this.setState({
            [type + 'DocFile']: {},
            [type + 'FileName']: null,
        }, () => {
            this.clearUploadValue();
            if(['capitalCommitment'],['sideLetter'].indexOf(type) === -1) {
                this.checkValidToEnableOfflineButton(type);
            }
        })
    }

    handleSideLetterModalClose = () => { this.setState({ sideLetterModal: false, lpProfileData: {}, selectedLpData: { lp: {} }, sideLetterFileName: null, sideLetterDocFile: {}, sideLetterErrorMsz: null }) }
    handleSideLetterModalOpen = () => { this.setState({ sideLetterModal: true }) }

    submitSideLetterFn = () => {
        var formData = new FormData();
        formData.append("fundId", this.state.selectedLpData.fundId);
        formData.append("subscriptionId", this.state.selectedLpData.id);
        formData.append("file", this.state.sideLetterFile);
        this.open();
        this.Fsnethttp.uploadSideLetter(formData, this.state.selectedLpData.lp.accountType).then(result => {
            this.close();
            this.handleSideLetterModalClose();
            PubSub.publish('toastNotification', { showToast: true, toastMessage: 'Side Letter has been uploaded succesfully.', toastType: 'success' });
            setTimeout(() => {
                window.location.reload();
            }, 1000);
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        sideLetterErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        sideLetterErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });

                }
            });
    }

    clearUploadValue = () => {
        if(document.getElementById('uploadBtn')) {
            document.getElementById('uploadBtn').value = "";
        }
    }

    uploadDoc = (type) => () => {
        this.setState({
            documentUploadType: type,
        }, () => {
            document.getElementById('uploadBtn').click();

        })
    }

    openUploadDoc = (type) => () => {
        this.FsnetUtil._openDoc(this.state[type+'FileUrl'])
    } 

    //Upload Side Letter document.
    handleChange = (event, type, docType) => {
        let obj;
        if (type === 'drop') {
            obj = event;
        } else {
            obj = event.target.files
        }
        const fileTypeName = docType == 'sideLetter' ? 'Side Letter document' : docType == 'amendment' ? 'Amendment document' : docType == 'fundDocument' ? 'Executed Fund document' : docType === 'capitalCommitment' ? 'Executed Capital Commitment document': 'Executed Subscription document'
        let reader = new FileReader();
        if (obj && obj.length > 0) {
            this.uploadFile = obj[0];
            let sFileName = obj[0].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            if (sFileExtension !== 'pdf') {
                this.clearUploadValue();
                alert(`${fileTypeName} must be in PDF format and smaller than 10MB.`)
                return true;
            }
            //File 10MB limit
            if (this.uploadFile.size <= this.Constants.SIZE_LIMIT) {
                reader.readAsDataURL(this.uploadFile);
                this.setState({
                    [docType + 'FileName']: obj[0].name,
                    [docType + 'File']: obj[0],
                    fundCommitmentErrorMsz:null,
                },()=>{
                    this.checkValidToEnableOfflineButton(docType)
                    this.clearUploadValue();
                    this.getDocTempFile(docType);
                });
            } else {
                this.clearUploadValue();
                alert(`${fileTypeName} should be less than 10 MB.`)
            }
        }
    }


    checkValidToEnableOfflineButton = (docType) => {
        let isValid = false;
        if(!this.state.showAmendments) {
            if((this.state.offlineExcutedDocuments.subscriptionAgreementUrl && this.state.subscriptionDocumentFileName) || (!this.state.offlineExcutedDocuments.subscriptionAgreementUrl)) {
                isValid = true
            }
            if((isValid && this.state.offlineExcutedDocuments.fundAgreementUrl && this.state.fundDocumentFileName)) {
                isValid = true
            } else {
                isValid = false
            }
        } else {
            if(docType === 'amendment' && ((!this.state.offlineExcutedDocuments.fundAgreementUrl && this.state.amendmentFileName) || (this.state.offlineExcutedDocuments.fundAgreementUrl && !this.state.amendmentFileName))) {
                if(this.state.selectedLpData.status === 10) {
                    if(this.state.selectedAmendments.length > 0) {
                        isValid = true
                    }
                } else {
                    if(this.state.selectedAmendments.length === this.state.offlineExcutedDocuments.atDocument.length) {
                        isValid = true
                    }
                }
            }
        }
        this.setState({
            validOfflineUploadDocs:isValid
        })
    }

    navigateToSubForm = (data) => () => {
        window.location.href = `/lp/investorInfo/${this.FsnetUtil._encrypt(data.id)}`
    }

    uploadOfflineDoc = (data) => () => {
        this.setState({
            selectedLpData: data
        }, () => {
            this.getOfflinePendingDocuments(data.id);
        })
    }

    openOfflineLpDocModal = () => {
        this.setState({
            offlineLpDocModal: true
        })
    }

    closeOfflineLpDocModal = () => {
        if((this.state.fundDocumentFileName || this.state.subscriptionDocumentFile) && this.state.showAmendments) {
            this.setState({
                showAmendments:false,
                validOfflineUploadDocs:true
            })
        } else {
            this.clearValuesForOfflineUpload();
        }
    }

    clearValuesForOfflineUpload = () => {
        this.setState({
            offlineLpDocModal: false,
            selectedLpData: { lp: {} },
            offlineLpUploadErrorMsz: null,
            fundDocumentFileName: null,
            subscriptionDocumentFileName: null,
            amendmentFileName:null,
            selectedAmendments:[],
            offlineExcutedDocuments:[]
        })
    }

    uploadOfflineLPFn = () => {
        if(this.state.subscriptionDocumentFileName) {
            this.uploadSubDoc()
        } else if(this.state.fundDocumentFileName) {
            if(this.state.offlineExcutedDocuments.fundAgreementUrl.amendmentId) {
                this.uploadRestatedFundSignDoc()
            } else {
                this.uploadFundSignDoc()
            }
        } else {
            this.uploadAmendmentDoc()
        }
    }

    uploadAmendmentDoc = () => {
        var formData = new FormData();
        formData.append("fundId", this.state.selectedLpData.fundId);
        formData.append("subscriptionId", this.state.selectedLpData.id);
        formData.append("fundAmendmentOfflineSignatures", this.state.amendmentFileName ? this.state.amendmentFile : this.state.fundDocumentFile);
        formData.append("documents", JSON.stringify(this.state.selectedAmendments));
        formData.append("docType", 'AMENDMENT_AGREEMENT');
        formData.append("date", new Date());
        formData.append("timeZone", Intl.DateTimeFormat().resolvedOptions().timeZone);
        this.open();
        this.Fsnethttp.offlineAA(formData).then(result => {
            this.close();
            this.clearValuesForOfflineUpload()
            PubSub.publish('toastNotification', { showToast: true, toastMessage: 'Offline Investor Documents has been uploaded succesfully.', toastType: 'success' });
            setTimeout(() => {
                window.location.reload();
            }, 1000);
        })
        .catch(error => {
            this.close();
            if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                this.setState({
                    offlineLpUploadErrorMsz: error.response.data.errors[0].msg,
                });
            } else {
                this.setState({
                    offlineLpUploadErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                });

            }
        });
    }

    uploadSubDoc = () => {
        var formData = new FormData();
        formData.append("fundId", this.state.selectedLpData.fundId);
        formData.append("subscriptionId", this.state.selectedLpData.id);
        formData.append("subscription", this.state.subscriptionDocumentFile);
        formData.append("date", new Date());
        formData.append("timeZone", Intl.DateTimeFormat().resolvedOptions().timeZone);
        this.open();
        this.Fsnethttp.offlineSA(formData).then(result => {
            this.close();
            this.uploadFundSignDoc()
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        offlineLpUploadErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        offlineLpUploadErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });

                }
            });
    }


    uploadFundSignDoc = () => {
        var formData = new FormData();
        formData.append("fundId", this.state.selectedLpData.fundId);
        formData.append("subscriptionId", this.state.selectedLpData.id);
        formData.append("signatures", this.state.fundDocumentFile);
        formData.append("date", new Date());
        formData.append("timeZone", Intl.DateTimeFormat().resolvedOptions().timeZone);
        this.open();
        this.Fsnethttp.offlineLpSignature(formData).then(result => {
            this.close();
            if(this.state.showAmendments) {
                this.uploadAmendmentDoc();
            } else {
                this.clearValuesForOfflineUpload();
                PubSub.publish('toastNotification', { showToast: true, toastMessage: 'Offline Investor Documents has been uploaded succesfully.', toastType: 'success' });
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            }
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        offlineLpUploadErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        offlineLpUploadErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });

                }
            });
    }

    uploadRestatedFundSignDoc = () => {
        let fundDocArray = [];
        fundDocArray.push(this.state.offlineExcutedDocuments.fundAgreementUrl)
        var formData = new FormData();
        formData.append("fundId", this.state.selectedLpData.fundId);
        formData.append("subscriptionId", this.state.selectedLpData.id);
        formData.append("fundAmendmentOfflineSignatures", this.state.fundDocumentFile);
        formData.append("docType", 'FUND_AGREEMENT');
        formData.append("documents", JSON.stringify(fundDocArray));
        formData.append("date", new Date());
        formData.append("timeZone", Intl.DateTimeFormat().resolvedOptions().timeZone);
        this.open();
        this.Fsnethttp.offlineAA(formData).then(result => {
            this.close();
            if(this.state.showAmendments) {
                this.uploadAmendmentDoc();
            } else {
                this.clearValuesForOfflineUpload();
                PubSub.publish('toastNotification', { showToast: true, toastMessage: 'Offline Investor Documents has been uploaded succesfully.', toastType: 'success' });
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            }
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        offlineLpUploadErrorMsz: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        offlineLpUploadErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });

                }
            });
    }

    getDocTempFile = (type) => {
        this.open();
        var formData = new FormData();
        formData.append("fundDoc", this.state[type+'File']);
        this.Fsnethttp.getDocTempFile(formData).then(result => {
            this.close();
            this.setState({
                [type+'FileUrl']: result.data.url,
            })
        })
        .catch(error => {
            this.close();
        });
    }

    getOfflinePendingDocuments = (id) => {
        this.open();
        this.Fsnethttp.getOfflinePendingDocuments(id).then(result => {
            this.close();
            this.setState({
                amendmentsList:result.data.atDocument,
                offlineExcutedDocuments:result.data,
                showAmendments: !result.data.fundAgreementUrl && !result.data.subscriptionAgreementUrl ? true : false
            },()=>{
                this.openOfflineLpDocModal();
            })
        })
        .catch(error => {
            this.close();
        });
    }


    showPendingAction = (data) =>  {
        let isPendingAction = false;
        let subscriptionstatus = data.subscriptionStatus.name;
        if(this.FsnetUtil.getUserRole() === 'GPDelegate') {
            return false;
        }
        if(this.FsnetUtil.getUserRole() === 'SecondaryGP' && subscriptionstatus !== 'Close-Ready') {
            if(subscriptionstatus === 'Closed' && (data.changeCommitmentAgreement && data.changeCommitmentAgreement.isPrimaryLpSigned && !data.changeCommitmentAgreement.isCurrentUserSigned && !data.changeCommitmentAgreement.isAllGpSignatoriesSigned) || (data.sideletterAgreement && data.sideletterAgreement.isPrimaryLpSigned && !data.sideletterAgreement.isCurrentUserSigned && !data.sideletterAgreement.isAllGpSignatoriesSigned)) {
                return true;
            }
        }
        if(this.FsnetUtil.getUserRole() === 'GP' && subscriptionstatus !== 'Close-Ready') {
            if(subscriptionstatus === 'Closed' && (data.changeCommitmentAgreement && data.changeCommitmentAgreement.isPrimaryLpSigned && data.changeCommitmentAgreement.isAllGpSignatoriesSigned  && !data.changeCommitmentAgreement.isCurrentUserSigned && !data.changeCommitmentAgreement.isPrimaryGpSigned)|| (data.sideletterAgreement && data.sideletterAgreement.isPrimaryLpSigned && data.sideletterAgreement.isAllGpSignatoriesSigned  && !data.sideletterAgreement.isCurrentUserSigned && !data.sideletterAgreement.isPrimaryGpSigned)) {
                return true;
            }
        }

        return isPendingAction;
    }

    getPendingActionName = (data) => {
        let name;
        let subscriptionstatus = data.subscriptionStatus.name;
        if(this.FsnetUtil.getUserRole() === 'GPDelegate') {
            return false;
        }

        if(this.FsnetUtil.getUserRole() === 'SecondaryGP' && subscriptionstatus !== 'Close-Ready') {
            if(subscriptionstatus === 'Closed' && (data.changeCommitmentAgreement && data.changeCommitmentAgreement.isPrimaryLpSigned && !data.changeCommitmentAgreement.isCurrentUserSigned && !data.changeCommitmentAgreement.isAllGpSignatoriesSigned )) {
                return 'Capital Commitment - Counter Sign';
            }
            if(subscriptionstatus === 'Closed' && (data.sideletterAgreement && data.sideletterAgreement.isPrimaryLpSigned && !data.sideletterAgreement.isCurrentUserSigned && !data.sideletterAgreement.isAllGpSignatoriesSigned)) {
                return 'Side Letter - Counter Sign';
            }
        } else if(this.FsnetUtil.getUserRole() === 'GP' && subscriptionstatus !== 'Close-Ready') {
            if(subscriptionstatus === 'Closed' && (data.changeCommitmentAgreement && data.changeCommitmentAgreement.isPrimaryLpSigned && data.changeCommitmentAgreement.isAllGpSignatoriesSigned  && !data.changeCommitmentAgreement.isCurrentUserSigned && !data.changeCommitmentAgreement.isPrimaryGpSigned)) {
                return 'Capital Commitment - Counter Sign';
            }
            if(subscriptionstatus === 'Closed' && (data.sideletterAgreement && data.sideletterAgreement.isPrimaryLpSigned && data.sideletterAgreement.isAllGpSignatoriesSigned  && !data.sideletterAgreement.isCurrentUserSigned && !data.sideletterAgreement.isPrimaryGpSigned)) {
                return 'Side Letter - Counter Sign';
            }
        } 

        return name;
    }

    openDoc = (url) => () =>{
        const docUrl = `${url}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    handleOnChangeAmendments = (obj) => (event) => {
        let list = [...this.state.selectedAmendments];
        let id = obj.documentId;
        if(!_.find(list, {documentId: obj.documentId})&& event.target.checked) {
            list.push(obj)
        } else {
            list = _.filter(list, function(x) {return x.documentId !== id})
        }
        this.setState({
            selectedAmendments:list
        },()=>{
            this.checkValidToEnableOfflineButton('amendment');
        })
    }

    render() {
        this.addedQuestionAlignment();
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                    trigger={['click']}
                    rootClose={true}
                    overlay={<Tooltip id={id} className="lpTableTooltip">{tooltip}</Tooltip>}
                    placement="bottom"
                    delayShow={300}
                    delayHide={150}
                    onClick={document.body.click()}
                    rootClose
                >
                    <span>{children}</span>
                </OverlayTrigger>
            );
        }
        const { fundId } = this.props;
        return (
            <div>
                <div className="lptableContainer">
                    <div>
                        <Row className="openingAndStatusBox titleMd height50">
                            <Col lg={12} md={12} sm={12} xs={12}>
                                <FormControl name='state' defaultValue={'all'} className="selectFormControl" onChange={(e) => this.filterChange(e)} componentClass="select">
                                    <option value='closed'>Closed</option>
                                    <option value='closed_and_close_ready'>Close-Ready + Closed</option>
                                    <option value='other'>Others</option>
                                    <option value='all'>View All</option>
                                    <option value='declined'>Declined</option>
                                </FormControl>
                            </Col>
                        </Row>
                        <div className="tableEdit">
                            <Table striped bordered className="tableMargin">
                                <div hidden={this.state.isExpanded} title="Expand View"><img className="cursor lpTableExpandIcon" src={popoutButton} alt="expand" onClick={() => { this.openFundDetailsModal() }} style={{ zIndex: zindexValue }} /></div>
                                <thead>
                                    <tr>
                                        {this.state.tableHeader.map((header, idx) => {
                                            return (
                                                
                                                <th key={`header_${idx}`} className={"cursor " + (header.property === 'questions' ? 'questionRow ' : '') + ( ['areYouAccreditedInvestor','areYouQualifiedPurchaser','areYouQualifiedClient','companiesAct','numberOfDirectEquityOwners','lookThorughIssues','erisaPlan','totalValueOfEquityInterests','releaseInvestmentEntityRequired','isSubjectToDisqualifyingEvent'].indexOf(header.property) > -1  ? 'minWidth100' : (header.property === 'firstName' ? 'lineHeight45' : (header.property === 'gpConsumed' ? 'minWidth210' : (header.property === 'lpCapitalCommitment' ? 'minWidth202' : ''))))} title={header.key} onClick={this.state.notSortedColumns.indexOf(header.property) < 0 ? this.sortViewFund(header) : null }>
                                                    {header.key}
                                                    {this.state.notSortedColumns.indexOf(header.property) < 0 && header.direction != null && header.direction == 'desc' ? <i className="fa fa-caret-down caretDown"></i> : null}
                                                    {this.state.notSortedColumns.indexOf(header.property) < 0 && header.direction != null && header.direction == 'asc' ? <i className="fa fa-caret-up caretDown"></i> : null}
                                                </th>
                                            )
                                        })}
                                    </tr>
                                </thead>
                                <tbody ref="fundScroll" hidden={this.state.fundDetailsEmpty}>
                                    {
                                        this.state.fundDetailList.map((data, idx) => {
                                            return (
                                                <tr key={`lp_data_${idx}`} subscription_id={data.id}>
                                                    <td title={data.trackerTitle && data.trackerTitle.name === undefined ? data.trackerTitle : data.trackerTitle.name}>
                                                        

                                                        {
                                                            this.showPendingAction(data) === true &&
                                                            <i className="fa fa-exclamation-circle lpPendingAction" title={this.getPendingActionName(data)} aria-hidden="true"></i>
                                                        }

                                                        <div>
                                                            <div className="lpInfo lpInfoWidth lpInfoWidthOverflow marginLeft30">
                                                                <div className="lpName">{data.trackerTitle && data.trackerTitle.name === undefined ? data.trackerTitle : data.trackerTitle.name}</div>
                                                                <span className={"statusIndicator "+ (data.subscriptionStatus && data.subscriptionStatus.id == 17 ? 'statusIndicatorPending' : '')}>
                                                                    {data.subscriptionStatus && data.subscriptionStatus.id != 16 ? data.lp.accountType == 'OfflineLP' ? data.subscriptionStatus.lpSideName + ' (Offline)' : data.subscriptionStatus.lpSideName : data.subscriptionStatus.id == 16 ? data.lp.accountType == 'OfflineLP' ? 'In Progress (Offline)' : 'In Progress' : null} &nbsp;
                                                                        <span className={"dot " + this.FsnetUtil._getClassNameForSubStatus(data.subscriptionStatus.name)}></span>
                                                                </span>
                                                            </div>
                                                            <LinkWithTooltip tooltip={
                                                                data.lp.accountType != 'OfflineLP' ?
                                                                    <ul>
                                                                        <li hidden={data.subscriptionStatus.name === 'Open'} onClick={(e) => this.viewPatnerShipDoc(e, { data })}>View Fund Document</li>
                                                                        {data.subscriptionStatus.name === 'Closed' || data.subscriptionStatus.name === 'Close-Ready' ? <li onClick={(e) => this.viewSubscriptionDoc(e, { data })}>View Subscription Document</li> : ''}
                                                                        <li hidden={data.subscriptionStatus.name === 'Closed'} onClick={(e) => this.editSubDoc(e, { data })}>Modify Subscription Document</li>
                                                                        {data.addendumAgreement ? <li>View Addendum Document</li> : ''}
                                                                        <li onClick={(e) => this.handlelpMessageModalShow(e, { data }, 'lpSendMessage')}>Send Message</li>
                                                                        {
                                                                            [3,6,11].indexOf(data.status) > -1 &&
                                                                            <li onClick={() => this.resendInvite(data)}>Re-invite</li>
                                                                        }

                                                                        {
                                                                            (data.subscriptionStatus.name === 'Closed' && data.changeCommitmentAgreement == undefined) || (data.changeCommitmentAgreement && data.changeCommitmentAgreement.isPrimaryGpSigned) ?
                                                                                <li onClick={(e) => this.handlelpMessageModalShow(e, { data }, 'increaseFundCommitment')}>Change Capital Commitment</li> :
                                                                                data.subscriptionStatus.name === 'Closed' && data.changeCommitmentAgreement && this.FsnetUtil.getUserRole() != 'GPDelegate' && ((data.changeCommitmentAgreement.isAllGpSignatoriesSigned && !data.changeCommitmentAgreement.isCurrentUserSigned && this.FsnetUtil.getUserRole() != 'SecondaryGP' && data.changeCommitmentAgreement.isPrimaryLpSigned) || (this.FsnetUtil.getUserRole() == 'SecondaryGP' && !data.changeCommitmentAgreement.isCurrentUserSigned && data.changeCommitmentAgreement.isPrimaryLpSigned)) ?
                                                                                    <li onClick={(e) => this.handleSignatureShow(data,false)}><img src={editImage} className="cursor" />Countersign Capital Commitment Increase</li> :
                                                                                    data.subscriptionStatus.name === 'Closed' && data.changeCommitmentAgreement && data.changeCommitmentAgreement.isCurrentUserSigned ?
                                                                                        <li onClick={(e) => this.handleSignatureShow(data,false)}>View Capital Commitment Increase Document</li>
                                                                                        : ''
                                                                        }
                                                                        {
                                                                            (data.changeCommitmentAgreement && data.changeCommitmentAgreement.CancelCommitmentAgreement) &&
                                                                            <li onClick={this.openCancelCapitalCommitmentModal(data)}>Cancel Capital Commitment</li>
                                                                        }

                                                                        {
                                                                            (data.subscriptionStatus.name === 'Closed' && data.sideletterAgreement == undefined) || (data.sideletterAgreement && data.sideletterAgreement.isPrimaryGpSigned) || (data.subscriptionStatus.name !== 'Closed') ?
                                                                                <li hidden={data.status == 3} onClick={this.sideLetters(data)}>Issue Side Letter</li>
                                                                                :
                                                                                data.subscriptionStatus.name === 'Closed' && data.sideletterAgreement && this.FsnetUtil.getUserRole() != 'GPDelegate' && ((!data.sideletterAgreement.isCurrentUserSigned && this.FsnetUtil.getUserRole() != 'SecondaryGP' && data.sideletterAgreement.isAllGpSignatoriesSigned && data.sideletterAgreement.isPrimaryLpSigned) || (this.FsnetUtil.getUserRole() == 'SecondaryGP' && !data.sideletterAgreement.isAllGpSignatoriesSigned && !data.sideletterAgreement.isCurrentUserSigned && data.sideletterAgreement.isPrimaryLpSigned)) ?
                                                                                    <li onClick={this.sideLetterSignature(data)}><img src={editImage} className="cursor" />Countersign Side Letter</li>
                                                                                    :
                                                                                    data.subscriptionStatus.name === 'Closed' && data.sideletterAgreement && data.sideletterAgreement.isCurrentUserSigned
                                                                                        ?
                                                                                        <li onClick={this.sideLetterSignature(data)}>View Side Letter Document</li>
                                                                                        : ''
                                                                        }
                                                                        
                                                                    </ul>
                                                                    :
                                                                    <ul>
                                                                        <li hidden={data.subscriptionStatus.name === 'Open'} onClick={(e) => this.viewPatnerShipDoc(e, { data })}>View Fund Document</li>
                                                                        {data.subscriptionStatus.name === 'Closed' || data.subscriptionStatus.name === 'Close-Ready' ? <li onClick={(e) => this.viewSubscriptionDoc(e, { data })}>View Subscription Document</li> : ''}
                                                                        {

                                                                            (['Closed','Close-Ready'].indexOf(data.subscriptionStatus.name) === -1 && ['New-Draft','Admin-Draft'].indexOf(data.fund.fundStatus.name) === -1) &&
                                                                            (data.subscriptionStatus.name !== 'Closed' && data.subscriptionStatus.name !== 'Close-Ready' && data.fund.fundStatus.name != 'New-Draft' && data.fund.fundStatus.name !== 'Admin-Draft') &&
                                                                            <li onClick={this.navigateToSubForm(data)}>Edit Subscription Form</li>
                                                                        }
                                                                        {
                                                                            (['New-Draft','Admin-Draft'].indexOf(data.fund.fundStatus.name) === -1 && this.FsnetUtil._CheckSubscriptionFormCompleted(data)) &&
                                                                            <li onClick={this.uploadOfflineDoc(data)}>Upload Executed Documents</li>
                                                                        }
                                                                        {
                                                                            (data.status != 3 && ['New-Draft','Admin-Draft'].indexOf(data.fund.fundStatus.name) === -1) &&
                                                                            <li onClick={this.sideLetters(data)}>Upload Executed Side Letter</li>
                                                                        }

                                                                        {
                                                                            (data.subscriptionStatus.name === 'Closed' && data.changeCommitmentAgreement == undefined) || (data.changeCommitmentAgreement && data.changeCommitmentAgreement.isPrimaryGpSigned) ?
                                                                                <li onClick={(e) => this.handlelpMessageModalShow(e, { data }, 'increaseFundCommitment')}>Change Capital Commitment</li> :
                                                                                data.subscriptionStatus.name === 'Closed' && data.changeCommitmentAgreement && this.FsnetUtil.getUserRole() != 'GPDelegate' && ((data.changeCommitmentAgreement.isAllGpSignatoriesSigned && !data.changeCommitmentAgreement.isCurrentUserSigned && this.FsnetUtil.getUserRole() != 'SecondaryGP' && data.changeCommitmentAgreement.isPrimaryLpSigned) || (this.FsnetUtil.getUserRole() == 'SecondaryGP' && !data.changeCommitmentAgreement.isCurrentUserSigned && data.changeCommitmentAgreement.isPrimaryLpSigned)) ?
                                                                                    <li onClick={(e) => this.handleSignatureShow(data,false)}><img src={editImage} className="cursor" />Countersign Capital Commitment Increase</li> :
                                                                                    data.subscriptionStatus.name === 'Closed' && data.changeCommitmentAgreement && data.changeCommitmentAgreement.isCurrentUserSigned ?
                                                                                        <li onClick={(e) => this.handleSignatureShow(data,false)}>View Capital Commitment Increase Document</li>
                                                                                        : ''
                                                                        }


                                                                    </ul>
                                                            } href="#" id="tooltip-1">
                                                                <img src={overFlow} alt="home_image" className="cursor alginThreeDots" />
                                                            </LinkWithTooltip>
                                                        </div>
                                                    </td>
                                                    <td title={this.FsnetUtil.getFullName(data.lp)}>
                                                        {
                                                            <div className="cursor marginLeft10" onClick={this.lpNameProfile(data.lp)}>
                                                                <div className="width50 left inline-block">
                                                                    <img src={data.lp.profilePic ? data.lp.profilePic.url : userDefaultImage} alt="img" className="imgEdit" />
                                                                </div>
                                                                <span className="trackerName">{data.trackerTitle.name ? data.trackerTitle.name : this.FsnetUtil.getFullName(data.lp)}</span>
                                                            </div>
                                                        }
                                                    </td>
                                                    <td title={data.investorType == 'LLC' ? 'Entity' : (data.investorType ? data.investorType : 'N/A')}>
                                                        {data.investorType == 'LLC' ? 'Entity' : (data.investorType ? data.investorType : 'N/A')}
                                                    </td>
                                                    <td title={data.investorType == 'LLC' || data.investorType == 'Trust' ? (data.investorSubType ? data.investorSubTypeInfo.name : (data.otherInvestorSubType ? data.otherInvestorSubType : 'N/A')) : 'N/A'}>
                                                        {data.investorType == 'LLC' || data.investorType == 'Trust' ? (data.investorSubType ? data.investorSubTypeInfo.name : (data.otherInvestorSubType ? data.otherInvestorSubType : 'N/A')) : 'N/A'}
                                                    </td>
                                                    <td className="minWidth202" title={data.lpCapitalCommitment ? this.FsnetUtil.convertToCurrency(data.lpCapitalCommitment) : '$0'}>
                                                        {data.lpCapitalCommitment ? this.FsnetUtil.convertToCurrency(data.lpCapitalCommitment) : '$0'}
                                                    </td>
                                                    <td className="minWidth210" title={data.gpConsumed ? this.FsnetUtil.convertToCurrency(data.gpConsumed) : '$0'}>
                                                        {data.gpConsumed ? this.FsnetUtil.convertToCurrency(data.gpConsumed) : '$0'}
                                                    </td>

                                                    <td className="minWidth210 cursor" onClick={(e) => this.openDisclosureModal('Prior Commitment Amounts', { data }, 'commitmentHistoryPretty')}>
                                                        {data.commitmentHistoryPretty && data.commitmentHistoryPretty.length > 0 ? 'Show Prior Commitment(s)' : ''}
                                                    </td>
                                                    <td title={data.investorCount || data.investorCount == 0 ? data.investorCount : 1}>
                                                        {data.investorCount || data.investorCount == 0 ? data.investorCount : 1}
                                                    </td>
                                                    <td title={data.placeOfResidence ? data.placeOfResidence : ''}>
                                                        {data.placeOfResidence ? data.placeOfResidence : 'N/A'}
                                                    </td>
                                                    <td title={data.placeOfOrganization ? data.placeOfOrganization : ''}>
                                                        {data.placeOfOrganization ? data.placeOfOrganization : 'N/A'}
                                                    </td>
                                                    <td className="cursor" onClick={(e) => this.openDisclosureModal('View Address', { data }, 'investorAddress')}>
                                                        {data.investorAddress ? 'View Address' : ''}
                                                    </td>
                                                    <td>
                                                        {data.fundPercentage}%
                                                </td>
                                                    <td className="minWidth100" title={data.areYouAccreditedInvestor ? 'Yes' : data.areYouAccreditedInvestor == false ? 'No' : ''}>
                                                        {data.areYouAccreditedInvestor ? 'Yes' : data.areYouAccreditedInvestor == false ? 'No' : ''}
                                                    </td>
                                                    <td className="minWidth100" title={data.areYouQualifiedPurchaser ? 'Yes' : data.areYouQualifiedPurchaser == false ? 'No' : ''}>
                                                        {data.areYouQualifiedPurchaser ? 'Yes' : data.areYouQualifiedPurchaser == false ? 'No' : ''}
                                                    </td>
                                                    <td className="minWidth100" title={data.areYouQualifiedClient ? 'Yes' : data.areYouQualifiedClient ? 'No' : ''}>
                                                        {data.areYouQualifiedClient ? 'Yes' : data.areYouQualifiedClient == false ? 'No' : ''}
                                                    </td>
                                                    <td className="minWidth100" title={data.companiesAct && data.companiesAct > 1 ? 'Yes' : 'No'}>
                                                        {data.companiesAct && data.companiesAct > 1 ? 'Yes' : 'No'}
                                                    </td>
                                                    {/* <td className="minWidth100" title={((data.companiesAct && (data.companiesAct == 1 || data.companiesAct == 4)) && (data.entityProposingAcquiringInvestment && data.anyOtherInvestorInTheFund && data.entityHasMadeInvestmentsPriorToThedate && data.partnershipWillNotConstituteMoreThanFortyPercent && data.beneficialInvestmentMadeByTheEntity)) ? 'N/A' : (data.numberOfDirectEquityOwners ? data.numberOfDirectEquityOwners : 1)}>
                                                    {((data.companiesAct && (data.companiesAct == 1 || data.companiesAct == 4)) && (data.entityProposingAcquiringInvestment && data.anyOtherInvestorInTheFund && data.entityHasMadeInvestmentsPriorToThedate && data.partnershipWillNotConstituteMoreThanFortyPercent && data.beneficialInvestmentMadeByTheEntity)) ? 'N/A' : (data.numberOfDirectEquityOwners ? data.numberOfDirectEquityOwners : 1)}
                                                </td> */}

                                                    <td className="minWidth100" title={data.numberOfDirectEquityOwners}>
                                                        {data.numberOfDirectEquityOwners}
                                                    </td>
                                                    <td className="minWidth100" title={data.lookThroughIssues}>
                                                        {data.lookThroughIssues}
                                                    </td>
                                                    <td className="minWidth100" title={(!data.employeeBenefitPlan && !data.planAsDefinedInSection4975e1 && !data.benefitPlanInvestor) ? 'No' : 'Yes'}>
                                                        {(!data.employeeBenefitPlan && !data.planAsDefinedInSection4975e1 && !data.benefitPlanInvestor) ? 'No' : 'Yes'}
                                                    </td>
                                                    <td className="minWidth100" title={!data.employeeBenefitPlan && !data.planAsDefinedInSection4975e1 && !data.benefitPlanInvestor ? '0%' : (!data.employeeBenefitPlan && !data.planAsDefinedInSection4975e1 ? `${data.totalValueOfEquityInterests ? data.totalValueOfEquityInterests : '0'}%` : '100%')}>
                                                        {!data.employeeBenefitPlan && !data.planAsDefinedInSection4975e1 && !data.benefitPlanInvestor ? '0%' : (!data.employeeBenefitPlan && !data.planAsDefinedInSection4975e1 ? `${data.totalValueOfEquityInterests ? data.totalValueOfEquityInterests : '0'}%` : '100%')}
                                                    </td>
                                                    <td className="minWidth100" title={data.releaseInvestmentEntityRequired ? 'Yes' : 'No'}>
                                                        {data.releaseInvestmentEntityRequired ? 'Yes' : 'No'}
                                                    </td>
                                                    <td className="minWidth100" title={data.isSubjectToDisqualifyingEvent ? 'Yes' : 'No'}>
                                                        {data.isSubjectToDisqualifyingEvent ? 'Yes' : 'No'}
                                                    </td>
                                                    <td className="cursor" title={data.fundManagerInfo ? data.fundManagerInfo : 'N/A'} onClick={(e) => this.openDisclosureModal('Other Investor Attributes', { data }, 'investorAttribute')}>
                                                        {data.fundManagerInfo ? data.fundManagerInfo : 'N/A'}
                                                    </td>
                                                    <td className="cursor" title={data.otherInvestorAttributes ? data.otherInvestorAttributes.join(', ') : 'N/A'} onClick={(e) => this.openDisclosureModal('Other Investor Attributes', { data }, 'otherInvestorAttribute')}>
                                                        {data.otherInvestorAttributes && data.otherInvestorAttributes.length ? data.otherInvestorAttributes.join(', ') : 'N/A'}
                                                    </td>
                                                    <td className="cursor">
                                                        {data.lpClosedDate ? data.lpClosedDate : 'N/A'}
                                                        {
                                                            (data.lpClosedDate && data.timezone) ? <span> ({data.timezone}) </span> : ''
                                                        }
                                                    </td>
                                                    {
                                                        data.investorQuestions && data.investorQuestions.length > 0 ?
                                                            data.investorQuestions.map((record, index) => {
                                                                return (
                                                                    <td key={index} header={record['questionTitle']} typeofquestion={record['typeOfQuestion']} onClick={(e) => this.openDisclosureModal('', { record }, 'question')} className="cursor paddingLeft34 questionRow" title={record['typeOfQuestion'] === 2 || record['typeOfQuestion'] === 3 ?
                                                                        record['typeOfQuestion'] === 2 ?
                                                                            record['questionResponse'] == 1 ? 'True' : record['questionResponse'] == 0 ? 'False' : 'N/A'
                                                                            :
                                                                            record['questionResponse'] == 1 ? 'Yes' : record['questionResponse'] == 0 ? 'No' : 'N/A'
                                                                        :
                                                                        record['questionResponse'] ? record['questionResponse'] : 'N/A'}>
                                                                        {
                                                                            record['typeOfQuestion'] === 2 || record['typeOfQuestion'] === 3 ?
                                                                                record['typeOfQuestion'] === 2 ?
                                                                                    record['questionResponse'] == 1 ? 'True' : record['questionResponse'] == 0 ? 'False' : 'N/A'
                                                                                    :
                                                                                    record['questionResponse'] == 1 ? 'Yes' : record['questionResponse'] == 0 ? 'No' : 'N/A'
                                                                                :
                                                                                record['questionResponse'] ? record['questionResponse'] : 'N/A'
                                                                        }
                                                                    </td>
                                                                );
                                                            })
                                                            :
                                                            ''
                                                    }
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </Table>
                            <div className="text-center" hidden={!this.state.fundDetailsEmpty}>
                                <p> No Investor data found</p>
                            </div>
                        </div>

                    </div>

                    <Modal backdrop="static" id={(this.state.hideSendMessage ? 'lpFundCommitmentModal' : 'lPNameProfileModal')} show={this.state.lpMessageModal} onHide={this.handlelpMessageModalClose} dialogClassName={"GPDelModalDialog " + (this.state.hideSendMessage && this.state.selectedLpData.lp.accountType != 'OfflineLP' ? 'lpFundCommitmentModalDialog' : this.state.selectedLpData.lp.accountType == 'OfflineLP' ? 'offlineLpFundCommitmentModalDialog' : 'lpProfileModalDialog')} className="">
                        <Modal.Header className="headerNone" closeButton>
                        </Modal.Header>
                        <Modal.Title></Modal.Title>
                        <Modal.Body>
                            {
                                this.state.lpProfileData['profilePic'] ?
                                    <img src={this.state.lpProfileData['profilePic']['url']} alt="img" className="profilePicLarge" />
                                    :
                                    <img src={profilePictureLarge} alt="img" className="profilePicLarge" />
                            }
                            <div className="profileNameModal ellipsis" title={this.FsnetUtil.getFullName(this.state.lpProfileData)}>{this.FsnetUtil.getFullName(this.state.lpProfileData)}</div>
                            <div className="profileNameDelegate">{this.state.lpProfileData.accountType}</div>
                            <div hidden={this.state.hideSendMessage}>
                                {this.state.selectedLpData.lp && this.state.selectedLpData.fund ?
                                    <div contentEditable="true" className="lpMessageTextBox">This is a reminder that your invitation to subscribe for an interest in {this.state.selectedLpData.fund.legalEntity}, which has been initiated, has not yet been completed by you.</div> : ''}
                                {/* As the Fund is closing soon, please visit <a href={this.state.sendMessageUrl}>Vanilla</a> to complete your subscription to the Fund.<br/>Thank you for your support.<br/>Best regards,<br/>The Vanilla Team */}
                                <div className="error marginTop10 marginBot10 text-center">{this.state.sendMessageError}</div>
                                <Button className="confirmAndSaveBtn btnWidth270 marginLeft50 btnEnabled" onClick={this.notifyMessageToLp}>Send Message</Button>
                            </div>
                            <div hidden={!this.state.hideSendMessage}>
                                {
                                    (this.state.selectedLpData.lp.accountType == 'OfflineLP') &&
                                    <div className="offlineCapitalCommitmentText">Please upload all pages of the Capital Commitment Increase letter, including the investor’s signature page.  DO NOT include the Fund Manager Signature page, as Vanilla will append this automatically through the e-signature process.</div>
                                }
                                <div className="Current-Fund-Commitm marginLeft50 marginTop10">Current Fund Commitment:</div>
                                <div className="contributionAmount marginLeft50">{this.FsnetUtil.convertToCurrency(this.state.selectedLpData.gpConsumed)}</div>
                                <div className="Current-Fund-Commitm marginLeft50">New Fund Commitment</div>
                                <FormControl type="text" placeholder="$10,0000.00" name="amount" value={this.state.amountCurrency} className="inputFormControl inputWidth270 marginBottom10 marginLeft50 inputNewFundCommitment" onChange={(e) => { this.handleChangeCapitalEvent(e) }} onKeyPress={(e) => { this.numbersOnly(e) }} onFocus={(e) => this.setState({ amountCurrency: this.state.amount })} onBlur={(e) => this.setState({ amount: e.target.value.trim() !== '' ? e.target.value.trim() : e.target.value, amountCurrency: e.target.value.trim() !== '' ? this.FsnetUtil.convertToCurrency(e.target.value.trim()) : '' })} />

                                {
                                    this.state.selectedLpData.lp.accountType == 'OfflineLP' ?
                                        this.state.capitalCommitmentFileName ?
                                            <div className="upload-doc-name marginTop20">
                                                <div className="title-md marginLeft50 width250 text-center breakWord inline-block">{this.state.capitalCommitmentFileName}<i className="fa fa-trash cursor-pointer marginLeft10" onClick={this.deleteFile('capitalCommitment')}></i></div>
                                            </div>

                                            :
                                            <div className="width350 text-center marginTop2 marginBot10">
                                                <Button className="fsnetSubmitButtonMd width250 marginLeft20 btnEnabled marginTop20" onClick={this.uploadDoc('capitalCommitment')}>Upload Capital Commitment</Button>
                                                <input type="file" id="uploadBtn" className="hide" onChange={(e) => this.handleChange(e, null, 'capitalCommitment')} />
                                            </div>
                                        :
                                        <div className="Current-Fund-Commitm marginLeft50">Comment</div>
                                }
                                {
                                    (this.state.selectedLpData.lp.accountType != 'OfflineLP') && 
                                    <FormControl componentClass="textarea" placeholder="Description" name="lpMessage" className="inputFormControl inputWidth270 height75 marginLeft50 inputNewFundCommitment" onChange={(e) => this.setState({ lpMessageText: e.target.value.trim() === '' ? e.target.value.trim() : e.target.value })} />
                                }
                                <div className="error fundError marginTop5">{this.state.fundCommitmentErrorMsz}</div>
                                <div className="text-center">
                                    <Button className={"confirmAndSaveBtn btnWidth270 marginBottom10 marginTop15 disabledBtnBgColor fontFamilyOpenSans  " + (!this.state.amount ? 'disabled' : 'btnEnabled')} disabled={!this.state.amount} onClick={this.changeCurrentFundCommitment}>Send Increase Offer</Button>
                                </div>
                            </div>
                            <Button className="fsnetCancelButton btnWidth270 marginLeft50 heightAndFontSize" onClick={this.handlelpMessageModalClose}>Cancel</Button>
                        </Modal.Body>
                    </Modal>

                    <Modal id="GPDelModal" backdrop="static" show={this.state.sideLetterModal} onHide={this.handleSideLetterModalClose}>
                        <Modal.Header className="headerNone" closeButton>
                        </Modal.Header>
                        <Modal.Title> Upload Side Letter</Modal.Title>
                        <Modal.Body>
                            <div className="title-md">{this.state.selectedLpData.lp && this.state.selectedLpData.lp.accountType == 'OfflineLP' ? 'Please upload all pages of the side letter including the Investor and Fund Manager signature pages.' : 'Please upload all pages of the side letter except the Investor and Fund Manager signature pages. Vanilla will append these at closing automatically.'} </div>
                            {
                                this.state.sideLetterFileName ?
                                    <div className="upload-doc-name marginTop20">
                                        <div className="title-md marginLeft10 width600 text-center breakWord inline-block">{this.state.sideLetterFileName}<i className="fa fa-trash cursor-pointer marginLeft10" onClick={this.deleteFile('sideLetter')}></i></div>
                                    </div>

                                    :
                                    <div className="width650 text-center marginTop2">
                                        <Button className="fsnetSubmitButtonMd width200 btnEnabled marginTop20" onClick={this.uploadDoc('sideLetter')}>Upload Side Letter</Button>
                                        <input type="file" id="uploadBtn" className="hide" onChange={(e) => this.handleChange(e, null, 'sideLetter')} />
                                    </div>
                            }

                            <div className="error marginTop10">{this.state.sideLetterErrorMsz}</div>

                            <Row className="marginTop30">
                                <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                    <Button type="button" className="fsnetCancelButton width250 marginLeft50" onClick={this.handleSideLetterModalClose}>Cancel</Button>
                                </Col>
                                <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                    <Button className={"fsnetSubmitButton btnWidth270 " + (this.state.sideLetterFileName ? 'btnEnabled' : 'disabled')} disabled={!this.state.sideLetterFileName} onClick={this.submitSideLetterFn}>{this.state.selectedLpData.lp.accountType == 'OfflineLP' ? 'Execute Side Letter' : 'Send to Investor'}</Button>
                                </Col>
                            </Row>

                        </Modal.Body>
                    </Modal>
                    {/* Upload Offline Documents */}
                    <Modal id="GPDelModal" backdrop="static" show={this.state.offlineLpDocModal} onHide={this.closeOfflineLpDocModal} dialogClassName="GPDelModalDialog gpDelFundModalDialog closeFundAgreementModal" className="GPSignModal GPCloseSignModal">
                        <Modal.Header className="headerNone" closeButton>
                        </Modal.Header>
                        <Modal.Title> Upload Executed Documents</Modal.Title>
                        <Modal.Body>
                            {
                                (this.state.offlineExcutedDocuments.atDocument && !this.state.offlineExcutedDocuments.fundAgreementUrl && !this.state.offlineExcutedDocuments.subscriptionAgreementUrl && this.state.offlineExcutedDocuments.atDocument.length ===0) ?
                                <div className="subtext marginBot10">There are no documents needed to upload for this investor.</div>
                                :
                                <div>
                                    <div className="subtext marginBot10" hidden={this.state.showAmendments}>Please upload the following documents  to make the investor available for closing.  You may return to this screen at any time  to re-upload these documents PRIOR to closing. </div>
                                    {
                                        !this.state.showAmendments ? 
                                            <div className="subtext marginLeft20">
                                                <input type="file" id="uploadBtn" className="hide" onChange={(e) => this.handleChange(e, null, this.state.documentUploadType)} />
                                                {
                                                    this.state.offlineExcutedDocuments.subscriptionAgreementUrl &&
                                                    <div>
                                                        <div>A full PDF copy of the executed Subscription Agreement, including the investor’s signature page.</div>
                                                        {
                                                            this.state.subscriptionDocumentFileName ?
                                                                <div className="upload-doc-name marginTop20">
                                                                    <div className="title-md marginLeft10 width600 text-center breakWord inline-block cursor link"><span onClick={this.openUploadDoc('subscriptionDocument')}> {this.state.subscriptionDocumentFileName}</span><i className="fa fa-trash cursor-pointer marginLeft10" onClick={this.deleteFile('subscriptionDocument')}></i></div>
                                                                </div>
                
                                                                :
                                                                <div className="width650 text-center marginTop2">
                                                                    <Button className="fsnetSubmitButtonMd width350 btnEnabled marginTop10" onClick={this.uploadDoc('subscriptionDocument')}>Upload Executed Subscription Agreement</Button>
                
                                                                </div>
                                                        }
                                                    </div>
                                                }
                                                {
                                                    this.state.offlineExcutedDocuments.fundAgreementUrl &&
                                                    <div>
                                                        <div className="marginTop10">
                                                            {
                                                                this.state.offlineExcutedDocuments.subscriptionAgreementUrl ? 
                                                                    'The Signature Page(s) ONLY for the investor’s Fund Agreement.Vanilla will use the currently active Fund Agreement and append this signature page.'
                                                                :
                                                                'Please upload the signature page(s) ONLY to the Amendment to the Fund Agreement.  By uploading this signature page, the investor will be deemed to have consented to the Amendment to the Fund Agreement and this item will no longer gate any closing.  Vanilla will use the currently active Amendment to the Fund Agreement and append this signature page.'
                                                            }     
                                                            
                                                            
                                                        </div>
                                                        {
                                                            this.state.fundDocumentFileName ?
                                                                <div className="upload-doc-name marginTop20">
                                                                    <div className="title-md marginLeft10 width600 text-center breakWord inline-block cursor link"><span onClick={this.openUploadDoc('fundDocument')}> {this.state.fundDocumentFileName}</span><i className="fa fa-trash cursor-pointer marginLeft10" onClick={this.deleteFile('fundDocument')}></i></div>
                                                                </div>

                                                                :
                                                                <div className="width650 text-center marginTop2">
                                                                    <Button className="fsnetSubmitButtonMd width350 btnEnabled marginTop10" onClick={this.uploadDoc('fundDocument')}>Upload Fund Agreement Signature Pages</Button>
                                                                </div>
                                                        }
                                                    </div>
                                                }
                                            </div>
                                        :
                                            <div className="subtext marginLeft20">
                                                <input type="file" id="uploadBtn" className="hide" onChange={(e) => this.handleChange(e, null, this.state.documentUploadType)} />
                                                <div className="subtext marginBot10">
                                                    {
                                                        !this.state.fundDocumentFileName ?
                                                        'Please upload the signature page(s) ONLY to the Amendment to the Fund Agreement.  By uploading this signature page, the investor will be deemed to have consented to the Amendment to the Fund Agreement and this item will no longer gate any closing.  Vanilla will use the currently active Amendment to the Fund Agreement and append this signature page.'
                                                        :
                                                        'By checking the below checkbox, you are confirming that the investor has confirmed their approval of the updated Fund Agreement via email and consents to their signature page being applied to the updated Fund Agreement.'
                                                    }
                                                </div>
                                            
                                                    <div>
                                                            <h1 className="title-md marginTop10 marginBotNone">Amendments:</h1>
                                                            <div className="consentFAandAmendents">
                                                                {
                                                                    this.state.amendmentsList.map((record, index) => {
                                                                        return (
                                                                            <Row className="marginTop10" key={index}>
                                                                                <Col lg={12} md={12} sm={12} xs={12}>
                                                                                    <CBox className="marginLeft10" checked={_.findIndex(this.state.selectedAmendments,{documentId:record.documentId}) > -1 ? true : false} onChange={this.handleOnChangeAmendments(record)}>
                                                                                        <span className="checkmark"></span>
                                                                                    </CBox>
                                                                                    <span style={{'position':'relative','top':'-7px'}} onClick={this.openDoc(record.url)} className="radio-span cursor">{record.filename}</span>
                                                                                </Col>
                                                                            </Row>
                                                                                
                                                                        );
                                                                    })
                                                                }
                                                            </div>
                                                    </div>
                                                {
                                                    this.state.amendmentFileName  ?
                                                        <div className="upload-doc-name marginTop20">
                                                            <div className="title-md marginLeft10 width600 text-center breakWord inline-block cursor link"><span onClick={this.openUploadDoc('amendment')}> {this.state.amendmentFileName}</span><i className="fa fa-trash cursor-pointer marginLeft10" onClick={this.deleteFile('amendment')}></i></div>
                                                        </div>
                                                        :
                                                        !this.state.fundDocumentFileName &&
                                                        <div className="width650 text-center marginTop2">
                                                            <Button className="fsnetSubmitButtonMd autoWidth btnEnabled marginTop10" onClick={this.uploadDoc('amendment')}>Upload signature page</Button>
                                                        </div>
                                                }
                                            </div>
                                    }
                                    <div className="error marginTop10">{this.state.offlineLpUploadErrorMsz}</div>
                                    <Row className="marginTop30">
                                        <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                            <Button type="button" className="fsnetCancelButton width250 marginLeft50" onClick={this.closeOfflineLpDocModal}>Cancel</Button>
                                        </Col>
                                        <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                            {
                                                this.state.amendmentsList.length > 0 && !this.state.showAmendments ?
                                                    <Button className={"fsnetSubmitButton btnWidth270 " + (this.state.validOfflineUploadDocs ? 'btnEnabled' : 'disabled')} disabled={!this.state.validOfflineUploadDocs} onClick={()=>this.setState({showAmendments:true},()=>{ this.checkValidToEnableOfflineButton('amendment')})}>Next</Button>
                                                :
                                                    <Button className={"fsnetSubmitButton btnWidth270 " + (this.state.validOfflineUploadDocs ? 'btnEnabled' : 'disabled')} disabled={!this.state.validOfflineUploadDocs} onClick={this.uploadOfflineLPFn}>Upload Documents</Button>
                                            }
                                        </Col>
                                    </Row>
                                </div>

                            }
                            

                        </Modal.Body>
                    </Modal>

                    <Modal id="confirmInvestorModal" backdrop="static" className="" dialogClassName="tooltipDialog" show={this.state.showDisclosureModal} onHide={this.hideDisclosureModal}>
                        <Modal.Header className="TtModalHeaderAlign" closeButton>
                            <h1>{this.state.modalTitle}</h1>
                        </Modal.Header>
                        <Modal.Body className="TtModalBody investorModal">
                            {
                                this.state.modalTitle != 'Prior Commitment Amounts' ?
                                    <div>
                                        {this.state.modalContent}
                                    </div> :

                                    this.state.modalContent.map((record, index) => {
                                        return (
                                            <div key={index}>
                                                {record}
                                            </div>
                                        );
                                    })
                            }


                        </Modal.Body>
                    </Modal>
                    <Modal id="GPDelModal" backdrop="static" show={this.state.signatureModal} onHide={this.handleSignatureClose} dialogClassName="GPDelModalDialog fundModalDialog" className="GPDelModalMarginLeft GPSignModal topZero">
                        <Modal.Header closeButton>
                        </Modal.Header>
                        <Modal.Title>{this.state.hasSignature ? 'Authentication Password' : 'Configure Signature'}</Modal.Title>
                        <Modal.Body>
                            {
                                !this.state.hasSignature ?
                                    <div className="title">Please Configure Signature using this <a href={"/settings/uploadSignature/" + this.FsnetUtil._encrypt(this.state.fundId)}>link</a>.</div>
                                    :
                                    <div>
                                        <div className="subtext modal-subtext">By entering your password, you represent that you have signing authority for the applicable Fund and/or Document.</div>
                                        <div className="subtext modal-subtext">Please re-enter your password</div>
                                        <div className="form-main-div signatureModalBody">
                                            <FormControl type="password" name="signaturePassword" className="inputFormControl inputFormControlCloseFund inputFormSignatuteAuth" placeholder="Password" onChange={(e) => this.handleSingaturePwd(e)} />
                                        </div>
                                        <div className="error marginBot12 height10">{this.state.signatureErrorMsz}</div>
                                        <Row>
                                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                                <Button type="button" className="fsnetCancelButton marginLeft50" onClick={this.handleSignatureClose}>Cancel</Button>
                                            </Col>
                                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                                <Button type="button" className={"fsnetSubmitButton closeFundSubmitBtn " + (this.state.enableSignatureBtn ? 'btnEnabled' : 'disabled')} disabled={!this.state.enableSignatureBtn} onClick={this.verifySignature}>Submit</Button>
                                            </Col>
                                        </Row>
                                    </div>
                            }
                        </Modal.Body>
                    </Modal>
                    <Modal id="CCIncrease" backdrop="static" show={this.state.ccIncreaseShow} onHide={this.ccIncreaseClose} dialogClassName="GPDelModalDialog fundModalDialog" className="GPDelModalMarginLeft GPSignModal">
                        <Modal.Header closeButton>
                        </Modal.Header>
                        <Modal.Title className="errorHeadAlign titleModal ">Error</Modal.Title>
                        <Modal.Body>

                            <div className="error marginBot12 height10 errMsgAlign">{this.state.ccErrorMsz}</div>
                            <Row>
                                <Col lg={12} md={12} sm={12} xs={12} className="paddingZero text-center" hidden={!this.state.showProceed}>
                                    <Button type="button" className="fsnetCancelButton marginLeft50 btnEnabled proceedWidth" onClick={this.ccIncreaseProceed}>Proceed Regardless</Button>
                                </Col>
                            </Row>
                        </Modal.Body>
                    </Modal>
                    <Loader isShow={this.state.showModal}></Loader>
                    <Modal id="GPDelModal" backdrop="static" show={this.state.cancelCapitalCommitmentModal}  onHide={this.closeCancelCapitalCommitmentModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title">Are you sure you want to cancel the Capital Commitment?</h1>
                        <Row className="fundBtnRow">
                            <Col lg={5} md={5} sm={5} xs={12}>
                                <Button type="button" className="fsnetCancelButton marginLeft50" onClick={this.closeCancelCapitalCommitmentModal}>Cancel</Button>
                            </Col>
                            <Col lg={7} md={7} sm={7} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width200 marginLeft50" onClick={this.cancelCapitalCommitment}>Proceed</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                </div>
            </div>
        );
    }
}

export default LpTableComponent;


import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
// import { EditorState, ContentState, convertToRaw } from 'draft-js';
// import htmlToDraft from 'html-to-draftjs';
// import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
// import draftToHtml from 'draftjs-to-html';
import Loader from '../../widgets/loader/loader.component';
import { Fsnethttp } from '../../services/fsnethttp';
import { FsnetUtil } from '../../util/util';
import { Editor } from '@tinymce/tinymce-react';

class editSubscriptionAggrement extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            // editorState: EditorState.createEmpty(),
            fundId: null,
            subscriptionId: 0,
            section: 'one',
            showSubMsz: false,
            content: null,
            loaded: false
        };
    }

    componentDidMount() {
        let url = window.location.href;
        let id = url.split('editSubForm')[1].split('/')[1];
        const fundId = id.split('&id=')[0]
        const subscriptionId = url.split('&id=')[1];
        const parts = url.split('/')
        const section = parts[parts.length - 1].indexOf('&id=') === -1 ? parts[parts.length - 1] : parts[parts.length - 1].split('&id=')[0]
        this.setState({ fundId: fundId, subscriptionId: subscriptionId ? subscriptionId : 0, section: section }, () => { this.getSectionData(); })
    }

    handleEditorChange = (e) => {
        console.log('Content was updated:', e.target.getContent());
        this.setState({
            content: e.target.getContent()
        })
    }


    getSectionData = () => {
        this.open();

        this.Fsnethttp.getSectionData(this.state.fundId, this.state.subscriptionId, this.state.section).then(result => {
            this.close();
            const html = result.data;
            this.setState({
                content: html,
                loaded: true
            })
            // const contentBlock = htmlToDraft(html);
            // const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            // this.setState({
            //     editorState: EditorState.createWithContent(contentState)
            // })
        })
            .catch(error => {
                this.close();
            });
    }

    // onEditorStateChange = (editorState) => {
    //     this.setState({
    //         editorState,
    //     });
    //     console.log(draftToHtml(convertToRaw(editorState.getCurrentContent())))
    // };

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    submitSubSection = () => {
        this.open();

        const postObj = { content: this.state.content }
        this.Fsnethttp.updateSectionData(this.state.fundId, this.state.subscriptionId, this.state.section, postObj).then(result => {
            this.close();
            const html = result.data;
            this.setState({ showSubMsz: true })
        })
            .catch(error => {
                this.close();
            });
    }

    closeWindow = () => {
        window.close();
    }

    render() {
        // const { editorState } = this.state;
        console.log(this.state.content)
        return (
            <div className="rdw-storybook-root container margin20">


                {/* <Editor
                        editorState={editorState}
                        wrapperClassName="demo-wrapper"
                        editorClassName="demo-editor"
                        onEditorStateChange={this.onEditorStateChange}
                    /> */}
                {
                    this.state.loaded ?
                        <div hidden={this.state.showSubMsz}>
                            <Editor
                                initialValue={this.state.content}
                                init={{
                                    nonbreaking_force_tab: true,
                                    // plugins: [
                                    //     'lists hr nonbreaking paste'
                                    // ],
                                    // toolbar: 'bold italic sizeselect  fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | insertfile undo redo',
                                    plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help',
                                    toolbar: 'bold italic sizeselect  fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | insertfile undo redo',
                                    menubar: false,
                                }}
                                onChange={this.handleEditorChange}
                            />
                            <div className="text-right margin20">
                                <Button className="fsnetSubmitButton btnEnabled width200" onClick={this.submitSubSection}>Save Changes</Button>
                            </div>
                        </div>

                        : ''
                }

                <div hidden={!this.state.showSubMsz}>
                    {
                        this.state.subscriptionId !== 0 ?
                            <h1 className="subtext marginTop10 margin20">The revised Subscription Agreement has been dispatched for electronic execution by the applicable Investor(s).</h1>
                            :
                            <h1 className="subtext marginTop10 margin20 text-center">Your edits to the Subscription Document have been inserted successfully.</h1>
                    }
                    <div className="margin20 text-center">
                        {/* {
                        this.state.subscriptionId !==  0 ? */}
                        <Button className="fsnetSubmitButton btnEnabled width90" onClick={this.closeWindow}>Ok</Button>
                        {/*: <Button className="fsnetSubmitButton btnEnabled" onClick={this.closeWindow}>Submit Edits to Investor(s)</Button>
                    } */}
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }

}

export default editSubscriptionAggrement;

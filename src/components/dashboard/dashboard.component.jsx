
import React, { Component } from 'react';
import './dashboard.component.css';
import { FsnetAuth } from '../../services/fsnetauth';
import { Row, Col, FormControl, Button, Modal } from 'react-bootstrap';
import userDefaultImage from '../../images/fund-default.png';
import { reactLocalStorage } from 'reactjs-localstorage';
import HeaderComponent from '../header/header.component';
import { Fsnethttp } from '../../services/fsnethttp';
import { Constants } from '../../constants/constants';
import { FsnetUtil } from '../../util/util';
import Loader from '../../widgets/loader/loader.component';
import boxNotificationImage from '../../images/boxNotiImage.svg';
import plusImg from '../../images/plus.svg';
import BellIcon from 'react-bell-icon';
import vanillaLogo from '../../images/Vanilla.png';
import Footer from '../footer/footer.component';

class DashboardComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            isHide: true,
            rowData: 0,
            filterState: 'Off',
            allGPFundsList: [],
            allLpFundsList: [],
            rejectedFundList: [],
            noFundsMessage: '',
            showRejectedFundText: false,
            jsonData: {},
            loggedInUserObj: {},
            addSubscriptionModal: false,
            subscriptionId: null
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    showActivityFeed = () => {
        this.setState({
            rowData: 1
        })
    }

    navigateToCreateFund = () => {
        this.props.history.push('/fundsetup/funddetails');
    }

    openFund = (e, id, status) => {
        // this.props.history.push('/fundsetup/funddetails/' + id);
        this.props.history.push('/fund/view/' + this.FsnetUtil._encrypt(id));
    }

    openLpFund = (e, id, status) => {
        if (status !== 'Open' && status !== 'Not Interested') {
            const path = status === 'Closed' ? 'review' : 'investorInfo';
            this.props.history.push('/lp/' + path + '/' + this.FsnetUtil._encrypt(id));
        }
    }

    redirectToPendingAction = (e, id) => {
        this.props.history.push('/lp/pendingActions/' + this.FsnetUtil._encrypt(id));
    }

    searchInputChangeEvent = (e) => {
        let value = e.target.value;
        if (value !== '') {

            if (value.trim() !== '') {
                this.Fsnethttp.getSearchFunds(value.trim()).then(result => {
                    if (result.data && result.data.data.length > 0) {
                        this.setState({
                            allGPFundsList: result.data.data
                        })
                    } else {
                        this.setState({
                            allGPFundsList: [],
                            noFundsMessage: this.Constants.NO_GP_SEARCH_FUNDS
                        })
                    }
                })
                .catch(error => {
                });
            }
        } else {
            this.getGpFunds()
        }
    }

    // Get current loggedin user details
    //If token is undefined then redirect to login page 
    componentDidMount() {
        this.getJsonData();
        if (this.FsnetAuth.isAdmin()) {
            window.location = '/adminDashboard/firmView';
        } else if (this.FsnetAuth.isAuthenticated()) {
            //Get user obj from local storage.
            let userObj = reactLocalStorage.getObject('userData');
            if (userObj) {
                if (['GP', 'GPDelegate', 'SecondaryGP'].indexOf(userObj.accountType) > -1) {
                    this.getGpFunds();
                } else {
                    this.getLpFunds();
                }
                this.setState({
                    loggedInUserObj: userObj,
                    dashboardType: userObj.accountType
                })
            }
        } else {
            window.location = '/';
        }
    }

    fundContainerWidth = () => {
        const windowWidth = window.innerWidth;
        const desktopWidth = this.state.dashboardType == 'LP' || this.state.dashboardType === 'LPDelegate' ? 400 : 300
        if (document.getElementsByClassName('fund-container') && windowWidth > 1200) {
            document.getElementsByClassName('fund-container')[0].style.width = (windowWidth - desktopWidth) + 'px';
        } else if (document.getElementsByClassName('fund-container') && windowWidth >= 1024 && windowWidth < 1200) {
            document.getElementsByClassName('fund-container')[0].style.width = (windowWidth - 50) + 'px';
        } else if (document.getElementsByClassName('fund-container') && windowWidth >= 768 && windowWidth < 1024 && (this.state.dashboardType == 'LP' || this.state.dashboardType === 'LPDelegate')) {
            document.getElementsByClassName('fund-container')[0].style.width = (windowWidth - 250) + 'px';
        }
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('dashboard').then(result => {
            this.setState({
                jsonData: result.data
            })
        });

    }

    //Get list of GP funds
    getGpFunds = () => {
        this.open();
        this.Fsnethttp.getListOfAllFunds().then(result => {
            this.close();
            this.setState({
                allGPFundsList: result.data.data,
                noFundsMessage: this.FsnetUtil.getUserRole() === 'GPDelegate' ? this.Constants.DELEGATE_NO_FUNDS : this.Constants.NO_GP_FUNDS
            })

        })
            .catch(error => {
                this.close();
                this.setState({
                    allGPFundsList: [],
                    noFundsMessage: this.Constants.NO_GP_FUNDS
                })
            });

    }

    checkNoRejectedFunds(data) {
        let count = 0;
        for (let index of data) {
            if ([3, 5, 6, 11].indexOf(index['subscriptionStatus']['id']) > -1) {
                count = count + 1;
            }
        }
        return count
    }

    //Get list of GP funds
    getLpFunds = () => {
        this.open();
        this.Fsnethttp.getListOfLpFunds().then(result => {
            this.close();
            const isRejectedFunds = this.checkNoRejectedFunds(result.data.data);
            const rejectedFundsArr = result.data.data.filter(obj => (([3, 5, 6, 11].indexOf(obj['subscriptionStatus']['id']) > -1) && obj['isPrimaryInvestment']))
            this.setState({
                rejectedFundList: rejectedFundsArr
            }, () => {
                const checkCountForIsErrorMessage = result.data.data.filter(obj => obj.isErrorMessage == true)
                const isErrorMessage = this.FsnetUtil.getUserRole() != 'LP' && result.data.data.length == checkCountForIsErrorMessage.length ? false : true
                if (result.data && result.data.data.length > 0 && isRejectedFunds != result.data.data.length && isErrorMessage) {
                    this.setState({
                        allLpFundsList: result.data.data,
                    })
                } else {
                    this.setState({
                        allLpFundsList: [],
                        noFundsMessage: this.FsnetUtil.getUserRole() != 'LP' && result.data.data.length == checkCountForIsErrorMessage.length && result.data.data.length > 0 ? this.Constants.NO_LPDELGATE_FUNDS : this.Constants.NO_LP_FUNDS
                    })
                }
                this.setState({

                })
            })

        })
            .catch(error => {
                this.close();
                this.setState({
                    allLpFundsList: [],
                    noFundsMessage: this.Constants.NO_LP_FUNDS
                })
            });
    }

    openpartnershipDocument = (link) => {
        if (link) {
            const docUrl = `${link.url}?token=${this.FsnetUtil.getToken()}`
            this.FsnetUtil._openDoc(docUrl)
        }
    }

    rejectFund = (id) => {
        if (id) {
            this.open();
            let obj = { subscriptionId: id }
            this.Fsnethttp.rejectGPInvitedFund(obj).then(result => {
                this.close();
                this.getLpFunds();
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    acceptFund = (id) => {
        if (id) {
            this.open();
            let obj = { subscriptionId: id }
            this.Fsnethttp.acceptGPInvitedFund(obj).then(result => {
                this.close();
                this.props.history.push('/lp/investorInfo/' + this.FsnetUtil._encrypt(id));
            })
                .catch(error => {
                    this.close();
                });
        }
    }

    logout = () => {
        if (this.FsnetUtil.getAdminRole() === 'FSNETAdministrator') {
            this.FsnetUtil._removeAdminData();
            setTimeout(() => {
                window.location.href = '/adminDashboard/firmView';
            }, 200);
        }
    }

    openAddSubscriptionModal = (id) => {
        this.setState({ addSubscriptionModal: true, subscriptionId: id })
    }

    closeAddSubscriptionModal = () => {
        this.setState({ addSubscriptionModal: false, subscriptionId: null })
    }

    createFormOtherSubscription = () => {
        this.open();
        let obj = { subscriptionId: this.state.subscriptionId }
        this.Fsnethttp.createFormOtherSubscription(obj).then(result => {
            this.close();
            this.closeAddSubscriptionModal();
            this.props.history.push('/lp/investorInfo/' + this.FsnetUtil._encrypt(result.data.subscription.id));
        })
            .catch(error => {
                this.close();
            });
    }

    render() {
        return (
            <Row className="dashboardContainer" id="MainDashboard">
                {
                    (this.FsnetUtil.getAdminRole() === 'FSNETAdministrator' && window.location.href.indexOf('/dashboard') > -1) &&
                    <div className="impersonateHeader text-center"><label className="title ellipsis">Impersonated as {this.FsnetUtil.getFullName(this.state.loggedInUserObj)}</label><Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.logout}>Go Back To Admin</Button></div>
                }
                <Row className="dashboardMainRow">
                    <Col lg={5} md={6} sm={6} xs={12}>
                        <img src={vanillaLogo} alt="vanilla" className="vanilla-logo marginLeft30" />
                    </Col>
                    <Col lg={7} md={6} sm={6} xs={12}>
                        <HeaderComponent ></HeaderComponent>
                    </Col>
                </Row>
                {
                    (['GP', 'GPDelegate', 'SecondaryGP'].indexOf(this.FsnetUtil.getUserRole()) > -1) ?
                        <Row className={"dashboardMainRow fund-container gp-container " + (this.state.allGPFundsList.length == 0 ? 'minHeight200' : '')} >
                            {
                                (this.FsnetUtil.getUserRole() === 'GPDelegate' && this.state.allGPFundsList.length === 0) ?
                                    ''
                                    :
                                    <Row>
                                        <div className="myFunds">{this.state.jsonData.SELECT_FUND}</div>
                                        <Col lg={12} md={12} sm={12} xs={12} className="filter-container">
                                            <Col lg={6} md={6} sm={6} xs={12} className="display-filter display-filter-padding display-left-filter">
                                                <span className="search-icon"><i className="fa fa-search" aria-hidden="true"></i></span>
                                                <FormControl type="text" placeholder="Search Funds" className="formFilterControl marginLeft15" onChange={(e) => this.searchInputChangeEvent(e)} />
                                            </Col>
                                            <Col lg={6} md={6} sm={6} xs={12} className="display-filter filter-right display-right-filter">
                                                <div className="filter-right-block">
                                                    <Button className="newFundButton" onClick={this.navigateToCreateFund}><img src={plusImg} alt="plusImg" className="plusImg" />New Fund</Button>
                                                </div>
                                            </Col>
                                        </Col>
                                    </Row>

                            }
                            {this.state.allGPFundsList.length > 0 ?
                                <div className="gp-col-container">
                                    {this.state.allGPFundsList.map((record, index) => {
                                        return (
                                            <div className="fund-col-container" key={index}>
                                                <div onClick={(e) => this.openFund(e, record['id'], record['status'])} className={"fundBoxEdit " + (record['status'] == 'Deactivated' ? 'disabled fundDeactivated' : '')}>
                                                    <div className="fundImageEdit">
                                                        {
                                                            record['fundImage'] ?
                                                                <img src={record['fundImage']['url']} alt="img" className="Fund-Image" /> :
                                                                <img src={userDefaultImage} alt="fund_image" className="Fund-Image" />
                                                        }
                                                        <div title={record['legalEntity']} className={'fund-name-gp ' + (record['legalEntity'].length <= 20 ? 'fund-name-align' : 'removeTop')} >{record['legalEntity']}</div>
                                                        <img src={boxNotificationImage} onClick={this.showActivityFeed} alt="notification-icon" className="notification-icon boxNotifStyle" />
                                                        <span className="notificationCount">0</span>
                                                        <div className="invitedDivStyle">{record['fundInvestor'].length} Invited</div>
                                                    </div>
                                                    <div className="actnRqStyle minimumHeight25">
                                                    </div>
                                                    <div className="Line"></div>
                                                    <div className="text-center progressDivAlign">
                                                        <div className="inProgressDiv">
                                                            <div className="Invited">{record['inProgress']}</div>
                                                            <div className="labelInvited">In Progress</div>
                                                            <div className="labelInvitedPrice" title={record['sumOfCapitalCommitmentForInProgress'] ? this.FsnetUtil.convertToCurrency(record['sumOfCapitalCommitmentForInProgress']) : '$0'}>{record['sumOfCapitalCommitmentForInProgress'] ? this.FsnetUtil.convertToCurrency(record['sumOfCapitalCommitmentForInProgress']) : '$0'}</div>
                                                        </div>
                                                        <div className="closeReadyDiv">
                                                            <div className="Invited1">{record['closeReady']}</div>
                                                            <div className="labelClosedReady">Close-Ready</div>
                                                            <div className="labelClosedPrice" title={record['sumOfCapitalCommitmentForCloseReady'] ? this.FsnetUtil.convertToCurrency(record['sumOfCapitalCommitmentForCloseReady']) : '$0'}>{record['sumOfCapitalCommitmentForCloseReady'] ? this.FsnetUtil.convertToCurrency(record['sumOfCapitalCommitmentForCloseReady']) : '$0'}</div>
                                                        </div>
                                                        <div className="closedDiv">
                                                            <div className="Invited2">{record['closed']}</div>
                                                            <div className="labelClosed">Closed</div>
                                                            <div className="labelClosedReadyPrice" title={record['sumOfCapitalCommitmentForClosed'] ? this.FsnetUtil.convertToCurrency(record['sumOfCapitalCommitmentForClosed']) : '$0'}>{record['sumOfCapitalCommitmentForClosed'] ? this.FsnetUtil.convertToCurrency(record['sumOfCapitalCommitmentForClosed']) : '$0'}</div>
                                                        </div>
                                                    </div>
                                                    <div className="Line clear-both"></div>


                                                    <div className="fund-progress-bar">
                                                        <div className="price-percentage" style={{ left: (record['fundTargetCommitmentPercent'] ? record['fundTargetCommitmentPercent'] + '%' : '0%') }} hidden={!record['fundTargetCommitment']}>
                                                            <div className="caretDownAlign"><span>T</span><i className="fa fa-caret-down caretDownTarget"></i></div>
                                                        </div>
                                                        <div className="progress-bar progress-bar-margin">
                                                            <div className={"progress progress-green progress-radius-left " + (!record['closeReadyPercent'] && !record['inProgressPercent'] ? 'progress-radius-left progress-radius-right' : '')} style={{ width: (record['closedPercent'] ? record['closedPercent'] + '%' : '0%') }}></div>
                                                            <div className={"progress progress-yellow " + (!record['inProgressPercent'] ? 'progress-radius-right ' : ' ') + (!record['closedPercent'] ? 'progress-radius-left' : '')} style={{ width: (record['closeReadyPercent'] ? record['closeReadyPercent'] + '%' : '0%') }}></div>
                                                            <div className={"progress progress-pink progress-radius-right " + (!record['closeReadyPercent'] && !record['closedPercent'] ? 'progress-radius-left progress-radius-right' : '')} style={{ width: (record['inProgressPercent'] ? record['inProgressPercent'] + '%' : '0%') }}></div>
                                                        </div>
                                                        <div className="price-percentage" style={{ left: (record['fundHardCapPercent'] ? record['fundHardCapPercent'] + '%' : '70%') }} hidden={!record['fundHardCap']}>
                                                            <div className="caretUpAlign"><i className="fa fa-caret-up"></i><span className="caretUpTarget">HC</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="bold tPriceStyle price-meter marginTop25">Target (T):&nbsp;<span>{this.FsnetUtil.convertToCurrency(record['fundTargetCommitment'] ? record['fundTargetCommitment'] : 0)}</span></div>
                                                    <div className="bold hcPriceStyle price-meter marginTop4">Hard Cap (HC):&nbsp;<span>{this.FsnetUtil.convertToCurrency(record['fundHardCap'] ? record['fundHardCap'] : 0)}</span></div>

                                                </div>
                                            </div>
                                        );
                                    })
                                    }
                                </div>
                                :
                                <Col lg={12} md={12} sm={12} xs={12}>
                                    <div className="title margin20 text-center marginTop50">{this.state.noFundsMessage}</div>
                                </Col>
                            }
                        </Row> :
                        <Row id="lpMainDashboard" className={"dashboardMainRow fund-container lp-container " + (this.state.allLpFundsList.length == 0 ? 'minHeight200' : '')} >
                            <div className="myFunds">My Funds</div>
                            {this.state.allLpFundsList.length > 0 ?
                                this.state.allLpFundsList.map((record, index) => {
                                    return (
                                        ([3, 5, 6, 11].indexOf(record['subscriptionStatus']['id']) == -1 && ((!record['isErrorMessage'] && this.FsnetUtil.getUserRole() != 'LP') || this.FsnetUtil.getUserRole() == 'LP')) &&
                                        <div className={"fund-lp-container " + (record['subscriptionStatus']['name'] == 'Deactivated' ? 'disabled fundDeactivated' : '')} key={index}>
                                            <div className="margin16">
                                                <div className="row">
                                                    {
                                                        record['fund']['fundImage'] ?
                                                            <img src={record['fund']['fundImage']['url']} alt="img" className="Fund-Image marginImg" /> :
                                                            <img src={userDefaultImage} alt="fund_image" className="Fund-Image marginImg" />
                                                    }
                                                    <div className='fund-Name' >
                                                        {record['fund']['legalEntity']}
                                                        <span className="dashboard-name width300 ellipsis" title={(record.investorType ? record['trackerTitle'] : '(New Investor Pending Input)') + (record.investorType != 'LLC' ? ' - ' + record['investorType'] : ' - Entity')}>{record.investorType ? record['trackerTitle'] : '(New Investor Pending Input)'}{
                                                            (record.investorType) && <span className="paddingLeft5">{record.investorType != 'LLC' ? '- ' + record['investorType'] : '- Entity'}</span>
                                                        }</span>


                                                    </div>
                                                    <div className="alert-div margin-top-15">
                                                        {
                                                            record['pendingActionsCount'] > 0 && record['subscriptionStatus']['name'] !== 'Deactivated' && (this.state.dashboardType === 'LP' || this.state.dashboardType === 'SecondaryLP') ?
                                                                <BellIcon width='40' color="red" active={true} className="cursor" animate={true} onClick={(e) => this.redirectToPendingAction(e, record['id'])} />
                                                                :
                                                                <BellIcon width='40' color="red" active={false} animate={false} />
                                                        }
                                                    </div>
                                                </div>
                                                <div className="row InvitationBox marginTop23" onClick={(e) => this.openLpFund(e, record['id'], record['subscriptionStatus']['name'])}>
                                                    <span className="invitationOpen" hidden={record['subscriptionStatus']['name'] !== 'Open'}>You have been Invited to join the Fund</span>
                                                    <span className="invitationOpen" hidden={record['subscriptionStatus']['name'] !== 'Removed'}>You have been Removed from this Fund</span>
                                                    <span className="invitationOpen" hidden={record['subscriptionStatus']['name'] !== 'Deactivated'}>You have been Deactivated from this Fund</span>
                                                    {
                                                        ((record['subscriptionStatus']['name'] == 'Open-Ready-Draft') || (record['subscriptionStatus']['name'] == 'Waiting For Signatory Signs' && !record.isCurrentUserSigned && this.FsnetUtil.getUserRole() === 'SecondaryLP') || (record['subscriptionStatus']['name'] == 'Waiting For Signatory Signs' && this.FsnetUtil.getUserRole() !== 'SecondaryLP' && (record.isAllLpSignatoriesSigned || record.noOfSignaturesRequired == 0 || !record.noOfSignaturesRequired))) &&
                                                        <span className="invitationOpen">You Are Invited - Documents Need Completion</span>
                                                    }
                                                    {
                                                        ((record['subscriptionStatus']['name'] == 'Waiting For Signatory Signs' && this.FsnetUtil.getUserRole() !== 'SecondaryLP' && (!record.isAllLpSignatoriesSigned && record.noOfSignaturesRequired > 0))) &&
                                                        <span className="invitationOpen">Waiting for Secondary Signatory</span>
                                                    }
                                                    {
                                                        (record['subscriptionStatus']['name'] == 'Waiting For Signatory Signs' && record.isCurrentUserSigned && this.FsnetUtil.getUserRole() === 'SecondaryLP') &&
                                                        <span className="invitationOpen">Waiting for other signatories</span>
                                                    }
                                                    <span className="invitationClosed" hidden={record['subscriptionStatus']['name'] !== 'Close-Ready'}>Documents Complete - Under Fund Manager Review</span>
                                                    <span className="invitationClosed" hidden={record['subscriptionStatus']['name'] !== 'Closed'}>Closed - Commitment Accepted</span>
                                                </div>
                                                <div className="row marginTop16">
                                                    <div className="width230">
                                                        <div className="labelDashboard">My Offer To Subscribe:</div>
                                                        <div className="name-title" title={record['lpCapitalCommitment'] ? this.FsnetUtil.convertToCurrency(record['lpCapitalCommitment']) : ''}>{record['lpCapitalCommitment'] ? this.FsnetUtil.convertToCurrency(record['lpCapitalCommitment']) : ''}</div>
                                                        <div className="name-title height25" hidden={record['lpCapitalCommitment']}></div>
                                                        <div className="name-title height25"></div>
                                                        <div className="labelDashboard" hidden={this.FsnetUtil.getUserRole() === 'LP'}>Investor Name:</div>
                                                    </div>
                                                    <div className="marginLeftMinus10">
                                                        <div className="labelDashboard">Fund Manager:</div>
                                                        <div className="name-title dashboard-ellipsis" title={this.FsnetUtil.getFullName(record['fund']['gp'])}>{this.FsnetUtil.getFullName(record['fund']['gp'])}</div>
                                                        {
                                                            record['fund']['gp']['cellNumber'] ?
                                                                <div className="name-title">{record['fund']['gp']['cellNumber']}</div>
                                                                :
                                                                <div className="name-title height25"></div>
                                                        }
                                                        <div className="name-title dashboard-ellipsis" title={this.FsnetUtil.getFullName(record['lp'])} hidden={this.FsnetUtil.getUserRole() === 'LP'}>{this.FsnetUtil.getFullName(record['lp'])}</div>
                                                    </div>
                                                </div>
                                                {
                                                    record['subscriptionStatus']['name'] == 'Open' ?
                                                        <div className="row marginTop16">
                                                            <Button type="button" className="emailManagerAndSupportButton marginRight25" onClick={() => this.acceptFund(record['id'])}>Yes, I am interested.</Button>
                                                            <Button type="button" className="emailManagerAndSupportButton" onClick={() => this.rejectFund(record['id'])}>No, not at this time.</Button>
                                                        </div>
                                                        :
                                                        <div className="row marginTop10">
                                                            <div>
                                                                <Button type="button" className="emailManagerAndSupportButton marginRight25"><a href={"mailto:" + record['fund']['gp']['email'] + "?Subject=In regards to Fund: " + record['fund']['legalEntity']}>Email Fund Manager</a></Button>
                                                                <Button type="button" className="emailManagerAndSupportButton"><a href={"mailto:vanillavc@cooley.com?Subject=In regards to Fund: " + record['fund']['legalEntity']}>Email Vanilla Support</a></Button>
                                                            </div>
                                                            {
                                                                this.FsnetUtil.getUserRole() == 'LP' &&
                                                                <div className="marginTop20 marginRight10 text-right width450">
                                                                    <a className="addLink" onClick={() => this.openAddSubscriptionModal(record['id'])}>Add Additional Investor</a>
                                                                </div>
                                                            }
                                                        </div>
                                                }
                                            </div>
                                        </div>
                                    );
                                })
                                :
                                <Col lg={12} md={12} sm={12} xs={12}>
                                    <div className="title margin20 text-center marginTop50">{this.state.noFundsMessage}</div>
                                </Col>
                            }
                            <div className="rejectedFunds" hidden={this.state.rejectedFundList.length == 0}>Rejected Funds</div>
                            {this.state.rejectedFundList.length > 0 ?
                                this.state.rejectedFundList.map((record, index) => {
                                    return (
                                        ([3, 5, 6, 11].indexOf(record['subscriptionStatus']['id']) > -1) && record['isPrimaryInvestment'] ?
                                            <div className="fund-lp-container" key={index}>
                                                <div className="margin16">
                                                    <div className="row">
                                                        {
                                                            record['fund']['fundImage'] ?
                                                                <img src={record['fund']['fundImage']['url']} alt="img" className="Fund-Image marginImg" /> :
                                                                <img src={userDefaultImage} alt="fund_image" className="Fund-Image marginImg" />
                                                        }
                                                        <div className='fund-Name' title={record['fund']['legalEntity']}>{record['fund']['legalEntity']}</div>
                                                    </div>
                                                    <div className="row InvitationBox marginTop23 pointerNone" >
                                                        {record['subscriptionStatus']['id'] === 3 ?
                                                            <span className="invitationOpen">You have declined to join this Fund</span>
                                                            :
                                                            <span className="invitationOpen">You have been Removed from this Fund</span>
                                                        }
                                                    </div>
                                                    <div className="row marginTop16">
                                                        <div className="width230">
                                                            <div className="labelDashboard">My Offer To Subscribe:</div>
                                                            <div className="name-title height25"></div>
                                                            <div className="name-title height25"></div>
                                                            <div className="labelDashboard" hidden={this.FsnetUtil.getUserRole() === 'LP'}>Investor Name:</div>
                                                        </div>
                                                        <div className="marginLeftMinus10">
                                                            <div className="labelDashboard">Fund Manager:</div>
                                                            <div className="name-title dashboard-ellipsis" title={this.FsnetUtil.getFullName(record['fund']['gp'])}>{this.FsnetUtil.getFullName(record['fund']['gp'])}</div>
                                                            {
                                                                record['fund']['gp']['cellNumber'] ?
                                                                    <div className="name-title">{record['fund']['gp']['cellNumber']}</div>
                                                                    :
                                                                    <div className="name-title height25"></div>
                                                            }
                                                            <div className="name-title dashboard-ellipsis" title={this.FsnetUtil.getFullName(record['lp'])} hidden={this.FsnetUtil.getUserRole() === 'LP'}>{this.FsnetUtil.getFullName(record['lp'])}</div>
                                                        </div>
                                                    </div>
                                                    <div className="row marginTop16">
                                                        <Button type="button" className="emailManagerAndSupportButton marginRight25"><a href={"mailto:" + record['fund']['gp']['email'] + "?Subject=In regards to Fund: " + record['fund']['legalEntity']}>Email Fund Manager</a></Button>
                                                        <Button type="button" className="emailManagerAndSupportButton"><a href={"mailto:vanillavc@cooley.com?Subject=In regards to Fund: " + record['fund']['legalEntity']}>Email Vanilla Support</a></Button>
                                                    </div>
                                                </div>
                                            </div>
                                            : ''
                                    );
                                })
                                :
                                <Col lg={12} md={12} sm={12} xs={12}>

                                </Col>
                            }
                        </Row>
                }
                <Loader isShow={this.state.showModal}></Loader>
                <Footer></Footer>

                {/* Add Subscription Confirmation Modal */}
                <Modal id="" show={this.state.addSubscriptionModal} onHide={this.closeAddSubscriptionModal} dialogClassName="delteFundDocDailog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Title className="title titleModal">{this.state.jsonData.ADDITIONAL_INVESTOR_TITLE}</Modal.Title>
                    <Modal.Body className="paddingTopNone">
                        {/* <h1 className="title">{this.state.jsonData.ADDITIONAL_INVESTOR_TITLE}</h1> */}
                        <div className="subtext minHeight50 marginTop10">{this.state.jsonData.ADDITIONAL_INVESTOR_TEXT}</div>
                        <Row className="marginTop20">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton marginLeft30" onClick={this.closeAddSubscriptionModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled marginLeft40" onClick={this.createFormOtherSubscription}>Proceed</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
            </Row>
        );
    }
}

export default DashboardComponent;


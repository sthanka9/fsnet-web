import React, { Component } from 'react';
import { PanelGroup, Panel, Button, Modal, Row, Col, FormControl, Tooltip, OverlayTrigger, Radio } from 'react-bootstrap';
import { reactLocalStorage } from 'reactjs-localstorage';
import Loader from '../../../widgets/loader/loader.component';
import { Fsnethttp } from '../../../services/fsnethttp';
import { PubSub } from 'pubsub-js';
import { Constants } from '../../../constants/constants';
import ToastComponent from '../../toast/toast.component';
import { FsnetUtil } from '../../../util/util';

var pageInfo, sectionWin, close = {}
class Step4Component extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            activeKey: '',
            fundId: null,
            eventKey: null,
            subscriptionId: 0,
            section: null,
            content: null,
            jsonData: {},
            fundData: {},
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            sendEditToInvestor: false,
            status: null,
            restoreModal: false,
            showSectionsPage: true,
            addQuestionModal: false,
            deleteQuestionModal: false,
            addQuestionPageValid: false,
            questionList: [],
            typeOfQuestion: null,
            isRequired: null,
            questionTitle: null,
            question: null,
            questionId: null,
            deleteQuestionId: null,
            uploadDocFile: {},
            showSendEditsToInvestor:false,
            isFormChanged:false,
            uploadDocModal:false
        };
        pageInfo = PubSub.publish('pageNumber', { type: 'sideNav', page: 'editSubForm' });
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })

    }

    componentDidMount() {
        this.getJsonData();
        let url = window.location.href;
        let id = url.split('editSubForm')[1].split('/')[1];
        id = this.FsnetUtil._decrypt(id)
        const fundId = id.split('&id=')[0]
        const subscriptionId = url.split('&id=')[1];
        this.setState({ fundId: fundId, subscriptionId: subscriptionId ? subscriptionId : 0 }, () => {
            this.getSubscriptionStatus();
            if (this.state.subscriptionId === 0) {
                this.getFundData()
            }
        })

    }

    getJsonData = () => {
        this.Fsnethttp.getJson('editSubscription').then(result => {
            this.setState({
                jsonData: result.data
            })
        });

    }

    getFundData = () => {
        this.open();

        this.Fsnethttp.getFund(this.state.fundId).then(result => {
            this.close();
            PubSub.publish('fundData', result.data.data);
            this.setState({ fundData: result.data.data })
        })
            .catch(error => {
                this.close();
            });
    }

    getSubscriptionStatus = () => {
        this.open();
        this.Fsnethttp.getSubscriptonEditHtmlStatus(this.state.fundId, this.state.subscriptionId).then(result => {
            this.close();
            this.setState({ sendEditToInvestor: result.data.show })
        })
        .catch(error => {
            this.close();
        });
    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(pageInfo);
        PubSub.unsubscribe(close);
    }

    closeToast(timed) {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    handleSelect = (activeKey) => {
        this.setState({ activeKey });
    }

    openSubAggrementSection = (type) => {
        if (this.state.subscriptionId === 0) {
            sectionWin = window.open(`/editSubForm/${this.state.fundId}/${this.state.subscriptionId}/${type}`, '_blank', 'width = 1000px, height = 800px');
        } else {
            sectionWin = window.open(`/editSubForm/${this.state.fundId}/${type}&id=${this.state.subscriptionId}`, '_blank', 'width = 1000px, height = 800px');
        }
        var $this = this;
        var timer = setInterval(function () {
            if (sectionWin && sectionWin.closed) {
                clearInterval(timer);
                $this.getSubscriptionStatus()
                $this.getSectionData();
            }
        }, 1000);
    }

    openSection = (key, section) => {
        if (key !== this.state.eventKey) {
            this.setState({ section: section, eventKey: key })
        } else {
            this.setState({ eventKey: null })
        }
    }

    getSectionData = () => {
        this.open();

        this.Fsnethttp.getSectionData(this.state.fundId, this.state.subscriptionId, this.state.section).then(result => {
            this.close();
            this.setState({ content: result.data })
        })
            .catch(error => {
                this.close();
            });
    }

    submitToInvestor = () => {
        this.open();
        this.Fsnethttp.reIssueSubscriptionAgreement(this.state.fundId, this.state.subscriptionId).then(result => {
            this.close();
            if (this.state.subscriptionId !== 0) {
                window.location.href = `/fund/view/${this.state.fundId}`
            } else {
                this.closeSendEditsToInvestorModal();
                this.setState({
                    showToast: true,
                    toastMessage: this.state.jsonData.SUB_MESSAGE,
                    toastType: 'success',
                },()=>{
                    setTimeout(() => {
                        this.props.history.push('/createfund/upload/' + this.FsnetUtil._encrypt(this.state.fundId));
                    }, 1000);
                })
            }
        })
        .catch(error => {
            this.close();
        });
    }

    openPdf = () => {
        const docUrl = `${this.Constants.BASE_URL}subscription/sample/generate/${this.state.fundId}/${this.state.subscriptionId}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }

    proceedToNext = () => {
        if(this.state.sendEditToInvestor && this.state.isFormChanged) {
            this.showSendEditsToInvestorModal()
        } else {
            if(this.state.subscriptionId != 0) {
                window.location.href = `/fund/view/${this.state.fundId}`
            } else {
                this.props.history.push('/createfund/upload/' + this.FsnetUtil._encrypt(this.state.fundId));
            }
        }
    }

    proceedToBack = () => {
        if(this.FsnetUtil.getUserRole() == 'GPDelegate') {
            this.props.history.push('/createfund/funddetails/' + this.FsnetUtil._encrypt(this.state.fundId));
        } else {
            this.props.history.push('/createfund/gpDelegate/' + this.FsnetUtil._encrypt(this.state.fundId));
        }
    }

    openRestoreModal = () => { this.setState({ restoreModal: true}) }
    closeRestoreModal = () => { this.setState({ restoreModal: false }) }

    openAddQuestionModal = () => { this.setState({ addQuestionModal: true }) }
    closeAddQuestionModal = (onInit) =>{
        this.setState({
            addQuestionModal: false, addQuestionPageValid: false
        }, () => {
            this.clearInputStateValues();
            if (onInit && typeof (onInit) === 'boolean') {
                this.questionList()
            }
        }
        )
    }

    openDeleteQuestionModal = (id) => { this.setState({ deleteQuestionModal: true, deleteQuestionId: id }) }
    closeDeleteQuestionModal = () => {
        this.setState({ deleteQuestionModal: false, deleteQuestionId: null }) 
    }

    restoreTextFn = () => {
        this.open();
        this.Fsnethttp.restoreToDefault(this.state.fundId, this.state.subscriptionId, this.state.section).then(result => {
            this.close();
            this.setState({ content: result.data.data ? result.data.data : '',isFormChanged:true })
            this.closeRestoreModal();
        })
            .catch(error => {
                this.close();
                this.closeRestoreModal();
            });
    }

    handleInputChangeEvent = (e, type, key, msz) => {
        let dataObj = {};
        switch (type) {
            case 'text':
                const value = e.target.value;
                if (value === '' || value === undefined || value.trim() === '') {
                    this.setState({
                        [key + 'Msz']: this.Constants[msz],
                        [key + 'Valid']: false,
                        [key + 'Border']: true,
                        [key]: ''
                    })
                    let name = key + 'Valid'
                    dataObj = {
                        [name]: false
                    };
                } else {
                    this.setState({
                        [key + 'Msz']: '',
                        [key + 'Valid']: true,
                        [key + 'Border']: false,
                        [key]: value
                    })
                    let name = key + 'Valid'
                    dataObj = {
                        [name]: true
                    };
                }
                this.updateStateParams(dataObj);
                break;
            case 'radio':
                this.setState({
                    [key + 'Msz']: '',
                    [key + 'Valid']: true,
                    [key + 'Border']: false,
                    [key]: msz
                })
                let name = key + 'Valid'
                dataObj = {
                    [name]: true
                };
                this.updateStateParams(dataObj);
                break;
            default:
                break;
        }
    }

    updateStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, () => {
            this.enableDisableSaveButton();
        });
    }

    enableDisableSaveButton = () => {
        let status = (this.state.typeOfQuestionValid && this.state.isRequiredValid && this.state.questionValid && this.state.questionTitleValid) ? true : false;
        this.setState({
            addQuestionPageValid: status
        });

    }

    saveQuestionFn = () => {
        this.open();
        const postObj = { fundId: this.state.fundId, subscriptionId: this.state.subscriptionId, typeOfQuestion: this.state.typeOfQuestion, isRequired: this.state.isRequired, questionTitle: this.state.questionTitle, question: this.state.question }
        this.Fsnethttp.saveQuestion(postObj, this.state.questionId).then(result => {
            this.close();
            this.closeAddQuestionModal(true);
        })
            .catch(error => {
                this.close();
            });
    }

    questionList = () => {
        this.open();
        this.Fsnethttp.questionList(this.state.fundId, this.state.subscriptionId).then(result => {
            this.close();
            this.setState({ showSectionsPage: false, questionList: result.data.questionsList })
        })
            .catch(error => {
                this.close();
            });
    }

    editQuestion = (data) => {
        const fileds = ['typeOfQuestion', 'isRequired', 'question', 'questionTitle'];
        for (let index of fileds) {
            this.setState({
                [index]: data[index],
                [index + 'Valid']: true,
                addQuestionPageValid: true,
                questionId: data['id']
            })
        }
        this.openAddQuestionModal();
    }

    deleteQuestion = () => {
        this.open();
        this.Fsnethttp.deleteQuestion(this.state.deleteQuestionId).then(result => {
            this.close();
            let updatedQuestionsList = this.state.questionList.filter(data => data.id !== this.state.deleteQuestionId);
            this.setState({ questionList: updatedQuestionsList }, () => { this.closeDeleteQuestionModal(); })
        })
        .catch(error => {
            this.close();
        });
    }


    clearInputStateValues = () => {
        const fileds = ['typeOfQuestion', 'isRequired', 'question', 'questionTitle'];
        for (let index of fileds) {
            this.setState({
                [index]: null,
                [index + 'Msz']: '',
                [index + 'Valid']: false,
                [index + 'Border']: false,
                questionId: null
            })
        }
    }

    uploadDoc = () => {
        document.getElementById('uploadBtn').click();
    }

    //Upload patnership document.
    handleChange = (event) => {
        let obj = event.target.files
        let reader = new FileReader();
        if(obj && obj.length > 0) {
            this.uploadFile = obj[0];
            let sFileName = obj[0].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            if(sFileExtension !== 'pdf') {
                this.clearFile()
                alert(this.state.jsonData.ERROR)
                return true;
            }
            //File 10MB limit
            if(this.uploadFile.size <= this.Constants.SIZE_LIMIT) {
                this.setState({
                    uploadDocFile : obj[0],
                    uploadDocFileName:obj[0].name,
                });
                // console.log(obj[0])
                reader.readAsDataURL(this.uploadFile);
            } else {
                this.clearFile()
               alert(this.state.jsonData.ERROR)
            }
        }
    }

    clearFile = () => {
        document.getElementById('uploadBtn').value = "";
    }

    deleteFile = () => {
        this.clearFile();
        this.setState({
            uploadDocFile:{},
            uploadDocFileName:null
        })
    }

    uploadSectionDocFile = () => {
        var formData = new FormData();
        formData.append("fundId", this.state.fundId);
        formData.append("subscriptionId", this.state.subscriptionId);
        formData.append("section", this.state.section);
        formData.append("file", this.state.uploadDocFile);
        this.open();
        this.Fsnethttp.uploadSectionFile(formData).then(result => {
            this.close();
            this.closeUploadDocModal();
            this.setState({
                uploadDocFile:{},
                showToast: true,
                isFormChanged:true,
                toastMessage: this.state.jsonData.TOAST_MESSAGE,
                toastType: 'success',
            })
            this.clearFile()
        })
        .catch(error => {
            this.close();
        });
    }

    downloadDoc = () => {
        this.open();
        this.Fsnethttp.downloadSectionDoc(this.state.fundId,this.state.subscriptionId,this.state.section).then(res => {
            this.close();
            if(res) {
                const docUrl = `${res.data.url}`
                this.FsnetUtil._openNewTab(docUrl)
            }
        }).catch(error => {
            this.close();
        })
    }

    showSendEditsToInvestorModal = () => {this.setState({showSendEditsToInvestor:true})}

    closeSendEditsToInvestorModal = () => {this.setState({showSendEditsToInvestor:false})}
    
    openUploadDocModal = () => {this.setState({uploadDocModal:true})}

    closeUploadDocModal = () => {this.setState({uploadDocModal:false},()=>{this.deleteFile()})}

    render() {
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
                <OverlayTrigger
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement="left"
                    delayShow={300}
                    delayHide={150}
                    rootClose
                >
                    <a href={href}>{children}</a>
                </OverlayTrigger>
            );
        }
        return (
            <div className={"edit-sub-aggrement " + (!this.state.showSectionsPage ? 'add-sub-aggrement' : '')}>
                {
                    this.state.showSectionsPage ?
                        <div>
                            <h1 className="subtext marginTop10 margin20">{this.state.jsonData.CLICK_BELOW_TO_MODIFY_SUBSCRIPTION_AGREEMENT}</h1>
                            <div className="margin20">
                                <Button className="fsnetSubmitButton btnEnabled" onClick={this.openPdf}>{this.state.jsonData.SUBSCRIPTION_AGREEMENT}</Button>
                                <Button className="fsnetSubmitButton btnEnabled marginLeft20" hidden={this.state.subscriptionId !== 0} onClick={this.questionList}>{this.state.jsonData.ADD_QUESTION}</Button>
                                <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_ADD_QUESTION}>
                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                </LinkWithTooltip>
                                {/* {
                                    this.state.show ?
                                        <Button className="fsnetSubmitButton btnEnabled pull-right" onClick={this.submitToInvestor}>{this.state.jsonData.SUBMIT_INVESTOR}</Button>
                                        : ''
                                } */}
                            </div>
                            <PanelGroup
                                accordion
                                id="accordion-controlled-example"
                                activeKey={this.state.activeKey}
                                onSelect={this.handleSelect}
                            >
                                <Panel eventKey="1">
                                    <Panel.Heading>
                                        <Panel.Title toggle><a onClick={(e)=>this.openSection(1,'one')}>{this.state.jsonData.SECTION_1}</a></Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <div className="edit-container"><Button className="fsnetSubmitButton btnEnabled width200" onClick={this.openRestoreModal}>{this.state.jsonData.RESTORE_BTN}</Button></div>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.downloadDoc}>{this.state.jsonData.DOWNLOAD}</Button>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.openUploadDocModal}>{this.state.jsonData.UPLOAD}</Button>
                                    </Panel.Body>
                                </Panel>
                                <Panel eventKey="2">
                                    <Panel.Heading>
                                        <Panel.Title toggle><a onClick={(e)=>this.openSection(2,'two')}>{this.state.jsonData.SECTION_2} & {this.state.jsonData.SECTION_3} & {this.state.jsonData.SECTION_4} & {this.state.jsonData.SECTION_5}</a></Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <div className="edit-container"><Button className="fsnetSubmitButton btnEnabled width200" onClick={this.openRestoreModal}>{this.state.jsonData.RESTORE_BTN}</Button></div>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.downloadDoc}>{this.state.jsonData.DOWNLOAD}</Button>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.openUploadDocModal}>{this.state.jsonData.UPLOAD}</Button>
                                    </Panel.Body>
                                </Panel>
                                <Panel eventKey="3">
                                    <Panel.Heading>
                                        <Panel.Title toggle><a onClick={(e)=>this.openSection(3,'three')}>{this.state.jsonData.SECTION_3}</a></Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <div className="edit-container"><Button className="fsnetSubmitButton btnEnabled width200" onClick={this.openRestoreModal}>{this.state.jsonData.RESTORE_BTN}</Button></div>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.downloadDoc}>{this.state.jsonData.DOWNLOAD}</Button>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.openUploadDocModal}>{this.state.jsonData.UPLOAD}</Button>
                                    </Panel.Body>
                                </Panel>
                                <Panel eventKey="4">
                                    <Panel.Heading>
                                        <Panel.Title toggle><a onClick={(e)=>this.openSection(4,'four')}>{this.state.jsonData.SECTION_4}</a></Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <div className="edit-container"><Button className="fsnetSubmitButton btnEnabled width200" onClick={this.openRestoreModal}>{this.state.jsonData.RESTORE_BTN}</Button></div>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.downloadDoc}>{this.state.jsonData.DOWNLOAD}</Button>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.openUploadDocModal}>{this.state.jsonData.UPLOAD}</Button>
                                    </Panel.Body>
                                </Panel>
                                <Panel eventKey="5">
                                    <Panel.Heading>
                                        <Panel.Title toggle><a onClick={(e)=>this.openSection(5,'five')}>{this.state.jsonData.SECTION_5}</a></Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <div className="edit-container"><Button className="fsnetSubmitButton btnEnabled width200" onClick={this.openRestoreModal}>{this.state.jsonData.RESTORE_BTN}</Button></div>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.downloadDoc}>{this.state.jsonData.DOWNLOAD}</Button>
                                        <Button className="fsnetSubmitButton btnEnabled width120" onClick={this.openUploadDocModal}>{this.state.jsonData.UPLOAD}</Button>
                                    </Panel.Body>
                                </Panel>
                            </PanelGroup>
                            <input type="file" id="uploadBtn" className="hide" onChange={(e) => this.handleChange(e)} />
                            {
                                this.state.subscriptionId != 0 ?
                                    <div className="footer-nav">
                                        <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.proceedToNext}>Save changes</Button>
                                    </div>
                                :
                                    <div className="footer-nav marginLeft20" hidden={this.state.subscriptionId !== 0}>
                                        <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                                        <i className="fa fa-chevron-right" onClick={this.proceedToNext} aria-hidden="true"></i>
                                    </div> 
                            }
                        </div>
                        :
                        <div>
                            <Button className="fsnetSubmitButton btnEnabled marginTop20 marginLeft20 width200" onClick={this.openAddQuestionModal}>{this.state.jsonData.QUESTION_BTN}</Button>
                            {this.state.questionList.length > 0 ?
                                <Row className="full-width marginTop30">
                                    <div className="name-heading marginLeft75 width320">
                                        Question Text
                                </div>
                                    <div className="name-heading marginLeft75">
                                        Type
                                </div>
                                </Row> : ''
                            }
                            <div className={"addQuestionContainer marginLeft30 " + (this.state.questionList.length === 0 ? 'borderNone' : 'marginTop10')}>
                                {this.state.questionList.length > 0 ?
                                    this.state.questionList.map((record, index) => {
                                        return (

                                            <div className="userRow" key={index}>
                                                <div className="lp-name">{record['question']}</div>
                                                <div className="question-type">{record['typeOfQuestion'] == 1 ? 'Text' : ''}{record['typeOfQuestion'] == 2 ? 'True/False' : ''}{record['typeOfQuestion'] == 3 ? 'Yes/No' : ''}</div>
                                                <i className="lp-edit-icon fa fa-pencil-square-o cursor" aria-hidden="true" onClick={() => { this.editQuestion(record) }}></i>
                                                <i className="lp-trash-icon fa fa-trash cursor" aria-hidden="true" onClick={() => this.openDeleteQuestionModal(record['id'])}></i>
                                            </div>
                                        );
                                    })
                                    :
                                    <div className="title margin20 text-center">{this.state.jsonData.NO_QUESTIONS}</div>
                                }
                            </div>
                            <div className="footer-nav">
                                <i className="fa fa-chevron-left" onClick={(e) => this.setState({ showSectionsPage: true })} aria-hidden="true"></i>
                            </div>
                        </div>
                }

                <Modal id="restoreModal" show={this.state.restoreModal} onHide={this.closeRestoreModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body className="marginTopNone">
                        <h1 className="title-md">{this.state.jsonData.RESTORE_MODAL_CONFIRMATION_TEXT}</h1>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.closeRestoreModal}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.restoreTextFn}>{this.state.jsonData.YES}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="restoreModal" show={this.state.deleteQuestionModal} onHide={this.closeDeleteQuestionModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title-md height58">{this.state.jsonData.DELETE_TITLE}</h1>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeDeleteQuestionModal}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.deleteQuestion}>{this.state.jsonData.YES}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="addQuestionModal" show={this.state.addQuestionModal} onHide={this.closeAddQuestionModal} dialogClassName="addQuestionDialog">
                    <Modal.Header className="heightNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <h1 className="title-md">{this.state.questionId ? 'Edit' : 'Add'} Question</h1>
                        <Row className="marginBot15">
                            <label className="label-text">Type</label>
                            <FormControl name='typeOfQuestion' defaultValue={0} value={this.state.typeOfQuestion} className={"selectFormControl " + (this.state.typeOfQuestionBorder ? 'inputError' : '')} componentClass="select" onChange={(e) => this.handleInputChangeEvent(e, 'text', 'typeOfQuestion', 'TYPE_REQUIRED')} onBlur={(e) => this.handleInputChangeEvent(e, 'text', 'typeOfQuestion', 'TYPE_REQUIRED')}>
                                <option value="">Select Type</option>
                                <option value={1}>Text</option>
                                <option value={2}>True/False</option>
                                <option value={3}>Yes/No</option>
                            </FormControl>
                            <span className="error">{this.state.typeMsz}</span>
                        </Row>
                        <Row className="marginBot15">
                            <label className="label-text">Required Filed</label>
                            <Radio name="isRequired" inline checked={this.state.isRequired == 1} onChange={(e) => this.handleInputChangeEvent(e, 'radio', 'isRequired', 1)}>
                                Mandatory
                                <span className="radio-checkmark"></span>
                            </Radio>
                            <Radio name="isRequired" className="marginLeft15" checked={this.state.isRequired == 0} inline onChange={(e) => this.handleInputChangeEvent(e, 'radio', 'isRequired', 0)}>
                                Optional
                                <span className="radio-checkmark"></span>
                            </Radio>
                        </Row>
                        <Row className="marginBot15">
                            <label className="label-text">Question Title</label>
                            <FormControl type="text" name="questionTitle" value={this.state.questionTitle} className={"inputFormControl " + (this.state.questionTitleBorder ? 'inputError' : '')} maxLength="50" onChange={(e) => this.handleInputChangeEvent(e, 'text', 'questionTitle', 'QUESTION_TITLE_REQUIRED')} onBlur={(e) => this.handleInputChangeEvent(e, 'text', 'questionTitle', 'QUESTION_TITLE_REQUIRED')} />
                            <span className="error">{this.state.questionTitleMsz}</span>
                        </Row>
                        <Row className="marginBot15">
                            <label className="label-text">Question</label>
                            <FormControl type="text" name="question" value={this.state.question} className={"inputFormControl " + (this.state.questionBorder ? 'inputError' : '')} onChange={(e) => this.handleInputChangeEvent(e, 'text', 'question', 'QUESTION_REQUIRED')} onBlur={(e) => this.handleInputChangeEvent(e, 'text', 'question', 'QUESTION_REQUIRED')} />
                            <span className="error">{this.state.questionMsz}</span>
                        </Row>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeAddQuestionModal}>{this.state.jsonData.CANCEL}</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className={"fsnetSubmitButton width200 " + (this.state.addQuestionPageValid ? 'btnEnabled' : 'pointerNone')} onClick={this.saveQuestionFn}>{this.state.jsonData.SAVE}</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Loader isShow={this.state.showModal}></Loader>

                <Modal id="confirmFundModal" show={this.state.showSendEditsToInvestor}  onHide={this.closeSendEditsToInvestorModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="title-md">You have made changes to the Subscription Agreement. Click "Send Edits" to save changes and notify close-ready investors. Otherwise, click Cancel to continue editing.</div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeSendEditsToInvestorModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.submitToInvestor}>Send Edits</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
                <Modal id="confirmFundModal" show={this.state.uploadDocModal}  onHide={this.closeUploadDocModal} dialogClassName="confirmFundDialog editSubModalDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="title-md">Remove Signature Pages and Investor Questionnaire, if any, before uploading.</div>
                        <div className="title-md marginTop10">To review our standard Investor Questionnaire, click "Cancel" then click View Subscription Agreement and see pages 3-6. If you require additional questions, please click "View/Add Investor Questions"</div>
                        {
                            this.state.uploadDocFileName ?
                                <div className="upload-doc-name marginTop20">
                                    <div className="title-md marginLeft10 width600 text-center breakWord inline-block">{this.state.uploadDocFileName}<i className="fa fa-trash cursor-pointer marginLeft10" onClick={this.deleteFile}></i></div>
                                </div>

                                :
                                <div className="width650 text-center marginTop20">
                                    <Button className="fsnetSubmitButtonMd width250 btnEnabled marginTop10" onClick={this.uploadDoc}>Upload Document</Button>

                                </div>
                        }
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeUploadDocModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className={"fsnetSubmitButton width200 " + (this.state.uploadDocFileName ? 'btnEnabled' : 'disabled')} disabled={!this.state.uploadDocFileName} onClick={this.uploadSectionDocFile}>Upload</Button>
                            </Col>
                        </Row>   
                    </Modal.Body>
                </Modal>
            </div>
        );
    }

}

export default Step4Component;

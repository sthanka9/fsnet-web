import React, { Component } from 'react';
import '../createfund.component.css';
import { Button,Modal,Row,Col } from 'react-bootstrap';
import { reactLocalStorage } from 'reactjs-localstorage';
import Loader from '../../../widgets/loader/loader.component';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetUtil } from '../../../util/util';
import {Constants} from '../../../constants/constants';
import FileDrop from 'react-file-drop';
import { PubSub } from 'pubsub-js';

class Step3Component extends Component {

    constructor(props){
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            uploadDocFile: {},
            uploadFileName: '',
            uploadDocSize:'',
            firmId:null,
            fundId:null,
            createdFundData:[],
            uploadFundPageValid: false,
            documentLink: '',
            jsonData:{}
        }
        PubSub.publish('pageNumber', {type:'sideNav', page: 'upload'});
    }

    componentDidMount() {
        this.getJsonData();
        let firmId = reactLocalStorage.getObject('firmId'); 
        // var url = window.location.href;
        // var parts = url.split("/");
        // var urlSplitFundId = parts[parts.length - 1];
        // let fundId = urlSplitFundId;
        let fundId = this.FsnetUtil._getId();
        this.setState({
            fundId: fundId,
            firmId:firmId
        })
        if(fundId !== undefined) {
            this.getFundDetails(fundId);
        }
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('fundAgreement').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    openDocument = () => {
        const docUrl = `${this.state.documentLink}?token=${this.FsnetUtil.getToken()}`
        this.FsnetUtil._openDoc(docUrl)
    }
    getFundDetails = (fundId) => {
        this.open();
            this.Fsnethttp.getFund(fundId).then(result=>{
                this.close();
                this.FsnetUtil.setLeftNavHeight();
                PubSub.publish('fundData', result.data.data);
                if(result.data) {
                    this.setState({ createdFundData: result.data.data, fundId: result.data.data.id }, () => this.updateDoc());
                } else {
                    this.setState({
                        createdFundData: [],
                    })
                }
            })
            .catch(error=>{
                this.close();
                this.setState({
                    createdFundData: []
                })
            });
    }

    updateDoc = () => {
        let data = this.state.createdFundData;
        if(data.partnershipDocument){
            this.setState({
                uploadFileName: data.partnershipDocument.originalname,
                uploadDocFile: data,
                uploadDocSize: (data.partnershipDocument.size/ this.Constants.SIZE_MB).toFixed(4)+' MB',
                uploadFundPageValid: true,
                documentLink: data.partnershipDocument.url
            })
        }
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    proceedToNext = () => {
        if(['New-Draft','Admin-Draft'].indexOf(this.state.createdFundData.status) > -1 && this.state.uploadFileName) {
            if(this.state.createdFundData.partnershipDocument) {
                if(this.state.uploadFileName === this.state.createdFundData.partnershipDocument.originalname) {
                    this.getFundDetails(this.state.fundId);
                    this.props.history.push('/createfund/investors/'+ this.FsnetUtil._encrypt(this.state.fundId));
                } else {
                    this.uploadDocApi()
                }
            } else {
                this.uploadDocApi()
            }
        } else {
            this.props.history.push('/createfund/investors/'+ this.FsnetUtil._encrypt(this.state.fundId));
        }
            
    }

    uploadDocApi = () => {
        this.open();
            //call the upload doc api
            var formData = new FormData();
            formData.append("fundId", this.state.fundId);
            formData.append("fundDoc", this.state.uploadDocFile);
            this.Fsnethttp.uploadDocumentToFund(this.state.fundId, formData).then(result=>{
                this.close();
                this.props.history.push('/createfund/investors/'+ this.FsnetUtil._encrypt(this.state.fundId));
            })
            .catch(error=>{
                this.close();
                if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        errorMessage: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        errorMessage: this.Constants.INTERNAL_SERVER_ERROR,
                    });

                }
            });
    }

    deleteFile = () => {
            let postObj = {fundId: this.state.fundId};
            this.open()
            this.Fsnethttp.deleteFile(postObj).then(result=>{
                this.close();
                document.getElementById('uploadBtn').value = "";
                this.getFundDetails(this.state.fundId);
                if(result) {
                    this.setState({
                        uploadDocFile: {},
                        uploadFileName: '',
                        uploadFundPageValid: false,
                        showDeleteAgreement:false,
                        errorMessage: ''
                    })
                }
            })
            .catch(error=>{
                this.close();
                this.setState({
                    uploadDocFile: {},
                    uploadFileName: '',
                    uploadFundPageValid: false
                })
            });
    }

    
    proceedToBack = () => {
        this.props.history.push('/createfund/editSubForm/'+ this.FsnetUtil._encrypt(this.state.fundId));
    }

    uploadBtnClick = () => {
        document.getElementById('uploadBtn').click();
    }

    checkDocAlreadyuploaded = () => {
        if(this.state.uploadFileName) {
            this.showDeleteAgreementModal();
            document.getElementById('uploadBtn').value = "";
        }
    }

    //Upload patnership document.
    handleChange = (event, type) => {
        this.checkDocAlreadyuploaded();
        let obj;
        if(type=== 'drop') {
            obj = event;
        } else {
            obj = event.target.files
        }
        let reader = new FileReader();
        if(obj && obj.length > 0) {
            this.uploadFile = obj[0];
            let sFileName = obj[0].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            if(sFileExtension !== 'pdf') {
                document.getElementById('uploadBtn').value = "";
                alert(this.Constants.UPLOAD_DOC_REQUIRED)
                return true;
            }
            //File 10MB limit
            if(this.uploadFile.size <= this.Constants.SIZE_LIMIT) {
                this.setState({
                    uploadDocFile : obj[0],
                    uploadDocSize: (this.uploadFile.size / this.Constants.SIZE_MB).toFixed(2)+' MB'
                });
                reader.readAsDataURL(this.uploadFile);
                this.setState({
                    uploadFileName: obj[0].name,
                    uploadFundPageValid: true
                });
                // reader.onload = () => {
                //     this.setState({
                //         currentUserImage : reader.result,
                //     });
                // }
            } else {
                document.getElementById('uploadBtn').value = "";
               alert('Fund document should be less than 10 MB.')
            }
        }
    }

    closeDeleteAgreementModal = () => {
        this.setState({showDeleteAgreement:false})
    }

    showDeleteAgreementModal = () => {
        this.setState({showDeleteAgreement:true})
    }

    render() {
        return (
            <div className="step3Class marginTop6">
                <h1 className="uploadFundDocument">{this.state.jsonData.UPLOAD_FUND_DOCUMENTS}</h1>
                <div className="chooseFileMargin">
                    <h1 className="title">{this.state.jsonData.CHOOSE_FILES_UPLOAD}</h1>
                    <div className="subtext">{this.state.jsonData.UPLOAD_YOUR_LPA}</div>
                    <div className="subtext marginTop15">{this.state.jsonData.ENSURE_FUND_AGREEMENT}</div>
                    {
                        this.state.createdFundData.status === 'Admin-Draft' || this.state.createdFundData.status === 'New-Draft' ?
                        <div className="uplodFileContainer marginTop46">
                            <input type="file" id="uploadBtn" className="hide" onChange={ (e) => this.handleChange(e) } />
                            <FileDrop onDrop={(e) => this.handleChange(e, 'drop')}>
                                <Button className="uploadFileBox" onClick={this.uploadBtnClick}></Button>
                                <span className="uploadFileSubtext">{this.Constants.DROP_YOUR_FILES_FROM_DESKTOP}</span>
                            </FileDrop>
                            <div className="docNameDivAlign"><div className="upload-doc-name">{this.state.uploadFileName} </div> </div>
                            <div className="filesize" hidden={this.state.uploadFileName ===''}>{this.state.uploadDocSize}  <i className="fa fa-trash cursor-pointer" onClick={this.deleteFile}></i></div>
                            
                        </div>
                        :
                        <div className="docContainer marginTop50"><a onClick={this.openDocument} className="upload-doc-name">{this.state.uploadFileName} </a> {this.state.uploadDocSize}</div>
                    }
                </div>
                <div className="error">{this.state.errorMessage}</div>
                <div className="footer-nav">
                    <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                    {/* <i className={"fa fa-chevron-right " + (!this.state.uploadFundPageValid ? 'disabled' : '')} onClick={this.proceedToNext}  aria-hidden="true"></i> */}
                    <i className="fa fa-chevron-right" onClick={this.proceedToNext}  aria-hidden="true"></i>
                </div>
                <Modal id="LPDelModal" backdrop="static" show={this.state.showDeleteAgreement} onHide={this.closeDeleteAgreementModal} dialogClassName="LPDelModalDialog">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>Delete Fund Agreement?</Modal.Title>
                    <Modal.Body>
                        <div className="subtext">It is not possible to upload a Fund Agreement when a Fund Agreement is already uploaded. If you wish to replace the Fund Agreement with a different Fund Agreement, please delete the existing Fund Agreement first then click "Upload File" again to upload the replacement.</div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeDeleteAgreementModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="paddingZero">
                                <Button type="button" className={"fsnetSubmitButton closeFundSubmitBtn btnEnabled"} onClick={this.deleteFile}>Confirm Deletion</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default Step3Component;




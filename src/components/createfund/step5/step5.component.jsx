import React, { Component } from 'react';
import '../createfund.component.css';
import { Button, Checkbox as CBox, Row, Col, FormControl, Tooltip, OverlayTrigger, Modal } from 'react-bootstrap';
import userDefaultImage from '../../../images/default_user.png';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetUtil } from '../../../util/util';
import { Constants } from '../../../constants/constants';
import Loader from '../../../widgets/loader/loader.component';
import { reactLocalStorage } from 'reactjs-localstorage';
import PhoneInput from 'react-phone-number-input';
import ToastComponent from '../../toast/toast.component';
import plusImg from '../../../images/plus.svg';
import 'react-phone-number-input/rrui.css';
import 'react-phone-number-input/style.css';
import { PubSub } from 'pubsub-js';
import LpModalComponent from '../../lp/lpmodals/lpmodals.component';

var close = {}
class Step5Component extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.Constants = new Constants();
        this.state = {
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            showAddLpModal: false,
            showNameAsc: true,
            showOrgAsc: true,
            dateAsc: false,
            investmentAsc: true,
            getLpList: [],
            firmId: null,
            fundId: null,
            lpErrorMsz: '',
            lpSelectedList: [],
            lpScreenError: '',
            orgName: '',
            step5PageValid: false,
            selectedLpObj: {},
            noDelegatesMsz: '',
            fundStatus: 'Open',
            fundObj: {},
            jsonData: {},
            assignedLps: [],
            unAssignedLps: [],
            selectedLps: [],
            unSelectedLps: [],
            lpType: null,
            modalType: null,
            inviteConfirmModal: false,
        }
        PubSub.subscribe('fundData', (msg, data) => {
            this.setState({
                fundId: data.id
            }, () => {
                this.showLps(data)
            })

        });

        PubSub.publish('pageNumber', { type: 'sideNav', page: 'lp' });

        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        PubSub.publish('lpProfileModal', data);
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    handleLpDelegateShow = (data) => () => {
        const name = this.FsnetUtil.getFullName(data);
        let fundObj = this.state.fundObj;
        fundObj['lpName'] = name
        fundObj['lpStatus'] = data.status
        PubSub.publish('openModal', { obj: { id: data.subscriptionId, lpId:data.id, fundId: this.state.fundId, fundObj:fundObj, lpStatus:data.status }, type: 'lpDelegate', isDelegateAddingFromFund:true });
    }

    addLpBtn = (type) => () => {
        PubSub.publish('openLpModal', { fundObj: this.state.fundObj, type: type });
    }
 
    // addLpOffineBtn = () => {
    //     PubSub.publish('openLpOfflineModal', {fundObj:this.state.fundObj,type:'lp'});
    // }

    editLpBtn = (lpObj, type) => () => {
        const value = lpObj.accountType == 'OfflineLP' ? true : false
        this.setState({
            lpType: type,
            selectedLpObj: lpObj,
            modalType: 'edit'
        }, () => {
            PubSub.publish('openLpEditModal', { fundObj: this.state.fundObj, lpObj: lpObj, type: type, isOfflineLP: value });
        })
    }

    deleteLp = (data, type) => () => {
        const value = data.accountType == 'OfflineLP' ? true : false
        this.setState({
            lpType: type,
            selectedLpObj: data,
            modalType: 'delete'
        }, () => {
            PubSub.publish('openLpDelFundModal', { data: this.state.fundObj, delegateId: data.id, type: type, subscriptionId: data.subscriptionId, isOfflineLP: value });
        })
    }

    deleteLpDelegate = (data,record) => () => {
        let fundObj = this.state.fundObj;
        fundObj['fundId'] = this.state.fundId;
        fundObj['subscriptionId'] = record.subscriptionId;
        this.setState({
            selectedLpObj: data,
            modalType: 'delete'
        }, () => {
            PubSub.publish('openLpDelegateModal', { data: this.state.fundObj, delegateId: data.id, subscriptionId:record.subscriptionId, fundId: this.state.fundId, isDelegateDeletingFromFund:true});
        })
    }

    proceedToNext = () => {
        if (this.state.assignedLps.length > 0) {
            this.navigateToNextPage()
        } else {
            this.setState({
                lpScreenError: this.Constants.LP_REQUIRED
            })
        }
    }

    navigateToNextPage = () => {
        if (this.state.fundStatus === 'Open' || this.state.fundStatus === 'New-Draft' || this.state.fundStatus === 'Admin-Draft') {
            this.props.history.push('/createfund/review/' + this.state.fundId);
        } else {
            //Using window location because of pubsub issue.
            window.location.href = `/fund/view/${this.state.fundId}`;
        }
    }

    proceedToBack = () => {
        this.props.history.push('/createfund/upload/' + this.FsnetUtil._encrypt(this.state.fundId));
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    componentDidMount() {
        this.getJsonData();
        let firmId = reactLocalStorage.getObject('firmId');
        // var url = window.location.href;
        // var parts = url.split("/");
        // var urlSplitFundId = parts[parts.length - 1];
        // let fundId = urlSplitFundId;
        let fundId = this.FsnetUtil._getId();
        this.setState({
            fundId: fundId,
            firmId: firmId
        }, () => {
            this.getLPApi()
        })
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('assignInvestors').then(result => {
            this.setState({
                jsonData: result.data
            })
        });

    }

    getLPApi = () => {
        this.open();
        this.Fsnethttp.getFund(this.state.fundId).then(result => {
            this.close();
            PubSub.publish('fundData', result.data.data);
            const responseData = result.data.data;
            const sortArray = responseData.lps.fundLps.length > 0 ? this.FsnetUtil._sortArrayWithDate(responseData.lps.fundLps) : []
            responseData.lps.fundLps = sortArray;
            this.setState({
                fundObj: responseData,
                fundStatus: responseData.status,
                assignedLps: responseData.lps.fundLps,
                unAssignedLps: responseData.lps.firmLps,
            })
        })
            .catch(error => {
                this.close();
                this.setState({
                    noDelegatesMsz: this.Constants.NO_LPS
                })
            });
    }

    showLps = (data) => {
        this.setState({ fundObj: data, assignedLps: data.lps.fundLps, unAssignedLps: data.lps.firmLps, fundStatus: data.status, noDelegatesMsz: this.Constants.NO_LPS });
    }


    sortLp = (e, colName, sortVal) => {

        let firmId = this.state.firmId;
        let fundId = this.state.fundId;
        if (this.state.assignedLps && this.state.assignedLps.length > 1) {
            this.open();
            this.Fsnethttp.getLpSort(firmId, fundId, colName, sortVal).then(result => {
                if (result.data && result.data.data.length > 0) {
                    this.close();
                    this.setState({ assignedLps: result.data.data });
                    if (colName === 'firstName') {
                        if (sortVal === 'desc') {
                            this.setState({
                                showNameAsc: true
                            })
                        } else {
                            this.setState({
                                showNameAsc: false
                            })
                        }
                    } else if (colName === 'date') {
                        if (sortVal === 'desc') {
                            this.setState({
                                dateAsc: true
                            })
                        } else {
                            this.setState({
                                dateAsc: false
                            })
                        }
                    } else {
                        if (sortVal === 'desc') {
                            this.setState({
                                showOrgAsc: true
                            })
                        } else {
                            this.setState({
                                showOrgAsc: false
                            })
                        }
                    }

                } else {
                    this.close();
                    this.setState({
                        getLpList: [],
                        showNameAsc: false
                    })
                }

            })
                .catch(error => {
                    this.close();
                    this.setState({
                        getLpList: []
                    })

                });
        }
    }

    handleCheckBox = (data, value) => () => {
        if (value) {
            //Add Id in unselected LP List
            //Add obj in unassigned lp list
            //Remove this obj in assigned lp list
            const getLpId = this.state.assignedLps.findIndex(obj => obj.id == data.id);
            const assignedLpsTemp = [...this.state.assignedLps];
            assignedLpsTemp.splice(getLpId, 1)
            this.setState({
                unAssignedLps: [...this.state.unAssignedLps, data],
                unSelectedLps: [...this.state.unSelectedLps, data.id],
                assignedLps: assignedLpsTemp,
            }, () => {
                this.updateFundData();
            })

        } else {
            const getLpId = this.state.unAssignedLps.findIndex(obj => obj.id == data.id);
            const unassignedLpsTemp = [...this.state.unAssignedLps];
            unassignedLpsTemp.splice(getLpId, 1)
            this.setState({
                selectedLps: [...this.state.selectedLps, data.id],
                assignedLps: [...this.state.assignedLps, data],
                unAssignedLps: unassignedLpsTemp,
            }, () => {
                this.updateFundData();
            })
        }
    }

    updateFundData = () => {
        let fundData = { ...this.state.fundObj };
        fundData.lps.firmLps = []
        fundData.lps.fundLps = []
        fundData.lps.firmLps = this.state.unAssignedLps;
        fundData.lps.fundLps = this.state.assignedLps;
        this.setState({
            fundObj: fundData
        })
    }

    addLpFn = (data) => () => {
        this.open();
        const postObj = { fundId: this.state.fundId, lpId: data.id }
        this.Fsnethttp.assingLp(postObj).then(result => {
            this.close();
            data['trackerTitle'] = result.data.trackerTitle;
            data['subscriptionId'] = result.data.subscriptionId;
            data['status'] = result.data.status;
            data['isInvestorInvited'] = result.data.isInvestorInvited;
            const getLpId = this.state.unAssignedLps.findIndex(obj => obj.id == data.id);
            const unassignedLpsTemp = [...this.state.unAssignedLps];
            const assignedLpsTemp = [...this.state.assignedLps];
            unassignedLpsTemp.splice(getLpId, 1)
            assignedLpsTemp.unshift(data)
            this.setState({
                unAssignedLps: unassignedLpsTemp,
                assignedLps: assignedLpsTemp
            }, () => {
                this.updateFundData()
            })
        })
            .catch(error => {
                this.close();
            });
    }


    getPendingInvitationInvestors = () => {
        let lpIds = this.state.assignedLps.map(obj => {
            if (obj.status == 'Invitation-Pending' && obj['isInvestorInvited'] == 1) {
                return obj.id
            } else { return null }
        });
        lpIds = lpIds.filter(obj => obj != null);
        return lpIds;
    }

    checkInviteModalToShow = () => {
        let investors = this.getPendingInvitationInvestors();
        if(investors.length > 0) {
            this.openInviteConfirmModal();
        } else {
            this.proceedToNext();
        }
    }

    inviteInvestors = (isFundStarted) => () => {
        this.open();
        let lpIds = this.state.assignedLps.map(obj => {
            if (obj.status == 'Invitation-Pending' && obj['isInvestorInvited'] == 1) {
                return obj.id
            } else { return null }
        });
        lpIds = lpIds.filter(obj => obj != null)
        let uncheckedLpIds = this.state.assignedLps.map(obj => {
            if (obj.status == 'Invitation-Pending' && obj['isInvestorInvited'] == 0) {
                return obj.id
            } else { return null }
        });
        uncheckedLpIds = uncheckedLpIds.filter(obj => obj != null)
        const postObj = { fundId: this.state.fundId, lpIds: lpIds,uncheckedLpIds:uncheckedLpIds }
        this.Fsnethttp.inviteInvestors(postObj).then(result => {
            this.close();
            if(!isFundStarted) {
                this.closeInviteConfirmModal();
                if(lpIds.length > 0) {
                    this.setState({
                        showToast: true,
                        toastMessage: 'Fund Invitations have been sent to the Investors successfully.',
                        toastType: 'success',
                    })
                }
                setTimeout(() => {
                    this.proceedToNext();
                }, 1000);
            } else {
                this.proceedToNext();
            }
        })
        .catch(error => {
            this.close();
        });

    }

    closeToast(timed) {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    sortLpByDate = (sort) => () => {
        if (this.state.assignedLps.length > 1) {
            const sortData = sort == 0 ? this.FsnetUtil._sortArrayWithDateAsc(this.state.assignedLps) : this.FsnetUtil._sortArrayWithDate(this.state.assignedLps)
            this.setState({
                dateAsc: sort == 0 ? true : false,
                assignedLps: sortData
            })
        }

    }

    sortInvestmentByName = (sort,type) => () => {
        let sortData;
        if (this.state.assignedLps.length > 1) {
            if(type == 'firstName') {
                sortData = sort == 0 ? this.FsnetUtil._sortUsingfirstNameAsc(this.state.assignedLps) : this.FsnetUtil._sortUsingfirstNameDesc(this.state.assignedLps)
            } else {
                sortData = sort == 0 ? this.FsnetUtil._sortUsingtrackerTitleAsc(this.state.assignedLps) : this.FsnetUtil._sortUsingtrackerTitleDesc(this.state.assignedLps)
            }
            this.setState({
                assignedLps: sortData
            },()=>{
                if(type == 'firstName') {
                    this.setState({
                        firstNameAsc: sort == 0 ? true : false,
                    })
                } else {
                    this.setState({
                        investmentAsc: sort == 0 ? true : false,
                    })
                }
            })
        }

    }

    openInviteConfirmModal = () => {
        if( this.state.assignedLps.length > 0) {
            this.setState({
                inviteConfirmModal: true
            })
        } else {
            this.setState({
                lpScreenError: this.Constants.LP_REQUIRED
            })
        }
    }

    closeInviteConfirmModal = () => {
        this.setState({
            inviteConfirmModal: false
        })
    }

    handleSendInvitation = (data) => (event) => {
        const status = data.status != 'Invitation-Pending' ? true : event.target.checked;
        const assignedLpsTemp = [...this.state.assignedLps];
        const getIndex = assignedLpsTemp.findIndex(obj => obj.id == data.id)
        let selectedLp = assignedLpsTemp.filter(obj => obj.id == data.id);
        selectedLp[0]['isInvestorInvited'] = status ? 1 : 0
        assignedLpsTemp[getIndex] = selectedLp[0]
        this.setState({
            assignedLps: assignedLpsTemp
        }, () => {
            this.updateFundData()
        })
    }

    render() {
        function LinkWithTooltip({ id, children, href, placement, tooltip }) {
            return (
                <OverlayTrigger
                    overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                    placement={placement ? placement : "right"}
                    delayShow={300}
                    delayHide={150}
                    rootClose
                >
                    <a href={href}>{children}</a>
                </OverlayTrigger>
            );
        }

        return (
            <div className="LpDelegatesContainer marginTop6">
                <h1 className="assignGp">{this.state.jsonData.ASSIGN_INVESTORS}</h1>
                <p className="Subtext marginBot20">{this.state.jsonData.SELECT_INVESTORS_LIST_BELOW}</p>
                <Button className="vanilla-button-md" onClick={this.addLpBtn('lp')}><i className="fa fa-plus paddingRight20"></i>{this.state.jsonData.INVESTOR}</Button>
                <Button className="vanilla-button-md marginLeft20" onClick={this.addLpBtn('offlineLp')}><i className="fa fa-plus paddingRight20"></i>Add Offline Investor</Button>
                <h1 className="assignGp marginTop20" hidden={this.state.assignedLps.length == 0}>Assigned Investors</h1>

                {this.state.assignedLps.length > 0 &&
                    <Row className="width1100 marginTop10">
                        {/* <div className="name-heading marginLeft75" hidden={!this.state.showNameAsc} onClick={(e) => this.sortLp(e, 'firstName', 'asc')}>
                            {this.state.jsonData.INVESTOR_NAME}
                                    <i className="fa fa-sort-asc" aria-hidden="true"  ></i>
                        </div>
                        <div className="name-heading marginLeft75" onClick={(e) => this.sortLp(e, 'firstName', 'desc')} hidden={this.state.showNameAsc}>
                            {this.state.jsonData.INVESTOR_NAME}
                                <i className="fa fa-sort-desc" aria-hidden="true"  ></i>
                        </div> */}
                        <div className="name-heading marginLeft75 width200 cursor" onClick={this.sortInvestmentByName(1,'firstName')} hidden={!this.state.firstNameAsc}>
                            {this.state.jsonData.INVESTOR_NAME}
                            <i className="fa fa-sort-asc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading marginLeft75 width200 cursor" onClick={this.sortInvestmentByName(0,'firstName')} hidden={this.state.firstNameAsc}>
                            {this.state.jsonData.INVESTOR_NAME}
                            <i className="fa fa-sort-desc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading width100Px cursor" hidden={!this.state.dateAsc}>
                            <span onClick={this.sortLpByDate(1)}>Invited</span>
                            <i className="fa fa-sort-asc" aria-hidden="true"></i>
                            <span className="paddingLeft5">
                                <LinkWithTooltip tooltip={this.state.jsonData.INVITED_TOOLTIP} placement="top">
                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                </LinkWithTooltip>
                            </span>
                        </div>
                        <div className="name-heading width100Px cursor" hidden={this.state.dateAsc}>
                            <span onClick={this.sortLpByDate(0)}>Invited</span>
                            <i className="fa fa-sort-desc" aria-hidden="true" ></i>
                            <span className="paddingLeft5">
                                <LinkWithTooltip tooltip={this.state.jsonData.INVITED_TOOLTIP} placement="top">
                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                </LinkWithTooltip>
                            </span>
                        </div>
                        <div className="name-heading marginLeft20 cursor" onClick={this.sortInvestmentByName(1)} hidden={!this.state.investmentAsc}>
                            Investment
                            <i className="fa fa-sort-asc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading marginLeft20 cursor" onClick={this.sortInvestmentByName(0)} hidden={this.state.investmentAsc}>
                            Investment
                            <i className="fa fa-sort-desc" aria-hidden="true"></i>
                        </div>
                        {/* <div className="name-heading marginLeft50" onClick={(e) => this.sortLp(e, 'organizationName', 'asc')} hidden={!this.state.showOrgAsc}>
                            {this.state.jsonData.ORGANIZATION}
                                <i className="fa fa-sort-asc" aria-hidden="true"></i>
                        </div>
                        <div className="name-heading marginLeft50" onClick={(e) => this.sortLp(e, 'organizationName', 'desc')} hidden={this.state.showOrgAsc}>
                            {this.state.jsonData.ORGANIZATION}
                                <i className="fa fa-sort-desc" aria-hidden="true" ></i>
                        </div> */}
                        <div className="name-heading marginLeft50 width100Px">
                            {this.state.jsonData.ORGANIZATION}
                        </div>
                        <div className="name-heading marginLeft50 width100Px">
                            Add Delegate
                        </div>
                        {/* <div className="name-heading width100Px paddingLeft10">
                            Send Invite
                        </div> */}
                        <div className="name-heading width50">
                            Edit
                        </div>
                        <div className="name-heading width100Px">
                            Remove from Fund
                        </div>
                    </Row>
                }

                {this.state.assignedLps.length > 0 &&
                    <div className={"userLPContainer width1000 " + (this.state.assignedLps.length === 0 ? 'borderNone' : 'marginTop10')}>
                        {(this.state.assignedLps.length > 0) &&
                            this.state.assignedLps.map((record, index) => {
                                return (
                                    <div>
                                        <div className="userRow" key={index}>
                                            <label className="userImageAlt">
                                                {
                                                    record['profilePic'] ?
                                                        <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                        : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                }
                                            </label>
                                            <div className="lp-name width200 relative">
                                                <div className="ellipsis" title={this.FsnetUtil.getFullName(record)}>{this.FsnetUtil.getFullName(record)}</div>
                                                <span className="accountTypeName">{record['status'] == 'Invitation-Pending' ? 'Investor - Pending Invitation' : this.FsnetUtil.getAccountTypeName(record['accountType'])}</span>
                                            </div>
                                            <div className="lp-name width100Px paddingLeft15">
                                                {
                                                    record['isInvestorInvited'] == 2 ? 
                                                    <span hidden={record.accountType != 'LP'}>{record['invitedDate']}
                                                        {
                                                            (record['invitedDate'] && record['timezone']) ? <span> ({record['timezone']})</span> : ''
                                                        }
                                                    </span>
                                                    : 
                                                    <span hidden={record.accountType != 'LP'}>
                                                        <CBox className={"marginLeft10 "+ (record.status != 'Invitation-Pending' ? 'checkboxDisabled':'')} checked={record['isInvestorInvited'] == 1} title={record.status != 'Invitation-Pending' ? 'Invitation Sent' : ''} onChange={this.handleSendInvitation(record)}>
                                                            <span className="checkmark"></span>
                                                        </CBox>
                                                    </span>
                                                }
                                                
                                            </div>
                                            <div className="lp-name width250 paddingLeft35">{record['trackerTitle'] ? record['trackerTitle'] : '(New Investor Pending Input)'}</div>
                                            <div className="lp-name width150 paddingLeft30">{record['organizationName']}</div>
                                            <div className="lp-name width100Px"><img src={plusImg} alt="add" hidden={record.accountType != 'LP'} className="cursor paddingLeft20" onClick={this.handleLpDelegateShow(record)} /></div>
                                            <i className="lp-edit-icon fa fa-pencil-square-o cursor" aria-hidden="true" onClick={this.editLpBtn(record, 1)}></i>
                                            <i className="lp-edit-icon fa fa-minus cursor" title="Remove from Fund" aria-hidden="true" onClick={this.deleteLp(record, 1)}></i>
                                        </div>
                                        {(record.lpDelegatesList && record.lpDelegatesList.length > 0) &&
                                            record.lpDelegatesList.map((delegate, delegateIndex) => {
                                                return (
                                                    <div>
                                                        <div className="userRow" key={delegateIndex}>
                                                            <label className="userImageAlt">
                                                                {
                                                                    delegate['profilePic'] ?
                                                                        <img src={delegate['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(delegate)} />
                                                                        : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(delegate)} />
                                                                }
                                                            </label>
                                                            <div className="lp-name width200 relative">
                                                                <div className="ellipsis" title={this.FsnetUtil.getFullName(delegate)}>{this.FsnetUtil.getFullName(delegate)}</div>
                                                                <span className="accountTypeName">{delegate['status'] == 'Invitation-Pending' ? 'Investor - Pending Invitation' : this.FsnetUtil.getAccountTypeName(delegate['accountType'])}</span>
                                                            </div>
                                                            <div className="lp-name width100Px paddingLeft15">{delegate['invitedDate'] ? delegate['invitedDate'] : ''}
                                                                {
                                                                    (record['invitedDate'] && record['timezone']) ? <span> ({record['timezone']})</span> : ''
                                                                }
                                                            </div>
                                                            <div className="lp-name width250 paddingLeft35">{delegate['trackerTitle'] ? delegate['trackerTitle'] : '(New Investor Pending Input)'}</div>
                                                            <div className="lp-name width150 paddingLeft30"></div>
                                                            <div className="lp-name width100Px">
                                                                {/* <img src={plusImg} alt="add" className="cursor paddingLeft20" onClick={this.handleLpDelegateShow(delegate)} /> */}
                                                            </div>
                                                            <div className="lp-edit-icon paddingLeft25"></div>
                                                            {/* <i className="lp-edit-icon fa fa-pencil-square-o cursor" aria-hidden="true" onClick={this.editLpBtn(delegate)}></i> */}
                                                            <i className="lp-edit-icon fa fa-minus cursor" title="Remove from Fund" aria-hidden="true" onClick={this.deleteLpDelegate(delegate,record)}></i>
                                                        </div>
                                                    </div>
                                                );
                                            })
                                        }
                                    </div>
                                );
                            })
                        }
                    </div>
                }

                {this.state.unAssignedLps.length > 0 &&
                    <h1 className="assignGp marginTop20">Unassigned Investors</h1>
                }


                {this.state.unAssignedLps.length > 0 &&
                    <Row className="full-width marginTop10">
                        <div className="name-heading marginLeft75">
                            {this.state.jsonData.INVESTOR_NAME}
                        </div>
                        <div className="name-heading">
                            {this.state.jsonData.ORGANIZATION}
                        </div>
                        <div className="name-heading width120">
                            Add to Fund
                            &nbsp;
                            <span>
                                <LinkWithTooltip tooltip={this.state.jsonData.CHECK_BOX_TO_INVITE_EXISTING_INVESTOR} placement="top">
                                    <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                </LinkWithTooltip>
                            </span>
                        </div>
                        <div className="name-heading width50 marginLeft20">
                            Edit
                        </div>
                        <div className="name-heading width100Px marginLeft20">
                            Delete
                        </div>
                    </Row>
                }

                {this.state.unAssignedLps.length > 0 &&
                    <div className={"userLPContainer width750 " + (this.state.unAssignedLps.length === 0 ? 'borderNone' : 'marginTop10')}>
                        {this.state.unAssignedLps.length > 0 &&
                            this.state.unAssignedLps.map((record, index) => {
                                return (

                                    <div className="userRow" key={index}>
                                        <label className="userImageAlt">
                                            {
                                                record['profilePic'] ?
                                                    <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                    : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                            }
                                        </label>
                                        <div className="lp-name width200">{this.FsnetUtil.getFullName(record)}</div>
                                        <div className="lp-name width200 paddingLeft0 paddingRight20">{record['organizationName']}</div>
                                        <div className="inline-block width100Px">
                                            <CBox key={record['selected']} checked={false} onChange={this.addLpFn(record, false)}>
                                                <span className="checkmark"></span>
                                            </CBox>
                                        </div>

                                        <i className="lp-edit-icon fa fa-pencil-square-o cursor" aria-hidden="true" onClick={this.editLpBtn(record, 0)}></i>
                                        <i className="lp-trash-icon fa fa-trash cursor paddingLeft10" aria-hidden="true" onClick={this.deleteLp(record, 0)}></i>
                                    </div>
                                );
                            })
                        }
                    </div>
                }

                {(this.state.unAssignedLps.length == 0 && this.state.assignedLps.length == 0) && <div className="title minHeight100 marginTop50 text-center">{this.state.noDelegatesMsz}</div>}

                <div className="error">{this.state.lpScreenError}</div>

                {
                    ['Open', 'New-Draft', 'Admin-Draft'].indexOf(this.state.fundStatus) > -1
                        ?
                        <div className="footer-nav">
                            <i className="fa fa-chevron-left" onClick={this.proceedToBack} aria-hidden="true"></i>
                            <i className={"fa fa-chevron-right " + (this.state.assignedLps.length == 0 ? 'disabled' : '')} onClick={this.inviteInvestors(true)} aria-hidden="true"></i>

                        </div>
                        :
                        <div className="footer-nav">
                            <Button type="button" className={"fsnetSubmitButton " + ((this.state.assignedLps.length == 0 || this.getPendingInvitationInvestors().length === 0) ? 'disabled' : 'btnEnabled')} disabled={this.state.assignedLps.length == 0 || this.getPendingInvitationInvestors().length === 0} onClick={this.checkInviteModalToShow}>Save/Send Invitation</Button>
                        </div>
                }
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Modal id="confirmFundModal" show={this.state.inviteConfirmModal} onHide={this.closeInviteConfirmModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="title-md ulpoadSignText">Warning: You are about to send email invitations to this Fund for all investors in the Assigned Investors Table  that have a check mark in the “Invited" column.  If there are any investors you do not wish to send an email invitation to at this time, please click “Cancel” below and uncheck the investors.</div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12} className="text-right">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeInviteConfirmModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.inviteInvestors(false)}>Send Invitations</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <LpModalComponent></LpModalComponent>
            </div>
        );
    }
}

export default Step5Component;




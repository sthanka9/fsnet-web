
import React, { Component } from 'react';
import '../createfund.component.css';
import { Button, Radio, Row, Col, FormControl,Tooltip,OverlayTrigger } from 'react-bootstrap';
import Loader from '../../../widgets/loader/loader.component';
import {Fsnethttp} from '../../../services/fsnethttp';
import {Constants} from '../../../constants/constants';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';
import { FsnetUtil } from '../../../util/util';
import FundImage from '../../../images/fund-default@2x.png';

class Step1Component extends Component{

    constructor(props){
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            fundImageName: 'fund_Pic.jpg',
            currentFundImage : FundImage,
            fundPicFile: {},
            fundDetailsPageValid: false,
            legalEntity:'',
            legalEntityValid:false,
            legalEntityBorder: false,
            legalEntityMsz:'',
            fundCommonName:'',
            fundCommonNameValid:false,
            fundCommonNameBorder: false,
            fundCommonNameMsz:'',
            fundManagerTitle:'',
            fundManagerTitleValid:false,
            fundManagerTitleBorder: false,
            fundManagerTitleMsz:'',
            fundManagerCommonName:'',
            fundManagerCommonNameValid:false,
            fundManagerCommonNameBorder: false,
            fundManagerCommonNameMsz:'',
            fundManagerLegalEntityName:'',
            fundManagerLegalEntityNameBorder:false,
            fundManagerLegalEntityNameMsz:'',
            fundManagerLegalEntityNameValid: false,
            fundHardCap:'',
            fundTargetCommitment:'',
            capitalCommitmentByGP:'',
            percentageOfLPCommitment:'',
            percentageOfLPAndGPAggregateCommitment:'',
            aTextBoxDisabled: false,
            bTextBoxDisabled: false,
            cTextBoxDisabled: false,
            fundDetailspageError:'',
            createdFundData:{},
            fundId:'',
            firmId: null,
            fundHardCapCurrencyValue:'',
            fundTargetCommitmentCurrencyValue: '',
            capitalCommitmentByGPCurrencyValue: '',
            fundTargetCommitmentValid: true,
            fundTargetCommitmentBorder: false,
            fundTargetCommitmentMsz: '',
            fundHardCapValid: true,
            fundHardCapBorder: false,
            fundHardCapMsz: '',
            imageRemoved: true,
            fundStatus:'Open',
            percentageOfLPAndGPAggregateCommitmentPer:'',
            percentageOfLPCommitmentPer:'',
            generalPartnersCapitalCommitmentindicated:'',
            generalPartnersCapitalCommitmentindicatedValid:false,
            generalPartnersCapitalCommitmentindicatedMsz:'',
            firmIdValid:true,
            jsonData:{},
            firmIdsList:[]
        }
    }


    componentDidMount() {
        this.getJsonData()
        // var url = window.location.href;
        // var parts = url.split("/");
        // var urlSplitFundId = parts[parts.length - 1];
        // let fundId = urlSplitFundId;
        let fundId = this.FsnetUtil._getId();
        PubSub.publish('pageNumber', {type:'sideNav', page: 'funddetails'});
        this.setState({
            firmId: parseInt(this.FsnetUtil.getFirmId())
        },()=>{
            if(this.FsnetUtil.getUserRole() != 'GP' && !parseInt(fundId)) {
                this.getListOfFirmIds();
            }
        })
        if(fundId && fundId !== 'funddetails') {
            this.open();
            this.setState({
                fundId: fundId,
            })
            this.Fsnethttp.getFund(fundId).then(result=>{
                this.close();
                this.FsnetUtil.setLeftNavHeight();
                if(result.data) {
                    this.updateFundDetails(result.data.data)
                } else {
                    this.setState({
                        createdFundData: {},
                    })
                }
            })
            .catch(error=>{
                this.close();
                this.setState({
                    createdFundData: {}
                })
            });
        }
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('fundDetails').then(result=>{
            this.setState({
                jsonData:result.data
            })
        });
        
    }

    getListOfFirmIds = () => {
        const delegateId = this.FsnetUtil.getUserId();
        if(delegateId) {
            this.Fsnethttp.getVcFirms(delegateId).then(result=>{
                this.close();
                this.setState({
                    firmIdsList:result.data
                })
            })
            .catch(error=>{
                this.close();
            });
        }
    }

    updateFundDetails = (data) => {
        this.setState({ createdFundData: data, fundId: data.id, firmId:reactLocalStorage.getObject('firmId') }, () => this.updateInputValues());
        let dataObj = {};
        dataObj ={
            fundManagerLegalEntityNameValid : true,
            legalEntityValid: true,
            fundCommonNameValid: true,
            fundManagerTitleValid:true,
            fundManagerCommonNameValid:true,
            fundTypeValid: true,
            hardCapApplicationRuleValid:true,
        };
        this.updateStateParams(dataObj);
    }

    targetCommitmentValidation = (value) => {
        let targetValue = value;
        let hardCapValue = this.state.fundHardCap;
        if(parseInt(targetValue) > parseInt(hardCapValue)) {
            return true;
        }
        return false;
    }

    hardCardValidation = (value) => {
        let hardCapValue = value;
        let tragetValue = this.state.fundTargetCommitment;
        if(parseInt(hardCapValue) < parseInt(tragetValue)) {
            return true;
        }
        return false;

    }


    updateInputValues = () => {
        let obj = this.state.createdFundData;
        this.setState({ 
            legalEntity: obj.legalEntity || '',
            fundHardCap: obj.fundHardCap ||  parseInt(obj.fundHardCap) == 0 ? obj.fundHardCap : '',
            fundCommonName: obj.fundCommonName ? obj.fundCommonName : '',
            fundManagerTitle: obj.fundManagerTitle ? obj.fundManagerTitle : '',
            fundManagerCommonName: obj.fundManagerCommonName ? obj.fundManagerCommonName : '',
            hardCapApplicationRule: obj.hardCapApplicationRule || 0,
            fundHardCapCurrencyValue: obj.fundHardCap ||  parseInt(obj.fundHardCap) == 0 ? this.FsnetUtil.convertToCurrency(obj.fundHardCap) : '',
            fundTargetCommitmentCurrencyValue: obj.fundTargetCommitment ||  parseInt(obj.fundTargetCommitment) == 0 ? this.FsnetUtil.convertToCurrency(obj.fundTargetCommitment) : '',
            capitalCommitmentByGPCurrencyValue: obj.capitalCommitmentByFundManager && parseInt(obj.capitalCommitmentByFundManager) !==0 ? this.FsnetUtil.convertToCurrency(obj.capitalCommitmentByFundManager) : '',
            fundType: obj.fundType || 0,
            fundManagerLegalEntityName: obj.fundManagerLegalEntityName || '',
            capitalCommitmentByGP: obj.capitalCommitmentByFundManager && parseInt(obj.capitalCommitmentByFundManager) !==0 ? obj.capitalCommitmentByFundManager: '',
            percentageOfLPCommitment: obj.percentageOfLPCommitment && parseInt(obj.percentageOfLPCommitment) !==0 ? obj.percentageOfLPCommitment : '',
            percentageOfLPCommitmentPer: obj.percentageOfLPCommitment && parseInt(obj.percentageOfLPCommitment) !==0 ? obj.percentageOfLPCommitment+'%' : '',
            fundTargetCommitment: obj.fundTargetCommitment || '',
            percentageOfLPAndGPAggregateCommitment: obj.percentageOfLPAndGPAggregateCommitment &&parseInt(obj.percentageOfLPAndGPAggregateCommitment) !==0 ? obj.percentageOfLPAndGPAggregateCommitment : '',
            percentageOfLPAndGPAggregateCommitmentPer: obj.percentageOfLPAndGPAggregateCommitment &&parseInt(obj.percentageOfLPAndGPAggregateCommitment) !==0 ? obj.percentageOfLPAndGPAggregateCommitment+'%' : '',
            currentFundImage: obj.fundImage? obj.fundImage.url: FundImage,
            generalPartnersCapitalCommitmentindicated: obj.generalPartnersCapitalCommitmentindicated? obj.generalPartnersCapitalCommitmentindicated: '',
            fundImageName: obj.fundImage? obj.fundImage.originalname: 'fund_Pic.jpg',
            fundStatus:obj.status || 'Open',
         }, () => this.disableFundParticipationFields());
    }

    disableFundParticipationFields = () => {
        if(!this.state.percentageOfLPCommitment && !this.state.percentageOfLPAndGPAggregateCommitment && !this.state.capitalCommitmentByGP) {
            this.enableAllTextBoxes()
        } else if(this.state.percentageOfLPCommitment) {
            this.enableATextField();
        } else if(this.state.percentageOfLPAndGPAggregateCommitment) {
            this.enableCTextField();
        } else if(this.state.capitalCommitmentByGP) {
            this.enableBTextField();
        }
    }

    // Update state params values and login button visibility

    updateStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, ()=>{
            this.enableDisableFundDetailsButton();
        });
    }

    // Enable / Disble functionality of Login Button

    enableDisableFundDetailsButton = () => {
        let status = (this.state.legalEntityValid &&this.state.fundCommonNameValid && this.state.fundManagerTitleValid && this.state.fundManagerCommonNameValid && this.state.fundManagerLegalEntityNameValid && this.state.fundTargetCommitmentValid && this.state.fundHardCapValid && this.state.fundTypeValid && this.state.hardCapApplicationRuleValid && this.state.firmIdValid) ? true : false;
        this.setState({
            fundDetailsPageValid : status
        });
    }

    numbersOnly = (event) => {
        if (((event.which != 46 || (event.which == 46 && event.target.value == '')) ||
            event.target.value.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    }

    addCurrencyValueToInput = (event, type) => {
        let dataObj = {};
        switch(type) {
            case 'fundHardCap':
                if(event.target.value) {
                    let error = this.hardCardValidation(event.target.value);
                    let value = this.FsnetUtil.convertToCurrency(event.target.value);
                    if(error) {
                        this.setState({
                            fundHardCapCurrencyValue: value,
                            fundHardCapValid: false,
                            fundHardCapBorder: true,
                            fundHardCapMsz: this.Constants.FUND_HARD_CAP_MSZ
                        })
                        dataObj ={
                            fundHardCapValid :false
                        };
                        this.updateStateParams(dataObj);
                    } else {
                        this.setState({
                            fundHardCapCurrencyValue: value,
                            fundHardCapValid: true,
                            fundHardCapBorder: '',
                            fundHardCapMsz: false,
                            fundTargetCommitmentValid: true,
                            fundTargetCommitmentMsz: '',
                            fundTargetCommitmentBorder: false
                        })
                        dataObj ={
                            fundHardCapValid :true,
                            fundTargetCommitmentValid: true
                        };
                        this.updateStateParams(dataObj);
                    }
                }
                break;
            case 'fundTargetCommitment':
                if(event.target.value) {
                    let error = this.targetCommitmentValidation(event.target.value);
                    let value = this.FsnetUtil.convertToCurrency(event.target.value);
                    if(error) {
                        this.setState({
                            fundTargetCommitmentCurrencyValue: value,
                            fundTargetCommitmentValid: false,
                            fundTargetCommitmentBorder: true,
                            fundTargetCommitmentMsz: this.Constants.FUND_TARGET_COMMITMENT_MSZ
                        })
                        dataObj ={
                            fundTargetCommitmentValid :false
                        };
                        this.updateStateParams(dataObj);
                    } else {
                        this.setState({
                            fundTargetCommitmentCurrencyValue: value,
                            fundTargetCommitmentValid: true,
                            fundTargetCommitmentMsz: '',
                            fundTargetCommitmentBorder: false,
                            fundHardCapValid: true,
                            fundHardCapBorder: '',
                            fundHardCapMsz: false,
                        })
                        dataObj ={
                            fundTargetCommitmentValid :true,
                            fundHardCapValid: true
                        };
                        this.updateStateParams(dataObj);
                    }
                }
                break;
            case 'capitalCommitmentByGP':
                if(event.target.value) {
                    let value = this.FsnetUtil.convertToCurrency(event.target.value);
                    this.setState({
                        capitalCommitmentByGPCurrencyValue: value
                    })
                }
                break;
            default:
                break;
        }

    }

    handleinputFocus = (event, type) => {
        switch(type) {
            case 'fundHardCap':
                if(event.target.value) {
                    this.setState({
                        fundHardCapCurrencyValue: this.state.fundHardCap
                    })
                }
                break;
            case 'fundTargetCommitment':
                if(event.target.value) {
                    this.setState({
                        fundTargetCommitmentCurrencyValue: this.state.fundTargetCommitment
                    })
                }
                break;
            case 'capitalCommitmentByGP':
                if(event.target.value) {
                    this.setState({
                        capitalCommitmentByGPCurrencyValue: this.state.capitalCommitmentByGP
                    })
                }
                break;
            case 'fundType':
                if(event.target.value) {
                    this.setState({
                        fundType: this.state.fundType
                    })
                }
                break;
            case type:
                if(event.target.value) {
                    this.setState({
                        [type+'Per']: this.state[type]
                    })
                }
                break;
            default:
                break;
        }

    }

    //Add Percentage to % input fields.
    addPercentageToInput = (event, type) => {
        switch(type) {
            case type:
                if(event.target.value) {
                    this.setState({
                        [type+'Per']:parseFloat(event.target.value)+'%',
                        [type]:parseFloat(event.target.value) //This is for float conversion if we have more than two dots
                    })
                }
                break;
            default: 
                break;
        }
    }
 
    //Fund details input change event
    fundDetailsInputHandleEvent = (event, type) => {
        let dataObj={};
        const allowNumberRegex = this.Constants.NUMBER_REGEX;
        switch(type) {
            case 'legalEntity':
                if(!event.target.value.trim()) {
                    this.setState({
                        legalEntityMsz: this.Constants.LEGAL_ENTITY_REQUIRED,
                        legalEntityValid: false,
                        legalEntityBorder: true,
                        legalEntity: ''
                    })
                    dataObj ={
                        legalEntityValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        legalEntityMsz: '',
                        legalEntityValid: true,
                        legalEntityBorder: false,
                        legalEntity: event.target.value
                    })
                    dataObj ={
                        legalEntityValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'fundCommonName':
                if(!event.target.value.trim()) {
                    this.setState({
                        fundCommonNameMsz: this.Constants.FUND_COMMON_NAME_REQUIRED,
                        fundCommonNameValid: false,
                        fundCommonNameBorder: true,
                        fundCommonName: ''
                    })
                    dataObj ={
                        fundCommonNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        fundCommonNameMsz: '',
                        fundCommonNameValid: true,
                        fundCommonNameBorder: false,
                        fundCommonName: event.target.value
                    })
                    dataObj ={
                        fundCommonNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'fundManagerTitle':
                if(!event.target.value.trim()) {
                    this.setState({
                        fundManagerTitleMsz: this.Constants.FUND_MANAGER_TITLE_REQUIRED,
                        fundManagerTitleValid: false,
                        fundManagerTitleBorder: true,
                        fundManagerTitle: ''
                    })
                    dataObj ={
                        fundManagerTitleValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        fundManagerTitleMsz: '',
                        fundManagerTitleValid: true,
                        fundManagerTitleBorder: false,
                        fundManagerTitle: event.target.value
                    })
                    dataObj ={
                        fundManagerTitleValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'fundManagerCommonName':
                if(!event.target.value.trim()) {
                    this.setState({
                        fundManagerCommonNameMsz: this.Constants.FUND_MANAGER_COMMON_NAME_REQUIRED,
                        fundManagerCommonNameValid: false,
                        fundManagerCommonNameBorder: true,
                        fundManagerCommonName: ''
                    })
                    dataObj ={
                        fundManagerCommonNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        fundManagerCommonNameMsz: '',
                        fundManagerCommonNameValid: true,
                        fundManagerCommonNameBorder: false,
                        fundManagerCommonName: event.target.value
                    })
                    dataObj ={
                        fundManagerCommonNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'fundHardCap':
                // if value is not blank, then test the regex
                if (!event.target.value.trim() || allowNumberRegex.test(event.target.value)) {
                    this.setState({
                        fundHardCap: parseFloat(event.target.value),
                        fundHardCapCurrencyValue: event.target.value
                    })
                }
                break;
            case 'firmId':
                if(!event.target.value.trim() || event.target.value.trim() == 0) {
                    this.setState({
                        firmIdMsz: this.Constants.FIRM_ID_REQUIRED,
                        firmIdValid: false,
                        firmIdBorder: true,
                        firmId: ''
                    })
                    dataObj ={
                        firmIdValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        firmIdMsz: '',
                        firmIdValid: true,
                        firmIdBorder: false,
                        firmId: event.target.value
                    })
                    dataObj ={
                        firmIdValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                this.setState({
                    firmId: event.target.value
                })
                break;
            case 'fundType':
                if(!event.target.value.trim() || event.target.value.trim() == 0) {
                    this.setState({
                        fundTypeMsz: this.Constants.FUND_TYPE_REQUIRED,
                        fundTypeValid: false,
                        fundTypeBorder: true,
                        fundType: ''
                    })
                    dataObj ={
                        fundTypeValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        fundTypeMsz: '',
                        fundTypeValid: true,
                        fundTypeBorder: false,
                        fundType: event.target.value
                    })
                    dataObj ={
                        fundTypeValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                this.setState({
                    fundType: event.target.value
                })
                break;
            case 'hardCapApplicationRule':
                if(!event.target.value.trim() || event.target.value.trim() == 0) {
                    this.setState({
                        hardCapApplicationRuleMsz: this.Constants.HARDCAP_VALIDATION_RULE_REQUIRED,
                        hardCapApplicationRuleValid: false,
                        hardCapApplicationRuleBorder: true,
                        hardCapApplicationRule: ''
                    })
                    dataObj ={
                        hardCapApplicationRuleValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        hardCapApplicationRuleMsz: '',
                        hardCapApplicationRuleValid: true,
                        hardCapApplicationRuleBorder: false,
                        hardCapApplicationRule: event.target.value
                    })
                    dataObj ={
                        hardCapApplicationRuleValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                this.setState({
                    hardCapApplicationRule: event.target.value
                })
                break;
            case 'fundTargetCommitment':
                if (!event.target.value.trim() || allowNumberRegex.test(event.target.value)) {
                    this.setState({
                        fundTargetCommitment: parseFloat(event.target.value),
                        fundTargetCommitmentCurrencyValue: event.target.value
                    })
                }
                break;
            case 'fundManagerLegalEntityName':
                if(!event.target.value.trim() || event.target.value.trim() === undefined) {
                    this.setState({
                        fundManagerLegalEntityNameMsz: this.Constants.LEGAL_ENTITY_NAME_REQUIRED,
                        fundManagerLegalEntityNameValid: false,
                        fundManagerLegalEntityNameBorder: true,
                        fundManagerLegalEntityName: ''
                    })
                    dataObj ={
                        fundManagerLegalEntityNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        fundManagerLegalEntityNameMsz: '',
                        fundManagerLegalEntityNameValid: true,
                        fundManagerLegalEntityNameBorder: false,
                        fundManagerLegalEntityName: event.target.value
                    })
                    dataObj ={
                        fundManagerLegalEntityNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                this.setState({
                    fundManagerLegalEntityName: event.target.value
                })
                break;
            case 'percentageOfLPCommitment':
                if(!event.target.value.trim()) {
                    this.setState({
                        percentageOfLPCommitment: '',
                        percentageOfLPCommitmentPer: ''
                    })
                    this.enableAllTextBoxes()
                } else {
                    if ((!event.target.value.trim() || allowNumberRegex.test(event.target.value)) && (parseInt(event.target.value) >0 && parseInt(event.target.value) <=100)) {
                        this.setState({
                            percentageOfLPCommitment: event.target.value,
                            percentageOfLPCommitmentPer: event.target.value
                        })
                        this.enableATextField();
                    }
                }
                break;
            case 'percentageOfLPAndGPAggregateCommitment':
                if(!event.target.value.trim()) {
                    this.setState({
                        percentageOfLPAndGPAggregateCommitment: '',
                        percentageOfLPAndGPAggregateCommitmentPer: ''
                    })
                    this.enableAllTextBoxes()
                } else {
                    if ((event.target.value.trim() === '' || allowNumberRegex.test(event.target.value)) && ((parseInt(event.target.value) >0 && parseInt(event.target.value) < 100))) {
                        this.setState({
                            percentageOfLPAndGPAggregateCommitment: event.target.value,
                            percentageOfLPAndGPAggregateCommitmentPer: event.target.value
                        })
                        this.enableCTextField();
                    }
                }
                break;
            case 'capitalCommitmentByGP':
                if(!event.target.value.trim()) {
                    this.setState({
                        capitalCommitmentByGP: '',
                        capitalCommitmentByGPCurrencyValue: '',
                    })
                    this.enableAllTextBoxes()
                    
                } else {
                    if (!event.target.value.trim() || allowNumberRegex.test(event.target.value)) {
                        this.setState({
                            capitalCommitmentByGP: parseFloat(event.target.value),
                            capitalCommitmentByGPCurrencyValue: event.target.value
                        })
                        this.enableBTextField();
                        
                    }
                }
                break;
            case 'generalPartnersCapitalCommitmentindicated1':
                this.setState({
                    generalPartnersCapitalCommitmentindicated: 1  
                })
                dataObj ={
                    generalPartnersCapitalCommitmentindicatedValid :true
                };
                this.updateStateParams(dataObj);
                break;
            case 'generalPartnersCapitalCommitmentindicated2':
                this.setState({
                    generalPartnersCapitalCommitmentindicated: 2  
                })
                dataObj ={
                    generalPartnersCapitalCommitmentindicatedValid :true
                };
                this.updateStateParams(dataObj);
                break;
            default:
                // do nothing
        }
    }

    enableATextField = () => {
        this.setState({
            aTextBoxDisabled: false,
            bTextBoxDisabled: true,
            cTextBoxDisabled: true,
            fundDetailspageError:''
        })
    }

    enableBTextField = () => {
        this.setState({
            aTextBoxDisabled: true,
            bTextBoxDisabled: false,
            cTextBoxDisabled: true,
            fundDetailspageError:''
        })
    }

    enableCTextField = () => {
        this.setState({
            aTextBoxDisabled: true,
            bTextBoxDisabled: true,
            cTextBoxDisabled: false,
            fundDetailspageError:''
        })
    }

    enableAllTextBoxes = () => {
        this.setState({
            aTextBoxDisabled: false,
            bTextBoxDisabled: false,
            cTextBoxDisabled: false,
            fundDetailspageError:''
        })
    }

    //Clear the image when user click on remove button
    //Clear the image name from state
    //Clear the input file value
    removeImageBtn = () => {
        this.setState({
            fundImageName: 'fund_Pic.jpg',
            currentFundImage : FundImage,
            fundPicFile: {},
            imageRemoved: true
        });
        document.getElementById('uploadBtn').value = "";
    }

    uploadBtnClick = () => {
        document.getElementById('uploadBtn').click();
    }

    //USer profile pic upload.
    handleChange = (event) => {
        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
            this.imageFile = event.target.files[0];
            let imageName = event.target.files[0].name
            var sFileExtension = imageName.split('.')[imageName.split('.').length - 1].toLowerCase();
            const imageFormats = ['png','jpg','jpeg','gif','tif','svg'];
            if(imageFormats.indexOf(sFileExtension) === -1) {
                document.getElementById('uploadBtn').value = "";
                alert('Please upload valid image.')
                return true;
            }
            //Change user profile image size limit here.
            if(this.imageFile.size <=1600000) {
                    this.setState({
                        fundPicFile : event.target.files[0],
                    });
                reader.readAsDataURL(this.imageFile);
                this.setState({
                    fundImageName: event.target.files[0].name,
                    imageRemoved: false
                });
                reader.onload = () => {
                    this.setState({
                        currentFundImage : reader.result,
                    });
                }
            } else {
               alert('Please upload image Maximum file size : 512X512')
            }
        }
    }


    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    proceedToNext = () => {
        if(this.state.firmId) {
            let error = false;
            if(!this.state.percentageOfLPCommitment && !this.state.capitalCommitmentByGP && !this.state.percentageOfLPAndGPAggregateCommitment) {
                error = true;
                this.setState({
                    fundDetailspageError: 'Please enter one Fund Manager’s Capital Commitment.'
                })
            }
            
            if(!this.state.fundHardCap && this.state.fundTargetCommitment) {
                error = true;
                this.setState({
                    fundDetailspageError: 'Please enter Fund hard carp.'
                })
            }
            
            if(!error) {
                this.open()
                var formData = new FormData();
                formData.append("vcfirmId", this.state.firmId);
                formData.append("legalEntity", this.state.legalEntity);
                formData.append("fundCommonName", this.state.fundCommonName);
                formData.append("fundManagerTitle", this.state.fundManagerTitle);
                formData.append("fundManagerCommonName", this.state.fundManagerCommonName);
                formData.append("hardCapApplicationRule", this.state.hardCapApplicationRule?this.state.hardCapApplicationRule:0);
                formData.append("fundHardCap", this.state.fundHardCap? this.state.fundHardCap : 0);
                formData.append("fundType", this.state.fundType ? this.state.fundType : 0);
                formData.append("fundTargetCommitment", this.state.fundTargetCommitment? this.state.fundTargetCommitment : 0);
                formData.append("fundManagerLegalEntityName", this.state.fundManagerLegalEntityName);
                formData.append("generalPartnersCapitalCommitmentindicated", this.state.generalPartnersCapitalCommitmentindicated);
                if(Object.keys(this.state.fundPicFile).length === 0 && this.state.fundPicFile.constructor === Object && this.state.imageRemoved){
                    formData.append("requestContainImage", false);
                } else {
                    formData.append("requestContainImage", true);
                }
                formData.append("fundImage", this.state.fundPicFile);
                if(this.state.fundId !== '') {
                    formData.append("fundId", this.state.fundId)
                }
                formData.append("percentageOfLPCommitment", this.state.percentageOfLPCommitment?this.state.percentageOfLPCommitment:0);
                formData.append("capitalCommitmentByFundManager", this.state.capitalCommitmentByGP?this.state.capitalCommitmentByGP:0);
                formData.append("percentageOfLPAndGPAggregateCommitment", this.state.percentageOfLPAndGPAggregateCommitment ? this.state.percentageOfLPAndGPAggregateCommitment : 0);
                this.Fsnethttp.fundStore(formData).then(result=>{
                    this.close();
                    this.setState({
                        fundDetailsPageValid: true,
                        createdFundData: result.data.data
                    })
                    if(this.FsnetUtil.getUserRole() == 'GP' || this.FsnetUtil.getUserRole() == 'FSNETAdministrator' || this.FsnetUtil.getUserRole() == 'Admin') {
                        this.props.history.push('/createfund/gpSignatories/'+this.FsnetUtil._encrypt(result.data.data.id));
                    } else if(this.FsnetUtil.getUserRole() == 'SecondaryGP') {
                        this.props.history.push('/createfund/gpDelegate/'+ this.FsnetUtil._encrypt(result.data.data.id));
                    }else {
                        this.props.history.push('/createfund/editSubForm/'+this.FsnetUtil._encrypt(result.data.data.id));
                    }
                    
                })
                .catch(error=>{
                    this.close();
                    if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                        this.setState({
                            fundDetailspageError: error.response.data.errors[0].msg,
                        });
                    } else {
                        this.setState({
                            fundDetailspageError: this.Constants.INTERNAL_SERVER_ERROR,
                        });
                    }
                });
            }
        } else {
            this.setState({
                fundDetailspageError: this.Constants.NO_FIRM_ID,
            });
        }
    }
    
    proceedToBack = () => {
        this.props.history.push('/createfund/funddetails');
           
    }

    render(){
        function LinkWithTooltip({ id, children, href, tooltip }) {
            return (
              <OverlayTrigger
                overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
                placement="left"
                delayShow={300}
                delayHide={150}
                rootClose
              >
                <a href={href}>{children}</a>
              </OverlayTrigger>
            );
          }
        return(
            <div className="step1FormClass">
                <div className="form-grid formGridDivMargin">
                    <h2 className="title">{this.state.jsonData.FUND_DETAILS}</h2>
                    <h4 className="subtext">{this.state.jsonData.ENTER_DETAILS_OF_FUND}</h4>
                   
                    <div id="step1Form">
                        {
                            this.state.firmIdsList.length > 0 &&
                            <Row className="step1Form-row">
                                <Col xs={6} md={6}>
                                    <label className="form-label">Fund Manager*</label>
                                    <FormControl defaultValue={0} className={"selectFormControl " + (this.state.firmIdBorder ? 'inputError' : '')}  componentClass="select" value= {this.state.firmId} onChange={(e) => this.fundDetailsInputHandleEvent(e,'firmId')} onBlur={(e) => this.fundDetailsInputHandleEvent(e,'firmId')} onFocus={(e)=>{this.handleinputFocus(e,'firmId')}}>
                                            <option value="0">Select Fund Manager</option>
                                            {this.state.firmIdsList.map((record, index) => {
                                                return (
                                                    <option value={record['id']} key={index} >{record['name']}</option>
                                                );
                                            })}
                                    </FormControl>
                                    <span className="error">{this.state.firmIdMsz}</span>
                                </Col>
                            </Row>
                        }
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_LEGAL_ENTITY_NAME}*
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_LEGAL_ENTITY_NAME}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span> 
                                </label>
                                <FormControl type="text" name="legalEntity" placeholder={this.state.jsonData.PLACEHOLDER_FUND_LEGAL_ENTITY_NAME} className={"inputFormControl " + (this.state.legalEntityBorder ? 'inputError' : '')} value= {this.state.legalEntity}  onChange={(e) => this.fundDetailsInputHandleEvent(e,'legalEntity')} onBlur={(e) => this.fundDetailsInputHandleEvent(e,'legalEntity')} />
                                <span className="error">{this.state.legalEntityMsz}</span>
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_COMMON_NAME}*
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_COMMON_NAME}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span> 
                                </label>
                                <FormControl type="text" name="fundCommonName" placeholder={this.state.jsonData.FUND_COMMON_NAME} className={"inputFormControl " + (this.state.fundCommonNameBorder ? 'inputError' : '')} value= {this.state.fundCommonName}  onChange={(e) => this.fundDetailsInputHandleEvent(e,'fundCommonName')} onBlur={(e) => this.fundDetailsInputHandleEvent(e,'fundCommonName')} />
                                <span className="error">{this.state.fundCommonNameMsz}</span>
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_MANAGER_LEGAL_ENTITY_NAME}*
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_MANAGER_LEGAL_ENTITY_NAME}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span> 
                                </label>                                
                                <FormControl type="text" placeholder={this.state.jsonData.PLACEHOLDER_FUND_MANAGER_LEGAL_ENTITY_NAME} className={"inputFormControl " + (this.state.fundManagerLegalEntityNameBorder ? 'inputError' : '')} value= {this.state.fundManagerLegalEntityName}  onChange={(e) => this.fundDetailsInputHandleEvent(e,'fundManagerLegalEntityName')} onBlur={(e) => this.fundDetailsInputHandleEvent(e,'fundManagerLegalEntityName')} />
                                <span className="error">{this.state.fundManagerLegalEntityNameMsz}</span>
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_MANAGER_TITLE}*
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_MANAGER_TITLE}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span> 
                                </label>
                                <FormControl type="text" name="fundManagerTitle" placeholder={this.state.jsonData.FUND_MANAGER_TITLE} className={"inputFormControl " + (this.state.fundManagerTitleBorder ? 'inputError' : '')} value= {this.state.fundManagerTitle}  onChange={(e) => this.fundDetailsInputHandleEvent(e,'fundManagerTitle')} onBlur={(e) => this.fundDetailsInputHandleEvent(e,'fundManagerTitle')} />
                                <span className="error">{this.state.fundManagerTitleMsz}</span>
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_MANAGER_COMMON_NAME}*
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUND_MANAGER_COMMON_NAME}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span> 
                                </label>
                                <FormControl type="text" name="fundManagerCommonName" placeholder={this.state.jsonData.FUND_MANAGER_COMMON_NAME} className={"inputFormControl " + (this.state.fundManagerCommonNameBorder ? 'inputError' : '')} value= {this.state.fundManagerCommonName}  onChange={(e) => this.fundDetailsInputHandleEvent(e,'fundManagerCommonName')} onBlur={(e) => this.fundDetailsInputHandleEvent(e,'fundManagerCommonName')} />
                                <span className="error">{this.state.fundManagerCommonNameMsz}</span>
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUNDS_TARGET_COMMITMENTS}
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUNDS_TARGET_COMMITMENTS}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span>
                                </label>
                                <FormControl type="text" placeholder={this.state.jsonData.PLACEHOLDER_FUNDS_TARGET_COMMITMENTS} className={"inputFormControl " + (this.state.fundTargetCommitmentBorder ? 'inputError' : '')} value= {this.state.fundTargetCommitmentCurrencyValue}  onChange={(e) => this.fundDetailsInputHandleEvent(e,'fundTargetCommitment')} onKeyPress={(e) => {this.numbersOnly(e)}} onBlur={(e)=>{this.addCurrencyValueToInput(e,'fundTargetCommitment')}} onFocus={(e)=>{this.handleinputFocus(e,'fundTargetCommitment')}} />
                                <span className="error">{this.state.fundTargetCommitmentMsz}</span>
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUNDS_HARD_CAP}
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FUNDS_HARD_CAP}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span> 
                                </label>
                                <FormControl type="text" value= {this.state.fundHardCapCurrencyValue} placeholder={this.state.jsonData.PLACEHOLDER_FUNDS_HARD_CAP} className={"inputFormControl " + (this.state.fundHardCapBorder ? 'inputError' : '')} onChange={(e)=> this.fundDetailsInputHandleEvent(e,'fundHardCap')} onKeyPress={(e) => {this.numbersOnly(e)}} onBlur={(e)=>{this.addCurrencyValueToInput(e,'fundHardCap')}} onFocus={(e)=>{this.handleinputFocus(e,'fundHardCap')}} />
                                <span className="error">{this.state.fundHardCapMsz}</span>
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FUND_TYPE}*</label>
                                <FormControl defaultValue={0} className={"selectFormControl " + (this.state.fundTypeBorder ? 'inputError' : '')}  componentClass="select" placeholder={this.state.jsonData.SELECT_FUND_TYPE} value= {this.state.fundType} onChange={(e) => this.fundDetailsInputHandleEvent(e,'fundType')} onBlur={(e) => this.fundDetailsInputHandleEvent(e,'fundType')} onFocus={(e)=>{this.handleinputFocus(e,'fundType')}}>
                                        <option value="0">{this.state.jsonData.SELECT_FUND_TYPE}</option>
                                        <option value="1">{this.state.jsonData.US_FUND}</option>
                                        <option value="2">{this.state.jsonData.NON_US_FUND}</option>
                                </FormControl>
                                <span className="error">{this.state.fundTypeMsz}</span>
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.HARD_CAP_APPLICATION_RULE}*
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_HARD_CAP_APPLICATION_RULE}>
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span> 
                                </label>
                                <FormControl defaultValue={0} className={"selectFormControl " + (this.state.hardCapApplicationRuleBorder ? 'inputError' : '')}  componentClass="select" placeholder={this.state.jsonData.SELECT_HARD_CAP_APPLICTION_RULE} value= {this.state.hardCapApplicationRule} onChange={(e) => this.fundDetailsInputHandleEvent(e,'hardCapApplicationRule')} onBlur={(e) => this.fundDetailsInputHandleEvent(e,'hardCapApplicationRule')} onFocus={(e)=>{this.handleinputFocus(e,'hardCapApplicationRule')}}>
                                        <option value="0">{this.state.jsonData.SELECT_HARD_CAP_APPLICTION_RULE}</option>
                                        <option value="1">{this.state.jsonData.INVESTOR_CAPITAL_COMMITMENT_ONLY}</option>
                                        <option value="2">{this.state.jsonData.INVESTOR_CAPITAL_COMMITMENT_PLUS_FUND_CAPITAL_COMMITMENT}</option>
                                </FormControl>
                                <span className="error">{this.state.hardCapApplicationRuleMsz}</span>
                            </Col>
                        </Row>
                        <h2 className="title marginTop40">{this.state.jsonData.FUND_MANAGER_CAPITAL_COMMITMENT}</h2>
                        <h4 className="subtext">{this.state.jsonData.SPECIFY_FUND_MANAGER_CAPITAL_COMMITMENT}</h4>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.PERCENTAGE_INVESTOR_COMMITMENTS}
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_PERCENTAGE_INVESTOR_COMMITMENTS} id="tooltip-1">
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span>    
                                </label>
                                <FormControl type="text" placeholder="1.00%" className="inputFormControl" value={this.state.percentageOfLPCommitmentPer} disabled={this.state.aTextBoxDisabled} onChange={(e)=> this.fundDetailsInputHandleEvent(e,'percentageOfLPCommitment')} onBlur={(e)=>{this.addPercentageToInput(e,'percentageOfLPCommitment')}} onFocus={(e)=>{this.handleinputFocus(e,'percentageOfLPCommitment')}}/>
                            </Col>
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.FIXED_COMMITMENT_DOLLARS}
                                        &nbsp;
                                        <span>
                                            <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_FIXED_COMMITMENT_DOLLARS} id="tooltip-1">
                                            <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                            </LinkWithTooltip>
                                        </span> 
                                </label>
                                <FormControl type="text" placeholder={this.state.jsonData.PLACEHOLDER_FUNDS_TARGET_COMMITMENTS} className="inputFormControl" disabled={this.state.bTextBoxDisabled} value={this.state.capitalCommitmentByGPCurrencyValue}  onChange={(e)=> this.fundDetailsInputHandleEvent(e,'capitalCommitmentByGP')}  onKeyPress={(e) => {this.numbersOnly(e)}} onBlur={(e)=>{this.addCurrencyValueToInput(e,'capitalCommitmentByGP')}} onFocus={(e)=>{this.handleinputFocus(e,'capitalCommitmentByGP')}} />
                            </Col>
                        </Row>
                        <Row className="step1Form-row">
                            <Col xs={6} md={6}>
                                <label className="form-label">{this.state.jsonData.PERCENTAGE_ALL_COMMITMENTS}
                                    &nbsp;
                                    <span>
                                        <LinkWithTooltip tooltip={this.state.jsonData.TOOLTIP_PERCENTAGE_ALL_COMMITMENTS} id="tooltip-1">
                                        <i className="fa fa-question-circle toolTipIconAlign" aria-hidden="true"></i>
                                        </LinkWithTooltip>
                                    </span> 
                                </label>
                                <FormControl type="text" placeholder="1.00%" className="inputFormControl" disabled={this.state.cTextBoxDisabled} value={this.state.percentageOfLPAndGPAggregateCommitmentPer} onChange={(e)=> this.fundDetailsInputHandleEvent(e,'percentageOfLPAndGPAggregateCommitment')} onBlur={(e)=>{this.addPercentageToInput(e,'percentageOfLPAndGPAggregateCommitment')}} onFocus={(e)=>{this.handleinputFocus(e,'percentageOfLPAndGPAggregateCommitment')}} />
                            </Col>
                        </Row>
                        <label className="profile-text">{this.state.jsonData.FUND_IMAGE_DIMENSIONS}</label>
                        <Row className="profile-Row profileMargin marginBottom30 marginLeft15">
                                <img src={this.state.currentFundImage} alt="profile-pic" className="profile-pic"/>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <input type="file" id="uploadBtn" className="hide" onChange={ (e) => this.handleChange(e) } />
                                <Button className="uploadFile" onClick={this.uploadBtnClick}>{this.state.jsonData.UPLOAD_FILE}</Button> <br/>
                                <label className="removeBtn" onClick={this.removeImageBtn}>{this.state.jsonData.REMOVE}</label>
                            </Col>
                        </Row>
                        <div className="marginTop20 error">{this.state.fundDetailspageError}</div>
                    </div>
                </div>
                {
                    this.state.fundStatus === 'Open' || this.state.fundStatus === 'New-Draft' ||  this.state.fundStatus === 'Admin-Draft' ?
                    <div className="footer-nav"> 
                        <i className="fa fa-chevron-left disabled" aria-hidden="true"></i>
                        <i className={"fa fa-chevron-right " + (!this.state.fundDetailsPageValid ? 'disabled' : '')} onClick={this.proceedToNext} aria-hidden="true"></i>
                    </div>
                    :
                    <div className="footer-nav"> 
                        <Button type="button" className={"fsnetSubmitButton " + (!this.state.fundDetailsPageValid ? 'disabled' : 'btnEnabled')} onClick={this.proceedToNext}>{this.state.jsonData.SAVE_CHANGES}</Button>
                    </div>
                }
               
                <Loader isShow={this.state.showModal}></Loader>

            </div>
        );
    }
}

export default Step1Component;




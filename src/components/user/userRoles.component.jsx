import React, { Component } from 'react';
import '../register/register.component.css';
import { Row, Col, Button, Checkbox as CBox, FormControl, Modal } from 'react-bootstrap';
import { Fsnethttp } from '../../services/fsnethttp';
import { Constants } from '../../constants/constants';
import { FsnetAuth } from '../../services/fsnetauth';
import vanillaLogo from '../../images/Vanilla.png';
import { FsnetUtil } from '../../util/util';
import Loader from '../../widgets/loader/loader.component';
import HeaderComponent from '../header/header.component'

class UserRolesComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            allRoles: []
        }
    }

    // ProgressLoader : close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () => {
        this.setState({ showModal: true });
    }

    componentDidMount() {
        if (this.FsnetAuth.isAuthenticated()) {
            this.open();
            this.Fsnethttp.listUsersForAccount().then(result => {
                this.close();
                this.setState({
                    allRoles: result.data.allusers,
                })
            })
                .catch(error => {
                    this.close();
                });
        } else {
            this.props.history.push(`/login`);
        }
    }

    switchRole = (data) => () => {
        this.open();
        const postObj = { userId: data.id }
        this.Fsnethttp.switchAccount(postObj).then(result => {
            let data = result.data;
            this.FsnetUtil._userSetData(data);
            setTimeout(() => {
                this.close();
                this.props.history.push(`/dashboard`);
            }, 100);
        })
            .catch(error => {
                this.close();
            });
    }


    render() {
        return (
            <div className="parentContainer" id="registerContainer">
               <Row className="dashboardMainRow">
                    <Col lg={5} md={6} sm={6} xs={12}>
                        <img src={vanillaLogo} alt="vanilla" className="vanilla-logo marginLeft30" />
                    </Col>
                    <Col lg={7} md={6} sm={6} xs={12}>
                        <HeaderComponent ></HeaderComponent>
                    </Col>
                </Row>
                <Row >
                    <div className="rolesContainer marginTop20">
                        <div className="row">
                            <div className="title-text ">Please select a role to view:</div>
                            <div className="container-fluid">
                                <div className="row">
                                    {
                                        this.state.allRoles.length > 0 &&
                                        this.state.allRoles.map((record, index) => {
                                            return (
                                                <div key={index} className="rolesTab ">
                                                    <div className="align-middle" onClick={this.switchRole(record)}>{this.FsnetUtil.getAccountTypeName(record.accountType)}</div>
                                                </div>
                                            );
                                        })
                                    }
                                </div>

                            </div>
                        </div>
                    </div>
                </Row>

                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default UserRolesComponent;


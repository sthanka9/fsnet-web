import React, { Component } from 'react';
import { Row, Col, Button, FormControl } from 'react-bootstrap';
import HeaderComponent from '../authheader/header.component'
import './forgotusername.component.css';
import { FsnetAuth } from '../../services/fsnetauth';
import { Fsnethttp } from '../../services/fsnethttp';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/rrui.css';
import 'react-phone-number-input/style.css';
import { Constants } from '../../constants/constants';
import Loader from '../../widgets/loader/loader.component';
import successImage from '../../images/success.png';

class ForgotUserNameComponent extends Component {
    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.state = {
            showForgotScreen1: true,
            showForgotScreen2: false,
            showForgotScreen3: false,
            showForgotScreen4: false,
            email: '',
            cellNumber: '',
            accountType: '',
            accountTypeRequired: '',
            forgotScreen1ErrorMsz: '',
            showVerifyCodeIcon: false,
            invalidVerifyCodeMsz: '',
            verificationCode: '',
            createNewPwdErr: false,
            createNewPwdErrMsz: '',
            password: '',
            confirmPassword: '',
            emailErrorBorder: false,
            cellNumberErrorBorder: false,
            accountTypeBorder: false,
            verifyCodeBorder: false,
            resetCfrmPasswordBorder: false,
            resetPasswordBorder: false,
            cellNumberErrorMsz: '',
            emailErrorMsz: '',
            isscreen1FormValid: false,
            isemailOrcellNumberValid: false,
            isAccountTypeValid: false,
            verifyCodeFormValid: false,
            isscreen3FormValid: false,
            username:'',
            hideScreen1:false,
            userNameList:[],
            usernameMsz:'',
            usernameBorder: false,
            jsonData:{}
        }
    }

    componentDidMount() {
        this.Constants = new Constants();
        this.Fsnethttp = new Fsnethttp();
        this.getJsonData()
        //Check if user already logged in
        //If yes redirect to dashboard page
        // if(this.FsnetAuth.isAuthenticated()){
        //     this.props.history.push('/dashboard');
        // }
    }

    getJsonData = () => {
        this.Fsnethttp.getJson('login').then(res=>{
            this.setState({
                jsonData:res.data
            })
        });
        
    }

    // Update state params values and login button visibility

    updateStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, () => {
            this.enableDisableForgotButtion();
        });
    }

    // Enable / Disble functionality of Login Button

    enableDisableForgotButtion = () => {
        let status = (this.state.isemailOrcellNumberValid) ? true : false;
        this.setState({
            isscreen1FormValid: status
        });
    }

    handleChangeEvent = (event, type) => {
        let dataObj = {};
        switch (type) {
            case 'email':
                if (event.target.value.trim() === '' && (this.state.cellNumber === '' || this.state.cellNumber === undefined)) {
                    this.setState({
                        email: event.target.value,
                        isemailOrcellNumberValid: false
                    })
                    dataObj = {
                        isemailOrcellNumberValid: false
                    };
                } else {
                    this.setState({
                        email: event.target.value,
                        forgotScreen1ErrorMsz: '',
                        emailErrorMsz: '',
                        emailErrorBorder: false,
                        isemailOrcellNumberValid: true
                    })
                    dataObj = {
                        isemailOrcellNumberValid: true
                    };
                }

                this.updateStateParams(dataObj);
                break;
            case 'cellNumber':
                if ((event === '' || event === undefined) && this.state.email === '') {
                    this.setState({
                        cellNumber: '',
                        isemailOrcellNumberValid: false
                    })
                    dataObj = {
                        isemailOrcellNumberValid: false
                    };
                } else {
                    this.setState({
                        cellNumber: event,
                        forgotScreen1ErrorMsz: '',
                        cellNumberErrorMsz: '',
                        cellNumberErrorBorder: false,
                        isemailOrcellNumberValid: true
                    })
                    dataObj = {
                        isemailOrcellNumberValid: true
                    };
                }
                this.updateStateParams(dataObj);
                break;
            case 'username':
                if (event.target.value === '0' || event.target.value === '' || event.target.value === undefined) {
                    this.setState({
                        usernameBorder: true,
                        usernameMsz: this.Constants.USER_NAME_REQUIRED,
                        username:'',
                        isscreen1FormValid: false,
                    })
                } else {
                    this.setState({
                        username: event.target.value,
                        usernameBorder: false,
                        usernameMsz:'',
                        isscreen1FormValid:true
                    })
                   
                }
                break;
            case 'password':
                if (event.target.value.trim() === '' || this.state.confirmPassword === '') {
                    this.setState({
                        isscreen3FormValid: false,
                    })
                } else {
                    this.setState({
                        isscreen3FormValid: true,
                    })
                }
                this.setState({
                    password: event.target.value,
                    createNewPwdErr: false,
                    createNewPwdErrMsz: '',
                    resetPasswordBorder: false,
                })
                break;
            case 'confirmPassword':
                if (event.target.value.trim() === '' || this.state.password === '') {
                    this.setState({
                        isscreen3FormValid: false,
                    })
                } else {
                    this.setState({
                        isscreen3FormValid: true,
                    })
                }
                this.setState({
                    confirmPassword: event.target.value,
                    createNewPwdErr: false,
                    createNewPwdErrMsz: '',
                    resetCfrmPasswordBorder: false,
                })
                break;
            default:
            // do nothing
        }
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    clean = (obj) => {
        for (var propName in obj) {
            if (obj[propName] === null || obj[propName] === undefined || obj[propName] === '') {
                delete obj[propName];
            }
        }
        return obj;
    }


     //Get all usernames 
     checkUserNames = () => {
         if(!this.state.hideScreen1) {
             let postObj = { email: this.state.email}
             this.Fsnethttp.forgotUsername(postObj).then(res => {
                 this.close();
                 if (res.data) {
                     if(res.data && res.data.data.length === 1) {
                         this.setState({
                             username: res.data.data[0],
                         },()=>{
                         })
                     } else if(res.data && res.data.data.length > 0) {
                         this.setState({
                             hideScreen1: true,
                             isscreen1FormValid:false,
                             userNameList: res.data.data
                         })
                     } else {
                         this.setState({
                             forgotScreen1ErrorMsz: this.Constants.FORGOT_USER_NOT_EXISTS
                         })
                     }
                 }
             })
             .catch(error => {
                 this.close();
                 if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        forgotScreen1ErrorMsz: error.response.data.errors[0].msg,
                    })
                 }else {
                    this.setState({
                        forgotScreen1ErrorMsz: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                 }
             });
         } else {
         }
    }


    sendCodeAgain = () => {
        let forgotObj = { email: this.state.email.trim()}
        let newObj = this.clean(forgotObj)
        this.Fsnethttp.forgotUsername(newObj).then(result => {
            this.close();
            if (result.data) {
                this.setState({
                    showForgotScreen1: false,
                    showForgotScreen4: true,
                    hideScreen1:false
                });
            }
        })
            .catch(error => {
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        forgotScreen1ErrorMsz: error.response.data.errors[0].msg
                    })
                }
            });
    }
    //Show forgot screens based on the screen type.
    showForgotScreenFn = (event,screen) => {
        switch (screen) {
            case 'screen1':
                let valid = this.screen1Validations();
                if (!valid) {
                    this.open();
                    this.sendCodeAgain();
                }
                break;
            case 'screen2':
                if (this.state.verificationCode !== '') {
                } else {
                    this.setState({
                        invalidVerifyCodeMsz: this.Constants.INVALID_VERIFY_CODE,
                        verifyCodeBorder: true
                    })
                }
                break;
            case 'screen3':
                let resetPwdvalid = this.screen3Validations();
                if (!resetPwdvalid) {
                    this.open();
                }
                break;
            case 'screen4':
                window.location = '/';;
                break;
            default:
            // do nothing
        }

    }

    screen1Validations = () => {
        let email = this.state.email.trim();
        let number = this.state.cellNumber;
        // let type = this.state.accountType;
        let error = false;
        let emailRegex = this.Constants.EMAIL_REGEX;
        // if (type === '0' || type === '') {
        //     this.setState({
        //         accountTypeRequired: this.Constants.ACCOUNT_TYPE_REQUIRED,
        //         accountTypeBorder: true
        //     })
        //     error = true;
        // }

        if (email === '' && (number === '' || number === undefined)) {
            this.setState({
                forgotScreen1ErrorMsz: this.Constants.EMAIL_MOBILE_REQUIRED,
                emailErrorBorder: true,
                cellNumberErrorBorder: true
            })
            error = true;
        }

        if (email !== '' && !emailRegex.test(email)) {
            this.setState({
                emailErrorMsz: this.Constants.VALID_EMAIL,
                emailErrorBorder: true,

            })
            error = true;
        }

        if (number !== '' && number !== undefined && (number.length < 12 || number.length > 13)) {
            this.setState({
                cellNumberErrorMsz: this.Constants.CELL_NUMBER_VALID,
                cellNumberErrorBorder: true,

            })
            error = true;
        }

        // else if(this.state.cellNumber !== undefined) {
        //     if((this.state.cellNumber.length < 12 || this.state.cellNumber.length > 13) && email === '') {
        //         error = true;
        //         this.setState({
        //             forgotScreen1ErrorMsz: this.Constants.CELL_NUMBER_VALID
        //         })
        //     }
        // }

        if (error) {
            return true
        } else {
            return false
        }
    }

    screen3Validations = () => {
        let password = this.state.password.trim();
        let cnfrmPassword = this.state.confirmPassword.trim();
        let error = false;
        if (password === '') {
            this.setState({
                resetPasswordBorder: true
            });
        }
        if (cnfrmPassword === '') {
            this.setState({
                resetCfrmPasswordBorder: true
            });
        }
        if (password === '' || cnfrmPassword === '') {
            this.setState({
                createNewPwdErr: true,
                createNewPwdErrMsz: this.Constants.LOGIN_PASSWORD_REQUIRED,
            });
            error = true;
        } else if (password !== cnfrmPassword) {
            this.setState({
                createNewPwdErr: true,
                createNewPwdErrMsz: this.Constants.REQUIRED_PASSWORD_AGAINPASSWORD_SAME,
            });
            error = true;
        } else {
            let passwordRegex = this.Constants.PASSWORD_REGEX;

            if (!passwordRegex.test(this.state.password) || !passwordRegex.test(this.state.confirmPassword)) {
                this.setState({
                    createNewPwdErr: true,
                    createNewPwdErrMsz: this.Constants.PASSWORD_NOT_MATCH,
                });
                error = true;
            }
        }

        if (error) {
            return true
        } else {
            return false
        }
    }

    render() {
        return (
            <div className="parentContainer" id="forgotContainer">
                <HeaderComponent></HeaderComponent>
                <Row className="forgotContainer">
                    <div className="forgotTopBorder"></div>
                    <div className="forgotParentDiv">
                        <h1 className="forgot-text" hidden={this.state.showForgotScreen4}>{this.state.jsonData.FORGOT_USERNAME}</h1>
                        {!this.state.hideScreen1 && !this.state.showForgotScreen2 && !this.state.showForgotScreen3 && !this.state.showForgotScreen4
                        ?
                            <div className="instructions-content" hidden={!this.state.showForgotScreen1}>{this.state.jsonData.FORGOT_USERNAME_SCREEN1_INSTRUCTIONS}</div>
                        : 
                            null
                        }
                        {/* <div className="instructions-content" hidden={!this.state.showForgotScreen1}>Enter your email or phone below to receive instructions to reset your password.</div> */}
                        <div className="instructions-content" hidden={!this.state.hideScreen1}>{this.state.jsonData.FORGOT_PASSWORD_MULTIPLE_USERS_TEXT}</div>
                        <div className="instructions-content marginBottom30" hidden={!this.state.showForgotScreen2}>{this.state.jsonData.FORGOT_PASSWORD_SCREEN2_TEXT}</div>
                        <div className="instructions-content" hidden={!this.state.showForgotScreen3}>{this.state.jsonData.FORGOT_PASSWORD_SCREEN3_TEXT}</div>
                        <Row hidden={!this.state.showForgotScreen1}>
                            <form>
                                <div hidden={this.state.hideScreen1} className="marginTop24">
                                    <Col lg={12} md={12} sm={12} xs={12} className="marginBot20">
                                        <label className="forgot-label-text">{this.state.jsonData.EMAIL_ADDRESS}</label>
                                        <input type="text" name="email" onChange={(e) => this.handleChangeEvent(e, 'email')} className={"forgotFormControl " + (this.state.emailErrorBorder ? 'inputError' : '')} placeholder={this.state.jsonData.EMAIL_ADDRESS_PLACEHOLDER} /><br />
                                        <span className="error">{this.state.emailErrorMsz}</span>
                                    </Col>
                                </div>
                                <Col lg={12} md={12} sm={12} xs={12} className="marginTop10" hidden={!this.state.hideScreen1}>
                                    <label className="forgot-label-text">{this.state.jsonData.USER_NAME_REQUIRED}</label>
                                    <FormControl defaultValue="0" className={"forgotFormControl " + (this.state.usernameBorder ? 'inputError' : '')} componentClass="select" onChange={(e) => this.handleChangeEvent(e, 'username')} placeholder="Select Role">
                                        <option value={0}>Select Username</option>
                                        {this.state.userNameList.map((record, index) => {
                                            return (
                                                <option value={record}  key={index} >{record}</option>
                                            );
                                        })}
                                    </FormControl>
                                    <span className="error">{this.state.usernameMsz}</span>
                                </Col>
                                <div className="error marginLeft15">{this.state.forgotScreen1ErrorMsz} </div>
                                <Row>
                                    <Button className={"forgot-submit " + (this.state.isscreen1FormValid ? 'btnEnabled' : '')} disabled={!this.state.isscreen1FormValid} onClick={(e) => this.showForgotScreenFn(e, 'screen1')}>{this.state.jsonData.SUBMIT_TEXT}</Button> <br />
                                </Row>
                                <label className="cancel-text cancelBtn"> <a href="/login">{this.state.jsonData.CANCEL_TEXT}</a></label>
                            </form>
                        </Row>
                        <Row hidden={!this.state.showForgotScreen4}>
                            <Col lg={12} md={12} sm={12} xs={12}>
                                <label className="forgot-success-text">{this.state.jsonData.SUCCESS}</label>
                                <label className="forgot-reset-text">{this.state.jsonData.FORGOT_USERNAME_SCREEN4_TEXT}</label>
                                <div className="success-icon">
                                    {/* <i className="fa fa-check-circle" aria-hidden="true"></i> */}
                                    <img src={successImage} alt="success" />
                                </div>
                                <div className="text-center">
                                    <Button className="forgot-submit fsnetBtn" onClick={(e) => this.showForgotScreenFn(e, 'screen4')}>{this.state.jsonData.SIGN_IN_TEXT}</Button> <br />
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Row>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default ForgotUserNameComponent;


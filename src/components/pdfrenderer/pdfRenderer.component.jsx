import React, { Component } from "react";
import {
  Button,
  Modal,
  FormControl,
  Row,
  Col,
  Form,
  ControlLabel
} from "react-bootstrap";
import html2canvas from "html2canvas";
import "./pdfRenderer.component.css";
import Loader from "../../widgets/loader/loader.component";
import { reactLocalStorage } from "reactjs-localstorage";
import { Fsnethttp } from "../../services/fsnethttp";
import { FsnetAuth } from "../../services/fsnetauth";
import { FsnetUtil } from "../../util/util";
import { Constants } from "../../constants/constants";
import { thisTypeAnnotation } from "@babel/types";

class PdfRenderer extends Component {
  constructor(props) {
    super(props);
    this.FsnetAuth = new FsnetAuth();
    this.Constants = new Constants();
    this.FsnetUtil = new FsnetUtil();
    this.Fsnethttp = new Fsnethttp();
    this.state = {
      showModal: false,
      showConfirmModal: false,
      showSignatureModal: false,
      signatureName: '',
      signatureFont: "azkia-demo",
      password: "",
      lpsubscriptionTotalObj: null,
      investorType: 'LLC',
      subscriptionAgreementUrl: null,
      fundAgreementUrl: null,
      subscriptionId: null,
      noAuthenticationPassword: false,
      signaturePasswordMsz: "",
      type: null,
      step: 'SA',
      accountType: this.FsnetUtil.getAdminRole(),
      subscriptionAgreementName: '',
      userName: this.FsnetUtil.userName(),
      title: null,
      disableSigButton: false,
      titleMask: '',
      sideLetterUrl: null,
      ammendmentUrl: [],
      ammendmentCount: 0,
      signFlowArrayDocs:[],
      isFinalDoc:false,
      fundId:null,
      isChangedPages:false
    };
  }

  componentDidMount() {
    const urlValues = this.FsnetUtil.decodeUrl();
    if (this.FsnetUtil._isIEEdge()) {
      this.open();
      localStorage.setItem('token', JSON.stringify(urlValues.token));
      this.getUserDataThroughToken();
      setTimeout(() => {
       this.checkUserAuthentication(urlValues)
      }, 2000);
    } else {
      this.checkUserAuthentication(urlValues);
    }
  }


  checkUserAuthentication = (urlValues) => {
    if(this.FsnetAuth.isAuthenticated()) {
      if(['LP','FSNETAdministrator','SecondaryLP'].indexOf(this.FsnetUtil.getUserRole()) > -1 ) {
        this.renderData(urlValues);
      } else {
        window.location.href = '/403'
      }
    } else {
      window.location.href = '/'
    }
  }


  

  getUserDataThroughToken = () => {
    this.Fsnethttp.getUserInfo().then(result => {
      localStorage.setItem('userData', JSON.stringify(result.data));
    })
      .catch(error => {
      });
  }

  renderData = (urlValues) => { console.log(urlValues);
    this.setState({
      type: urlValues.type,
      subscriptionId: this.FsnetUtil._decrypt(urlValues.sId),
      fundId:urlValues.fundId ? this.FsnetUtil._decrypt(urlValues.fundId) : null,
      isChangedPages: urlValues.fundId ? true : false
    }, () => {
      if (urlValues.type === 'SA') {
        this.getSA(this.FsnetUtil._decrypt(urlValues.sId));
      } else {
        this.getPdfUrl(urlValues);
      }
    })
  }


  getPdfUrl = (data) => {
    this.open();
    let url;
    if (data.type === 'AA') {
      url = `${this.Constants.BASE_URL}document/view/dfsammendment/${this.FsnetUtil._decrypt(data.sId)}/${this.FsnetUtil._decrypt(data.documentId)}`
    } else if (data.type === 'CC') {
      url = `${this.Constants.BASE_URL}document/view/ccfile/${this.FsnetUtil._decrypt(data.sId)}`
    } else if (data.type === 'SL') {
      url = `${this.Constants.BASE_URL}document/view/sia/${this.FsnetUtil._decrypt(data.sId)}`
    } else if (data.type === 'FA') {
      url = `${this.Constants.BASE_URL}document/view/afa/${this.FsnetUtil._decrypt(data.sId)}/${this.FsnetUtil._decrypt(data.documentId)}`
    } else if (data.type === 'CP') {
      url = `${this.Constants.BASE_URL}document/view/cp/${this.FsnetUtil._decrypt(data.sId)}/${this.FsnetUtil._decrypt(data.documentId)}`
    } else if (data.type === 'CPFA') {
      url = `${this.Constants.BASE_URL}document/view/afa/${this.FsnetUtil._decrypt(data.sId)}/${this.FsnetUtil._decrypt(data.documentId)}`
      this.getCPForClosedInvestors(url, this.FsnetUtil._decrypt(data.sId), this.FsnetUtil._decrypt(data.documentId)); // getting file paths
    }

    if (data.type !== 'CPFA') {
      this.setState({
            docUrl: `${url}?token=${this.FsnetUtil.getToken()}`,
            changedPageUrl: this.state.isChangedPages ? `${this.Constants.BASE_URL}document/view/cp/${this.FsnetUtil._decrypt(data.fundId)}?token=${this.FsnetUtil.getToken()}` : null
          },
          () => {
            //Check doc has permission.
            this.Fsnethttp.checkDocForSigning(url).then(result => {
            })
                .catch(error => {
                });
            this.closeLoader()
          });
    }

  }

  closeLoader = () => {
    const value = this.state.type === 'SA' ? 10000 : 3000
    setTimeout(() => {
      this.close();
    }, value);
  }

  getCPForClosedInvestors = (url, subscriptionId,documentId) => {
    this.Fsnethttp.getCPForClosedInvestors(subscriptionId,documentId).then(result => {
        console.log('2nd state');
      this.setState({
            docUrl: `${url}?token=${this.FsnetUtil.getToken()}`,
            changedPageUrl:  `${result.data.changedPageUrl}?token=${this.FsnetUtil.getToken()}`
          },
          () => {
            //Check doc has permission.
            this.Fsnethttp.checkDocForSigning(url).then(result => {
            })
                .catch(error => {
                });
            this.closeLoader()
          });

    }).catch(error => {
      this.close();
    })
  }

  getSA = (id) => {
    if (id && this.FsnetUtil.getToken()) {
      this.open();
      this.Fsnethttp.getSubscriptionAgrement(id, 'currentSignatureView').then(result => {
        if (result.data) {
          this.setState(
            {
              subscriptionAgreementUrl: result.data.subscriptionAgreementUrl ? result.data.subscriptionAgreementUrl + '?token=' + this.FsnetUtil.getToken() : null,
              fundAgreementUrl: result.data.fundAgreementUrl ? result.data.fundAgreementUrl + '?token=' + this.FsnetUtil.getToken() : null,
              capitalcommitmentUrl: result.data.capitalcommitmentUrl ? result.data.capitalcommitmentUrl + '?token=' + this.FsnetUtil.getToken() : null,
              changedPageUrl: result.data.changedPageUrl ? result.data.changedPageUrl + '?token=' + this.FsnetUtil.getToken() : null,
              sideLetterUrl: result.data.sideLetterUrl ? result.data.sideLetterUrl + '?token=' + this.FsnetUtil.getToken() : null,
              ammendmentUrl: result.data.ammendmentUrl ? result.data.ammendmentUrl : [],
              subscriptionId: id,
              getAmmendmentUrl: result.data.ammendmentUrl ? result.data.ammendmentUrl[0]+ '?token=' + this.FsnetUtil.getToken():null,
              subscriptionAgreementName: result.data.subscription ? result.data.subscription.subscriptionAgreementName : '',
              investorType: result.data.subscription ? result.data.subscription.investorType : 'LLC',
              signatureName: result.data.subscription ? result.data.subscription.dynamicTitle : this.FsnetUtil.userName(),
              title: result.data.subscription.investorType === 'LLC' ? result.data.subscription.dynamicTitle : result.data.subscription.investorType === 'Trust' ? result.data.subscription.dynamicTitle ? result.data.subscription.dynamicTitle : 'Trustee' : result.data.subscription.dynamicTitle ? result.data.subscription.dynamicTitle : '',
              titleMask: result.data.subscription.investorType === 'LLC' ? result.data.subscription.dynamicTitle : result.data.subscription.investorType === 'Trust' ? result.data.subscription.dynamicTitle ? result.data.subscription.dynamicTitle : 'Trustee' : result.data.subscription.dynamicTitle ? result.data.subscription.dynamicTitle : ''
            },
            () => {
              this.closeLoader();
              this.mergetDocsInArray();
            }
          );
        }
      })
        .catch(error => {
          this.close();
        });
    }
  }

  mergetDocsInArray = () => {
    let docArray = [];
    if(this.state.subscriptionAgreementUrl) {
      docArray.push('SA')
    } 
    if(this.state.changedPageUrl) {
      docArray.push('CP')
    }
    if(this.state.fundAgreementUrl) {
      docArray.push('FA')
    } 
    if(this.state.sideLetterUrl) {
      docArray.push('SL')
    } 
    if(this.state.ammendmentUrl.length > 0) {
      docArray.push('AA')
    } 
    this.setState({
      signFlowArrayDocs:docArray
    },()=>{
      this.setState({
        step:this.state.signFlowArrayDocs[0],
        isFinalDoc: docArray.length === 1 ? true:this.state.isFinalDoc
      })
    })

  }

  changeState = (e, step) => {
    this.open();
    this.setState({ step: step }, () => {
      setTimeout(() => {
        this.close();
        this.getAmmendmentUrlFromList()
      }, 1000);
    })
  }

  close = () => {
    this.setState({ showModal: false });
  }

  // ProgressLoader : show progress loade
  open = () => {
    this.setState({ showModal: true });
  }

  initHeightForModal = () => {
    setTimeout(() => {
      this.changeSignatureStyles()
    }, 100);
  }

  openSignatureModal = (isCapital) => {
    this.setState({ signatureName: this.FsnetUtil.userName() });
    this.setState({ showSignatureModal: true }, () => {
      this.initHeightForModal();
      if (
        navigator.userAgent.indexOf("MSIE") != -1 ||
        !!document.documentMode == true
      ) {
        document.getElementById("pdf-view").style.display = "none";
      }
    });
  };

  openConfiramtionModal = () => {
    this.setState({ showConfirmModal: true });
  };

  closeSignatureModal = () => {
    if (
      navigator.userAgent.indexOf("MSIE") != -1 ||
      !!document.documentMode == true
    ) {
      document.getElementById("pdf-view").style.display = "block";
    }
    this.setState({
      showSignatureModal: false,
      password: "",
      noAuthenticationPassword: false,
      signaturePasswordMsz: "",
    });
  };

  closeConfirmModal = () => {
    this.setState({ showConfirmModal: false });
  };

  checkError = () => {
    if (this.state.type === 'SA' && this.state.investorType !== 'Individual') {
      if (this.state.title === '' || this.state.title === null) {
        this.setState({
          signaturePasswordMsz: this.state.investorType === 'LLC' ? 'Please Enter the value for Signatory Title' : "Please Enter the value for Signatory Title"
        })
        return true;
      }
    }
    return false;
  }

  convertToImage = () => {
    let value = this.checkError();
    if (!value) {
      this.open();
      html2canvas(document.getElementById("img_val")).then(canvas => {
        let postObj = {
          signaturePic: canvas.toDataURL("image/png"),
          signaturePassword: this.state.password,
          subscriptionId: this.state.subscriptionId,
          date: new Date(),
          timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
          subscriptionFileName: this.state.subscriptionAgreementName
        };
        if (this.state.investorType === 'LLC') {
          postObj['entityTitle'] = this.state.title
        } else if (this.state.investorType === 'Trust') {
          postObj['trustTitle'] = this.state.title
        } else if (this.state.investorType === 'Individual' && this.state.title) {
          postObj['legalTitle'] = this.state.title
        }
        if (this.state.type !== "SA") {
          const urlValues = this.FsnetUtil.decodeUrl();
          postObj[
            "documentId"
          ] = this.FsnetUtil._decrypt(urlValues.documentId);
        }
        this.Fsnethttp
          .signatureAppendAPI(this.state.type, postObj)
          .then(result => {
            reactLocalStorage.set(
              "signedDoc",
              JSON.stringify({
                type: "success",
                url: this.state.type !== "SA" ? result.data.url : null
              })
            );
            this.close();
            this.closeSignatureModal();
            window.close();
          })
          .catch(error => {
            this.close();
            if (
              error.response !== undefined &&
              error.response.data !== undefined &&
              error.response.data.errors !== undefined &&
              error.response.data.errors[0].code === "SET_PASSWORD"
            ) {
              this.setState({
                noAuthenticationPassword: true
              });
            } else if (
              error.response !== undefined &&
              error.response.data !== undefined &&
              error.response.data.errors !== undefined
            ) {
              this.setState({
                signaturePasswordMsz: error.response.data.errors[0].msg
              });
            }
          });
      });
    }
  };

  changeSignatureStyles = () => {
    if (this.state.userName && this.state.userName.length > 25) {
      document.getElementById('img_val').style.lineHeight = '40px';
    } else {
      document.getElementById('img_val').style.lineHeight = 'initial';
      document.getElementById('img_val').style.height = 'initial';
    }
  }

  onChange = field => event => {
    this.setState({ [field]: event.target.value, signaturePasswordMsz: '', disableSigButton: event.target.value.trim() === '' && field == 'title' ? true : false }, () => {
      if (field !== 'title') {
        this.initHeightForModal();
      }
    });
  };

  getAmmendmentUrlFromList = () => {
    if (this.state.ammendmentUrl.length > 0 && this.state.step === 'AA') {
      let count = this.state.ammendmentCount;
      this.setState({
        ammendmentCount: this.state.ammendmentCount === this.state.ammendmentUrl.length ? this.state.ammendmentCount : this.state.ammendmentCount + 1,
        getAmmendmentUrl: count === this.state.ammendmentUrl.length ? this.state.getAmmendmentUrl : `${this.state.ammendmentUrl[count]}?token=${this.FsnetUtil.getToken()}`,
        isFinalDoc: count+1 === this.state.ammendmentUrl.length ? true:false
      }, () => {
        if (count === this.state.ammendmentUrl.length) {
          this.openSignatureModal(false)
        }
      })
    }
  }

  getDocTypeName = (type) => {
    let name;
    if (type === 'CC') {
      name = 'Capital Commitment'
    } else if (type === 'SL') {
      name = 'Side Letter'
    } else if (type === 'AA') {
      name = 'Amendment to Fund Agreement'
    } else if (type === 'FA' || !type) {
      name = 'Fund Agreement'
    } else if (type === 'CP') {
      name = 'Changed Pages'
    } else if (type === 'CPFA') {
      name = 'Fund Agreement'
    }
    return name;
  }


  getCurrentUrlDoc = () => {
    let url;
    if(this.state.step === 'SL') {
      url = this.state.sideLetterUrl
    } else if (this.state.step === 'CP') {
      url = this.state.changedPageUrl
    } else if (this.state.step === 'FA') {
      url = this.state.fundAgreementUrl
    } else if (this.state.step === 'SA') {
      url = this.state.subscriptionAgreementUrl
    } 
    return url;
  }

  getButtonName = () => {
    let name = 'Approved.';
    let index = this.state.signFlowArrayDocs.indexOf(this.state.step)
    let getNextDoc = this.state.signFlowArrayDocs[index+1];
    name = `${this.state.step !== 'CP' ? 'Approved. ':''}Continue to review ${this.state.step === 'AA'? 'Amendment to Fund Agreement': this.getDocTypeName(getNextDoc)}`
    return name;
  }

  changeStep = () => {
    this.open();
    let index = this.state.signFlowArrayDocs.indexOf(this.state.step)
    let getNextDoc = this.state.signFlowArrayDocs[index+1];
    this.setState({
      step:this.state.step === 'AA' ? 'AA':getNextDoc,
      isFinalDoc: getNextDoc !== 'AA' && this.state.signFlowArrayDocs.indexOf(getNextDoc) === this.state.signFlowArrayDocs.length-1 ? true : false
    },()=>{
      if(this.state.step === 'AA') {
        this.getAmmendmentUrlFromList()
      }
      setTimeout(() => {
        this.close()
      }, 1000);
    })
  }

  timingLoader = () => {
    this.open();
    this.setState({isChangedPages:false})
    setTimeout(() => {
      this.close();
    }, 3000);
  }

  handleOnEnterKey = (event) => {
    if (event.key === 'Enter') {
        event.preventDefault();
    }
  }

  render() {
    return (
      <React.Fragment>
        {
            this.state.type === 'SA' ?
            <div className="pdf">
              <h1 className="title pdf-title">
                   {this.FsnetUtil._getTitleName(this.state.step)}
              </h1>
              {
                this.state.isFinalDoc ? 
                <Button type="button" className={"fsnetSubmitButton fontFamilyBold fundapprovedBtn " + (this.state.accountType === 'FSNETAdministrator' ? 'disabled':'btnEnabled')} onClick={() => this.openSignatureModal(false)}>
                      Approved. Apply Electronic Signature to All Reviewed Documents
                    </Button>
                :
                <Button type="button" className="fsnetSubmitButton btnEnabled fontFamilyBold subapprovedBtn" onClick={this.changeStep}>
                      {this.getButtonName()}
                  </Button>
              }
              <div className="iframe-div">
                <iframe className="pdf-frame" src={this.state.step === 'AA' ? this.state.getAmmendmentUrl : this.getCurrentUrlDoc()} scrolling="no" frameBorder="0" />
              </div>
                   
            </div>
              
              :
              this.state.isChangedPages ?
                <div className="pdf">
                    <h1 className="title pdf-title">
                      Please review the Changed pages document.
                    </h1>
                    <Button type="button" className="fsnetSubmitButton btnEnabled fontFamilyBold subapprovedBtn" onClick={this.timingLoader}>
                      Continue to review Fund Agreement
                    </Button>
                    <div className="iframe-div">
                      <iframe className="pdf-frame" src={this.state.changedPageUrl} scrolling="no" frameBorder="0" id="pdf-view" />
                    </div>
                </div>
              :

              <div className="pdf">
                <h1 className="title pdf-title">
                  Please review the {this.getDocTypeName(this.state.type)} document below. Once you are satisfied, please click “Confirm and Sign” to append your signature.
                </h1>
                <Button type="button" className={"fsnetSubmitButton fontFamilyBold " + (this.state.accountType === 'FSNETAdministrator' ? 'disabled' : 'btnEnabled')} onClick={() => this.openSignatureModal(true)}>
                  Confirm and Sign
                </Button>
                <div className="iframe-div">
                  <iframe className="pdf-frame" src={this.state.docUrl} scrolling="no" frameBorder="0" id="pdf-view" />
                </div>
              </div>
        }

        <Modal
          backdrop="static" className={!this.state.noAuthenticationPassword ? "generateSignature" : "passwordModal"} show={this.state.showSignatureModal} onHide={this.closeSignatureModal}>
          <Modal.Header closeButton />
          <Modal.Body hidden={this.state.noAuthenticationPassword}>
            <label className="label-text marginBot12 width100 fontFamilyBoldSize">
              Configure and Append Signature
            </label>
            <Row className="marginEdit">
              <Col lg={12} md={12} sm={12} xs={12} className="width40 paddingZero">
                <label className="input-label">Signature Fonts</label>
                <FormControl name="signatureFont" className="selectFormControl" componentClass="select" value={this.state.signatureFont} onChange={this.onChange("signatureFont")}>
                  <option value="azkia-demo">Symphony</option>
                  <option value="adine-kirnberg">Affinity</option>
                  <option value="400 30px akadora">Caviar</option>
                  <option value="400 30px 'Great Vibes', Helvetica, sans-serif">
                    Great Vibes
                  </option>
                </FormControl>
              </Col>
            </Row>
            {this.state.signatureFont === "adine-kirnberg" ||
              this.state.signatureFont === "azkia-demo" ? (
                <div id="img_val" style={{ fontFamily: this.state.signatureFont }}>
                  {this.FsnetUtil.userName()}
                </div>
              ) : (
                <div id="img_val" style={{ font: this.state.signatureFont }}>
                  {this.FsnetUtil.userName()}
                </div>
              )}
            <Form autoComplete="off">

              {
                this.state.type === 'SA' &&
                <div>
                  {
                    (this.state.investorType == 'LLC' || this.state.investorType == 'Trust') &&
                    <ControlLabel className="block marginTop10">{this.state.investorType == 'LLC' ? 'Enter your Signatory Title within the Entity' : 'Enter your Signatory Title within the Trust'}</ControlLabel>
                  }
                  {
                    (this.state.investorType === 'Individual' && this.state.titleMask !== '') &&
                    <ControlLabel className="block marginTop10">Enter your Signatory Title within the Joint Individual</ControlLabel>
                  }
                  <FormControl type="text" name="title" className="inputFormControl  marginBot20" placeholder={this.state.investorType == 'LLC' ? 'Director' : 'Trustee'} autoComplete="off" value={this.state.title} hidden={this.state.titleMask === ''} onChange={this.onChange("title")} />
                </div>
              }
              <ControlLabel className="marginTop10 block">Please re-enter your password </ControlLabel>
              <ControlLabel className="marginTop10 font14">{this.Constants.PASSWORD_MESSAGE}</ControlLabel>
              <FormControl type="password" name="signaturePassword" className="inputFormControl  marginBot10" onKeyPress={this.handleOnEnterKey} autoComplete="new-password" value={this.state.password} placeholder="Enter password" onChange={e => this.setState({ password: e.target.value.trim() })} />
              <div className="error marginBot10 height20">
                {this.state.signaturePasswordMsz}
              </div>
              <Button type="button" className="fsnetSubmitButton btnEnabled uploadBtn fontFamilyBold" onClick={this.convertToImage} disabled={this.state.password === "" || this.state.disableSigButton}>
                {" "}
                Add Signature{" "}
              </Button>
            </Form>
          </Modal.Body>
          <Modal.Body hidden={!this.state.noAuthenticationPassword}>
            <div className="title">
              Please add authentication password using this{" "}
              <a href="/settings/authentication-password">link</a>.
            </div>
          </Modal.Body>
        </Modal>
        <Loader isShow={this.state.showModal} />
      </React.Fragment>
    );
  }
}

export default PdfRenderer;

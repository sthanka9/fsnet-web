import React, { Component } from 'react';
import './header.component.css';
import { Row, Col, Button, Modal, Radio } from 'react-bootstrap';
import userDefaultImage from '../../images/default_user.png';
import { reactLocalStorage } from 'reactjs-localstorage';
import { FsnetAuth } from '../../services/fsnetauth';
import profileImage from '../../images/key.svg';
import backImage from '../../images/back.svg';
import editImage from '../../images/edit.svg';
import notificationImage from '../../images/notifications.png';
import SidebarComponent from '../sidebar/sidebar.component';
import { Fsnethttp } from '../../services/fsnethttp';
import { FsnetUtil } from '../../util/util';
import { PubSub } from 'pubsub-js';
import Loader from '../../widgets/loader/loader.component';

import $ from 'jquery';
let userObj = {}, alert = {};
class HeaderComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.handleClick = this.handleClick.bind(this)
        this.state = {
            loggedInUserObj: [],
            showModal: false,
            showandhideUserDropdown: true,
            menuShow: false,
            userImage: userDefaultImage,
            allAlertsList: [],
            alertsCount: 0,
            allRoles: [],
            switchRoleId: null,
            SwitchAccountModal: false
        }

        alert = PubSub.subscribe('alertCount', (msg, data) => {
            this.getAllAlerts(userObj.id)
        });
    }

    //ProgressLoader : Close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () => {
        this.setState({ showModal: true });
    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
        PubSub.unsubscribe(alert);
    }

    toggleMenu = (value) => {
        if (value) {
            this.setState({
                alertsCount: value
            })
        }
        if (!this.state.menuShow) {
            $('.alerts-section').css({ '-webkit-overflow-scrolling': 'touch' })
            $('html,body').css({ 'overflow-y': 'hidden', 'height': '100%' })
        } else {
            $('html,body').css({ 'overflow-y': 'inherit', 'height': 'auto' })
        }
        this.setState({
            menuShow: !this.state.menuShow
        })
    }

    userDropdownList = () => {
        let value = this.state.showandhideUserDropdown;
        this.setState({
            showandhideUserDropdown: !value,
        })
    }

    logout = () => {
        if (this.FsnetUtil.getAdminRole() === 'FSNETAdministrator') {
            this.FsnetUtil._removeAdminData()
            setTimeout(() => {
                window.location.href = '/adminDashboard/firmView';
            }, 200);
        } else {
            this.Fsnethttp.logout().then(result => {
                this.FsnetUtil._ClearLocalStorage();
            });
        }
    }

    componentDidMount() {
        if (this.FsnetAuth.isAuthenticated()) {
            //Get user obj from local storage.
            userObj = reactLocalStorage.getObject('userData');
            let page = this.FsnetUtil.getLpFundId();
            if (userObj && page !== 'profile') {
                this.setState({
                    loggedInUserObj: userObj,
                    userImage: userObj.profilePic ? userObj.profilePic.url : userDefaultImage
                })
            } else {
                this.getUserInfo();
            }
            this.getAllAlerts(userObj.id)
        } else {
            window.location = '/';
        }
    }

    componentWillMount() {
        document.addEventListener('mousedown',this.handleClick,false);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown',this.handleClick,false);
    }

    handleClick = (e) => {
        if(this.node.contains(e.target)) {
            return;
        }

        this.handleClickOutSide();
    }

    handleClickOutSide = () => {
        this.setState({
            showandhideUserDropdown: true,
        })
    }

    getUserInfo = () => {

        this.Fsnethttp.getUserProfile().then(result => {
            let userData = JSON.parse(reactLocalStorage.get('userData'));
            userData['firstName'] = result.data.firstName;
            userData['middleName'] = result.data.middleName;
            userData['lastName'] = result.data.lastName;
            userData['cellNumber'] = result.data.cellNumber;
            userData['profilePic'] = result.data.profilePic;
            reactLocalStorage.set('userData', JSON.stringify(userData));
            this.setState({
                loggedInUserObj: userData,
                userImage: userData.profilePic ? userData.profilePic.url : userDefaultImage
            })
        })
            .catch(error => {
            });
    }



    getAllAlerts = (id) => {
        if (id) {

            this.Fsnethttp.getAllAlerts(id).then(result => {
                this.setState({
                    allAlertsList: result.data.data,
                    alertsCount: result.data.data.length
                })
            })
                .catch(error => {
                });
        }
    }

    switchUserRole = () => {
        this.open();
        const postObj = { userId: this.state.switchRoleId }
        this.Fsnethttp.switchAccount(postObj).then(result => {
            let data = result.data;
            this.FsnetUtil._userSetData(data);
            this.closeSwitchAccountModal();
            setTimeout(() => {
                this.close();
                window.location.href = '/dashboard';
            }, 100);
        })
            .catch(error => {
                this.close();
            });
    }

    handleSwitchRole = (event) => {
        this.setState({ switchRoleId: event.target.value })
    }

    getUserRoles = () => {
        this.open();
        this.Fsnethttp.listUsersForAccount().then(result => {
            this.close();
            this.setState({
                allRoles: result.data.allusers,
                SwitchAccountModal: true,
                switchRoleId: this.FsnetUtil.getUserId()
            }, () => {
                this.userDropdownList()
            })
        })
            .catch(error => {
                this.close();
            });
    }

    openSwitchAccountModal = () => {
        this.getUserRoles();
    }

    closeSwitchAccountModal = () => { this.setState({ SwitchAccountModal: false, switchRoleId: null }) }


    render() {
        return (
            <Col className="headers headersDashboard marginBot20">
                {
                    (this.FsnetUtil.getAdminRole() === 'FSNETAdministrator' && (window.location.href.indexOf('/dashboard') === -1 && window.location.href.indexOf('/lp') === -1)) &&
                    <div className="impersonateHeader text-center"><label className="title ellipsis">Impersonated as {this.FsnetUtil.getFullName(this.state.loggedInUserObj)}</label><Button type="button" className="fsnetSubmitButton btnEnabled" onClick={this.logout}>Go Back To Admin</Button></div>
                }
                <Row className="rightHeader">
                    <div className="user-dropdown-list" hidden={this.state.showandhideUserDropdown}  ref={node => this.node = node}>
                        <ul>
                            {
                                this.FsnetUtil.getUserRole() != 'FSNETAdministrator' &&
                                <li><img src={profileImage} alt="switch-account" className="dropDownImg" /><a onClick={this.openSwitchAccountModal}>Switch Role</a></li>
                            }
                            <li><img src={editImage} alt="edit-profile" className="dropDownImg" /><a href={`/settings/profile/${this.FsnetUtil._encrypt(this.state.loggedInUserObj.id)}`}>Edit Profile</a></li>
                            <li><img src={profileImage} alt="change-password" className="dropDownImg" /><a href={`/settings/change-password/${this.FsnetUtil._encrypt(this.state.loggedInUserObj.id)}`}>Change Password</a></li>
                            <li onClick={this.logout}><img src={backImage} alt="logout" className="dropDownImg" /><a>Log Out</a></li>
                        </ul>
                    </div>
                    <Col xs={8} className="" onClick={this.userDropdownList}>
                        <div>
                            <div className={"user-name " + (this.state.showandhideUserDropdown ? '' : 'active')} title={this.FsnetUtil.getFullName(this.state.loggedInUserObj)}>{this.FsnetUtil.getFullNameWithEllipsis(this.state.loggedInUserObj)}
                            </div>
                            {
                                window.location.href.indexOf('userRoles') == -1 &&
                                <span className="table-heading remember-text">{this.FsnetUtil.getAccountTypeName(this.FsnetUtil.getUserRole())}</span>
                            }
                        </div>
                    </Col>
                    <Col xs={1} className="downArrowIcon" onClick={this.userDropdownList}>
                        <i className="fa fa-chevron-down"></i>
                    </Col>
                    <Col xs={2} className="">
                        <img src={this.state.userImage} alt="profilePic" className="profilePic" />
                    </Col>
                    <Col xs={1} className="">
                        <img onClick={() => { this.toggleMenu() }} src={notificationImage} alt="notification-icon" className="notification-icon" />
                    </Col>
                    <span className={"notification-count " + (this.state.alertsCount >= 100 ? 'count3' : '')} hidden={this.state.alertsCount === 0}>{this.state.alertsCount}</span>
                </Row>
                <SidebarComponent visible={this.state.menuShow} toggleClose={this.toggleMenu}></SidebarComponent>
                <Loader isShow={this.state.showModal}></Loader>
                <Modal id="GPDelModal" show={this.state.SwitchAccountModal} onHide={this.closeSwitchAccountModal} className="switchRoleRadio">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body className={(this.state.allRoles.length <=1? 'height80 marginTop5' : '')}>
                        {
                            this.state.allRoles.length > 1 &&
                            <p className="title-text marginLeftNone">Please select a role to view:</p>
                        }
                        <Row className="marginLeftNone">
                            {
                                this.state.allRoles.length > 1 ?
                                    this.state.allRoles.map((record, index) => {
                                        return (
                                            <div key={index} style={{ width: '50%' }}>
                                                <Radio key={index} name="previousFund" value={record['id']} onChange={this.handleSwitchRole} checked={this.state.switchRoleId == record['id']} inline>{this.FsnetUtil.getAccountTypeName(record.accountType)}
                                                    <span className="radio-checkmark"></span>
                                                </Radio>
                                            </div>
                                        );
                                    })
                                    :
                                    <div className="title text-center width100">You do not currently have any other roles within Vanilla.</div>
                            }
                        </Row>
                        {
                            this.state.allRoles.length > 1 &&
                            <div className="text-center marginTop10">
                                <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.switchUserRole}>Switch Role</Button>
                            </div>
                        }
                    </Modal.Body>
                </Modal>
            </Col>
            // </Row>
        );
    }
}

export default HeaderComponent;




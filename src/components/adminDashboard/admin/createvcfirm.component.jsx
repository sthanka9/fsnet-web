import React, { Component } from 'react';
import './createvcfirm.component.css';
import { Row,Col, FormControl, Button,Radio, FormGroup} from 'react-bootstrap';
import {Fsnethttp} from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import {Constants} from '../../../constants/constants';
import {FsnetUtil} from '../../../util/util';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/rrui.css';
import 'react-phone-number-input/style.css';
import Loader from '../../../widgets/loader/loader.component';

var close = {}
class CreateVcFirmComponent extends Component{

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth(); 
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
        this.state = {
            showModal: false,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            firmNameBorder: false,
            firmName: '',
            firmNameValid: false,
            firmNameMsz: '',
            firmFirstNameBorder: false,
            firmFirstName: '',
            firmFirstNameValid: false,
            firmFirstNameMsz: '',
            firmLastNameBorder: false,
            firmLastName: '',
            firmLastNameValid: false,
            firmLastNameMsz: '',
            firmMiddleName:'',
            emailBorder: false,
            email: '',
            emailValid: false,
            emailMsz: '',
            cellNumberBorder: false,
            cellNumber: '',
            cellNumberValid: false,
            cellNumberMsz: '',
            isFormValid: false,
            allowGPdelegatesToSign: null,
            isImporsonatingAllowed: null,
            isImporsonatingAllowedValid: false,
            allowGPdelegatesToSignValid: false,
            subscriptonType:1,
            vcFirmError: '',
            jsonData:{},
            showHtml:false,
            userName:'',
            userNameExists:'',
            isUserNameValid: false,
            userNameBorder: false,
        }
    }

    componentDidMount() {
        if(this.FsnetAuth.isAdmin()){
            this.getJsonData()
            this.FsnetUtil.setLeftNavHeight();
        }else{
           window.location.href = '/';
        }  
    }

    closeToast = (timed) => {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    getJsonData() {
        this.Fsnethttp.getJson('admin').then(result=>{
            this.setState({
                jsonData:result.data,
                showHtml:true
            })
        });
        
    }

    redirectHome = () => {
        window.location.href = '/';
    }

    //Onchange event for all input text boxes.
    handleInputChangeEvent = (event,type) => {
        let dataObj = {}; 
        this.setState({
            vcFirmError: ''
        })
        switch(type) {
            case 'firmname':
                let firstNameValue = event.target.value.trim()
                if( firstNameValue === '' || firstNameValue === undefined) {
                    this.setState({
                        firmNameMsz: this.Constants.FIRM_NAME_REQUIRED,
                        firmName: '',
                        firmNameBorder: true,
                        firmNameValid: false
                    })
                    dataObj ={
                        firmNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        firmNameMsz: '',
                        firmName: event.target.value,
                        firmNameBorder: false,
                        firmNameValid: true
                    })
                    dataObj ={
                        firmNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'firmFirstName':
                let firmFirstNameValue = event.target.value.trim()
                if(firmFirstNameValue === '' || firmFirstNameValue === undefined) {
                    this.setState({
                        firmFirstNameMsz: this.Constants.FIRST_NAME_REQUIRED,
                        firmFirstName: '',
                        firmFirstNameBorder: true,
                        firmFirstNameValid: false
                    })
                    dataObj ={
                        firmFirstNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        firmFirstNameMsz: '',
                        firmFirstName: event.target.value,
                        firmFirstNameBorder: false,
                        firmFirstNameValid: true
                    })
                    dataObj ={
                        firmFirstNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'firmLastName':
                let firmLastNameValue = event.target.value.trim()
                if(firmLastNameValue === '' || firmLastNameValue === undefined) {
                    this.setState({
                        firmLastNameMsz: this.Constants.LAST_NAME_REQUIRED,
                        firmLastName: '',
                        firmLastNameBorder: true,
                        firmLastNameValid: false
                    })
                    dataObj ={
                        firmLastNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        firmLastNameMsz: '',
                        firmLastName: event.target.value,
                        firmLastNameBorder: false,
                        firmLastNameValid: true
                    })
                    dataObj ={
                        firmLastNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'firmMiddleName':
                let firmMiddleNameValue = event.target.value.trim()
                if(firmMiddleNameValue === '' || firmMiddleNameValue === undefined) {
                    this.setState({
                        firmMiddleName: '',
                    })
                } else {
                    this.setState({
                        firmMiddleName: event.target.value,
                    })
                }
                break;
            case 'email':
                let emailValue = event.target.value.trim()
                if(emailValue === '' || emailValue === undefined) {
                    this.setState({
                        emailMsz: this.Constants.VALID_EMAIL,
                        email: '',
                        emailBorder: true,
                        emailValid: false
                    })
                    dataObj ={
                        emailValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        emailMsz: '',
                        email: event.target.value,
                        emailBorder: false,
                        emailValid: true
                    })
                    dataObj ={
                        emailValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'cellNumber':
                if(event === '' || event === undefined) {
                    this.setState({
                        cellNumberMsz: this.Constants.CELL_NUMBER_VALID,
                        cellNumber: '',
                        cellNumberBorder: true,
                        cellNumberValid: false
                    })
                    dataObj ={
                        cellNumberValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        cellNumberMsz: '',
                        cellNumber: event,
                        cellNumberBorder: false,
                        cellNumberValid: true
                    })
                    dataObj ={
                        cellNumberValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'allowGPdelegatesToSign':
                if(event === 'on') {
                    this.setState({
                        allowGPdelegatesToSign: 1
                    })
                } else if(event === 'off') {
                    this.setState({
                        allowGPdelegatesToSign: 0
                    })
                }
                dataObj ={
                    allowGPdelegatesToSignValid :true
                };
                this.updateStateParams(dataObj);
                break;
            case 'isImporsonatingAllowed':
                if(event === 'on') {
                    this.setState({
                        isImporsonatingAllowed: 1
                    })
                } else if(event === 'off') {
                    this.setState({
                        isImporsonatingAllowed: 0
                    })
                }
                dataObj ={
                    isImporsonatingAllowedValid :true
                };
                this.updateStateParams(dataObj);
                break;
            case 'password':
                let passwordValue = event.target.value.trim()
                if(passwordValue === '' || passwordValue === undefined) {
                    this.setState({
                        passwordMsz: this.Constants.LOGIN_PASSWORD_REQUIRED,
                        password: '',
                        passwordBorder: true,
                        passwordValid: false
                    })
                    dataObj ={
                        passwordValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        passwordMsz: '',
                        password: passwordValue,
                        passwordBorder: false,
                        passwordValid: true
                    })
                    dataObj ={
                        passwordValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'cnfrmPassword':
                let cnfrmPasswordValue = event.target.value.trim()
                if(cnfrmPasswordValue === '' || cnfrmPasswordValue === undefined) {
                    this.setState({
                        cnfrmPasswordMsz: this.Constants.CNFRM_PWD_REQUIRED,
                        cnfrmPassword: '',
                        cnfrmPasswordBorder: true,
                        cnfrmPasswordValid: false
                    })
                    dataObj ={
                        cnfrmPasswordValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        cnfrmPasswordMsz: '',
                        cnfrmPassword: cnfrmPasswordValue,
                        cnfrmPasswordBorder: false,
                        cnfrmPasswordValid: true
                    })
                    dataObj ={
                        cnfrmPasswordValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            case 'username':
                //Add username value to state
                //Clear the username exists message
                if(event.target.value === '' || event.target.value === undefined) {
                    this.setState({
                        userNameExists: this.Constants.LOGIN_USER_NAME_REQUIRED,
                        isUserNameValid: false,
                        userNameBorder: true,
                        userName: ''
                    })
                    dataObj ={
                        isUserNameValid :false
                    };
                    this.updateStateParams(dataObj);
                } else {
                    this.setState({
                        userName: event.target.value,
                        userNameExists: '',
                        isUserNameValid: true,
                        userNameBorder: false,
                    })
                    dataObj ={
                        isUserNameValid :true
                    };
                    this.updateStateParams(dataObj);
                }
                break;
            default:
                //Do Nothing
                break;

        }

    }

    // Update state params values and login button visibility

    updateStateParams(updatedDataObject){
        this.setState(updatedDataObject, ()=>{
            this.enableDisableSubmitButton();
        });
    }

    // Enable / Disble functionality of Login Button

    enableDisableSubmitButton(){
        let status = (this.state.firmNameValid && this.state.firmFirstNameValid && this.state.firmLastNameValid && this.state.emailValid) ? true : false;
        this.setState({
            isFormValid : status
        });
    }

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    validations() {
        let passwordRegex = this.Constants.PASSWORD_REGEX;
       
        if(this.state.password !== this.state.cnfrmPassword) {
            this.setState({
                vcFirmError:this.Constants.REQUIRED_PASSWORD_AGAINPASSWORD_SAME
            })
            return true;
        } else if((!passwordRegex.test(this.state.password) || !passwordRegex.test(this.state.cnfrmPassword)) && (this.state.password.trim() !== '' && this.state.cnfrmPassword.trim() !== '')) {
            this.setState({
                vcFirmError:this.Constants.PASSWORD_RULE_MESSAGE
            })
            return true;
        }
        return false
    }

    createVcFirmBtn = () => {
        // let valid = this.validations();
        // if(!valid) {
            let postObj = {firmName:this.state.firmName, firstName:this.state.firmFirstName,middleName:this.state.firmMiddleName, lastName:this.state.firmLastName, email: this.state.email, subscriptonType: this.state.subscriptonType};
            this.open();
            this.Fsnethttp.createVcFirm(postObj).then(result=>{
                this.close();
                this.setState({
                    showToast: true,
                    toastMessage: 'Fund Manager has been created successfully.',
                    toastType: 'success',
                    fundList: this.state.fundList
                },()=>{
                    setTimeout(() => {
                        this.props.history.push('/adminDashboard/firmView')
                    }, 2000);
                })
            })
            .catch(error=>{
                this.close();
                if(error.response!==undefined && error.response.data !==undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        vcFirmError: error.response.data.errors[0].msg,
                    });
                } else {
                    this.setState({
                        vcFirmError: this.Constants.INTERNAL_SERVER_ERROR,
                    });
                }
            });
        // }
    }

    render(){
        return(
            <div className="width100" hidden={!this.state.showHtml}>
                <div className="main-heading"><span className="main-title">{this.state.jsonData.ADD_FIRM_TEXT}</span></div>
                    <div className="parentDiv" id="addFirmContainer" >
                        <label className="label-header">{this.state.jsonData.FIRM_DETAILS}</label>
                        <div className="subText">{this.state.jsonData.FIRM_DETAILS_MESSAGE}</div>
                        <Row className="marginBot20">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">{this.state.jsonData.FIRM_NAME_REQUIRED}</label>
                                <FormControl type="text" name="firmName" value={this.state.firmName} className={"inputFormControl " + (this.state.firmNameBorder ? 'inputError' : '')} maxLength="200" placeholder={this.state.jsonData.ADD_FIRM_PLACEHOLDER} onChange={(e) => this.handleInputChangeEvent(e,'firmname')} onBlur={(e) => this.handleInputChangeEvent(e,'firmname')}/>
                                <span className="error">{this.state.firmNameMsz}</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40"></Col>
                        </Row>
                        <label className="label-header">{this.state.jsonData.MAIN_CONTACT_DETAILS}</label>
                        <div className="subText">{this.state.jsonData.FIRM_CONTACT_DETAILS}</div>
                        <Row className="marginBot20">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">{this.state.jsonData.FIRST_NAME_REQUIRED}</label>
                                <FormControl type="text" name="firmFirstName" value={this.state.firmFirstName} className={"inputFormControl " + (this.state.firmFirstNameBorder ? 'inputError' : '')} maxLength="200" placeholder="Elisha" onChange={(e) => this.handleInputChangeEvent(e,'firmFirstName')} onBlur={(e) => this.handleInputChangeEvent(e,'firmFirstName')}/>
                                <span className="error">{this.state.firmFirstNameMsz}</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">{this.state.jsonData.LAST_NAME_REQUIRED}</label>
                                <FormControl type="text" name="firmLastName" value={this.state.firmLastName} className={"inputFormControl " + (this.state.firmLastNameBorder ? 'inputError' : '')} maxLength="200" placeholder="Benedict" onChange={(e) => this.handleInputChangeEvent(e,'firmLastName')} onBlur={(e) => this.handleInputChangeEvent(e,'firmLastName')}/>
                                <span className="error">{this.state.firmLastNameMsz}</span>
                            </Col>
                        </Row>
                        <Row className="marginBot20">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">{this.state.jsonData.MIDDLE_NAME_REQUIRED}</label>
                                <FormControl type="text" name="firmMiddleName" value={this.state.firmMiddleName} className="inputFormControl" maxLength="200" placeholder="Elisha" onChange={(e) => this.handleInputChangeEvent(e,'firmMiddleName')} onBlur={(e) => this.handleInputChangeEvent(e,'firmMiddleName')}/>
                            </Col>
                        </Row>
                        <Row className="marginBot20">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="label-text">{this.state.jsonData.EMAIL_ADDRESS_REQUIRED}</label>
                                <FormControl type="text" name="firmName" value={this.state.email} className={"inputFormControl " + (this.state.emailBorder ? 'inputError' : '')} maxLength="200" placeholder="EBenedict@gmail.com" onChange={(e) => this.handleInputChangeEvent(e,'email')} onBlur={(e) => this.handleInputChangeEvent(e,'email')}/>
                                <span className="error">{this.state.emailMsz}</span>
                            </Col>
                        </Row>
                        <div className="error marginLeft15">{this.state.vcFirmError}</div>
                    </div>
                    <div className="footerBorder"></div>
                    <div className="parentDiv margin20">
                        <Button className={"fsnetSubmitButton width200 "+ (this.state.isFormValid ? 'btnEnabled' : '') } disabled={!this.state.isFormValid} onClick={this.createVcFirmBtn}>Submit</Button>
                    </div>
                {/* </Row> */}
                <Loader isShow={this.state.showModal}></Loader>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>

            </div>
        );
    }
}

export default CreateVcFirmComponent;




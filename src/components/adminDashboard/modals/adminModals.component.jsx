import React, { Component } from 'react';
import { Button, Row, Col, Modal, FormControl } from 'react-bootstrap';
import '../adminDashboard.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import {Constants} from '../../../constants/constants';

var close, deleteFund, deleteUser,unlockUser,openfundDeactivateModal = {}
class AdminModalsComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            deleteFund:false,
            deleteUser:false,
            userId:null,
            accountType:null,
            reasonValid: true,
            
        }
        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
        deleteFund = PubSub.subscribe('deleteFund', (msg, data) => {
            this.deleteFundShow(data)
        });
        deleteUser = PubSub.subscribe('deleteUser', (msg, data) => {
            this.deleteUserShow(data)
        });
       unlockUser = PubSub.subscribe('unlockUser', (msg, data) => {
            this.openUnlockUserModal(data)
        });
        openfundDeactivateModal = PubSub.subscribe('openfundDeactivateModal', (msg, data) => {
            this.setState({
                fundId: data.fundId,
                fundName: data.fundName,
                fundStatus: data.fundStatus
            }, () => {
                this.handlefundDelShow();
            })
        });
    }

    componentWillUnmount() {
        // PubSub.unsubscribe(close);
        // PubSub.unsubscribe(deleteFund);
        // PubSub.unsubscribe(deleteUser);
        // PubSub.unsubscribe(unlockUser);
        // PubSub.unsubscribe(openfundDeactivateModal);
    }

    handlefundDelShow = () => {
        this.setState({ fundDeactivateModal: true });
    }

    handlefundDelClose = () => {
        this.setState({ reasonTouched: false, reasonValid: true, fundDeactivateModal: false });
    }

    closeToast = (timed) => {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }

    componentDidMount() {
        document.getElementById('leftNavBar').style.position = ''
    }

    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }

    deleteFundShow = (data) => {
        this.setState({deleteFund:true, fundId:data.fundId})
    }
    deleteFundClose = () =>{this.setState({deleteFund:false})}

    deleteUserShow = (data) => {
        this.setState({deleteUser:true, userId:data.id,accountType:data.accountType})
    }

    deleteUserClose = () => {this.setState({deleteUser:false})}


    deleteAFund = () => {
        this.open();
        const obj = {fundId:this.state.fundId}
        this.Fsnethttp.deleteFund(obj).then(result => {
            this.close();
            this.setState({
                showToast: true,
                toastMessage: 'Fund has been deleted succesfully.',
                toastType: 'success',
            })
            this.deleteFundClose();
            setTimeout(() => {
                window.location.reload();
            }, 2000);
        })
        .catch(error => {
            this.close();
        });
    }

    deleteUser = () => {
        this.open();
        const obj = {id:this.state.userId}
        this.Fsnethttp.deleteUser(obj).then(result => {
            this.close();
            this.setState({
                showToast: true,
                toastMessage: 'User has been deleted succesfully.',
                toastType: 'success',
            })
            this.deleteUserClose();
            setTimeout(() => {
                window.location.reload();
            }, 2000);
        })
        .catch(error => {
            this.close();
        });

    }

    unlockUser = () => {
        this.open();
        const postObj = {'isUnlockUser':true}
        this.Fsnethttp.sendNotification(this.state.unlockUserData.id,postObj).then(result => {
            this.close();
            if(result.data) {
                this.setState({
                    showToast: true,
                    toastMessage: 'User has been unlocked succesfully.',
                    toastType: 'success',
                })
                this.closeUnlockUserModal();
                setTimeout(() => {
                    window.location.reload();
                }, 2000);
            }
        })
        .catch(error => {
            this.close();
        });
    }

    openUnlockUserModal = (data) => {
        this.setState({unlockUserModal:true,unlockUserData:data})
    }

    closeUnlockUserModal = () => {
        this.setState({unlockUserModal:false})
    }

    handleInputEventChange = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name]: value,
            [name + 'Touched']: true
        })
    }

    deactivateFund = () => {
        if (this.state.fundStatus != 'New-Draft' && this.state.fundStatus !== 'Admin-Draft') {
            if (!this.state.reason) {
                this.setState({
                    reasonValid: false
                }, () => {
                    this.deactivateFundStatus();
                })
            } else {
                this.setState({
                    reasonValid: true
                }, () => {
                    this.deactivateFundStatus();
                })
            }
        } else {
            this.setState({
                reasonValid: true
            }, () => {
                this.deactivateFundStatus();
            })
        }
    }

    deactivateFundStatus = () => {

        let postObj = { fundId: this.state.fundId, deactivateReason: this.state.reason };
        if (this.state.reasonValid) {
            this.open()
            this.Fsnethttp.deactivateFund(postObj).then(result => {
                this.close();
                if (result) {
                    window.location.reload();
                }
            })
                .catch(error => {
                    this.close();
                });
        }
    }


    
    render() {
        return (
            <div className="width100">
                {/* Unlock User */}
                <Modal id="LPDelModal" backdrop="static" show={this.state.unlockUserModal} onHide={this.closeUnlockUserModal} dialogClassName="adminDeleteDialog delModal">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>Unlock User</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">Warning: This user has been locked out of their account due to repeated failed login attempts.  Are you sure you wish to unlock this account?</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeUnlockUserModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.unlockUser}>Yes - Unlock</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <Modal id="LPDelModal" backdrop="static" show={this.state.deleteFund} onHide={this.deleteFundClose} dialogClassName="adminDeleteDialog delModal">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>Delete Fund</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">Are you sure you want to delete this Fund? This will have the effect of permanently removing the Fund from the system, and all Fund Data will be deleted ?</div>
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.deleteFundClose}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={() => this.deleteAFund()}>Confirm Deletion</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                {/* delete user */}
                <Modal id="LPDelModal" backdrop="static" show={this.state.deleteUser} onHide={this.deleteUserClose} dialogClassName="adminDeleteDialog delModal">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>Delete User</Modal.Title>
                    <Modal.Body>
                        {
                            this.state.accountType == "SecondaryGP" || this.state.accountType == "SecondaryLP" ?
                            <div className="subtext modal-subtext">This option will permanently delete the secondary signatory from your profile.  If you wish to save the secondary signatory for future use, you may simply disable the secondary signatory instead of deleting the secondary signatory entirely.  Do you wish to proceed to delete the secondary signatory entirely?</div>
                            :
                            <div className="subtext modal-subtext">Warning! – Deleting this user’s account will delete the entire account from the Vanilla System and ALL Funds the user is a participant in. Are you sure you want to delete this user from the system and all their data? To delete the user from a select fund, you can do so through the fund profile.</div>
                        }
                        <div className="form-main-div">
                        </div>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.deleteUserClose}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={() => this.deleteUser()}>Confirm Deletion</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal id="GPDelModal" backdrop="static" className="GPDelModalDeactivate" show={this.state.fundDeactivateModal} onHide={this.handlefundDelClose} dialogClassName="GPDelModalDialog fundModalDialog ">
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Modal.Title>Deactivate {this.state.fundName}</Modal.Title>
                    <Modal.Body>
                        <div className="subtext modal-subtext">Are you sure you want to deactivate {this.state.fundName}?</div>
                        <div className="form-main-div">
                            {/* { this.state.fundStatus != 'Open' && this.state.fundStatus != 'Open-Ready'
                            ? */}
                            <form>
                                <div className="marginBot20">
                                    <label className="form-label">Reason*:</label>
                                    <FormControl componentClass="textarea" placeholder="Reason for Deactivating Fund" name="reason" value={this.state.reason} className={"inputFormControl textarea col-md-11 " + (!this.state.reasonValid ? 'inputError' : '')} onChange={(e) => this.handleInputEventChange(e)} onFocus={(e) => { this.setState({ reasonValid: true }) }} />
                                    {!this.state.reasonValid ? <span className="error height10">{this.Constants.REASON_REQUIRED}</span> : null}
                                </div>
                            </form>
                            {/* :
                                null
                            } */}

                        </div>
                        <Row className={(this.state.reasonValid ? "fundBtnRow":"marginTopN10")}>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton" onClick={this.handlefundDelClose}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetCancelButton btnEnabled" onClick={this.deactivateFund}>Deactivate</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>                        
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default AdminModalsComponent;


import React, { Component } from 'react';
import './adminDashboard.component.css';
import Loader from '../../widgets/loader/loader.component';
import { Constants } from '../../constants/constants';
import { Row, Col } from 'react-bootstrap';
import { Fsnethttp } from '../../services/fsnethttp';
import { FsnetAuth } from '../../services/fsnetauth';
import HeaderComponent from '../header/header.component';
import { Route, Link,Switch,Redirect } from "react-router-dom";
import { FsnetUtil } from '../../util/util';
import vanillaLogo from '../../images/Vanilla-white.png';
import vanillaDarkLogo from '../../images/Vanilla.png';
import firmViewComponent from '../adminDashboard/firmView/firmView.component';
import fundManagersComponent from '../adminDashboard/userManagement/fundManagers/fundManagers.component';
import investorsComponent from '../adminDashboard/userManagement/investors/investors.component';
// import activityLogComponent from '../adminDashboard/activityLog/activityLog.component';
import eventsLogComponent from '../adminDashboard/eventsLog/eventsLog.component';
import CreateVcFirmComponent from '../adminDashboard/admin/createvcfirm.component';
import { reactLocalStorage } from 'reactjs-localstorage';

class adminDashboardComponent extends Component {

    constructor(props) {
        super(props);
        this.FsnetAuth = new FsnetAuth();
        this.Constants = new Constants();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.state = {
            showModal: false,
            show: false,
            showSideNav: true,
            currentPage:'firmView',
            userData:{}
        }
        
           
    }

    //Unsuscribe the pubsub
    componentWillUnmount() {
    }

    componentDidMount() {
        //Check user is valid or not
        //If not redirect to login page
        if (!this.FsnetAuth.isAdmin()) {
            this.Fsnethttp.logout().then(result => {
                this.FsnetUtil._ClearLocalStorage();
            });
        } else {
            window.scrollTo(0, 0);
            // this.FsnetUtil.setLeftNavHeight();
            this.setState({
                currentPage: this.FsnetUtil.getAdminUrlName(),
                userData:JSON.parse(reactLocalStorage.get('userData'))
            })
        }
    }

    updatePageNumber = (name) => {
        this.setState({
            currentPage: name
        })
    }

    

    // ProgressLoader : close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loade
    open = () =>{
        this.setState({ showModal: true });
    }

    //Header hamburger click
    hamburgerClick = () => {
        if (this.state.showSideNav == true) {
            this.setState({
                showSideNav: false
            })
        } else {
            this.setState({
                showSideNav: true
            })
        }
    }

    

    render() {
        const { match } = this.props;
        return (
            <div>
                <nav className="navbar navbar-custom" hidden={this.state.isExpanded}>
                    <div className="navbar-header">
                        <div className="sidenav">
                            <h1 className="text-left"><i className="fa fa-bars" aria-hidden="true" onClick={(e) => this.hamburgerClick()}></i>&nbsp; <img src={vanillaLogo} alt="vanilla" className="vanilla-logo"/></h1>
                        </div>
                    </div>
                    <div className="text-center navbar-collapse-custom" id="navbar-collapse" hidden={!this.state.showSideNav}>
                        <div className="sidenav" id="leftNavBar">
                            <h1 className="text-left logoHamburger"><i className="fa fa-bars" aria-hidden="true"></i><img src={vanillaLogo} alt="vanilla" className="vanilla-logo"/></h1>
                            <ul className="sidenav-menu marginTop30">
                                <li>
                                    <Link to={"/adminDashboard/firmView"} className={(this.state.currentPage === 'firmView' ? 'active' : '')} onClick={(e) =>this.updatePageNumber('firmView')}><span>Firm/Fund View</span></Link>
                                </li>
                                <li>
                                    <Link to="/adminDashboard/managers" className={(this.state.currentPage === 'managers' || this.state.currentPage === 'investors' ? 'active' : '')} onClick={(e) =>this.updatePageNumber('managers')}><span>User Management</span></Link>
                                        <Link to={"/adminDashboard/managers"} className={"marginLeft20 "+ (this.state.currentPage === 'managers' ? 'active' : '')} onClick={(e) =>this.updatePageNumber('managers')}><span>Fund Managers and Delegates</span></Link>
                                        <Link to={"/adminDashboard/investors"} className={"marginLeft20 "+ (this.state.currentPage === 'investors' ? 'active' : '')} onClick={(e) =>this.updatePageNumber('investors')}><span>Investors and Delegates</span></Link>
                                </li>
                                {/* <li>
                                    <Link to={"/adminDashboard/activityLog"} className={(this.state.currentPage === 'activityLog' ? 'active' : '')} onClick={(e) =>this.updatePageNumber('activityLog')}><span>Help Desk Activity Log</span></Link>
                                </li> */}
                                <li>
                                    <Link to={"/adminDashboard/eventsLog"} className={(this.state.currentPage === 'eventsLog' ? 'active' : '')} onClick={(e) =>this.updatePageNumber('eventsLog')}><span>Events Log</span></Link>
                                </li>
                                
                            </ul>
                        </div>

                    </div>
                </nav>

                <div className={"main "+(this.state.isExpanded ? 'marginHorizontal30 expanded' : '')}>
                    <div className={"headerAlign "+(this.state.isExpanded ? 'marginTop20' : '')}>
                        <div className="pull-left" hidden={!this.state.isExpanded}>
                            <img src={vanillaDarkLogo} alt="vanilla" className="vanilla-logo marginLeft30"/>
                        </div>
                        <HeaderComponent></HeaderComponent>
                    </div>
                    <div className="contentWidth">
                        {/* Actual Content */}
                        <Row className="main-content">
                            <Switch>
                                <Route exact path={`${match.url}/firmView`} component={firmViewComponent} />                     
                                <Route exact path={`${match.url}/managers`} component={fundManagersComponent} />                     
                                <Route exact path={`${match.url}/investors`} component={investorsComponent} />                     
                                {/* <Route exact path={`${match.url}/activityLog`} component={activityLogComponent} />                      */}
                                <Route exact path={`${match.url}/eventsLog`} component={eventsLogComponent} />                     
                                <Route exact path={`${match.url}/admin`} component={CreateVcFirmComponent} />                     
                                <Redirect from={`${match.url}/*`} to='/404'/>                
                                <Redirect from={`${match.url}/`} to='/404'/>                
                            </Switch>
                        </Row>
                    </div>
                </div>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default adminDashboardComponent;


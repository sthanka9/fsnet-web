import React, { Component } from 'react';
import '../../adminDashboard.component.css';
import { Button, FormControl, Row, Col, Radio } from 'react-bootstrap';
import Loader from '../../../../widgets/loader/loader.component';
import { Fsnethttp } from '../../../../services/fsnethttp';
import { FsnetAuth } from '../../../../services/fsnetauth';
import { FsnetUtil } from '../../../../util/util';
import ToastComponent from '../../../toast/toast.component';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/rrui.css';
import 'react-phone-number-input/style.css';
import { PubSub } from 'pubsub-js';
import {Constants} from '../../../../constants/constants';

var close = {}
class userComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            showAddUserScreen:false,
            fundsData:[],
            userType:null,
            lpList:[],
            fundId:null,
            lpId:null,
            firstName:null,
            lastName:null,
            middleName:null,
            email:null,
            username:'',
            cellNumber:null,
            password:null,
            cnfrmPassword:null,
            lpIdValid: true,
            userErrorMsz:null
        }
        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast = (timed) => {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }

    componentDidMount() {
        if (this.FsnetAuth.isAdmin()) {
            document.getElementById('leftNavBar').style.position = '';
            this.FsnetUtil.setLeftNavHeight();
            this.setState({ 
                userType: this.props.userType,
                fundsData: this.props.fundsList
            });
        }
    }

    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }

    showMainScreen = () => {
        this.props.onCancel(false);  
    }

    getFundLPList = (id) => {
        this.open();
        this.Fsnethttp.getFundLPList(id).then(result => {
            this.close();
            this.setState({
                lpList: result.data,
                lpIdValid:false
            })
        })
        .catch(error => {
            this.close();
        });
    }

    inputHandleChange = (e,type,msz) => {
        let value;
        let dataObj = {};
        if(type === 'fundId' ||  type === 'lpId') {
            let selectedId = e.target.value;
            if((this.state.userType === 'LPDelegate' || this.state.userType === 'SecondaryLP' ) && type === 'fundId') {
                if(selectedId > 0) {
                    this.getFundLPList(selectedId)
                } else {
                    this.setState({lpList:[],lpIdValid:false})
                }
            }
            this.setState({
                [type] : selectedId
            },()=>{
                if(selectedId == 0) {
                    this.setState({
                        [type+'Border']: true,
                        [type+'Msz']: this.Constants[msz],
                        [type+'Valid']: false,
                    })
                } else {
                    this.setState({
                        [type+'Border']: false,
                        [type+'Msz']: '',
                        [type+'Valid']: true,
                    })
                }
                dataObj ={
                    [type+'Valid'] : selectedId > 0 ? true : false
                };
                this.updateStateParams(dataObj);
            })
        } else {
            if(type === 'cellNumber') {
                value = e;
            } else {
                if(e.target.value.trim() === '' || e.target.value.trim() === undefined) {
                    value = e.target.value.trim();
                } else {
                    value = e.target.value;
                }
            }
            if(value === '' || value === undefined) {
                this.setState({
                    [type]: '',
                    [type+'Border']: true,
                    [type+'Msz']: this.Constants[msz],
                    [type+'Valid']: false,
                })
                dataObj ={
                    [type+'Valid'] :false
                };
                this.updateStateParams(dataObj);
            } else {
                this.setState({
                    [type]: value,
                    [type+'Border']: false,
                    [type+'Msz']: '',
                    [type+'Valid']: true,
                })
                dataObj ={
                    [type+'Valid'] :true
                };
                this.updateStateParams(dataObj);
            }
        } 
        
    }

    updateStateParams = (updatedDataObject) => {
        this.setState(updatedDataObject, ()=>{
            this.enableDisableSubmitButton();
        });
    }

    // Enable / Disble functionality of Save changes Button

    enableDisableSubmitButton = () => {
        let status = (this.state.firstNameValid && this.state.lastNameValid && this.state.emailValid && this.state.fundIdValid && this.state.lpIdValid) ? true : false;
        this.setState({
            isFormValid : status,
            userErrorMsz:''
        });
    }

    saveUser = () => {
        let valid = this.checkValidations();
        if(!valid) {
            this.open();
            let postObj = {firstName:this.state.firstName, lastName:this.state.lastName,middleName:this.state.middleName, email:this.state.email, cellNumber:this.state.cellNumber, fundId:parseInt(this.state.fundId)};
            let url = '';
            if(this.state.userType === 'LPDelegate') {
                postObj['lpId'] = parseInt(this.state.lpId);
                url = 'admin/add/LpDelegate';
            } else if(this.state.userType === 'LP') {
                url = 'admin/add/Lp';
            } else if(this.state.userType === 'GPDelegate') {
                url = 'admin/add/GpDelegate';
            } else if(this.state.userType === 'SecondaryGP') {
                url = 'admin/add/GpSignatory ';
            } else if(this.state.userType === 'SecondaryLP') {
                postObj['lpId'] = parseInt(this.state.lpId);
                url = 'admin/add/LpSignatory  ';
            }
            this.Fsnethttp.addUser(postObj,url).then(result=>{
                this.close();
                this.setState({
                    showToast: true,
                    toastMessage: this.state.userType === 'GPDelegate' ? 'Delegate has been successfully added.' : this.state.userType === 'LP' ? 'Investor has been successfully added.': this.state.userType === 'SecondaryGP' ? 'Secondary Signatory has been successfully added.': this.state.userType === 'SecondaryLP' ? 'Secondary Signatory has been successfully added.' : 'Investor Delegate has been successfully added.',
                    toastType: 'success',
                })
                setTimeout(() => {
                    this.showMainScreen()
                }, 2000);
            })
            .catch(error=>{
                this.close();
                if (error.response !== undefined && error.response.data !== undefined && error.response.data.errors !== undefined) {
                    this.setState({
                        userErrorMsz: error.response.data.errors[0].msg,
                    });
                }
            });
        }
    }

    checkValidations = () => {
        let error = false;
        if(this.state.cellNumber) {
            if(this.state.cellNumber.length < 12 || this.state.cellNumber.length > 13 ) {
                this.setState({
                    userErrorMsz: this.Constants.CELL_NUMBER_VALID
                })
                error = true;
            }
        } 
        if(this.state.email !== '' && this.state.email !== null && this.state.email !== undefined) {
            let emailRegex = this.Constants.EMAIL_REGEX
            if (!emailRegex.test(this.state.email)) {
                this.setState({
                    userErrorMsz: this.Constants.VALID_EMAIL
                })
                error = true;
            }
        }
        if (error) {
            return true
        } else {
            return false
        }
    }

    render() {
        return (
            <div className="width100 marginLeft30 marginTop20">
                <div className="">
                    {
                        this.state.userType === 'LP' ?
                        <h1 className="title">Add Investor</h1>
                        :
                        this.state.userType === 'SecondaryLP' ?
                            <h1 className="title">Add Investor Secondary Signatory</h1>
                        :
                        this.state.userType === 'SecondaryGP' ?
                            <h1 className="title">Add Fund Manager Secondary Signatory</h1>
                        :
                            <h1 className="title">Add Delegate</h1>
                    }
                    <form>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Select Fund</label>
                                <FormControl name='fund' defaultValue={0}  className={"selectFormControl " + (this.state.fundIdBorder ? 'inputError' : '')} componentClass="select" onChange={ (e) => this.inputHandleChange(e,'fundId','FUND_REQUIRED') } onBlur={ (e) => this.inputHandleChange(e,'fundId','FUND_REQUIRED') }>
                                    <option value={0}>Select Fund</option>
                                    {this.state.fundsData.map((record, index) => {
                                        return (
                                            <option value={record['id']} key={index} >{record['legalEntity']}</option>
                                        );
                                    })}
                                </FormControl>
                                <span className="error">{this.state.fundIdMsz}</span>
                            </Col>
                            {
                                this.state.userType === 'LPDelegate' || this.state.userType === 'SecondaryLP'?
                                    <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                        <label className="input-label">Select Investor</label>
                                        <FormControl name='lp' defaultValue={0} className={"selectFormControl " + (this.state.lpIdBorder ? 'inputError' : '')} componentClass="select" onChange={ (e) => this.inputHandleChange(e,'lpId','INVESTOR_REQUIRED') } onBlur={ (e) => this.inputHandleChange(e,'lpId','INVESTOR_REQUIRED') }>
                                            <option value={0}>Select Investor</option>
                                            {this.state.lpList.map((record, index) => {
                                                return (
                                                    <option value={record['lpId']} key={index} >{record['title']}</option>
                                                );
                                            })}
                                        </FormControl>
                                        <span className="error">{this.state.lpIdMsz}</span>
                                    </Col>
                                :''
                            }
                        </Row>
                        
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">First Name</label>
                                <FormControl type="text" name="firstName" placeholder="Charles" value={this.state.firstName} maxLength="200" className={"inputFormControl " + (this.state.firstNameBorder ? 'inputError' : '')} onChange={ (e) => this.inputHandleChange(e, 'firstName')} onBlur={ (e) => this.inputHandleChange(e, 'firstName','FIRST_NAME_REQUIRED')}/>
                                <span className="error">{this.state.firstNameMsz}</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Last Name</label>
                                <FormControl type="text" name="lastName" placeholder="Xavier" maxLength="200" value={this.state.lastName} className={"inputFormControl " + (this.state.lastNameBorder ? 'inputError' : '')} onChange={ (e) => this.inputHandleChange(e, 'lastName')} onBlur={ (e) => this.inputHandleChange(e, 'lastName','LAST_NAME_REQUIRED')}/>
                                <span className="error">{this.state.lastNameMsz}</span>
                            </Col>
                        </Row>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Middle Name or Initial</label>
                                <FormControl type="text" name="middleName" placeholder="Elisha" value={this.state.middleName} maxLength="200" className="inputFormControl" onChange={ (e) => this.inputHandleChange(e, 'middleName')} onBlur={ (e) => this.inputHandleChange(e, 'middleName')}/>
                            </Col>
                        </Row>
                        <Row className="marginBot24">
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Email</label>
                                <FormControl type="text" name="email" placeholder="ProfessorX@gmail.com" value={this.state.email} className={"inputFormControl " + (this.state.emailBorder ? 'inputError' : '')} onChange={ (e) => this.inputHandleChange(e, 'email')} onBlur={ (e) => this.inputHandleChange(e, 'email','VALID_EMAIL')}/>
                                <span className="error">{this.state.emailMsz}</span>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="width40">
                                <label className="input-label">Phone Number (Cell)</label>
                                <PhoneInput maxLength="14" placeholder="(123) 456-7890"  country="US" value={this.state.cellNumber}  onChange={ phone => this.inputHandleChange(phone, 'cellNumber','CELL_NUMBER_VALID')}/>
                            </Col>
                        </Row>
                    </form>
                    <div className="error marginTop20">{this.state.userErrorMsz}</div>
                </div>
                <div className="footer-profile">
                    <Button type="button" className="fsnetCancelButton marginRight20" onClick={this.showMainScreen}>Cancel</Button>
                    <Button type="button" className={"fsnetSubmitButton width200 "+ (this.state.isFormValid ? 'btnEnabled' : 'disabled') } onClick={this.saveUser} disabled={!this.state.isFormValid}>Save Changes</Button>
                </div>

                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default userComponent;


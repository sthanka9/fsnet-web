import React, { Component } from 'react';
import '../../adminDashboard.component.css';
import { Button, FormControl, Row, Col, Radio } from 'react-bootstrap';
import Loader from '../../../../widgets/loader/loader.component';
import { Fsnethttp } from '../../../../services/fsnethttp';
import { FsnetAuth } from '../../../../services/fsnetauth';
import { FsnetUtil } from '../../../../util/util';
import ToastComponent from '../../../toast/toast.component';
import userDefaultImage from '../../../../images/default_user.png';
import AdminModalsComponent from '../../modals/adminModals.component';
import UserComponent from '../user/user.component';
import { PubSub } from 'pubsub-js';
import {Constants} from '../../../../constants/constants';
import key from '../../../../images/key.svg';
import edit from '../../../../images/edit.svg';
import Pagination from "react-js-pagination";
import { reactLocalStorage } from 'reactjs-localstorage';

var close = {}
class adminsComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            usersList:[],
            showNameAsc:false,
            itemsCountPerPage:10,
            totalItemsCount:0,
            totalFundItemsCount:0,
            pageRangeDisplayed:5,
            activePage:1,
            noUsersMsz:null,
            fundsList:[],
            searchUser:null,
            filterFundId:0,
            showAddUserScreen:false,
            userType: 'Admin'
        }
        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast(timed) {
        if(timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })  
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })  
        }
    }

    componentDidMount() {
        if (this.FsnetAuth.isSuperAdmin()) {
            const postObj = {'page':1}
            this.getListOfUsers(postObj,true)
        } else {
            this.props.history.push(`/adminDashboard/firmView`);
        }
    }

    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }

    getListOfUsers = (postObj,loader) => {
        if(loader) {
            this.open();
        }
        this.Fsnethttp.getAdminList(postObj).then(result => {
            this.close();
            this.setState({
                usersList:result.data.users ? result.data.users.rows :[],
                totalItemsCount:result.data.users.count,
                noUsersMsz: postObj['search'] ? this.Constants.NO_ADMIN_SEARCH_USERS: this.Constants.NO_ADMIN,
                searchUser: postObj['search'] ? this.state.searchUser : null,
                filterFundId: postObj['fundId'] ? this.state.filterFundId :0
            },()=>{
                this.FsnetUtil.handleTableHeight(this.state.usersList.length)
                this.FsnetUtil.setLeftNavHeight();
                document.getElementById('leftNavBar').style.position = '';
            })
        })
        .catch(error => {
            this.close();
            this.setState({
                usersList:[]
            })
        });
    }

    searchInputChangeEvent = (e) => {
        let value = e.target.value.trim() != '' ? e.target.value : null;
        let postObj = {};
        if(value) {
            postObj = {'search':value}
        } else {
            postObj = {'page':1}
        }
        this.setState({activePage:1,noUsersMsz:this.Constants.NO_GP_SEARCH_USERS,searchUser:value},()=>{
            this.getListOfUsers(postObj,false)
        })
    }

    sortField = (sortType) => {
        const postObj = {'orderBy':'firstName','order':sortType}
        this.setState({showNameAsc:!this.state.showNameAsc,activePage:1},()=>{
            this.getListOfUsers(postObj,true)
        })
    }

    handlePageChange = (pageNumber) => {
        this.setState({activePage: pageNumber},()=>{
            let queryObj = {'page':this.state.activePage}
            if(this.state.showNameAsc) {
                queryObj['orderBy'] = 'firstName';
                queryObj['order'] = 'desc';
            }
            this.getListOfUsers(queryObj,true)
        });
    }

    sendNotification = (data,value) => {
        const obj = {'isEmailNotification':value}
        this.open();
        this.Fsnethttp.sendNotification(data.id,obj).then(result => {
            this.close();
            let userList = this.state.usersList;
            for(let index of userList) {
                if(index.id === data.id) {
                    index['isEmailNotification'] = value;
                    
                }
            }
            this.setState({
                usersList: userList
            })
        })
        .catch(error => {
            this.close();
        });
    }

    deleteUser = (data) => {
        const obj = {id:data.id,accountType:data.accountType}
        PubSub.publish('deleteUser', obj);
    }

    redirectToProfile = (data) => {
        window.location.href = `/settings/profile/${this.FsnetUtil._encrypt(data.id)}`
    }

    showAddUser = (role) => {
        this.setState({ 
            userType:role
        },()=>{
            this.setState({
                showAddUserScreen: true 
            })
        })
    }

    showMainScreen = (value) => {
        this.setState({showAddUserScreen:value})
        const postObj = {'page':1};
        this.getListOfUsers(postObj,true);
    }
    
    unlockUser = (data) => () => {
        const obj = {id:data.id}
        PubSub.publish('unlockUser', obj);
    }

    render() {
        return (
            <div className="width100 adminContainer">
                <div className="main-heading"><span className="main-title">User Management</span></div>
                {
                    !this.state.showAddUserScreen ?

                        <div className="userContainer">
                            <div className="margin30">
                                <label className="title">Administrators</label>
                            </div>
                            <div className="marginLeft30">
                                <Button type="button" className="fsnetSubmitButton btnEnabled adminButton marginRight20" onClick={()=>this.showAddUser('Admin')}>Create Administrator</Button>
                            </div>
                            <div className="marginTop20 marginLeft15">
                                <span className="fund-search"><i className="fa fa-search" aria-hidden="true"></i></span>
                                <FormControl type="text" placeholder="Search" value={this.state.searchUser || ''} className="formFilterControl" onChange={(e) => this.searchInputChangeEvent(e)}/>
                            </div>
                            <div className="full-width marginTop30 marginLeft30">
                                <div className="table-heading width300 paddingLeft40 cursor" hidden={!this.state.showNameAsc} onClick={() => this.sortField('asc')}>
                                    Name
                                    <i className="fa fa-sort-asc" aria-hidden="true"  ></i>
                                </div>
                                <div className="table-heading width300 paddingLeft40 cursor" hidden={this.state.showNameAsc} onClick={() => this.sortField('desc')}>
                                    Name
                                    <i className="fa fa-sort-desc" aria-hidden="true"  ></i>
                                </div>
                                <div className="table-heading width250 paddingLeft30">
                                    Email Address
                                </div>
                                {/* <div className="table-heading width100Px">
                                    Username
                                </div> */}
                               {/* <div className="table-heading width100Px">
                                    Impersonate
                                </div>*/}
                                <div className="table-heading width100Px">
                                    Edit Profile
                                </div>
                                <div className="table-heading width200">
                                    Receive Notification Emails?
                                </div>
                                <div className="table-heading width50">
                                    Delete
                                </div>
                                <div className="table-heading width100Px paddingLeft10">
                                    Unlock User
                                </div>
                            </div>
                            <div className="tableContainer marginLeft30 marginTop10">
                                    {this.state.usersList.length > 0 ?
                                        this.state.usersList.map((record, index) => {
                                            return (

                                                <div className="tableRow" key={index}>
                                                    <div className="userText width300  text-ellipsis paddingRight10" title={this.FsnetUtil.getFullName(record)}>
                                                        <label className="userImageAlt">
                                                            {
                                                                record['profilePic'] ?
                                                                    <img src={record['profilePic']['url']} alt="img" className="user-image" />
                                                                    : <img src={userDefaultImage} alt="img" className="user-image" />
                                                            }
                                                        </label>
                                                        <span className="paddingLeft5">{this.FsnetUtil.getFullName(record)}</span>
                                                        <span className="user-role">{record['accountTypeMask']}</span>
                                                    </div>
                                                    <div className="userText width250 text-ellipsis paddingRight10" title={record['email']}>{record['email']}</div>
                                                    <div className="userText width100Px paddingLeft20"><img src={edit} alt="edit" onClick={()=>this.redirectToProfile(record)}/></div>
                                                    <div className="userText width200 paddingLeft20">
                                                        <Radio name={record['id']} key={`${record['id']}_yes`} checked={record['isEmailNotification']} className="marginLeft15" inline onChange={()=>this.sendNotification(record,true)}>
                                                            Yes
                                                            <span className="radio-checkmark"></span>
                                                        </Radio>
                                                        <Radio name={record['id']} key={`${record['id']}_no`} checked={record['isEmailNotification'] === false || record['isEmailNotification'] === null} className="marginLeft15" inline onChange={()=>this.sendNotification(record,false)}>
                                                            No
                                                            <span className="radio-checkmark"></span>
                                                        </Radio>
                                                    </div>
                                                    <div className="userText width50 paddingLeft10"><i className={"trash-icon fa fa-trash " + (record['accountType'] !== 'GP' ? 'cursor' : 'disabled')} hidden={record['accountType'] === 'GP'} aria-hidden="true" onClick={()=>this.deleteUser(record)}></i></div>
                                                    <div className="userText width100Px redLink paddingLeft10" hidden={record.loginFailAttempts < 5} onClick={this.unlockUser(record)}>Unlock User</div>
                                                </div>
                                            );
                                        })
                                        :
                                        <div className="title margin20">{this.state.noUsersMsz}</div>
                                    }
                                
                            </div>
                            {
                                this.state.totalItemsCount > this.state.itemsCountPerPage ?
                                <div className="marginLeft30">
                                    <Pagination
                                        activePage={this.state.activePage}
                                        itemsCountPerPage={this.state.itemsCountPerPage}
                                        totalItemsCount={this.state.totalItemsCount}
                                        pageRangeDisplayed={this.state.pageRangeDisplayed}
                                        onChange={this.handlePageChange}
                                    />
                                </div>:''
                            }
                    </div>
                    :
                    <UserComponent onCancel={this.showMainScreen} fundsList={this.state.fundsList} userType={this.state.userType}></UserComponent>
                }   
                
                <AdminModalsComponent></AdminModalsComponent>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default adminsComponent;


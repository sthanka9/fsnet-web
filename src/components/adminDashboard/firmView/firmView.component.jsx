import React, { Component } from 'react';
import '../adminDashboard.component.css';
import Loader from '../../../widgets/loader/loader.component';
import { Button, FormControl, Row, Col, Modal } from 'react-bootstrap';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';
import AdminModalsComponent from '../modals/adminModals.component'
import { PubSub } from 'pubsub-js';
import { Constants } from '../../../constants/constants';
import userDefaultImage from '../../../images/default_user.png';
import Pagination from "react-js-pagination";
import { reactLocalStorage } from 'reactjs-localstorage';
import edit from '../../../images/edit.svg';
import plusImg from '../../../images/plus.svg';

var close = {}
class firmViewComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            firmList: [],
            fundList:[],
            showFirmNameAsc: false,
            showFirstNameAsc: false,
            showFundNameAsc:false,
            vcfirmId:null,
            activeFirmPage: 1,
            activeFundPage: 1,
            deleteFund:false,
            fundId:null,
            noFundsMsz: '',
            noFirmsMsz:'',
            jsonData: {},
            itemsCountPerPage:10,
            totalFirmItemsCount:0,
            totalFundItemsCount:0,
            pageRangeDisplayed:5,
            searchFundValue:null
        }

        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast = (timed) => {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    componentDidMount() {
        if (this.FsnetAuth.isAdmin()) {
            this.getJsonData();
            this.FsnetUtil.setLeftNavHeight();
            const queryObj = {'limit':1}
            this.getListOfFirms(queryObj);
            document.getElementById('leftNavBar').style.position = ''
        }
    }

    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }

    //lpNameProfile Modal
    lpNameProfile = (data) => () => {
        // PubSub.publish('profileModal', data);
    }

    getJsonData = () =>{
        this.Fsnethttp.getJson('admindashboard').then(result => {
            this.setState({
                jsonData: result.data
            })
        });
    }

    getListOfFirms = (obj) => {
        this.open();
        this.Fsnethttp.getFirms(obj).then(result => {
            this.close();
            this.setState({ firmList: result.data.data?result.data.data:[],noFirmsMsz:this.Constants.NO_FIRMS,noFundsMsz:this.Constants.NO_FUNDS, totalFirmItemsCount:result.data.itemCount }, () => {
                
                if(result.data.data && result.data.data.length > 0) {
                    const queryObj = {'limit':1}
                    this.getListOfFunds(result.data.data[0]['vcfirmId'],queryObj,true)
                }
            })
        })
        .catch(error => {
            this.close();
            this.setState({noFirmsMsz:this.Constants.NO_FIRMS,noFundsMsz:this.Constants.NO_FUNDS})
        });
    }

    getListOfFunds = (vcfirmId,obj,loader) => {
        reactLocalStorage.set('firmId', vcfirmId);
        this.setState({vcfirmId:vcfirmId})
        if(loader) {
            this.open();
        }
        this.Fsnethttp.getFunds(vcfirmId,obj).then(result => {
            this.close();
            this.setState({
                fundList:result.data.data ? result.data.data :[], searchFundValue: obj['search'] ? this.state.searchFundValue:null, noFundsMsz:obj['search'] ? this.Constants.NO_SEARCH_FUNDS : this.Constants.NO_FUNDS
            })

        })
        .catch(error => {
            this.close();
            this.setState({noFundsMsz:this.Constants.NO_FUNDS})
        });
    }

    proceedToNext = (data,type) => {
        if(type) {
            window.location.href = `/fund/view/${data.id}`;
        } else {
            reactLocalStorage.set('firmId', data.vcfirmId);
            window.location.href = `/fundsetup/funddetails`;
        }
    }

    handleFirmPageChange = (pageNumber) => {
        this.setState({activeFirmPage: pageNumber},()=>{
            let queryObj = {'limit':this.state.activeFirmPage}
            if(this.state.showFirmNameAsc) {
                queryObj['orderBy'] = 'firmName';
                queryObj['order'] = 'desc';
            }
            this.getListOfFirms(queryObj)
        });
    }

    handleFundPageChange = (pageNumber) => {
        this.setState({activeFundPage: pageNumber},()=>{
            let queryObj = {'limit':this.state.activeFundPage}
            if(this.state.showFundNameAsc) {
                queryObj['orderBy'] = 'legalEntity';
                queryObj['order'] = 'desc';
            }
            this.getListOfFunds(this.state.vcfirmId,queryObj,true)
        });
    }

    searchInputChangeEvent = (e) =>{
        let value = e.target.value.trim() != '' ? e.target.value : null;
        let queryObj = {};
        this.setState({searchFundValue:value})
        if(value) {
            queryObj = {'search':value}
        } else {
            queryObj = {'limit':1}
        }
        this.getListOfFunds(this.state.vcfirmId,queryObj,false)
        
    }

    sortField = (type,sort) => {
        const queryObj = {'orderBy':type,'order':sort}
        this.setState({activeFirmPage:1,showFirmNameAsc: !this.state.showFirmNameAsc},()=>{
            this.getListOfFirms(queryObj)
        })
    }

    sortFundNameField = (type,sort) => {
        const queryObj = {'orderBy':type,'order':sort}
        this.setState({activeFundPage:1,showFundNameAsc:!this.state.showFundNameAsc},()=>{
            this.getListOfFunds(this.state.vcfirmId,queryObj,true)
        })
    }
    
    sendEmailInviteToGp = (data) => {
        this.open();
        this.Fsnethttp.sendInviteToGp({vcfirmId:data.vcfirmId}).then(result => {
            this.close();
            this.setState({
                showToast: true,
                toastMessage: 'Fund details have been sent to the Fund Manager successfully.',
                toastType: 'success',
            })
            this.getListOfFirms('firmName', 'asc');
        })
        .catch(error => {
            this.close();
        });
    }

    deleteFundShow = (data) => {
        const obj = {fundId:data.id}
        PubSub.publish('deleteFund', obj);
    }

    adminSendEmail = (data) => {
        let fundObj = { fundId: data.id }
        this.Fsnethttp.triggerAccountAndFundDetailsEmail(fundObj).then(result => {
            this.close();
            for(let index of this.state.fundList) {
                if(index['id'] === data.id) {
                    index['enableEmailTriggerButton'] = false
                }
            }
            this.setState({
                showToast: true,
                toastMessage: 'Fund details have been sent to the Fund Manager successfully.',
                toastType: 'success',
                fundList: this.state.fundList
            })
        })
        .catch(error => {
            this.close();
        });
    }

    impersonateUser = (data) => {
        this.open();
        const postObj = {userId:data.gpId}
        this.Fsnethttp.impersonateUser(postObj).then(result => {
            this.close();
            if(result.data) {
                this.FsnetUtil._impersonateSetData(result.data)
                setTimeout(() => {
                    window.location.href = `/fund/view/${this.FsnetUtil._encrypt(data.id)}`;
                }, 200);
            }
        })
        .catch(error => {
            this.close();
        });
        
    }

    confirmDeactivate = (data) => () => {
        // PubSub.publish('openfundDeactivateModal', {fundId: data.id, fundName: data.legalEntity, fundStatus: data.status})
    }

    render() {
        return (
            <div className="width100 adminContainer">
                <div className="main-heading"><span className="main-title">{this.state.jsonData.FIRM_TITLE}</span></div>
                <div className="firmContainer">
                    <div className="margin30">
                        <label className="title width450">Firms</label>
                        <Button type="button" className="fsnetSubmitButton btnEnabled firmButton" onClick={()=>this.props.history.push('/adminDashboard/admin')}>Create New Firm/Fund Manager</Button>
                    </div>

                    <div className="full-width marginTop20 marginLeft30">
                        <div className="table-heading width250 paddingLeft30 cursor" hidden={!this.state.showFirmNameAsc} onClick={() => this.sortField('firmName', 'asc')}>
                            Firm Name
                            <i className="fa fa-sort-asc" aria-hidden="true"  ></i>
                        </div>
                        <div className="table-heading width250 paddingLeft30 cursor" hidden={this.state.showFirmNameAsc} onClick={() => this.sortField('firmName', 'desc')}>
                            Firm Name
                            <i className="fa fa-sort-desc" aria-hidden="true"  ></i>
                        </div>

                        {/* <div className="table-heading width250 paddingLeft30 cursor" hidden={!this.state.showFirstNameAsc} onClick={() => this.sortField('firstName', 'asc')}>
                            Fund Manager Name
                            <i className="fa fa-sort-asc" aria-hidden="true"  ></i>
                        </div>
                        <div className="table-heading width250 paddingLeft30 cursor" hidden={this.state.showFirstNameAsc} onClick={() => this.sortField('firstName', 'desc')}>
                            Fund Manager Name
                            <i className="fa fa-sort-desc" aria-hidden="true"  ></i>
                        </div> */}
                        <div className="table-heading width250 paddingLeft30 ">
                            Fund Manager Name
                        </div>
                        <div className="table-heading  width100Px">
                            Create Fund
                        </div>
                        <div className="table-heading width100Px text-center marginLeft20">
                            Notifications
                        </div>
                    </div>

                    <div className="tableContainer marginLeft30 marginTop10">
                        {this.state.firmList.length > 0 ?
                            this.state.firmList.map((record, index) => {
                                return (

                                    <div className={"tableRow table-hover " + (this.state.vcfirmId === record['vcfirmId'] ? 'active':'')} key={index}>
                                        <div className="userText width250 text-ellipsis alignEllipsisText paddingLeft30 cursor" title={record['firmName']} onClick={() => this.getListOfFunds(record['vcfirmId'],{'limit':1},true)}>{record['firmName']}</div>
                                        <label className="userImageAlt">
                                            {
                                                record['profilePic'] ?
                                                    <img src={record['profilePic']['url']} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                                    : <img src={userDefaultImage} alt="img" className="user-image" onClick={this.lpNameProfile(record)} />
                                            }
                                        </label>
                                        <div className="userText width200 text-ellipsis alignEllipsisText paddingLeft15" title={this.FsnetUtil.getFullName(record)} onClick={() => this.getListOfFunds(record['vcfirmId'],{'limit':1},true)}>{this.FsnetUtil.getFullName(record)}</div>
                                        <div className="userText width100Px paddingLeft30"><img src={plusImg} alt="edit" onClick={()=>this.proceedToNext(record)}/></div>
                                        <div className="userText width100Px paddingLeft30">
                                            <Button type="button" className={"fsnetSubmitButton sendNotify " + (record['isEmailConfirmed'] ? 'disabled' : 'btnEnabled')} onClick={()=>this.sendEmailInviteToGp(record)}>{!record['isEmailConfirmed'] && record['isGPNotified']?'Resend Email':'Send Email'}</Button>
                                        </div>
                                    </div>
                                );
                            })
                            :
                            <div className="title margin20">{this.state.noFirmsMsz}</div>
                        }
                    </div>
                    {
                        this.state.totalFirmItemsCount > this.state.itemsCountPerPage ?
                        <div className="marginLeft30">
                            <Pagination
                                activePage={this.state.activeFirmPage}
                                itemsCountPerPage={this.state.itemsCountPerPage}
                                totalItemsCount={this.state.totalFirmItemsCount}
                                pageRangeDisplayed={this.state.pageRangeDisplayed}
                                onChange={this.handleFirmPageChange}
                            />
                        </div>:''
                    }
                </div>
                <div className="marginTop20">
                    <div className="marginLeft30">
                        <label className="title">Funds</label>
                    </div>
                    <div className="marginLeft15 marginTop10">
                        <span className="fund-search"><i className="fa fa-search" aria-hidden="true"></i></span>
                        <FormControl type="text" placeholder="Search Funds" value={this.state.searchFundValue || ''} className="formFilterControl" onChange={(e) => this.searchInputChangeEvent(e)}/>
                    </div>

                    <div className="full-width marginTop20 marginLeft30">
                        <div className="table-heading width250 paddingLeft30 cursor" hidden={!this.state.showFundNameAsc} onClick={() => this.sortFundNameField('legalEntity','asc')}>
                            Fund Name
                            <i className="fa fa-sort-asc" aria-hidden="true cursor"  ></i>
                        </div>
                        <div className="table-heading width250 paddingLeft30" hidden={this.state.showFundNameAsc} onClick={() => this.sortFundNameField('legalEntity','desc')}>
                            Fund Name
                            <i className="fa fa-sort-desc" aria-hidden="true"  ></i>
                        </div>
                        <div className="table-heading width150">
                            Date Established
                        </div>
                        <div className="table-heading width150">
                            Status
                        </div>
                        <div className="table-heading width100Px">
                            # of Investors
                        </div>
                        <div className="table-heading width50">
                            Edit
                        </div>
                        <div className="table-heading width50">
                            Delete
                        </div>
                        <div className="table-heading width100Px paddingLeft10">
                            Notifications
                        </div>
                        {/* <div className="table-heading width100Px paddingLeft20">
                            Deactivate
                        </div> */}
                    </div>

                    <div className="tableContainer width900 marginLeft30 marginTop10">
                        {this.state.fundList.length > 0 ?
                            this.state.fundList.map((record, index) => {
                                return (
                                    <div className="tableRow" key={index}>
                                        <div className="userText width250 paddingLeft30 text-ellipsis" title={record['legalEntity']}>{record['legalEntity']}</div>
                                        <div className="userText width150 paddingLeft20">{this.FsnetUtil.dateFormat(record['createdAt'])}</div>
                                        <div className="userText width150">{record['status']['name']}</div>
                                        <div className="userText width100Px paddingLeft30">{record['totalInvestor']}</div>
                                        <div className="userText width50"><img src={edit} alt="edit" onClick={()=>this.impersonateUser(record)}/></div>
                                        <div className="userText width50"><i className="trash-icon fa fa-trash cursor" aria-hidden="true" onClick={()=>this.deleteFundShow(record)}></i></div>
                                        <div className="userText width100Px">
                                            <Button type="button" className={"fsnetSubmitButton sendNotify " + (record['enableEmailTriggerButton'] ? 'btnEnabled' : 'disabled')} onClick={()=>this.adminSendEmail(record)}>Send Email</Button>
                                        </div>
                                        {/* <div className="userText width50 paddingLeft30"><i className="fa fa-ban" aria-hidden="true" onClick={this.confirmDeactivate(record)}></i></div> */}
                                    </div>
                                );
                            })
                            :
                            <div className="title marginTop20 text-center">{this.state.noFundsMsz}</div>
                        }
                        
                    </div>
                    {
                        this.state.totalFundItemsCount > this.state.itemsCountPerPage ?
                        <div className="marginLeft30">
                            <Pagination
                                activePage={this.state.activeFundPage}
                                itemsCountPerPage={this.state.itemsCountPerPage}
                                totalItemsCount={this.state.totalFundItemsCount}
                                pageRangeDisplayed={this.state.pageRangeDisplayed}
                                onChange={this.handleFundPageChange}
                            />
                        </div>:''
                    }
                </div>
                <AdminModalsComponent></AdminModalsComponent>
                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default firmViewComponent;


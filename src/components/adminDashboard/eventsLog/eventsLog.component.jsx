import React, { Component } from 'react';
import '../adminDashboard.component.css';
import { FormControl } from 'react-bootstrap';
import Loader from '../../../widgets/loader/loader.component';
import { Fsnethttp } from '../../../services/fsnethttp';
import { FsnetAuth } from '../../../services/fsnetauth';
import { FsnetUtil } from '../../../util/util';
import ToastComponent from '../../toast/toast.component';
import { PubSub } from 'pubsub-js';
import { Constants } from '../../../constants/constants';
import Pagination from "react-js-pagination";

var close = {}
class eventsLogComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            showToast: false,
            toastMessage: '',
            toastType: 'success',
            eventsList: [],
            noEventsMsz: '',
            itemsCountPerPage: 10,
            totalItemsCount: 0,
            pageRangeDisplayed: 5,
            activePage: 1,
            eventTypes:[],
            listUsers:[],
            fundsList:[],
            eventType:0,
            fundId:0,
            userId:0,

        }
        this.FsnetAuth = new FsnetAuth();
        this.FsnetUtil = new FsnetUtil();
        this.Fsnethttp = new Fsnethttp();
        this.Constants = new Constants();
        close = PubSub.subscribe('closeToast', (msg, data) => {
            this.closeToast(data.timed);
        })
    }

    componentWillUnmount() {
        PubSub.unsubscribe(close);
    }

    closeToast(timed) {
        if (timed) {
            setTimeout(() => {
                this.setState({
                    showToast: false,
                    toastMessage: '',
                    toastType: 'success'
                })
            }, 4000);
        } else {
            this.setState({
                showToast: false,
                toastMessage: '',
                toastType: 'success'
            })
        }
    }

    componentDidMount() {
        this.eventsLog();
        this.getEventTypes();
        this.listUsers();
        this.getAllFunds();
    }

    eventsLog(pageNumber,obj) {
        this.open();
        this.Fsnethttp.eventsLog(pageNumber,obj).then(result => {
            this.close();
            this.setState({
                eventsList: result.data.eventsList.rows,
                totalItemsCount: result.data.eventsList.count,
                noEventsMsz: result.data.eventsList.rows.length == 0 ? this.Constants.NO_EVENTS : '',
                activePage:result.data.eventsList.rows.length == 0 ? 1 : this.state.activePage
            },()=>{
                document.getElementById('leftNavBar').style.position = ''
                this.FsnetUtil.setLeftNavHeight();
            })
        })
        .catch(error => {
            this.close();
        });
    }


    getEventTypes() {
        this.open();
        this.Fsnethttp.eventTypes().then(result => {
            this.close();
            this.setState({
                eventTypes: result.data
            })
        })
        .catch(error => {
            this.close();
        });
    }

    listUsers() {
        this.open();
        this.Fsnethttp.listUsers().then(result => {
            this.close();
            this.setState({
                listUsers: result.data
            })
        })
        .catch(error => {
            this.close();
        });
    }

    getAllFunds() {
        this.open();
        this.Fsnethttp.getAllFunds().then(result => {
            this.close();
            this.setState({
                fundsList: result.data
            })
        })
        .catch(error => {
            this.close();
        });
    }

    //ProgressLoader : Close progress loader
    close = () =>{
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () =>{
        this.setState({ showModal: true });
    }

    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber }, () => {
            let queryObj={};
            if(this.state.fundId !=0){
                queryObj['fundId'] = this.state.fundId
            }
            if(this.state.userId !=0){
                queryObj['userId'] = this.state.userId
            }
            if(this.state.eventType !=0){
                queryObj['eventType'] = this.state.eventType
            }
            this.eventsLog(pageNumber,queryObj)
        });
    }

    handleFilterChange = (e, type) =>{
        const value = e.target.value;
        this.setState({
            [type]:value
        },()=>{
            let queryObj={};
            if(this.state.fundId !=0){
                queryObj['fundId'] = this.state.fundId
            }
            if(this.state.userId !=0){
                queryObj['userId'] = this.state.userId
            }
            if(this.state.eventType !=0){
                queryObj['eventType'] = this.state.eventType
            }
            this.eventsLog(this.state.activePage,queryObj)
        })
    }


    render() {
        return (
            <div className="width100 adminContainer eventsLogContainer">
                <div className="main-heading"><span className="main-title">Events Log</span></div>
                <div className="marginTop20 marginLeft15 userContainer">
                    <FormControl name='filterFund' defaultValue={0} className="selectFormControl" value={this.state.eventType} componentClass="select" onChange={(e) => { this.handleFilterChange(e,'eventType') }}>
                        <option value={0}>Filter By: Event Type</option>
                        {this.state.eventTypes.map((record, index) => {
                            return (
                                <option value={record['eventType']} key={index} >{record['eventType']}</option>
                            );
                        })}
                    </FormControl>
                    <FormControl name='filterFund' defaultValue={0} className="selectFormControl marginLeft30" value={this.state.fundId} componentClass="select" onChange={(e) => { this.handleFilterChange(e,'fundId') }}>
                        <option value={0}>Filter By: Fund</option>
                        {this.state.fundsList.map((record, index) => {
                            return (
                                <option value={record['id']} key={index} >{record['legalEntity']}</option>
                            );
                        })}
                    </FormControl>
                    <FormControl name='filterFund' defaultValue={0} className="selectFormControl marginLeft30" value={this.state.userId} componentClass="select" onChange={(e) => { this.handleFilterChange(e,'userId') }}>
                        <option value={0}>Filter By: User</option>
                        {this.state.listUsers.map((record, index) => {
                            return (
                                <option value={record['id']} key={index} >{this.FsnetUtil.getFullName(record)}</option>
                            );
                        })}
                    </FormControl>
                </div>
                <div className="full-width marginTop30 marginLeft30 width1060">
                    <div className="table-heading width300 marginLeft30">
                        Event
                    </div>
                    <div className="table-heading width200">
                        User
                    </div>
                    <div className="table-heading width200">
                        Fund
                    </div>
                    <div className="table-heading width200">
                        Timestamp
                    </div>
                    <div className="table-heading width100Px">
                        IP Address
                    </div>
                </div>
                <div className={"tableContainer marginLeft30 marginTop10 width1060 "+(this.state.eventsList.length > 0 && 'height400')}>
                    {this.state.eventsList.length > 0 ?
                        this.state.eventsList.map((record, index) => {
                            return (
                                <div className="tableRow" key={index}>
                                    <div className="userText width300 marginLeft30 text-ellipsis paddingRight20" title={record['eventType']}>
                                        {record['eventType']}
                                    </div>
                                    <div className="userText width200 text-ellipsis paddingRight20" title={this.FsnetUtil.getFullName(record['eventUser'])}>
                                        {this.FsnetUtil.getFullName(record['eventUser'])}
                                    </div>
                                    <div className="userText width200 text-ellipsis" title={record['eventFund']['legalEntity']}>
                                        {record['eventFund']['legalEntity']}
                                    </div>
                                    <div className="userText width200 text-ellipsis">
                                        {this.FsnetUtil.UtcDateToLocalTimezone(record['eventTimestamp'])}
                                    </div>
                                    <div className="userText width100Px">
                                        {record['ipAddress']}
                                    </div>
                                </div>
                            );
                        })
                        :
                        <div className="title margin20">{this.state.noEventsMsz}</div>
                    }
                </div>
                {
                        this.state.totalItemsCount > this.state.itemsCountPerPage ?
                            <div className="marginLeft30">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.itemsCountPerPage}
                                    totalItemsCount={this.state.totalItemsCount}
                                    pageRangeDisplayed={this.state.pageRangeDisplayed}
                                    onChange={this.handlePageChange}
                                />
                            </div> : ''
                    }

                <ToastComponent showToast={this.state.showToast} toastMessage={this.state.toastMessage} toastType={this.state.toastType}></ToastComponent>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default eventsLogComponent;


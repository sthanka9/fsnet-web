import React, { Component } from 'react';
import './sidebar.component.css';
import { Row, Col, Modal, Button } from 'react-bootstrap';
import icon from '../../images/icon.svg';
import info from '../../images/info.svg';
import closeSmall from '../../images/close-small.svg';
import { Fsnethttp } from '../../services/fsnethttp';
import { FsnetUtil } from '../../util/util';
import { reactLocalStorage } from 'reactjs-localstorage';
import { PubSub } from 'pubsub-js';
import Loader from '../../widgets/loader/loader.component';

let alertHeight = '', userObj = {}
class SidebarComponent extends Component {

    constructor(props) {
        super(props);
        this.Fsnethttp = new Fsnethttp();
        this.FsnetUtil = new FsnetUtil();
        this.state = {
            allAlertsList: [],
            alertCount: 0,
            alertObj: {},
            showModal: false,
            switchRoleModal: false
        }
    }

    closeAlertModal = () => {
        this.props.toggleClose(this.state.alertCount);
    }

    componentDidMount() {
        //Get user obj from local storage.
        userObj = reactLocalStorage.getObject('userData');
        this.getAllAlerts(userObj.id)

    }

    //ProgressLoader : Close progress loader
    close = () => {
        this.setState({ showModal: false });
    }

    // ProgressLoader : show progress loader
    open = () => {
        this.setState({ showModal: true });
    }

    getAllAlerts = (id) => {
        if (id) {

            this.Fsnethttp.getAllAlerts(id).then(result => {
                this.setState({
                    allAlertsList: result.data.data,
                    alertCount: result.data.data.length
                }, () => {
                    //console.log(this.state.allAlertsList)
                    this.modifyAlertList()
                    if (this.state.allAlertsList.length > 0 ) {
                        alertHeight = window.innerHeight - 100;
                    }
                })
            })
                .catch(error => {
                });
        }
    }

    navigate = (data) => (event) => {
        if (event.target.nodeName === "A") {
            this.setState({
                alertObj: data
            }, () => {
                if (this.FsnetUtil.getUserRole() == data.accountType) {
                    this.generateAlertLink()
                } else {
                    this.openSwitchRoleModal()
                }
            })
        }
    }

    generateAlertLink = () => {
        const type = this.state.alertObj.notification && this.state.alertObj.notification.type ? this.state.alertObj.notification.type : null;
        const docType = this.state.alertObj.notification && this.state.alertObj.notification.docType ? this.state.alertObj.notification.docType:null
        if (type == 'FUND_INVITED') {
            window.location.href = '/dashboard'
        } else if(type == 'FUND_SIGNATURE') {
            window.location.href = `/lp/review/${this.FsnetUtil._encrypt(this.state.alertObj.notification.subscriptionId)}`
        } else if(type == 'FUND_APPROVE') {
            window.location.href = `/lp/pendingActions/${this.FsnetUtil._encrypt(this.state.alertObj.notification.subscriptionId)}`
        } else if(['CHANGE_COMMITMENT_AGREEMENT','SIDE_LETTER_AGREEMENT'].indexOf(docType) > -1) {
            if(['LP','LPDelegate','SecondaryLP'].indexOf(this.FsnetUtil.getUserRole()) > -1) {
                window.location.href = `/lp/review/${this.FsnetUtil._encrypt(this.state.alertObj.notification.subscriptionId)}&envelope_id=${this.FsnetUtil._encrypt(this.state.alertObj['notification']['documentId'])}&type=${this.state.alertObj['notification']['docType']}&id=${this.FsnetUtil._encrypt(this.state.alertObj['notification']['subscriptionId'])}`;
            } else {
                window.location.href = `/fund/view/${this.FsnetUtil._encrypt(this.state.alertObj['notification']['fundId'])}&envelope_id=${this.FsnetUtil._encrypt(this.state.alertObj['notification']['documentId'])}&type=${this.state.alertObj['notification']['docType']}&id=${this.FsnetUtil._encrypt(this.state.alertObj['notification']['subscriptionId'])}`;
            }
        } else {
            if(['LP','LPDelegate','SecondaryLP'].indexOf(this.FsnetUtil.getUserRole()) > -1) {
                window.location.href = `/lp/review/${this.FsnetUtil._encrypt(this.state.alertObj.notification.subscriptionId)}`;
            } else {
                window.location.href = `/fund/view/${this.FsnetUtil._encrypt(this.state.alertObj['notification']['fundId'])}`;
            }
        }
    }




    switchUserRole = () => {
        this.open();
        const postObj = { userId: this.state.alertObj.toUserId }
        this.Fsnethttp.switchAccount(postObj).then(result => {
            let data = result.data;
            this.FsnetUtil._userSetData(data);
            setTimeout(() => {
                this.close();
                this.generateAlertLink();
                this.closeSwitchRoleModal();
            }, 100);
        })
        .catch(error => {
            this.close();
        });
    }

    closeSwitchRoleModal = () => { this.setState({ switchRoleModal: false, alertObj: {} }) }
    openSwitchRoleModal = () => { 
        this.setState({ switchRoleModal: true },()=>{
            this.closeAlertModal();
        }) 
    }

    modifyAlertList = () => {
        let list = this.state.allAlertsList;
        for (let index of list) {
            let message = index['notification']['htmlMessage'] ? index['notification']['htmlMessage']:null;
            if(message) {
                if (message.indexOf('[GPName]') !== -1) {
                    message = message.replace('[GPName]', index['notification']['gpName'] ? index['notification']['gpName'] : this.FsnetUtil.getFullName(index['notification']))
                }
                if (message.indexOf('[LPName]') !== -1) {
                    message = message.replace('[LPName]', index['notification']['lpName'] ? index['notification']['lpName'] : this.FsnetUtil.getFullName(index['notification']))
                }
                if (message.indexOf('[numberOfexistingOrProspectives]') !== -1) {
                    message = message.replace('[numberOfexistingOrProspectives]', index['notification']['numberOfexistingOrProspectives'])
                }
                if (message.indexOf('[fundName]') !== -1) {
                    message = message.replace('[fundName]', index['notification']['fundName'])
                }
                if (message.indexOf('[fundManagerCommonName]') !== -1) {
                    message = message.replace('[fundManagerCommonName]', index['notification']['fundManagerCommonName'])
                }
                if (message.indexOf('[fundManager]') !== -1) {
                    message = message.replace('[fundManager]', index['notification']['fundManager'])
                }
                if (message.indexOf('[docType]') !== -1) {
                    message = message.replace('[docType]', index['notification']['docType'])
                }
                if (message.indexOf('[fundCommonName]') !== -1) {
                    message = message.replace('[fundCommonName]', index['notification']['fundCommonName'])
                }
                if (message.indexOf('[CapitalCommitmentAccepted]') !== -1) {
                    message = message.replace('[CapitalCommitmentAccepted]', index['notification']['CapitalCommitmentAccepted'] ? index['notification']['CapitalCommitmentAccepted'] : index['notification']['capitalCommitmentAccepted'])
                }
                if (message.indexOf('[DelegateName]') !== -1) {
                    message = message.replace('[DelegateName]', index['notification']['delegateName'])
                }
                if (message.indexOf('[CapitalCommitmentOfferedAmount]') !== -1) {
                    message = message.replace('[CapitalCommitmentOfferedAmount]', index['notification']['CapitalCommitmentOfferedAmount'] ? index['notification']['CapitalCommitmentOfferedAmount'] : index['notification']['capitalCommitmentOfferedAmount'])
                }
                if (message.indexOf('[VanillaLink]') !== -1) {
                    message = message.replace('[VanillaLink]', `<a>Click Here</a>`)
                }
                if (message.indexOf('[approve]') !== -1) {
                    message = message.replace('[approve]', `<a>Approve</a>`)
                }
                if (message.indexOf('[FundCommonName]') !== -1) {
                    message = message.replace('[FundCommonName]', `<a>${index['notification']['fundName']}</a>`)
                }
                if (message.indexOf('[visit]') !== -1) {
                    message = message.replace('[visit]', `<a>visit</a>`)
                }
                if (message.indexOf('[counter-sign]') !== -1) {
                    message = message.replace('[counter-sign]', `<a>sign</a>`)
                }
                if (message.indexOf('[investorName]') !== -1) {
                    message = message.replace('[investorName]', index['notification']['investorName'])
                }
            }
            index['notification']['htmlMessage'] = message;
        }
        this.setState({
            allAlertsList: list
        })
    }

    dismissAllAlerts = () => {

        this.Fsnethttp.dismissAllAlerts().then(result => {
            this.setState({
                allAlertsList: [],
                alertCount: 0
            })
            PubSub.publish('alertCount', true);
        })
            .catch(error => {
            });
    }

    dismissAlert = (e, data) => {

        if (data.record.id) {
            let postobj = { notificationId: data.record.id, isRead: true }
            this.Fsnethttp.dismissAlert(postobj).then(result => {
                let alertsList = this.state.allAlertsList.filter(alert => alert.id !== data.record.id);
                this.setState({
                    allAlertsList: alertsList,
                    alertCount: alertsList.length
                })
                PubSub.publish('alertCount', true);
            })
                .catch(error => {
                });
        }
    }

    render() {
        return (
            <div>
                <div className={"notificationOverlay " + (this.props.visible ? 'show' : 'hide')}>
                </div>
                <Row className={"notificationContainer " + (this.props.visible ? 'show' : 'hide')}>
                    <Row className="header">
                        <Col xs={10} sm={10} md={10} className="title">
                            Alerts
                        </Col>
                        <Col xs={2} sm={2} md={2} className="title closeAlert" onClick={this.closeAlertModal}>
                            <img src={icon} alt="home_image" className="" />
                        </Col>
                        <div className="dismissAll" hidden={this.state.allAlertsList.length === 0} onClick={this.dismissAllAlerts}>Dismiss All</div>
                    </Row>
                    {/* style={{ height: height+'px' }} */}
                    <Row className="alerts-section" id="user-alert" style={{ height: alertHeight + 'px' }}>
                        {this.state.allAlertsList.length > 0 ?
                            this.state.allAlertsList.map((record, index) => {
                                return (
                                    <Row className="alert-row" key={index}>
                                        <Col xs={2} sm={2} md={2}>
                                            <img src={info} alt="home_image" className="alert-home-image" />
                                        </Col>
                                        <Col xs={7} sm={7} md={7} className="alertPaddingLeft">
                                            <Row>
                                                {/* <div className="alert-text">{record['firstName']} {record['lastName']} ({record['accountType']})</div> */}
                                            </Row>
                                            <Row>
                                                {
                                                    record['notification'] ?
                                                        <div onClick={this.navigate(record)} className="alert-text alertTextWidth" dangerouslySetInnerHTML={{
                                                            __html: record['notification']['htmlMessage']
                                                        }} />
                                                        : ''
                                                }
                                            </Row>
                                        </Col>
                                        <Col xs={3} sm={3} md={3} className="paddingZero">
                                            <Row>
                                                <div className="date">{this.FsnetUtil.convertDate(this.FsnetUtil.UtcDateToLocalTimezoneWithoutFormat(record['createdAt']))}</div>
                                                <div className="date alertWidth"><div className="dismiss"><img src={closeSmall} alt="home_image" className="" onClick={(e) => this.dismissAlert(e, { record })} /></div></div>
                                            </Row>
                                        </Col>
                                    </Row>
                                );
                            }) :
                            <div className="title marginTop50 text-center width100">No Alerts</div>
                        }
                    </Row>

                </Row>
                <Modal id="confirmFundModal" show={this.state.switchRoleModal} onHide={this.closeSwitchRoleModal} dialogClassName="confirmFundDialog">
                    <Modal.Header className="headerNone" closeButton>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="title-md ulpoadSignText">The link which you have clicked is related to your role as a {this.FsnetUtil.getAccountTypeName(this.state.alertObj.accountType)}. Do you want to switch to this role?</div>
                        <Row className="fundBtnRow">
                            <Col lg={6} md={6} sm={6} xs={12} className="text-right">
                                <Button type="button" className="fsnetCancelButton" onClick={this.closeSwitchRoleModal}>Cancel</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <Button type="button" className="fsnetSubmitButton btnEnabled width200" onClick={this.switchUserRole}>Proceed</Button>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Loader isShow={this.state.showModal}></Loader>
            </div>
        );
    }
}

export default SidebarComponent;




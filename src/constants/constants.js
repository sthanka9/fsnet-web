export class Constants{

    // Environments settings

    // Develop URL
    // BASE_URL = `http://192.168.1.109:8000/api/v1/`;
    VERSION = "Vanilla v1.1";

    // PROD URL
    //  BASE_URL = `https://api-prod.vanillavc.com/api/v1/`;

    // UAT URL
    //  BASE_URL = `https://api-uat.vanillavc.com/api/v1/`;
    
    // QA URL
    // BASE_URL = `http://api-qa.vanillavc.com/api/v1/`;
     BASE_URL = `http://192.168.1.89:8000/api/v1/`;

    //Vanilla URL

    // URL = 'http://qa.vanillavc.com';
     URL = `http://localhost:8000`;

    _registerLink = `${this.URL}/register/`
    AUTO_SAVE_TIME = 10000;
    EMAIL_REGEX= /^([a-zA-Z0-9_+\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    NUMBER_REGEX = /^[0-9]*[.]{0,1}[0-9]/;
    NUMBER_DOT_REGEX = /^[0-9]*\.?[0-9]*$/;
    PASSWORD_REGEX = /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[?\{\}\|\(\)\`~!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/;
    NUMBER_HUNDRED_REGEX = /^(.*[^0-9]|)(999|[1-9]\d{0,2})([^0-9].*|)$/;
    _99REGEX = /^[0-9]{0,3}\.?[0-9]{0,2}$/;
    REQUIRED_PASSWORD_CONFIRMPASSWORD_SAME = "Password and Password Confirm should be the same.";
    CURRENT_PWD_REQUIRED = 'Please enter current password.';
    CNFRM_PWD_REQUIRED = 'Please enter confirm password.';
    REQUIRED_PASSWORD_AGAINPASSWORD_SAME = "Passwords do not match.";
    TERMS_CONDITIONS = "Please accept terms & conditions.";
    MOBILE_NUMBER_FORMAT = "Phone number is invalid.";
    PASSWORD_RULE_MESSAGE = 'Password must be at least 8 characters and include one number and one special character (e.g., & @ !).").';
    USER_NAME_EXISTS = 'Username already in use.';
    USER_NAME_REQUIRED = 'Please select username.';
    REASON_REQUIRED = 'Please enter reason to deactivate fund.';
    LOGIN_USER_NAME_REQUIRED = 'Please enter username.';
    FORGOT_USER_NOT_EXISTS = 'Username/Email address does not exist.';
    FIRM_NAME_REQUIRED = 'Please enter firm name.';
    FIRST_NAME_REQUIRED = 'Please enter first name.';
    LAST_NAME_REQUIRED = 'Please enter last name.';
    MIDDLE_NAME_REQUIRED = 'Please enter Middle name.';
    LOGIN_PASSWORD_REQUIRED = 'Please enter password.';
    LOGIN_EMAIL_REQUIRED = 'Please enter Email.';
    INVALID_LOGIN = 'Invalid credentials. Please try again. Please note your account will get locked after 5 unsuccessful attempts.';
    CELL_NUMBER_REQUIRED = 'Phone number is required.';
    CELL_NUMBER_VALID = 'Please enter valid phone number.';
    ACCOUNT_TYPE_REQUIRED = 'Please enter account type.';
    EMAIL_MOBILE_REQUIRED = 'Please enter email or cell number.';
    VALID_EMAIL = 'Please enter valid email.'
    INVALID_VERIFY_CODE = 'Please enter valid code.';
    PASSWORD_NOT_MATCH = 'Password must be at least 8 characters and include one number and one special character (e.g., & @ !).")';
    INTERNAL_SERVER_ERROR = 'Something went wrong.';
    NO_FIRM_ID = 'Please select Fund Manager.';
    USERNAME_FORMAT = 'Username can only contain letters and numbers.';
    USERNAME_MIN_CHARCTERS = 'Username should be 5 to 60 characters.';
    GP_DELEGATES_REQUIRED = 'Please select at least one Delegate for this fund.';
    LP_REQUIRED = 'Please select at least one Investor for this fund.';
    LEGAL_ENTITY_REQUIRED = 'Please enter Legal Entity.';
    FUND_COMMON_NAME_REQUIRED = 'Please enter Fund Common Name.';
    FUND_MANAGER_TITLE_REQUIRED = 'Please enter Fund Manager Title.';
    FUND_MANAGER_COMMON_NAME_REQUIRED = 'Please enter Fund Manager Common Name.';
    LEGAL_ENTITY_NAME_REQUIRED = 'Please enter Fund Manager Legal Entity Name.';
    FUND_TYPE_REQUIRED = 'Please select any one of the Fund Type.';
    TIME_ZONE_REQUIRED = 'Please select any one of the Time Zone.';
    FIRM_ID_REQUIRED = 'Please select any one of the Fund manager.';
    HARDCAP_VALIDATION_RULE_REQUIRED = 'Please select any one of the Hard Cap Application Rule.';
    TYPE_OF_EQUITY_REQUIRED = 'Please select any one of the Fund Entity Type.';
    UPLOAD_DOC_REQUIRED = 'Fund document must be in PDF format and smaller than 10MB.';
    UPLOAD_TEXT = 'Click to select and upload the PDF file or drag and drop from your desktop to upload. File size cannot exceed 10MB.';
    SIZE_LIMIT = 10485760;
    SIZE_MB = 1048576;
    FUND_TARGET_COMMITMENT_MSZ = 'The Fund Target Commitment should always be less than the Hard Cap value.';
    FUND_TARGET_COMMITMENT_ZERO_MSZ = 'The Fund Target Commitment should always be greater than the value Zero.';
    FUND_HARD_CAP_MSZ = 'Hard Cap value should always be greater than the Fund Target Commitment.';
    FUND_HARD_CAP_ZERO_MSZ = 'Hard Cap value should always be greater than the value Zero.s';
    NO_GP_DELEGATES = 'You haven’t added any Delegates to this Fund yet.';
    NO_GP_SIGNATORIES = 'You haven’t added any Secondary Signatories to this Fund yet.';
    NO_LP_SIGNATORIES = 'You haven’t added any Secondary Signatories to this Fund yet.';
    NO_LPS = 'You haven’t added any Investors to this Fund yet.';
    NO_ADDENDUMS = 'You haven’t added any Addendums to this Fund yet.';
    NO_GP_FUNDS = 'No Funds available. Please click on New Fund button to create new Fund.';
    NO_LP_FUNDS = 'No Funds available.';
    NO_LPDELGATE_FUNDS = 'The investor for which you are an assigned delegate has not yet registered with Vanilla. Once they have registered an account with Vanilla you may act as a delegate on their behalf for the Funds which you have been assigned to.';
    NO_DOCUMENTLOCKER_DOCUMENTS = 'There are no completed documents to display.';
    NO_SIDELETTER_DOCUMENTS = 'There are no Side Letter documents to display.';
    NO_AMENDMENT_DOCUMENTS = 'There are no Amendment documents to display.';
    NO_PENDING_ACTIONS_DOCUMENTS = 'There are no actions pending your approval at this time.';
    NO_DOCUMENTLOCKER_FILTER_DOCUMENTS = 'There are no completed documents to display that match your selected filter(s). Please remove filters to view all available documents.';
    DELEGATE_EXISTS = 'Delegate is already exists.';
    SIGNATORY_EXISTS = 'Secondary Signatory is already exists.';
    SEND_MESSAGE_ERROR = 'Message cannot be empty.';
    NO_GP_SEARCH_FUNDS = 'There are no funds with the given search name.';
    NO_PREPOPULATE_SEARCH_FUNDS = 'There are no prior Funds in which you have participated that match your search.';
    NO_INVESTORS_CLOSED_READY_STATUS = 'There are no Investors in Close-Ready status.';
    DROP_YOUR_FILES_FROM_DESKTOP  = 'Click to select and upload the PDF file or drag and drop from your desktop to upload. File size cannot exceed 10MB.';
    FUND_REQUIRED = 'Please select Fund.';
    INVESTOR_REQUIRED = 'Please select Investor.'
    NO_EVENTS = 'There are No Events.';
    SIDE_LETTER_SETTING_COUNT = 'Closing setting count should be greater than side letter setting count.'
    DELEGATE_NO_FUNDS = 'You have been invited as a Delegate to assist with this Fund. In order to register as a Delegate, it is necessary that the person who has been invited in the role of Primary Fund Manager first establish his or her account. Once this action is completed, you will be able to see the Fund.'
    ENTER_SIGNATURE_AUTHENTICATION_PASSWORD = 'Please re-enter your password';
    PASSWORD_MESSAGE = 'By entering your password, you represent that you have signing authority for the applicable Fund and/or Document.';

    /**************LP Subscrption Form Start****************/
    NAME_REQUIRED = 'Please enter name.';
    STREET_REQUIRED = 'Please enter Street name.';
    CITY_REQUIRED = 'Please enter City name.';
    STATE_REQUIRED = 'Please enter State name.';
    ZIP_REQUIRED = 'Please enter Zip Code.';
    ENTITY_NAME_REQUIRED = 'Please enter Entity’s Name.';
    ENTITY_TITLE_REQUIRED = 'Please enter your title within the Entity.';
    TRUST_NAME_REQUIRED = 'Please enter Trust’s Name.';
    INVESTOR_SUB_TYPE_REQUIRED = 'Please select Investor Sub Type.';
    COUNTRY_REQUIRED = 'Please select Country.';
    COUNTRY_RESIDENCE_REQUIRED = 'Please select Country of residence.';
    COUNTRY_DOMICILE_REQUIRED = 'Please select Country of domicile.';
    STATE_REQUIRED = 'Please select State.';
    STATE_RESIDENCE_REQUIRED = 'Please select State of residence.';
    STATE_DOMICILE_REQUIRED = 'Please select State of domicile.';
    ENTITY_TYPE_REQUIRED = 'Please enter Entity type.';
    LEGAL_TITLE_REQUIRED = 'Please enter Legal Title.'
    JURIDICTION_REQUIRED = 'Please select Entity legally registered.';
    LEGAL_DESIGNATION_REQUIRED = 'Please enter the legal title designation for fund to hold the Trust’s interest.';
    ENTITY_US_501_REQUIRED = 'Please select Is the Entity a U.S. 501(c)(3).';
    SIMILAR_TYPE_VEHICLE_REQUIRED = 'Please select Entity a fund-of-funds or a similar type vehicle.';
    EQUITY_OWNERS_REQUIRED = 'Please enter number of direct equity owners of the Entity.';
    ENTITY_TAX_REQUIRED = 'Please select Entity tax exempt for U.S. federal income tax purposes.'
    EXISTING_INVESTORS_REQUIRED = 'Please enter existing or prospective investors.';
    TOTAL_VALUE_REQUIRED = 'Please enter total value of equity interests in the Trust is held by “benefit plan investors”.';
    TOTAL_VALUE_REQUIRED_TRUST = 'Please input the percentage of equity interests in the Trust is held by “benefit plan investors”.';
    TOTAL_VALUE_REQUIRED_LLC = 'Please input the percentage of equity interests in the Entity is held by “benefit plan investors”.';
    TRUST_501_REQUIRED = 'Please select is the Trust a 501(c)(3).';
    INDIRECT_BENIFICIAL_REQUIRED = 'Cannot be left blank.';//'Please enter Entity or its direct or indirect beneficial owners.';
    INDIVIDUAL_BENIFICIAL_REQUIRED = 'Please enter Individual or its direct or indirect beneficial owners.';
    INDIRECT_BENIFICIAL_TRUST_REQUIRED = 'Please enter Trust or its direct or indirect beneficial owners.';
    SELECT_STATE_REQUIRED = 'Please select What State is the entity registered in?';
    SELECT_COUNTRY_REQUIRED = 'Please select What Country is the entity registered in?';
    NEW_FUND_COMMITMENT = 'New Fund Commitment value should be more than the current Fund commitment.';
    NO_FUND_AGREEMENT = 'You haven’t added any Fund Agreement Change to this Fund yet.';
    NO_FIRMS = 'There are no Firms to display.';
    NO_FUNDS = 'No Funds have been created for the selected Firm.';
    NO_SEARCH_FUNDS = 'There are no Funds that match your search. Please revise your search.';
    NO_GP_USERS = 'There are no Fund Managers or Delegates to display.';
    NO_GP_FILTER_USERS = 'There are no Fund Managers or Delegates to display. Please revise your search or edit your filters.';
    NO_LP_FILTER_USERS = 'There are no Investors or Investor Delegates to display. Please revise your search or edit your filters.';
    NO_LP_USERS = 'There are no investors or Investor Delegates to display.';
    NO_GP_SEARCH_USERS = 'There are no Fund Managers or Delegates to display. Please revise your search or edit your filters.';
    NO_LP_SEARCH_USERS = 'There are no Investors or Investor Delegates to display. Please revise your search or edit your filters.';
    LIMIT_GP_SIGNATORIES = 'Please select maximum three users as Secondary Signatories to the Fund.';
    IMPERSONATE_UNLOCK_MSZ = 'This user account has been locked. Please unlock the user using the “Unlock User” option.';
    USER_NOT_REGISTERED = 'User is not registered in Vanilla.';
    /**************LP Subscrption Form End***************/

    /****** Add Question Error Mszs */
    TYPE_REQUIRED = 'Please select type.';
    IS_REQUIRED = 'Please select is required field.';
    QUESTION_REQUIRED = 'Please enter question.';
    QUESTION_TITLE_REQUIRED = 'Please enter question title.';
    REQUIRED_FIELD = 'This is required field.'
    PASSWORD_MESSAGE = 'By entering your password, you represent that you have signing authority for the applicable Fund and/or Document.';
    TERMINATE_MESSAGE = 'By entering your password, you are authorizing the termination of the Amendment.';



    /****** Validation rules */
    INVESTORS_COUNT_EXCESS_99 = 'You are attempting to close a constituency of investors which would cause the Fund to lose its exemption under Section 3(c)(1) of the Companies Act.  This is generally not advisable.  Please consult your legal adviser.';
    CLOSED_INVESTORS_COUNT_EXCESS_99 = 'You are attempting to close a constituency of investors which would cause the Fund be in a position where it could not rely on either Section 3(c)(1) or Section 3(c)(7) of the Companies Act, and thus would likely have no viable exemption from registration under the Companies Act.';
    INVESTORS_COUNT_EXCESS_499 = 'You are attempting to close a constituency of investors which would cause the Fund to lose its exemption under Section 3(c)(7) of the Companies Act.  This is generally not advisable.  Please discuss with your legal counsel.';
    ERISA_WARNINGS = 'You are attempting to close a constituency of investors such that the Fund would have over 25% ERISA Benefit Plan money and would become subject to regulation as a Fund holding Plan Assets unless the Fund operates as a venture capital operating company.  Please consult your legal adviser.';
    DISQUALIFYING_EVENT = 'You are attempting to close a constituency of investors such that the Fund would have one or more investors which has had a Disqualifying Event.  Please consult your legal adviser.';
    ACCREDITED_EVENT_1 = 'You are attempting to close a constituency of investors such that the Fund would have a non-accredited investor.';
    ACCREDITED_EVENT_2 = 'This is a serious situation which is almost never advisable. Please consult your legal adviser.';
    EEA_WARNINGS = "Please be advised that [Investor Name] [verbal] proposing to subscribe to [Fund Name] but [verbal1] inside the European Economic Area (EEA) and as such there may be legal implications of accepting their Capital Commitment under applicable AIFMD or AIFMD-like rules.  Please consult the Fund's legal counsel immediately, prior to any further communications with the proposed Investor and prior to closing.";
    EQUITY_OWNERS_EVENT = "[Investor Name] has indicated that they are under common control with another Investor which they designated as [text_value].  Please note that if these investors add up collectively to 10% or more of total Investor interests at the applicable closing, you should carefully review with your counsel the 1940 Act Investor Count as Vanilla will not automatically count them as related and therefore the 1940 Investor Act Count may be higher than indicated by Vanilla.  Please discuss this issue with your legal counsel if applicable.";
    SIGNATURE_PWD = 'Please re-enter your password.';
    LP_CLOSED_SUBSCRIPTION = "It is very unusual to need to delete a closed-upon Investor.  Please discuss this option with the Fund's counsel and make extraordinarily certain you wish to do so prior to proceeding.  This action once invoked cannot be reversed.";

    INVITE_ALL = 'This feature will invite all investors which have been added to this Fund but have not yet been sent an email invitation. Upon clicking this button, all uninvited investors on this screen will be sent email invitations to join the Fund.';
    FIRST_NAME='First Name';
    LAST_NAME='Last Name';
    MIDDLE_NAME='Middle Name or Initial';
    ORG_NAME='Organization Name';
    PHONE_NUMBER='Phone Number (Cell)';
    PLACEHOLDER_ORG_NAME='Organization Name';
    EMAIL='Email Address';
    PLACEHOLDER_EMAIL='ProfessorX@gmail.com';
    PLACEHOLDER_FIRST_NAME='Charles';
    PLACEHOLDER_LAST_NAME='Xavier';
    FUND_SETUP_MANDATORY_1 = ['legalEntity','fundCommonName','fundManagerLegalEntityName','fundManagerTitle','fundManagerCommonName','fundEntityType','fundTargetCommitment','fundHardCap','fundType','hardCapApplicationRule','timezone'];
    FUND_FIELDS = ['legalEntity','fundCommonName','fundManagerLegalEntityName','fundManagerTitle','fundManagerCommonName','fundEntityType','fundTargetCommitment','fundHardCap','fundType','hardCapApplicationRule','timezone','partnershipDocument']
    FUND_SETUP_MANDATORY_2=['percentageOfLPCommitment','capitalCommitmentByFundManager','percentageOfLPAndGPAggregateCommitment'];
    FUND_ASTERIK='Fields marked with an asterisk (*) must be completed before in order to continue setting up the fund, including inviting delegates. Fields marked with double asterisks (**) are required before investors can be invited to subscribe.';
    CANCEL = 'Cancel';
    CONFIRM_DELETION = 'Confirm Deletion';
    LINK_COPIED = 'Link copied successfully.';
    RESEND_EMAIL = 'Resend email successfully.';
    LINK_COPIED_ERROR = 'Unable to copy the link.';
    RESEND_EMAIL_ERROR = 'Unable to Resend email.';
    DOC_UPLOAD_MSZ = 'Document has been uploaded successfully.';
    NO_STATUS_HISTORY = 'No track history found.';
    ACTION_LINK = 'Copy Link.';
    RESEND_EMAIL_LINK = 'Resend email.';
    DOC_UPLOAD_MSZ = 'Document has been uploaded successfully.';
    EDIT_LP_TITLE = 'Edit Investor';
    DELETE_LP_TITLE = 'Delete Investor';
    EDIT_LPDELEGATE_TITLE = 'Edit Investor Delegate';
    RESEND_EMAIL_TITLE = 'Resend Email Invitation';
    COPY_LINK_TITLE = 'Copy Link';
    DELETE_LP_DELEGATE_LINK = 'Delete Investor Delegate';
    DELETE_GP_SIGNATORY_TITLE = 'Delete Fund Manager Co Signatory';
    EDIT_GP_SIGNATORY_TITLE = 'Edit Fund Manager Co Signatory';
    EDIT_GP_DELEGATE_TITLE = 'Edit Fund Manager Delegate';
    DELETE_GP_DELEGATE_TITLE = 'Delete Fund Manager Delegate';
    EXISTING_INVESTORS = 'Existing Investors';
    ADD_EXISTING_INVESTORS = 'Add Existing Investors';
    EXISTING_INVESTORS_TITLE = 'Click "Select Investors" below to invite investors that have been added to other Funds.';
    NO_EXISTING_INVESTORS = 'There are no existing investors to select for this Fund.';
    TIME_ZONE_TOOLTIP = 'Set to the time zone of the domicile of the fund. For example, a Cayman-based fund should use Eastern Standard Time.';
    TIME_ZONE_DEFAULT_VALUE = 1;
}

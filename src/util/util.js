import moment from 'moment';

export class FsnetUtil{
    convertToCurrency(value) {
        let amount = parseFloat(value);
        //This formatter will support IE Edge only
        if(amount !== '' && (this.get_browser().name !== 'MSIE' || this.get_browser().name === 'MSIE' && this.get_browser().version > 10) ) {
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 0,
                // the default value for minimumFractionDigits depends on the currency
                // and is usually already 2
            });
            return formatter.format(amount); 
        } else {
             return "$" + amount;
        }
    }
    convertToCurrencyTwodecimals(value) {
        let amount = parseFloat(value);
        //This formatter will support IE Edge only
        if(amount !== '' && (this.get_browser().name !== 'MSIE' || this.get_browser().name === 'MSIE' && this.get_browser().version > 10) ) {
            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2,
                // the default value for minimumFractionDigits depends on the currency
                // and is usually already 2
            });
            return formatter.format(amount); 
        } else {
             return "$" + amount;
        }
    }


    get_browser() {
        var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
        if(/trident/i.test(M[1])){
            tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
            return {name:'IE',version:(tem[1]||'')};
            }   
        if(M[1]==='Chrome'){
            tem=ua.match(/\bOPR|Edge\/(\d+)/)
            if(tem!=null)   {return {name:'Opera', version:tem[1]};}
            }   
        M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
        return {
          name: M[0],
          version: M[1]
        };
     }

    convertNumberToMillion(labelValue) {
        // Nine Zeroes for Billions
        return Math.abs(Number(labelValue)) >= 1.0e+9
    
        ? Math.abs(Number(labelValue)) / 1.0e+9 + "B"
        // Six Zeroes for Millions 
        : Math.abs(Number(labelValue)) >= 1.0e+6
    
        ? Math.abs(Number(labelValue)) / 1.0e+6 + "M"
        // Three Zeroes for Thousands
        : Math.abs(Number(labelValue)) >= 1.0e+3
    
        ? Math.abs(Number(labelValue)) / 1.0e+3 + "K"
    
        : Math.abs(Number(labelValue));
    }

    getLpFundId() {
        var url = window.location.href;
        var parts = url.split("/");
        var urlSplitFundId = parts[parts.length - 1];
        if(urlSplitFundId.indexOf('&envelope_id=') === -1 && urlSplitFundId .indexOf('type=')  === -1 && urlSplitFundId .indexOf('id=')  === -1) {
            return parseInt(urlSplitFundId);
        } else {
            var id = urlSplitFundId.split('&')[0]
            if(id.indexOf('?type=') === -1) {
                return this._decrypt(id)
            } else {
                let newId = id.split('?type')[0]
                return this._decrypt(id)
            }
            
        }
    }

    _decrypt(value) {
        let name= null;
        try {
            name = atob(value);
        } catch {
            name = null
        }
        return name ? name : null
    }

    _encrypt(value) {
        let name= null;
        try {
            name = btoa(value);
        } catch {
            name = null
        }
        return name ? name : null
    }

    _getId() {
        var url = window.location.href;
        var parts = url.split("/");
        var urlSplitFundId = parts[parts.length - 1];
        var queryStringPart = urlSplitFundId.split('?');
        urlSplitFundId = queryStringPart[0];
        if(urlSplitFundId.indexOf('&')) {
            urlSplitFundId = urlSplitFundId.split('&')[0]
        }
        return urlSplitFundId !== 'funddetails' ? this._decrypt(urlSplitFundId): 0
        
    }

    checkEnvelopNameExists() {
        var url = window.location.href;
        var envelopeId = url.indexOf("envelope_id=");
        return envelopeId;
    }

    getAdminUrlName() {
        var url = window.location.href;
        var name = url.split("adminDashboard/");
        return name[1];
    }

    getEnvelopeId () {
        var url = window.location.href;
        var envelopeId = url.indexOf("envelope_id=").split('&type=');
        return envelopeId[0];
    }

    getEnvelopeIdForCapitalCommitment() {
        var url = window.location.href;
        var parts = url.split("capitalCommitmentPdf/");
        var urlSplitFundId = parts[1].split('/')
        var envelopId = urlSplitFundId[0]
        return envelopId;
    }

    getEnvelopeIdForSideLetter() {
        var url = window.location.href;
        var parts = url.split("sideLetterPdf/");
        var urlSplitFundId = parts[1].split('/')
        var envelopId = urlSplitFundId[0]
        return envelopId;
    }

    getEnvelopeId() {
        var url = window.location.href;
        var parts = url.split("id=");
        var urlSplitFundId = parts[parts.length - 1];
        var envelopId = urlSplitFundId.split("?")[0];
        var checkType = envelopId.split('&')[0];
        return checkType;
    }

    getType() {
        var url = window.location.href;
        var parts = url.split("type=");
        var urlSplitFundId = parts[parts.length - 1];
        var type = urlSplitFundId.split("?")[0]
        if(type.indexOf('id=') !== -1) {
            return type.split('&id=')[0]
        } else {
            return type;
        }
    }
    getId() {
        var url = window.location.href;
        var parts = url.split("/");
        var urlSplitFundId = parts[parts.length - 1];
        if(urlSplitFundId.indexOf('id=') === -1) {
            return urlSplitFundId;
        } else {
            var id = urlSplitFundId.split('&')[0]
            return id
        }
    }

    getAlertId() {
        var url = window.location.href;
        var parts = url.split("&id=");
        var id = parts[parts.length - 1];
        return id
    }

    getCurrentPageForLP() {
        var url = window.location.href;
        var parts = url.split("/");
        var page = parts[parts.length - 2];
        return page;
    }

    percentage(partialValue, totalValue) {
        return (100 * partialValue) / totalValue;
    } 

    checkNullOrEmpty(list,obj) {
        for(let index of list) {
            if(obj[index] === null || obj[index] === '' || obj[index] === undefined) {
                if(index == 'otherInvestorAttributes' && obj[index] && obj[index].length == 0) {
                    return false;
                }
                return false;
            }
        }
        return true;
    }

    _iNull(val) {
        if(val === null || val === '' || val === undefined) {
            return true;
        }
        return false;
    }


    decodeEntities(encodedString) {
        var textArea = document.createElement('textarea');
        textArea.innerHTML = encodedString;
        return textArea.value;
    }

    decodeObj(obj) {
        if (!Array.isArray(obj) && typeof obj != 'object') return obj;
        return Object.keys(obj).reduce((acc, key) => {
          acc[key] = obj[key] != null ? (typeof obj[key] == 'string'? this.decodeEntities(obj[key]) : this.decodeObj(obj[key])): null;
          return acc;
        }, Array.isArray(obj)? []:{});
    }

    convertDate(date) {
        // Hack For IE
        if (!String.prototype.padStart) {
            String.prototype.padStart = function padStart(targetLength, padString) {
                targetLength = targetLength >> 0; //truncate if number, or convert non-number to 0;
                padString = String(typeof padString !== 'undefined' ? padString : ' ');
                if (this.length >= targetLength) {
                    return String(this);
                } else {
                    targetLength = targetLength - this.length;
                    if (targetLength > padString.length) {
                        padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
                    }
                    return padString.slice(0, targetLength) + String(this);
                }
            };
        }
        var date = new Date(date);
        var hours = date.getHours() > 12 ? date.getHours()-12 : date.getHours()
        var amPm = date.getHours() >= 12 ? 'PM' : 'AM';
        var convertedDate = date.getMonth()+1+'/'+date.getDate()+' - '+hours+':'+String(date.getMinutes()).padStart(2, "0")+' '+amPm;
        return convertedDate;
    }

    getDateAndDay(date) {
        // Hack For IE
        if (!String.prototype.padStart) {
            String.prototype.padStart = function padStart(targetLength, padString) {
                targetLength = targetLength >> 0; //truncate if number, or convert non-number to 0;
                padString = String(typeof padString !== 'undefined' ? padString : ' ');
                if (this.length >= targetLength) {
                    return String(this);
                } else {
                    targetLength = targetLength - this.length;
                    if (targetLength > padString.length) {
                        padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
                    }
                    return padString.slice(0, targetLength) + String(this);
                }
            };
        }
        var date = new Date(date);
        var convertedDate = date.getMonth()+1+'/'+date.getDate();
        return convertedDate;
    }

    getTime(date) {
        // Hack For IE
        if (!String.prototype.padStart) {
            String.prototype.padStart = function padStart(targetLength, padString) {
                targetLength = targetLength >> 0; //truncate if number, or convert non-number to 0;
                padString = String(typeof padString !== 'undefined' ? padString : ' ');
                if (this.length >= targetLength) {
                    return String(this);
                } else {
                    targetLength = targetLength - this.length;
                    if (targetLength > padString.length) {
                        padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
                    }
                    return padString.slice(0, targetLength) + String(this);
                }
            };
        }
        var date = new Date(date);
        var hours = date.getHours() > 12 ? date.getHours()-12 : date.getHours()
        var amPm = date.getHours() >= 12 ? 'PM' : 'AM';
        var convertedDate = hours+':'+String(date.getMinutes()).padStart(2, "0")+' '+amPm;
        return convertedDate;
    }

    dateFormat(date) {
        if(date) {
            var date = new Date(date);
            var convertedDate = date.getMonth()+1+'/'+date.getDate()+'/'+date.getFullYear();
            return convertedDate;
        }
    }
    
    allIndexOf(str, toSearch) {
        var indices = [];
        for(var pos = str.indexOf(toSearch); pos !== -1; pos = str.indexOf(toSearch, pos + 1)) {
            indices.push(pos);
        }
        return indices;
    }

    setLeftNavHeight() {
        setTimeout(() => {
            if(document.getElementById('leftNavBar')) {
                document.getElementById('leftNavBar').style.height = 'auto';
                const docHeight = document.getElementById('root').scrollHeight;;
                document.getElementById('leftNavBar').style.height = docHeight+100+'px';
            }
        }, 100);
    }

    getFileNameByContentDisposition(contentDisposition){
        var regex = /filename[^;=\n]*=(UTF-8(['"]*))?(.*)/;
        var matches = regex.exec(contentDisposition);
        var filename;
    
        if (matches != null && matches[3]) { 
          filename = matches[3].replace(/['"]/g, '');
        }
    
        return decodeURI(filename);
    }

    handleTableHeight(count) {
        if(count < 4) {
            document.getElementsByClassName('tableContainer ')[0].style.height = '250px'
        } else {
            document.getElementsByClassName('tableContainer ')[0].style.height = '500px'
        }
    }

    userName() {
        const user = JSON.parse(localStorage.getItem('userData'))
        return !user ? null : user['middleName'] ? `${user['firstName']} ${user['middleName']} ${user['lastName']}` : `${user['firstName']} ${user['lastName']}`
    }

    getUserRole() {
        const user = JSON.parse(localStorage.getItem('userData'));
        return user && user.accountType ? user.accountType : null;
    }

    getProfilePic() {
        const user = JSON.parse(localStorage.getItem('userData'));
        return user && user.profilePic ? user.profilePic.url : null;
    }

    getAdminRole() {
        const user = JSON.parse(localStorage.getItem('adminUserData'));
        return user && user.accountType ? user.accountType : null;
    }

    getFullName(user) {
        return user['middleName'] ? `${user['firstName']} ${user['middleName']} ${user['lastName']}` : `${user['firstName']} ${user['lastName']}`
    }

    getFullNameWithEllipsis(user) {
        const name = user['middleName'] ? `${user['firstName']} ${user['middleName']} ${user['lastName']}` : `${user['firstName']} ${user['lastName']}`;
        return name.length > 30 ? name.slice(0,30)+'...' : name

    }

    getUserId() {
        const user = JSON.parse(localStorage.getItem('userData'));
        return user && user.id ? user.id : null;
    }

    getUserData() {
        const user = JSON.parse(localStorage.getItem('userData'));
        return user ? user : null;
    }

    getAccountTypeName(name) {
        let nameMask = '';
        if(name == 'SecondaryGP') {
            nameMask = 'Co Signatory'
        } else if (name == 'GP') {
            nameMask = 'Fund Manager'
        } else if (name == 'OfflineLP') {
            nameMask = 'Offline Investor'
        } else if (name == 'LP') {
            nameMask = 'Investor'
        } else if (name == 'LPDelegate') {
            nameMask = 'Investor Delegate'
        } else if (name == 'GPDelegate') {
            nameMask = 'Fund Manager Delegate'
        } else if (name == 'SecondaryLP') {
            nameMask = 'Secondary Signatory - Investor'
        } else if (name == 'FSNETAdministrator') {
            nameMask = 'Vanilla Administrator'
        }
        return nameMask ? nameMask : name
    }

    getFirmId() {
        const firmId = localStorage.firmId;
        return firmId ? parseInt(firmId) : null;
    }

    getToken() {
        const token = JSON.parse(localStorage.getItem('token'));
        return token ? token : null;
    }

    decodeUrl() {
        const decodeUrl = decodeURI(window.location.search)
                            .replace('?', '')
                            .split('&')
                            .map(param => param.split('='))
                            .reduce((values, [ key, value ]) => {
                                values[ key ] = value
                                return values
                            }, {})
        return decodeUrl ? decodeUrl : null
    }

    _getSubscriptionPagesCount(type) {
        const roles = ['GP','GPDelegate','SecondaryGP','LPDelegate','SecondaryLP'];
        const currentRole = this.getUserRole();
        let count;
        if(type == 'Individual') {
            count = roles.indexOf(currentRole) == -1 ? 5 : 5
        } else if(type == 'LLC' || type == 'Trust') {
            count = roles.indexOf(currentRole) == -1 ? 8 : 8
        }
        return count ? count : 0;
    }

    _getClassNameForSubStatus(status) {
        let className = ''
        if(status == 'Closed') {
            className = 'dot-closed'
        } else if(status == 'Close-Ready') {
            className = 'dot-closeReady'
        } else if(status == 'Open-Ready-Draft' || status == 'Waiting For Signatory Signs' || !status) {
            className = 'dot-openReady-Draft'
        } else if(status == 'Open') {
            className = 'dot-open'
        } else if(status == 'declined' || status == 'Not Interested' || status == 'Declined' || status == 'Rescind') {
            className = 'dot-Not-Participating'
        }

        return className ? className : 'dot-open'
    }

    _CheckSubscriptionFormCompleted(obj) {
        let mandatoryFields = [];
        let isNoEmpty = true
        if(obj.investorType == 'Individual') {
            mandatoryFields = ['isSubjectToDisqualifyingEvent','mailingAddressCountry','areYouAccreditedInvestor','areYouQualifiedPurchaser','lpCapitalCommitment']
        } else if(obj.investorType == 'LLC') {
            mandatoryFields = ['isSubjectToDisqualifyingEvent','mailingAddressCountry','areYouAccreditedInvestor','areYouQualifiedPurchaser',
                'companiesAct','employeeBenefitPlan','lpCapitalCommitment']
        } else if(obj.investorType == 'Trust') {
            mandatoryFields = ['isSubjectToDisqualifyingEvent','mailingAddressCountry','areYouAccreditedInvestor','areYouQualifiedPurchaser',
                'companiesAct','employeeBenefitPlan','lpCapitalCommitment']
        }

        if(obj.investorType) {
            for(let idx of mandatoryFields) {
                if(obj[idx] == null || obj[idx] == undefined) {
                    isNoEmpty = false
                    break;
                }
            }
        } else {
            isNoEmpty = false
        }


        return isNoEmpty;
    }

    _sortArrayWithDate(list) {
        const  sortList = list.sort(this.custom_sort);
        return sortList ? sortList : []
    }

    _sortArrayWithDateAsc(list) {
        const  sortList = list.sort(this.custom_sort_asc);
        return sortList ? sortList : []
    }

    custom_sort(a, b) {
        return new Date(b.invitedDate).getTime() - new Date(a.invitedDate).getTime();
    }

    custom_sort_asc(a, b) {
        return new Date(a.invitedDate).getTime() - new Date(b.invitedDate).getTime();
    }

    trackerTitleAsc(a, b) {
        // Use toUpperCase() to ignore character casing
        const genreA = a.trackerTitle ? a.trackerTitle.toUpperCase() : '(NEW INVESTOR PENDING INPUT)'.toUpperCase(); 
        const genreB = b.trackerTitle ? b.trackerTitle.toUpperCase() : '(NEW INVESTOR PENDING INPUT)'.toUpperCase(); 
      
        let comparison = 0;
        if (genreA > genreB) {
          comparison = 1;
        } else if (genreA < genreB) {
          comparison = -1;
        }
        return comparison;
    }

    trackerTitleDesc(a, b) {
        // Use toUpperCase() to ignore character casing
        const genreA = a.trackerTitle ? a.trackerTitle.toUpperCase() : '(NEW INVESTOR PENDING INPUT)'.toUpperCase(); 
        const genreB = b.trackerTitle ? b.trackerTitle.toUpperCase() : '(NEW INVESTOR PENDING INPUT)'.toUpperCase(); 
      
        let comparison = 0;
        if (genreA > genreB) {
          comparison = -1;
        } else if (genreA < genreB) {
          comparison = 1;
        }
        return comparison;
    }


    _sortUsingtrackerTitleAsc(list) {
       return list.sort(this.trackerTitleAsc);
    }

    _sortUsingtrackerTitleDesc(list) {
        return list.sort(this.trackerTitleDesc);
    }

    firstNameAsc(a, b) {
        // Use toUpperCase() to ignore character casing
        const genreA = a['middleName'] ? `${a['firstName']} ${a['middleName']} ${a['lastName']}` : `${a['firstName']} ${a['lastName']}`
        const genreB = b['middleName'] ? `${b['firstName']} ${b['middleName']} ${b['lastName']}` : `${b['firstName']} ${b['lastName']}`
      
        let comparison = 0;
        if (genreA > genreB) {
          comparison = 1;
        } else if (genreA < genreB) {
          comparison = -1;
        }
        return comparison;
    }

    firstNameDesc(a, b) {
        // Use toUpperCase() to ignore character casing
        const genreA = a['middleName'] ? `${a['firstName']} ${a['middleName']} ${a['lastName']}` : `${a['firstName']} ${a['lastName']}`
        const genreB = b['middleName'] ? `${b['firstName']} ${b['middleName']} ${b['lastName']}` : `${b['firstName']} ${b['lastName']}`
      
        let comparison = 0;
        if (genreA > genreB) {
          comparison = -1;
        } else if (genreA < genreB) {
          comparison = 1;
        }
        return comparison;
    }

    _sortUsingfirstNameAsc(list) {
       return list.sort(this.firstNameAsc);
    }

    _sortUsingfirstNameDesc(list) {
        return list.sort(this.firstNameDesc);
    }

    _emailValidation(email) {
        let error = false;
        let emailRegex = /^([a-zA-Z0-9_+\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        if (email && !emailRegex.test(email)) {
            error = true;
        }
        return error
    }

    _cellNumberValidation(number) {
        let error = false;
        if (number && (number.length < 12 || number.length > 13)) {
            error = true;
        }
        return error
    }

    _getAllRoles() {
        const allusers = localStorage.getItem('allusers');
        return allusers ? JSON.parse(allusers) : [];
    }

    _impersonateSetData(data) {
        localStorage.setItem('adminUserData', localStorage.getItem('userData'));
        localStorage.setItem('userData', JSON.stringify(data.user));
        localStorage.setItem('firmId', JSON.stringify(data.user.vcfirmId));
        localStorage.setItem('adminToken', localStorage.getItem('token'));
        localStorage.setItem('token', JSON.stringify(data.token));
        localStorage.setItem('allusers', data.allusers && data.allusers.length > 1 ? JSON.stringify(data.allusers) : []);
    }

    _userSetData(data) {
        localStorage.setItem('userData', JSON.stringify(data.user));
        localStorage.setItem('firmId', JSON.stringify(data.user.vcfirmId));
        localStorage.setItem('token', JSON.stringify(data.token));
        localStorage.setItem('allusers', data.allusers && data.allusers.length > 1 ? JSON.stringify(data.allusers) : []);
    }

    _removeAdminData() {
        const userData = localStorage.getItem('adminUserData');
        const token = localStorage.getItem('adminToken');
        localStorage.clear();
        localStorage.setItem('userData',userData);
        localStorage.setItem('token',token );
        localStorage.setItem('allusers',[])
    }

    _openDoc(url) {
        if(url.indexOf('token=') == -1) {
            url = url+`?token=${this.getToken()}`
        }
        window.open(url, '_blank', 'width = 1000px, height = 800px');
    }

    _openNewTab(url) {
        if(url.indexOf('token=') == -1) {
            url = url+`?token=${this.getToken()}`
        }
        window.open(url, '_blank');
    }

    _ClearLocalStorage(proceed) {
        localStorage.clear();
        if(!proceed) {
            window.location.href = '/'
        }
    }

    _isIEEdge() {
        return /Edge\/\d./i.test(navigator.userAgent) ? true : false
    }

    _getTitleName(step) {
        let name = '';
        if(step === 'default') {
            name = `Your Subscription Agreement has been completed. Please review the
            document below. Once you are satisfied, please click "Confirm and Sign"
            to append your signature.`
        } else if(step === 'SA') {
            name = `Please review and approve your Subscription Agreement.`
        } else if(step === 'FA') {
            name = `Please review and approve the Fund Agreement.`
        } else if(step === 'SL') {
            name = `Please review and approve the Side Letter Agreement.`
        } else if(step === 'CP') {
            name = `Please review the Changed pages document.`
        } else if(step === 'AA') {
            name = `Please review and approve your Amendment to Fund Agreement.`
        } else if(step === 'CC') {
            name = `Please review and approve your Capital Commitment Agreement.`
        }
        return name;
    }

    UtcDateToTimezoneDate(date, offset)
    {
        if(date && offset) {
            return moment(date).utcOffset(offset).format('MM-DD-YYYY HH:mm:ss');
        } else {
            return date;
        }
    }

    UtcDateToDateTimeFormat(date)
    {
        return moment(date).format('MM-DD-YYYY HH:mm:ss ')
    }

    UtcDateToLocalTimezone(date)
    {
        let offset = new Date().getTimezoneOffset();
        return moment(date).utcOffset(offset).format('MM-DD-YYYY HH:mm:ss');
    }

    UtcDateToLocalTimezoneWithoutFormat(date)
    {
        let offset = new Date().getTimezoneOffset();
        return moment(date).utcOffset(offset).format();
    }

    getDateFromFormat(date)
    {
        let Date = date.split(" ");
        return Date[0];
    }

    getTimeFromFormat(date)
    {
        let Date = date.split(" ");
        if(Date[1] && Date[2])
            return Date[1]+' '+Date[2];
        else
            return Date[1];
    }


}

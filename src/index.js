import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import FsnetRouter from './actions/router';
import { Provider } from 'react-redux'
import configureStore from './store';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './polyfills'
// import registerServiceWorker from './registerServiceWorker';
import { unregister } from './registerServiceWorker';

ReactDOM.render(
<Provider store={configureStore()}>
  <FsnetRouter />
 </Provider>,
 document.getElementById('root'));
//registerServiceWorker();
unregister();   
